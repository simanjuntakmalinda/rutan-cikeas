﻿using Dapper;
using RDM.LP.DataAccess.Helper;
using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Service;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;


namespace RDM.LP.DataAccess.Model
{
    public class SqlBaseRepository : IRepository, IDisposable
    {
        private readonly SqlConnection sQLConnection;

        public SqlBaseRepository()
        {
            var datasource = Properties.Settings.Default.DBDataSource;
            var username = Properties.Settings.Default.DBUserName;
            var password = SimpleRSA.Decrypt(Properties.Settings.Default.DBPassword);
            sQLConnection = new SqlConnection("Data Source=" + datasource + ";Initial Catalog=RDM.RutanManagement;user id="+ username + ";password="+ password + ";");

        }

        public SqlBaseRepository(string datasource, string username, string password)
        {
            Properties.Settings.Default.DBDataSource = datasource;
            Properties.Settings.Default.Save();
            Properties.Settings.Default.DBUserName = username;
            Properties.Settings.Default.Save();
            Properties.Settings.Default.DBPassword = SimpleRSA.Encrypt(password);
            Properties.Settings.Default.Save();
            sQLConnection = new SqlConnection("Data Source=" + datasource + ";Initial Catalog=RDM.RutanManagement;user id=" + username + ";password=" + password + ";");
        }

        public T GetSingle<T>(string sqlQuery, object data = null)
        {
            if (sQLConnection.State == System.Data.ConnectionState.Closed)
            {
                sQLConnection.Open();
            }

            return sQLConnection.QueryFirst<T>(sqlQuery, data);
        }

        public IEnumerable<T> Get<T>(string sqlQuery, object data = null)
        {
            try
            {
                if (sQLConnection.State == System.Data.ConnectionState.Closed)
                {
                    sQLConnection.Open();
                }

                return sQLConnection.Query<T>(sqlQuery, data);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public int InsertIntoDB<T>(string sqlQuery, object data)
        {
            if (sQLConnection.State == System.Data.ConnectionState.Closed)
            {
                sQLConnection.Open();
            }

            return (int)sQLConnection.ExecuteScalar(sqlQuery,data);

        } 

        public void UpdateToDB<T>(string sqlQuery, object data = null)
        {
            if (sQLConnection.State == System.Data.ConnectionState.Closed)
            {
                sQLConnection.Open();
            }

            sQLConnection.Query<T>(sqlQuery, data);
        }

        public void DeleteFromDB<T>(string sqlQuery, T data)
        {
            if (sQLConnection.State == System.Data.ConnectionState.Closed)
            {
                sQLConnection.Open();
            }

            sQLConnection.Query<T>(sqlQuery, data);
        }

        public void ExecuteQuery(string sqlQuery, object data)
        {
            if (sQLConnection.State == System.Data.ConnectionState.Closed)
            {
                sQLConnection.Open();
            }

            sQLConnection.Execute(sqlQuery, data);
        }


        public string ConnString
        {
            get
            {
                return Environment.CurrentDirectory + "\\Database\\DB_EMP.sqlite";
            }
        }

        public SqlConnection Connection
        {
            get { return sQLConnection; }
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    sQLConnection.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SqLiteBaseRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        

        #endregion IDisposable Support
    }
}