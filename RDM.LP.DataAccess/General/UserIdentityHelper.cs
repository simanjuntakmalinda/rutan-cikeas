﻿using RDM.LP.DataAccess.Helper;
using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Service;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Threading.Tasks;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace RDM.LP.DataAccess.General
{
    public class UserIdentityHelper : IDisposable, IUserIdentityHelper
    {
        protected LoginService login;

        public UserIdentityHelper()
        {
            login = new LoginService();
        }

        public UserIdentityHelper(string datasource, string username, string password)
        {
            login = new LoginService();
        }

        public AspnetUsers GetLoggedIn()
        {
            return login.GetLoggedIn();
        }

        public bool Login(string username, string password, bool isLoggedIn, out AspnetUsers DataUser)
        {
            var user = GetLocal(username, password);
            if (user != null)
            {
                if (user.IsLocked == 1)
                {
                    //throw new Exception(String.Format("<html>UserName: {0} status is locked...<br>Please Contact Administrator to Release Lock Status !", username));
                    throw new Exception(String.Format("<html>Status UserName: {0} dikunci...<br>Mohon Menghubungi Administrator untuk Membuka Kunci Status!", username));
                }

                DataUser = user;
                DataUser.IsLoggedIn = isLoggedIn ? 1 : 0;
                UpdateLastLogin(DataUser);
                return true;
            }
            else
            {
                //throw new Exception(String.Format("<html>Invalid UserName or Password...<br>Please Login With Valid UserName dan Password...!"));
                throw new Exception(String.Format("<html>Username atau Password Salah...<br>Silahkan Login dengan Username dan Password yang tepat...!"));
            }

        }

        public static string GetBasicSecurityPass(string UserName)
        {
            if (UserName != null)
            {
                var xx = UserName.ToUpper();
                var xxx = "f8e512c5e85b5d00121b2d454be5b332";
                var md5 = CreateMD5(xx + xxx);
                return System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(md5));
            }
            return string.Empty;
        }

        private static string CreateMD5(string input)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        private AspnetUsers GetLocal(string username, string password)
        {
            var IsExist = login.Get(username);
            if (IsExist != null)
            {
                var pwd2 = SimpleRSA.Decrypt(IsExist.Password);
                if (password.Equals(pwd2))
                {
                    return IsExist;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        private void UpdateLastLogin(AspnetUsers data)
        {
            var datalogin = login.Get(data.UserName);
            datalogin.IsLoggedIn = data.IsLoggedIn;
            login.UpdateLastLogin(datalogin);
        }

        private void UpdateLocalTable(AspnetUsers data, string initpassword, bool isLoggedIn)
        {
            var IsExist = login.Get(data.UserName);
            var pwd = SimpleRSA.Encrypt(initpassword);
            if (IsExist != null)
            {
                login.Update(data.UserName, pwd, isLoggedIn);
            }
            else
            {
                data.Password = pwd;
                data.IsLoggedIn = isLoggedIn ? 1 : 0;
                login.Create(data);
            }
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    login.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~UserIdentityHelper() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}