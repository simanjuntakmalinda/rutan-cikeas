﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class UsersParam : BaseItem
    {
        public string UserName { get; set; }
        public string ParamName { get; set; }
        public string ParamValue { get; set; }

    }
}