﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class Building : BaseItem
    {

        public string BuildingName { get; set; }

        public int JumlahLantai { get; set; }

    }
}