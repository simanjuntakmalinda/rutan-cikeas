﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class PangkatPolisi : BaseItem
    {
        public string Nama { get; set; }
        public string Kepanjangan { get; set; }
    }
}