﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class AuditTrail 
    {
        public string UserName { get; set; }

        public string Modul { get; set; }

        public string Activities { get; set; }

        public DateTime LogDate { get; set; }
    }
}