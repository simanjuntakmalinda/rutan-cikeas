﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class CellStatus : BaseItem
    {

        public string Status { get; set; }

    }
}