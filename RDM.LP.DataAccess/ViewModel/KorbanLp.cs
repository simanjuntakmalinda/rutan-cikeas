﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.ViewModel
{
    public class KorbanLp : BaseItem
    {
        public int LpId { get; set; }

        public int KorbanId { get; set; }

        public int? TipeIdKorban { get; set; }

        public string NamaPeroranganKorban { get; set; }

        public string NamaPerusahaanKorban { get; set; }

        public string NamaInstansiKorban { get; set; }

        public string KontakKorban { get; set; }

        public int? NegaraIdKorban { get; set; }

        public string SIUP { get; set; }

        public string JenisIdentitasKorban { get; set; }

        public string NomorIdentitasKorban { get; set; }

        public string TempatLahirKorban { get; set; }

        public DateTime? TanggalLahirKorban { get; set; }

        public string JenisKelaminKorban { get; set; }

        public int? SukuIdKorban { get; set; }

        public int? PekerjaanIdKorban { get; set; }

        public int? AgamaIdKorban { get; set; }

        public int? PendidikanIdKorban { get; set; }

        public string NomorTeleponKorban { get; set; }

        public string EmailKorban { get; set; }

        public int? PropinsiIdKorban { get; set; }

        public int? KotaIdKorban { get; set; }

        public int? KecamatanIdKorban { get; set; }

        public int? KelurahanIdKorban { get; set; }

        public int? UmurKorban { get; set; }

        public string AlamatKorban { get; set; }

        public int PendudukId { get; set; }

        public string IbuKandung { get; set; }

        public string BapakKandung { get; set; }

        public string NamaAlias { get; set; }

        public bool? StatusKawin { get; set; }
    }
}
