﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class StatusTahanan : BaseItem
    {
        public string RegID { get; set; }
        public string FullName { get; set; }
        public string NoSel { get; set; }
        public string NoSuratPenangkapan { get; set; }
        public DateTime? TanggalPenangkapan { get; set; }
        public string NoSuratPenahanan { get; set; }
        public DateTime? TanggalPenahanan { get; set; }
        public string CellCode { get; set; }
    }
    
}