﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class InmatesCase : BaseItem
    {
        public int CaseId { get; set; }
        public string RegID { get; set; }
        public string CaseName { get; set; }
    }
}