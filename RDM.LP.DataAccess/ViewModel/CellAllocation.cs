﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.ViewModel
{
    public class CellAllocation : BaseItem
    {
        public string InmatesRegCode { get; set; }

        public string CellCode { get; set; }
        public int CellCodeId { get; set; }
        public DateTime DateEnter { get; set; }

        public string CellStatus { get; set; }
        public bool IsCheckedOut { get; set; }
        public DateTime? CheckedOutDate { get; set; }
        public string CheckedOutNote { get; set; }
        public int CellAllocationStatusId { get; set; }
    }

    public class CellAllocate : Inmates
    {
        public string CellCode { get; set; }
        public int CellCodeId { get; set; }
        public DateTime DateEnter { get; set; }
        public DateTime DateReg { get; set; }
        public string CellStatus { get; set; }
        public string InmatesRegCode { get; set; }
        public string SourceImage { get; set; }
        public string CellAllocationStatus { get; set; }
        public int CellAllocationStatusId { get; set; }
        public bool IsCheckedOut { get; set; }
        public DateTime? CheckedOutDate { get; set; }
        public string CheckedOutNote { get; set; }
        public string BuildingName { get; set; }
        public string CellFloor { get; set; }
        public string CellType { get; set; }
        public string InUseCounter { get; set; }
        public string IDInmates { get; set; }
        public string BuildFloor { get; set; }
    }
}
