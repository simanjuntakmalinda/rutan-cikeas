﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class Kelurahan : BaseItem
    {
        public int Id_Kelurahan { get; set; }

        public int KecamatanId { get; set; }

        public string Nama_Kelurahan { get; set; }
    }
}