﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class HeaderSurat
    {
        public string Header { get; set; }
        public string TempatDikeluarkan { get; set; }
    }
}