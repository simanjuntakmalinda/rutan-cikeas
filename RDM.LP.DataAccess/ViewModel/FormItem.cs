﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class FormItem : BaseItem
    {

        public int FormItem_Id { get; set; }

        public int Form_Id { get; set; }

        public string Label { get; set; }

        public string Pilihan { get; set; }
    }
}