﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class Negara : BaseItem
    {
        public int Id_Negara { get; set; }

        public string Kode_Negara { get; set; }

        public string Nama_Negara { get; set; }
    }
}