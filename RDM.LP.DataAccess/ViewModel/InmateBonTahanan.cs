﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.ViewModel
{
    public class InmateBonTahanan : BaseItem
    {
        public string RegID { get; set; }
        public int BonTahananId { get; set; }
    }

    public class GetInmateBontahanan : Inmates
    {
        public string BuildingName { get; set; }
        public string CellNo { get; set; }
        public string NamaLengkap { get; set; }
        public int BonTahananId { get; set; }
    }
}
