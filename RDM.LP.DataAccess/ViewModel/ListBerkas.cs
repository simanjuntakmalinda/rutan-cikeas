﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class ListBerkas : BaseItem
    {
        public int List_Berkas_Id { get; set; }

        public int Case_Id { get; set; }

        public byte State { get; set; }

        public byte Status_Upload { get; set; }

        public int Forms_Id { get; set; }

        public int? Id_Key { get; set; }

        public string No_Berkas { get; set; }

        public string Tanggal { get; set; }

        public string TanggalDitandatangani { get; set; }

        public string File_Name { get; set; }

        public string UploadedBy { get; set; }

        public DateTime? UploadedDate { get; set; }

        public int? TotalHalaman { get; set; }

        public bool? Approve { get; set; }

        public string ApproveBy { get; set; }

        public int? KeyIdOriginal { get; set; }

        public int? SprinlidikSprindikIdOriginal { get; set; }

        public string TandaTanganText { get; set; }

        public string MengetahuiText { get; set; }

        public byte? Versi { get; set; }

        public bool? IsLimpah { get; set; }

        public int? LokasiIdDibuat { get; set; }
    }
}