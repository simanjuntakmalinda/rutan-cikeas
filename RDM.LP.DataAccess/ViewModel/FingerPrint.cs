﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class FingerPrint : BaseItem
    {

        public string ImagePrint { get; set; }

        public string ByteImagePrint { get; set; }

        public string FingerId { get; set; }

        public string RegID { get; set; }

        public bool IsVisitor { get; set; }
        
        public int InmateId { get; set; }
        
        public int VisitorId { get; set; }

        public string FullName { get; set; }

    }
}