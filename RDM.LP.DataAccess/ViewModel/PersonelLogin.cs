﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class PersonelLogin
    {
        public bool StatLogin { get; set; }
        public string StrMessage { get; set; }
        public string DeviceId { get; set; }
        public string NoHP { get; set; }
        public string status_login { get; set; }
        public int personel_id { get; set; }
        public string personel_nama { get; set; }
        public string personel_agama { get; set; }
        public DateTime personel_tanggal_lahir { get; set; }
        public string personel_kelamin { get; set; }
        public string personel_no_telp { get; set; }
        public string personel_email { get; set; }
        public string personel_nrp { get; set; }
        public string personel_pangkat { get; set; }
        public string personel_pangkat_singkat { get; set; }
        public string personel_jabatan { get; set; }
        public string personel_status_kepenyidikan { get; set; }
        public string personel_kepegawaian { get; set; }
        public string personel_nomor_skep { get; set; }
        public string personel_satker { get; set; }
        public string personel_universitas { get; set; }
        public string personel_pendidikan_polri { get; set; }
        public int personel_tahun_lulus { get; set; }
        public string personel_status_reserse { get; set; }
        public string personel_tahun_pendidikan_reserse { get; set; }
        public string personel_tempat_pendidikan_reserse { get; set; }
        public string personel_materi_serse { get; set; }
        public string personel_nomor_telepon { get; set; }
        public string UserName { get; set; }
        public string personel_device_id { get; set; }
        public string Token1 { get; set; }
    }
}