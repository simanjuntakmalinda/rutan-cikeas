﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class SQLLogData : BaseItem
    {
        public string SqlText { get; set; }

    }
}