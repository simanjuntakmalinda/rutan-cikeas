﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class Photos : BaseItem
    {
        public string RegID { get; set; }
        public string Type { get; set; }
        public string Data { get; set; }
        public string Note { get; set; }
    }
}