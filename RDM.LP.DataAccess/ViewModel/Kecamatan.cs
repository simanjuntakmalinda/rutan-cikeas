﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class Kecamatan : BaseItem
    {
        public int Id_Kecamatan { get; set; }

        public int KotaId { get; set; }

        public string Nama_Kecamatan { get; set; }
    }
}