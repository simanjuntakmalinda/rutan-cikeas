﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.ViewModel
{
    public class BonTahanan : BaseItem
    {
        public string NoBonTahanan { get; set; }
        public string RegID { get; set; }
        public string NamaTahanan { get; set; }
        public string BuildingName { get; set; }
        public string CellCode { get; set; }
        public string CellNo { get; set; }

        public string Keperluan { get; set; }
        public string NamaKeperluan { get; set; }
        public string Lokasi { get; set; }
        public DateTime TanggalMulai { get; set; }
        public DateTime TanggalSelesai { get; set; }
        public string Keterangan { get; set; }

        public string TipePemohon { get; set; }
        public string NamaPemohon { get; set; }
        public string NrpPemohon { get; set; }
        public string PangkatPemohon { get; set; }
        public string NamaPangkatPemohon { get; set; }
        public string SatkerPemohon { get; set; }
        public string NoHpPemohon { get; set; }

        public string NamaPenanggungJawab { get; set; }
        public string NrpPenanggungJawab { get; set; }
        public string PangkatPenanggungJawab { get; set; }
        public string NamaPangkatPenanggungJawab { get; set; }
        public string SatkerPenanggungJawab { get; set; }
        public string NoHpPenanggungJawab { get; set; }

        public string NamaPenyetuju { get; set; }
        public string NrpPenyetuju { get; set; }
        public string PangkatPenyetuju { get; set; }
        public string NamaPangkatPenyetuju { get; set; }
        public string SatkerPenyetuju { get; set; }
        public string NoHpPenyetuju { get; set; }

        public string StatusTahanan { get; set; }
    }
}
