﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class Network : BaseItem
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Inmates { get; set; }
        public string SeizedAssets { get; set; }
    }
}