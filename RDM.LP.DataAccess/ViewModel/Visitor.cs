﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class Visitor : BaseItem
    {
        public string RegID { get; set; }
        public string FullName { get; set; }
        public string IDType { get; set; }
        public string IDNo { get; set; }
        public string Address { get; set; }
        public string TelpNo { get; set; }
        public string Gender { get; set; }
        public string Religion { get; set; }
        public string City { get; set; }
        public string PlaceOfBirth { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string MaritalStatus { get; set; }
        public string Vocation { get; set; }
        public string Nationality { get; set; }
        public DateTime DateReg { get; set; }
    }
}