﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class DasboardChart
    {
        public int Category { get; set; }
        public int Value { get; set; }
    }
}