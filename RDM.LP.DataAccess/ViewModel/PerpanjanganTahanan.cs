﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class PerpanjanganTahanan : BaseItem
    {
        public string RegID { get; set; }
        public int StatusTahananId { get; set; }
        public string PerpanjanganKe { get; set; }
        public string NoSurat { get; set; }
        public DateTime? TanggalPerpanjangan { get; set; }
        public int Durasi { get; set; }
        public string Dokumen { get; set; }
    }
}