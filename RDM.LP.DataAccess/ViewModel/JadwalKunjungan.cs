﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class JadwalKunjungan : BaseItem
    {
        public string InmatesRegID { get; set; }

        public string InmatesName { get; set; }

        public string NoIdentitas { get; set; }

        public string NoSelTahanan { get; set; }

        public bool BolehDikunjungi { get; set; }

        public string PeriodeKunjungan { get; set; }

        public string WaktuKunjungan { get; set; }
    }
}