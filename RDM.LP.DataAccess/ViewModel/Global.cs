﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.ViewModel
{
    public static class Global
    {
        //public static Color MainColor = Color.FromArgb(63,81,181);
        public static Color MainColor = Color.FromArgb(45, 45, 48);
        //public static Color MainColorLight = Color.FromArgb(20, 192, 57, 43);
        public static Color MainColorLight = Color.White;
        public static string MainFont = "Roboto";
        public static string MainFontMedium = "Roboto Medium";
        public static string MainFontLight = "Roboto Light";
        public static float CustomFontSizeMain = 13.5f;
    }
}
