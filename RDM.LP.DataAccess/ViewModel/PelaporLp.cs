﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.ViewModel
{
    public class PelaporLp : BaseItem
    {
        public int LpId { get; set; }

        public Nullable<int> PendudukId { get; set; }

        public int PelaporId { get; set; }

        public string JenisIdentitasPelapor { get; set; }

        public string NomorIdentitasPelapor { get; set; }

        public string NamaPelapor { get; set; }

        public string JenisKelaminPelapor { get; set; }

        public string AlamatPelapor { get; set; }

        public string TempatLahirPelapor { get; set; }

        public DateTime? TanggalLahirPelapor { get; set; }

        public int? UmurPelapor { get; set; }

        public int? SukuIdPelapor { get; set; }

        public int? PekerjaanIdPelapor { get; set; }

        public int? PendidikanIdPelapor { get; set; }

        public string KewarganegaraanPelapor { get; set; }

        public int? NegaraIdPelapor { get; set; }

        public int? PropinsiIdPelapor { get; set; }

        public int? KotaIdPelapor { get; set; }

        public int? KecamatanIdPelapor { get; set; }

        public int? KelurahanIdPelapor { get; set; }

        public string KampungPelapor { get; set; }

        public string JalanPelapor { get; set; }

        public int? AgamaIdPelapor { get; set; }

        public string NomorTeleponPelapor { get; set; }

        public string EmailPelapor { get; set; }

        public bool AlamatNonNKRI { get; set; }

        public string AlamatLengkapNonNKRI { get; set; }

        public string PemberiKuasa { get; set; }

        public string IbuKandung { get; set; }

        public string BapakKandung { get; set; }

        public string NamaAlias { get; set; }

        public Nullable<bool> StatusKawin { get; set; }

        public string PekerjaanPemberiKuasa { get; set; }

        public string AlamatPemberiKuasa { get; set; }
    }
}
