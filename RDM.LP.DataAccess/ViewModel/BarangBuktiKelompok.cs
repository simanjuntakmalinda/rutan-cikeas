﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class BarangBuktiKelompok : BaseItem
    {
        public string Kode { get; set; }
        public string Nama { get; set; }
    }
}