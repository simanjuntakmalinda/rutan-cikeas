﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.ViewModel
{
    public class KendaraanTahanan:BaseItem
    {
        public int KategoriKendaraan { get; set; }

        public string NoPlat { get; set; }

        public string Pengemudi { get; set; }

        public DateTime TanggalMasuk { get; set; }

        public DateTime? WaktuMasuk { get; set; }

        public string ZonaWaktuMasuk { get; set; }

        public string Keperluan { get; set; }

        public string Catatan { get; set; }

        public bool IsCheckedOut { get; set; }

        public DateTime? WaktuKeluar { get; set; }

        public string NamaKategoriKendaraan { get; set; }
    }
    
}
