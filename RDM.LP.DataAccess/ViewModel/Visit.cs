﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class Visit : BaseItem
    {
        public int InmatesId { get; set; }

        public string InmatesName { get; set; }

        public int VisitorId { get; set; }

        public string VisitorName { get; set; }

        public DateTime? StartVisit { get; set; }

        public DateTime? EndVisit { get; set; }

        public bool IsCheckedOut { get; set; }

        public string Relation { get; set; }

        public string Type { get; set; }

        public string BringingItems { get; set; }

        public string Notes { get; set; }
    }
}