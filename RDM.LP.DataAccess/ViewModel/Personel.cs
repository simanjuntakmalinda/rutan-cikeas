﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class Personel : BaseItem
    {
        public int RoleId { get; set; }
        public int? PangkatPolisiId { get; set; }
        public string Pangkat { get; set; }
        public int TipePersonelId { get; set; }
        public string Nama { get; set; }
        public string Nrp { get; set; }
        public string NomorTelepon { get; set; }
        public string Email { get; set; }
        public int? MemberUserIdOriginal { get; set; }
        public int? JenisMember { get; set; }
        public string MemberUser { get; set; }
        public string JenisKelamin { get; set; }
        public DateTime TanggalLahir { get; set; }
        public string Agama { get; set; }
        public string Gelar { get; set; }
        public int? LokasiPolisiId { get; set; }
        public string LokasiPolisi { get; set; }
        public string Universitas { get; set; }
        public string PendidikanPolri { get; set; }
        public int? TahunLulusPolri { get; set; }
        public string StatusSertifikasi { get; set; }
        public string NomorSertifikasi { get; set; }
        public string StatusReserse { get; set; }
        public int? JenisPersonel { get; set; }
        public int? TahunPendidikanSerse { get; set; }
        public string TahunPendidikanSerseText { get; set; }
        public string TempatPendidikanSerse { get; set; }
        public string MateriSerse { get; set; }
        public string StatusPenyidik { get; set; }
        public string Foto { get; set; }
        public bool? IsActive { get; set; }
        public string Encrypt1 { get; set; }
        public string Encrypt2 { get; set; }
        public bool? StatusSkep { get; set; }
        public string NomorSkep { get; set; }
        public string DeviceId { get; set; }
        public int? LokasiPolisiBKOAsal { get; set; }
        public int? PersonelId { get; set; }
        public int? PersonelID { get; set; }
        public string FullNama { get; set; }
    }
}