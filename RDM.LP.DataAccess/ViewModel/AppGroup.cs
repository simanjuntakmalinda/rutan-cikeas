﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class AppGroup : BaseItem
    {
        public string Group { get; set; }
        public int Active { get; set; }
    }
}