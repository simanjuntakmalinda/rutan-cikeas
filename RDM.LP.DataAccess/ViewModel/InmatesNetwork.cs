﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class InmatesNetwork : BaseItem
    {
        public int NetworkId { get; set; }
        public string RegID { get; set; }
        public string NetworkName { get; set; }

    }
}