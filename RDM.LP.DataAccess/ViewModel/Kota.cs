﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class Kota : BaseItem
    {
        public int Id_Kota { get; set; }

        public int PropinsiId { get; set; }

        public string Nama_Kota { get; set; }
    }
}