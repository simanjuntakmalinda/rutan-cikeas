﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class BarangBukti : BaseItem
    {

        public int LpId { get; set; }

        public int CaseId { get; set; }

        public int? KategoriId { get; set; }

        public int? DetailId { get; set; }

        public decimal? Jumlah { get; set; }

        public string Satuan { get; set; }

        public string XMLDetail { get; set; }

        public string MerkSenpi { get; set; }

        public string TypeSenpi { get; set; }

        public string Caliber { get; set; }

        public string NomorSeri { get; set; }

        public string NomorRegistrasi { get; set; }

        public int? IsiSilinder { get; set; }

        public string NamaPemilik { get; set; }

        public string NomorRangka { get; set; }

        public string Alamat { get; set; }

        public string NomorMesin { get; set; }

        public string Merk { get; set; }

        public string Warna { get; set; }

        public string Type { get; set; }

        public string BahanBakar { get; set; }

        public string Jenis { get; set; }

        public string WarnaTNKB { get; set; }

        public string Model { get; set; }

        public int? TahunRegistrasi { get; set; }

        public int? TahunPembuatan { get; set; }

        public string NomorBPKB { get; set; }

        public int? Klasifikasi { get; set; }

        public string Bentuk { get; set; }

        public string Merek { get; set; }

        public string Detail { get; set; }

        public string Nama { get; set; }

        public string Golongan { get; set; }

        public string Instansi { get; set; }

        public int? LpBarangBuktiIdOriginal { get; set; }

        public string Penyimpan { get; set; }

        public int? Status { get; set; }

        public string Keterangan { get; set; }

        public int? TipeBB { get; set; }

        public string NamaKategoriBB { get; set; }

        public string NamaDetailBB { get; set; }
    }
}