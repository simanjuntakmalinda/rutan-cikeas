﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class TipePersonel : BaseItem
    {
        public string Nama { get; set; }
    }
}