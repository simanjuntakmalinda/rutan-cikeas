﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.ViewModel
{
    public class TerlaporLp : BaseItem
    {

        public int LpId { get; set; }

        public string JenisIdentitasTerlapor { get; set; }

        public string NomorIdentitasTerlapor { get; set; }

        public string NamaTerlapor { get; set; }

        public bool JenisKelaminTerlapor { get; set; }

        public string JenisKelamin { get; set; }

        public string AlamatTerlapor { get; set; }

        public string TempatLahirTerlapor { get; set; }

        public DateTime? TanggalLahirTerlapor { get; set; }

        public int? SukuIdTerlapor { get; set; }

        public int? PekerjaanIdTerlapor { get; set; }

        public int? AgamaIdTerlapor { get; set; }

        public int? PendidikanIdTerlapor { get; set; }

        public int? NegaraIdTerlapor { get; set; }

        public string NomorTeleponTerlapor { get; set; }

        public int? UmurTerlapor { get; set; }

        public string EmailTerlapor { get; set; }

        public string KewarganegaraanTerlapor { get; set; }

        public virtual string PekerjaanNama { get; set; }

        public virtual string AgamaNama { get; set; }

        public virtual string PendidikanNama { get; set; }

        public int PendudukId { get; set; }

        public string KampungTerlapor { get; set; }

        public string IbuKandung { get; set; }

        public string BapakKandung { get; set; }

        public string NamaAlias { get; set; }

        public bool? StatusKawin { get; set; }

        public int? PropinsiIdTerlapor { get; set; }

        public int? KotaIdTerlapor { get; set; }

        public int? KecamatanIdTerlapor { get; set; }

        public int? KelurahanIdTerlapor { get; set; }
    }
}
