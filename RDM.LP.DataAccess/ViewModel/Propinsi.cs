﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class Propinsi : BaseItem
    {
            public int Id_Propinsi { get; set; }

            public string Nama_Propinsi { get; set; }

            public string ZonaWaktu { get; set; }
    }
}