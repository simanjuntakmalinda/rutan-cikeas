﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class AspnetUsers : BaseItem
    {
        public string UserName { get; set; }

        public int UserGroup { get; set; }

        public string Password { get; set; }

        public DateTime? LastLogin { get; set; }

        public int IsLoggedIn { get; set; }

        public int IsLocked { get; set; }

        public string Status { get; set; }

        public string UserGroupName { get; set; }

        public bool InmateModul { get; set; }

        public bool VisitorModul { get; set; }

        public bool CellAllocationModul { get; set; }

        public bool UserManagementModul { get; set; }

        public bool ReportModul { get; set; }

        public bool AuditTrailModul { get; set; }

        public bool SeizedAssetsModul { get; set; }

        public bool BonTahananModul { get; set; }

        public bool PrintFormModul { get; set; }

        public bool KendaraanModul { get; set; }

        public bool JadwalKunjunganModul { get; set; }

    }
}