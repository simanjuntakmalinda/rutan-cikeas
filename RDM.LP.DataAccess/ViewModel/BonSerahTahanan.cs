﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.ViewModel
{
    public class BonSerahTahanan : BaseItem
    {
        public string NoBonTahanan { get; set; }
        public string RegIDTahanan { get; set; }
        public string NamaTahanan { get; set; }
        public string CellCode { get; set; }
        public string CellFloor { get; set; }
        public string CellNumber { get; set; }
        public string BuildingName { get; set; }
        public DateTime TanggalTerima { get; set; }
        public DateTime? JamTerima { get; set; }
        public string ZonaWaktuTerima { get; set; }
        public string StatusTahanan { get; set; }

        public string NamaYangMenerima { get; set; }
        public string NrpYangMenerima { get; set; }
        public string PangkatYangMenerima { get; set; }
        public string SatkerYangMenerima { get; set; }

        public string NamaYangMenyerahkan { get; set; }
        public string NrpYangMenyerahkan { get; set; }
        public string PangkatYangMenyerahkan { get; set; }
        public string SatkerYangMenyerahkan { get; set; }

        public string NamaYangMenyetujui { get; set; }
        public string NrpYangMenyetujui { get; set; }
        public string PangkatYangMenyetujui { get; set; }
        public string SatkerYangMenyetujui { get; set; }
    }

    public class GetSerahBon : BonSerahTahanan
    {
        public int IdBonTahanan { get; set; }
        public int IdPangkatMenyerahkan { get; set; }
        public int IdPangkatMenerima { get; set; }
        public int IdPangkatMenyetujui { get; set; }
    }
}
