﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class Tahanan : BaseItem
    {
        public int? JenisIdentitas { get; set; }

        public string NoIdentitas { get; set; }

        public string FullName { get; set; }

        public string TempatLahir { get; set; }

        public DateTime? TanggalLahir { get; set; }

        public string NamaIbuKandung { get; set; }

        public string NamaAlias { get; set; }

        public int? StatusKawin { get; set; }

        public int? JenisKelamin { get; set; }

        public int? Agama { get; set; }

        public int? Pendidikan { get; set; }

        public string NamaNegara { get; set; }

        public string NoTelepon { get; set; }

        public string AlamatEmail { get; set; }

        public string Alamat { get; set; }

        public string Kasus { get; set; }

        public string KeteranganKasus { get; set; }

    }
}