﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class MasterBarangBukti : BaseItem
    {
        public int? Kelompok_Id { get; set; }
        public string Kode { get; set; }
        public string Jenis { get; set; }
        public string Merk { get; set; }
        public string Bentuk { get; set; }
        public string Satuan { get; set; }
        public int? JumlahRoda { get; set; }
        public string KelompokNama { get; set; }
        public string ddlValue { get; set; }
    }
}