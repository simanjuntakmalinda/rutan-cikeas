﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class Inmates : BaseItem
    {
        public string RegID { get; set; }
        public string FullName { get; set; }
        public string IDType { get; set; }
        public string IDNo { get; set; }
        public string Identitas { get; set; }
        public string Address { get; set; }
        public string TelpNo { get; set; }
        public string Gender { get; set; }
        public string Religion { get; set; }
        public string City { get; set; }
        public string Category { get; set; }
        public string Type { get; set; }
        public string Network { get; set; }
        public DateTime? DateReg { get; set; }
        public string SourceLocation { get; set; }
        public string Notes { get; set; }
        public int? CellAllocationStatusId { get; set; }
        public int? CaseId { get; set; }
        public int? NetworkId { get; set; }
        public string MaritalStatus { get; set; }
        public string Nationality { get; set; }
        public string Vocation { get; set; }
        public string PlaceOfBirth { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CellCode { get; set; }
        public string CellNumber { get; set; }
        public string Cases { get; set; }
        public string Peran { get; set; }
        public string LokasiPenangkapan { get; set; }
        public DateTime TanggalKejadian { get; set; }
        public DateTime TanggalPenangkapan { get; set; }
        public bool BolehDikunjungi { get; set; }
        public string PeriodeKunjungan { get; set; }
        public string WaktuKunjungan { get; set; }
        public string BeratBadan { get; set; }
        public string TinggiBadan { get; set; }
        public DateTime? DateCheckedOut { get; set; }
        public bool IsCheckedout { get; set; }
        public string NoSuratPenangkapan { get; set; }
        public string NoSuratPenahanan { get; set; }
        public DateTime TanggalPenahanan { get; set; }
    }
}