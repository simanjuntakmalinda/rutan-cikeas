﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class Params 
    {
        public int Id { get; set; }
        public string ParamName { get; set; }
        public string ParamValue { get; set; }
    }
}