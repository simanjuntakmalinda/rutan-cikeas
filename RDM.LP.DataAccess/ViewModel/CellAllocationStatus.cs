﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.ViewModel
{
    public class CellAllocationStatus : BaseItem
    {
        public string Status { get; set; }
    }
}
