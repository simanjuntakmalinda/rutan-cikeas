﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.ViewModel
{
    public class SaksiLp : BaseItem
    {
        public string SaksiSumber;

        public int LpId { get; set; }

        public bool SaksiAhli { get; set; }

        public int SaksiId { get; set; }

        public string JenisIdentitasSaksi { get; set; }

        public string NomorIdentitasSaksi { get; set; }

        public string NamaSaksi { get; set; }

        public string JenisKelaminSaksi { get; set; }

        public string AlamatSaksi { get; set; }

        public string TempatLahirSaksi { get; set; }

        public DateTime? TanggalLahirSaksi { get; set; }

        public int? SukuIdSaksi { get; set; }

        public int? PekerjaanIdSaksi { get; set; }

        public int? PendidikanIdSaksi { get; set; }

        public int? NegaraIdSaksi { get; set; }

        public int? AgamaIdSaksi { get; set; }

        public string NomorTeleponSaksi { get; set; }

        public int? UmurSaksi { get; set; }

        public string EmailSaksi { get; set; }

        public string Kewarganegaraan { get; set; }

        public string PekerjaanNama { get; set; }

        public string AgamaNama { get; set; }

        public string PendidikanNama { get; set; }

        public string NegaraNama { get; set; }

        public int? PendudukId { get; set; }

        public string IbuKandung { get; set; }

        public string BapakKandung { get; set; }

        public string NamaAlias { get; set; }

        public bool? StatusKawin { get; set; }

        public Byte? TipeSaksi { get; set; }

        public int? PropinsiIdSaksi { get; set; }

        public int? KotaIdSaksi { get; set; }

        public int? KecamatanIdSaksi { get; set; }

        public int? KelurahanIdSaksi { get; set; }

        public int? SaksiIdSumber { get; set; }

        public int CaseId { get; set; }

        public string DasarHukum { get; set; }

        public string Keahlian { get; set; }

        public string NamaSaksiSumber { get; set; }

        public string NamaPelaporSumber { get; set; }

        public string NamaKorbanSumber { get; set; }

        public string NoBerkasSumberSaksi { get; set; }
    }
}
