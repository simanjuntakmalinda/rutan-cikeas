﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class Cell : BaseItem
    {

        public string CellNumber { get; set; }

        public string CellCode { get; set; }

        public int? BuildingId { get; set; }

        public string BuildingName { get; set; }

        public string CellFloor { get; set; }

        public string CellDescription { get; set; }

        public string CellStatus { get; set; }

        public string CellType { get; set; }

        public string CellTypeId { get; set; }

        public int InUseCounter { get; set; }

    }

    public class GetCell : Cell
    {
        public string Roommate { get; set; }
        public int IdCellAllocation { get; set; }
        public string RegRoommate { get; set; }
        public string InmatesRegCode { get; set; }
    }
}