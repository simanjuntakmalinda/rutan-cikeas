﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class JenisKejahatan : BaseItem
    {
        public int GolonganKejahatanId { get; set; }
        public string Nama { get; set; }
        public string Pasal { get; set; }
        public int? UndangUndangId { get; set; }
    }
}