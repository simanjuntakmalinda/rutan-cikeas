﻿using System;
using System.Collections.Generic;

namespace RDM.LP.DataAccess.ViewModel
{
    public class FirstSynch : BaseItem
    {
        public IEnumerable<Agama> Agama { get; set; }
        public IEnumerable<BarangBukti> BarangBukti { get; set; }
        public IEnumerable<BarangBuktiKelompok> BarangBuktiKelompok { get; set; }
        public IEnumerable<FormItem> FormItem { get; set; }
        public IEnumerable<GolonganKejahatan> GolonganKejahatan { get; set; }
        public IEnumerable<JenisKejahatan> JenisKejahatan { get; set; }
        public IEnumerable<Kecamatan> Kecamatan { get; set; }
        public IEnumerable<Kelurahan> Kelurahan { get; set; }
        public IEnumerable<Kota> Kota { get; set; }
        public IEnumerable<MasterBarangBukti> MasterBarangBukti { get; set; }
        public IEnumerable<Negara> Negara { get; set; }
        public IEnumerable<PangkatPolisi> PangkatPolisi { get; set; }
        public IEnumerable<Pekerjaan> Pekerjaan { get; set; }
        public IEnumerable<Pendidikan> Pendidikan { get; set; }
        public IEnumerable<Propinsi> Propinsi { get; set; }
        public IEnumerable<TipePersonel> TipePersonel { get; set; }
    }
}