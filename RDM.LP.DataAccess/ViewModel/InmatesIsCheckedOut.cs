﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.ViewModel
{
    public class InmatesIsCheckedOut : BaseItem
    {
        public int InmatesId { get; set; }

        public string RegID { get; set; }

        public string InmatesName { get; set; }

        public DateTime? DateReg { get; set; }

        public DateTime? DateCheckedOut { get; set; }

        public string Notes { get; set; }

        public bool IsCheckedOut { get; set; }
    }

    public class GetInmatesCheckedOut : Inmates
    {
        public int InmatesId { get; set; }

        public string RegID { get; set; }

        public string InmatesName { get; set; }

        public string CheckedOutNote { get; set; }

        public bool IsCheckedOut { get; set; }
    }
}
