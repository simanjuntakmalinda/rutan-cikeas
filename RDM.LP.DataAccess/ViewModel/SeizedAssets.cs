﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.ViewModel
{
    public class SeizedAssets : BaseItem
    {
        public int CaseId { get; set; }

        public string CaseName { get; set; }

        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public string Detail { get; set; }

        public string Quantity { get; set; }

        public string StorageType { get; set; }

        public string StorageLocation { get; set; }

        public DateTime? ConfiscatedDate { get; set; }

        public string Note { get; set; }

        public string Nama { get; set; }

        public int SeizedAssetsId { get; set; }

    }
}
