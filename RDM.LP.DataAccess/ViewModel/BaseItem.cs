﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class BaseItem
    {
        public int Id { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }

        public string CreatedLocation { get; set; }

        public string UpdatedLocation { get; set; }

        public string DeletedBy { get; set; }

        public DateTime? DeletedDate { get; set; }
    }
}