﻿using System;

namespace RDM.LP.DataAccess.ViewModel
{
    public class VisitorVIP : BaseItem
    {
        public string FullName { get; set; }
        public string IDType { get; set; }
        public string IDNo { get; set; }
        public string Address { get; set; }
        public string Purpose { get; set; }
        public string Notes { get; set; }

        public DateTime? StartVisit { get; set; }

        public DateTime? EndVisit { get; set; }

        public bool IsCheckedOut { get; set; }
    }
}