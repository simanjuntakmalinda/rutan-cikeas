﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.ViewModel
{
    public class LokasiPolisi : BaseItem
    {
        public string Nama { get; set; }
        public int RootId { get; set; }
        public int TipeLokasiPolisi { get; set; }
        public int Type { get; set; }
    }
}
