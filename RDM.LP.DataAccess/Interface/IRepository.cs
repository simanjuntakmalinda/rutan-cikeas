﻿using System.Collections.Generic;


namespace RDM.LP.DataAccess.Interface
{
    public interface IRepository : System.IDisposable
    {
        IEnumerable<T> Get<T>(string sqlQuery, object data = null);
        T GetSingle<T>(string sqlQuery, object data = null);
        int InsertIntoDB<T>(string sqlQuery, object data = null);
        void UpdateToDB<T>(string sqlQuery, object data = null);
        void DeleteFromDB<T>(string sqlQuery, T data);
        void ExecuteQuery(string sqlQuery, object data = null);
    }
}