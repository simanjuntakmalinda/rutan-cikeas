﻿using RDM.LP.DataAccess.ViewModel;
using System;

namespace RDM.LP.DataAccess.Interface
{
    public interface IUserIdentityHelper : IDisposable
    {
        bool Login(string username, string password, bool isLoggedIn, out AspnetUsers DataUser);
        new void Dispose();
    }
}