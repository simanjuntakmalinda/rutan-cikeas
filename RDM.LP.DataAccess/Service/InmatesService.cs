﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class InmatesService : IDisposable
    {
        private readonly IRepository dataaccess;

        public InmatesService()
        {
            dataaccess = new SqlBaseRepository();
        }

        public Inmates GetDetailByIdentity(string idtype, string idno)
        {
            string sqlStr;
            sqlStr = @"SELECT A.* FROM Inmates A WHERE A.IDType= '" + idtype + "' AND IDNo='" + idno + "' ORDER BY CreatedDate DESC";
            return dataaccess.Get<Inmates>(sqlStr).FirstOrDefault();
        }

        public Inmates GetDetailByRegID(string RegID)
        {
            string sqlStr;
            sqlStr = @"SELECT A.* FROM Inmates A WHERE A.RegID= '" + RegID + "' ORDER BY CreatedDate DESC";
            return dataaccess.Get<Inmates>(sqlStr).FirstOrDefault();
        }

        public Inmates GetDetailById(int Id)
        {
            string sqlStr;
            sqlStr = @"SELECT A.* FROM Inmates A WHERE A.Id= '" + Id + "' ORDER BY CreatedDate DESC";
            return dataaccess.Get<Inmates>(sqlStr).FirstOrDefault();
        }
        public IEnumerable<Inmates> GetDetailRpt()
        {
            string sqlStr;
            sqlStr = @"SELECT B.RegID, B.FullName, B.DateReg, B.Gender + '/' + CAST (CASE WHEN dateadd(year, datediff (year, B.DateOfBirth, getdate()), B.DateOfBirth) > getdate()
                        THEN datediff(year, B.DateOfBirth, getdate()) - 1
                        ELSE datediff(year, B.DateOfBirth, getdate()) END AS VARCHAR(3)) as Gender , C.CellCode,
                        (SELECT Stuff(
                        (SELECT N', ' + D.Name  FROM Cases A INNER JOIN InmatesCase B ON A.Id = B.CaseId INNER JOIN Inmates C ON B.RegID = C.RegID
                        WHERE B.CaseId = D.Id FOR XML PATH(''),TYPE)
                        .value('text()[1]','nvarchar(max)'),1,2,N'')
                        ) Cases
                        FROM Inmates B 
                        LEFT JOIN InmatesCase A ON B.RegID = A.RegID
                        LEFT JOIN CellAllocation C ON B.RegID = C.InmatesRegCode 
                        LEFT JOIN Cases D ON A.CaseId = D.Id WHERE C.IsCheckedOut = 0 ORDER BY FullName";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public string GetRegID()
        {
            string lastno;
            string sqlStr = String.Format("SELECT ISNULL(MAX(RIGHT(RegID,4)),'0000') from Inmates WHERE LEN(RegID) = 13");
            var data = dataaccess.Get<int?>(sqlStr);
            lastno = string.Format("R{0}{1:00}{2:00}{3:0000}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, data.ElementAtOrDefault(0) + 1);
            return lastno;
        }

        public IEnumerable<Inmates> GetByRegId(string RegNo)
        {
            string sqlStr;
            sqlStr = @"SELECT * FROM Inmates A WHERE A.RegID ='" + RegNo + "' and A.RegID not in (SELECT InmatesRegCode FROM CellAllocation WHERE IsCheckedOut = 0)";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetByRegisterId(string RegNo)
        {
            string sqlStr;
            sqlStr = @"SELECT * FROM Inmates A WHERE A.RegID ='" + RegNo + "'";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetByInmatesRegId(string RegNo)
        {
            string sqlStr;
            sqlStr = @"SELECT * FROM Inmates A WHERE A.RegID ='" + RegNo + "'";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetByName()
        {
            string sqlStr;
            sqlStr = @"SELECT A.* FROM Inmates A WHERE A.RegID not in (SELECT InmatesRegCode FROM CellAllocation WHERE IsCheckedOut = 0)";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        //tidak termasuk tahanan yang sudah keluar (dari DAFTAR TAHANAN / InmatesIsCheckedOut)
        public IEnumerable<Inmates> GetByNameNotOut()
        {
            string sqlStr;
            sqlStr = @"SELECT A.*
                            FROM Inmates A
                            WHERE A.RegID in (SELECT InmatesRegCode FROM CellAllocation A JOIN InmatesIsCheckedOut B ON A.InmatesRegCode = B.RegID WHERE A.IsCheckedOut = 1 AND B.IsCheckedOut = 0)
	                        AND A.RegID not in (SELECT InmatesRegCode FROM CellAllocation WHERE IsCheckedOut = 0)
                        UNION all
                        SELECT A.*
                            FROM Inmates A
                            WHERE A.RegID not in (SELECT InmatesRegCode FROM CellAllocation)";

            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetByNameAndJadwal()
        {
            string sqlStr;
            sqlStr = @"SELECT A.* 
                        FROM Inmates A 
                        WHERE A.RegID in (SELECT InmatesRegCode FROM CellAllocation WHERE IsCheckedOut = 0) 
                        AND A.RegID not in (SELECT InmatesRegID FROM JadwalKunjungan)";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetByNameAndJadwalEdit(string RegID)
        {
            string sqlStr;
            sqlStr = @"SELECT A.* 
                        FROM Inmates A 
                        WHERE A.RegID in (SELECT InmatesRegCode FROM CellAllocation)
                        AND A.RegID not in (SELECT InmatesRegID FROM JadwalKunjungan WHERE InmatesRegID != '" + RegID + "')";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetByKTP(string no)
        {
            string sqlStr;
            sqlStr = @"SELECT A.* FROM Inmates A WHERE A.IDNo like '%" + no + "%'and A.RegID not in (SELECT InmatesRegCode FROM CellAllocation WHERE IsCheckedOut = 0)";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetByRegIdVisit(string RegNo)
        {
            string sqlStr;
            //sqlStr = @"SELECT * FROM Inmates A WHERE A.RegID like '%" + RegNo + "%'and A.RegID in (SELECT InmatesRegCode FROM CellAllocation WHERE IsCheckedOut = 0)";
            sqlStr = @"SELECT A.*, B.CellAllocationStatusId, C.BolehDikunjungi, C.PeriodeKunjungan, C.WaktuKunjungan
                        FROM Inmates A LEFT JOIN CellAllocation B ON A.RegID = B.InmatesRegCode
                        LEFT JOIN JadwalKunjungan C ON A.RegID = C.InmatesRegID
                        WHERE A.RegID like '%" + RegNo + "%' and A.RegID in (SELECT InmatesRegCode FROM CellAllocation WHERE IsCheckedOut = 0)";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetInmatesByReg(string RegNo)
        {
            string sqlStr;
            //sqlStr = @"SELECT * FROM Inmates A WHERE A.RegID like '%" + RegNo + "%'and A.RegID in (SELECT InmatesRegCode FROM CellAllocation WHERE IsCheckedOut = 0)";
            sqlStr = @"SELECT b.FullName, b.RegID, b.IDType, b.IDNo, b.Gender, d.CellNumber, isnull(b.NosuratPenahanan, '') as NoSuratPenahanan, isnull(b.NoSuratPenangkapan, '') as NoSuratPenangkapan, isnull(b.TanggalPenahanan, GETDATE()) as TanggalPenahanan, isnull(b.TanggalPenangkapan, GETDATE()) as TanggalPenangkapan
                        FROM Inmates b
                        LEFT JOIN CellAllocation c ON b.RegID = c.InmatesRegCode
                        LEFT JOIN Cell d ON c.CellCode = d.CellCode
                        WHERE b.RegID like '%" + RegNo + "%' and c.IsCheckedOut=0 and RegID not in (select RegId from StatusTahanan)";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetInmatesByName(string Name)
        {
            string sqlStr;
            //sqlStr = @"SELECT * FROM Inmates A WHERE A.RegID like '%" + RegNo + "%'and A.RegID in (SELECT InmatesRegCode FROM CellAllocation WHERE IsCheckedOut = 0)";
            sqlStr = @"SELECT b.FullName, b.RegID, b.IDType, b.IDNo, b.Gender, d.CellNumber, isnull(b.NosuratPenahanan, '') as NoSuratPenahanan, isnull(b.NoSuratPenangkapan, '') as NoSuratPenangkapan, isnull(b.TanggalPenahanan, GETDATE()) as TanggalPenahanan, isnull(b.TanggalPenangkapan, GETDATE()) as TanggalPenangkapan
                        FROM Inmates b
                        LEFT JOIN CellAllocation c ON b.RegID = c.InmatesRegCode
                        LEFT JOIN Cell d ON c.CellCode = d.CellCode
                        WHERE b.FullName like '%" + Name+ "%' and c.IsCheckedOut=0 and RegID not in (select RegId from StatusTahanan)";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetInmatesByKTP(string noKTP)
        {
            string sqlStr;
            //sqlStr = @"SELECT * FROM Inmates A WHERE A.RegID like '%" + RegNo + "%'and A.RegID in (SELECT InmatesRegCode FROM CellAllocation WHERE IsCheckedOut = 0)";
            sqlStr = @"SELECT b.FullName, b.RegID, b.IDType, b.IDNo, b.Gender, d.CellNumber, isnull(b.NosuratPenahanan, '') as NoSuratPenahanan, isnull(b.NoSuratPenangkapan, '') as NoSuratPenangkapan, isnull(b.TanggalPenahanan, GETDATE()) as TanggalPenahanan, isnull(b.TanggalPenangkapan, GETDATE()) as TanggalPenangkapan
                        FROM Inmates b
                        LEFT JOIN CellAllocation c ON b.RegID = c.InmatesRegCode
                        LEFT JOIN Cell d ON c.CellCode = d.CellCode
                        WHERE b.IDNo like '%" + noKTP + "%' and c.IsCheckedOut=0 and RegID not in (select RegId from StatusTahanan)";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetByRegIdJadwal(string RegNo)
        {
            string sqlStr;
            sqlStr = @"SELECT A.*, C.CellCode
                        FROM Inmates A
                        LEFT JOIN InmatesIsCheckedOut B ON A.RegID = B.RegID
                        LEFT JOIN CellAllocation C ON A.RegID = C.InmatesRegCode
                        WHERE A.RegID in (SELECT RegID FROM InmatesIsCheckedOut WHERE IsCheckedOut = 0) 
                        AND A.RegID not in (SELECT InmatesRegID FROM JadwalKunjungan)
                        AND B.IsCheckedOut = 0
                        AND A.RegID like '%" + RegNo + "%' AND C.IsCheckedOut = 0";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetByNameVisit(string Name)
        {
            string sqlStr;
            //sqlStr = @"SELECT A.* FROM Inmates A WHERE A.FullName like '%" + Name + "%'and A.RegID in (SELECT InmatesRegCode FROM CellAllocation WHERE IsCheckedOut = 0)";
            sqlStr = @"SELECT A.*, C.CellAllocationStatusId
                        FROM Inmates A LEFT JOIN CellAllocation C ON A.RegID = C.InmatesRegCode
                        WHERE A.FullName like '%" + Name + "%' and C.IsCheckedOut =0";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetByNameJadwal(string Name)
        {
            string sqlStr;
            sqlStr = @"SELECT A.*, C.CellCode
                        FROM Inmates A
                        LEFT JOIN InmatesIsCheckedOut B ON A.RegID = B.RegID
						LEFT JOIN CellAllocation C ON A.RegID = C.InmatesRegCode
                        WHERE A.RegID in (SELECT RegID FROM InmatesIsCheckedOut WHERE IsCheckedOut = 0) 
                        AND A.RegID not in (SELECT InmatesRegID FROM JadwalKunjungan)
                        AND B.IsCheckedOut = 0
                        AND A.FullName like '%" + Name + "%' AND C.IsCheckedOut = 0";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetByKTPVisit(string no)
        {
            string sqlStr;
            //sqlStr = @"SELECT A.* FROM Inmates A WHERE A.IDNo like '%" + no + "%'and A.RegID in (SELECT InmatesRegCode FROM CellAllocation WHERE IsCheckedOut = 0)";
            sqlStr = @"SELECT A.*, B.CellAllocationStatusId, C.BolehDikunjungi, C.PeriodeKunjungan, C.WaktuKunjungan
                        FROM Inmates A LEFT JOIN CellAllocation B ON A.RegID = B.InmatesRegCode
                        LEFT JOIN JadwalKunjungan C ON A.RegID = C.InmatesRegID
                        WHERE A.IDNo like '%" + no + "%' and A.RegID in (SELECT InmatesRegCode FROM CellAllocation WHERE IsCheckedOut = 0)";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetByKTPJadwal(string NoKtp)
        {
            string sqlStr;
            sqlStr = @"SELECT A.*, C.CellCode
                        FROM Inmates A
                        LEFT JOIN InmatesIsCheckedOut B ON A.RegID = B.RegID
                        LEFT JOIN CellAllocation C ON A.RegID = C.InmatesRegCode
                        WHERE A.RegID in (SELECT RegID FROM InmatesIsCheckedOut WHERE IsCheckedOut = 0) 
                        AND A.RegID not in (SELECT InmatesRegID FROM JadwalKunjungan)
                        AND B.IsCheckedOut = 0
                        AND A.IDNo like '%" + NoKtp + "%' AND C.IsCheckedOut = 0";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public Inmates GetDetailById(string id)
        {
            string sqlStr;
            sqlStr = @"SELECT A.* FROM Inmates A  WHERE A.Id= " + id;
            return dataaccess.Get<Inmates>(sqlStr).FirstOrDefault();
        }

        public IEnumerable<Inmates> GetBySearch(string keysearch)
        {
            string sqlStr;
            sqlStr = @"SELECT A.* FROM Inmates A WHERE 
                       A.FullName like '%" + keysearch + "%' OR A.TelpNo like '%" + keysearch + "%' OR A.IDNo like '%" + keysearch + "%'";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Photos> GetImage(string RegId)
        {
            string sqlStr;
            sqlStr = @"SELECT * FROM Photos WHERE RegID ='" + RegId + "'";
            return dataaccess.Get<Photos>(sqlStr);
        }

        public IEnumerable<FingerPrint> GetFingerPrint(string RegId)
        {
            string sqlStr;
            sqlStr = @"SELECT A.RegID, F.ImagePrint, F.ByteImagePrint, F.FingerId FROM Inmates A join FingerPrint F ON A.RegID = F.RegID WHERE A.RegID = '" + RegId + "'";
            return dataaccess.Get<FingerPrint>(sqlStr);
        }

        public IEnumerable<Inmates> Get()
        {
            string sqlStr;
            sqlStr = @"SELECT * FROM Inmates ORDER BY CreatedDate DESC";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetAllName()
        {
            string sqlStr;
            sqlStr = @"SELECT A.RegID, A.RegID +' - '+ A.FullName FullName FROM Inmates A ORDER BY CreatedDate DESC";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<Inmates> GetAllNameTahanan()
        {
            string sqlStr;
            sqlStr = @"SELECT A.RegID, A.RegID +' - '+ A.FullName FullName FROM Inmates A LEFT JOIN CellAllocation B ON A.RegID = B.InmatesRegCode WHERE B.IsCheckedOut = 0 ORDER BY B.CreatedDate DESC";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public IEnumerable<DasboardChart> GetCountLast30days()
        {
            string sqlStr = @"SELECT DAY(IndividualDate) Category,(SELECT COUNT(0) FROM inmates WHERE CONVERT(varchar,CreatedDate,101) = CONVERT(varchar,IndividualDate,101)) Value FROM DateRange('d', (select dateadd(day, -29, getdate())), getdate())";
            return dataaccess.Get<DasboardChart>(sqlStr);
        }

        public int GetCountToday()
        {
            string sqlStr = @"SELECT COUNT(0) FROM Inmates WHERE CONVERT(VARCHAR(10),CreatedDate,101) = CONVERT(VARCHAR(10),getdate(),101)";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int GetCount()
        {
            string sqlStr = @"SELECT Count(0) FROM Inmates";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public Inmates GetDetailByRegId(string RegNo)
        {
            string sqlStr;
            sqlStr = @"SELECT * FROM Inmates A WHERE A.RegID ='" + RegNo + "'";
            return dataaccess.Get<Inmates>(sqlStr).FirstOrDefault();
        }

        public Inmates GetDetailByRegIdKunjungan(string RegNo)
        {
            string sqlStr;
            sqlStr = @"SELECT A.*, B.CellCode FROM Inmates A LEFT JOIN CellAllocation B ON A.RegID = B.InmatesRegCode WHERE A.RegID ='" + RegNo + "'";
            return dataaccess.Get<Inmates>(sqlStr).FirstOrDefault();
        }

        public IEnumerable<Inmates> GetHistory(string RegID)
        {
            string sqlStr;
            sqlStr = @"SELECT B.Id, B.RegID, B.FullName, B.IDNo + ' (' + B.IDType + ') ' AS Identitas, B.DateReg, C.DateCheckedOut
                        FROM Inmates A 
                        LEFT OUTER JOIN Inmates B ON A.IDNo = B.IDNo 
                        LEFT JOIN InmatesIsCheckedOut C ON B.RegID = C.RegID 
                        WHERE A.IDType = B.IDType and A.RegID = '" + RegID + "' ORDER BY B.Id DESC";
            return dataaccess.Get<Inmates>(sqlStr);
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<Inmates>("DELETE FROM Inmates", null);
        }

        public void DeleteById(string Id)
        {
            var data = GetDetailById(Id);

            dataaccess.DeleteFromDB<Inmates>(string.Format("DELETE FROM InmatesPhoto WHERE RegID ='{0}'", data.RegID), null);
            dataaccess.DeleteFromDB<Inmates>(string.Format("DELETE FROM FingerPrint WHERE RegID ='{0}'", data.RegID), null);
            dataaccess.DeleteFromDB<Inmates>(string.Format("DELETE FROM InmatesCase WHERE RegID ='{0}'", data.RegID), null);
            dataaccess.DeleteFromDB<Inmates>(string.Format("DELETE FROM Inmates WHERE Id ='{0}'", Id), null);
        }

        public int Post(Inmates data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id" && item.Name != "BolehDikunjungi" && item.Name != "NetworkId" && item.Name != "IsCheckedout")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO Inmates({0}) OUTPUT INSERTED.ID VALUES({1});", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<Inmates>(strSql);
        }



        public void Update(Inmates data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id" && item.Name != "BolehDikunjungi" && item.Name != "NetworkId" && item.Name != "IsCheckedout")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    fieldsValue = String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                    columnName = columnName + String.Format("{0}={1}", item.Name, fieldsValue);

                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            strSql = String.Format("UPDATE Inmates SET {0} WHERE Id={1}", columnName, data.Id);
            dataaccess.UpdateToDB<Inmates>(strSql);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        #endregion
    }
}
