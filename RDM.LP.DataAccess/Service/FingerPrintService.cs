﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class FingerPrintService : IDisposable
    {
        private readonly IRepository dataaccess;

        public FingerPrintService()
        {
            dataaccess = new SqlBaseRepository();
        }

        public IEnumerable<FingerPrint> GetNotCheckeoutVisitorFingerPrint()
        {
            string sqlStr;
            sqlStr = @"SELECT C.*,B.FullName,B.Id VisitorId FROM Visit A LEFT OUTER JOIN Visitor B ON A.VisitorId = B.Id LEFT OUTER JOIN FingerPrint C ON B.RegID = C.RegID WHERE A.IsCheckedOut = 0";
            return dataaccess.Get<FingerPrint>(sqlStr);
        }

        public IEnumerable<FingerPrint> GetNotCheckeoutInmatesFingerPrint()
        {
            string sqlStr;
            sqlStr = @"SELECT C.*,B.FullName,B.Id TahananId 
                        FROM InmatesIsCheckedOut A 
                        LEFT OUTER JOIN Inmates B ON A.InmatesId = B.Id 
                        LEFT OUTER JOIN FingerPrint C ON B.RegID = C.RegID 
                        WHERE A.IsCheckedOut = 0";
            return dataaccess.Get<FingerPrint>(sqlStr);
        }

        public IEnumerable<FingerPrint> Get()
        {
            string sqlStr;
            sqlStr = @"SELECT * 
                        FROM FingerPrint 
                        ORDER BY CreatedDate ASC";
            return dataaccess.Get<FingerPrint>(sqlStr);
        }

        public IEnumerable<FingerPrint> GetInmates()
        {
            string sqlStr;
            sqlStr = @"SELECT a.*, b.Id InmateId, b.FullName 
                        FROM FingerPrint a 
                        LEFT JOIN Inmates b on a.RegID = b.RegID 
                        WHERE a.IsVisitor = 0 
                        ORDER BY a.CreatedDate ASC";
            return dataaccess.Get<FingerPrint>(sqlStr);
        }

        public IEnumerable<FingerPrint> GetVisitor()
        {
            string sqlStr;
            sqlStr = @"SELECT a.*, b.Id VisitorId, b.FullName 
                        FROM FingerPrint a 
                        LEFT JOIN Visitor b on a.RegID = b.RegID 
                        WHERE a.IsVisitor = 1 
                        ORDER BY a.CreatedDate DESC";
            return dataaccess.Get<FingerPrint>(sqlStr);
        }

        public IEnumerable<FingerPrint> GetInmatesByDesc()
        {
            string sqlStr;
            sqlStr = @"SELECT * 
                        FROM FingerPrint 
                        WHERE IsVisitor = 0 
                        ORDER BY CreatedDate Desc";
            return dataaccess.Get<FingerPrint>(sqlStr);
        }

        public IEnumerable<FingerPrint> GetVisitorByDesc()
        {
            string sqlStr;
            sqlStr = @"SELECT * 
                        FROM FingerPrint 
                        WHERE IsVisitor = 1 
                        ORDER BY CreatedDate Desc";
            return dataaccess.Get<FingerPrint>(sqlStr);
        }

        public FingerPrint GetById(int Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT * FROM FingerPrint WHERE Id = {0}", Id);
            return dataaccess.Get<FingerPrint>(sqlStr).FirstOrDefault();
        }

        public FingerPrint GetByRegID(string regID)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT * FROM FingerPrint WHERE RegID ='{0}'", regID);
            return dataaccess.Get<FingerPrint>(sqlStr).FirstOrDefault();
        }

        public FingerPrint GetByImagePrint(string imagePrint)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT * FROM FingerPrint WHERE ImagePrint like '{0}'", imagePrint);
            return dataaccess.Get<FingerPrint>(sqlStr).FirstOrDefault();
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<Cell>("DELETE FROM FingerPrint", null);
        }

        public void DeleteById(string id)
        {
            dataaccess.DeleteFromDB<FingerPrint>(string.Format("DELETE FROM FingerPrint WHERE Id ='{0}'", id), null);
        }

        public int GetCount()
        {
            string sqlStr = @"SELECT Count(0) FROM FingerPrint";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public IEnumerable<FingerPrint> GetAllByRegID(string regID)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT * FROM FingerPrint WHERE RegID ='{0}'", regID);
            return dataaccess.Get<FingerPrint>(sqlStr);
        }

        public FingerPrint GetByRegId(string regID)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT * FROM FingerPrint WHERE RegID like '{0}' ORDER BY Id DESC", regID);
            return dataaccess.Get<FingerPrint>(sqlStr).FirstOrDefault();
        }

        public void DeleteByRegId(string RegID)
        {
            dataaccess.DeleteFromDB<Photos>(string.Format("DELETE FROM FingerPrint WHERE RegID ='{0}'", RegID), null);
        }

        public int Post(FingerPrint data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id" && item.Name != "InmateId" && item.Name != "VisitorId")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO FingerPrint({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<FingerPrint>(strSql);
        }

        public void Update(FingerPrint data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id" && item.Name != "InmateId" && item.Name != "VisitorId")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    fieldsValue = String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                    columnName = columnName + String.Format("{0}={1}", item.Name, fieldsValue);

                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            strSql = String.Format("UPDATE FingerPrint SET {0} WHERE Id={1}", columnName, data.Id);
            dataaccess.UpdateToDB<FingerPrint>(strSql);
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
