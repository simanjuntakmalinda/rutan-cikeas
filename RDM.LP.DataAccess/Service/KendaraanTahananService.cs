﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class KendaraanTahananService : IDisposable
    {
        private readonly IRepository dataaccess;

        public KendaraanTahananService()
        {
            dataaccess = new SqlBaseRepository();
        }

        public IEnumerable<KendaraanTahanan> Get()
        {
            string sqlStr;
            sqlStr = @"SELECT a.*, b.Nama NamaKategoriKendaraan FROM KendaraanTahanan a join Kendaraan b on a.KategoriKendaraan = b.Id ORDER BY CreatedDate DESC";
            return dataaccess.Get<KendaraanTahanan>(sqlStr);
        }

        public void CheckOutKendaraan(int id, string username)
        {
            string strSql = "UPDATE KendaraanTahanan SET IsCheckedOut = 1, WaktuKeluar= getdate(), UpdatedBy='" + username + "',UpdatedDate=getdate() WHERE Id =" + id;
            dataaccess.UpdateToDB<KendaraanTahanan>(strSql);
        }

        public IEnumerable<KendaraanTahanan> GetAvailableKendaraanTahanan()
        {
            string sqlStr;
            sqlStr = @"SELECT a.*, b.Nama as NamaKategoriKendaraan FROM KendaraanTahanan a join Kendaraan b on a.KategoriKendaraan = b.Id WHERE a.IsCheckedOut = 0 ORDER BY CreatedDate DESC";
            return dataaccess.Get<KendaraanTahanan>(sqlStr);
        }

        public IEnumerable<KendaraanTahanan> GetCheckedOutKendaraanTahanan()
        {
            string sqlStr;
            sqlStr = @"SELECT a.*, b.Nama as NamaKategoriKendaraan FROM KendaraanTahanan a join Kendaraan b on a.KategoriKendaraan = b.Id WHERE a.IsCheckedOut = 1 ORDER BY CreatedDate DESC";
            return dataaccess.Get<KendaraanTahanan>(sqlStr);
        }

        public KendaraanTahanan GetById(int Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT a.*, b.Nama as NamaKategoriKendaraan FROM KendaraanTahanan a left outer join Kendaraan b on a.KategoriKendaraan = b.Id WHERE a.Id = {0}", Id);
            return dataaccess.Get<KendaraanTahanan>(sqlStr).FirstOrDefault();
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<KendaraanTahanan>("DELETE FROM KendaraanTahanan", null);
        }

        public void DeleteById(int id)
        {
            dataaccess.DeleteFromDB<KendaraanTahanan>(string.Format("DELETE FROM KendaraanTahanan WHERE Id ='{0}'", id), null);
        }

        public int GetCount()
        {
            string sqlStr = @"SELECT Count(0) FROM KendaraanTahanan";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int Post(KendaraanTahanan data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO KendaraanTahanan({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<KendaraanTahanan>(strSql);
        }

        public void Update(KendaraanTahanan data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    fieldsValue = String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                    columnName = columnName + String.Format("{0}={1}", item.Name, fieldsValue);

                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            strSql = String.Format("UPDATE KendaraanTahanan SET {0} WHERE Id={1}", columnName, data.Id);
            dataaccess.UpdateToDB<KendaraanTahanan>(strSql);
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
