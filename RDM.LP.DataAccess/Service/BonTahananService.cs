﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class BonTahananService : IDisposable
    {
        private readonly IRepository dataaccess;

        public BonTahananService()
        {
            dataaccess = new SqlBaseRepository();
        }

        public IEnumerable<BonTahanan> Get()
        {
            string sqlStr;
            sqlStr = @"SELECT C.Nama AS NamaKeperluan, A.* FROM BonTahanan A JOIN KeperluanBonTahanan C ON A.Keperluan = C.Id ORDER BY A.CreatedDate DESC";
            return dataaccess.Get<BonTahanan>(sqlStr);
        }

        public IEnumerable<BonTahanan> GetOut()
        {
            string sqlStr;
            //sqlStr = @"SELECT B.FullName AS NamaTahanan, C.Nama AS NamaKeperluan, A.*, D.RegID FROM BonTahanan A JOIN InmateBonTahanan D ON A.Id = D.BonTahananId JOIN Inmates B ON D.RegID = B.RegID JOIN KeperluanBonTahanan C ON A.Keperluan = C.Id WHERE A.StatusTahanan = 'Diminta' ORDER BY A.CreatedDate DESC";
            sqlStr = @"SELECT C.Nama AS NamaKeperluan, A.* FROM BonTahanan A JOIN KeperluanBonTahanan C ON A.Keperluan = C.Id WHERE A.StatusTahanan = 'Diminta' ORDER BY A.CreatedDate DESC";
            return dataaccess.Get<BonTahanan>(sqlStr);
        }

        public IEnumerable<BonTahanan> GetIn()
        {
            string sqlStr;
            sqlStr = @"SELECT C.Nama AS NamaKeperluan, A.* FROM BonTahanan A JOIN KeperluanBonTahanan C ON A.Keperluan = C.Id WHERE A.StatusTahanan = 'Keluar' ORDER BY A.CreatedDate DESC";
            return dataaccess.Get<BonTahanan>(sqlStr);
        }

        public BonTahanan GetById(int Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT C.Nama AS NamaKeperluan, A.*, D.Nama AS NamaPangkatPemohon, E.Nama AS NamaPangkatPenanggungJawab, F.Nama AS NamaPangkatPenyetuju  FROM BonTahanan A LEFT JOIN KeperluanBonTahanan C ON A.Keperluan = C.Id LEFT JOIN PangkatPolisi D ON A.PangkatPemohon = D.Id LEFT JOIN PangkatPolisi E ON A.PangkatPenanggungJawab = E.Id LEFT JOIN PangkatPolisi F ON A.PangkatPenyetuju = F.Id WHERE A.Id ='" + Id + "'");
            return dataaccess.Get<BonTahanan>(sqlStr).FirstOrDefault();
        }

        public BonTahanan GetIdByNo(string NoBon)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT * FROM BonTahanan WHERE NoBonTahanan = '{0}'", NoBon);
            return dataaccess.Get<BonTahanan>(sqlStr).FirstOrDefault();
        }

        public BonTahanan GetByNo(string NoBon)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT B.FullName AS NamaTahanan, I.BuildingName, H.CellNumber AS CellNo, H.CellCode, C.Nama AS NamaKeperluan, A.*," +
                " D.Nama AS NamaPangkatPemohon, E.Nama AS NamaPangkatPenanggungJawab, F.Nama AS NamaPangkatPenyetuju " +
                " FROM BonTahanan A" +
                " LEFT JOIN InmateBonTahanan X ON A.Id = X.BonTahananId" +
                " LEFT JOIN Inmates B ON X.RegID = B.RegID" +
                " LEFT JOIN KeperluanBonTahanan C ON A.Keperluan = C.Id" +
                " LEFT JOIN PangkatPolisi D ON A.PangkatPemohon = D.Id" +
                " LEFT JOIN PangkatPolisi E ON A.PangkatPenanggungJawab = E.Id" +
                " LEFT JOIN PangkatPolisi F ON A.PangkatPenyetuju = F.Id" +
                " LEFT JOIN CellAllocation G ON B.RegID = G.InmatesRegCode" +
                " LEFT JOIN Cell H ON G.CellCodeId = H.Id" +
                " LEFT JOIN Building I ON H.BuildingId = I.Id" +
                " WHERE A.NoBonTahanan = {0}", NoBon);
            return dataaccess.Get<BonTahanan>(sqlStr).FirstOrDefault();
        }

        public BonTahanan GetByInamateRegID(string RegID)
        {
            string sqlStr;
            sqlStr = @"SELECT B.FullName AS NamaTahanan, A.* FROM BonTahanan A JOIN Inmates B ON A.RegID = B.RegID WHERE RegID = '" + RegID + "'";
            return dataaccess.Get<BonTahanan>(sqlStr).FirstOrDefault();
        }

        public void DeleteById(int Id)
        {
            dataaccess.DeleteFromDB<BonTahanan>(string.Format("DELETE FROM BonTahanan WHERE Id ='{0}'", Id), null);
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<Cases>("DELETE FROM BonTahanan", null);
        }

        public void DeleteByInmatesRegID(string RegID)
        {
            dataaccess.DeleteFromDB<BonTahanan>(string.Format("DELETE FROM BonTahanan WHERE RegID ='{0}'", RegID), null);
        }

        public int GetCount()
        {
            string sqlStr = @"SELECT Count(0) FROM BonTahanan";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int GetCountByRegID(string RegID)
        {
            string sqlStr = string.Empty;
            sqlStr = @"SELECT Count(0) FROM BonTahanan WHERE RegID = '" + RegID + "'";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int Post(BonTahanan data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO BonTahanan({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<BonTahanan>(strSql);
        }

        public void Update(BonTahanan data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    fieldsValue = String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                    columnName = columnName + String.Format("{0}={1}", item.Name, fieldsValue);

                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            strSql = String.Format("UPDATE BonTahanan SET {0} WHERE Id={1}", columnName, data.Id);
            dataaccess.UpdateToDB<BonTahanan>(strSql);
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
