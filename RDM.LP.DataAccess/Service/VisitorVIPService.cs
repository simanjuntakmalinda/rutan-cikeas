﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class VisitorVIPService : IDisposable
    {
        private readonly IRepository dataaccess;

        public VisitorVIPService()
        {
            dataaccess = new SqlBaseRepository();
        }

        public VisitorVIP GetDetailById(string id)
        {
            string sqlStr;
            sqlStr = @"SELECT A.* FROM VisitorVIP A WHERE A.Id= " + id;
            return dataaccess.Get<VisitorVIP>(sqlStr).FirstOrDefault();
        }

        public VisitorVIP GetDetailByIdentity(string idtype, string idno)
        {
            string sqlStr;
            sqlStr = @"SELECT A.* FROM VisitorVIP A WHERE A.IDType= '" + idtype + "' AND IDNo='" + idno + "'";
            return dataaccess.Get<VisitorVIP>(sqlStr).FirstOrDefault();
        }

        public IEnumerable<VisitorVIP> Get()
        {
            string sqlStr;
            sqlStr = @"SELECT * FROM VisitorVIP ORDER BY CreatedDate DESC";
            return dataaccess.Get<VisitorVIP>(sqlStr);
        }

        public IEnumerable<VisitorVIP> GetSudahCheckedOut()
        {
            string sqlStr;
            sqlStr = @"SELECT * FROM VisitorVIP WHERE IsCheckedOut = 'true' ORDER BY CreatedDate DESC";
            return dataaccess.Get<VisitorVIP>(sqlStr);
        }

        public IEnumerable<VisitorVIP> GetBelumCheckedOut()
        {
            string sqlStr;
            sqlStr = @"SELECT * FROM VisitorVIP WHERE IsCheckedOut = 'false' ORDER BY CreatedDate DESC";
            return dataaccess.Get<VisitorVIP>(sqlStr);
        }

        public int GetCount()
        {
              string sqlStr = @"SELECT COUNT(0) FROM VisitorVIP";
              return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int GetCountToday()
        {
            string sqlStr = @"SELECT COUNT(0) FROM VisitorVIP WHERE CONVERT(VARCHAR(10),CreatedDate,101) = CONVERT(VARCHAR(10),getdate(),101)";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public IEnumerable<DasboardChart> GetCountLast30days()
        {
            string sqlStr = @"SELECT DAY(IndividualDate) Category,(SELECT COUNT(0) FROM VisitorVIP WHERE CONVERT(varchar,CreatedDate,101) = CONVERT(varchar,IndividualDate,101)) Value FROM DateRange('d', (select dateadd(day, -29, getdate())), getdate())";
            return dataaccess.Get<DasboardChart>(sqlStr);
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<VisitorVIP>("DELETE FROM VisitorVIP", null);
        }

        public void DeleteById(string Id)
        {
            dataaccess.DeleteFromDB<VisitorVIP>(string.Format("DELETE FROM VisitorVIP WHERE Id ='{0}'", Id), null);
        }

        public void CheckOut(int id, string username)
        {
            string strSql = "UPDATE VisitorVIP SET IsCheckedOut = 1, EndVisit= getdate(), UpdatedBy='" + username + "',UpdatedDate=getdate() WHERE Id =" + id;
            dataaccess.UpdateToDB<VisitorVIP>(strSql);
        }

        public int Post(VisitorVIP data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO VisitorVIP({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<VisitorVIP>(strSql);
        }

        public void Update(VisitorVIP data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id" && item.Name != "DateReg")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    fieldsValue = String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                    columnName = columnName + String.Format("{0}={1}", item.Name, fieldsValue);

                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            strSql = String.Format("UPDATE VisitorVIP SET {0} WHERE Id={1}", columnName, data.Id);
            dataaccess.UpdateToDB<VisitorVIP>(strSql);
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
