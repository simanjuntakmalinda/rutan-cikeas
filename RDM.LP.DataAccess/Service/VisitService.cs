﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class VisitService : IDisposable
    {
        private readonly IRepository dataaccess;

        public VisitService()
        {
            dataaccess = new SqlBaseRepository();
        }


        public void CheckOutVisitor(int visitid,string username)
        {
            string strSql = "UPDATE Visit SET IsCheckedOut = 1, EndVisit= getdate(), UpdatedBy='" + username + "',UpdatedDate=getdate() WHERE IsCheckedOut = 0 AND Id =" + visitid;
            dataaccess.UpdateToDB<Visit>(strSql);
        }

        public void CheckOutVisitorByVisitorId(int visitorid, string username)
        {
            string strSql = "UPDATE Visit SET IsCheckedOut = 1, EndVisit= getdate(), UpdatedBy='" + username + "',UpdatedDate=getdate() WHERE IsCheckedOut = 0 AND VisitorId =" + visitorid;
            dataaccess.UpdateToDB<Visit>(strSql);
        }

        public Visit GetDetailById(string id)
        {
            string sqlStr;
            sqlStr = @"SELECT A.*,B.FullName InmatesName, C.FullName VisitorName FROM Visit A LEFT OUTER JOIN Inmates B ON A.InmatesId = B.Id LEFT OUTER JOIN Visitor C ON A.VisitorId = C.Id WHERE A.Id= " + id;
            return dataaccess.Get<Visit>(sqlStr).FirstOrDefault();
        }

        public IEnumerable<Visit> Get()
        {
            string sqlStr;
            sqlStr = @"SELECT A.*, B.FullName InmatesName, C.FullName VisitorName FROM Visit A LEFT OUTER JOIN Inmates B ON A.InmatesId = B.Id LEFT OUTER JOIN Visitor C ON A.VisitorId = C.Id ORDER BY A.CreatedDate DESC";
            return dataaccess.Get<Visit>(sqlStr);
        }

        public IEnumerable<Visit> GetHistory(int visitorId)
        {
            string sqlStr;
            sqlStr = @"SELECT A.*, B.FullName InmatesName, C.FullName VisitorName FROM Visit A LEFT OUTER JOIN Inmates B ON A.InmatesId = B.Id LEFT OUTER JOIN Visitor C ON A.VisitorId = C.Id WHERE A.VisitorId = " + visitorId + " ORDER BY A.CreatedDate DESC";
            return dataaccess.Get<Visit>(sqlStr);
        }

        public IEnumerable<Visit> GetAllWithNoCheckedOut()
        {
            string sqlStr;
            sqlStr = @"SELECT A.*, B.FullName InmatesName, C.FullName VisitorName FROM Visit A LEFT OUTER JOIN Inmates B ON A.InmatesId = B.Id LEFT OUTER JOIN Visitor C ON A.VisitorId = C.Id WHERE A.IsCheckedOut = 0 ORDER BY A.CreatedDate DESC";
            return dataaccess.Get<Visit>(sqlStr);
        }

        public Visit GetVisitorCheckout(string Id)
        {
            string sqlStr;
            sqlStr = @"select *  from Visit where Id = '" + Id + "' and IsCheckedOut = 0";
            return dataaccess.Get<Visit>(sqlStr).FirstOrDefault();
        }

        public IEnumerable<Visit> GetAllKunjungan()
        {
            string sqlStr;
            sqlStr = @"SELECT A.*, B.FullName InmatesName, C.FullName VisitorName FROM Visit A LEFT OUTER JOIN Inmates B ON A.InmatesId = B.Id LEFT OUTER JOIN Visitor C ON A.VisitorId = C.Id ORDER BY A.CreatedDate DESC";
            return dataaccess.Get<Visit>(sqlStr);
        }

        public int GetCount()
        {
              string sqlStr = @"SELECT Count(0) FROM Visit";
              return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public Visit GetDetailByRegID(string RegID)
        {
            string sqlStr;
            sqlStr = @"SELECT A.*,B.FullName InmatesName, C.FullName VisitorName FROM Visit A LEFT OUTER JOIN Inmates B ON A.InmatesId = B.Id LEFT OUTER JOIN Visitor C ON A.VisitorId = C.Id WHERE C.RegID= '" + RegID + "' ORDER BY CreatedDate DESC";
            return dataaccess.Get<Visit>(sqlStr).FirstOrDefault();
        }

        public IEnumerable<Visit> GetDetailVisitByRegID(string RegID)
        {
            string sqlStr;
            sqlStr = @"SELECT A.*,B.FullName InmatesName, C.FullName VisitorName FROM Visit A LEFT OUTER JOIN Inmates B ON A.InmatesId = B.Id LEFT OUTER JOIN Visitor C ON A.VisitorId = C.Id WHERE C.RegID= '" + RegID + "' ORDER BY CreatedDate DESC";
            return dataaccess.Get<Visit>(sqlStr);
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<Visit>("DELETE FROM Visit", null);
        }

        public void DeleteById(string Id)
        {
            dataaccess.DeleteFromDB<Visit>(string.Format("DELETE FROM Visit WHERE Id ='{0}'", Id), null);
        }

        public int Post(Visit data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO Visit({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<Visit>(strSql);
        }


        public void Update(Visit data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    fieldsValue = String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                    columnName = columnName + String.Format("{0}={1}", item.Name, fieldsValue);

                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            strSql = String.Format("UPDATE Visit SET {0} WHERE Id={1}", columnName, data.Id);
            dataaccess.UpdateToDB<Visit>(strSql);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
