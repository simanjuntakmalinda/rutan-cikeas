﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class VisitorLogService : IDisposable
    {
        private readonly IRepository dataaccess;

        public VisitorLogService()
        {
            dataaccess = new SqlBaseRepository();
        }
         
        public IEnumerable<VisitorLog> Get()
        {
            string sqlStr;
                sqlStr = @"SELECT [Id], [RegID] FROM VisitorLog"; 
            return dataaccess.Get<VisitorLog>(sqlStr);
        }

        public int GetCount()
        {
              string sqlStr = @"SELECT COUNT(0) FROM VisitorLog";
              return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<VisitorLog>("DELETE FROM VisitorLog", null);
        }

        public string GetRegID()
        {
            string lastno;
            string sqlStr = String.Format("SELECT ISNULL(MAX(RIGHT(RegID,6)),'000001') from VisitorLog WHERE LEN(RegID) = 8");
            var data = dataaccess.Get<int?>(sqlStr);
            lastno = string.Format("VC{0:000000}", data.ElementAtOrDefault(0) + 1);
            return lastno;
        }

        public VisitorLog GetDetailByRegID(string RegID)
        {
            string sqlStr;
            sqlStr = @"SELECT A.* FROM VisitorLog A  WHERE A.RegID= '" + RegID + "'";
            return dataaccess.Get<VisitorLog>(sqlStr).FirstOrDefault();
        }

        public int Post(VisitorLog data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO VisitorLog({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<VisitorLog>(strSql);
        }

        public void Update(VisitorLog data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    fieldsValue = String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                    columnName = columnName + String.Format("{0}={1}", item.Name, fieldsValue);

                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            strSql = String.Format("UPDATE VisitorLog SET {0} WHERE Id={1}", columnName, data.Id);
            dataaccess.UpdateToDB<VisitorLog>(strSql);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
