﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class InmatesIsCheckedOutService : IDisposable
    {
        private readonly IRepository dataaccess;

        public InmatesIsCheckedOutService()
        {
            dataaccess = new SqlBaseRepository();
        }

        public IEnumerable<InmatesIsCheckedOut> Get()
        {
            string sqlStr;
            sqlStr = @"SELECT * FROM InmatesIsCheckedOut ORDER BY CreatedDate DESC";
            return dataaccess.Get<InmatesIsCheckedOut>(sqlStr);
        }

        public int GetCount()
        {
            string sqlStr = @"SELECT COUNT(0) FROM InmatesIsCheckedOut";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int GetCountToday()
        {
            string sqlStr = @"SELECT COUNT(0) FROM InmatesIsCheckedOut WHERE CONVERT(VARCHAR(10),CreatedDate,101) = CONVERT(VARCHAR(10),getdate(),101)";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public GetInmatesCheckedOut GetDetailById(int Id)
        {
            string sqlStr;
            sqlStr = @"SELECT A.Id, A.InmatesId, A.RegID, A.InmatesName, A.DateReg, A.DateCheckedOut, A.Notes, B.* FROM InmatesIsCheckedOut A JOIN Inmates B ON B.Id = A.InmatesId WHERE A.Id= '" + Id + "'";
            return dataaccess.Get<GetInmatesCheckedOut>(sqlStr).FirstOrDefault();
        }

        public InmatesIsCheckedOut GetInmatesCheckoutCell(string RegID)
        {
            string sqlStr;
            sqlStr = @"select *  from InmatesIsCheckedOut where RegID = '" + RegID + "' and IsCheckedOut = 0";
            return dataaccess.Get<InmatesIsCheckedOut>(sqlStr).FirstOrDefault();
        }

        public IEnumerable<InmatesIsCheckedOut> GetAllInmatesWithNoCheckedOut()
        {
            string sqlStr;
            sqlStr = @"SELECT A.*, B.FullName InmatesName FROM InmatesIsCheckedOut A JOIN Inmates B ON A.InmatesId = B.Id WHERE A.IsCheckedOut = 0 ORDER BY A.CreatedDate DESC";
            return dataaccess.Get<InmatesIsCheckedOut>(sqlStr);
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<Visitor>("DELETE FROM InmatesIsCheckedOut", null);
        }

        public void DeleteById(string Id)
        {
            dataaccess.DeleteFromDB<Visitor>(string.Format("DELETE FROM InmatesIsCheckedOut WHERE Id ='{0}'", Id), null);
        }

        public int Post(InmatesIsCheckedOut data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO InmatesIsCheckedOut({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<InmatesIsCheckedOut>(strSql);
        }

        public void Update(InmatesIsCheckedOut data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id" && item.Name != "DateReg")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    fieldsValue = String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                    columnName = columnName + String.Format("{0}={1}", item.Name, fieldsValue);

                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            strSql = String.Format("UPDATE InmatesIsCheckedOut SET {0} WHERE Id={1}", columnName, data.Id);
            dataaccess.UpdateToDB<InmatesIsCheckedOut>(strSql);
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
