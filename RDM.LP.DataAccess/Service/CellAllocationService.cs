﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class CellAllocationService : IDisposable
    {
        private readonly IRepository dataaccess;

        public CellAllocationService()
        {
            dataaccess = new SqlBaseRepository();
        }
         
        public IEnumerable<CellAllocate> Get()
        {
            string sqlStr;
                sqlStr = @"SELECT A.Id, A.InmatesRegCode, C.CellCode, A.CellCodeId, A.CellStatus, A.DateEnter, A.CellAllocationStatusId, A.IsCheckedOut, 
                                A.CheckedOutDate, A.CheckedOutNote, A.CreatedBy, A.CreatedDate, A.CreatedLocation, A.UpdatedBy, A.UpdatedDate, A.UpdatedLocation, 
                                B.FullName + ' ( ' + B.RegID + ')' as FullName 
                            FROM CellAllocation A 
                            LEFT JOIN Cell C On A.CellCodeId = C.Id
                            JOIN Inmates B ON A.InmatesRegCode = B.RegID 
                            WHERE A.IsCheckedOut !=1 
                            ORDER BY A.CreatedDate DESC"; 
            return dataaccess.Get<CellAllocate>(sqlStr);
        }

        public IEnumerable<CellAllocate> GetByCellCode(string cellcode)
        {
            string sqlStr;
            sqlStr = @"SELECT * FROM CellAllocation WHERE CellCode = '" + cellcode +  "'";
            return dataaccess.Get<CellAllocate>(sqlStr);
        }

        public CellAllocate GetById(int Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT A.*, " +
                "B.FullName, B.Category, B.DateReg, B.IDType" +" +' / '+ "+ "B.IDNo as IDInmates, B.Gender, B.IDNo, B.Network, B.Type, B.Religion, B.DateReg, B.Peran, B.LokasiPenangkapan, B.TanggalKejadian, B.TanggalPenangkapan, C.Status as CellAllocationStatus," +
                " E.BuildingName,D.CellFloor, E.BuildingName" + " +' / '+ " + "D.CellFloor as BuildFloor,D.CellStatus, D.CellType,D.InUseCounter" +
                " FROM CellAllocation A JOIN Inmates B ON A.InmatesRegCode = B.RegID" +
                " LEFT JOIN CellAllocationStatus C ON A.CellAllocationStatusId = C.Id" +
                " LEFT JOIN Cell D ON A.CellCode = D.CellCode " +
                " LEFT JOIN Building E  ON D.BuildingId = E.Id WHERE A.Id = {0}", Id);
            return dataaccess.Get<CellAllocate>(sqlStr).FirstOrDefault();
        }

        public CellAllocate GetByRegId(string RegId)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT A.*, " +
                "B.FullName, B.Category, B.DateReg, B.IDType" + " +' / '+ " + "B.IDNo as IDInmates, B.Gender, B.IDNo, B.Network, B.Type, B.Religion, B.DateReg, B.Peran, B.LokasiPenangkapan, B.TanggalKejadian, B.TanggalPenangkapan, C.Status as CellAllocationStatus," +
                " E.BuildingName,D.CellFloor, E.BuildingName" + " +' / '+ " + "D.CellFloor as BuildFloor,D.CellStatus, D.CellType,D.InUseCounter" +
                " FROM CellAllocation A JOIN Inmates B ON A.InmatesRegCode = B.RegID" +
                " LEFT JOIN CellAllocationStatus C ON A.CellAllocationStatusId = C.Id" +
                " LEFT JOIN Cell D ON A.CellCode = D.CellCode " +
                " LEFT JOIN Building E  ON D.BuildingId = E.Id WHERE A.InmatesRegCode = '{0}'", RegId);
            return dataaccess.Get<CellAllocate>(sqlStr).FirstOrDefault();
        }

        public CellAllocate GetByInamateRegCode(string RegID)
        {
            string sqlStr;
            sqlStr = @"SELECT * FROM CellAllocation WHERE InmatesRegCode = '" + RegID + "'";
            return dataaccess.Get<CellAllocate>(sqlStr).FirstOrDefault();
        }

        public IEnumerable<CellAllocation> GetHistory(string inmatesId)
        {
            string sqlStr;
            sqlStr = @"SELECT A.InmatesRegCode, A.CellCode, A.DateEnter, A.CheckedOutDate,CASE WHEN A.IsCheckedOut = 1 THEN 'Keluar' ELSE 'Ditahan' END AS CellStatus FROM CellAllocation A WHERE A.InmatesRegCode = '" +inmatesId+ "' order by CheckedOutDate asc";
            return dataaccess.Get<CellAllocation>(sqlStr);
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<Cases>("DELETE FROM AppUser", null);
        }

        public void DeleteByInmatesRegCode(string inmatesregcode)
        {
            dataaccess.DeleteFromDB<CellAllocation>(string.Format("DELETE FROM CellAllocation WHERE InmatesRegCode ='{0}'", inmatesregcode), null);
        }

        public CellAllocation GetInmatesCheckoutCell(string Id)
        {
            string sqlStr;
            sqlStr = @"select InmatesRegCode  from CellAllocation where InmatesRegCode = '" + Id + "' and IsCheckedOut = 0";
            return dataaccess.Get<CellAllocation>(sqlStr).FirstOrDefault();
        }

        public CellAllocate GetInmatesById(int Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT * FROM CellAllocation WHERE CellCodeId = {0}", Id);
            return dataaccess.Get<CellAllocate>(sqlStr).FirstOrDefault();
        }

        public int GetCountByWardTypeStatusAvailable(int wardtype)
        {
            string sqlStr = string.Empty;
            switch (wardtype)
            {
                case 1: // Ward A
                    sqlStr = @"SELECT COunt(0) FROM Cell WHERE CellStatus = 'TERSEDIA' AND BuildingId = 1";
                    break;
                case 2: // Ward B
                    sqlStr = @"SELECT COunt(0) FROM Cell WHERE CellStatus = 'TERSEDIA' AND BuildingId = 2";
                    break;
                case 3: // Ward C
                    sqlStr = @"SELECT COunt(0) FROM Cell WHERE CellStatus = 'TERSEDIA' AND BuildingId = 3";
                    break;
            }
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int GetCountByWardTypeStatusOccupied(int wardtype)
        {
            string sqlStr = string.Empty;
            switch (wardtype)
            {
                case 1: // Ward A
                    sqlStr = @"SELECT COunt(0) FROM Cell WHERE CellStatus = 'DITEMPATI' AND BuildingId = 1";
                    break;
                case 2: // Ward B
                    sqlStr = @"SELECT COunt(0) FROM Cell WHERE CellStatus = 'DITEMPATI' AND BuildingId = 2";
                    break;
                case 3: // Ward C
                    sqlStr = @"SELECT COunt(0) FROM Cell WHERE CellStatus = 'DITEMPATI' AND BuildingId = 3";
                    break;
            }
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int GetCountByWardTypeStatusMaintenance(int wardtype)
        {
            string sqlStr = string.Empty;
            switch (wardtype)
            {
                case 1: // Ward A
                    sqlStr = @"SELECT COunt(0) FROM Cell WHERE CellStatus = 'DALAM PEMELIHARAAN' AND BuildingId = 1";
                    break;
                case 2: // Ward B
                    sqlStr = @"SELECT COunt(0) FROM Cell WHERE CellStatus = 'DALAM PEMELIHARAAN' AND BuildingId = 2";
                    break;
                case 3: // Ward C
                    sqlStr = @"SELECT COunt(0) FROM Cell WHERE CellStatus = 'DALAM PEMELIHARAAN' AND BuildingId = 3";
                    break;
            }
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int GetCountByWardType(int wardtype)
        {
            string sqlStr = string.Empty;
            switch (wardtype)       
            {
                case 1: // Ward A
                    sqlStr = @"SELECT COunt(0) FROM Cell WHERE BuildingId = 1";
                    break;
                case 2: // Ward B
                    sqlStr = @"SELECT COunt(0) FROM Cell WHERE BuildingId = 2";
                    break;
                case 3: // Ward C
                    sqlStr = @"SELECT COunt(0) FROM Cell WHERE BuildingId = 3";
                    break;
            }
          
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }



        public IEnumerable<Dasboard> GetCountByWardTypeGroupByGender(int wardtype)
        {
            string sqlStr = string.Empty;
            switch (wardtype)
            {
                case 1: // Ward A
                    sqlStr = @"SELECT AA.Gender Nama, SUM(AA.Total) Total FROM (SELECT 'LAKI-LAKI' Gender, 0 Total UNION SELECT 'PEREMPUAN' Gender, 0 Total UNION SELECT A.* FROM(SELECT  C.Gender,Count(0) Total FROM CellAllocation A INNER JOIN Cell B ON A.CellCode = B.CellCode INNER JOIN Inmates C ON A.InmatesRegCode = C.RegID WHERE B.BuildingId =1 AND A.IsCheckedOut=0 GROUP BY C.Gender) A) AA GROUP BY AA.Gender ORDER BY AA.Gender DESC";
                    break;
                case 2: // Ward B
                    sqlStr = @"SELECT AA.Gender Nama, SUM(AA.Total) Total FROM (SELECT 'LAKI-LAKI' Gender, 0 Total UNION SELECT 'PEREMPUAN' Gender, 0 Total UNION SELECT A.* FROM(SELECT  C.Gender,Count(0) Total FROM CellAllocation A INNER JOIN Cell B ON A.CellCode = B.CellCode INNER JOIN Inmates C ON A.InmatesRegCode = C.RegID WHERE B.BuildingId =2 AND A.IsCheckedOut=0 GROUP BY C.Gender) A) AA GROUP BY AA.Gender ORDER BY AA.Gender DESC";
                    break;
                case 3: // Ward C
                    sqlStr = @"SELECT AA.Gender Nama, SUM(AA.Total) Total FROM (SELECT 'LAKI-LAKI' Gender, 0 Total UNION SELECT 'PEREMPUAN' Gender, 0 Total UNION SELECT A.* FROM(SELECT  C.Gender,Count(0) Total FROM CellAllocation A INNER JOIN Cell B ON A.CellCode = B.CellCode INNER JOIN Inmates C ON A.InmatesRegCode = C.RegID WHERE B.BuildingId =3 AND A.IsCheckedOut=0 GROUP BY C.Gender) A) AA GROUP BY AA.Gender ORDER BY AA.Gender DESC";
                    break;
            }

            return dataaccess.Get<Dasboard>(sqlStr);
        }


        public IEnumerable<Dasboard> GetCountByWardTypeGroupByReligion(int wardtype)
        {
            string sqlStr = string.Empty;
            switch (wardtype)
            {
                case 1: // Ward A
                    sqlStr = @"SELECT  C.Religion Nama,Count(0) Total FROM CellAllocation A INNER JOIN Cell B ON A.CellCode = B.CellCode INNER JOIN Inmates C ON A.InmatesRegCode = C.RegID WHERE B.BuildingId =1 AND A.IsCheckedOut=0 GROUP BY C.Religion";
                    break;
                case 2: // Ward B
                    sqlStr = @"SELECT  C.Religion Nama,Count(0) Total FROM CellAllocation A INNER JOIN Cell B ON A.CellCode = B.CellCode INNER JOIN Inmates C ON A.InmatesRegCode = C.RegID WHERE B.BuildingId =2 AND A.IsCheckedOut=0 GROUP BY C.Religion";
                    break;
                case 3: // Ward C
                    sqlStr = @"SELECT  C.Religion Nama,Count(0) Total FROM CellAllocation A INNER JOIN Cell B ON A.CellCode = B.CellCode INNER JOIN Inmates C ON A.InmatesRegCode = C.RegID WHERE B.BuildingId =3 AND A.IsCheckedOut=0 GROUP BY C.Religion";
                    break;
            }

            return dataaccess.Get<Dasboard>(sqlStr);
        }
        public int GetCount()
        {
              string sqlStr = @"SELECT COunt(0) FROM CellAllocation";
              return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int Post(CellAllocation data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO CellAllocation({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<CellAllocation>(strSql);
        }

        public void Update(CellAllocation data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    fieldsValue = String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                    columnName = columnName + String.Format("{0}={1}", item.Name, fieldsValue);
                    
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            strSql = String.Format("UPDATE CellAllocation SET {0} WHERE Id={1}", columnName, data.Id);
            dataaccess.UpdateToDB<CellAllocation>(strSql);
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
