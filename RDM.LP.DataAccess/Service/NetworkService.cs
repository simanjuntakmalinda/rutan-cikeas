﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class NetworkService : IDisposable
    {
        private readonly IRepository dataaccess;

        public NetworkService()
        {
            dataaccess = new SqlBaseRepository();
        }
         
        public IEnumerable<Network> Get()
        {
            string sqlStr;
                sqlStr = @"SELECT * FROM Network"; 
            return dataaccess.Get<Network>(sqlStr);
        }

        public Network GetbyId(int Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT * " +
                "FROM Network A " +
                "WHERE A.Id = {0} ", Id);
            return dataaccess.Get<Network>(sqlStr).FirstOrDefault();
        }

        public IEnumerable<InmatesNetwork> GetInmatesNetwork(string regID)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT A.Id, A.NetworkId, B.Name NetworkName FROM InmatesNetwork A LEFT JOIN Network B ON A.NetworkId = B.Id WHERE A.RegID ='{0}'", regID);
            return dataaccess.Get<InmatesNetwork>(sqlStr);
        }

        public int GetCount()
        {
              string sqlStr = @"SELECT COunt(0) FROM Network";
              return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<Network>("DELETE FROM Network", null);
        }

        public void DeleteNetworkByRegID(string regID)
        {
            dataaccess.DeleteFromDB<Inmates>(string.Format("DELETE FROM InmatesNetwork WHERE RegID ='{0}'", regID), null);
        }

        public int Post(Network data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO Network({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<InmatesNetwork>(strSql);
        }

        public string GetNetworkByRegId(string regID)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT STUFF((" +
                    "SELECT DISTINCT ', ' + CAST(B.Name AS VARCHAR(100)) " +
                    "FROM Network B " +
                    "JOIN InmatesNetwork C ON B.Id = C.NetworkId " +
                    "WHERE C.RegID = A.RegID " +
                    "FOR XML PATH('')), 1, 1, '') AS Nama " +
                "FROM Inmates A " +
                "WHERE A.RegID ='{0}'", regID);
            return dataaccess.Get<string>(sqlStr).ElementAtOrDefault(0);
        }

        public int PostNetwork(InmatesNetwork data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO InmatesNetwork({0}) OUTPUT INSERTED.ID VALUES({1});", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<Inmates>(strSql);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
