﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class CellService : IDisposable
    {
        private readonly IRepository dataaccess;

        public CellService()
        {
            dataaccess = new SqlBaseRepository();
        }

        public IEnumerable<Cell> Get()
        {
            string sqlStr;
            sqlStr = @"SELECT a.*, b.BuildingName FROM Cell a join Building b on a.Buildingid = b.Id ORDER BY CreatedDate DESC";
            return dataaccess.Get<Cell>(sqlStr);
        }

        public IEnumerable<Cell> GetAvailableCell()
        {
            string sqlStr;
            sqlStr = @"SELECT a.*, b.BuildingName FROM Cell a join Building b on a.Buildingid = b.Id WHERE a.CellStatus ='TERSEDIA' ORDER BY CreatedDate DESC";
            return dataaccess.Get<Cell>(sqlStr);
        }

        public Cell GetByCode(string code)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT a.*, b.BuildingName FROM Cell a left outer join Building b on a.Buildingid = b.Id WHERE a.CellCode = '{0}'", code);
            return dataaccess.Get<Cell>(sqlStr).FirstOrDefault();
        }

        public GetCell GetRoommate(string code)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT a.CellCode, a.BuildingId, a.CellFloor, a.CellNumber, a.CellStatus, a.InUseCounter, c.Id IdCellAllocation, c.InmatesRegCode, c.IsCheckedOut, d.FullName Roommate FROM Cell a" +
                " left outer join CellAllocation c on a.CellCode = c.CellCode " +
                " left outer join Inmates d on c.InmatesRegCode = d.RegID" +
                " WHERE a.CellCode = '{0}' and c.IsCheckedOut = 0", code);
            return dataaccess.Get<GetCell>(sqlStr).FirstOrDefault();
        }

        public GetCell GetRoommateDetail(string regid, string code)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT a.CellCode, a.BuildingId, a.CellFloor, a.CellNumber, a.CellStatus, a.InUseCounter, c.Id IdCellAllocation, c.InmatesRegCode, c.IsCheckedOut, d.FullName Roommate, d.RegID RegRoommate FROM Cell a" +
                " left outer join CellAllocation c on a.CellCode = c.CellCode " +
                " left outer join Inmates d on c.InmatesRegCode = d.RegID" +
                " WHERE d.RegID != '{0}' and a.CellCode = '{1}' and c.IsCheckedOut = 0", regid, code);
            return dataaccess.Get<GetCell>(sqlStr).FirstOrDefault();
        }

        public IEnumerable<Cell> GetByType(string type)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT a.*, b.BuildingName FROM Cell a join Building b on a.Buildingid = b.Id WHERE a.CellStatus ='TERSEDIA' and a.CellType = '{0}' ORDER BY CreatedDate DESC", type);
            return dataaccess.Get<Cell>(sqlStr);
        }

        public Cell GetById(int Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT a.*, b.BuildingName FROM Cell a left outer join Building b on a.Buildingid = b.Id WHERE a.Id = {0}", Id);
            return dataaccess.Get<Cell>(sqlStr).FirstOrDefault();
        }
        
        public Cell GetByCellCode(string cellCode)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT a.*, b.BuildingName FROM Cell a left outer join Building b on a.Buildingid = b.Id WHERE a.CellCode ='{0}'", cellCode);
            return dataaccess.Get<Cell>(sqlStr).FirstOrDefault();
        }
        
        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<Cell>("DELETE FROM Cell", null);
        }
        
        public void DeleteByCellCode(string cellCode)
        {
            dataaccess.DeleteFromDB<Cell>(string.Format("DELETE FROM Cell WHERE CellCode ='{0}'", cellCode), null);
        }
        
        public int GetCount()
        {
            string sqlStr = @"SELECT Count(0) FROM Cell";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }
        
        public int Post(Cell data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO Cell({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<Cell>(strSql);
        }
        
        public void Update(Cell data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    fieldsValue = String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                    columnName = columnName + String.Format("{0}={1}", item.Name, fieldsValue);
        
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            strSql = String.Format("UPDATE Cell SET {0} WHERE Id={1}", columnName, data.Id);
            dataaccess.UpdateToDB<Cell>(strSql);
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
