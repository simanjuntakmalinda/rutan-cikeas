﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class BonTerimaTahananService : IDisposable
    {
        private readonly IRepository dataaccess;

        public BonTerimaTahananService()
        {
            dataaccess = new SqlBaseRepository();
        }

        public IEnumerable<GetTerimaBon> Get()
        {
            string sqlStr;
            //sqlStr = @"SELECT B.Id AS IdBonTahanan, B.NoBonTahanan, B.StatusTahanan, C.RegID AS RegIDTahanan, C.FullName AS NamaTahanan, E.CellCode, E.CellFloor, E.CellNumber, F.BuildingName,
            //            A.Id, A.NamaYangMenyerahkan, A.NamaYangMenerima, DATEADD(ms, DATEDIFF(ms, '00:00:00', A.JamTerima), CONVERT(DATETIME, A.TanggalTerima)) AS TanggalTerima, DATEADD(ms, DATEDIFF(ms, '00:00:00', A.JamTerima), CONVERT(DATETIME, A.TanggalTerima)) AS JamTerima, A.ZonaWaktuTerima, A.NamaYangMenyerahkan, A.NrpYangMenyerahkan, A.PangkatYangMenyerahkan, A.SatkerYangMenyerahkan, A.NamaYangMenerima, A.NrpYangMenerima, A.PangkatYangMenerima, A.SatkerYangMenerima, A.NamaYangMenyetujui, A.NrpYangMenyetujui, A.PangkatYangMenyetujui, A.SatkerYangMenyetujui, A.CreatedBy, A.CreatedDate, A.CreatedLocation, A.UpdatedBy, A.UpdatedDate, A.UpdatedLocation
            //            FROM BonTerimaTahanan A 
            //            JOIN BonTahanan B ON A.NoBonTahanan = B.Id
            //            JOIN InmateBonTahanan G ON A.Id = G.BonTahananId
            //            JOIN Inmates C ON G.RegID = C.RegID
            //            LEFT JOIN CellAllocation D ON C.RegID = D.InmatesRegCode
            //            LEFT JOIN Cell E ON D.CellCodeId = E.Id
            //            LEFT JOIN Building F ON E.BuildingId = F.Id
            //            ORDER BY A.CreatedDate DESC";
            sqlStr = @"SELECT B.Id AS IdBonTahanan, B.NoBonTahanan, B.StatusTahanan,
                        A.Id, A.NamaYangMenyerahkan, A.NamaYangMenerima, DATEADD(ms, DATEDIFF(ms, '00:00:00', A.JamTerima), CONVERT(DATETIME, A.TanggalTerima)) AS TanggalTerima, DATEADD(ms, DATEDIFF(ms, '00:00:00', A.JamTerima), CONVERT(DATETIME, A.TanggalTerima)) AS JamTerima, A.ZonaWaktuTerima, A.NamaYangMenyerahkan, A.NrpYangMenyerahkan, A.PangkatYangMenyerahkan, A.SatkerYangMenyerahkan, A.NamaYangMenerima, A.NrpYangMenerima, A.PangkatYangMenerima, A.SatkerYangMenerima, A.NamaYangMenyetujui, A.NrpYangMenyetujui, A.PangkatYangMenyetujui, A.SatkerYangMenyetujui, A.CreatedBy, A.CreatedDate, A.CreatedLocation, A.UpdatedBy, A.UpdatedDate, A.UpdatedLocation
                        FROM BonTerimaTahanan A 
                        JOIN BonTahanan B ON A.NoBonTahanan = B.Id
                        ORDER BY A.CreatedDate DESC";
            return dataaccess.Get<GetTerimaBon>(sqlStr);
        }

        public GetTerimaBon GetById(int Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT B.Id AS IdBonTahanan, B.NoBonTahanan, B.StatusTahanan, C.RegID AS RegIDTahanan, C.FullName AS NamaTahanan, E.CellCode, E.CellFloor, E.CellNumber, F.BuildingName," +
                " A.Id, A.NamaYangMenyerahkan, A.NamaYangMenerima, DATEADD(ms, DATEDIFF(ms, '00:00:00', A.JamTerima), CONVERT(DATETIME, A.TanggalTerima)) AS TanggalTerima, DATEADD(ms, DATEDIFF(ms, '00:00:00'," +
                " A.JamTerima), CONVERT(DATETIME, A.TanggalTerima)) AS JamTerima, A.ZonaWaktuTerima, A.NamaYangMenyerahkan, A.NrpYangMenyerahkan, H.Id AS IdPangkatMenyerahkan, H.Nama AS PangkatYangMenyerahkan, A.SatkerYangMenyerahkan, A.NamaYangMenerima," +
                " A.NrpYangMenerima, G.Id AS IdPangkatMenerima, G.Nama AS PangkatYangMenerima, A.SatkerYangMenerima, A.NamaYangMenyetujui, A.NrpYangMenyetujui, I.Id AS IdPangkatMenyetujui, I.Nama AS PangkatYangMenyetujui, A.SatkerYangMenyetujui, A.CreatedBy, A.CreatedDate, A.CreatedLocation, A.UpdatedBy, A.UpdatedDate, A.UpdatedLocation" +
                " FROM BonTerimaTahanan A" +
                " JOIN BonTahanan B ON A.NoBonTahanan = B.Id" +
                " JOIN InmateBonTahanan X ON B.Id = X.BonTahananId" +
                " JOIN Inmates C ON X.RegID = C.RegID" +
                " LEFT JOIN CellAllocation D ON C.RegID = D.InmatesRegCode" +
                " LEFT JOIN Cell E ON D.CellCodeId = E.Id" +
                " LEFT JOIN Building F ON E.BuildingId = F.Id" +
                " LEFT JOIN PangkatPolisi G ON A.PangkatYangMenerima = G.Id" +
                " LEFT JOIN PangkatPolisi H ON A.PangkatYangMenyerahkan = H.Id" +
                " LEFT JOIN PangkatPolisi I ON A.PangkatYangMenyetujui = I.Id" +
                " WHERE A.Id = {0}", Id);
            return dataaccess.Get<GetTerimaBon>(sqlStr).FirstOrDefault();
        }

        public GetTerimaBon GetByInamateRegID(string RegID)
        {
            string sqlStr;
            sqlStr = @"SELECT B.Id AS IdBonTahanan, B.NoBonTahanan, C.RegID AS RegIDTahanan, C.FullName AS NamaTahanan, E.CellCode, E.CellFloor, E.CellNumber, F.BuildingName, A.* 
                        FROM BonTerimaTahanan A 
                        JOIN BonTahanan B ON A.NoBonTahanan = B.Id
                        JOIN InmateBonTahanan G ON A.Id = G.BonTahananId
                        JOIN Inmates C ON G.RegID = C.RegID
                        LEFT JOIN CellAllocation D ON C.RegID = D.InmatesRegCode
                        LEFT JOIN Cell E ON D.CellCodeId = E.Id
                        LEFT JOIN Building F ON E.BuildingId = F.Id
                        WHERE RegID = '" + RegID + "'";
            return dataaccess.Get<GetTerimaBon>(sqlStr).FirstOrDefault();
        }

        public void DeleteById(int Id)
        {
            dataaccess.DeleteFromDB<BonTerimaTahanan>(string.Format("DELETE FROM BonTerimaTahanan WHERE Id ='{0}'", Id), null);
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<Cases>("DELETE FROM BonTerimaTahanan", null);
        }

        public int GetCount()
        {
            string sqlStr = @"SELECT Count(0) FROM BonTerimaTahanan";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int Post(BonTerimaTahanan data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO BonTerimaTahanan({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<BonTerimaTahanan>(strSql);
        }

        public void Update(BonTerimaTahanan data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    fieldsValue = String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                    columnName = columnName + String.Format("{0}={1}", item.Name, fieldsValue);

                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            strSql = String.Format("UPDATE BonTerimaTahanan SET {0} WHERE Id={1}", columnName, data.Id);
            dataaccess.UpdateToDB<BonTerimaTahanan>(strSql);
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
