﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class PhotosService : IDisposable
    {
        private readonly IRepository dataaccess;

        public PhotosService()
        {
            dataaccess = new SqlBaseRepository();
        }
         
        public IEnumerable<Photos> Get()
        {
            string sqlStr;
                sqlStr = @"SELECT * FROM Photos"; 
            return dataaccess.Get<Photos>(sqlStr);
        }

        public IEnumerable<Photos> GetByRegID(string regid)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT * FROM Photos WHERE RegID = '{0}'",regid);
            return dataaccess.Get<Photos>(sqlStr);
        }

        public int GetCount()
        {
              string sqlStr = @"SELECT COunt(0) FROM Photos";
              return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<Photos>("DELETE FROM Photos", null);
        }

        public void DeleteByRegId(string RegID)
        {
            dataaccess.DeleteFromDB<Photos>(string.Format("DELETE FROM Photos WHERE RegID ='{0}'", RegID), null);
        }

        public void DeleteLastPhotoByRegId(string RegID)
        {
            //dataaccess.DeleteFromDB<Photos>(string.Format("DELETE TOP(1) FROM Photos WHERE RegID ='{0}' ORDER BY Id DESC", RegID), null);
            dataaccess.DeleteFromDB<Photos>(string.Format("WITH T  AS(SELECT TOP 1 * FROM   Photos WHERE RegID = '{0}' ORDER  BY Id DESC) DELETE FROM T", RegID), null);
        }

        public void UploadFoto(Photos data)
        {
            var strSql = String.Format("INSERT INTO Photos(RegID,Data,CreatedDate,CreatedBy) VALUES('{0}',{1},'{2}','{3}')", data.RegID, data.Data,data.CreatedDate,data.CreatedBy);
            dataaccess.InsertIntoDB<Photos>(strSql);
        }

        public int Post(Photos data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "byte[]":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO Photos({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<Photos>(strSql);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
