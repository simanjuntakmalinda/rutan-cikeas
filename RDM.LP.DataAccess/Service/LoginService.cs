﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Linq;

namespace RDM.LP.DataAccess.Service
{
    public class LoginService : IDisposable
    {
        private readonly IRepository dataaccess;

        public LoginService()
        {
            dataaccess = new SqlBaseRepository();
        }


        public AspnetUsers GetLoggedIn()
        {
            var data = dataaccess.Get<AspnetUsers>("SELECT * FROM AppUser WHERE IsLoggedIn = 1");
            return data.FirstOrDefault();
        }

        public AspnetUsers Get(string username, string password)
        {
            var data = dataaccess.Get<AspnetUsers>("SELECT * FROM AppUser WHERE UserName=@uname AND Password=@pwd", new { uname = username, pwd = password });
            return data.FirstOrDefault();
        }

        public AspnetUsers Get(string username)
        {
            var data = dataaccess.Get<AspnetUsers>("SELECT * FROM AppUser WHERE LOWER(UserName)=@uname", new { uname = username.ToLower() });
            return data.FirstOrDefault();
        }

        public void UpdateLastLogin(AspnetUsers user)
        {
            string sqlstr = string.Format("UPDATE AppUser SET LastLogin=getdate(), IsLoggedIn={0} WHERE LOWER(UserName)= '{1}'", user.IsLoggedIn  , user.UserName);
            dataaccess.ExecuteQuery(sqlstr);
        }

        public void UpdateIsLoggedIn(string username, bool isLoggedIn)
        {
            int rememberLogin;
            if (isLoggedIn)
                rememberLogin = 1;
            else
                rememberLogin = 0;
            string sqlstr = @"UPDATE AppUser SET IsLoggedIn=@remember WHERE UserName=@uname";
            dataaccess.ExecuteQuery(sqlstr, new { uname = username, remember = rememberLogin });
        }

        public void Update(string username, string password, bool isLoggedIn)
        {
            int rememberLogin;
            if (isLoggedIn)
                rememberLogin = 1;
            else
                rememberLogin = 0;
            string sqlstr = @"UPDATE AppUser SET Password=@pwd, IsLoggedIn=@remember WHERE UserName=@uname";
            dataaccess.ExecuteQuery(sqlstr, new { uname = username, pwd = password, remember = rememberLogin });
        }

        public void Create(AspnetUsers data)
        {
            string sqlstr = String.Format(@"INSERT INTO AppUser
                               (
                                [Password]
                               ,[UserName]
                               ,[IsLoggedIn]
                               )
                         VALUES
                               ('{0}','{1}','{2}', 0)", data.Password,data.UserName);

            dataaccess.InsertIntoDB<AspnetUsers>(sqlstr);
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~LoginService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}