﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class SeizedAssetsService : IDisposable
    {
        private readonly IRepository dataaccess;

        public SeizedAssetsService()
        {
            dataaccess = new SqlBaseRepository();
        }

        public IEnumerable<SeizedAssets> Get()
        {
            string sqlStr;
            sqlStr = @"SELECT A.Id, A.Name AS CaseName, A.CreatedDate, A.CreatedBy,
                                STUFF((SELECT DISTINCT ', ' + CAST(D.Quantity + ' ' + C.Nama AS VARCHAR(100))
                                FROM SeizedAssetsCase B
		                        LEFT JOIN SeizedAssets D ON B.SeizedAssetsId = D.Id
		                        LEFT JOIN SeizedAssetsCategory C ON D.CategoryId = C.Id
                                WHERE B.CaseId = A.Id
                                FOR XML PATH('')),1,1,'') AS Nama
                        FROM Cases A
                        WHERE A.Id IN (SELECT CaseId FROM SeizedAssetsCase)
                        ORDER BY CreatedDate DESC";
            return dataaccess.Get<SeizedAssets>(sqlStr);
        }

        public SeizedAssets GetById(int Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT A.Id, A.Name AS CaseName, A.CreatedDate, A.CreatedBy, " +
                "STUFF((SELECT DISTINCT ', ' + CAST(D.Quantity + ' ' + C.Nama AS VARCHAR(100)) " +
                "FROM SeizedAssetsCase B " +
                "LEFT JOIN SeizedAssets D ON B.SeizedAssetsId = D.Id " +
                "LEFT JOIN SeizedAssetsCategory C ON D.CategoryId = C.Id " +
                "WHERE B.CaseId = A.Id " +
                "FOR XML PATH('')), 1, 1, '') AS Nama " +
                "FROM Cases A " +
                "WHERE A.Id IN(SELECT CaseId FROM SeizedAssetsCase) AND A.Id = { 0} ", Id);
            return dataaccess.Get<SeizedAssets>(sqlStr).FirstOrDefault();
        }

        public IEnumerable<SeizedAssets> GetByCaseId(int Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT C.Id, A.Id AS CaseId, A.Name AS CaseName, C.CategoryId, D.Nama AS CategoryName, " +
                "C.Detail, C.Quantity, C.StorageType, C.StorageLocation, C.ConfiscatedDate, C.Note " +
                "FROM Cases A " +
                "JOIN SeizedAssetsCase B ON A.Id = B.CaseId " +
                "LEFT JOIN SeizedAssets C ON B.SeizedAssetsId = C.Id " +
                "LEFT JOIN SeizedAssetsCategory D ON C.CategoryId = D.Id " +
                "WHERE A.Id = {0} ", Id);
            return dataaccess.Get<SeizedAssets>(sqlStr);
        }

        public IEnumerable<SeizedAssets> GetAllByRegID(string RegID)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT C.Id, A.Id AS CaseId, A.Name AS CaseName, C.CategoryId, D.Nama AS CategoryName, " +
                "C.Detail, C.Quantity, C.StorageType, C.StorageLocation, C.ConfiscatedDate, C.Note " +
                "FROM Cases A " +
                "LEFT JOIN SeizedAssetsCase B ON A.Id = B.CaseId " +
                "LEFT JOIN SeizedAssets C ON B.SeizedAssetsId = C.Id " +
                "LEFT JOIN SeizedAssetsCategory D ON C.CategoryId = D.Id " +
                "LEFT JOIN InmatesCase E ON A.Id = E.CaseId " +
                "WHERE C.Id IS NOT NULL AND E.RegID = '{0}' ", RegID);
            return dataaccess.Get<SeizedAssets>(sqlStr);
        }

        public IEnumerable<SeizedAssets> GetCases(int Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT A.*, C.Nama AS CategoryName, D.Name AS CaseName, D.Id AS CaseId " +
                "FROM SeizedAssets A " +
                "LEFT JOIN SeizedAssetsCase B ON A.Id = B.SeizedAssetsId " +
                "LEFT JOIN SeizedAssetsCategory C ON A.CategoryId = C.Id " +
                "LEFT JOIN Cases D ON B.CaseId = D.Id WHERE A.Id = {0} ", Id);
            return dataaccess.Get<SeizedAssets>(sqlStr);
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<Cases>("DELETE FROM SeizedAssets", null);
        }

        public void DeleteById(int id)
        {
            dataaccess.DeleteFromDB<Cases>(string.Format("DELETE FROM SeizedAssets WHERE Id = '{0}'", id), null);
        }

        public void DeleteSeizedAssetsCase(int id)
        {
            dataaccess.DeleteFromDB<Cases>(string.Format("DELETE FROM SeizedAssetsCase WHERE CaseId = '{0}' ", id), null);
        }

        public int GetCount()
        {
            string sqlStr = @"SELECT COUNT(0) FROM SeizedAssets";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int Post(SeizedAssets data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false 
                    && item.Name != "Id"
                    && item.Name != "CaseId"
                    && item.Name != "SeizedAssetsId")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO SeizedAssets({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<SeizedAssets>(strSql);
        }

        public int PostAssetsCase(SeizedAssets data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null 
                    && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id" && item.Name != "CategoryId")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO SeizedAssetsCase({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<SeizedAssets>(strSql);
        }

        public void Update(SeizedAssets data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null 
                    && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false 
                    && item.Name != "Id" 
                    && item.Name != "CategoryName"
                    && item.Name != "SeizedAssetsId")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    fieldsValue = String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                    columnName = columnName + String.Format("{0}={1}", item.Name, fieldsValue);

                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            strSql = String.Format("UPDATE SeizedAssets SET {0} WHERE Id={1}", columnName, data.Id);
            dataaccess.UpdateToDB<SeizedAssets>(strSql);
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}