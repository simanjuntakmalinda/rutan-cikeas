﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class AuditTrailService : IDisposable
    {
        private readonly IRepository dataaccess;

        public AuditTrailService()
        {
            dataaccess = new SqlBaseRepository();
        }
         
        public IEnumerable<AuditTrail> Get(string username, string startdate, string enddate)
        {
            string sqlStr;
            if(username == "ALL USER")
                sqlStr = String.Format("SELECT * FROM AuditTrail WHERE LogDate BETWEEN  '{0}' AND DATEADD(s,-1,DATEADD(d,1,'{1}'))", startdate, enddate);
            else
                sqlStr = String.Format("SELECT * FROM AuditTrail WHERE UserName='{0}' AND LogDate BETWEEN  '{1}' AND DATEADD(s,-1,DATEADD(d,1,'{2}'))", username,startdate,enddate); 
            return dataaccess.Get<AuditTrail>(sqlStr);
        }

        public int GetCount()
        {
              string sqlStr = @"SELECT COunt(0) FROM AuditTrail";
              return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<AuditTrail>("DELETE FROM AuditTrail", null);
        }

        public int Post(AuditTrail data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false /*&& item.Name != "Id"*/)
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO AuditTrail({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<AuditTrail>(strSql);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
