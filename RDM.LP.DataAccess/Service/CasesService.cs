﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class CasesService : IDisposable
    {
        private readonly IRepository dataaccess;

        public CasesService()
        {
            dataaccess = new SqlBaseRepository();
        }

        public IEnumerable<Cases> Get()
        {
            string sqlStr;
            sqlStr = @"SELECT * FROM Cases";
            return dataaccess.Get<Cases>(sqlStr);
        }

        public Cases GetbyId(int Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT * " +
                "FROM Cases A " +
                "WHERE A.Id = {0} ", Id);
            return dataaccess.Get<Cases>(sqlStr).FirstOrDefault();
        }

        public IEnumerable<Cases> GetDetailRpt()
        {
            string sqlStr;
            sqlStr = @"SELECT X.Name ,
                        (SELECT Stuff(
                        (SELECT N', ' + C.FullName  FROM Cases A LEFT JOIN InmatesCase B ON A.Id = B.CaseId LEFT JOIN Inmates C ON B.RegID = C.RegID 
                        WHERE B.CaseId = X.Id FOR XML PATH(''),TYPE)
                        .value('text()[1]','nvarchar(max)'),1,2,N'')
                        ) Inmates,
                        (SELECT Stuff(
                        (SELECT N', ' + A.Detail  FROM SeizedAssets A LEFT JOIN SeizedAssetsCase B ON A.Id = B.SeizedAssetsId 
                         WHERE B.CaseId = X.Id FOR XML PATH(''),TYPE)
                        .value('text()[1]','nvarchar(max)'),1,2,N'')
                        ) SeizedAssets
                        FROM CASES X";
            return dataaccess.Get<Cases>(sqlStr);
        }

        public IEnumerable<InmatesCase> GetInmatesCase(string regID)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT A.Id, A.CaseId, B.Name CaseName FROM InmatesCase A LEFT JOIN Cases B ON A.CaseId = B.Id WHERE A.RegID ='{0}'", regID);
            return dataaccess.Get<InmatesCase>(sqlStr);
        }

        public int GetCount()
        {
            string sqlStr = @"SELECT Count(0) FROM Cases";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<Cases>("DELETE FROM Cases", null);
        }

        public void DeleteInmatesCase(string regID)
        {
            dataaccess.DeleteFromDB<Inmates>(string.Format("DELETE FROM InmatesCase WHERE RegID ='{0}'", regID), null);
        }

        public int Post(Cases data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null 
                    && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false 
                    && item.Name != "Id"
                    && item.Name != "LpId"
                    && item.Name != "KlasifikasiKasusId"
                    && item.Name != "Status")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO Cases({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<Cases>(strSql);
        }

        public string GetCaseByRegId(string regID)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT STUFF((" +
                    "SELECT DISTINCT ', ' + CAST(B.Name AS VARCHAR(100)) " +
                    "FROM Cases B " +
                    "JOIN InmatesCase C ON B.Id = C.CaseId " +
                    "WHERE C.RegID = A.RegID " +
                    "FOR XML PATH('')), 1, 1, '') AS Nama " +
                "FROM Inmates A " +
                "WHERE A.RegID ='{0}'", regID);
            return dataaccess.Get<string>(sqlStr).ElementAtOrDefault(0);
        }

        public int PostCases(InmatesCase data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO InmatesCase({0}) OUTPUT INSERTED.ID VALUES({1});", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<Inmates>(strSql);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
