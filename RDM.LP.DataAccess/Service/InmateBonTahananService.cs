﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class InmateBonTahananService : IDisposable
    {
        private readonly IRepository dataaccess;

        public InmateBonTahananService()
        {
            dataaccess = new SqlBaseRepository();
        }

        public IEnumerable<InmateBonTahanan> Get()
        {
            string sqlStr;
            sqlStr = @"SELECT * FROM InmateBonTahanan ORDER BY CreatedDate DESC";
            return dataaccess.Get<InmateBonTahanan>(sqlStr);
        }

        public InmateBonTahanan GetById(int Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT * FROM InmateBonTahanan WHERE Id = {0}", Id);
            return dataaccess.Get<InmateBonTahanan>(sqlStr).FirstOrDefault();
        }

        public IEnumerable<GetInmateBontahanan> GetByBonTahananId(int Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT B.Id, B.BonTahananId, B.RegID, X.*, X.RegID +' - '+X.FullName as NamaLengkap, I.BuildingName, H.CellNumber AS CellNo, H.CellCode FROM InmateBonTahanan B JOIN Inmates X ON X.RegID = B.RegID LEFT JOIN CellAllocation G ON B.RegID = G.InmatesRegCode LEFT JOIN Cell H ON G.CellCodeId = H.Id LEFT JOIN Building I ON H.BuildingId = I.Id WHERE G.IsCheckedOut = 0 and B.BonTahananId ='" + Id + "'");
            return dataaccess.Get<GetInmateBontahanan>(sqlStr);
        }

        public InmateBonTahanan GetByInamateRegID(string RegID)
        {
            string sqlStr;
            sqlStr = @"SELECT A.* FROM InmateBonTahanan A WHERE RegID = '" + RegID + "'";
            return dataaccess.Get<InmateBonTahanan>(sqlStr).FirstOrDefault();
        }

        public void DeleteById(int Id)
        {
            dataaccess.DeleteFromDB<InmateBonTahanan>(string.Format("DELETE FROM InmateBonTahanan WHERE Id ='{0}'", Id), null);
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<InmateBonTahanan>("DELETE FROM InmateBonTahanan", null);
        }

        public void DeleteByInmatesRegID(string RegID)
        {
            dataaccess.DeleteFromDB<InmateBonTahanan>(string.Format("DELETE FROM InmateBonTahanan WHERE RegID ='{0}'", RegID), null);
        }
        
        public void DeleteByBonId(int Id)
        {
            dataaccess.DeleteFromDB<InmateBonTahanan>(string.Format("DELETE FROM InmateBonTahanan WHERE BonTahananId ='{0}'", Id), null);
        }

        public int GetCount()
        {
            string sqlStr = @"SELECT Count(0) FROM InmateBonTahanan";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int GetCountByRegID(string RegID)
        {
            string sqlStr = string.Empty;
            sqlStr = @"SELECT Count(0) FROM InmateBonTahanan WHERE RegID = '" + RegID + "'";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int Post(InmateBonTahanan data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO InmateBonTahanan({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<InmateBonTahanan>(strSql);
        }

        public void Update(InmateBonTahanan data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    fieldsValue = String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                    columnName = columnName + String.Format("{0}={1}", item.Name, fieldsValue);

                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            strSql = String.Format("UPDATE InmateBonTahanan SET {0} WHERE Id={1}", columnName, data.Id);
            dataaccess.UpdateToDB<InmateBonTahanan>(strSql);
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
