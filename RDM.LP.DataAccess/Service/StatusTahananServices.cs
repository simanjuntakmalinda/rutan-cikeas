﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class StatusTahananService : IDisposable
    {
        private readonly IRepository dataaccess;

        public StatusTahananService()
        {
            dataaccess = new SqlBaseRepository();
        }

        public IEnumerable<StatusTahanan> Get()
        {
            string sqlStr;
            sqlStr = @"SELECT a.Id, a.Id as StatusId , a.RegId, b.FullName, a.CreatedDate, a.CreatedBy, b.NoSuratPenangkapan, b.NoSuratPenahanan,b.TanggalPenangkapan,b.TanggalPenahanan FROM StatusTahanan a JOIN Inmates b ON a.RegId=b.RegID";
            return dataaccess.Get<StatusTahanan>(sqlStr);
        }

        public IEnumerable<StatusTahanan> GetAll()
        {
            string sqlStr;
            sqlStr = @"SELECT a.Id, a.Id as StatusId , a.RegId, b.FullName, b.CreatedDate, b.CreatedBy, b.NoSuratPenangkapan, b.NoSuratPenahanan,b.TanggalPenangkapan,b.TanggalPenahanan,e.CellNumber as NoSel, e.CellCode FROM StatusTahanan a JOIN Inmates b ON a.RegId=b.RegID left join CellAllocation d on a.RegId = d.InmatesRegCode
left join Cell e on d.CellCode = e.CellCode where d.IsCheckedOut = 0";
            return dataaccess.Get<StatusTahanan>(sqlStr);
        }

        public void DeleteById(int Id)
        {
            dataaccess.DeleteFromDB<StatusTahanan>(string.Format("DELETE FROM StatusTahanan WHERE Id ={0}", Id), null);
            dataaccess.DeleteFromDB<PerpanjanganTahanan>(string.Format("DELETE FROM PerpanjanganTahanan WHERE StatusTahananId = {0}", Id), null);
        }

        public int GetCount()
        {
            string sqlStr = @"SELECT COunt(0) FROM StatusTahanan";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int GetCountByRegId(string regid)
        {
            string sqlStr = @"SELECT Count(0) FROM StatusTahanan where RegId = '"+regid+"'";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public StatusTahanan GetById(int Id)
        {
            string sqlStr;
            sqlStr = @"SELECT a.RegId, b.FullName, b.NoSuratPenangkapan, b.NoSuratPenahanan, 
                        b.TanggalPenangkapan, b.TanggalPenahanan, d.CellNumber as NoSel, b.CreatedDate, b.CreatedBy, b.UpdatedDate, b.UpdatedBy
                        FROM StatusTahanan a JOIN Inmates b ON a.RegId = b.RegID 
                        LEFT JOIN CellAllocation c ON a.RegId = c.InmatesRegCode 
                        LEFT JOIN Cell d ON c.CellCode = d.CellCode WHERE a.Id ="+Id+" and c.IsCheckedOut =0";
            return dataaccess.Get<StatusTahanan>(sqlStr).FirstOrDefault();
        }

        public StatusTahanan GetByRegId(string Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT * FROM StatusTahanan WHERE RegId = '{0}'", Id);
            return dataaccess.Get<StatusTahanan>(sqlStr).FirstOrDefault();
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<StatusTahanan>("DELETE FROM StatusTahanan", null);
        }

        public int Post(StatusTahanan data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO StatusTahanan({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<StatusTahanan>(strSql);
        }

        public void Update(StatusTahanan data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    fieldsValue = String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                    columnName = columnName + String.Format("{0}={1}", item.Name, fieldsValue);

                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            strSql = String.Format("UPDATE StatusTahanan SET {0} WHERE Id={1}", columnName, data.Id);
            dataaccess.UpdateToDB<StatusTahanan>(strSql);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
