﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class LokasiPolisiService : IDisposable
    {
        private readonly IRepository dataaccess;

        public LokasiPolisiService()
        {
            dataaccess = new SqlBaseRepository();
        }

        public IEnumerable<LokasiPolisi> GetPolda()
        {
            string sqlStr;
            sqlStr = @"SELECT [Id]
                                ,[Nama]
                            FROM [LokasiPolisi] WHERE Type =1 and TipeLokasiPolisiId =2";
            return dataaccess.Get<LokasiPolisi>(sqlStr);
        }

        public IEnumerable<LokasiPolisi> GetPolresByRootId(int rootid)
        {
            string sqlStr;
            sqlStr = @"SELECT [Id]
                                ,[Nama]
                            FROM [LokasiPolisi] WHERE Type =1 and TipeLokasiPolisiId =3 and RootId =" +rootid;
            return dataaccess.Get<LokasiPolisi>(sqlStr);
        }

        public IEnumerable<LokasiPolisi> GetPolsekByRootId(int rootid)
        {
            string sqlStr;
            sqlStr = @"SELECT [Id]
                                ,[Nama]
                            FROM [LokasiPolisi] WHERE Type =1 and TipeLokasiPolisiId =4 and RootId =" + rootid;
            return dataaccess.Get<LokasiPolisi>(sqlStr);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
