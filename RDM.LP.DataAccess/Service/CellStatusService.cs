﻿using RDM.LP.DataAccess.Interface;
using RDM.LP.DataAccess.Model;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDM.LP.DataAccess.Service
{
    public class CellStatusService : IDisposable
    {
        private readonly IRepository dataaccess;

        public CellStatusService()
        {
            dataaccess = new SqlBaseRepository();
        }

        public IEnumerable<CellStatus> Get()
        {
            string sqlStr;
            sqlStr = @"SELECT * FROM CellStatus ORDER BY CreatedDate ASC";
            return dataaccess.Get<CellStatus>(sqlStr);
        }

        public CellStatus GetById(int Id)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT * FROM CellStatus WHERE Id = {0}", Id);
            return dataaccess.Get<CellStatus>(sqlStr).FirstOrDefault();
        }

        public CellStatus GetByStatusName(string status)
        {
            string sqlStr;
            sqlStr = string.Format("SELECT * FROM CellStatus WHERE Status ='{0}'", status);
            return dataaccess.Get<CellStatus>(sqlStr).FirstOrDefault();
        }

        public void DeleteAll()
        {
            dataaccess.DeleteFromDB<CellStatus>("DELETE FROM CellStatus", null);
        }

        public void DeleteByStatus(string status)
        {
            dataaccess.DeleteFromDB<CellStatus>(string.Format("DELETE FROM CellStatus WHERE Status ='{0}'", status), null);
        }

        public int GetCount()
        {
            string sqlStr = @"SELECT Count(0) FROM CellStatus";
            return dataaccess.Get<int>(sqlStr).ElementAtOrDefault(0);
        }

        public int Post(CellStatus data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    columnName = columnName + String.Format("{0},", item.Name);
                    fieldsValue = fieldsValue + String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            fieldsValue = fieldsValue.Remove(fieldsValue.Length - 1);
            strSql = String.Format("INSERT INTO CellStatus({0}) OUTPUT INSERTED.ID VALUES({1})", columnName, fieldsValue);
            return dataaccess.InsertIntoDB<CellStatus>(strSql);
        }

        public void Update(CellStatus data)
        {
            var fieldsValue = string.Empty;
            var columnName = string.Empty;
            var typeSign = string.Empty;
            var strSql = string.Empty;
            foreach (var item in data.GetType().GetProperties())
            {
                if (item.GetValue(data, null) != null && item.GetValue(data, null).GetType().FullName.Contains("CaseManagement") == false && item.Name != "Id")
                {
                    switch (item.PropertyType.Name)
                    {
                        case "Int16":
                        case "Int32":
                        case "Int64":
                        case "byte":
                        case "Byte":
                        case "long":
                        case "decimal":
                        case "Decimal":
                        case "double":
                        case "Double":
                        case "Boolean":
                        case "bool":
                            typeSign = string.Empty;
                            break;
                        default:
                            typeSign = "'";
                            break;
                    }
                    fieldsValue = String.Format(typeSign + "{0}" + typeSign + ",", item.GetValue(data, null).ToString().Replace("False", "0").Replace("True", "1").Replace("'", "`"));
                    columnName = columnName + String.Format("{0}={1}", item.Name, fieldsValue);

                }
            }
            columnName = columnName.Remove(columnName.Length - 1);
            strSql = String.Format("UPDATE CellStatus SET {0} WHERE Id={1}", columnName, data.Id);
            dataaccess.UpdateToDB<CellStatus>(strSql);
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    dataaccess.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
