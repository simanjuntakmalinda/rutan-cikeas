﻿using PdfiumViewer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace RDM.LP.Desktop.Presentation
{
    public partial class Help : Telerik.WinControls.UI.RadForm
    {
        public Help()
        {
            InitializeComponent();
            this.Text = String.Format("Panduan {0}", Application.ProductName);
            //this.lblBantuan.Text = String.Format("Panduan {0}", Application.ProductName);
            //this.pdfRenderer.Load(PdfDocument.Load(Application.StartupPath + "\\Help\\usermanuallp.pdf"));
        }
    }
}
