﻿using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.Primitives;

namespace RDM.LP.Desktop.Presentation
{
    public partial class About : Telerik.WinControls.UI.RadForm
    {
        public About()
        {
            InitializeComponent();
            TextPrimitive primitive = (TextPrimitive)lblAbout.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 20, FontStyle.Bold);
            primitive.ForeColor = Color.Gold;
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(4, 4), Color.Black);
            lblAbout.Text = Application.ProductName.ToUpper();

            primitive = (TextPrimitive)lblVersion.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font("Verdana", 8, FontStyle.Bold);
            primitive.ForeColor = Color.LightYellow;
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 1), Color.Black);
            lblVersion.Text = string.Format("Version {0}", Application.ProductVersion);

            this.Text = String.Format("{0}", Application.ProductName);
            lblPoweredBy.Text = "Powered By:";
        }
    }
}
