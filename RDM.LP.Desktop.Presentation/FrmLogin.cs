﻿using RDM.LP.DataAccess.General;
using System;
using System.Windows.Forms;
using Telerik.WinControls;
using RDM.LP.DataAccess.ViewModel;
using System.Drawing;
using System.Threading;
using System.IO;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.Primitives;
using Telerik.WinControls.UI.Localization;

namespace RDM.LP.Desktop.Presentation
{
    public partial class FrmLogin : Telerik.WinControls.UI.RadForm
    {
        public FrmLogin()
        {
            ThemeResolutionService.ApplicationThemeName = null;
            InitializeComponent();
            //InitLocalization();
            InitForm();
            chkIngat.CheckStateChanged += chkIngat_CheckedChanged;
        }

        private void InitLocalization()
        {
            RadMessageLocalizationProvider.CurrentProvider = new RDMRadMessageLocalizationProvider();
            RadGridLocalizationProvider.CurrentProvider = new RDMGridViewLocalizationProvider();
        }

        private void chkIngat_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIngat.Checked)
                MsgBox("Info", "Untuk selanjutnya, anda akan melewati proses login untuk masuk ke : " + Application.ProductName,TipeMsg.Info);
        }


        #region Events

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            ThemeResolutionService.ApplicationThemeName = "";
            this.CenterToScreen();
        }

        private void chktampilan_CheckStateChanged(object sender, EventArgs e)
        {
            this.UsertxtPassword.PasswordChar = this.chktampilan.Checked ? char.MinValue : '*';
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (validateOK()) { 
                ThreadStart loginThreadStart = new ThreadStart(Login);
                Thread logThread = new Thread(loginThreadStart);
                logThread.Start();
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (validateOK())
                {
                    ThreadStart threadStart = new ThreadStart(Login);
                    Thread thread = new Thread(threadStart);
                    thread.SetApartmentState(ApartmentState.STA);
                    thread.Start();
                }
            }
        }

        #endregion

        #region Functions

        private void InitForm()
        {
            ThemeResolutionService.ApplicationThemeName = "Material";
            TextPrimitive primitive = (TextPrimitive)lblTitle.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 21, FontStyle.Bold);
            primitive.ForeColor = Color.Gold;
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(4, 4), Color.Black);
            lblTitle.Text = Application.ProductName.ToUpper();

            primitive = (TextPrimitive)lblVersion.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font("Verdana", 8, FontStyle.Bold);
            primitive.ForeColor = Color.LightYellow;
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 1), Color.Black);

			Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;

			//lblVersion.Text = string.Format("Version {0}", Application.ProductVersion);
			lblVersion.Text = string.Format("Version {0}.{1}.build-{2}.rev-{3}", version.Major, version.Minor, version.Build, version.Revision);

			primitive = (TextPrimitive)lblUser.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 11, FontStyle.Bold);
            primitive.ForeColor = Color.White;
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 2), Color.Black);

            primitive = (TextPrimitive)lblPwd.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 11, FontStyle.Bold);
            primitive.ForeColor = Color.White;
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 2), Color.Black);

            //this.Size = new Size(670, 370);
            this.Text = string.Format("{0} :: Login", Application.ProductName, Application.ProductVersion);
            this.radWaitingBar1.Visible = false;
            this.radWaitingBar1.RootElement.EnableElementShadow = false;
            this.radWaitingBar1.WaitingBarElement.SeparatorElement.BackColor = Color.FromArgb(140, 25, 25);
            this.radWaitingBar1.WaitingBarElement.SeparatorElement.BackColor2 = Color.FromArgb(140, 25, 25);
            this.radWaitingBar1.WaitingBarElement.SeparatorElement.BackColor3 = Color.FromArgb(140, 25, 25);
            this.radWaitingBar1.WaitingBarElement.SeparatorElement.BackColor4 = Color.FromArgb(140, 25, 25);
            this.radWaitingBar1.StartWaiting();
            this.toolTip1.SetToolTip(this.chkIngat, "Check this option to remember the login process.\nLogin process will be skip for the next login time");
            //this.txtPassword.Text = "superuser123";
            //UsertxtUserName.Text = UserFunction.GetSettings(SaveSettingsType.UserID);
            //UsertxtUserName.CharacterCasing = CharacterCasing.Normal;
        }

        private bool validateOK()
        {
            if (this.UsertxtUserName.Text.Trim() == string.Empty)
            {
                this.UsertxtUserName.Focus();
                MsgBox("Login Failed", "Please Enter Your UserName !", TipeMsg.Error);
                return false;
            }

            if (this.UsertxtPassword.Text.Trim() == string.Empty)
            {
                this.UsertxtPassword.Focus();
                MsgBox("Login Failed", "Please Enter Your Password !", TipeMsg.Error);
                return false;
            }
            return true;
        }

        private void Login()
        {
            Application.DoEvents();
            btnLogin.Invoke((MethodInvoker)delegate {
                radWaitingBar1.Visible = true;
            });

            Application.DoEvents();
            string uName = UsertxtUserName.Text;
            string passwd = UsertxtPassword.Text;
            bool isLoggedIn = chkIngat.Checked;

            Application.DoEvents();
            using (var loginx = new UserIdentityHelper())
            {
                Application.DoEvents(); 
                AspnetUsers DataUser = null;
                try
                {
                    bool validKah = loginx.Login(uName, passwd, isLoggedIn, out DataUser);
                    if (validKah)
                    {
                        UserFunction.SetLoggedIn(DataUser);
                        fadingRingWaitingBarIndicatorElement1.Text = String.Format("Checking new version of {0}", Application.ProductName);
                        if (UserFunction.CheckUpdate(btnLogin))
                        {
                            Application.Restart();
                            return;
                        }
                        btnLogin.Invoke((MethodInvoker)delegate
                        {
                            this.UsertxtPassword.Text = String.Empty;
                            GlobalForm.formlogin.Hide();
                            radWaitingBar1.Visible = false;
                            Main main = new Main();
                            main.WindowState = FormWindowState.Maximized;
                            main.Show();
                            radWaitingBar1.Visible = false;
                        });
                    }
                }
                catch (Exception ex)
                {
                    btnLogin.Invoke((MethodInvoker)delegate
                    {
                        fadingRingWaitingBarIndicatorElement1.Text = "Please Wait";
                        radWaitingBar1.Visible = false;
                        MsgBox("Error", ex.Message,TipeMsg.Error);
                    });
                }

            }
        }

        private void MsgBox(string title, string bodymsg, TipeMsg type )
        {
            RadMessageBox.SetThemeName("Material");
            switch (type)
            {
                case TipeMsg.Info:
                    RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, bodymsg, title, MessageBoxButtons.OK, RadMessageIcon.Info);
                    break;
                default:
                    RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, bodymsg, title, MessageBoxButtons.OK, RadMessageIcon.Error);
                    break;
            }

         }

        #endregion

        private void radScheduler1_Click(object sender, EventArgs e)
        {

        }

        private void chktampilan_CheckStateChanged_1(object sender, EventArgs e)
        {

        }
    }
}