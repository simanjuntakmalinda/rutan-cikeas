﻿using RDM.LP.DataAccess.ViewModel;
using RDM.LP.Desktop.Presentation.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace RDM.LP.Desktop.Presentation.CustomElements
{
    public class OverviewListViewVisualItem : IconListViewVisualItem
    {

        public LightVisualElement InputDate { get { return this.inputDate; } }
        public LightVisualElement DataInfo { get { return this.dataInfo; } }
        

        protected override Type ThemeEffectiveType
        {
            get
            {
                return typeof(IconListViewVisualItem);
            }
        }

        private LightVisualElement dataId = new LightVisualElement();
        private LightVisualElement dataStatus = new LightVisualElement();
        private LightVisualElement dataInfo = new LightVisualElement();
        private LightVisualElement inputDate = new LightVisualElement(); 
        private LightVisualElement additionInfo = new LightVisualElement();
        private LightVisualElement addition2Info = new LightVisualElement();
        private StackLayoutElement verticalContainer = new StackLayoutElement();
        private StackLayoutElement dataHeaderContainer = new StackLayoutElement(); 
        private StackLayoutElement dataFooterContainer = new StackLayoutElement();

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            verticalContainer.Orientation = System.Windows.Forms.Orientation.Vertical;
            verticalContainer.NotifyParentOnMouseInput = true;
            verticalContainer.ShouldHandleMouseInput = false;
            verticalContainer.StretchHorizontally = true;
            //verticalContainer.StretchVertically = true;
            verticalContainer.Size = new Size(250, 120);
            //verticalContainer.ShadowDepth = 3;


            dataHeaderContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            dataHeaderContainer.NotifyParentOnMouseInput = true;
            dataHeaderContainer.ShouldHandleMouseInput = false;
            dataHeaderContainer.Children.Add(dataId);
            dataHeaderContainer.Children.Add(dataStatus);
            dataHeaderContainer.StretchHorizontally = true;

            dataId.NotifyParentOnMouseInput = true;
            dataId.ShouldHandleMouseInput = false;
            dataId.StretchHorizontally = true;
            dataId.CustomFont = Global.MainFont;
            dataId.CustomFontSize = 9;
            dataId.CustomFontStyle = FontStyle.Bold;
            dataId.Margin = new System.Windows.Forms.Padding(5, 10, 0, 0);
            dataId.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
           

            dataStatus.NotifyParentOnMouseInput = true;
            dataStatus.ShouldHandleMouseInput = false;
            dataStatus.StretchHorizontally = false;
            dataStatus.CustomFont = Global.MainFont; 
            dataStatus.CustomFontSize = 9;
            dataStatus.CustomFontStyle = FontStyle.Regular;
            dataStatus.Margin = new System.Windows.Forms.Padding(0,5,5,10);

            dataFooterContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            dataFooterContainer.NotifyParentOnMouseInput = true;
            dataFooterContainer.ShouldHandleMouseInput = false;
            //dataFooterContainer.StretchHorizontally = true;
            dataFooterContainer.DrawFill = true;
            dataFooterContainer.BackColor = Color.White;
            dataFooterContainer.GradientStyle = GradientStyles.Solid;
            dataFooterContainer.MinSize = new Size(251,30);
            dataFooterContainer.Margin = new Padding(-9,0,0,0);
            dataFooterContainer.Padding = new Padding(5);

            dataInfo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            dataInfo.StretchHorizontally = false;
            dataInfo.Layout.LeftPart.Padding = new Padding(20, 0, 8, 0);
            dataInfo.MaxSize = new Size(220, 100);

            dataInfo.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            dataInfo.NotifyParentOnMouseInput = true;
            dataInfo.ShouldHandleMouseInput = false;
            dataInfo.CustomFont = Global.MainFont;
            dataInfo.CustomFontSize = 9;
            dataInfo.CustomFontStyle = FontStyle.Regular;
            

            addition2Info.Text = "";
            inputDate.NotifyParentOnMouseInput = true;
            inputDate.ShouldHandleMouseInput = false;

            inputDate.StretchVertically = true;
            additionInfo.StretchVertically = true;
            addition2Info.StretchVertically = true;
            dataFooterContainer.Children.Add(inputDate);
            //dataFooterContainer.Children.Add(additionInfo);
            //dataFooterContainer.Children.Add(addition2Info); 

            addition2Info.NotifyParentOnMouseInput = true;
            addition2Info.ShouldHandleMouseInput = false;
            addition2Info.StretchHorizontally = false;
            addition2Info.Alignment = ContentAlignment.MiddleRight;
            addition2Info.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;           
            addition2Info.CustomFont = Global.MainFont;
            addition2Info.CustomFontSize = 9;
            addition2Info.CustomFontStyle = FontStyle.Regular;

            additionInfo.NotifyParentOnMouseInput = true;
            additionInfo.ShouldHandleMouseInput = false;
            additionInfo.StretchHorizontally = false;
            additionInfo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            additionInfo.CustomFont = Global.MainFont;
            additionInfo.CustomFontSize = 9;
            additionInfo.CustomFontStyle = FontStyle.Regular;

            inputDate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            inputDate.CustomFont = Global.MainFont;
            inputDate.CustomFontSize = 9;
            inputDate.CustomFontStyle = FontStyle.Regular;
            inputDate.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);    
            inputDate.StretchHorizontally = false; 
             
            verticalContainer.Children.Add(dataHeaderContainer);
            verticalContainer.Children.Add(dataInfo);
            verticalContainer.Children.Add(dataFooterContainer);

            this.Children.Add(this.verticalContainer);


            dataInfo.Click += dataInfo_Click;
        }
        
        protected override void SynchronizeProperties()
        {
                       
        }

        private void dataInfo_Click(object sender, EventArgs e)
        {
        }
    }
}