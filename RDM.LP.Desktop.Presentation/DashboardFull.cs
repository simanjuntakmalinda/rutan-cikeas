﻿using RDM.LP.DataAccess.ViewModel;
using RDM.LP.Desktop.Presentation.CustomControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.Primitives;

namespace RDM.LP.Desktop.Presentation
{
    public partial class DashboardFull : Telerik.WinControls.UI.RadForm
    {
        public Dashboard dashboard { get { return this.dashboard1; } }
        public DashboardFull()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            dashboard1.FullScreen.Click += FullScreen_Click;
        }

        private void FullScreen_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void InitForm()
        {
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            this.BackColor = Color.Black;
            this.ShowInTaskbar = false;
            this.TopMost = true;
            this.dashboard1.NavigatorDashboard.Hide();
            TextPrimitive primitive = (TextPrimitive)lblTitle.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 15, FontStyle.Bold);
            primitive.ForeColor = Color.Yellow;
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(-1, 2), Color.Black);
            lblTitle.Text = Application.ProductName.ToUpper();
            lblVersion.Text = string.Format("{0} {1}", Language.GetLanguageString(LanguangeId.VERSION), Application.ProductVersion);
            lblVersion.ForeColor = Color.White;
            Cursor.Hide();
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                Cursor.Show();
                this.Close();
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }

    }
}
