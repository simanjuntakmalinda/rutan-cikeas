﻿using RDM.LP.DataAccess.General;
using RDM.LP.DataAccess.Helper;
using RDM.LP.DataAccess.Service;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool instanceCountOne = false;
            try
            {
                using (Mutex mtex = new Mutex(true, Application.ProductName, out instanceCountOne))
                {
                    if (instanceCountOne)
                    {
                        var regionFormat = Microsoft.Win32.Registry.GetValue("HKEY_CURRENT_USER\\Control Panel\\International", "sShortDate", "").ToString();
                        if(regionFormat == "M/d/yyyy" || regionFormat == "MM/dd/yyyy")
                        {
                        }
                        else
                        {
                            RadMessageBox.SetThemeName("Material");
                            RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, Application.ProductName + " required regional short date format : M/d/yyyy or MM/dd/yyyy", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
                            return;
                        }

                    InitSettings();
                        Application.EnableVisualStyles();
                        Application.SetCompatibleTextRenderingDefault(false);
                        var formLogin = new FrmLogin();
                        GlobalForm.formlogin = formLogin;


                    //Check Auto Login
                        var password = SimpleRSA.Decrypt(Properties.Settings.Default.DBPassword);
                        var loginx = new UserIdentityHelper(Properties.Settings.Default.DBDataSource, Properties.Settings.Default.DBUserName, password);
                        var userlogin = loginx.GetLoggedIn();
                        if (userlogin != null)
                        {
                            UserFunction.SetLoggedIn(userlogin);
                            Main main = new Main();
                            main.WindowState = FormWindowState.Maximized;
                            Application.Run(main);
                        }
                        else
                        {
                            Application.Run(GlobalForm.formlogin);
                        }
                        mtex.ReleaseMutex();
                    }
                    else
                    {
                        RadMessageBox.SetThemeName("Material");
                        RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, Application.ProductName + " already running...!", "Warning", MessageBoxButtons.OK, RadMessageIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                var d = UserFunction.GetSettings(SaveSettingsType.DBDataSource);
                var u = UserFunction.GetSettings(SaveSettingsType.DBUserName);
                var p = UserFunction.GetSettings(SaveSettingsType.DBPassword);
                UserFunction.MsgBox(TipeMsg.Error, ex.Message);
                Environment.Exit(0);
            }


            void InitSettings()
            {
                String[] dataparams = new string[3];
                using (StreamReader sr = new StreamReader("dbinit.rdm"))
                {
                    for (int i = 0; i <= 2; ++i)
                    {
                        dataparams[i] = sr.ReadLine();
                    }
                }

                if (UserFunction.GetSettings(SaveSettingsType.DashboardRefresinterval) == "0")
                {
                    UserFunction.SaveSettings(SaveSettingsType.DashboardRefresinterval, 20);
                }
                if (UserFunction.GetSettings(SaveSettingsType.DBDataSource) == string.Empty)
                {
                    UserFunction.SaveSettings(SaveSettingsType.DBDataSource, dataparams[0]);
                }
                if (UserFunction.GetSettings(SaveSettingsType.DBUserName) == string.Empty)
                {
                    UserFunction.SaveSettings(SaveSettingsType.DBUserName, dataparams[1]);
                   
                }
                if (UserFunction.GetSettings(SaveSettingsType.DBPassword) == string.Empty)
                {
                    UserFunction.SaveSettings(SaveSettingsType.DBPassword, dataparams[2]);

                    ParamsService param = new ParamsService(dataparams[0], dataparams[1], SimpleRSA.Decrypt(dataparams[2]));
                    param.DeleteAll();
                    param.Post(new Params { ParamName = SaveSettingsType.DBDataSource.ToString(), ParamValue = dataparams[0] });
                    param.Post(new Params { ParamName = SaveSettingsType.DBUserName.ToString(), ParamValue = dataparams[1] });
                    param.Post(new Params { ParamName = SaveSettingsType.DBPassword.ToString(), ParamValue = dataparams[2] });
                }

                if (UserFunction.GetSettings(SaveSettingsType.CellCodeSeperator1) == string.Empty)
                {
                    UserFunction.SaveSettings(SaveSettingsType.CellCodeSeperator1, "-");
                }
                if (UserFunction.GetSettings(SaveSettingsType.CellCodeSeperator2) == string.Empty)
                {
                    UserFunction.SaveSettings(SaveSettingsType.CellCodeSeperator2, ".");
                }
                if (UserFunction.GetSettings(SaveSettingsType.GridPagingSize) == "0")
                {
                    UserFunction.SaveSettings(SaveSettingsType.GridPagingSize, "20");
                }

                //if (File.Exists("dbinit.rdm"))
                //{
                //    File.Delete("dbinit.rdm");
                //}

            }
        }
    }
}
