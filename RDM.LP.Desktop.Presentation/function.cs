﻿using RDM.LP.DataAccess.Service;
using RDM.LP.DataAccess.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Newtonsoft.Json;
using System.Net.NetworkInformation;
using System.Threading;
using System.ComponentModel;
using System.Drawing;
using Telerik.WinControls.Primitives;
using RDM.LP.Desktop.Presentation.Properties;
using Telerik.WinControls.UI.Localization;
using Telerik.WinControls.UI.Export;
using Telerik.WinControls.Zip;
using System.Security.Principal;
using System.Security.AccessControl;
using System.Drawing.Imaging;
using RDM.LP.DataAccess.Helper;

namespace RDM.LP.Desktop.Presentation
{

    public class GlobalVariables
    {
        public static string lpSelected;
        public static string SprindikSelected;
        public static int CaseIdSelected;
        public static int ListBerkasIdSelected;
        public static long LastInsertRowId;
        public static long LokasiPolisi;
        public static string UserID;
        public static string ApiToken;

        public static int PanelAktif = 0;
        public static int RegisterCount = -1;
        public static bool isVisitor;
        public static byte[][] RegTmps = new byte[10][];
        public static List<int> FingerPrintID = new List<int>();
        public static List<string> RegisterList = new List<string>();
        public static List<string> FPImageList = new List<string>();
        public const int REGISTER_FINGER_COUNT = 10;
    }

    public class GlobalForm
    {
        public static FrmLogin formlogin;
    }

    public class GlobalFunction
    {

        public static void InvisibleControlDetail(TableLayoutPanel tableLayoutPanel, int row)
        {
            foreach (Control ctl in tableLayoutPanel.Controls)
                if (tableLayoutPanel.GetRow(ctl) == row) ctl.Visible = false;
        }

        public static void VisibleControlDetail(TableLayoutPanel tableLayoutPanel, int row)
        {
            foreach (Control ctl in tableLayoutPanel.Controls)
                if (tableLayoutPanel.GetRow(ctl) == row) ctl.Visible = true;
        }
    }

    public class ZonaWaktu
    {
        public string Nama
        {
            get;
            set;
        }
        public int Id
        {
            get;
            set;
        }
        public ZonaWaktu(int r, string n)
        {
            Id = r;
            Nama = n;
        }
    }

    public class MainForm
    {
        public static Main formMain;
    }



    public enum LanguangeId
    {
        //MENU
        ADD_NEW_VISIT,
        ADD_NEW_VISITOR,
        ADD_NEW_VISITOR_VIP,
        ADD_NEW_INMATES,
        ADD_NEW_USER,
        ADD_NEW_CELL,
        ADD_NEW_KENDARAAN,
        ADD_NEW_ALLOCATION,
        SETTINGS,
        LOGOUT_FROM_SYSTEM,
        CHANGE_PASSWORD,
        WELCOME,
        LAST_LOGIN,
        VERSION,
        ADD_NEW_ASSETS,
        ADD_NEW_BON_TAHANAN,
        ADD_NEW_BON_SERAH_TAHANAN,
        ADD_NEW_BON_TERIMA_TAHANAN,
        ADD_NEW_JADWAL_KUNJUNGAN,
        ADD_NEW_STATUS_TAHANAN
    }

    

    public enum SearchType
    {
        Tahanan,
        Visitor,
        REG_NO,
        NAME,
        ID
    }

    public enum SaveSettingsType
    {
        View,
        UserID,
        NoHP,
        LastLogin,
        DBUserName,
        DBDataSource,
        DBPassword,
        DashboardRefresinterval,
        CellCodeSeperator1,
        CellCodeSeperator2,
        GridPagingSize,
        RawDataPassword
    }

    public enum ExportGridType
    {
        PDF,
        CSV
    }

    public enum TipeMsg
    {
        Gagal,
        Error,
        Info,
        Konfirmasi
    }

    public enum ModulName
    {
        LOGIN,
        MASTERCELL,
        MASTERUSER,
        MASTERKENDARAAN,
        MASTERVISITOR,
        MASTERNETWORK,
        VISITORVISIT,
        VISITORCHECKOUT,
        VISITORPHOTO,
        VISITORFINGERPRINT,
        MASTERINMATES,
        INMATESPHOTO,
        INMATESFINGERPRINT,
        CELLALLOCATION,
        KENDARAANTAHANAN,
        BONTAHANAN,
        STATUSTAHANAN
    }

    public class UserFunction
    {
        private static string newVersion;

        public static bool IsNumeric(string s)
        {
            long output;
            return long.TryParse(s, out output);
        }

        public static void GeneratePdf(string tipe, string RegID)
        {
            var sourcetemplate = string.Empty;
            var generatedtemplate = string.Empty;
            var targettemplate = string.Empty;

            switch (tipe)
            {
                case "Inmates":
                    sourcetemplate = Application.StartupPath + "\\templates\\inmates.html";
                    generatedtemplate = Application.StartupPath + "\\templates\\temp\\inmates.html";
                    targettemplate = Application.StartupPath + "\\templates\\temp\\inmates.pdf";
                    GenerateHTMLInmatesForm(sourcetemplate, generatedtemplate, RegID);
                    break;
                case "Visitor":
                    sourcetemplate = Application.StartupPath + "\\templates\\visitor.html";
                    generatedtemplate = Application.StartupPath + "\\templates\\temp\\visitor.html";
                    targettemplate = Application.StartupPath + "\\templates\\temp\\visitor.pdf";
                    GenerateHTMLVisitorForm(sourcetemplate, generatedtemplate, RegID);
                    break;
				 case "BonTahanan":
                    sourcetemplate = Application.StartupPath + "\\templates\\bontahanan.html";
                    generatedtemplate = Application.StartupPath + "\\templates\\temp\\bontahanan.html";
                    targettemplate = Application.StartupPath + "\\templates\\temp\\bontahanan.pdf";
                    GenerateHTMLBonTahananForm(sourcetemplate, generatedtemplate, RegID);
                    break;
                case "BonSerahTahanan":
                    sourcetemplate = Application.StartupPath + "\\templates\\bonserahtahanan.html";
                    generatedtemplate = Application.StartupPath + "\\templates\\temp\\bonserahtahanan.html";
                    targettemplate = Application.StartupPath + "\\templates\\temp\\bonserahtahanan.pdf";
                    GenerateHTMLBonSerahTahananForm(sourcetemplate, generatedtemplate, RegID);
                    break;
                case "BonTerimaTahanan":
                    sourcetemplate = Application.StartupPath + "\\templates\\bonterimatahanan.html";
                    generatedtemplate = Application.StartupPath + "\\templates\\temp\\bonterimatahanan.html";
                    targettemplate = Application.StartupPath + "\\templates\\temp\\bonterimatahanan.pdf";
                    GenerateHTMLBonTerimaTahananForm(sourcetemplate, generatedtemplate, RegID);
                    break;
            }



            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = Application.StartupPath + "\\templates\\wkhtmltopdf.exe";
            startInfo.Arguments = string.Format(" -B 20mm --minimum-font-size 12 --zoom 1.2 \"{0}\" \"{1}\"", generatedtemplate, targettemplate);
            process.StartInfo.UseShellExecute = false;
            process.StartInfo = startInfo;
            process.Start();
            Thread.Sleep(2000);
        }

        private static void GenerateHTMLInmatesForm(string source, string generatedtemplate, string RegID)
        {
            InmatesService inmateserv = new InmatesService();
            CellAllocationService allocationserv = new CellAllocationService();
            CasesService caseserv = new CasesService();
            PhotosService photoserv = new PhotosService();
            FingerPrintService fingerserv = new FingerPrintService();
            SeizedAssetsService bbserv = new SeizedAssetsService();


            var data = inmateserv.GetDetailByRegId(RegID);
            var datacell = allocationserv.GetByInamateRegCode(RegID);
            var datacase = caseserv.GetInmatesCase(RegID);
            int a = caseserv.GetCount();
            var datafoto = photoserv.GetByRegID(RegID);
            var datafinger = fingerserv.GetAllByRegID(RegID);
            var databb = bbserv.GetAllByRegID(RegID);

            if (File.Exists(generatedtemplate))
                File.Delete(generatedtemplate);

            File.Copy(source, generatedtemplate, true);

            //Kasus
            var cases = "<ul>";
            foreach (var item in datacase)
            {
                cases = cases + "<span class='profilebold'><li>" + item.CaseName ?? item.CaseName.ToUpper() + " / " + data.Category ?? data.Category.ToUpper() + "</li></span>";
            }
            cases = cases + "</ul>";

            //Barang Bukti
            var bb = string.Empty;
            foreach (var item in databb)
            {
                bb = bb + "<tr><td>" + item.CaseName + "</td><td>" + item.Detail + "</td><td>" + item.Quantity + "</td><td>" + item.Note + "</td></tr>";
            }

            //Photo
            foreach (var item in datafoto)
            {
                PictureBox picbox = new PictureBox();
                var imageBytes = (byte[])Convert.FromBase64String(item.Data);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);
                picbox.Image = image;

                switch (item.Note)
                {
                    case "FRONT":
                        picbox.Image.Save(Application.StartupPath + "\\templates\\temp\\inmate1.png", ImageFormat.Png);
                        break;
                    case "LEFT":
                        picbox.Image.Save(Application.StartupPath + "\\templates\\temp\\inmate2.png", ImageFormat.Png);
                        break;
                    case "RIGHT":
                        picbox.Image.Save(Application.StartupPath + "\\templates\\temp\\inmate3.png", ImageFormat.Png);
                        break;
                }
            }


            //Finger
            foreach (var item in datafinger)
            {
                PictureBox picbox = new PictureBox();
                var imageBytes = (byte[])Convert.FromBase64String(item.ImagePrint);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);
                picbox.Image = image;

                switch (item.FingerId)
                {
                    case "R1":
                        picbox.Image.Save(Application.StartupPath + "\\templates\\temp\\R1.png", ImageFormat.Png);
                        break;
                    case "R2":
                        picbox.Image.Save(Application.StartupPath + "\\templates\\temp\\R2.png", ImageFormat.Png);
                        break;
                    case "R3":
                        picbox.Image.Save(Application.StartupPath + "\\templates\\temp\\R3.png", ImageFormat.Png);
                        break;
                    case "R4":
                        picbox.Image.Save(Application.StartupPath + "\\templates\\temp\\R4.png", ImageFormat.Png);
                        break;
                    case "R5":
                        picbox.Image.Save(Application.StartupPath + "\\templates\\temp\\R5.png", ImageFormat.Png);
                        break;
                    case "L1":
                        picbox.Image.Save(Application.StartupPath + "\\templates\\temp\\L1.png", ImageFormat.Png);
                        break;
                    case "L2":
                        picbox.Image.Save(Application.StartupPath + "\\templates\\temp\\L2.png", ImageFormat.Png);
                        break;
                    case "L3":
                        picbox.Image.Save(Application.StartupPath + "\\templates\\temp\\L3.png", ImageFormat.Png);
                        break;
                    case "L4":
                        picbox.Image.Save(Application.StartupPath + "\\templates\\temp\\L4.png", ImageFormat.Png);
                        break;
                    case "L5":
                        picbox.Image.Save(Application.StartupPath + "\\templates\\temp\\L5.png", ImageFormat.Png);
                        break;
                }
            }


            string str = string.Empty;
            str = File.ReadAllText(generatedtemplate);
            str = str.Replace("{RegID}", data.RegID);
            str = str.Replace("{FullName}", data.FullName);
            str = str.Replace("{IDType}", data.IDType);
            str = str.Replace("{IDNo}", data.IDNo);
            str = str.Replace("{PlaceOfBirth}", data.PlaceOfBirth);
            str = str.Replace("{DateOfBirth}", data.DateOfBirth.ToString("dd MMMM yyyy").ToUpper());
            str = str.Replace("{Nationality}", data.Nationality);
            str = str.Replace("{Religion}", data.Religion);
            str = str.Replace("{Gender}", data.Gender);
            str = str.Replace("{BeratBadan}", data.BeratBadan);
            str = str.Replace("{TinggiBadan}", data.TinggiBadan);
            str = str.Replace("{MaritalStatus}", data.MaritalStatus);
            str = str.Replace("{Vocation}", data.Vocation);
            str = str.Replace("{Address}", data.Address);
            str = str.Replace("{NoSuratPenahanan}", data.NoSuratPenahanan);
            str = str.Replace("{DateReg}", data.DateReg.Value.ToString("dd MMM yyyy").ToUpper());
            str = str.Replace("{CellCode}", datacell?.CellCode);
            str = str.Replace("{Cases}", cases);
            str = str.Replace("{SeizedAssets}", bb);
            str = str.Replace("{inmatephoto-1}", "inmate1.png");
            str = str.Replace("{inmatephoto-2}", "inmate2.png");
            str = str.Replace("{inmatephoto-3}", "inmate3.png");

            str = str.Replace("{R1}", "R1.png");
            str = str.Replace("{R2}", "R2.png");
            str = str.Replace("{R3}", "R3.png");
            str = str.Replace("{R4}", "R4.png");
            str = str.Replace("{R5}", "R5.png");
            str = str.Replace("{L1}", "L1.png");
            str = str.Replace("{L2}", "L2.png");
            str = str.Replace("{L3}", "L3.png");
            str = str.Replace("{L4}", "L4.png");
            str = str.Replace("{L5}", "L5.png");
            File.WriteAllText(generatedtemplate, str);
        }

        private static void GenerateHTMLVisitorForm(string source, string generatedtemplate, string RegID)
        {
            VisitorService visitorserv = new VisitorService();
            VisitService visitserv = new VisitService();
            PhotosService photoserv = new PhotosService();
            FingerPrintService fingerserv = new FingerPrintService();

            var data = visitorserv.GetDetailByRegID(RegID);
            var datavisit = visitserv.GetDetailByRegID(RegID);
            var datafoto = photoserv.GetByRegID(RegID);
            var datafinger = fingerserv.GetAllByRegID(RegID);
            var datahistory = visitserv.GetDetailVisitByRegID(RegID).Where(x => x.Id != datavisit.Id);

            if (File.Exists(generatedtemplate))
                File.Delete(generatedtemplate);

            File.Copy(source, generatedtemplate, true);

            //Photo
            foreach (var item in datafoto)
            {
                PictureBox picbox = new PictureBox();
                var imageBytes = (byte[])Convert.FromBase64String(item.Data);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);
                picbox.Image = image;
                picbox.Image.Save(Application.StartupPath + "\\templates\\temp\\visitor.png", ImageFormat.Png);
            }


            //Finger
            foreach (var item in datafinger)
            {
                PictureBox picbox = new PictureBox();
                var imageBytes = (byte[])Convert.FromBase64String(item.ImagePrint);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);
                picbox.Image = image;
                picbox.Image.Save(Application.StartupPath + "\\templates\\temp\\VR1.png", ImageFormat.Png);

            }

            var history = string.Empty;
            foreach (var item in datahistory)
            {
                history = history + "<tr><td>" + item.StartVisit.Value.ToString("dd MMMM yyyy") + "</td><td>" + item.InmatesName + "</td><td>" + item.BringingItems + "</td></tr>";
            }

            string str = string.Empty;
            str = File.ReadAllText(generatedtemplate);
            str = str.Replace("{RegID}", data.RegID);
            str = str.Replace("{FullName}", data.FullName);
            str = str.Replace("{IDType}", data.IDType);
            str = str.Replace("{IDNo}", data.IDNo);
            str = str.Replace("{PlaceOfBirth}", data.PlaceOfBirth);
            str = str.Replace("{DateOfBirth}", data.DateOfBirth.ToString("dd MMMM yyyy").ToUpper());
            str = str.Replace("{Nationality}", data.Nationality);
            str = str.Replace("{Religion}", data.Religion);
            str = str.Replace("{Gender}", data.Gender);
            str = str.Replace("{MaritalStatus}", data.MaritalStatus);
            str = str.Replace("{Vocation}", data.Vocation);
            str = str.Replace("{Address}", data.Address);
            str = str.Replace("{DateReg}", data.DateReg.ToString("dd MMM yyyy").ToUpper());
            str = str.Replace("{Type}", datavisit.Type);
            str = str.Replace("{BringingItems}", datavisit.BringingItems);
            str = str.Replace("{Notes}", datavisit.Notes);
            str = str.Replace("{ListVisit}", history);
            str = str.Replace("{visitorphoto}", "visitor.png");
            str = str.Replace("{VR1}", "VR1.png");
            File.WriteAllText(generatedtemplate, str);
        }
        
        private static void GenerateHTMLBonTahananForm(string source, string generatedtemplate, string Id)
        {
            BonTahananService bonserv = new BonTahananService();
            var data = bonserv.GetById(Convert.ToInt32(Id));

            InmateBonTahananService iboserv = new InmateBonTahananService();
            var datainmate = iboserv.GetByBonTahananId(Convert.ToInt32(Id));

            // Inmate
            int count = 0;
            var tahanan = string.Empty;
            var margin = 0;
            foreach (var item in datainmate)
            {
                count++;
                margin = margin - 25 - (count*2);

                CasesService caseserv = new CasesService();
                var datacase = caseserv.GetInmatesCase(item.RegID);

                //Kasus
                var cases = string.Empty;
                foreach (var itemx in datacase)
                {
                    cases = cases + itemx.CaseName ?? itemx.CaseName.ToUpper() + ", ";
                    cases = cases + ", ";
                }

                if (datacase.Count() > 0)
                {
                    cases = cases.Substring(0, cases.Length - 2);
                }

                tahanan = tahanan +
                    "<tr align='left' valign='top'><td width='5%'>" + count + ".</td>" +
                    "<td width='35%'>NAMA</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.FullName.ToUpper() + "</td></tr>" +

                    "<tr align='left' valign='top'><td width='5%'></td>" +
                    "<td width='35%'>NO REGISTER</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.RegID.ToUpper() + "</td></tr>" +

                    "<tr align='left' valign='top'><td width='5%'></td>" +
                    "<td width='35%'>TANGGAL REGISTRASI</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.DateReg.Value.ToString("dd MMM yyyy").ToUpper() + "</td></tr>" +

                    "<tr align='left' valign='top'><td width='5%'></td>" +
                    "<td width='35%'>IDENTITAS</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.IDNo.ToUpper() + " (" + item.IDType + ")</td></tr>" +

                    "<tr align='left' valign='top'><td width='5%'></td>" +
                    "<td width='35%'>TEMPAT / TANGGAL LAHIR</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.PlaceOfBirth + " / " + item.DateOfBirth.ToString("dd MMM yyyy").ToUpper() + "</td></tr>" +


                    "<tr align='left' valign='top'><td width='5%'></td>" +
                    "<td width='35%'>STATUS</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.Nationality + " / " + item.Religion + " / " + item.Gender + " / " + item.MaritalStatus + "</td></tr>" +


                    "<tr align='left' valign='top'><td width='5%'></td>" +
                    "<td width='35%'>PEKERJAAN</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.Vocation + "</td></tr>" +


                    "<tr align='left' valign='top'><td width='5%'></td>" +
                    "<td width='35%'>ALAMAT</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.Address + "</td></tr>" +


                    "<tr align='left' valign='top'><td width='5%'></td>" +
                    "<td width='35%'>BLOK / NO SEL</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.BuildingName.ToUpper() + " / " + item.CellNo + "</td></tr>" +


                    "<tr align='left' valign='top'><td width='5%'></td>" +
                    "<td width='35%'>KASUS</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + cases + "</td></tr><br>";

            }

            if (File.Exists(generatedtemplate))
                File.Delete(generatedtemplate);

            File.Copy(source, generatedtemplate, true);

            string str = string.Empty;
            str = File.ReadAllText(generatedtemplate);

            str = str.Replace("{margin}", margin.ToString() + "px");

            string imagelocation = Application.StartupPath + "\\Resources\\logo_bareskrim.jpg";
            str = str.Replace("{logo}", "<p><img src = '"+ imagelocation  + "' height = '100px' width = '100px'></p>");

            str = str.Replace("{Tahanan}", tahanan);
            str = str.Replace("{NoSurat}", data.NoBonTahanan);
            str = str.Replace("{Keperluan}", data.NamaKeperluan);
            str = str.Replace("{Lokasi}", data.Lokasi);
            str = str.Replace("{Durasi}", data.TanggalMulai.ToString("dd MMMM yyyy").ToUpper() + " S.D. " + data.TanggalSelesai.ToString("dd MMMM yyyy").ToUpper());
            str = str.Replace("{Keterangan}", data.Keterangan);

            if (data.NamaPenanggungJawab != String.Empty)
            {
                str = str.Replace("{NrpPenanggungJawab}", data.NrpPenanggungJawab);
                str = str.Replace("{NamaPenanggungJawab}", data.NamaPenanggungJawab);
                str = str.Replace("{PangkatPenanggungJawab}", data.NamaPangkatPenanggungJawab);
                str = str.Replace("{SatkerPenanggungJawab}", data.SatkerPenanggungJawab);
                str = str.Replace("{NoHpPenanggungJawab}", data.NoHpPenanggungJawab);
            }
            else
            {
                str = str.Replace("{NrpPenanggungJawab}", "");
                str = str.Replace("{NamaPenanggungJawab}", "");
                str = str.Replace("{PangkatPenanggungJawab}", "");
                str = str.Replace("{SatkerPenanggungJawab}", "");
                str = str.Replace("{NoHpPenanggungJawab}", "");
            }

            //str = str.Replace("{NamaPemohon}", data.NamaPemohon);
            //if(data.TipePemohon == "Jaksa")
            //{
            //    str = str.Replace("{NomorIndukPemohon}", "NIP. " + data.NrpPemohon);
            //}
            //else
            //{
            //    str = str.Replace("{NomorIndukPemohon}", data.NamaPangkatPemohon + " NRP " + data.NrpPemohon);
            //}
            //str = str.Replace("{PangkatPemohon}", data.NamaPangkatPemohon);
            //str = str.Replace("{NrpPemohon}", data.NrpPemohon);
            //str = str.Replace("{SatkerPemohon}", data.SatkerPemohon);
            //str = str.Replace("{NoHpPemohon}", data.NoHpPemohon);

            //str = str.Replace("{NrpPenyetuju}", data.NrpPenyetuju);
            //str = str.Replace("{NamaPenyetuju}", data.NamaPenyetuju);
            //str = str.Replace("{PangkatPenyetuju}", data.NamaPangkatPenyetuju);
            //str = str.Replace("{SatkerPenyetuju}", data.SatkerPenyetuju);

            string imageTtdLocation = Application.StartupPath + "\\Resources\\ttdJaksa.jpg";
            var ttd = string.Empty;
            if (data.TipePemohon == "Jaksa")
            {
                ttd = "<tr>" +
                         "<td>Jaksa<br><p><img src = '" + imageTtdLocation + "' height = '110px' width = '275px'></p>" +
                         "<td>a.n KEPALA RUMAH TAHANAN<br>DENSUS KHUSUS 88 ANTI TEROR<br><br><br><br><br></td>" +
                      "</tr>" +
                      "<tr>" +
                         "<td><span class='assignmentbold '>"+ data.NamaPemohon + "</span></td>" +
                         "<td><span class='assignmentbold'>"+ data.NamaPenyetuju + "</span></td>" +
                      "</tr>" +
                      "<tr>" +
                         "<td><span class='assignmentbold overline'>NIP. " + data.NrpPemohon + "</span></td>" +
                         "<td><span class='assignmentbold overline'>"+ data.NamaPangkatPenyetuju + " NRP "+ data.NrpPenyetuju + "</span></td>" +
                      "</tr>";
            }
            else
            {
                //if (data.NamaPenanggungJawab == String.Empty)
                //{
                    ttd = "<tr>" +
                         "<td>Pemohon<br><br><br><br><br><br></td>" +
                         "<td>a.n KEPALA RUMAH TAHANAN<br>DENSUS KHUSUS 88 ANTI TEROR<br><br><br><br><br></td>" +
                      "</tr>" +
                      "<tr>" +
                         "<td><span class='assignmentbold '>" + data.NamaPemohon + "</span></td>" +
                         "<td><span class='assignmentbold'>" + data.NamaPenyetuju + "</span></td>" +
                      "</tr>" +
                      "<tr>" +
                         "<td><span class='assignmentbold overline'>"+ data.NamaPangkatPemohon + " NRP " + data.NrpPemohon + "</span></td>" +
                         "<td><span class='assignmentbold overline'>" + data.NamaPangkatPenyetuju + " NRP " + data.NrpPenyetuju + "</span></td>" +
                      "</tr>";
                /*}
                else
                {
                    ttd = "<tr>" +
                         "<td>Pemohon<br><br><br><br><br><br></td>" +
                         "<td>Pengawal<br><br><br><br><br><br></td>" +
                         "<td>a.n KEPALA RUMAH TAHANAN<br>DENSUS KHUSUS 88 ANTI TEROR<br><br><br><br><br></td>" +
                      "</tr>" +
                      "<tr>" +
                         "<td><span class='assignmentbold'>" + data.NamaPemohon + "</span></td>" +
                         "<td><span class='assignmentbold'>" + data.NamaPenanggungJawab + "</span></td>" +
                         "<td><span class='assignmentbold'>" + data.NamaPenyetuju + "</span></td>" +
                      "</tr>" +
                      "<tr>" +
                         "<td><span class='assignmentbold overline'>" + data.NamaPangkatPemohon + " NRP " + data.NrpPemohon + "</span></td>" +
                         "<td><span class='assignmentbold overline'>" + data.NamaPangkatPenanggungJawab + " NRP " + data.NrpPenanggungJawab + "</span></td>" +
                         "<td><span class='assignmentbold overline'>" + data.NamaPangkatPenyetuju + " NRP " + data.NrpPenyetuju + "</span></td>" +
                      "</tr>";
                }*/
            }
            str = str.Replace("{tandatangan}", ttd);

            File.WriteAllText(generatedtemplate, str);
        }

        private static void GenerateHTMLBonSerahTahananForm(string source, string generatedtemplate, string Id)
        {
            BonSerahTahananService bonserv = new BonSerahTahananService();
            var data = bonserv.GetById(Convert.ToInt32(Id));

            InmateBonTahananService iboserv = new InmateBonTahananService();
            var datainmate = iboserv.GetByBonTahananId(Convert.ToInt32(data.IdBonTahanan));

            if (File.Exists(generatedtemplate))
                File.Delete(generatedtemplate);

            File.Copy(source, generatedtemplate, true);

            // Inmate
            int count = 0;
            var tahanan = string.Empty;
            foreach (var item in datainmate)
            {
                count++;
                tahanan = tahanan +
                    "<tr align='left' valign='top'>" +
                    "<td width='5%'>" + count + ".</td>" +
                    "<td width='35%'>NO REGISTER</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.RegID.ToUpper() + "</td>" +
                    "</tr>" +

                    "<tr align='left' valign='top'>" +
                    "<td width='5%'></td>" +
                    "<td width='35%'>NAMA</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.FullName.ToUpper() + "</td>" +
                    "</tr><br>" +

                    "<tr align='left' valign='top'>" +
                    "<td width='5%'></td>" +
                    "<td width='35%'>IDENTITAS</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.IDNo.ToUpper() + " (" + item.IDType + ")</td>" +
                    "</tr><br>" +

                    "<tr align='left' valign='top'>" +
                    "<td width='5%'></td>" +
                    "<td width='35%'>BLOK / NO SEL</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.BuildingName.ToUpper() + " / " + item.CellNo + ")</td>" +
                    "</tr><br>";

            }

            string str = string.Empty;
            str = File.ReadAllText(generatedtemplate);
            str = str.Replace("{NoBonTahanan}", data.NoBonTahanan);
            str = str.Replace("{NamaTahanan}", tahanan);
            str = str.Replace("{Waktu}", data.TanggalTerima.ToString("dd MMM yyyy hh:mm").ToUpper());
            str = str.Replace("{NrpYangMenerima}", data?.NrpYangMenerima == null ? "" : data.NrpYangMenerima);
            str = str.Replace("{NamaYangMenerima}", data?.NamaYangMenerima == null ? "" : data.NamaYangMenerima);
            str = str.Replace("{PangkatYangMenerima}", data?.PangkatYangMenerima == null ? "" : data.PangkatYangMenerima);
            str = str.Replace("{NrpYangMenyerahkan}", data?.NrpYangMenyerahkan == null ? "" : data.NrpYangMenyerahkan);
            str = str.Replace("{NamaYangMenyerahkan}", data?.NamaYangMenyerahkan == null ? "" : data.NamaYangMenyerahkan);
            str = str.Replace("{PangkatYangMenyerahkan}", data?.PangkatYangMenyerahkan == null ? "" : data.PangkatYangMenyerahkan);
            str = str.Replace("{NrpYangMenyetujui}", data?.NrpYangMenyetujui == null ? "" : data.NrpYangMenyetujui);
            str = str.Replace("{NamaYangMenyetujui}", data?.NamaYangMenyetujui == null ? "" : data.NamaYangMenyetujui);
            str = str.Replace("{PangkatYangMenyetujui}", data?.PangkatYangMenyetujui == null ? "" : data.PangkatYangMenyetujui);
            File.WriteAllText(generatedtemplate, str);
        }

        private static void GenerateHTMLBonTerimaTahananForm(string source, string generatedtemplate, string Id)
        {
            BonTerimaTahananService bonserv = new BonTerimaTahananService();
            var data = bonserv.GetById(Convert.ToInt32(Id));

            InmateBonTahananService iboserv = new InmateBonTahananService();
            var datainmate = iboserv.GetByBonTahananId(Convert.ToInt32(data.IdBonTahanan));

            if (File.Exists(generatedtemplate))
                File.Delete(generatedtemplate);

            File.Copy(source, generatedtemplate, true);

            // Inmate
            int count = 0;
            var tahanan = string.Empty;
            foreach (var item in datainmate)
            {
                count++;
                tahanan = tahanan +
                    "<tr align='left' valign='top'>" +
                    "<td width='5%'>" + count + ".</td>" +
                    "<td width='35%'>NO REGISTER</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.RegID.ToUpper() + "</td>" +
                    "</tr>" +

                    "<tr align='left' valign='top'>" +
                    "<td width='5%'></td>" +
                    "<td width='35%'>NAMA</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.FullName.ToUpper() + "</td>" +
                    "</tr><br>" +

                    "<tr align='left' valign='top'>" +
                    "<td width='5%'></td>" +
                    "<td width='35%'>IDENTITAS</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.IDNo.ToUpper() + " (" + item.IDType + ")</td>" +
                    "</tr><br>" +

                    "<tr align='left' valign='top'>" +
                    "<td width='5%'></td>" +
                    "<td width='35%'>BLOK / NO SEL</td>" +
                    "<td width='2%'>:</td>" +
                    "<td>" + item.BuildingName.ToUpper() + " / " + item.CellNo + ")</td>" +
                    "</tr><br>";

            }

            string str = string.Empty;
            str = File.ReadAllText(generatedtemplate);
            str = str.Replace("{NoBonTahanan}", data.NoBonTahanan);
            str = str.Replace("{NamaTahanan}", tahanan);
            str = str.Replace("{Waktu}", data.TanggalTerima.ToString("dd MMM yyyy hh:mm").ToUpper());
            str = str.Replace("{NrpYangMenerima}", data?.NrpYangMenerima == null ? "" : data.NrpYangMenerima);
            str = str.Replace("{NamaYangMenerima}", data?.NamaYangMenerima == null ? "" : data.NamaYangMenerima);
            str = str.Replace("{PangkatYangMenerima}", data?.PangkatYangMenerima == null ? "" : data.PangkatYangMenerima);
            str = str.Replace("{NrpYangMenyerahkan}", data?.NrpYangMenyerahkan == null ? "" : data.NrpYangMenyerahkan);
            str = str.Replace("{NamaYangMenyerahkan}", data?.NamaYangMenyerahkan == null ? "" : data.NamaYangMenyerahkan);
            str = str.Replace("{PangkatYangMenyerahkan}", data?.PangkatYangMenyerahkan == null ? "" : data.PangkatYangMenyerahkan);
            str = str.Replace("{NrpYangMenyetujui}", data?.NrpYangMenyetujui == null ? "" : data.NrpYangMenyetujui);
            str = str.Replace("{NamaYangMenyetujui}", data?.NamaYangMenyetujui == null ? "" : data.NamaYangMenyetujui);
            str = str.Replace("{PangkatYangMenyetujui}", data?.PangkatYangMenyetujui == null ? "" : data.PangkatYangMenyetujui);
            File.WriteAllText(generatedtemplate, str);
        }

        public static void ExportRawFile()
        {
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(Application.StartupPath + "\\tmp");
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = "zip";
            saveFileDialog.Filter = "zip Files (*.zip)|*.zip";
            saveFileDialog.FilterIndex = 1;
            saveFileDialog.Title = "Select File Save Location";
            saveFileDialog.FileName = DateTime.Now.ToString("yyyyMMdd") + "RawData.zip";
            saveFileDialog.InitialDirectory = "C:\\";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (!dir.Exists)
                {


                    DirectoryInfo dInfo = new DirectoryInfo(Application.StartupPath);
                    DirectorySecurity dSecurity = dInfo.GetAccessControl();
                    dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
                    dInfo.SetAccessControl(dSecurity);
                    createDirectory(dir.FullName);
                    //dir.Create();
                }

                InmatesService inmateserv = new InmatesService();
                using (var textWriter = File.CreateText(Application.StartupPath + "\\tmp\\" + DateTime.Now.ToString("yyyyMMdd") + "InmatesList.csv"))
                {
                    foreach (var line in ToCsv(inmateserv.Get()))
                    {
                        textWriter.WriteLine(line);
                    }
                }

                VisitorService visitorserv = new VisitorService();
                using (var textWriter = File.CreateText(Application.StartupPath + "\\tmp\\" + DateTime.Now.ToString("yyyyMMdd") + "VisitorList.csv"))
                {
                    foreach (var line in ToCsv(visitorserv.Get()))
                    {
                        textWriter.WriteLine(line);
                    }
                }

                PhotosService photoserv = new PhotosService();
                using (var textWriter = File.CreateText(Application.StartupPath + "\\tmp\\" + DateTime.Now.ToString("yyyyMMdd") + "PhotoList.csv"))
                {
                    foreach (var line in ToCsv(photoserv.Get()))
                    {
                        textWriter.WriteLine(line);
                    }
                }

                FingerPrintService fingerprintserv = new FingerPrintService();
                using (var textWriter = File.CreateText(Application.StartupPath + "\\tmp\\" + DateTime.Now.ToString("yyyyMMdd") + "FingerPrintList.csv"))
                {
                    foreach (var line in ToCsv(fingerprintserv.Get()))
                    {
                        textWriter.WriteLine(line);
                    }
                }
                CreateEncryptedZipFileFromDirectory(Application.StartupPath + "\\tmp", saveFileDialog.FileName);
                dir = new System.IO.DirectoryInfo(Application.StartupPath + "\\tmp");
                if (dir.Exists)
                {
                    //ClearAttributes(dir.FullName);
                    deleteDirectory(dir.FullName);
                    //dir.Delete(true);
                    File.Delete(Application.StartupPath + "\\test.bat");
                }
                UserFunction.MsgBox(TipeMsg.Info, "Raw Data Generated Successfully !");
            }
        }

        private static void createDirectory(string dirpath)
        {
            StreamWriter sw = new StreamWriter(Application.StartupPath + "\\test.bat");
            sw.Write("mkdir \"" + dirpath + "\"");
            sw.Close();
            System.Diagnostics.ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(Application.StartupPath + "\\test.bat");
            p.WindowStyle = ProcessWindowStyle.Hidden;
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo = p;
            proc.Start();
            proc.WaitForExit();
        }

        private static void deleteDirectory(string dirpath)
        {
            StreamWriter sw = new StreamWriter(Application.StartupPath + "\\test.bat");
            sw.Write("rmdir \"" + dirpath + "\" /s /q");
            sw.Close();
            System.Diagnostics.ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(Application.StartupPath + "\\test.bat");
            p.WindowStyle = ProcessWindowStyle.Hidden;
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo = p;
            proc.Start();
            proc.WaitForExit();
        }


        private static IEnumerable<string> ToCsv<T>(IEnumerable<T> list)
        {
            var fields = typeof(T).GetFields();
            var properties = typeof(T).GetProperties();

            foreach (var @object in list)
            {
                yield return string.Join(",",
                                         fields.Select(x => (x.GetValue(@object) ?? string.Empty).ToString())
                                               .Concat(properties.Select(p => (p.GetValue(@object, null) ?? string.Empty).ToString()))
                                               .ToArray());
            }
        }

        private static void CreateEncryptedZipFileFromDirectory(string sourceDirectoryName, string destinationArchiveFileName)
        {
            char[] directorySeparatorChar = new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar };

            if (!string.IsNullOrEmpty(sourceDirectoryName))
            {
                using (FileStream archiveStream = File.Open(
                    destinationArchiveFileName,
                    FileMode.Create))
                {
                    // Set password to protect ZIP archive
                    DefaultEncryptionSettings encryptionSettings = new DefaultEncryptionSettings();
					encryptionSettings.Password = UserFunction.GetSettings(SaveSettingsType.RawDataPassword) == string.Empty ? "12345" : SimpleRSA.Decrypt(UserFunction.GetSettings(SaveSettingsType.RawDataPassword));

                    using (ZipArchive archive = new ZipArchive(
                        archiveStream,
                        ZipArchiveMode.Create,
                        true,
                        null,
                        null,
                        encryptionSettings))
                    {
                        foreach (string fileName in Directory.GetFiles(sourceDirectoryName))
                        {
                            using (FileStream file = File.OpenRead(fileName))
                            {
                                int length = fileName.Length - sourceDirectoryName.Length;
                                string entryName = fileName.Substring(sourceDirectoryName.Length, length);
                                entryName = entryName.TrimStart(directorySeparatorChar);

                                using (ZipArchiveEntry entry = archive.CreateEntry(entryName))
                                {
                                    using (Stream entryStream = entry.Open())
                                    {
                                        file.CopyTo(entryStream);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        public static void ExportGrid(ExportGridType tipe, string FileName, string Title, RadGridView grid)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            switch (tipe)
            {
                case ExportGridType.PDF:
                    saveFileDialog = new SaveFileDialog();
                    saveFileDialog.DefaultExt = "pdf";
                    saveFileDialog.Filter = "pdf Files (*.pdf)|*.pdf|All files (*.*)|*.*";
                    saveFileDialog.FilterIndex = 1;
                    saveFileDialog.Title = "Select Save Location";
                    saveFileDialog.FileName = FileName;
                    saveFileDialog.InitialDirectory = "C:\\";
                    if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        grid.EnablePaging = false;
                        ExportToPDF exporter = new ExportToPDF(grid);
                        exporter.PageTitle = Title;
                        exporter.FitToPageWidth = true;
                        exporter.PdfExportSettings.PageWidth = 297;
                        exporter.PdfExportSettings.PageHeight = 210;
                        exporter.RunExport(saveFileDialog.FileName);
                        grid.EnablePaging = true;
                        UserFunction.MsgBox(TipeMsg.Info, "Data diekspor ke PDF!");
                    }
                    break;
                case ExportGridType.CSV:
                    saveFileDialog = new SaveFileDialog();
                    saveFileDialog.DefaultExt = "csv";
                    saveFileDialog.Filter = "csv Files (*.csv)|*.csv|All files (*.*)|*.*";
                    saveFileDialog.FilterIndex = 1;
                    saveFileDialog.Title = "Select Save Location";
                    saveFileDialog.FileName = FileName;
                    saveFileDialog.InitialDirectory = "C:\\";
                    if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        grid.EnablePaging = false;
                        ExportToCSV exporter = new ExportToCSV(grid);
                        exporter.RunExport(saveFileDialog.FileName);
                        grid.EnablePaging = true;
                        UserFunction.MsgBox(TipeMsg.Info, "Data diekspor ke CSV !");
                    }
                    break;
                default:
                    break;
            }
        }

        public static void StatusCellColor(string cellStatus, RadLabel picStatusCell, RadLabel lblErorCellCode)
        {
            switch (cellStatus)
            {
                case "TERSEDIA":
                    picStatusCell.BackColor = Color.Lime;
                    lblErorCellCode.Text = "Cell TERSEDIA";
                    lblErorCellCode.ForeColor = Color.Lime;
                    break;
                case "DALAM PEMELIHARAAN":
                    picStatusCell.BackColor = Color.Yellow;
                    lblErorCellCode.Text = "Cell DALAM PEMELIHARAAN";
                    lblErorCellCode.ForeColor = Color.Yellow;
                    break;
                case "DITEMPATI":
                    picStatusCell.BackColor = Color.Red;
                    lblErorCellCode.Text = "Cell DITEMPATI";
                    lblErorCellCode.ForeColor = Color.Red;
                    break;
            }
        }

        public static void SetLoggedIn(AspnetUsers DataUser)
        {
            Application.DoEvents();
            if (DataUser.LastLogin != null)
                UserFunction.SaveSettings(SaveSettingsType.LastLogin, DataUser.LastLogin.Value.ToString("dd MMM yyyy HH:mm:ss"));

            UserFunction.SaveSettings(SaveSettingsType.UserID, DataUser.UserName);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul=ModulName.LOGIN.ToString(), Activities="Login With UserName: " + DataUser.UserName });
            GlobalVariables.UserID = DataUser.UserName;
            Application.DoEvents();
            Thread.Sleep(1000);
            Application.DoEvents();
        }

        public static void LoadDataAllocationToGrid(RadGridView gvAllocation)
        {
            gvAllocation.DataSource = null;
            CellAllocationService alloc = new CellAllocationService();
            gvAllocation.AutoGenerateColumns = true;
            gvAllocation.DataSource = alloc.Get();

            gvAllocation.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvAllocation.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvAllocation.Columns[0].Width = 20;
            gvAllocation.Columns[1].Width = 20;
            //gvAllocation.Columns[2].Width = 20;

            gvAllocation.Columns[0].IsVisible = true;
            gvAllocation.Columns[0].ReadOnly = false;
            gvAllocation.Columns[0].NullValue = Resources.view_detail;
            gvAllocation.Columns[0].FormatString = "Tampilan Detail";

            //gvAllocation.Columns[1].IsVisible = true;
            //gvAllocation.Columns[1].ReadOnly = false;
            //gvAllocation.Columns[1].NullValue = Resources.edit1;
            //gvAllocation.Columns[1].FormatString = "Ubah Data";

            gvAllocation.Columns[1].IsVisible = true;
            gvAllocation.Columns[1].ReadOnly = false;
            gvAllocation.Columns[1].NullValue = Resources.checkedout;
            gvAllocation.Columns[1].FormatString = "Keluar";

            gvAllocation.Columns["CellCode"].IsVisible = true;
            gvAllocation.Columns["CellCode"].HeaderText = "KODE SEL";
            gvAllocation.Columns["CellCode"].Width = 100;

            gvAllocation.Columns["FullName"].IsVisible = true;
            gvAllocation.Columns["FullName"].HeaderText = "NAMA TAHANAN (ID REG)";
            gvAllocation.Columns["FullName"].Width = 150;
            gvAllocation.Columns["FullName"].WrapText = true;

            //gvAllocation.Columns["CellStatus"].IsVisible = true;
            //gvAllocation.Columns["CellStatus"].HeaderText = "CELL STATUS";
            //gvAllocation.Columns["CellStatus"].Width = 100;

            gvAllocation.Columns["CreatedBy"].IsVisible = true;
            gvAllocation.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvAllocation.Columns["CreatedBy"].Width = 100;

            gvAllocation.Columns["CreatedDate"].IsVisible = true;
            gvAllocation.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvAllocation.Columns["CreatedDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvAllocation.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvAllocation.Columns["CreatedDate"].Width = 100;

            //gvUser.Columns["UpdatedBy"].IsVisible = true;
            //gvUser.Columns["UpdatedBy"].HeaderText = "UPDATE OLEH";
            //gvUser.Columns["UpdatedBy"].Width = 100;

            //gvUser.Columns["UpdatedDate"].IsVisible = true;
            //gvUser.Columns["UpdatedDate"].HeaderText = "TGL UPDATE";
            //gvUser.Columns["UpdatedDate"].FormatString = "{0:dd MMM yyyy hh:mm:ss}";
            //gvUser.Columns["UpdatedDate"].Width = 100;

        }

        public static void LoadDataUserToGrid(RadGridView gvUser)
        {
            gvUser.DataSource = null;
            AppUserService user = new AppUserService();
            gvUser.AutoGenerateColumns = true;
            gvUser.DataSource = user.Get();

            gvUser.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvUser.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvUser.Columns[0].Width = 20;
            gvUser.Columns[1].Width = 20;
            gvUser.Columns[2].Width = 20;

            gvUser.Columns[0].IsVisible = true;
            gvUser.Columns[0].ReadOnly = false;
            gvUser.Columns[0].NullValue = Resources.view_detail;

            gvUser.Columns[1].IsVisible = true;
            gvUser.Columns[1].ReadOnly = false;
            gvUser.Columns[1].NullValue = Resources.edit1;

            gvUser.Columns[2].IsVisible = true;
            gvUser.Columns[2].ReadOnly = false;
            gvUser.Columns[2].NullValue = Resources.delete;


            gvUser.Columns["Username"].IsVisible = true;
            gvUser.Columns["Username"].HeaderText = "ID PENGGUNA";
            gvUser.Columns["Username"].Width = 100;

            gvUser.Columns["LastLogin"].IsVisible = true;
            gvUser.Columns["LastLogin"].HeaderText = "TERAKHIR LOGIN";
            gvUser.Columns["LastLogin"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvUser.Columns["LastLogin"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvUser.Columns["LastLogin"].Width = 100;

            gvUser.Columns["Status"].IsVisible = true;
            gvUser.Columns["Status"].HeaderText = "STATUS";
            gvUser.Columns["Status"].Width = 100;

            gvUser.Columns["CreatedBy"].IsVisible = true;
            gvUser.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvUser.Columns["CreatedBy"].Width = 100;

            gvUser.Columns["CreatedDate"].IsVisible = true;
            gvUser.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvUser.Columns["CreatedDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvUser.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvUser.Columns["CreatedDate"].Width = 100;

            //gvUser.Columns["UpdatedBy"].IsVisible = true;
            //gvUser.Columns["UpdatedBy"].HeaderText = "UPDATE OLEH";
            //gvUser.Columns["UpdatedBy"].Width = 100;

            //gvUser.Columns["UpdatedDate"].IsVisible = true;
            //gvUser.Columns["UpdatedDate"].HeaderText = "TGL UPDATE";
            //gvUser.Columns["UpdatedDate"].FormatString = "{0:dd MMM yyyy hh:mm:ss}";
            //gvUser.Columns["UpdatedDate"].Width = 100;

        }

        public static void LoadDataHistoryVisitToGrid(RadGridView gvUser, int visitorId)
        {
            gvUser.DataSource = null;
            VisitService visitserv = new VisitService();
            gvUser.AutoGenerateColumns = true;
            gvUser.DataSource = visitserv.GetHistory(visitorId);

            gvUser.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvUser.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvUser.Columns["StartVisit"].IsVisible = true;
            gvUser.Columns["StartVisit"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvUser.Columns["StartVisit"].FormatString = "{0:dd MMM yyyy HH:mm}";
            gvUser.Columns["StartVisit"].HeaderText = "WAKTU MASUK";
            gvUser.Columns["StartVisit"].Width = 100;

            gvUser.Columns["EndVisit"].IsVisible = true;
            gvUser.Columns["EndVisit"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvUser.Columns["EndVisit"].HeaderText = "WAKTU KELUAR";
            gvUser.Columns["EndVisit"].FormatString = "{0:dd MMM yyyy HH:mm}";
            gvUser.Columns["EndVisit"].Width = 100;


            gvUser.Columns["Relation"].IsVisible = true;
            gvUser.Columns["Relation"].HeaderText = "HUBUNGAN";
            gvUser.Columns["Relation"].Width = 70;

            gvUser.Columns["InmatesName"].IsVisible = true;
            gvUser.Columns["InmatesName"].HeaderText = "TAHANAN";
            gvUser.Columns["InmatesName"].Width = 100;

            gvUser.Columns["Type"].IsVisible = true;
            gvUser.Columns["Type"].HeaderText = "TIPE";
            gvUser.Columns["Type"].Width = 70;

            gvUser.Columns["BringingItems"].IsVisible = true;
            gvUser.Columns["BringingItems"].HeaderText = "CATATAN";
            gvUser.Columns["BringingItems"].Width = 150;
            
        }

        public static void LoadDataHistoryCelAllocationToGrid(RadGridView gvUser, string inmatesId)
        {
            gvUser.DataSource = null;
            CellAllocationService allocserv = new CellAllocationService();
            gvUser.AutoGenerateColumns = true;
            gvUser.DataSource = allocserv.GetHistory(inmatesId);

            gvUser.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvUser.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }
            gvUser.Columns["InmatesRegCode"].IsVisible = true;
            gvUser.Columns["InmatesRegCode"].HeaderText = "REG ID";
            gvUser.Columns["InmatesRegCode"].Width = 70;

            gvUser.Columns["CellCode"].IsVisible = true;
            gvUser.Columns["CellCode"].HeaderText = "KODE SEL";
            gvUser.Columns["CellCode"].Width = 100;

            gvUser.Columns["CellStatus"].IsVisible = true;
            gvUser.Columns["CellStatus"].HeaderText = "STATUS PENAHANAN";
            gvUser.Columns["CellStatus"].Width = 70;

            gvUser.Columns["DateEnter"].IsVisible = true;
            gvUser.Columns["DateEnter"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvUser.Columns["DateEnter"].FormatString = "{0:dd MMM yyyy HH:mm}";
            gvUser.Columns["DateEnter"].HeaderText = "WAKTU MASUK";
            gvUser.Columns["DateEnter"].Width = 100;

            gvUser.Columns["CheckedOutDate"].IsVisible = true;
            gvUser.Columns["CheckedOutDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvUser.Columns["CheckedOutDate"].HeaderText = "WAKTU KELUAR";
            gvUser.Columns["CheckedOutDate"].FormatString = "{0:dd MMM yyyy HH:mm}";
            gvUser.Columns["CheckedOutDate"].Width = 100;

        }

        public static void LoadDataHistoryTahananToGrid(RadGridView gvHistory, string regID)
        {
            gvHistory.DataSource = null;
            InmatesService inmateserv = new InmatesService();
            gvHistory.AutoGenerateColumns = true;
            gvHistory.DataSource = inmateserv.GetHistory(regID);

            gvHistory.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvHistory.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvHistory.Columns["RegID"].IsVisible = true;
            gvHistory.Columns["RegID"].HeaderText = "NOMOR REGISTER";
            gvHistory.Columns["RegID"].Width = 100;

            gvHistory.Columns["FullName"].IsVisible = true;
            gvHistory.Columns["FullName"].HeaderText = "NAMA LENGKAP";
            gvHistory.Columns["FullName"].Width = 100;

            gvHistory.Columns["Identitas"].IsVisible = true;
            gvHistory.Columns["Identitas"].HeaderText = "IDENTITAS";
            gvHistory.Columns["Identitas"].Width = 100;

            gvHistory.Columns["DateReg"].IsVisible = true;
            gvHistory.Columns["DateReg"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvHistory.Columns["DateReg"].FormatString = "{0:dd MMM yyyy HH:mm}";
            gvHistory.Columns["DateReg"].HeaderText = "WAKTU MASUK";
            gvHistory.Columns["DateReg"].Width = 100;

            gvHistory.Columns["DateCheckedOut"].IsVisible = true;
            gvHistory.Columns["DateCheckedOut"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvHistory.Columns["DateCheckedOut"].HeaderText = "WAKTU KELUAR";
            gvHistory.Columns["DateCheckedOut"].FormatString = "{0:dd MMM yyyy HH:mm}";
            gvHistory.Columns["DateCheckedOut"].Width = 100;

        }

        public static void LoadDataRptCasesToGrid(RadGridView gvCell)
        {
            gvCell.DataSource = null;
            CasesService casesserv = new CasesService();
            gvCell.AutoGenerateColumns = true;
            gvCell.DataSource = casesserv.GetDetailRpt();

            gvCell.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvCell.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvCell.Columns["Name"].IsVisible = true;
            gvCell.Columns["Name"].HeaderText = "KASUS";
            gvCell.Columns["Name"].Width = 150;

            gvCell.Columns["Inmates"].IsVisible = true;
            gvCell.Columns["Inmates"].HeaderText = "NAMA TAHANAN";
            gvCell.Columns["Inmates"].Width = 250;

            gvCell.Columns["SeizedAssets"].IsVisible = true;
            gvCell.Columns["SeizedAssets"].HeaderText = "BENDA SITAAN";
            gvCell.Columns["SeizedAssets"].Width = 250;

        }

        public static void LoadDataRptInmateToGrid(RadGridView gvCell)
        {
            gvCell.DataSource = null;
            InmatesService inmates = new InmatesService();
            gvCell.AutoGenerateColumns = true;
            gvCell.DataSource = inmates.GetDetailRpt();

            gvCell.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvCell.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

  
            gvCell.Columns["RegID"].IsVisible = true;
            gvCell.Columns["RegID"].HeaderText = "ID REG";
            gvCell.Columns["RegID"].Width = 100;

            gvCell.Columns["DateReg"].IsVisible = true;
            gvCell.Columns["DateReg"].HeaderText = "TANGGAL REG";
            gvCell.Columns["DateReg"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvCell.Columns["DateReg"].FormatString = "{0:dd MMM yyyy}";
            gvCell.Columns["DateReg"].Width = 100;

            gvCell.Columns["FullName"].IsVisible = true;
            gvCell.Columns["FullName"].HeaderText = "NAMA LENGKAP";
            gvCell.Columns["FullName"].Width = 200;

            gvCell.Columns["Gender"].IsVisible = true;
            gvCell.Columns["Gender"].HeaderText = "JENIS KELAMIN/UMUR";
            gvCell.Columns["Gender"].Width = 100;

            gvCell.Columns["CellCode"].IsVisible = true;
            gvCell.Columns["CellCode"].HeaderText = "NO SEL";
            gvCell.Columns["CellCode"].Width = 100;

            gvCell.Columns["Cases"].IsVisible = true;
            gvCell.Columns["Cases"].HeaderText = "KASUS";
            gvCell.Columns["Cases"].Width = 200;
        }

        public static void LoadDataCellToGrid(RadGridView gvCell)
        {
            gvCell.DataSource = null;
            CellService cell = new CellService();
            gvCell.AutoGenerateColumns = true;
            gvCell.DataSource = cell.Get();

            gvCell.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvCell.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvCell.Columns[0].Width = 20;
            gvCell.Columns[1].Width = 20;
            gvCell.Columns[2].Width = 20;

            gvCell.Columns[0].IsVisible = true;
            gvCell.Columns[0].ReadOnly = false;
            gvCell.Columns[0].NullValue = Resources.view_detail;

            gvCell.Columns[1].IsVisible = true;
            gvCell.Columns[1].ReadOnly = false;
            gvCell.Columns[1].NullValue = Resources.edit1;

            gvCell.Columns[2].IsVisible = true;
            gvCell.Columns[2].ReadOnly = false;
            gvCell.Columns[2].NullValue = Resources.delete;

            gvCell.Columns["CellCode"].IsVisible = true;
            gvCell.Columns["CellCode"].HeaderText = "KODE SEL";
            gvCell.Columns["CellCode"].Width = 100;

            gvCell.Columns["CellNumber"].IsVisible = true;
            gvCell.Columns["CellNumber"].HeaderText = "NOMOR SEL";
            gvCell.Columns["CellNumber"].Width = 100;

            gvCell.Columns["BuildingName"].IsVisible = true;
            gvCell.Columns["BuildingName"].HeaderText = "NAMA GEDUNG";
            gvCell.Columns["BuildingName"].Width = 100;

            gvCell.Columns["CellFloor"].IsVisible = true;
            gvCell.Columns["CellFloor"].HeaderText = "LANTAI";
            gvCell.Columns["CellFloor"].Width = 100;

            gvCell.Columns["CellStatus"].IsVisible = true;
            gvCell.Columns["CellStatus"].HeaderText = "STATUS";
            gvCell.Columns["CellStatus"].Width = 100;

            gvCell.Columns["CreatedBy"].IsVisible = true;
            gvCell.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvCell.Columns["CreatedBy"].Width = 100;

            gvCell.Columns["CreatedDate"].IsVisible = true;
            gvCell.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvCell.Columns["CreatedDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvCell.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvCell.Columns["CreatedDate"].Width = 100;

            //gvCell.Columns["UpdatedBy"].IsVisible = true;
            //gvCell.Columns["UpdatedBy"].HeaderText = "UPDATE OLEH";
            //gvCell.Columns["UpdatedBy"].Width = 100;

            //gvCell.Columns["UpdatedDate"].IsVisible = true;
            //gvCell.Columns["UpdatedDate"].HeaderText = "TGL UPDATE";
            //gvCell.Columns["UpdatedDate"].FormatString = "{0:dd MMM yyyy hh:mm:ss}";
            //gvCell.Columns["UpdatedDate"].Width = 100;

        }

        public static void LoadDataKendaraanToGrid(RadGridView gvKend)
        {
            gvKend.DataSource = null;
            KendaraanTahananService kend = new KendaraanTahananService();
            gvKend.AutoGenerateColumns = true;
            gvKend.DataSource = kend.GetAvailableKendaraanTahanan();

            gvKend.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvKend.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvKend.Columns[0].Width = 20;
            gvKend.Columns[1].Width = 20;
            gvKend.Columns[2].Width = 20;

            gvKend.Columns[0].IsVisible = true;
            gvKend.Columns[0].ReadOnly = false;
            gvKend.Columns[0].NullValue = Resources.view_detail;
            gvKend.Columns[0].FormatString = "Tampilan Detail";

            gvKend.Columns[1].IsVisible = true;
            gvKend.Columns[1].ReadOnly = false;
            gvKend.Columns[1].NullValue = Resources.edit1;
            gvKend.Columns[1].FormatString = "Ubah Data";

            gvKend.Columns[2].IsVisible = true;
            gvKend.Columns[2].ReadOnly = false;
            gvKend.Columns[2].NullValue = Resources.checkedout;
            gvKend.Columns[2].FormatString = "Keluar";

            gvKend.Columns["NamaKategoriKendaraan"].IsVisible = true;
            gvKend.Columns["NamaKategoriKendaraan"].HeaderText = "KATEGORI";
            gvKend.Columns["NamaKategoriKendaraan"].Width = 100;

            gvKend.Columns["NoPlat"].IsVisible = true;
            gvKend.Columns["NoPlat"].HeaderText = "NO PLAT";
            gvKend.Columns["NoPlat"].Width = 70;

            gvKend.Columns["Pengemudi"].IsVisible = true;
            gvKend.Columns["Pengemudi"].HeaderText = "PENGEMUDI";
            gvKend.Columns["Pengemudi"].Width = 100;

            gvKend.Columns["TanggalMasuk"].IsVisible = true;
            gvKend.Columns["TanggalMasuk"].HeaderText = "TANGGAL/JAM MASUK";
            gvKend.Columns["TanggalMasuk"].Width = 150;
            gvKend.Columns["TanggalMasuk"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvKend.Columns["TanggalMasuk"].FormatString = "{0:dd MMM yyyy HH:mm}";

            //gvKend.Columns["CreatedDate"].IsVisible = true;
            //gvKend.Columns["CreatedDate"].HeaderText = "WAKTU MASUK";
            //gvKend.Columns["CreatedDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            //gvKend.Columns["CreatedDate"].FormatString = "{HH:mm}";
            //gvKend.Columns["CreatedDate"].Width = 100;       

            gvKend.Columns["CreatedBy"].IsVisible = true;
            gvKend.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvKend.Columns["CreatedBy"].Width = 100;

            gvKend.Columns["CreatedDate"].IsVisible = true;
            gvKend.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvKend.Columns["CreatedDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvKend.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy}";
            gvKend.Columns["CreatedDate"].Width = 100;

            //gvCell.Columns["UpdatedBy"].IsVisible = true;
            //gvCell.Columns["UpdatedBy"].HeaderText = "UPDATE OLEH";
            //gvCell.Columns["UpdatedBy"].Width = 100;

            //gvCell.Columns["UpdatedDate"].IsVisible = true;
            //gvCell.Columns["UpdatedDate"].HeaderText = "TGL UPDATE";
            //gvCell.Columns["UpdatedDate"].FormatString = "{0:dd MMM yyyy hh:mm:ss}";
            //gvCell.Columns["UpdatedDate"].Width = 100;

        }

        public static void LoadDataKendaraanKeluarToGrid(RadGridView gvKend)
        {
            gvKend.DataSource = null;
            KendaraanTahananService kend = new KendaraanTahananService();
            gvKend.AutoGenerateColumns = true;
            gvKend.DataSource = kend.GetCheckedOutKendaraanTahanan();

            gvKend.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvKend.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvKend.Columns[0].Width = 20;

            gvKend.Columns[0].IsVisible = true;
            gvKend.Columns[0].ReadOnly = false;
            gvKend.Columns[0].NullValue = Resources.view_detail;
            gvKend.Columns[0].FormatString = "Tampilan Detail";

            gvKend.Columns["NamaKategoriKendaraan"].IsVisible = true;
            gvKend.Columns["NamaKategoriKendaraan"].HeaderText = "KATEGORI";
            gvKend.Columns["NamaKategoriKendaraan"].Width = 100;

            gvKend.Columns["NoPlat"].IsVisible = true;
            gvKend.Columns["NoPlat"].HeaderText = "NO PLAT";
            gvKend.Columns["NoPlat"].Width = 70;

            gvKend.Columns["Pengemudi"].IsVisible = true;
            gvKend.Columns["Pengemudi"].HeaderText = "PENGEMUDI";
            gvKend.Columns["Pengemudi"].Width = 100;

            gvKend.Columns["WaktuKeluar"].IsVisible = true;
            gvKend.Columns["WaktuKeluar"].HeaderText = "WAKTU KELUAR";
            gvKend.Columns["WaktuKeluar"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvKend.Columns["WaktuKeluar"].FormatString = "{0:dd MMM yyyy HH:mm}";
            gvKend.Columns["WaktuKeluar"].Width = 150;

            gvKend.Columns["CreatedBy"].IsVisible = true;
            gvKend.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvKend.Columns["CreatedBy"].Width = 100;

            gvKend.Columns["CreatedDate"].IsVisible = true;
            gvKend.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvKend.Columns["CreatedDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvKend.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy}";
            gvKend.Columns["CreatedDate"].Width = 100;

            //gvCell.Columns["UpdatedBy"].IsVisible = true;
            //gvCell.Columns["UpdatedBy"].HeaderText = "UPDATE OLEH";
            //gvCell.Columns["UpdatedBy"].Width = 100;

            //gvCell.Columns["UpdatedDate"].IsVisible = true;
            //gvCell.Columns["UpdatedDate"].HeaderText = "TGL UPDATE";
            //gvCell.Columns["UpdatedDate"].FormatString = "{0:dd MMM yyyy hh:mm:ss}";
            //gvCell.Columns["UpdatedDate"].Width = 100;

        }

        public static void LoadDataStatusTahananToGrid(RadGridView gvTahanan)
        {
            gvTahanan.DataSource = null;
            StatusTahananService tahanan = new StatusTahananService();
            gvTahanan.AutoGenerateColumns = true;
            gvTahanan.DataSource = tahanan.Get();

            gvTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvTahanan.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvTahanan.Columns[0].Width = 20;
            gvTahanan.Columns[1].Width = 20;
            gvTahanan.Columns[2].Width = 20;

            gvTahanan.Columns[0].IsVisible = true;
            gvTahanan.Columns[0].ReadOnly = false;
            gvTahanan.Columns[0].NullValue = Resources.view_detail;

            gvTahanan.Columns[1].IsVisible = true;
            gvTahanan.Columns[1].ReadOnly = false;
            gvTahanan.Columns[1].NullValue = Resources.edit1;

            gvTahanan.Columns[2].IsVisible = true;
            gvTahanan.Columns[2].ReadOnly = false;
            gvTahanan.Columns[2].NullValue = Resources.delete;

            gvTahanan.Columns["Id"].IsVisible = false;

            gvTahanan.Columns["RegID"].IsVisible = true;
            gvTahanan.Columns["RegID"].HeaderText = "NO REGISTRASI";
            gvTahanan.Columns["RegID"].Width = 150;

            gvTahanan.Columns["FullName"].IsVisible = true;
            gvTahanan.Columns["FullName"].HeaderText = "NAMA LENGKAP";
            gvTahanan.Columns["FullName"].Width = 150;

            //gvTahanan.Columns["CellNumber"].IsVisible = true;
            //gvTahanan.Columns["CellNumber"].HeaderText = "NOMOR SEL";
            //gvTahanan.Columns["CellNumber"].Width = 200;

            //gvTahanan.Columns["DateOfBirth"].IsVisible = true;
            //gvTahanan.Columns["DateOfBirth"].HeaderText = "DATE OF BIRTH";
            //gvTahanan.Columns["DateOfBirth"].Width = 200;

            gvTahanan.Columns["CreatedBy"].IsVisible = true;
            gvTahanan.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvTahanan.Columns["CreatedBy"].Width = 100;

            gvTahanan.Columns["CreatedDate"].IsVisible = true;
            gvTahanan.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvTahanan.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvTahanan.Columns["CreatedDate"].Width = 150;

            //gvTahanan.Columns["UpdatedBy"].IsVisible = true;
            //gvTahanan.Columns["UpdatedBy"].HeaderText = "UPDATE OLEH";
            //gvTahanan.Columns["UpdatedBy"].Width = 100;

            //gvTahanan.Columns["UpdatedDate"].IsVisible = true;
            //gvTahanan.Columns["UpdatedDate"].HeaderText = "TGL UPDATE";
            //gvTahanan.Columns["UpdatedDate"].FormatString = "{0:dd MMM yyyy hh:mm:ss}";
            //gvTahanan.Columns["UpdatedDate"].Width = 100;

        }

        public static void LoadDataTahananToGrid(RadGridView gvTahanan)
        {
            gvTahanan.DataSource = null;
            InmatesService tahanan = new InmatesService();
            gvTahanan.AutoGenerateColumns = true;
            gvTahanan.DataSource = tahanan.Get();

            gvTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvTahanan.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvTahanan.Columns[0].Width = 20;
            gvTahanan.Columns[1].Width = 20;
            gvTahanan.Columns[2].Width = 20;

            gvTahanan.Columns[0].IsVisible = true;
            gvTahanan.Columns[0].ReadOnly = false;
            gvTahanan.Columns[0].NullValue = Resources.view_detail;

            gvTahanan.Columns[1].IsVisible = true;
            gvTahanan.Columns[1].ReadOnly = false;
            gvTahanan.Columns[1].NullValue = Resources.edit1;

            gvTahanan.Columns[2].IsVisible = true;
            gvTahanan.Columns[2].ReadOnly = false;
            gvTahanan.Columns[2].NullValue = Resources.delete;

            gvTahanan.Columns["RegID"].IsVisible = true;
            gvTahanan.Columns["RegID"].HeaderText = "NO REGISTRASI";
            gvTahanan.Columns["RegID"].Width = 200;

            gvTahanan.Columns["FullName"].IsVisible = true;
            gvTahanan.Columns["FullName"].HeaderText = "NAMA LENGKAP";
            gvTahanan.Columns["FullName"].Width = 200;

            //gvTahanan.Columns["PlaceOfBirth"].IsVisible = true;
            //gvTahanan.Columns["PlaceOfBirth"].HeaderText = "PLACE OF BIRTH";
            //gvTahanan.Columns["PlaceOfBirth"].Width = 200;

            //gvTahanan.Columns["DateOfBirth"].IsVisible = true;
            //gvTahanan.Columns["DateOfBirth"].HeaderText = "DATE OF BIRTH";
            //gvTahanan.Columns["DateOfBirth"].Width = 200;

            gvTahanan.Columns["Gender"].IsVisible = true;
            gvTahanan.Columns["Gender"].HeaderText = "JENIS KELAMIN";
            gvTahanan.Columns["Gender"].Width = 50;

            gvTahanan.Columns["IDType"].IsVisible = true;
            gvTahanan.Columns["IDType"].HeaderText = "JENIS IDENTITAS";
            gvTahanan.Columns["IDType"].Width = 100;

            gvTahanan.Columns["IDNo"].IsVisible = true;
            gvTahanan.Columns["IDNo"].HeaderText = "NO IDENTITAS";
            gvTahanan.Columns["IDNo"].Width = 100;

            gvTahanan.Columns["CreatedBy"].IsVisible = true;
            gvTahanan.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvTahanan.Columns["CreatedBy"].Width = 100;

            gvTahanan.Columns["CreatedDate"].IsVisible = true;
            gvTahanan.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvTahanan.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvTahanan.Columns["CreatedDate"].Width = 100;

            //gvTahanan.Columns["UpdatedBy"].IsVisible = true;
            //gvTahanan.Columns["UpdatedBy"].HeaderText = "UPDATE OLEH";
            //gvTahanan.Columns["UpdatedBy"].Width = 100;

            //gvTahanan.Columns["UpdatedDate"].IsVisible = true;
            //gvTahanan.Columns["UpdatedDate"].HeaderText = "TGL UPDATE";
            //gvTahanan.Columns["UpdatedDate"].FormatString = "{0:dd MMM yyyy hh:mm:ss}";
            //gvTahanan.Columns["UpdatedDate"].Width = 100;

        }

        public static void LoadDataPrintFormTahananToGrid(RadGridView gvTahanan)
        {
            gvTahanan.DataSource = null;
            InmatesService tahanan = new InmatesService();
            gvTahanan.AutoGenerateColumns = true;
            gvTahanan.DataSource = tahanan.Get();

            gvTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvTahanan.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvTahanan.Columns[0].Width = 20;
            gvTahanan.Columns[1].Width = 20;
            //gvTahanan.Columns[2].Width = 20;

            gvTahanan.Columns[0].IsVisible = true;
            gvTahanan.Columns[0].ReadOnly = false;
            gvTahanan.Columns[0].NullValue = Resources.view_detail;

            //gvTahanan.Columns[1].IsVisible = true;
            //gvTahanan.Columns[1].ReadOnly = false;
            //gvTahanan.Columns[1].NullValue = Resources.edit1;

            gvTahanan.Columns[1].IsVisible = true;
            gvTahanan.Columns[1].ReadOnly = false;
            gvTahanan.Columns[1].NullValue = Resources.delete;

            gvTahanan.Columns["RegID"].IsVisible = true;
            gvTahanan.Columns["RegID"].HeaderText = "NO DAFTAR";
            gvTahanan.Columns["RegID"].Width = 200;

            gvTahanan.Columns["FullName"].IsVisible = true;
            gvTahanan.Columns["FullName"].HeaderText = "NAMA LENGKAP";
            gvTahanan.Columns["FullName"].Width = 200;

            gvTahanan.Columns["Gender"].IsVisible = true;
            gvTahanan.Columns["Gender"].HeaderText = "JENIS KELAMIN";
            gvTahanan.Columns["Gender"].Width = 50;

            gvTahanan.Columns["IDType"].IsVisible = true;
            gvTahanan.Columns["IDType"].HeaderText = "JENIS IDENTITAS";
            gvTahanan.Columns["IDType"].Width = 100;

            gvTahanan.Columns["IDNo"].IsVisible = true;
            gvTahanan.Columns["IDNo"].HeaderText = "NO IDENTITAS";
            gvTahanan.Columns["IDNo"].Width = 100;

            gvTahanan.Columns["CreatedBy"].IsVisible = true;
            gvTahanan.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvTahanan.Columns["CreatedBy"].Width = 100;

            gvTahanan.Columns["CreatedDate"].IsVisible = true;
            gvTahanan.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvTahanan.Columns["CreatedDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvTahanan.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvTahanan.Columns["CreatedDate"].Width = 100;

            //gvTahanan.Columns["UpdatedBy"].IsVisible = true;
            //gvTahanan.Columns["UpdatedBy"].HeaderText = "UPDATE OLEH";
            //gvTahanan.Columns["UpdatedBy"].Width = 100;

            //gvTahanan.Columns["UpdatedDate"].IsVisible = true;
            //gvTahanan.Columns["UpdatedDate"].HeaderText = "TGL UPDATE";
            //gvTahanan.Columns["UpdatedDate"].FormatString = "{0:dd MMM yyyy hh:mm:ss}";
            //gvTahanan.Columns["UpdatedDate"].Width = 100;

        }

        public static void LoadDataKunjunganToGrid(RadGridView gvVisit)
        {
            gvVisit.DataSource = null;
            VisitService kunjungan = new VisitService();
            gvVisit.AutoGenerateColumns = true;
            //gvVisit.DataSource = kunjungan.GetAllWithNoCheckedOut();\
            gvVisit.DataSource = kunjungan.GetAllKunjungan();

            gvVisit.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvVisit.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvVisit.Columns[0].Width = 25;
            //gvVisit.Columns[1].Width = 20;
            //gvVisit.Columns[2].Width = 20;

            gvVisit.Columns[0].IsVisible = true;
            gvVisit.Columns[0].ReadOnly = false;
            gvVisit.Columns[0].NullValue = Resources.view_detail;

            //gvVisit.Columns[1].IsVisible = true;
            //gvVisit.Columns[1].ReadOnly = false;
            //gvVisit.Columns[1].NullValue = Resources.edit1;

            //gvVisit.Columns[2].IsVisible = true;
            //gvVisit.Columns[2].ReadOnly = false;
            //gvVisit.Columns[2].NullValue = Resources.delete;

            gvVisit.Columns["VisitorName"].IsVisible = true;
            gvVisit.Columns["VisitorName"].HeaderText = "NAMA PENGUNJUNG";
            gvVisit.Columns["VisitorName"].Width = 100;

            gvVisit.Columns["InmatesName"].IsVisible = true;
            gvVisit.Columns["InmatesName"].HeaderText = "TAHANAN";
            gvVisit.Columns["InmatesName"].Width = 100;

            gvVisit.Columns["StartVisit"].IsVisible = true;
            gvVisit.Columns["StartVisit"].HeaderText = "WAKTU MASUK";
            gvVisit.Columns["StartVisit"].FormatString = "{0:dd MMM yyyy HH:mm}";
            gvVisit.Columns["StartVisit"].Width = 100;

            gvVisit.Columns["EndVisit"].IsVisible = true;
            gvVisit.Columns["EndVisit"].HeaderText = "WAKTU KELUAR";
            gvVisit.Columns["EndVisit"].FormatString = "{0:dd MMM yyyy HH:mm}";
            gvVisit.Columns["EndVisit"].Width = 100;

            gvVisit.Columns["CreatedBy"].IsVisible = true;
            gvVisit.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvVisit.Columns["CreatedBy"].Width = 100;

            gvVisit.Columns["CreatedDate"].IsVisible = true;
            gvVisit.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvVisit.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvVisit.Columns["CreatedDate"].Width = 100;

            gvVisit.Columns.Move(4, 2);
            //gvTahanan.Columns["UpdatedBy"].IsVisible = true;
            //gvTahanan.Columns["UpdatedBy"].HeaderText = "UPDATE OLEH";
            //gvTahanan.Columns["UpdatedBy"].Width = 100;

            //gvTahanan.Columns["UpdatedDate"].IsVisible = true;
            //gvTahanan.Columns["UpdatedDate"].HeaderText = "TGL UPDATE";
            //gvTahanan.Columns["UpdatedDate"].FormatString = "{0:dd MMM yyyy hh:mm:ss}";
            //gvTahanan.Columns["UpdatedDate"].Width = 100;

        }

        public static void LoadDataTahananCheckedOutToGrid(RadGridView gvVisit)
        {
            gvVisit.DataSource = null;
            InmatesIsCheckedOutService checkout = new InmatesIsCheckedOutService();
            gvVisit.AutoGenerateColumns = true;
            gvVisit.DataSource = checkout.GetAllInmatesWithNoCheckedOut();

            gvVisit.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvVisit.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvVisit.Columns[0].Width = 25;
            //gvVisit.Columns[1].Width = 20;
            //gvVisit.Columns[2].Width = 20;

            gvVisit.Columns[0].IsVisible = true;
            gvVisit.Columns[0].ReadOnly = false;
            gvVisit.Columns[0].NullValue = Resources.view_detail;

            //gvVisit.Columns[1].IsVisible = true;
            //gvVisit.Columns[1].ReadOnly = false;
            //gvVisit.Columns[1].NullValue = Resources.edit1;

            //gvVisit.Columns[2].IsVisible = true;
            //gvVisit.Columns[2].ReadOnly = false;
            //gvVisit.Columns[2].NullValue = Resources.delete;

            gvVisit.Columns["RegID"].IsVisible = true;
            gvVisit.Columns["RegID"].HeaderText = "NO REGISTER";
            gvVisit.Columns["RegID"].Width = 100;

            gvVisit.Columns["InmatesName"].IsVisible = true;
            gvVisit.Columns["InmatesName"].HeaderText = "NAMA TAHANAN";
            gvVisit.Columns["InmatesName"].Width = 100;

            gvVisit.Columns["DateReg"].IsVisible = true;
            gvVisit.Columns["DateReg"].HeaderText = "TANGGAL REGISTRASI";
            gvVisit.Columns["DateReg"].FormatString = "{0:dd MMM yyyy hh:mm}";
            gvVisit.Columns["DateReg"].Width = 100;

            gvVisit.Columns["DateCheckedOut"].IsVisible = true;
            gvVisit.Columns["DateCheckedOut"].HeaderText = "TANGGAL KELUAR";
            gvVisit.Columns["DateCheckedOut"].FormatString = "{0:dd MMM yyyy hh:mm}";
            gvVisit.Columns["DateCheckedOut"].Width = 100;

            //gvVisit.Columns["Notes"].IsVisible = true;
            //gvVisit.Columns["Notes"].HeaderText = "CATATAN";
            //gvVisit.Columns["Notes"].Width = 100;

            gvVisit.Columns["CreatedBy"].IsVisible = true;
            gvVisit.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvVisit.Columns["CreatedBy"].Width = 100;

            gvVisit.Columns["CreatedDate"].IsVisible = true;
            gvVisit.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvVisit.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvVisit.Columns["CreatedDate"].Width = 100;

            gvVisit.Columns.Move(4, 2);
            //gvTahanan.Columns["UpdatedBy"].IsVisible = true;
            //gvTahanan.Columns["UpdatedBy"].HeaderText = "UPDATE OLEH";
            //gvTahanan.Columns["UpdatedBy"].Width = 100;

            //gvTahanan.Columns["UpdatedDate"].IsVisible = true;
            //gvTahanan.Columns["UpdatedDate"].HeaderText = "TGL UPDATE";
            //gvTahanan.Columns["UpdatedDate"].FormatString = "{0:dd MMM yyyy hh:mm:ss}";
            //gvTahanan.Columns["UpdatedDate"].Width = 100;

        }

        public static void LoadDataPegunjungToGrid(RadGridView gvVisitor)
        {
            gvVisitor.DataSource = null;
            VisitorService pegunjung = new VisitorService();
            gvVisitor.AutoGenerateColumns = true;
            gvVisitor.DataSource = pegunjung.Get();

            gvVisitor.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvVisitor.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvVisitor.Columns[0].Width = 20;
            gvVisitor.Columns[1].Width = 20;
            gvVisitor.Columns[2].Width = 20;

            gvVisitor.Columns[0].IsVisible = true;
            gvVisitor.Columns[0].ReadOnly = false;
            gvVisitor.Columns[0].NullValue = Resources.view_detail;

            gvVisitor.Columns[1].IsVisible = true;
            gvVisitor.Columns[1].ReadOnly = false;
            gvVisitor.Columns[1].NullValue = Resources.edit1;

            gvVisitor.Columns[2].IsVisible = true;
            gvVisitor.Columns[2].ReadOnly = false;
            gvVisitor.Columns[2].NullValue = Resources.delete;

            gvVisitor.Columns["RegID"].IsVisible = true;
            gvVisitor.Columns["RegID"].HeaderText = "NO REGISTER";
            gvVisitor.Columns["RegID"].Width = 100;

            gvVisitor.Columns["FullName"].IsVisible = true;
            gvVisitor.Columns["FullName"].HeaderText = "NAMA PENGUNJUNG";
            gvVisitor.Columns["FullName"].Width = 100;

            gvVisitor.Columns["Gender"].IsVisible = true;
            gvVisitor.Columns["Gender"].HeaderText = "JENIS KELAMIN";
            gvVisitor.Columns["Gender"].Width = 50;

            gvVisitor.Columns["IDType"].IsVisible = true;
            gvVisitor.Columns["IDType"].HeaderText = "JENIS IDENTITAS";
            gvVisitor.Columns["IDType"].Width = 100;

            gvVisitor.Columns["IDNo"].IsVisible = true;
            gvVisitor.Columns["IDNo"].HeaderText = "NO IDENTITAS";
            gvVisitor.Columns["IDNo"].Width = 100;

            gvVisitor.Columns["CreatedBy"].IsVisible = true;
            gvVisitor.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvVisitor.Columns["CreatedBy"].Width = 100;

            gvVisitor.Columns["CreatedDate"].IsVisible = true;
            gvVisitor.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvVisitor.Columns["CreatedDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvVisitor.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvVisitor.Columns["CreatedDate"].Width = 100;

            //gvTahanan.Columns["UpdatedBy"].IsVisible = true;
            //gvTahanan.Columns["UpdatedBy"].HeaderText = "UPDATE OLEH";
            //gvTahanan.Columns["UpdatedBy"].Width = 100;

            //gvTahanan.Columns["UpdatedDate"].IsVisible = true;
            //gvTahanan.Columns["UpdatedDate"].HeaderText = "TGL UPDATE";
            //gvTahanan.Columns["UpdatedDate"].FormatString = "{0:dd MMM yyyy hh:mm:ss}";
            //gvTahanan.Columns["UpdatedDate"].Width = 100;

        }

        public static void LoadDataPrintVisitorToGrid(RadGridView gvVisitor)
        {
            gvVisitor.DataSource = null;
            VisitorService pegunjung = new VisitorService();
            gvVisitor.AutoGenerateColumns = true;
            gvVisitor.DataSource = pegunjung.Get();

            gvVisitor.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvVisitor.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvVisitor.Columns[0].Width = 20;

            gvVisitor.Columns[0].IsVisible = true;
            gvVisitor.Columns[0].ReadOnly = false;
            gvVisitor.Columns[0].NullValue = Resources.view_detail;

            gvVisitor.Columns["RegID"].IsVisible = true;
            gvVisitor.Columns["RegID"].HeaderText = "NO REGISTER";
            gvVisitor.Columns["RegID"].Width = 200;

            gvVisitor.Columns["FullName"].IsVisible = true;
            gvVisitor.Columns["FullName"].HeaderText = "NAMA LENGKAP";
            gvVisitor.Columns["FullName"].Width = 200;

            gvVisitor.Columns["Gender"].IsVisible = true;
            gvVisitor.Columns["Gender"].HeaderText = "JENIS KELAMIN";
            gvVisitor.Columns["Gender"].Width = 50;

            gvVisitor.Columns["IDType"].IsVisible = true;
            gvVisitor.Columns["IDType"].HeaderText = "JENIS IDENTITAS";
            gvVisitor.Columns["IDType"].Width = 100;

            gvVisitor.Columns["IDNo"].IsVisible = true;
            gvVisitor.Columns["IDNo"].HeaderText = "NO IDENTITAS";
            gvVisitor.Columns["IDNo"].Width = 100;

            gvVisitor.Columns["CreatedBy"].IsVisible = true;
            gvVisitor.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvVisitor.Columns["CreatedBy"].Width = 100;

            gvVisitor.Columns["CreatedDate"].IsVisible = true;
            gvVisitor.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvVisitor.Columns["CreatedDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvVisitor.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvVisitor.Columns["CreatedDate"].Width = 100;

            //gvTahanan.Columns["UpdatedBy"].IsVisible = true;
            //gvTahanan.Columns["UpdatedBy"].HeaderText = "UPDATE OLEH";
            //gvTahanan.Columns["UpdatedBy"].Width = 100;

            //gvTahanan.Columns["UpdatedDate"].IsVisible = true;
            //gvTahanan.Columns["UpdatedDate"].HeaderText = "TGL UPDATE";
            //gvTahanan.Columns["UpdatedDate"].FormatString = "{0:dd MMM yyyy hh:mm:ss}";
            //gvTahanan.Columns["UpdatedDate"].Width = 100;

        }

        public static void LoadDataSeizedAssetsToGrid(RadGridView gvSeizedAssets)
        {
            gvSeizedAssets.DataSource = null;
            SeizedAssetsService assets = new SeizedAssetsService();
            gvSeizedAssets.AutoGenerateColumns = true;
            gvSeizedAssets.DataSource = assets.Get();

            gvSeizedAssets.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvSeizedAssets.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvSeizedAssets.Columns[0].Width = 30;
            gvSeizedAssets.Columns[1].Width = 30;
            gvSeizedAssets.Columns[2].Width = 30;

            gvSeizedAssets.Columns[0].IsVisible = true;
            gvSeizedAssets.Columns[0].ReadOnly = false;
            gvSeizedAssets.Columns[0].NullValue = Resources.view_detail;

            gvSeizedAssets.Columns[1].IsVisible = true;
            gvSeizedAssets.Columns[1].ReadOnly = false;
            gvSeizedAssets.Columns[1].NullValue = Resources.edit1;

            gvSeizedAssets.Columns[2].IsVisible = true;
            gvSeizedAssets.Columns[2].ReadOnly = false;
            gvSeizedAssets.Columns[2].NullValue = Resources.delete;

            gvSeizedAssets.Columns["Id"].IsVisible = false;
            gvSeizedAssets.Columns["Id"].HeaderText = "Id";
            gvSeizedAssets.Columns["Id"].Width = 100;

            gvSeizedAssets.Columns["CaseName"].IsVisible = true;
            gvSeizedAssets.Columns["CaseName"].HeaderText = "NAMA KASUS";
            gvSeizedAssets.Columns["CaseName"].Width = 200;

            gvSeizedAssets.Columns["Nama"].IsVisible = true;
            gvSeizedAssets.Columns["Nama"].HeaderText = "BENDA SITAAN";
            gvSeizedAssets.Columns["Nama"].Width = 300;

            gvSeizedAssets.Columns["CreatedBy"].IsVisible = true;
            gvSeizedAssets.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvSeizedAssets.Columns["CreatedBy"].Width = 150;

            gvSeizedAssets.Columns["CreatedDate"].IsVisible = true;
            gvSeizedAssets.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvSeizedAssets.Columns["CreatedDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvSeizedAssets.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvSeizedAssets.Columns["CreatedDate"].Width = 150;

            //gvSeizedAssets.Columns["UpdatedBy"].IsVisible = true;
            //gvSeizedAssets.Columns["UpdatedBy"].HeaderText = "UPDATE OLEH";
            //gvSeizedAssets.Columns["UpdatedBy"].Width = 100;

            //gvSeizedAssets.Columns["UpdatedDate"].IsVisible = true;
            //gvSeizedAssets.Columns["UpdatedDate"].HeaderText = "TGL UPDATE";
            //gvSeizedAssets.Columns["UpdatedDate"].FormatString = "{0:dd MMM yyyy hh:mm:ss}";
            //gvSeizedAssets.Columns["UpdatedDate"].Width = 100;

        }

        public static void LoadDataSeizedAssetsCaseToGrid(RadGridView gvAssets, int Id)
        {
            gvAssets.DataSource = null;
            SeizedAssetsService aset = new SeizedAssetsService();
            gvAssets.AutoGenerateColumns = true;
            gvAssets.DataSource = aset.GetByCaseId(Id);
            gvAssets.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvAssets.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }


            gvAssets.Columns["Id"].IsVisible = false;
            gvAssets.Columns["Id"].HeaderText = "Id";
            gvAssets.Columns["Id"].Width = 100;

            gvAssets.Columns["CategoryName"].IsVisible = true;
            gvAssets.Columns["CategoryName"].HeaderText = "NAMA KATEGORI";
            gvAssets.Columns["CategoryName"].Width = 200;

            gvAssets.Columns["Detail"].IsVisible = true;
            gvAssets.Columns["Detail"].HeaderText = "DETAIL";
            gvAssets.Columns["Detail"].Width = 200;

            gvAssets.Columns["Quantity"].IsVisible = true;
            gvAssets.Columns["Quantity"].HeaderText = "JUMLAH";
            gvAssets.Columns["Quantity"].Width = 200;

            gvAssets.Columns["StorageType"].IsVisible = true;
            gvAssets.Columns["StorageType"].HeaderText = "TIPE PENYIMPANAN";
            gvAssets.Columns["StorageType"].Width = 200;

            gvAssets.Columns["StorageLocation"].IsVisible = true;
            gvAssets.Columns["StorageLocation"].HeaderText = "LOKASI PENYIMPANAN";
            gvAssets.Columns["StorageLocation"].Width = 200;

            gvAssets.Columns["Note"].IsVisible = true;
            gvAssets.Columns["Note"].HeaderText = "CATATAN";
            gvAssets.Columns["Note"].Width = 200;
		}

        public static void LoadDataBonTahananToGrid(RadGridView gvBonTahanan)
        {
            gvBonTahanan.DataSource = null;
            BonTahananService bonServ = new BonTahananService();
            gvBonTahanan.AutoGenerateColumns = true;
            gvBonTahanan.DataSource = bonServ.Get();
            gvBonTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvBonTahanan.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvBonTahanan.Columns[0].Width = 30;
            gvBonTahanan.Columns[1].Width = 30;
            gvBonTahanan.Columns[2].Width = 30;
            gvBonTahanan.Columns[3].Width = 30;

            gvBonTahanan.Columns[0].IsVisible = true;
            gvBonTahanan.Columns[0].ReadOnly = false;
            gvBonTahanan.Columns[0].NullValue = Resources.view_detail;

            gvBonTahanan.Columns[1].IsVisible = true;
            gvBonTahanan.Columns[1].ReadOnly = false;
            gvBonTahanan.Columns[1].NullValue = Resources.edit1;

            gvBonTahanan.Columns[2].IsVisible = true;
            gvBonTahanan.Columns[2].ReadOnly = false;
            gvBonTahanan.Columns[2].NullValue = Resources.delete;

            gvBonTahanan.Columns[3].IsVisible = true;
            gvBonTahanan.Columns[3].ReadOnly = false;
            gvBonTahanan.Columns[3].NullValue = Resources.print;

            gvBonTahanan.Columns["Id"].IsVisible = false;
            gvBonTahanan.Columns["Id"].HeaderText = "Id";
            gvBonTahanan.Columns["Id"].Width = 100;

            gvBonTahanan.Columns["NoBonTahanan"].IsVisible = true;
            gvBonTahanan.Columns["NoBonTahanan"].HeaderText = "NO BON TAHANAN";
            gvBonTahanan.Columns["NoBonTahanan"].Width = 200;

            //gvBonTahanan.Columns["NamaTahanan"].IsVisible = true;
            //gvBonTahanan.Columns["NamaTahanan"].HeaderText = "NAMA TAHANAN";
            //gvBonTahanan.Columns["NamaTahanan"].Width = 200;

            gvBonTahanan.Columns["NamaKeperluan"].IsVisible = true;
            gvBonTahanan.Columns["NamaKeperluan"].HeaderText = "KEPERLUAN";
            gvBonTahanan.Columns["NamaKeperluan"].Width = 200;

            //gvBonTahanan.Columns["Lokasi"].IsVisible = true;
            //gvBonTahanan.Columns["Lokasi"].HeaderText = "LOKASI";
            //gvBonTahanan.Columns["Lokasi"].Width = 200;

            //gvBonTahanan.Columns["Keterangan"].IsVisible = true;
            //gvBonTahanan.Columns["Keterangan"].HeaderText = "KETERANGAN";
            //gvBonTahanan.Columns["Keterangan"].Width = 200;

            gvBonTahanan.Columns["NamaPemohon"].IsVisible = true;
            gvBonTahanan.Columns["NamaPemohon"].HeaderText = "YANG MEMOHON";
            gvBonTahanan.Columns["NamaPemohon"].Width = 200;

            gvBonTahanan.Columns["NamaPenanggungJawab"].IsVisible = true;
            gvBonTahanan.Columns["NamaPenanggungJawab"].HeaderText = "YANG MENJEMPUT";
            gvBonTahanan.Columns["NamaPenanggungJawab"].Width = 200;

            gvBonTahanan.Columns["NamaPenyetuju"].IsVisible = true;
            gvBonTahanan.Columns["NamaPenyetuju"].HeaderText = "YANG MENYETUJUI";
            gvBonTahanan.Columns["NamaPenyetuju"].Width = 200;	
        }

        public static void LoadDataBonSerahTahananToGrid(RadGridView gvBonSerahTahanan)
        {
            gvBonSerahTahanan.DataSource = null;
            BonSerahTahananService bonServ = new BonSerahTahananService();
            gvBonSerahTahanan.AutoGenerateColumns = true;
            gvBonSerahTahanan.DataSource = bonServ.Get();
            gvBonSerahTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvBonSerahTahanan.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvBonSerahTahanan.Columns[0].Width = 30;
            gvBonSerahTahanan.Columns[1].Width = 30;
            gvBonSerahTahanan.Columns[2].Width = 30;
            
            gvBonSerahTahanan.Columns[0].IsVisible = true;
            gvBonSerahTahanan.Columns[0].ReadOnly = false;
            gvBonSerahTahanan.Columns[0].NullValue = Resources.edit1;

            gvBonSerahTahanan.Columns[1].IsVisible = true;
            gvBonSerahTahanan.Columns[1].ReadOnly = false;
            gvBonSerahTahanan.Columns[1].NullValue = Resources.delete;

            gvBonSerahTahanan.Columns[2].IsVisible = true;
            gvBonSerahTahanan.Columns[2].ReadOnly = false;
            gvBonSerahTahanan.Columns[2].NullValue = Resources.print;

            gvBonSerahTahanan.Columns["Id"].IsVisible = false;
            gvBonSerahTahanan.Columns["Id"].HeaderText = "Id";
            gvBonSerahTahanan.Columns["Id"].Width = 100;

            gvBonSerahTahanan.Columns["NoBonTahanan"].IsVisible = true;
            gvBonSerahTahanan.Columns["NoBonTahanan"].HeaderText = "NO BON TAHANAN";
            gvBonSerahTahanan.Columns["NoBonTahanan"].Width = 200;

            //gvBonSerahTahanan.Columns["NamaTahanan"].IsVisible = true;
            //gvBonSerahTahanan.Columns["NamaTahanan"].HeaderText = "NAMA TAHANAN";
            //gvBonSerahTahanan.Columns["NamaTahanan"].Width = 200;

            //gvBonSerahTahanan.Columns["CellCode"].IsVisible = true;
            //gvBonSerahTahanan.Columns["CellCode"].HeaderText = "NOMOR KAMAR";
            //gvBonSerahTahanan.Columns["CellCode"].Width = 200;

            gvBonSerahTahanan.Columns["TanggalTerima"].IsVisible = true;
            gvBonSerahTahanan.Columns["TanggalTerima"].HeaderText = "TANGGAL DISERAHKAN";
            gvBonSerahTahanan.Columns["TanggalTerima"].Width = 200;

            gvBonSerahTahanan.Columns["NamaYangMenyerahkan"].IsVisible = true;
            gvBonSerahTahanan.Columns["NamaYangMenyerahkan"].HeaderText = "YANG MENYERAHKAN";
            gvBonSerahTahanan.Columns["NamaYangMenyerahkan"].Width = 200;

            gvBonSerahTahanan.Columns["NamaYangMenerima"].IsVisible = true;
            gvBonSerahTahanan.Columns["NamaYangMenerima"].HeaderText = "YANG MENERIMA";
            gvBonSerahTahanan.Columns["NamaYangMenerima"].Width = 200;
        }

        public static void LoadDataBonTerimaTahananToGrid(RadGridView gvBonTerimaTahanan)
        {
            gvBonTerimaTahanan.DataSource = null;
            BonTerimaTahananService bonServ = new BonTerimaTahananService();
            gvBonTerimaTahanan.AutoGenerateColumns = true;
            gvBonTerimaTahanan.DataSource = bonServ.Get();
            gvBonTerimaTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvBonTerimaTahanan.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvBonTerimaTahanan.Columns[0].Width = 30;
            gvBonTerimaTahanan.Columns[1].Width = 30;
            gvBonTerimaTahanan.Columns[2].Width = 30;

            gvBonTerimaTahanan.Columns[0].IsVisible = true;
            gvBonTerimaTahanan.Columns[0].ReadOnly = false;
            gvBonTerimaTahanan.Columns[0].NullValue = Resources.edit1;

            gvBonTerimaTahanan.Columns[1].IsVisible = true;
            gvBonTerimaTahanan.Columns[1].ReadOnly = false;
            gvBonTerimaTahanan.Columns[1].NullValue = Resources.delete;

            gvBonTerimaTahanan.Columns[2].IsVisible = true;
            gvBonTerimaTahanan.Columns[2].ReadOnly = false;
            gvBonTerimaTahanan.Columns[2].NullValue = Resources.print;

            gvBonTerimaTahanan.Columns["Id"].IsVisible = false;
            gvBonTerimaTahanan.Columns["Id"].HeaderText = "Id";
            gvBonTerimaTahanan.Columns["Id"].Width = 100;

            gvBonTerimaTahanan.Columns["NoBonTahanan"].IsVisible = true;
            gvBonTerimaTahanan.Columns["NoBonTahanan"].HeaderText = "NO BON TAHANAN";
            gvBonTerimaTahanan.Columns["NoBonTahanan"].Width = 200;

            //gvBonTerimaTahanan.Columns["NamaTahanan"].IsVisible = true;
            //gvBonTerimaTahanan.Columns["NamaTahanan"].HeaderText = "NAMA TAHANAN";
            //gvBonTerimaTahanan.Columns["NamaTahanan"].Width = 200;

            //gvBonTerimaTahanan.Columns["CellCode"].IsVisible = true;
            //gvBonTerimaTahanan.Columns["CellCode"].HeaderText = "NOMOR KAMAR";
            //gvBonTerimaTahanan.Columns["CellCode"].Width = 200;

            gvBonTerimaTahanan.Columns["TanggalTerima"].IsVisible = true;
            gvBonTerimaTahanan.Columns["TanggalTerima"].HeaderText = "TANGGAL DITERIMA";
            gvBonTerimaTahanan.Columns["TanggalTerima"].Width = 200;

            gvBonTerimaTahanan.Columns["NamaYangMenerima"].IsVisible = true;
            gvBonTerimaTahanan.Columns["NamaYangMenerima"].HeaderText = "YANG MENERIMA";
            gvBonTerimaTahanan.Columns["NamaYangMenerima"].Width = 200;

            gvBonTerimaTahanan.Columns["NamaYangMenyerahkan"].IsVisible = true;
            gvBonTerimaTahanan.Columns["NamaYangMenyerahkan"].HeaderText = "YANG MENYERAHKAN";
            gvBonTerimaTahanan.Columns["NamaYangMenyerahkan"].Width = 200;
        }

        public static void LoadDataJadwalKunjunganToGrid(RadGridView gvJadwalKunjungan)
        {
            gvJadwalKunjungan.DataSource = null;
            JadwalKunjunganService user = new JadwalKunjunganService();
            gvJadwalKunjungan.AutoGenerateColumns = true;
            gvJadwalKunjungan.DataSource = user.Get();

            gvJadwalKunjungan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvJadwalKunjungan.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvJadwalKunjungan.Columns[0].Width = 20;
            gvJadwalKunjungan.Columns[1].Width = 20;
            gvJadwalKunjungan.Columns[2].Width = 20;

            gvJadwalKunjungan.Columns[0].IsVisible = true;
            gvJadwalKunjungan.Columns[0].ReadOnly = false;
            gvJadwalKunjungan.Columns[0].NullValue = Resources.view_detail;

            gvJadwalKunjungan.Columns[1].IsVisible = true;
            gvJadwalKunjungan.Columns[1].ReadOnly = false;
            gvJadwalKunjungan.Columns[1].NullValue = Resources.edit1;

            gvJadwalKunjungan.Columns[2].IsVisible = true;
            gvJadwalKunjungan.Columns[2].ReadOnly = false;
            gvJadwalKunjungan.Columns[2].NullValue = Resources.delete;


            gvJadwalKunjungan.Columns["InmatesRegID"].IsVisible = true;
            gvJadwalKunjungan.Columns["InmatesRegID"].HeaderText = "INMATES REG ID";
            gvJadwalKunjungan.Columns["InmatesRegID"].Width = 100;

            gvJadwalKunjungan.Columns["InmatesName"].IsVisible = true;
            gvJadwalKunjungan.Columns["InmatesName"].HeaderText = "INMATES NAME";
            gvJadwalKunjungan.Columns["InmatesName"].Width = 100;

            gvJadwalKunjungan.Columns["BolehDikunjungi"].IsVisible = true;
            gvJadwalKunjungan.Columns["BolehDikunjungi"].HeaderText = "BOLEH DIKUNJUNGI";
            gvJadwalKunjungan.Columns["BolehDikunjungi"].Width = 100;

            gvJadwalKunjungan.Columns["PeriodeKunjungan"].IsVisible = true;
            gvJadwalKunjungan.Columns["PeriodeKunjungan"].HeaderText = "PERIODE KUNJUNGAN";
            gvJadwalKunjungan.Columns["PeriodeKunjungan"].Width = 100;

            gvJadwalKunjungan.Columns["WaktuKunjungan"].IsVisible = true;
            gvJadwalKunjungan.Columns["WaktuKunjungan"].HeaderText = "WAKTU KUNJUNGAN";
            gvJadwalKunjungan.Columns["WaktuKunjungan"].Width = 100;

            gvJadwalKunjungan.Columns["CreatedBy"].IsVisible = true;
            gvJadwalKunjungan.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvJadwalKunjungan.Columns["CreatedBy"].Width = 100;

            gvJadwalKunjungan.Columns["CreatedDate"].IsVisible = true;
            gvJadwalKunjungan.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvJadwalKunjungan.Columns["CreatedDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvJadwalKunjungan.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvJadwalKunjungan.Columns["CreatedDate"].Width = 100;

            //gvJadwalKunjungan.Columns["UpdatedBy"].IsVisible = true;
            //gvJadwalKunjungan.Columns["UpdatedBy"].HeaderText = "UPDATE OLEH";
            //gvJadwalKunjungan.Columns["UpdatedBy"].Width = 100;

            //gvJadwalKunjungan.Columns["UpdatedDate"].IsVisible = true;
            //gvJadwalKunjungan.Columns["UpdatedDate"].HeaderText = "TGL UPDATE";
            //gvJadwalKunjungan.Columns["UpdatedDate"].FormatString = "{0:dd MMM yyyy hh:mm:ss}";
            //gvJadwalKunjungan.Columns["UpdatedDate"].Width = 100;

        }

        public static void LoadDataPegunjungVIPToGrid(RadGridView gvVisitorVIP)
        {
            gvVisitorVIP.DataSource = null;
            VisitorVIPService pegunjung = new VisitorVIPService();
            gvVisitorVIP.AutoGenerateColumns = true;
            gvVisitorVIP.DataSource = pegunjung.GetBelumCheckedOut();

            gvVisitorVIP.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvVisitorVIP.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvVisitorVIP.Columns[0].Width = 20;
            gvVisitorVIP.Columns[1].Width = 20;
            gvVisitorVIP.Columns[2].Width = 20;

            gvVisitorVIP.Columns[0].IsVisible = true;
            gvVisitorVIP.Columns[0].ReadOnly = false;
            gvVisitorVIP.Columns[0].NullValue = Resources.view_detail;

            gvVisitorVIP.Columns[1].IsVisible = true;
            gvVisitorVIP.Columns[1].ReadOnly = false;
            gvVisitorVIP.Columns[1].NullValue = Resources.edit1;


            gvVisitorVIP.Columns[2].IsVisible = true;
            gvVisitorVIP.Columns[2].ReadOnly = false;
            gvVisitorVIP.Columns[2].NullValue = Resources.checkedout;

            gvVisitorVIP.Columns["FullName"].IsVisible = true;
            gvVisitorVIP.Columns["FullName"].HeaderText = "NAMA LENGKAP PENGUNJUNG";
            gvVisitorVIP.Columns["FullName"].Width = 200;

            gvVisitorVIP.Columns["IDType"].IsVisible = true;
            gvVisitorVIP.Columns["IDType"].HeaderText = "JENIS IDENTITAS";
            gvVisitorVIP.Columns["IDType"].Width = 100;

            gvVisitorVIP.Columns["IDNo"].IsVisible = true;
            gvVisitorVIP.Columns["IDNo"].HeaderText = "NO IDENTITAS";
            gvVisitorVIP.Columns["IDNo"].Width = 100;

            gvVisitorVIP.Columns["StartVisit"].IsVisible = true;
            gvVisitorVIP.Columns["StartVisit"].HeaderText = "WAKTU BERKUNJUNG";
            gvVisitorVIP.Columns["StartVisit"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvVisitorVIP.Columns["StartVisit"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvVisitorVIP.Columns["StartVisit"].Width = 100;

            gvVisitorVIP.Columns["CreatedBy"].IsVisible = true;
            gvVisitorVIP.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvVisitorVIP.Columns["CreatedBy"].Width = 100;

            gvVisitorVIP.Columns["CreatedDate"].IsVisible = true;
            gvVisitorVIP.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvVisitorVIP.Columns["CreatedDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvVisitorVIP.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvVisitorVIP.Columns["CreatedDate"].Width = 100;

        }

        public static void LoadDataKunjunganVIPToGrid(RadGridView gvVisitorVIP)
        {
            gvVisitorVIP.DataSource = null;
            VisitorVIPService pegunjung = new VisitorVIPService();
            gvVisitorVIP.AutoGenerateColumns = true;
            gvVisitorVIP.DataSource = pegunjung.GetSudahCheckedOut();

            gvVisitorVIP.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvVisitorVIP.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvVisitorVIP.Columns[0].Width = 20;

            gvVisitorVIP.Columns[0].IsVisible = true;
            gvVisitorVIP.Columns[0].ReadOnly = false;
            gvVisitorVIP.Columns[0].NullValue = Resources.view_detail;

            gvVisitorVIP.Columns["FullName"].IsVisible = true;
            gvVisitorVIP.Columns["FullName"].HeaderText = "NAMA LENGKAP PENGUNJUNG";
            gvVisitorVIP.Columns["FullName"].Width = 200;

            gvVisitorVIP.Columns["IDType"].IsVisible = true;
            gvVisitorVIP.Columns["IDType"].HeaderText = "JENIS IDENTITAS";
            gvVisitorVIP.Columns["IDType"].Width = 100;

            gvVisitorVIP.Columns["IDNo"].IsVisible = true;
            gvVisitorVIP.Columns["IDNo"].HeaderText = "NO IDENTITAS";
            gvVisitorVIP.Columns["IDNo"].Width = 100;

            gvVisitorVIP.Columns["StartVisit"].IsVisible = true;
            gvVisitorVIP.Columns["StartVisit"].HeaderText = "WAKTU MULAI BERKUNJUNG";
            gvVisitorVIP.Columns["StartVisit"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvVisitorVIP.Columns["StartVisit"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvVisitorVIP.Columns["StartVisit"].Width = 100;

            gvVisitorVIP.Columns["EndVisit"].IsVisible = true;
            gvVisitorVIP.Columns["EndVisit"].HeaderText = "WAKTU SELESAI BERKUNJUNG";
            gvVisitorVIP.Columns["EndVisit"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvVisitorVIP.Columns["EndVisit"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvVisitorVIP.Columns["EndVisit"].Width = 100;

            gvVisitorVIP.Columns["CreatedBy"].IsVisible = true;
            gvVisitorVIP.Columns["CreatedBy"].HeaderText = "DIBUAT OLEH";
            gvVisitorVIP.Columns["CreatedBy"].Width = 100;

            gvVisitorVIP.Columns["CreatedDate"].IsVisible = true;
            gvVisitorVIP.Columns["CreatedDate"].HeaderText = "DIBUAT TANGGAL";
            gvVisitorVIP.Columns["CreatedDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvVisitorVIP.Columns["CreatedDate"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvVisitorVIP.Columns["CreatedDate"].Width = 100;

        }

        public static void SetInitGridView(RadGridView gridcontrol)
        {
            gridcontrol.TableElement.CellSpacing = 1;
            gridcontrol.RootElement.EnableElementShadow = false;
            gridcontrol.GridViewElement.DrawFill = false;
            gridcontrol.TableElement.Margin = new Padding(0, 0, 0, 0);
            gridcontrol.BackColor = Color.Transparent;
            gridcontrol.GridViewElement.DrawFill = true;
            gridcontrol.AllowAddNewRow = false;
            gridcontrol.EnableGrouping = false;
            gridcontrol.EnableFiltering = true;
            gridcontrol.EnableCustomFiltering = true;
            gridcontrol.ShowFilteringRow = false;
            gridcontrol.GridViewElement.CustomFont = Global.MainFont;
            gridcontrol.GridViewElement.CustomFontSize = Global.CustomFontSizeMain;
            gridcontrol.TableElement.CustomFontStyle = FontStyle.Bold;
            gridcontrol.TableElement.CustomFontSize = 20f;
            gridcontrol.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            gridcontrol.AutoGenerateColumns = false;
            gridcontrol.AllowSearchRow = true;
            gridcontrol.EnablePaging = true;
            gridcontrol.PageSize = Convert.ToInt32(Convert.ToInt32(UserFunction.GetSettings(SaveSettingsType.GridPagingSize))==0?20: Convert.ToInt32(UserFunction.GetSettings(SaveSettingsType.GridPagingSize)));
            gridcontrol.GridViewElement.PagingPanelElement.TextBoxStripElement.CustomFont  = Global.MainFont;
            gridcontrol.GridViewElement.PagingPanelElement.TextBoxStripElement.CustomFontSize = 12f;
            gridcontrol.EnableFiltering = true;
            gridcontrol.MasterTemplate.ShowHeaderCellButtons = true;
            gridcontrol.MasterTemplate.ShowFilteringRow = false;
            gridcontrol.AllowRowResize = false;
            gridcontrol.AllowRowReorder = false;
            gridcontrol.AllowEditRow = false;

        }

        public static void SetDefaultTableLayoutPanelSetting(TableLayoutPanel tableLayoutPanel1)
        {
            foreach (Control ctrl in tableLayoutPanel1.Controls)
            {
                if (ctrl.GetType() == typeof(RadTextBox))
                {
                    RadTextBox txt = ((RadTextBox)ctrl);
                    if (txt.Name.StartsWith("txt"))
                    {
                        txt.TextBoxElement.CustomFont = Global.MainFont;
                        txt.TextBoxElement.CustomFontSize = 12.5f;
                        //txt.ForeColor = Color.FromArgb(89, 89, 89);
                        txt.ForeColor = Color.Navy;
                        txt.BackColor = Color.Transparent;
                        txt.CharacterCasing = CharacterCasing.Upper;
                        if (txt.Multiline == true)
                            txt.TextBoxElement.Border.ForeColor = Global.MainColor;

                    }
                    else if (txt.Name.StartsWith("Usertxt"))
                    {
                        txt.TextBoxElement.CustomFont = Global.MainFont;
                        txt.TextBoxElement.CustomFontSize = 12.5f;
                        //txt.ForeColor = Color.FromArgb(89, 89, 89);
                        txt.ForeColor = Color.Navy;
                        txt.BackColor = Color.Transparent;
                        if (txt.Multiline == true)
                            txt.TextBoxElement.Border.ForeColor = Global.MainColor;
                    }
                    else if (txt.Name.StartsWith("psw"))
                    {
                        txt.TextBoxElement.CustomFont = Global.MainFont;
                        txt.TextBoxElement.CustomFontSize = 12.5f;
                        txt.ForeColor = Color.Navy;
                        txt.BackColor = Color.Transparent;
                    }


                }

                if (ctrl.GetType() == typeof(Panel))
                {
                    foreach (Control ctrl2 in ctrl.Controls)
                    {
                        if (ctrl2.GetType() == typeof(RadRadioButton))
                        {
                            RadRadioButton radio = ((RadRadioButton)ctrl2);
                            radio.ButtonElement.TextElement.CustomFont = Global.MainFont;
                            radio.ButtonElement.TextElement.CustomFontSize = Global.CustomFontSizeMain;
                            radio.ForeColor = Color.FromArgb(89, 89, 89);
                            radio.BackColor = Color.Transparent;
                            radio.ButtonElement.CheckMarkPrimitive.FindDescendant<RadioPrimitive>().DefaultSize = new Size(20, 20);
                            radio.ButtonElement.CheckMarkPrimitive.FindDescendant<RadioPrimitive>().MinSize = new Size(20, 20);
                            radio.ButtonElement.CheckMarkPrimitive.FindDescendant<RadioPrimitive>().Margin = new Padding(10, 4, 5, 5);
                        }

                        if (ctrl2.GetType() == typeof(RadCheckedDropDownList))
                        {
                            RadCheckedDropDownList ddl = ((RadCheckedDropDownList)ctrl2);
                            ddl.DropDownListElement.CustomFont = Global.MainFont;
                            ddl.DropDownListElement.CustomFontSize = Global.CustomFontSizeMain;

                            ddl.DropDownListElement.CustomFontSize = Global.CustomFontSizeMain;
                            ddl.ListElement.CustomFontSize = Global.CustomFontSizeMain;

                            //ddl.ForeColor = Color.FromArgb(89, 89, 89);
                            ddl.ForeColor = Color.Navy;

                            ddl.BackColor = Color.Transparent;//FromArgb(0, 255, 255, 255);
                            ddl.ListElement.DrawBorder = true;
                            ddl.AutoSizeItems = false;
                            ddl.Padding = new Padding(5, 0, 0, 0);
                        }

                    }
                }

                if (ctrl.GetType() == typeof(RadPanel))
                {
                    foreach (Control ctrl2 in ctrl.Controls)
                    {

                        if (ctrl2.GetType() == typeof(RadRadioButton))
                        {
                            RadRadioButton radio = ((RadRadioButton)ctrl2);
                            radio.ButtonElement.TextElement.CustomFont = Global.MainFont;
                            radio.ButtonElement.TextElement.CustomFontSize = Global.CustomFontSizeMain;
                            radio.ForeColor = Color.FromArgb(89, 89, 89);
                            radio.BackColor = Color.Transparent;
                            radio.ButtonElement.CheckMarkPrimitive.FindDescendant<RadioPrimitive>().DefaultSize = new Size(20, 20);
                            radio.ButtonElement.CheckMarkPrimitive.FindDescendant<RadioPrimitive>().MinSize = new Size(20, 20);
                            radio.ButtonElement.CheckMarkPrimitive.FindDescendant<RadioPrimitive>().Margin = new Padding(5, 4, 5, 5);
                        }

                        if (ctrl2.GetType() == typeof(RadLabel))
                        {
                            RadLabel label = ((RadLabel)ctrl2);
                            label.LabelElement.CustomFont = Global.MainFont;
                            label.LabelElement.CustomFontSize = 13.5f;
                            label.LabelElement.CustomFontStyle = FontStyle.Bold;
                            //label.ForeColor = Color.FromArgb(89, 89, 89);
                            label.ForeColor = Color.Navy;
                            label.BackColor = Color.Transparent;
                            label.Text = label.Text.ToUpper();
                        }

                        if (ctrl2.GetType() == typeof(RadTextBox))
                        {
                            RadTextBox txt = ((RadTextBox)ctrl2);
                            if (txt.Name.StartsWith("txt"))
                            {
                                txt.TextBoxElement.CustomFont = Global.MainFont;
                                txt.TextBoxElement.CustomFontSize = 12.5f;
                                //txt.ForeColor = Color.FromArgb(89, 89, 89);
                                txt.ForeColor = Color.Navy;
                                txt.BackColor = Color.Transparent;
                                txt.CharacterCasing = CharacterCasing.Upper;
                                if (txt.Multiline == true)
                                    txt.TextBoxElement.Border.ForeColor = Global.MainColor;

                            }
                            else if (txt.Name.StartsWith("psw"))
                            {
                                txt.TextBoxElement.CustomFont = Global.MainFont;
                                txt.TextBoxElement.CustomFontSize = 12.5f;
                                txt.ForeColor = Color.Navy;
                                txt.BackColor = Color.Transparent;
                            }


                        }

                        if (ctrl2.GetType() == typeof(RadDateTimePicker))
                        {
                            RadDateTimePicker dt = ((RadDateTimePicker)ctrl2);
                            dt.DateTimePickerElement.CustomFont = Global.MainFont;
                            dt.DateTimePickerElement.CustomFontSize = 12.5f;
                            dt.DateTimePickerElement.ForeColor = Color.FromArgb(33, 33, 33);
                            dt.DateTimePickerElement.BackColor = Color.Transparent;
                            dt.Format = DateTimePickerFormat.Custom;
                            dt.CustomFormat = "dd MMMM yyyy";
                            dt.DateTimePickerElement.CalendarSize = new Size(350, 380);
                            dt.DateTimePickerElement.TextBoxElement.Padding = new Padding(10, 0, 0, 0);
                            //dt.DateTimePickerElement.ElementTree.RootElement.  = true;
                            dt.BackColor = Color.Transparent;
                            dt.RootElement.EnableElementShadow = false;
                            dt.Culture = new System.Globalization.CultureInfo("id");
                            dt.Value = DateTime.Today.ToLocalTime();

                            var data = dt.DateTimePickerElement.Parent.Parent;

                            if (dt.Name.StartsWith("datetime"))
                            {
                                dt.DateTimePickerElement.ShowTimePicker = true;
                                dt.Format = DateTimePickerFormat.Custom;
                                dt.CustomFormat = "dd MMMM yyyy hh:mm tt";
                                (dt.DateTimePickerElement.CurrentBehavior as RadDateTimePickerCalendar).DropDownMinSize = new System.Drawing.Size(330, 250);
                            }
                        }

                        if (ctrl2.GetType() == typeof(RadTimePicker))
                        {
                            RadTimePicker dt = ((RadTimePicker)ctrl2);
                            dt.TimePickerElement.CustomFont = Global.MainFont;
                            dt.TimePickerElement.CustomFontSize = 12.5f;
                            dt.TimePickerElement.ForeColor = Color.FromArgb(33, 33, 33);
                            dt.TimePickerElement.BackColor = Color.Transparent;
                            dt.TimePickerElement.Format = "hh:mm";
                            //dt.DateTimePickerElement.ElementTree.RootElement.  = true;
                            dt.BackColor = Color.Transparent;
                            dt.RootElement.EnableElementShadow = false;
                            dt.Culture = new System.Globalization.CultureInfo("id");
                            dt.Value = DateTime.Today.ToLocalTime();

                            var data = dt.TimePickerElement.Parent.Parent;
                        }

                        if (ctrl2.GetType() == typeof(RadDropDownList))
                        {
                            RadDropDownList ddl = ((RadDropDownList)ctrl2);
                            ddl.DropDownListElement.CustomFont = Global.MainFont;
                            ddl.DropDownListElement.CustomFontSize = Global.CustomFontSizeMain;
                            ddl.ListElement.CustomFontSize = Global.CustomFontSizeMain;

                            //ddl.ForeColor = Color.FromArgb(89, 89, 89);
                            ddl.ForeColor = Color.Navy;

                            ddl.BackColor = Color.Transparent;//FromArgb(0, 255, 255, 255);
                            ddl.ListElement.DrawBorder = true;
                            ddl.AutoSizeItems = false;
                            ddl.Padding = new Padding(5, 0, 0, 0);

                        }

                        if (ctrl2.GetType() == typeof(RadCheckedDropDownList))
                        {
                            RadDropDownList ddl = ((RadDropDownList)ctrl2);
                            ddl.DropDownListElement.CustomFont = Global.MainFont;
                            ddl.DropDownListElement.CustomFontSize = Global.CustomFontSizeMain;
                            ddl.ListElement.CustomFontSize = Global.CustomFontSizeMain;

                            //ddl.ForeColor = Color.FromArgb(89, 89, 89);
                            ddl.ForeColor = Color.Navy;

                            ddl.BackColor = Color.Transparent;//FromArgb(0, 255, 255, 255);
                            ddl.ListElement.DrawBorder = true;
                            ddl.AutoSizeItems = false;
                            ddl.Padding = new Padding(5, 0, 0, 0);

                        }

                    }
                }

                if (ctrl.GetType() == typeof(RadLabel))
                {
                    RadLabel label = ((RadLabel)ctrl);
                    label.LabelElement.CustomFont = Global.MainFont;
                    label.LabelElement.CustomFontSize = 13.5f;
                    label.LabelElement.CustomFontStyle = FontStyle.Bold;
                    label.ForeColor = Color.FromArgb(89, 89, 89);
                    //label.ForeColor = Color.Black;
                    label.BackColor = Color.Transparent;
                    label.Text = label.Text.ToUpper();

                    if (label.Name.Contains("lblDetail") || label.Name.Contains("lblData"))
                    {
                        label.ForeColor = Color.FromArgb(192, 57, 43);
                    }

                }

                if (ctrl.GetType() == typeof(RadDropDownList))
                {
                    RadDropDownList ddl = ((RadDropDownList)ctrl);
                    ddl.DropDownListElement.CustomFont = Global.MainFont;
                    ddl.DropDownListElement.CustomFontSize = Global.CustomFontSizeMain;

                    ddl.DropDownListElement.CustomFontSize = Global.CustomFontSizeMain;
                    ddl.ListElement.CustomFontSize = Global.CustomFontSizeMain;

                    //ddl.ForeColor = Color.FromArgb(89, 89, 89);
                    ddl.ForeColor = Color.Navy;

                    ddl.BackColor = Color.Transparent;//FromArgb(0, 255, 255, 255);
                    ddl.ListElement.DrawBorder = true;
                    ddl.AutoSizeItems = false;
                    ddl.Padding = new Padding(5, 0, 0, 0);

                }

                if (ctrl.GetType() == typeof(RadCheckBox))
                {
                    RadCheckBox cbk = ((RadCheckBox)ctrl);
                    cbk.BackColor = Color.Transparent;
                    cbk.ButtonElement.Size = new System.Drawing.Size(270, 50);
                    cbk.RootElement.EnableElementShadow = false;
                    //cbk.ButtonElement.CheckMarkPrimitive.Border.BoxStyle = Telerik.WinControls.BorderBoxStyle.SingleBorder;
                    //cbk.ButtonElement.TextElement.Margin = new Padding(10, 0, 0, 0);
                    //cbk.ButtonElement.TextElement.CustomFont = Global.MainFont;
                    //cbk.ButtonElement.TextElement.CustomFontSize = Global.CustomFontSizeMain;
                    //cbk.ButtonElement.TextElement.ForeColor = Color.FromArgb(89, 89, 89);

                    //cbk.ButtonElement.CheckMarkPrimitive.FindDescendant<CheckPrimitive>().DefaultSize = new Size(20, 20);
                    //cbk.ButtonElement.CheckMarkPrimitive.FindDescendant<CheckPrimitive>().MinSize = new Size(20, 20);
                    //cbk.ButtonElement.CheckMarkPrimitive.FindDescendant<CheckPrimitive>().Margin = new Padding(5, 4, 5, 5);

                }

                if (ctrl.GetType() == typeof(RadDateTimePicker))
                {
                    RadDateTimePicker dt = ((RadDateTimePicker)ctrl);
                    dt.DateTimePickerElement.CustomFont = Global.MainFont;
                    dt.DateTimePickerElement.CustomFontSize = 12.5f;
                    dt.DateTimePickerElement.ForeColor = Color.FromArgb(33, 33, 33);
                    dt.DateTimePickerElement.BackColor = Color.Transparent;
                    dt.Format = DateTimePickerFormat.Custom;
                    dt.CustomFormat = "dd MMMM yyyy";
                    dt.DateTimePickerElement.CalendarSize = new Size(350, 380);
                    dt.DateTimePickerElement.TextBoxElement.Padding = new Padding(10, 0, 0, 0);
                    //dt.DateTimePickerElement.ElementTree.RootElement.  = true;
                    dt.BackColor = Color.Transparent;
                    dt.RootElement.EnableElementShadow = false;
                    dt.Culture = new System.Globalization.CultureInfo("id");
                    dt.Value = DateTime.Today.ToLocalTime();

                    var data = dt.DateTimePickerElement.Parent.Parent;
                }

                if (ctrl.GetType() == typeof(RadTimePicker))
                {
                    RadTimePicker dt = ((RadTimePicker)ctrl);
                    dt.TimePickerElement.CustomFont = Global.MainFont;
                    dt.TimePickerElement.CustomFontSize = 12.5f;
                    dt.TimePickerElement.ForeColor = Color.FromArgb(33, 33, 33);
                    dt.TimePickerElement.BackColor = Color.Transparent;
                    dt.TimePickerElement.EditorElement.Padding = new Padding(10, 0, 0, 0);
                    dt.RootElement.EnableElementShadow = false;
                    dt.BackColor = Color.Transparent;
                }

                if (ctrl.GetType() == typeof(RadLabel))
                {
                    RadLabel lbl = ((RadLabel)ctrl);
                    if (lbl.Name.StartsWith("lblDetail") || ctrl.Name == "lblData")
                    {
                        RadLabel label = ((RadLabel)ctrl);
                        label.ForeColor = Color.FromArgb(192, 57, 43);
                        label.LabelElement.CustomFontSize = 15f;
                        label.LabelElement.CustomFontStyle = FontStyle.Bold;
                        label.BackColor = Color.Transparent;
                    }
                }
            }

        }

        public static bool CheckUpdate(RadButton btn)
        {
            return false;
        }

        private static bool DownloadUpdate(RadButton btn,string versi)
        {
            return true;
        }

        private static void wc_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {

        }

        public static bool IsOnline()
        {
            try
            {
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool Ping()
        {
            bool pingable = false;
            return pingable;
        }

        public static string Capitalize(string s)
        {
            s = s.ToLower();
            var result = Regex.Replace(s, @"\b(\w)", m => m.Value.ToUpper());
            result = Regex.Replace(result, @"(\s(dari|di|oleh|dan)|\'[st])\b", m => m.Value.ToLower(), RegexOptions.IgnoreCase);
            return result;
        }

        

        public static void ClearControls(Control control)
        {
            foreach (Control ctrl in control.Controls)
            {
                if (ctrl.GetType() == typeof(RadTextBox)) { 
                    ctrl.Text = string.Empty;
                    ctrl.Enabled = true;
                }
                if (ctrl.GetType() == typeof(RadDropDownList)) { 
                    ((RadDropDownList)ctrl).SelectedIndex = 0;
                    ((RadDropDownList)ctrl).Enabled = true;
                }
                if (ctrl.GetType() == typeof(RadCheckBox)) { 
                    ((RadCheckBox)ctrl).Checked = false;
                    ((RadCheckBox)ctrl).Enabled = true;
                }
                if (ctrl.GetType() == typeof(RadPanel))
                {
                    foreach (Control ctrl2 in ctrl.Controls)
                    {
                        if (ctrl2.GetType() == typeof(RadTextBox)) { 
                            ctrl2.Text = string.Empty;
                            ctrl2.Enabled = true;
                        }
                        if (ctrl2.GetType() == typeof(RadDropDownList)) { 
                            ((RadDropDownList)ctrl2).SelectedText = string.Empty;
                            ((RadDropDownList)ctrl2).Enabled = true;
                        }
                        if (ctrl2.GetType() == typeof(RadCheckBox)) { 
                            ((RadCheckBox)ctrl2).Checked = false;
                            ((RadCheckBox)ctrl2).Enabled = true;
                        }
                    }
                }
            }
        }

        public static string GetIPAddress()
        {
            return Dns.GetHostEntry(Dns.GetHostName())
                       .AddressList.First(
                           f => f.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                       .ToString();
        }

        public static DialogResult MsgBox(TipeMsg title, string bodymsg)
        {
            RadMessageBox.SetThemeName("Material");
            DialogResult ds;
            switch (title)
            {
                case TipeMsg.Gagal:
                    ds = RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen },bodymsg, title.ToString(), MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    break;
                case TipeMsg.Error:
                    ds = RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, bodymsg, title.ToString(), MessageBoxButtons.OK, RadMessageIcon.Error);
                    break;
                case TipeMsg.Info:
                    ds = RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, bodymsg, title.ToString(), MessageBoxButtons.OK, RadMessageIcon.Info);
                    break;
                case TipeMsg.Konfirmasi:
                    ds = RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, bodymsg, title.ToString(), MessageBoxButtons.YesNo, RadMessageIcon.Question);
                    break;
                default:
                    ds = RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, bodymsg, title.ToString(), MessageBoxButtons.OK, RadMessageIcon.None);
                    break;
            }
            return ds;
        }

        public static DialogResult Confirm(string bodymsg)
        {
            return RadMessageBox.Show("<html><size=11>" + bodymsg, "Konfirmasi", MessageBoxButtons.YesNo, RadMessageIcon.Question);
        }

        public static void SaveActivitiesLog(AuditTrail data)
        {
            AuditTrailService audittrailserv = new AuditTrailService();
            data.UserName = GetSettings(SaveSettingsType.UserID);
            data.LogDate = DateTime.Now.ToLocalTime();
            audittrailserv.Post(data);
        }

        public static string JsonString(object data)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(data);
        }

        public static void SaveSettings(SaveSettingsType tipe, object val)
        {
            if (tipe == SaveSettingsType.View)
            {
                Properties.Settings.Default.ViewMode = (int)val;
                Properties.Settings.Default.Save();
            }

            if (tipe == SaveSettingsType.UserID)
            {
                Properties.Settings.Default.UserID = val.ToString();
                Properties.Settings.Default.Save();
            }

            if (tipe == SaveSettingsType.NoHP)
            {
                Properties.Settings.Default.NoHP = val.ToString();
                Properties.Settings.Default.Save();
            }

            if (tipe == SaveSettingsType.LastLogin)
            {
                Properties.Settings.Default.LastLogin = val.ToString();
                Properties.Settings.Default.Save();
            }

            if (tipe == SaveSettingsType.DBDataSource)
            {
                Properties.Settings.Default.DBDataSource = val.ToString();
                Properties.Settings.Default.Save();
            }

            if (tipe == SaveSettingsType.DBUserName)
            {
                Properties.Settings.Default.DBUserName = val.ToString();
                Properties.Settings.Default.Save();
            }

            if (tipe == SaveSettingsType.DBPassword)
            {
                Properties.Settings.Default.DBPassword = val.ToString();
                Properties.Settings.Default.Save();
            }

            if (tipe == SaveSettingsType.DashboardRefresinterval)
            {
                Properties.Settings.Default.DashboardRefresinterval = Convert.ToInt32(val);
                Properties.Settings.Default.Save();
            }

            if (tipe == SaveSettingsType.CellCodeSeperator1)
            {
                Properties.Settings.Default.CellCodeSeperator1 =  val.ToString();
                Properties.Settings.Default.Save();
            }

            if (tipe == SaveSettingsType.CellCodeSeperator2)
            {
                Properties.Settings.Default.CellCodeSeperator2 = val.ToString();
                Properties.Settings.Default.Save();
            }

            if (tipe == SaveSettingsType.GridPagingSize)
            {
                Properties.Settings.Default.GridPagingSize = Convert.ToInt32(val);
                Properties.Settings.Default.Save();
            }

            if (tipe == SaveSettingsType.RawDataPassword)
            {
                Properties.Settings.Default.RawDataPsw = val.ToString();
                Properties.Settings.Default.Save();
            }
        }


        public static string GetSettings(SaveSettingsType tipe)
        {
            if (tipe == SaveSettingsType.View)
            {
                return Convert.ToString(Properties.Settings.Default.ViewMode);
            }

            if (tipe == SaveSettingsType.UserID)
            {
                return Properties.Settings.Default.UserID;
            }
            if (tipe == SaveSettingsType.NoHP)
            {
                return Properties.Settings.Default.NoHP;
            }


            if (tipe == SaveSettingsType.LastLogin)
            {
                return  Properties.Settings.Default.LastLogin;
            }

            if (tipe == SaveSettingsType.DBDataSource)
            {
                return Convert.ToString(Properties.Settings.Default.DBDataSource);
            }

            if (tipe == SaveSettingsType.DBUserName)
            {
                return Convert.ToString(Properties.Settings.Default.DBUserName);
            }

            if (tipe == SaveSettingsType.DBPassword)
            {
                return Convert.ToString(Properties.Settings.Default.DBPassword);
            }

            if (tipe == SaveSettingsType.DashboardRefresinterval)
            {
                return Convert.ToString(Properties.Settings.Default.DashboardRefresinterval);
            }

            if (tipe == SaveSettingsType.CellCodeSeperator1)
            {
                return Convert.ToString(Properties.Settings.Default.CellCodeSeperator1);
            }

            if (tipe == SaveSettingsType.CellCodeSeperator2)
            {
                return Convert.ToString(Properties.Settings.Default.CellCodeSeperator2);
            }

            if (tipe == SaveSettingsType.GridPagingSize)
            {
                return Convert.ToString(Properties.Settings.Default.GridPagingSize);
            }

            if (tipe == SaveSettingsType.RawDataPassword)
            {
				return Convert.ToString(Properties.Settings.Default.RawDataPsw);
            }
            return string.Empty;
        }

		internal static void LoadDDLKeperluanBon(RadDropDownList ddlKeperluanBon)
        {
            //ddl Network
            KeperluanBonTahananService keperluanserv = new KeperluanBonTahananService();
            ddlKeperluanBon.Items.Clear();
            ddlKeperluanBon.DisplayMember = "Nama";
            ddlKeperluanBon.ValueMember = "Id";
            ddlKeperluanBon.DataSource = keperluanserv.Get();
        }
        internal static void LoadDDLNetwork(RadDropDownList dDLNetwork)
        {
            //ddl Network
            NetworkService inmatenetworkserv = new NetworkService();
            dDLNetwork.Items.Clear();
            dDLNetwork.DisplayMember = "Name";
            dDLNetwork.ValueMember = "Id";
            dDLNetwork.DataSource = inmatenetworkserv.Get();
        }
        internal static void LoadDDLKategoriKendaraan(RadDropDownList dDLKategori)
        {
            //ddl Kategori
            KendaraanService serv = new KendaraanService();
            dDLKategori.Items.Clear();
            dDLKategori.DisplayMember = "Nama";
            dDLKategori.ValueMember = "Id";
            dDLKategori.DataSource = serv.Get();
        }

        internal static void LoadDDLCases(RadCheckedDropDownList dDLCases)
        {
            //ddl Cases
            CasesService caseserv = new CasesService();
            dDLCases.Items.Clear();
            dDLCases.DisplayMember = "Name";
            dDLCases.ValueMember = "Id";
            dDLCases.DataSource = caseserv.Get();
        }

        internal static void LoadDDLCategory(RadDropDownList dDLCategory)
        {
            //ddl Category
            SeizedassetsCategoryService assetscategoryserv = new SeizedassetsCategoryService();
            dDLCategory.Items.Clear();
            dDLCategory.DisplayMember = "Nama";
            dDLCategory.ValueMember = "Id";
            dDLCategory.DataSource = assetscategoryserv.Get();
        }

    }

    public class RDMGridViewLocalizationProvider : RadGridLocalizationProvider
    {
        public override string GetLocalizedString(string id)
        {
            switch (id)
            {
                case RadGridStringId.SearchRowTextBoxNullText: return "Masukkan text pencarian";
                case RadGridStringId.PagingPanelOfPagesLabel: return "dari";
                case RadGridStringId.PagingPanelPagesLabel: return "Halaman";
                default:
                    return base.GetLocalizedString(id);
            }
        }
    }

    public class RDMRadMessageLocalizationProvider : RadMessageLocalizationProvider
    {
        public override string GetLocalizedString(string id)
        {
            switch (id)
            {
                case RadMessageStringID.AbortButton: return "Gagal";
                case RadMessageStringID.CancelButton: return "Batal";
                case RadMessageStringID.IgnoreButton: return "Abaikan";
                case RadMessageStringID.NoButton: return "Tidak";
                case RadMessageStringID.OKButton: return "Baik";
                case RadMessageStringID.RetryButton: return "Coba Lagi";
                case RadMessageStringID.YesButton: return "Ya";
                default:
                    return base.GetLocalizedString(id);
            }
        }
    }


    public class Language 
    {
        public static string GetLanguageString(LanguangeId id)
        {
            switch (id)
            {
                //Menu
                case LanguangeId.ADD_NEW_VISIT:return "TAMBAH REGISTRASI BARU";
                case LanguangeId.ADD_NEW_VISITOR: return "TAMBAH PENGUNJUNG BARU";
                case LanguangeId.ADD_NEW_VISITOR_VIP: return "TAMBAH PENGUNJUNG VIP BARU";
                case LanguangeId.ADD_NEW_INMATES: return "TAMBAH TAHANAN BARU";
                case LanguangeId.ADD_NEW_USER: return "TAMBAH PENGGUNA BARU";
                case LanguangeId.ADD_NEW_CELL: return "TAMBAH SEL BARU";
                case LanguangeId.ADD_NEW_ALLOCATION: return "TAMBAH ALOKASI SEL BARU";
                case LanguangeId.ADD_NEW_ASSETS: return "TAMBAH BENDA SITAAN BARU";
				case LanguangeId.ADD_NEW_BON_TAHANAN: return "TAMBAH BON TAHANAN BARU";
                case LanguangeId.ADD_NEW_BON_SERAH_TAHANAN: return "TAMBAH BON SERAH TAHANAN BARU";
                case LanguangeId.ADD_NEW_BON_TERIMA_TAHANAN: return "TAMBAH BON TERIMA TAHANAN BARU";
                case LanguangeId.ADD_NEW_KENDARAAN: return "TAMBAH KENDARAAN BARU";
                case LanguangeId.ADD_NEW_JADWAL_KUNJUNGAN: return "TAMBAH JADWAL KUNJUNGAN BARU";
                case LanguangeId.ADD_NEW_STATUS_TAHANAN: return "TAMBAH STATUS TAHANAN";

                case LanguangeId.SETTINGS: return "PENGATURAN";
                case LanguangeId.WELCOME: return "SELAMAT DATANG";
                case LanguangeId.LAST_LOGIN: return "Terakhir Login";
                case LanguangeId.VERSION: return "Versi";
                //Tooltip
                case LanguangeId.LOGOUT_FROM_SYSTEM: return "Keluar dari sistem";
                case LanguangeId.CHANGE_PASSWORD: return "Ubah Kata Sandi";
                default:return string.Empty;
            }
        }
    }

}
