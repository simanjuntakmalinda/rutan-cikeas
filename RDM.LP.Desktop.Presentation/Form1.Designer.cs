﻿namespace RDM.LP.Desktop.Presentation
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tambahTersangka1 = new RDM.CM.Desktop.Presentation.CustomControls.TambahTersangka();
            this.SuspendLayout();
            // 
            // tambahTersangka1
            // 
            this.tambahTersangka1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tambahTersangka1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tambahTersangka1.Location = new System.Drawing.Point(0, 0);
            this.tambahTersangka1.Name = "tambahTersangka1";
            this.tambahTersangka1.Size = new System.Drawing.Size(1057, 783);
            this.tambahTersangka1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1057, 783);
            this.Controls.Add(this.tambahTersangka1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private CM.Desktop.Presentation.CustomControls.TambahTersangka tambahTersangka1;
    }
}