﻿using PdfiumViewer;

namespace RDM.LP.Desktop.Presentation
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.titleBarControl1 = new RDM.LP.Desktop.Presentation.CustomControls.TitleBarControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.mainContainer = new Telerik.WinControls.UI.RadPageView();
            this.OverviewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.rptInmates = new RDM.LP.Desktop.Presentation.CustomControls.RptInmates();
            this.rptCases = new RDM.LP.Desktop.Presentation.CustomControls.RptCases();
            this.rptCellOccupancy = new RDM.LP.Desktop.Presentation.CustomControls.RptCellOccupancy();
            this.rptStatusTahanan = new RDM.LP.Desktop.Presentation.CustomControls.RptStatusTahanan();
            this.PanelHome = new Telerik.WinControls.UI.RadPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblVersion = new Telerik.WinControls.UI.RadLabel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.overviewView = new Telerik.WinControls.UI.RadListView();
            this.listCellAllocation = new RDM.LP.Desktop.Presentation.CustomControls.ListCellAllocation();
            this.listaudittrail = new RDM.LP.Desktop.Presentation.CustomControls.ListAuditTrail();
            this.listCell = new RDM.LP.Desktop.Presentation.CustomControls.ListCell();
            this.dashboard = new RDM.LP.Desktop.Presentation.CustomControls.Dashboard();
            this.listKunjungan = new RDM.LP.Desktop.Presentation.CustomControls.ListKunjungan();
            this.listVisitor = new RDM.LP.Desktop.Presentation.CustomControls.ListVisitor();
            this.listVisitorVIP = new RDM.LP.Desktop.Presentation.CustomControls.ListVisitorVIP();
            this.listKunjunganVIP = new RDM.LP.Desktop.Presentation.CustomControls.ListKunjunganVIP();
            this.listTahanan = new RDM.LP.Desktop.Presentation.CustomControls.ListTahanan();
            this.daftarTahanan = new RDM.LP.Desktop.Presentation.CustomControls.DaftarTahanan();
            this.listUser = new RDM.LP.Desktop.Presentation.CustomControls.ListUser();
            this.listPrintFormVisitor = new RDM.LP.Desktop.Presentation.CustomControls.ListPrintFormVisitor();
            this.listPrintFormTahanan = new RDM.LP.Desktop.Presentation.CustomControls.ListPrintFormTahanan();
            this.previewDoc = new RDM.LP.Desktop.Presentation.CustomControls.PreviewDoc();
            this.AddDocView = new Telerik.WinControls.UI.RadPageViewPage();
            this.cellAllocation = new RDM.LP.Desktop.Presentation.CustomControls.FormCellAllocation();
            this.cell = new RDM.LP.Desktop.Presentation.CustomControls.FormCell();
            this.visitor = new RDM.LP.Desktop.Presentation.CustomControls.FormVisitorShort();
            this.visitorVIP = new RDM.LP.Desktop.Presentation.CustomControls.FormVisitorVIP();
            this.visit = new RDM.LP.Desktop.Presentation.CustomControls.FormVisit();
            this.tahanan = new RDM.LP.Desktop.Presentation.CustomControls.FormTahanan();
            this.user = new RDM.LP.Desktop.Presentation.CustomControls.FormUser();
            this.radCollapsiblePanel1 = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.radTreeView1 = new Telerik.WinControls.UI.RadTreeView();
            this.radBreadCrumb1 = new Telerik.WinControls.UI.RadBreadCrumb();
            this.topControl1 = new RDM.LP.Desktop.Presentation.CustomControls.TopControl();
            this.topLeft1 = new RDM.LP.Desktop.Presentation.CustomControls.TopLeft();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip4 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip5 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip6 = new System.Windows.Forms.ToolTip(this.components);
            this.synchDetail = new Telerik.WinControls.UI.RadPanel();
            this.btnSynch = new Telerik.WinControls.UI.RadButton();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.lblStatusCon = new Telerik.WinControls.UI.RadLabel();
            this.loadingSync = new System.Windows.Forms.PictureBox();
            this.onoff = new Telerik.WinControls.UI.RadLabel();
            this.toolTip7 = new System.Windows.Forms.ToolTip(this.components);
            this.pesanDetail = new Telerik.WinControls.UI.RadPanel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.lblHeadPesan = new Telerik.WinControls.UI.RadLabel();
            this.loadingPesan = new System.Windows.Forms.PictureBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.toolTip8 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip9 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip10 = new System.Windows.Forms.ToolTip(this.components);
            this.radWaitingBar1 = new Telerik.WinControls.UI.RadWaitingBar();
            this.waitingBarIndicatorElement2 = new Telerik.WinControls.UI.WaitingBarIndicatorElement();
            this.waitingBarIndicatorElement1 = new Telerik.WinControls.UI.WaitingBarIndicatorElement();
            this.tahananDetail = new RDM.LP.Desktop.Presentation.CustomControls.TahananDetail();
            this.statusTahananDetail = new RDM.LP.Desktop.Presentation.CustomControls.StatusTahananDetail();
            this.perpanjangantahanan = new RDM.LP.Desktop.Presentation.CustomControls.FormPerpanjangan1();
            this.tahananDetailCheckedOut = new RDM.LP.Desktop.Presentation.CustomControls.TahananDetailCheckedOut();
            this.searchData = new RDM.LP.Desktop.Presentation.CustomControls.SearchData();
            this.visitDetail = new RDM.LP.Desktop.Presentation.CustomControls.VisitDetail();
            this.visitorDetail = new RDM.LP.Desktop.Presentation.CustomControls.VisitorDetail();
            this.visitorVIPDetail = new RDM.LP.Desktop.Presentation.CustomControls.VisitorVIPDetail();
            this.userDetail = new RDM.LP.Desktop.Presentation.CustomControls.UserDetail();
            this.changepsw = new RDM.LP.Desktop.Presentation.CustomControls.FormPswChange();
            this.cellDetail = new RDM.LP.Desktop.Presentation.CustomControls.CellDetail();
            this._Dashboard = new RDM.LP.Desktop.Presentation.CustomControls.Dashboard();
            this.captureFingerPrint1 = new RDM.LP.Desktop.Presentation.CustomControls.CaptureFingerPrint();
            this.fingerPrint_Inmate = new RDM.LP.Desktop.Presentation.CustomControls.FingerPrint_Inmate();
            this.cellAllocationDetail = new RDM.LP.Desktop.Presentation.CustomControls.CellAllocationDetail();
            this.checkedOut = new RDM.LP.Desktop.Presentation.CustomControls.CheckedOutConfirm();
            this.checkedOutinmates = new RDM.LP.Desktop.Presentation.CustomControls.CheckedOutInmatesConfirm();
            this.capturePhoto = new RDM.LP.Desktop.Presentation.CustomControls.CapturePhoto();
            this.listSeizedAssets = new RDM.LP.Desktop.Presentation.CustomControls.ListSeizedAssets();
            this.seizedAssets = new RDM.LP.Desktop.Presentation.CustomControls.FormSeizedAssets();
            this.seizedAssetsDetail = new RDM.LP.Desktop.Presentation.CustomControls.SeizedAssetsDetail();
            this.newSeizedAssets = new RDM.LP.Desktop.Presentation.CustomControls.NewSeizedAssets();
			this.listBonTahanan = new RDM.LP.Desktop.Presentation.CustomControls.ListBonTahanan();
			this.bonTahanan = new RDM.LP.Desktop.Presentation.CustomControls.FormBonTahanan();
			this.bonTahananDetail = new RDM.LP.Desktop.Presentation.CustomControls.BonTahananDetail();
            this.settings = new RDM.LP.Desktop.Presentation.CustomControls.ChangeSettings();
            this.cameraSource = new RDM.LP.Desktop.Presentation.CustomControls.CameraSource();
            this.opacity = new RDM.LP.Desktop.Presentation.CustomControls.Opacity();
			this.keperluanBonTahanan = new RDM.LP.Desktop.Presentation.CustomControls.FormKeperluanBonTahanan();
            this.bonSerahTahanan = new RDM.LP.Desktop.Presentation.CustomControls.FormSerahTahanan();
            this.listBonSerahTahanan = new RDM.LP.Desktop.Presentation.CustomControls.ListBonSerahTahanan();
            this.bonTerimaTahanan = new RDM.LP.Desktop.Presentation.CustomControls.FormTerimaTahanan();
            this.listBonTerimaTahanan = new RDM.LP.Desktop.Presentation.CustomControls.ListBonTerimaTahanan();
            this.listkendaraan = new RDM.LP.Desktop.Presentation.CustomControls.ListKendaraan();
            this.liststatustahanan = new RDM.LP.Desktop.Presentation.CustomControls.ListStatusTahanan();
            this.kendaraan = new RDM.LP.Desktop.Presentation.CustomControls.FormKendaraan();
            this.kendaraanTahananDetail = new RDM.LP.Desktop.Presentation.CustomControls.KendaraanTahananDetail();
            this.listkendaraankeluar = new RDM.LP.Desktop.Presentation.CustomControls.ListKendaraanKeluar();
            this.listJadwalKunjungan = new RDM.LP.Desktop.Presentation.CustomControls.ListJadwalKunjungan();
            this.jadwalKunjungan = new RDM.LP.Desktop.Presentation.CustomControls.FormJadwalKunjungan();
            this.jadwalKunjunganDetail = new RDM.LP.Desktop.Presentation.CustomControls.JadwalKunjunganDetail();
            this.statusTahanan = new RDM.LP.Desktop.Presentation.CustomControls.FormStatusTahanan();
            this.pdfViewer = new RDM.LP.Desktop.Presentation.CustomControls.PDFViewer();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainContainer)).BeginInit();
            this.mainContainer.SuspendLayout();
            this.OverviewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelHome)).BeginInit();
            this.PanelHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.overviewView)).BeginInit();
            this.overviewView.SuspendLayout();
            this.AddDocView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).BeginInit();
            this.radCollapsiblePanel1.PanelContainer.SuspendLayout();
            this.radCollapsiblePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBreadCrumb1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.synchDetail)).BeginInit();
            this.synchDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSynch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStatusCon)).BeginInit();
            this.lblStatusCon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadingSync)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onoff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pesanDetail)).BeginInit();
            this.pesanDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeadPesan)).BeginInit();
            this.lblHeadPesan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadingPesan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // titleBarControl1
            // 
            this.titleBarControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(53)))), ((int)(((byte)(54)))));
            this.titleBarControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleBarControl1.Location = new System.Drawing.Point(0, 0);
            this.titleBarControl1.Name = "titleBarControl1";
            this.titleBarControl1.Size = new System.Drawing.Size(1654, 33);
            this.titleBarControl1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 367F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.mainContainer, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.radCollapsiblePanel1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.radBreadCrumb1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.topControl1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.topLeft1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 33);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.617927F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.115336F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.26674F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1654, 750);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // mainContainer
            // 
            this.mainContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(99)))), ((int)(((byte)(99)))));
            this.mainContainer.Controls.Add(this.OverviewPage);
            this.mainContainer.Controls.Add(this.AddDocView);
            this.mainContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainContainer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainContainer.ForeColor = System.Drawing.Color.White;
            this.mainContainer.Location = new System.Drawing.Point(300, 102);
            this.mainContainer.Margin = new System.Windows.Forms.Padding(0);
            this.mainContainer.Name = "mainContainer";
            this.mainContainer.SelectedPage = this.OverviewPage;
            this.mainContainer.Size = new System.Drawing.Size(1354, 648);
            this.mainContainer.TabIndex = 5;
            this.mainContainer.ThemeName = "Material";
            // 
            // OverviewPage
            // 
            this.OverviewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.OverviewPage.Controls.Add(this.rptCellOccupancy);
            this.OverviewPage.Controls.Add(this.rptStatusTahanan);
            this.OverviewPage.Controls.Add(this.PanelHome);
            this.OverviewPage.Controls.Add(this.overviewView);
            this.OverviewPage.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.folder;
            this.OverviewPage.ItemSize = new System.Drawing.SizeF(121F, 55F);
            this.OverviewPage.Location = new System.Drawing.Point(6, 61);
            this.OverviewPage.Name = "OverviewPage";
            this.OverviewPage.Size = new System.Drawing.Size(1342, 581);
            this.OverviewPage.Text = "OVERVIEW";
            // 
            // rptCellOccupancy
            // 
            this.rptCellOccupancy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rptCellOccupancy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptCellOccupancy.Location = new System.Drawing.Point(0, 0);
            this.rptCellOccupancy.Name = "rptCellOccupancy";
            this.rptCellOccupancy.Size = new System.Drawing.Size(1342, 521);
            this.rptCellOccupancy.TabIndex = 224;
            this.rptCellOccupancy.Visible = false;
            // 
            // rptStatusTahanan
            // 
            this.rptStatusTahanan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rptStatusTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptStatusTahanan.Location = new System.Drawing.Point(0, 0);
            this.rptStatusTahanan.Name = "rptStatusTahanan";
            this.rptStatusTahanan.Size = new System.Drawing.Size(1342, 521);
            this.rptStatusTahanan.TabIndex = 224;
            this.rptStatusTahanan.Visible = false;
            // 
            // PanelHome
            // 
            this.PanelHome.BackColor = System.Drawing.Color.Transparent;
            this.PanelHome.Controls.Add(this.pictureBox1);
            this.PanelHome.Controls.Add(this.lblVersion);
            this.PanelHome.Controls.Add(this.lblTitle);
            this.PanelHome.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelHome.Location = new System.Drawing.Point(0, 521);
            this.PanelHome.Margin = new System.Windows.Forms.Padding(0);
            this.PanelHome.Name = "PanelHome";
            this.PanelHome.Padding = new System.Windows.Forms.Padding(10);
            // 
            // 
            // 
            this.PanelHome.RootElement.Opacity = 0.4D;
            this.PanelHome.Size = new System.Drawing.Size(1342, 60);
            this.PanelHome.TabIndex = 17;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).ShadowColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).RippleAnimationColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).FocusBorderColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).HighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).BorderHighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).Opacity = 0.4D;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(10);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(0))).Opacity = 0.4D;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).InnerColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).InnerColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).InnerColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 2);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(57, 56);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // lblVersion
            // 
            this.lblVersion.BackColor = System.Drawing.Color.Transparent;
            this.lblVersion.Location = new System.Drawing.Point(8, 10);
            this.lblVersion.Margin = new System.Windows.Forms.Padding(0);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Padding = new System.Windows.Forms.Padding(55, 0, 0, 0);
            this.lblVersion.Size = new System.Drawing.Size(57, 2);
            this.lblVersion.TabIndex = 34;
            this.lblVersion.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblTitle.Font = new System.Drawing.Font("Verdana", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(10, 29);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.lblTitle.ShowItemToolTips = false;
            this.lblTitle.Size = new System.Drawing.Size(1322, 26);
            this.lblTitle.TabIndex = 13;
            this.lblTitle.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // overviewView
            // 
            this.overviewView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(216)))), ((int)(((byte)(255)))));
            this.overviewView.Controls.Add(this.listKunjunganVIP);
            this.overviewView.Controls.Add(this.listVisitorVIP);
            this.overviewView.Controls.Add(this.listJadwalKunjungan);
            this.overviewView.Controls.Add(this.listkendaraankeluar);
            this.overviewView.Controls.Add(this.listkendaraan);
            this.overviewView.Controls.Add(this.liststatustahanan);
            this.overviewView.Controls.Add(this.listBonTerimaTahanan);
            this.overviewView.Controls.Add(this.listBonSerahTahanan);
            this.overviewView.Controls.Add(this.listBonTahanan);
            this.overviewView.Controls.Add(this.previewDoc);
            this.overviewView.Controls.Add(this.rptInmates);
            this.overviewView.Controls.Add(this.rptCases);
            this.overviewView.Controls.Add(this.listCellAllocation);
            this.overviewView.Controls.Add(this.listaudittrail);
            this.overviewView.Controls.Add(this.listCell);
            this.overviewView.Controls.Add(this.dashboard);
            this.overviewView.Controls.Add(this.listKunjungan);
            this.overviewView.Controls.Add(this.listVisitor);
            this.overviewView.Controls.Add(this.listTahanan);
            this.overviewView.Controls.Add(this.daftarTahanan);
            this.overviewView.Controls.Add(this.listUser);
            this.overviewView.Controls.Add(this.listSeizedAssets);
            this.overviewView.Controls.Add(this.listPrintFormVisitor);
            this.overviewView.Controls.Add(this.listPrintFormTahanan);
            this.overviewView.Controls.Add(this.PDFViewer);
            this.overviewView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.overviewView.GroupItemSize = new System.Drawing.Size(200, 36);
            this.overviewView.ItemSize = new System.Drawing.Size(250, 36);
            this.overviewView.Location = new System.Drawing.Point(0, 0);
            this.overviewView.Margin = new System.Windows.Forms.Padding(0);
            this.overviewView.Name = "overviewView";
            this.overviewView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 60);
            this.overviewView.Size = new System.Drawing.Size(1342, 581);
            // 
            // listJadwalKunjungan
            // 
            this.listJadwalKunjungan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listJadwalKunjungan.Location = new System.Drawing.Point(0, 0);
            this.listJadwalKunjungan.Name = "listJadwalKunjungan";
            this.listJadwalKunjungan.Size = new System.Drawing.Size(1342, 521);
            this.listJadwalKunjungan.TabIndex = 1;
            this.listJadwalKunjungan.Visible = false;
            this.overviewView.TabIndex = 15;
            // 
            // listkendaraankeluar
            // 
            this.listkendaraankeluar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listkendaraankeluar.Location = new System.Drawing.Point(0, 0);
            this.listkendaraankeluar.Name = "listkendaraankeluar";
            this.listkendaraankeluar.Size = new System.Drawing.Size(1342, 521);
            this.listkendaraankeluar.TabIndex = 1;
            this.listkendaraankeluar.Visible = false;
            this.overviewView.TabIndex = 15;
            // 
            // listkendaraan
            // 
            this.listkendaraan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listkendaraan.Location = new System.Drawing.Point(0, 0);
            this.listkendaraan.Name = "listkendaraan";
            this.listkendaraan.Size = new System.Drawing.Size(1342, 521);
            this.listkendaraan.TabIndex = 1;
            this.listkendaraan.Visible = false;
            this.overviewView.TabIndex = 15;
            // 
            // liststatustahanan
            // 
            this.liststatustahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.liststatustahanan.Location = new System.Drawing.Point(0, 0);
            this.liststatustahanan.Name = "liststatustahanan";
            this.liststatustahanan.Size = new System.Drawing.Size(1342, 521);
            this.liststatustahanan.TabIndex = 1;
            this.liststatustahanan.Visible = false;
            this.overviewView.TabIndex = 15;
            // 
            // listBonTerimaTahanan
            // 
            this.listBonTerimaTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBonTerimaTahanan.Location = new System.Drawing.Point(0, 0);
            this.listBonTerimaTahanan.Name = "listBonTerimaTahanan";
            this.listBonTerimaTahanan.Size = new System.Drawing.Size(1342, 521);
            this.listBonTerimaTahanan.TabIndex = 1;
            this.listBonTerimaTahanan.Visible = false;
            this.overviewView.TabIndex = 15;
            // 
            // listBonSerahTahanan
            // 
            this.listBonSerahTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBonSerahTahanan.Location = new System.Drawing.Point(0, 0);
            this.listBonSerahTahanan.Name = "listBonSerahTahanan";
            this.listBonSerahTahanan.Size = new System.Drawing.Size(1342, 521);
            this.listBonSerahTahanan.TabIndex = 1;
            this.listBonSerahTahanan.Visible = false;
            this.overviewView.TabIndex = 15;
			// 
            // listBonTahanan
            // 
            this.listBonTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBonTahanan.Location = new System.Drawing.Point(0, 0);
            this.listBonTahanan.Name = "listBonTahanan";
            this.listBonTahanan.Size = new System.Drawing.Size(1342, 521);
            this.listBonTahanan.TabIndex = 1;
            this.listBonTahanan.Visible = false;
            // 
            // rptCases
            // 
            this.rptCases.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptCases.Location = new System.Drawing.Point(0, 0);
            this.rptCases.Name = "rptCases";
            this.rptCases.Size = new System.Drawing.Size(1342, 521);
            this.rptCases.TabIndex = 9;
            this.rptCases.Visible = false;
            // 
            // rptInmates
            // 
            this.rptInmates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rptInmates.Location = new System.Drawing.Point(0, 0);
            this.rptInmates.Name = "rptInmates";
            this.rptInmates.Size = new System.Drawing.Size(1342, 521);
            this.rptInmates.TabIndex = 9;
            this.rptInmates.Visible = false;
            // 
            // listCellAllocation
            // 
            this.listCellAllocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listCellAllocation.Location = new System.Drawing.Point(0, 0);
            this.listCellAllocation.Name = "listCellAllocation";
            this.listCellAllocation.Size = new System.Drawing.Size(1342, 521);
            this.listCellAllocation.TabIndex = 9;
            this.listCellAllocation.Visible = false;
            // 
            // listSeizedAssets
            // 
            this.listSeizedAssets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listSeizedAssets.Location = new System.Drawing.Point(0, 0);
            this.listSeizedAssets.Name = "listSeizedAssets";
            this.listSeizedAssets.Size = new System.Drawing.Size(1342, 521);
            this.listSeizedAssets.TabIndex = 9;
            this.listSeizedAssets.Visible = false;
            // 
            // listaudittrail
            // 
            this.listaudittrail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listaudittrail.Location = new System.Drawing.Point(0, 0);
            this.listaudittrail.Name = "listaudittrail";
            this.listaudittrail.Size = new System.Drawing.Size(1342, 521);
            this.listaudittrail.TabIndex = 8;
            this.listaudittrail.Visible = false;
            // 
            // listCell
            // 
            this.listCell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listCell.Location = new System.Drawing.Point(0, 0);
            this.listCell.Name = "listCell";
            this.listCell.Size = new System.Drawing.Size(1342, 521);
            this.listCell.TabIndex = 7;
            this.listCell.Visible = false;
            // 
            // dashboard
            // 
            this.dashboard.BackColor = System.Drawing.Color.Transparent;
            this.dashboard.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dashboard.BackgroundImage")));
            this.dashboard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.dashboard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dashboard.Location = new System.Drawing.Point(0, 0);
            this.dashboard.Name = "dashboard";
            this.dashboard.Size = new System.Drawing.Size(1342, 521);
            this.dashboard.TabIndex = 6;
            // 
            // listKunjungan
            // 
            this.listKunjungan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listKunjungan.Location = new System.Drawing.Point(0, 0);
            this.listKunjungan.Name = "listKunjungan";
            this.listKunjungan.Size = new System.Drawing.Size(1342, 521);
            this.listKunjungan.TabIndex = 3;
            this.listKunjungan.Visible = false;
            // 
            // listKunjunganVIP
            // 
            this.listKunjunganVIP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listKunjunganVIP.Location = new System.Drawing.Point(0, 0);
            this.listKunjunganVIP.Name = "listKunjunganVIP";
            this.listKunjunganVIP.Size = new System.Drawing.Size(1342, 521);
            this.listKunjunganVIP.TabIndex = 3;
            this.listKunjunganVIP.Visible = false;
            // 
            // listVisitor
            // 
            this.listVisitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listVisitor.Location = new System.Drawing.Point(0, 0);
            this.listVisitor.Name = "listVisitor";
            this.listVisitor.Size = new System.Drawing.Size(1342, 521);
            this.listVisitor.TabIndex = 2;
            this.listVisitor.Visible = false;
            // 
            // listVisitorVIP
            // 
            this.listVisitorVIP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listVisitorVIP.Location = new System.Drawing.Point(0, 0);
            this.listVisitorVIP.Name = "listVisitorVIP";
            this.listVisitorVIP.Size = new System.Drawing.Size(1342, 521);
            this.listVisitorVIP.TabIndex = 2;
            this.listVisitorVIP.Visible = false;
            // 
            // listTahanan
            // 
            this.listTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listTahanan.Location = new System.Drawing.Point(0, 0);
            this.listTahanan.Name = "listTahanan";
            this.listTahanan.Size = new System.Drawing.Size(1342, 521);
            this.listTahanan.TabIndex = 1;
            this.listTahanan.Visible = false;
            // 
            // daftarTahanan
            // 
            this.daftarTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.daftarTahanan.Location = new System.Drawing.Point(0, 0);
            this.daftarTahanan.Name = "daftarTahanan";
            this.daftarTahanan.Size = new System.Drawing.Size(1342, 521);
            this.daftarTahanan.TabIndex = 1;
            this.daftarTahanan.Visible = false;
            // 
            // listUser
            // 
            this.listUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listUser.Location = new System.Drawing.Point(0, 0);
            this.listUser.Name = "listUser";
            this.listUser.Size = new System.Drawing.Size(1342, 521);
            this.listUser.TabIndex = 0;
            this.listUser.Visible = false;
            // 
            // listPrintFormVisitor
            // 
            this.listPrintFormVisitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listPrintFormVisitor.Location = new System.Drawing.Point(0, 0);
            this.listPrintFormVisitor.Name = "listPrintFormVisitor";
            this.listPrintFormVisitor.Size = new System.Drawing.Size(1342, 521);
            this.listPrintFormVisitor.TabIndex = 2;
            this.listPrintFormVisitor.Visible = false;
            // 
            // listPrintFormTahanan
            // 
            this.listPrintFormTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listPrintFormTahanan.Location = new System.Drawing.Point(0, 0);
            this.listPrintFormTahanan.Name = "listPrintFormTahanan";
            this.listPrintFormTahanan.Size = new System.Drawing.Size(1342, 521);
            this.listPrintFormTahanan.TabIndex = 2;
            this.listPrintFormTahanan.Visible = false;
            // 
            // previewDoc
            // 
            this.previewDoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewDoc.Location = new System.Drawing.Point(0, 0);
            this.previewDoc.Name = "previewDoc";
            this.previewDoc.Size = new System.Drawing.Size(1342, 521);
            this.previewDoc.TabIndex = 2;
            this.previewDoc.Visible = false;
            // 
            // AddDocView
            // 
            this.AddDocView.Controls.Add(this.visitorVIP);
            this.AddDocView.Controls.Add(this.jadwalKunjungan);
            this.AddDocView.Controls.Add(this.statusTahanan);
            this.AddDocView.Controls.Add(this.kendaraan);
            this.AddDocView.Controls.Add(this.bonTerimaTahanan);
            this.AddDocView.Controls.Add(this.bonSerahTahanan);
            this.AddDocView.Controls.Add(this.bonTahanan);
            this.AddDocView.Controls.Add(this.cellAllocation);
            this.AddDocView.Controls.Add(this.cell);
            this.AddDocView.Controls.Add(this.visitor);
            this.AddDocView.Controls.Add(this.visit);
            this.AddDocView.Controls.Add(this.tahanan);
            this.AddDocView.Controls.Add(this.user);
            this.AddDocView.Controls.Add(this.seizedAssets);
            this.AddDocView.Image = ((System.Drawing.Image)(resources.GetObject("AddDocView.Image")));
            this.AddDocView.ItemSize = new System.Drawing.SizeF(111F, 55F);
            this.AddDocView.Location = new System.Drawing.Point(6, 61);
            this.AddDocView.Name = "AddDocView";
            this.AddDocView.Size = new System.Drawing.Size(1342, 581);
            this.AddDocView.Text = "Add New";
            // 
            // cellAllocation
            // 
            this.cellAllocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cellAllocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cellAllocation.Location = new System.Drawing.Point(0, 0);
            this.cellAllocation.Name = "cellAllocation";
            this.cellAllocation.Size = new System.Drawing.Size(1342, 581);
            this.cellAllocation.TabIndex = 219;
            // 
            // kendaraan
            // 
            this.kendaraan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.kendaraan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kendaraan.Location = new System.Drawing.Point(0, 0);
            this.kendaraan.Name = "kendaraan";
            this.kendaraan.Size = new System.Drawing.Size(1342, 581);
            this.kendaraan.TabIndex = 194;
            this.kendaraan.Visible = false;
            // 
            // bonTahanan
            // 
            this.bonTahanan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bonTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonTahanan.Location = new System.Drawing.Point(0, 0);
            this.bonTahanan.Name = "bonTahanan";
            this.bonTahanan.Size = new System.Drawing.Size(1342, 581);
            this.bonTahanan.TabIndex = 194;
            this.bonTahanan.Visible = false;
            // 
            // bonSerahTahanan
            // 
            this.bonSerahTahanan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bonSerahTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonSerahTahanan.Location = new System.Drawing.Point(0, 0);
            this.bonSerahTahanan.Name = "bonSerahTahanan";
            this.bonSerahTahanan.Size = new System.Drawing.Size(1342, 581);
            this.bonSerahTahanan.TabIndex = 194;
            this.bonSerahTahanan.Visible = false;
            // 
            // bonTerimaTahanan
            // 
            this.bonTerimaTahanan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bonTerimaTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonTerimaTahanan.Location = new System.Drawing.Point(0, 0);
            this.bonTerimaTahanan.Name = "bonTerimaTahanan";
            this.bonTerimaTahanan.Size = new System.Drawing.Size(1342, 581);
            this.bonTerimaTahanan.TabIndex = 194;
            this.bonTerimaTahanan.Visible = false;
            //
            // seizedAssets
            // 
            this.seizedAssets.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.seizedAssets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.seizedAssets.Location = new System.Drawing.Point(0, 0);
            this.seizedAssets.Name = "seizedAssets";
            this.seizedAssets.Size = new System.Drawing.Size(1342, 581);
            this.seizedAssets.TabIndex = 219;
			this.seizedAssets.Visible = false;
            // 
            // cell
            // 
            this.cell.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cell.Location = new System.Drawing.Point(0, 0);
            this.cell.Name = "cell";
            this.cell.Size = new System.Drawing.Size(1342, 581);
            this.cell.TabIndex = 195;
            this.cell.Visible = false;
            // 
            // visitor
            // 
            this.visitor.BackColor = System.Drawing.Color.White;
            this.visitor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.visitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.visitor.Location = new System.Drawing.Point(0, 0);
            this.visitor.Name = "visitor";
            this.visitor.Size = new System.Drawing.Size(1342, 581);
            this.visitor.TabIndex = 194;
            this.visitor.Visible = false;
            // 
            // visitorVIP
            // 
            this.visitorVIP.BackColor = System.Drawing.Color.White;
            this.visitorVIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.visitorVIP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.visitorVIP.Location = new System.Drawing.Point(0, 0);
            this.visitorVIP.Name = "visitorVIP";
            this.visitorVIP.Size = new System.Drawing.Size(1342, 581);
            this.visitorVIP.TabIndex = 194;
            this.visitorVIP.Visible = false;
            // 
            // visit
            // 
            this.visit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.visit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.visit.Location = new System.Drawing.Point(0, 0);
            this.visit.Name = "visit";
            this.visit.Size = new System.Drawing.Size(1342, 581);
            this.visit.TabIndex = 192;
            // 
            // tahanan
            // 
            this.tahanan.BackColor = System.Drawing.Color.White;
            this.tahanan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tahanan.Location = new System.Drawing.Point(0, 0);
            this.tahanan.Name = "tahanan";
            this.tahanan.Size = new System.Drawing.Size(1342, 581);
            this.tahanan.TabIndex = 190;
            // 
            // user
            // 
            this.user.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.user.Dock = System.Windows.Forms.DockStyle.Fill;
            this.user.Location = new System.Drawing.Point(0, 0);
            this.user.Name = "user";
            this.user.Size = new System.Drawing.Size(1342, 581);
            this.user.TabIndex = 189;
            this.user.Visible = false;
            // 
            // jadwalKunjungan
            // 
            this.jadwalKunjungan.BackColor = System.Drawing.Color.White;
            this.jadwalKunjungan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.jadwalKunjungan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.jadwalKunjungan.Location = new System.Drawing.Point(0, 0);
            this.jadwalKunjungan.Name = "jadwalKunjungan";
            this.jadwalKunjungan.Size = new System.Drawing.Size(1342, 581);
            this.jadwalKunjungan.TabIndex = 194;
            this.jadwalKunjungan.Visible = false;
            // 
            // statusTahanan
            // 
            this.statusTahanan.BackColor = System.Drawing.Color.White;
            this.statusTahanan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.statusTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statusTahanan.Location = new System.Drawing.Point(0, 0);
            this.statusTahanan.Name = "statusTahanan";
            this.statusTahanan.Size = new System.Drawing.Size(1342, 581);
            this.statusTahanan.TabIndex = 194;
            this.statusTahanan.Visible = false;
            // 
            // pdfViewer
            // 
            this.pdfViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pdfViewer.Location = new System.Drawing.Point(0, 0);
            this.pdfViewer.Name = "pdfViewer";
            this.pdfViewer.Size = new System.Drawing.Size(1342, 521);
            this.pdfViewer.TabIndex = 0;
            this.pdfViewer.Visible = false;
            // 
            // radCollapsiblePanel1
            // 
            this.radCollapsiblePanel1.AnimationFrames = 150;
            this.radCollapsiblePanel1.AnimationInterval = 300;
            this.radCollapsiblePanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.radCollapsiblePanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radCollapsiblePanel1.ExpandDirection = Telerik.WinControls.UI.RadDirection.Right;
            this.radCollapsiblePanel1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCollapsiblePanel1.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.radCollapsiblePanel1.HorizontalHeaderAlignment = Telerik.WinControls.UI.RadHorizontalAlignment.Center;
            this.radCollapsiblePanel1.Location = new System.Drawing.Point(0, 57);
            this.radCollapsiblePanel1.Margin = new System.Windows.Forms.Padding(0);
            this.radCollapsiblePanel1.Name = "radCollapsiblePanel1";
            this.radCollapsiblePanel1.OwnerBoundsCache = new System.Drawing.Rectangle(0, 57, 330, 693);
            // 
            // radCollapsiblePanel1.PanelContainer
            // 
            this.radCollapsiblePanel1.PanelContainer.Controls.Add(this.radTreeView1);
            this.radCollapsiblePanel1.PanelContainer.Location = new System.Drawing.Point(0, 0);
            this.radCollapsiblePanel1.PanelContainer.Size = new System.Drawing.Size(277, 693);
            this.tableLayoutPanel2.SetRowSpan(this.radCollapsiblePanel1, 2);
            this.radCollapsiblePanel1.Size = new System.Drawing.Size(300, 693);
            this.radCollapsiblePanel1.TabIndex = 1;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).ExpandDirection = Telerik.WinControls.UI.RadDirection.Right;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).AnimationInterval = 300;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).AnimationFrames = 150;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).BorderGradientAngle = 180F;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).BorderGradientStyle = Telerik.WinControls.GradientStyles.Linear;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(33)))), ((int)(((byte)(17)))));
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).BackColor = System.Drawing.Color.Black;
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadCollapsiblePanelElement)(this.radCollapsiblePanel1.GetChildAt(0))).Margin = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.CollapsiblePanelHeaderElement)(this.radCollapsiblePanel1.GetChildAt(0).GetChildAt(1))).HorizontalHeaderAlignment = Telerik.WinControls.UI.RadHorizontalAlignment.Center;
            ((Telerik.WinControls.UI.CollapsiblePanelHeaderElement)(this.radCollapsiblePanel1.GetChildAt(0).GetChildAt(1))).VerticalHeaderAlignment = Telerik.WinControls.UI.RadVerticalAlignment.Top;
            ((Telerik.WinControls.UI.CollapsiblePanelHeaderElement)(this.radCollapsiblePanel1.GetChildAt(0).GetChildAt(1))).Orientation = System.Windows.Forms.Orientation.Vertical;
            ((Telerik.WinControls.UI.CollapsiblePanelButtonElement)(this.radCollapsiblePanel1.GetChildAt(0).GetChildAt(1).GetChildAt(0))).TextOrientation = System.Windows.Forms.Orientation.Horizontal;
            ((Telerik.WinControls.UI.CollapsiblePanelButtonElement)(this.radCollapsiblePanel1.GetChildAt(0).GetChildAt(1).GetChildAt(0))).StretchVertically = false;
            // 
            // radTreeView1
            // 
            this.radTreeView1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            this.radTreeView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTreeView1.ForeColor = System.Drawing.Color.Black;
            this.radTreeView1.ItemHeight = 35;
            this.radTreeView1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.radTreeView1.LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            this.radTreeView1.Location = new System.Drawing.Point(0, 0);
            this.radTreeView1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.radTreeView1.Name = "radTreeView1";
            this.radTreeView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView1.Size = new System.Drawing.Size(277, 693);
            this.radTreeView1.SpacingBetweenNodes = -1;
            this.radTreeView1.TabIndex = 0;
            this.radTreeView1.TreeIndent = 25;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).ItemHeight = 35;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).LineStyle = Telerik.WinControls.UI.TreeLineStyle.Solid;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).TreeIndent = 25;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).FullRowSelect = true;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).NodeSpacing = -1;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).BackColor2 = System.Drawing.Color.Orange;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).BackColor3 = System.Drawing.Color.Orange;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).BackColor4 = System.Drawing.Color.Orange;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).Image = null;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).ImageLayout = System.Windows.Forms.ImageLayout.Center;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).ImageOpacity = 0.1D;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).RippleAnimationColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).HighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).BorderHighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadTreeViewElement)(this.radTreeView1.GetChildAt(0))).BackColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadScrollBarElement)(this.radTreeView1.GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTreeView1.GetChildAt(0).GetChildAt(1).GetChildAt(0))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTreeView1.GetChildAt(0).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTreeView1.GetChildAt(0).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTreeView1.GetChildAt(0).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTreeView1.GetChildAt(0).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTreeView1.GetChildAt(0).GetChildAt(1).GetChildAt(1))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTreeView1.GetChildAt(0).GetChildAt(1).GetChildAt(1))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTreeView1.GetChildAt(0).GetChildAt(1).GetChildAt(1))).BackColor4 = System.Drawing.Color.Transparent;
            // 
            // radBreadCrumb1
            // 
            this.radBreadCrumb1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            this.radBreadCrumb1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radBreadCrumb1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radBreadCrumb1.Location = new System.Drawing.Point(301, 58);
            this.radBreadCrumb1.Margin = new System.Windows.Forms.Padding(1);
            this.radBreadCrumb1.Name = "radBreadCrumb1";
            this.radBreadCrumb1.Size = new System.Drawing.Size(1352, 43);
            this.radBreadCrumb1.TabIndex = 2;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radBreadCrumb1.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radBreadCrumb1.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radBreadCrumb1.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radBreadCrumb1.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radBreadCrumb1.GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // topControl1
            // 
            this.topControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.topControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topControl1.Location = new System.Drawing.Point(300, 0);
            this.topControl1.Margin = new System.Windows.Forms.Padding(0);
            this.topControl1.Name = "topControl1";
            this.topControl1.Size = new System.Drawing.Size(1354, 57);
            this.topControl1.TabIndex = 3;
            // 
            // topLeft1
            // 
            this.topLeft1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topLeft1.Location = new System.Drawing.Point(0, 0);
            this.topLeft1.Margin = new System.Windows.Forms.Padding(0);
            this.topLeft1.Name = "topLeft1";
            this.topLeft1.Size = new System.Drawing.Size(300, 57);
            this.topLeft1.TabIndex = 4;
            // 
            // toolTip1
            // 
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip1.ToolTipTitle = "Tanggal Input";
            // 
            // toolTip2
            // 
            this.toolTip2.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip2.ToolTipTitle = "Info";
            // 
            // toolTip3
            // 
            this.toolTip3.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip3.ToolTipTitle = "Info";
            // 
            // toolTip4
            // 
            this.toolTip4.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip4.ToolTipTitle = "Info";
            // 
            // toolTip5
            // 
            this.toolTip5.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip5.ToolTipTitle = "Info";
            // 
            // toolTip6
            // 
            this.toolTip6.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip6.ToolTipTitle = "Info";
            // 
            // synchDetail
            // 
            this.synchDetail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.synchDetail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.synchDetail.Controls.Add(this.btnSynch);
            this.synchDetail.Controls.Add(this.radLabel3);
            this.synchDetail.Controls.Add(this.radLabel4);
            this.synchDetail.Controls.Add(this.radLabel2);
            this.synchDetail.Controls.Add(this.radLabel1);
            this.synchDetail.Controls.Add(this.lblStatusCon);
            this.synchDetail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.synchDetail.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.synchDetail.ForeColor = System.Drawing.Color.White;
            this.synchDetail.Location = new System.Drawing.Point(32739, 282);
            this.synchDetail.Margin = new System.Windows.Forms.Padding(0);
            this.synchDetail.Name = "synchDetail";
            this.synchDetail.Size = new System.Drawing.Size(329, 171);
            this.synchDetail.TabIndex = 8;
            this.synchDetail.UseCompatibleTextRendering = false;
            ((Telerik.WinControls.UI.RadPanelElement)(this.synchDetail.GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadPanelElement)(this.synchDetail.GetChildAt(0))).BorderHighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.synchDetail.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.synchDetail.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.synchDetail.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.synchDetail.GetChildAt(0).GetChildAt(1))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.synchDetail.GetChildAt(0).GetChildAt(1))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.synchDetail.GetChildAt(0).GetChildAt(1))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.synchDetail.GetChildAt(0).GetChildAt(1))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.synchDetail.GetChildAt(0).GetChildAt(1))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.synchDetail.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.synchDetail.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // btnSynch
            // 
            this.btnSynch.ForeColor = System.Drawing.Color.DarkRed;
            this.btnSynch.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.rotate;
            this.btnSynch.Location = new System.Drawing.Point(0, 94);
            this.btnSynch.Name = "btnSynch";
            this.btnSynch.Size = new System.Drawing.Size(310, 36);
            this.btnSynch.TabIndex = 5;
            this.btnSynch.Text = "Lakukan Proses Sinkronisasi";
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnSynch.GetChildAt(0))).Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.rotate;
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnSynch.GetChildAt(0))).Text = "Lakukan Proses Sinkronisasi";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnSynch.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnSynch.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.ForeColor = System.Drawing.Color.DarkRed;
            this.radLabel3.Location = new System.Drawing.Point(0, 63);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(156, 22);
            this.radLabel3.TabIndex = 4;
            this.radLabel3.Text = "Suskes";
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.ForeColor = System.Drawing.Color.DarkRed;
            this.radLabel4.Location = new System.Drawing.Point(0, 63);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(156, 22);
            this.radLabel4.TabIndex = 3;
            this.radLabel4.Text = "Status Sync Terakhir  :";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.ForeColor = System.Drawing.Color.DarkRed;
            this.radLabel2.Location = new System.Drawing.Point(0, 36);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(156, 22);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "12 Jan 2018 13:35:24";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.ForeColor = System.Drawing.Color.DarkRed;
            this.radLabel1.Location = new System.Drawing.Point(0, 36);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(156, 22);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Sinkronisasi Terakhir :";
            // 
            // lblStatusCon
            // 
            this.lblStatusCon.AutoSize = false;
            this.lblStatusCon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(33)))), ((int)(((byte)(17)))));
            this.lblStatusCon.Controls.Add(this.loadingSync);
            this.lblStatusCon.Controls.Add(this.onoff);
            this.lblStatusCon.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblStatusCon.ForeColor = System.Drawing.Color.White;
            this.lblStatusCon.Location = new System.Drawing.Point(0, 0);
            this.lblStatusCon.Name = "lblStatusCon";
            // 
            // 
            // 
            this.lblStatusCon.RootElement.CustomFont = "Roboto";
            this.lblStatusCon.RootElement.CustomFontSize = 9F;
            this.lblStatusCon.RootElement.CustomFontStyle = System.Drawing.FontStyle.Regular;
            this.lblStatusCon.Size = new System.Drawing.Size(329, 27);
            this.lblStatusCon.TabIndex = 0;
            this.lblStatusCon.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.lblStatusCon.TextWrap = false;
            this.lblStatusCon.ThemeName = "ControlDefault";
            ((Telerik.WinControls.UI.RadLabelElement)(this.lblStatusCon.GetChildAt(0))).TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            ((Telerik.WinControls.UI.RadLabelElement)(this.lblStatusCon.GetChildAt(0))).Text = "";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.lblStatusCon.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblStatusCon.GetChildAt(0).GetChildAt(1))).FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblStatusCon.GetChildAt(0).GetChildAt(1))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblStatusCon.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            // 
            // loadingSync
            // 
            this.loadingSync.BackColor = System.Drawing.Color.White;
            this.loadingSync.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.loader;
            this.loadingSync.Location = new System.Drawing.Point(0, 5);
            this.loadingSync.Margin = new System.Windows.Forms.Padding(0);
            this.loadingSync.Name = "loadingSync";
            this.loadingSync.Size = new System.Drawing.Size(16, 16);
            this.loadingSync.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.loadingSync.TabIndex = 10;
            this.loadingSync.TabStop = false;
            this.loadingSync.Visible = false;
            // 
            // onoff
            // 
            this.onoff.AutoSize = false;
            this.onoff.BackColor = System.Drawing.Color.LimeGreen;
            this.onoff.Location = new System.Drawing.Point(0, 5);
            this.onoff.Name = "onoff";
            this.onoff.Size = new System.Drawing.Size(16, 16);
            this.onoff.TabIndex = 9;
            // 
            // toolTip7
            // 
            this.toolTip7.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip7.ToolTipTitle = "Info";
            // 
            // pesanDetail
            // 
            this.pesanDetail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.pesanDetail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pesanDetail.Controls.Add(this.radScrollablePanel1);
            this.pesanDetail.Controls.Add(this.lblHeadPesan);
            this.pesanDetail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pesanDetail.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pesanDetail.ForeColor = System.Drawing.Color.White;
            this.pesanDetail.Location = new System.Drawing.Point(32739, 280);
            this.pesanDetail.Margin = new System.Windows.Forms.Padding(0);
            this.pesanDetail.Name = "pesanDetail";
            this.pesanDetail.Size = new System.Drawing.Size(329, 445);
            this.pesanDetail.TabIndex = 10;
            this.pesanDetail.UseCompatibleTextRendering = false;
            ((Telerik.WinControls.UI.RadPanelElement)(this.pesanDetail.GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadPanelElement)(this.pesanDetail.GetChildAt(0))).BorderHighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.pesanDetail.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.pesanDetail.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.pesanDetail.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.pesanDetail.GetChildAt(0).GetChildAt(1))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.pesanDetail.GetChildAt(0).GetChildAt(1))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.pesanDetail.GetChildAt(0).GetChildAt(1))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.pesanDetail.GetChildAt(0).GetChildAt(1))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.pesanDetail.GetChildAt(0).GetChildAt(1))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.pesanDetail.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.pesanDetail.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.Background;
            this.radScrollablePanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 27);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            this.radScrollablePanel1.Padding = new System.Windows.Forms.Padding(0);
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Location = new System.Drawing.Point(0, 0);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(329, 418);
            this.radScrollablePanel1.Size = new System.Drawing.Size(329, 418);
            this.radScrollablePanel1.TabIndex = 1;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radScrollablePanel1.GetChildAt(0).GetChildAt(1))).BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radScrollablePanel1.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            // 
            // lblHeadPesan
            // 
            this.lblHeadPesan.AutoSize = false;
            this.lblHeadPesan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(33)))), ((int)(((byte)(17)))));
            this.lblHeadPesan.Controls.Add(this.loadingPesan);
            this.lblHeadPesan.Controls.Add(this.radLabel10);
            this.lblHeadPesan.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHeadPesan.ForeColor = System.Drawing.Color.White;
            this.lblHeadPesan.Location = new System.Drawing.Point(0, 0);
            this.lblHeadPesan.Name = "lblHeadPesan";
            // 
            // 
            // 
            this.lblHeadPesan.RootElement.CustomFont = "Roboto";
            this.lblHeadPesan.RootElement.CustomFontSize = 9F;
            this.lblHeadPesan.RootElement.CustomFontStyle = System.Drawing.FontStyle.Regular;
            this.lblHeadPesan.Size = new System.Drawing.Size(329, 27);
            this.lblHeadPesan.TabIndex = 0;
            this.lblHeadPesan.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.lblHeadPesan.TextWrap = false;
            this.lblHeadPesan.ThemeName = "ControlDefault";
            ((Telerik.WinControls.UI.RadLabelElement)(this.lblHeadPesan.GetChildAt(0))).TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            ((Telerik.WinControls.UI.RadLabelElement)(this.lblHeadPesan.GetChildAt(0))).Text = "";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.lblHeadPesan.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblHeadPesan.GetChildAt(0).GetChildAt(1))).FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblHeadPesan.GetChildAt(0).GetChildAt(1))).GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblHeadPesan.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            // 
            // loadingPesan
            // 
            this.loadingPesan.BackColor = System.Drawing.Color.White;
            this.loadingPesan.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.loader;
            this.loadingPesan.Location = new System.Drawing.Point(0, 6);
            this.loadingPesan.Margin = new System.Windows.Forms.Padding(0);
            this.loadingPesan.Name = "loadingPesan";
            this.loadingPesan.Size = new System.Drawing.Size(16, 16);
            this.loadingPesan.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.loadingPesan.TabIndex = 11;
            this.loadingPesan.TabStop = false;
            // 
            // radLabel10
            // 
            this.radLabel10.AutoSize = false;
            this.radLabel10.BackColor = System.Drawing.Color.Transparent;
            this.radLabel10.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.msg;
            this.radLabel10.Location = new System.Drawing.Point(0, 5);
            this.radLabel10.Margin = new System.Windows.Forms.Padding(0);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(25, 20);
            this.radLabel10.TabIndex = 9;
            // 
            // toolTip8
            // 
            this.toolTip8.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip8.ToolTipTitle = "Info";
            // 
            // toolTip9
            // 
            this.toolTip9.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip9.ToolTipTitle = "Info";
            // 
            // toolTip10
            // 
            this.toolTip10.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip10.ToolTipTitle = "Info";
            // 
            // radWaitingBar1
            // 
            this.radWaitingBar1.Location = new System.Drawing.Point(8000, 8000);
            this.radWaitingBar1.Name = "radWaitingBar1";
            this.radWaitingBar1.Size = new System.Drawing.Size(162, 30);
            this.radWaitingBar1.TabIndex = 186;
            this.radWaitingBar1.Text = "radWaitingBar1";
            // 
            // 
            // 
            this.radWaitingBar1.WaitingBarElement.WaitingIndicatorSize = new System.Drawing.Size(100, 14);
            this.radWaitingBar1.WaitingIndicators.Add(this.waitingBarIndicatorElement2);
            this.radWaitingBar1.WaitingIndicators.Add(this.waitingBarIndicatorElement1);
            this.radWaitingBar1.WaitingIndicatorSize = new System.Drawing.Size(100, 14);
            this.radWaitingBar1.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.Dash;
            ((Telerik.WinControls.UI.RadWaitingBarElement)(this.radWaitingBar1.GetChildAt(0))).WaitingIndicatorSize = new System.Drawing.Size(100, 14);
            // 
            // waitingBarIndicatorElement2
            // 
            this.waitingBarIndicatorElement2.Name = "waitingBarIndicatorElement2";
            this.waitingBarIndicatorElement2.StretchHorizontally = false;
            // 
            // waitingBarIndicatorElement1
            // 
            this.waitingBarIndicatorElement1.Name = "waitingBarIndicatorElement1";
            this.waitingBarIndicatorElement1.StretchHorizontally = false;
            // 
            // tahananDetail
            // 
            this.tahananDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tahananDetail.Location = new System.Drawing.Point(2040, 0);
            this.tahananDetail.Name = "tahananDetail";
            this.tahananDetail.Padding = new System.Windows.Forms.Padding(5);
            this.tahananDetail.Size = new System.Drawing.Size(845, 575);
            this.tahananDetail.TabIndex = 192;
            this.tahananDetail.Visible = false;
            // 
            // statusTahananDetail
            // 
            this.statusTahananDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.statusTahananDetail.Location = new System.Drawing.Point(2040, 0);
            this.statusTahananDetail.Name = "statusTahananDetail";
            this.statusTahananDetail.Padding = new System.Windows.Forms.Padding(5);
            this.statusTahananDetail.Size = new System.Drawing.Size(845, 575);
            this.statusTahananDetail.TabIndex = 192;
            this.statusTahananDetail.Visible = false;
            // 
            // perpanjangantahanan
            // 
            this.perpanjangantahanan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.perpanjangantahanan.Location = new System.Drawing.Point(2040, 0);
            this.perpanjangantahanan.Name = "perpanjangantahanan";
            this.perpanjangantahanan.Padding = new System.Windows.Forms.Padding(5);
            this.perpanjangantahanan.Size = new System.Drawing.Size(845, 575);
            this.perpanjangantahanan.TabIndex = 192;
            this.perpanjangantahanan.Visible = false;
            // 
            // tahananDetailCheckedOut
            // 
            this.tahananDetailCheckedOut.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tahananDetailCheckedOut.Location = new System.Drawing.Point(2040, 0);
            this.tahananDetailCheckedOut.Name = "tahananDetailCheckedOut";
            this.tahananDetailCheckedOut.Padding = new System.Windows.Forms.Padding(5);
            this.tahananDetailCheckedOut.Size = new System.Drawing.Size(845, 575);
            this.tahananDetailCheckedOut.TabIndex = 192;
            this.tahananDetailCheckedOut.Visible = false;
            // 
            // searchData
            // 
            this.searchData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.searchData.Location = new System.Drawing.Point(3650, 131);
            this.searchData.Name = "searchData";
            this.searchData.Padding = new System.Windows.Forms.Padding(5);
            this.searchData.Size = new System.Drawing.Size(925, 521);
            this.searchData.TabIndex = 196;
            this.searchData.Visible = false;
            // 
            // visitDetail
            // 
            this.visitDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.visitDetail.Location = new System.Drawing.Point(32739, 38);
            this.visitDetail.Name = "visitDetail";
            this.visitDetail.Padding = new System.Windows.Forms.Padding(5);
            this.visitDetail.Size = new System.Drawing.Size(995, 647);
            this.visitDetail.TabIndex = 198;
            // 
            // visitorDetail
            // 
            this.visitorDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.visitorDetail.Location = new System.Drawing.Point(327, 38);
            this.visitorDetail.Name = "visitorDetail";
            this.visitorDetail.Padding = new System.Windows.Forms.Padding(5);
            this.visitorDetail.Size = new System.Drawing.Size(838, 646);
            this.visitorDetail.TabIndex = 199;
            this.visitorDetail.Visible = false;
            // 
            // visitorVIPDetail
            // 
            this.visitorDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.visitorDetail.Location = new System.Drawing.Point(327, 38);
            this.visitorDetail.Name = "visitorVIPDetail";
            this.visitorDetail.Padding = new System.Windows.Forms.Padding(5);
            this.visitorDetail.Size = new System.Drawing.Size(838, 646);
            this.visitorDetail.TabIndex = 199;
            this.visitorDetail.Visible = false;
            // 
            // userDetail
            // 
            this.userDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.userDetail.Location = new System.Drawing.Point(32700, 38);
            this.userDetail.Margin = new System.Windows.Forms.Padding(15);
            this.userDetail.Name = "userDetail";
            this.userDetail.Padding = new System.Windows.Forms.Padding(5);
            this.userDetail.Size = new System.Drawing.Size(995, 620);
            this.userDetail.TabIndex = 207;
            this.userDetail.Visible = false;
            // 
            // changepsw
            // 
            this.changepsw.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.changepsw.Location = new System.Drawing.Point(32700, 144);
            this.changepsw.Name = "changepsw";
            this.changepsw.Padding = new System.Windows.Forms.Padding(5);
            this.changepsw.Size = new System.Drawing.Size(760, 413);
            this.changepsw.TabIndex = 209;
            this.changepsw.Visible = false;
            // 
            // cellDetail
            // 
            this.cellDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cellDetail.Location = new System.Drawing.Point(32700, -4);
            this.cellDetail.Name = "cellDetail";
            this.cellDetail.Padding = new System.Windows.Forms.Padding(5);
            this.cellDetail.Size = new System.Drawing.Size(883, 615);
            this.cellDetail.TabIndex = 211;
            // 
            // _Dashboard
            // 
            this._Dashboard.BackColor = System.Drawing.Color.Transparent;
            this._Dashboard.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_Dashboard.BackgroundImage")));
            this._Dashboard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this._Dashboard.Location = new System.Drawing.Point(32739, 47);
            this._Dashboard.Name = "_Dashboard";
            this._Dashboard.Size = new System.Drawing.Size(1239, 689);
            this._Dashboard.TabIndex = 213;
            // 
            // captureFingerPrint1
            // 
            this.captureFingerPrint1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(204)))), ((int)(((byte)(51)))));
            this.captureFingerPrint1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.captureFingerPrint1.Location = new System.Drawing.Point(32739, 165);
            this.captureFingerPrint1.Margin = new System.Windows.Forms.Padding(0);
            this.captureFingerPrint1.Name = "captureFingerPrint1";
            this.captureFingerPrint1.Size = new System.Drawing.Size(278, 427);
            this.captureFingerPrint1.TabIndex = 217;
            this.captureFingerPrint1.Visible = false;
            // 
            // fingerPrint_Inmate
            // 
            this.fingerPrint_Inmate.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.fingerPrint_Inmate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fingerPrint_Inmate.Location = new System.Drawing.Point(27500, 17);
            this.fingerPrint_Inmate.Name = "fingerPrint_Inmate";
            this.fingerPrint_Inmate.Size = new System.Drawing.Size(830, 592);
            this.fingerPrint_Inmate.TabIndex = 215;
            this.fingerPrint_Inmate.Visible = false;
            // 
            // cellAllocationDetail
            // 
            this.cellAllocationDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.cellAllocationDetail.Location = new System.Drawing.Point(20100, 24);
            this.cellAllocationDetail.Name = "cellAllocationDetail";
            this.cellAllocationDetail.Padding = new System.Windows.Forms.Padding(5);
            this.cellAllocationDetail.Size = new System.Drawing.Size(1170, 735);
            this.cellAllocationDetail.TabIndex = 219;
            // 
            // checkedOut
            // 
            this.checkedOut.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.checkedOut.Location = new System.Drawing.Point(327, 157);
            this.checkedOut.Name = "checkedOut";
            this.checkedOut.Padding = new System.Windows.Forms.Padding(5);
            this.checkedOut.Size = new System.Drawing.Size(809, 384);
            this.checkedOut.TabIndex = 221;
            // 
            // checkedOutinmates
            // 
            this.checkedOutinmates.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.checkedOutinmates.Location = new System.Drawing.Point(327, 157);
            this.checkedOutinmates.Name = "checkedOutinmates";
            this.checkedOutinmates.Padding = new System.Windows.Forms.Padding(5);
            this.checkedOutinmates.Size = new System.Drawing.Size(809, 384);
            this.checkedOutinmates.TabIndex = 221;
            // 
            // capturePhoto1
            // 
            this.capturePhoto.Location = new System.Drawing.Point(502, -9);
            this.capturePhoto.Name = "capturePhoto1";
            this.capturePhoto.Size = new System.Drawing.Size(395, 660);
            this.capturePhoto.TabIndex = 223;
            this.capturePhoto.Visible = false;
            // 
            // opacity
            // 
            this.opacity.Location = new System.Drawing.Point(0, 0);
            this.opacity.Name = "opacity";
            this.opacity.Size = new System.Drawing.Size(1000, 1000);
            this.opacity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.opacity.TabIndex = 223;
            this.opacity.Visible = false;
            // 
            // seizedAssetsDetail
            // 
            this.seizedAssetsDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.seizedAssetsDetail.Location = new System.Drawing.Point(327, 38);
            this.seizedAssetsDetail.Name = "seizedAssetsDetail";
            this.seizedAssetsDetail.Padding = new System.Windows.Forms.Padding(5);
            this.seizedAssetsDetail.Size = new System.Drawing.Size(995, 646);
            this.seizedAssetsDetail.TabIndex = 224;
            this.seizedAssetsDetail.Visible = false;
            // 
            // newSeizedAssets
            // 
            this.newSeizedAssets.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.newSeizedAssets.Location = new System.Drawing.Point(327, 38);
            this.newSeizedAssets.Name = "newSeizedAssets";
            this.newSeizedAssets.Padding = new System.Windows.Forms.Padding(5);
            this.newSeizedAssets.Size = new System.Drawing.Size(800, 600);
            this.newSeizedAssets.TabIndex = 224;
            this.newSeizedAssets.Visible = false;
            // 
			// keperluanBonTahanan
            // 
            this.keperluanBonTahanan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.keperluanBonTahanan.Location = new System.Drawing.Point(327, 38);
            this.keperluanBonTahanan.Name = "keperluanBonTahanan";
            this.keperluanBonTahanan.Padding = new System.Windows.Forms.Padding(5);
            this.keperluanBonTahanan.Size = new System.Drawing.Size(800, 375);
            this.keperluanBonTahanan.TabIndex = 224;
            this.keperluanBonTahanan.Visible = false;
            // 
            // kendaraanTahananDetail
            // 
            this.kendaraanTahananDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.kendaraanTahananDetail.Location = new System.Drawing.Point(2040, 0);
            this.kendaraanTahananDetail.Name = "kendaraanTahananDetail";
            this.kendaraanTahananDetail.Padding = new System.Windows.Forms.Padding(5);
            this.kendaraanTahananDetail.Size = new System.Drawing.Size(845, 620);
            this.kendaraanTahananDetail.TabIndex = 192;
            this.kendaraanTahananDetail.Visible = false;
            // 
            // bonTahananDetail
            // 
            this.bonTahananDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bonTahananDetail.Location = new System.Drawing.Point(2040, 0);
            this.bonTahananDetail.Name = "bonTahananDetail";
            this.bonTahananDetail.Padding = new System.Windows.Forms.Padding(5);
            this.bonTahananDetail.Size = new System.Drawing.Size(845, 620);
            this.bonTahananDetail.TabIndex = 192;
            this.bonTahananDetail.Visible = false;
            // 
            // jadwalKunjunganDetail
            // 
            this.jadwalKunjunganDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.jadwalKunjunganDetail.Location = new System.Drawing.Point(2040, 0);
            this.jadwalKunjunganDetail.Name = "jadwalKunjunganDetail";
            this.jadwalKunjunganDetail.Padding = new System.Windows.Forms.Padding(5);
            this.jadwalKunjunganDetail.Size = new System.Drawing.Size(845, 620);
            this.jadwalKunjunganDetail.TabIndex = 192;
            this.jadwalKunjunganDetail.Visible = false;
            // 
            // settings
            // 
            this.settings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.settings.Location = new System.Drawing.Point(32739, 128);
            this.settings.Name = "settings";
            this.settings.Padding = new System.Windows.Forms.Padding(5);
            this.settings.Size = new System.Drawing.Size(914, 540);
            this.settings.TabIndex = 201;
            this.settings.Visible = false;
            // 
            // cameraSource
            // 
            this.cameraSource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cameraSource.Location = new System.Drawing.Point(32700, 320);
            this.cameraSource.Name = "cameraSource";
            this.cameraSource.Size = new System.Drawing.Size(581, 186);
            this.cameraSource.TabIndex = 82;
            this.cameraSource.Visible = false;
            // 
            // Main
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1654, 783);
            this.Controls.Add(this.visitorVIPDetail);
            this.Controls.Add(this.jadwalKunjunganDetail);
            this.Controls.Add(this.keperluanBonTahanan);
            this.Controls.Add(this.settings);
            this.Controls.Add(this.captureFingerPrint1);
            this.Controls.Add(this.fingerPrint_Inmate);
            this.Controls.Add(this.opacity);
            this.Controls.Add(this.kendaraanTahananDetail);
            this.Controls.Add(this.statusTahananDetail);
            this.Controls.Add(this.bonTahananDetail);
            this.Controls.Add(this.newSeizedAssets);
            this.Controls.Add(this.seizedAssetsDetail);
            this.Controls.Add(this.capturePhoto);
            this.Controls.Add(this.checkedOut);
            this.Controls.Add(this.checkedOutinmates);
            this.Controls.Add(this.cameraSource);
            this.Controls.Add(this.cellAllocationDetail);

            this.Controls.Add(this._Dashboard);
            this.Controls.Add(this.cellDetail);
            this.Controls.Add(this.changepsw);
            this.Controls.Add(this.userDetail);
            this.Controls.Add(this.visitorDetail);
            this.Controls.Add(this.visitDetail);
            this.Controls.Add(this.searchData);
            this.Controls.Add(this.tahananDetail);
            this.Controls.Add(this.perpanjangantahanan);
            this.Controls.Add(this.tahananDetailCheckedOut);
            this.Controls.Add(this.radWaitingBar1);
            this.Controls.Add(this.pesanDetail);
            this.Controls.Add(this.synchDetail);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.titleBarControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Main";
            this.Text = "Main";
            this.ThemeName = "Material";
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainContainer)).EndInit();
            this.mainContainer.ResumeLayout(false);
            this.OverviewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PanelHome)).EndInit();
            this.PanelHome.ResumeLayout(false);
            this.PanelHome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.overviewView)).EndInit();
            this.overviewView.ResumeLayout(false);
            this.AddDocView.ResumeLayout(false);
            this.radCollapsiblePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCollapsiblePanel1)).EndInit();
            this.radCollapsiblePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBreadCrumb1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.synchDetail)).EndInit();
            this.synchDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnSynch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStatusCon)).EndInit();
            this.lblStatusCon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loadingSync)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onoff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pesanDetail)).EndInit();
            this.pesanDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblHeadPesan)).EndInit();
            this.lblHeadPesan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loadingPesan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CustomControls.TitleBarControl titleBarControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadCollapsiblePanel radCollapsiblePanel1;
        private Telerik.WinControls.UI.RadTreeView radTreeView1;
        private Telerik.WinControls.UI.RadBreadCrumb radBreadCrumb1;
        private CustomControls.TopControl topControl1;
        private CustomControls.TopLeft topLeft1;
        private Telerik.WinControls.UI.RadPageView mainContainer;
        private Telerik.WinControls.UI.RadPageViewPage OverviewPage;
        private System.Windows.Forms.ToolTip toolTip1;
        private Telerik.WinControls.UI.RadPageViewPage AddDocView;
        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ToolTip toolTip3;
        private System.Windows.Forms.ToolTip toolTip4;
        private System.Windows.Forms.ToolTip toolTip5;
        private System.Windows.Forms.ToolTip toolTip6;
        private Telerik.WinControls.UI.RadPanel synchDetail;
        private Telerik.WinControls.UI.RadLabel lblStatusCon;
        private Telerik.WinControls.UI.RadLabel onoff;
        private System.Windows.Forms.ToolTip toolTip7;
        private System.Windows.Forms.PictureBox loadingSync;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadButton btnSynch;
        private Telerik.WinControls.UI.RadPanel pesanDetail;
        private Telerik.WinControls.UI.RadLabel lblHeadPesan;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private System.Windows.Forms.PictureBox loadingPesan;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.ToolTip toolTip8;
        private System.Windows.Forms.ToolTip toolTip9;
        private System.Windows.Forms.ToolTip toolTip10;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar1;
        private Telerik.WinControls.UI.WaitingBarIndicatorElement waitingBarIndicatorElement2;
        private Telerik.WinControls.UI.WaitingBarIndicatorElement waitingBarIndicatorElement1;
        private CustomControls.FormUser user;
        private CustomControls.FormTahanan tahanan;
        private CustomControls.TahananDetail tahananDetail;
        private CustomControls.FormPerpanjangan1 perpanjangantahanan;
        private CustomControls.TahananDetailCheckedOut tahananDetailCheckedOut;
        private CustomControls.SearchData searchData;
        private CustomControls.VisitDetail visitDetail;
        private CustomControls.VisitorDetail visitorDetail;
        private CustomControls.VisitorVIPDetail visitorVIPDetail;
        private CustomControls.FormVisit visit;
        private CustomControls.UserDetail userDetail;
        private CustomControls.FormVisitorShort visitor;
        private CustomControls.FormVisitorVIP visitorVIP;
        private CustomControls.FormPswChange changepsw;
        private CustomControls.FormCell cell;
        private CustomControls.CellDetail cellDetail;
        private Telerik.WinControls.UI.RadListView overviewView;
        private CustomControls.ListAuditTrail listaudittrail;
        private CustomControls.ListCell listCell;
        private CustomControls.Dashboard dashboard;
        private CustomControls.ListKunjungan listKunjungan;
        private CustomControls.ListKunjunganVIP listKunjunganVIP;
        private CustomControls.ListVisitor listVisitor;
        private CustomControls.ListVisitorVIP listVisitorVIP;
        private CustomControls.ListTahanan listTahanan;
        private CustomControls.DaftarTahanan daftarTahanan;
        private CustomControls.ListUser listUser;
        private Telerik.WinControls.UI.RadPanel PanelHome;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadLabel lblVersion;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private CustomControls.Dashboard _Dashboard;
        private CustomControls.ListCellAllocation listCellAllocation;
        private CustomControls.CaptureFingerPrint captureFingerPrint1;
        private CustomControls.FingerPrint_Inmate fingerPrint_Inmate;
        private CustomControls.FormCellAllocation cellAllocation;
        private CustomControls.CellAllocationDetail cellAllocationDetail;
        private CustomControls.CheckedOutConfirm checkedOut;
        private CustomControls.CheckedOutInmatesConfirm checkedOutinmates;
        private CustomControls.RptCellOccupancy rptCellOccupancy;
        private CustomControls.RptStatusTahanan rptStatusTahanan;
        private CustomControls.CapturePhoto capturePhoto;
        private CustomControls.ListSeizedAssets listSeizedAssets;
        private CustomControls.FormSeizedAssets seizedAssets;
        private CustomControls.SeizedAssetsDetail seizedAssetsDetail;
        private CustomControls.NewSeizedAssets newSeizedAssets;
        private CustomControls.ChangeSettings settings;
        private CustomControls.CameraSource cameraSource;
        private CustomControls.Opacity opacity;
        private CustomControls.ListPrintFormVisitor listPrintFormVisitor;
        private CustomControls.ListPrintFormTahanan listPrintFormTahanan;
        private CustomControls.PreviewDoc previewDoc;
        private CustomControls.RptInmates rptInmates;
        private CustomControls.RptCases rptCases;
		private CustomControls.ListBonTahanan listBonTahanan;
        private CustomControls.FormBonTahanan bonTahanan;
        private CustomControls.BonTahananDetail bonTahananDetail;
        private CustomControls.FormKeperluanBonTahanan keperluanBonTahanan;
        private CustomControls.FormSerahTahanan bonSerahTahanan;
        private CustomControls.ListBonSerahTahanan listBonSerahTahanan;
        private CustomControls.FormTerimaTahanan bonTerimaTahanan;
        private CustomControls.ListBonTerimaTahanan listBonTerimaTahanan;
        private CustomControls.ListKendaraan listkendaraan;
        private CustomControls.ListStatusTahanan liststatustahanan;
        private CustomControls.FormKendaraan kendaraan;
        private CustomControls.KendaraanTahananDetail kendaraanTahananDetail;
        private CustomControls.StatusTahananDetail statusTahananDetail;
        private CustomControls.ListKendaraanKeluar listkendaraankeluar;
        private CustomControls.ListJadwalKunjungan listJadwalKunjungan;
        private CustomControls.FormJadwalKunjungan jadwalKunjungan;
        private CustomControls.JadwalKunjunganDetail jadwalKunjunganDetail;
        private CustomControls.PDFViewer pdfViewer;
        private CustomControls.FormStatusTahanan statusTahanan;
    }
}
