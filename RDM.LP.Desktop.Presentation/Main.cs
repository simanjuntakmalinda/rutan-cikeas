﻿using RDM.LP.DataAccess.Service;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.Desktop.Presentation.CustomControls;
using RDM.LP.Desktop.Presentation.CustomElements;
using RDM.LP.Desktop.Presentation.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.Primitives;
using Telerik.WinControls.UI;

namespace RDM.LP.Desktop.Presentation
{
    public partial class Main : Telerik.WinControls.UI.ShapedForm
    {
        #region Global
        public RadPageView MainContainer { get { return this.mainContainer; } }

        public RadPageViewPage Overview { get { return this.OverviewPage; } }
        public RadPageViewPage AddNew { get { return this.AddDocView; } }
        

        public TitleBarControl TitleBar { get { return this.titleBarControl1; } }
        public RadButton BtnSynch { get { return this.btnSynch; } }
        public RadWaitingBar WaitingBar { get { return this.radWaitingBar1; } }
        public ChangeSettings setting { get { return this.settings; } }

        public ListUser ListUser { get { return this.listUser; } }
        public ListStatusTahanan ListStatusTahanan { get { return this.liststatustahanan; } }
        public ListTahanan ListTahanan { get { return this.listTahanan; } }
        public DaftarTahanan DaftarTahanan { get { return this.daftarTahanan; } }
        public ListVisitor ListVisitor { get { return this.listVisitor; } }
        public ListVisitorVIP ListVisitorVIP { get { return this.listVisitorVIP; } }
        public ListKunjungan ListKunjungan { get { return this.listKunjungan; } }
        public ListKunjunganVIP ListKunjunganVIP { get { return this.listKunjunganVIP; } }
        public ListCell ListCell { get { return this.listCell; } }
        public ListAuditTrail AuditTrail { get { return this.listaudittrail; } }
        public ListCellAllocation ListCellAllocation { get { return this.listCellAllocation; } }
		public ListBonTahanan ListBonTahanan { get { return this.listBonTahanan; } }
        public ListBonSerahTahanan ListBonSerahTahanan { get { return this.listBonSerahTahanan; } }
        public ListBonTerimaTahanan ListBonTerimaTahanan { get { return this.listBonTerimaTahanan; } }
        public ListKendaraan ListKendaraan { get { return this.listkendaraan; } }
        public ListKendaraanKeluar ListKendaraanKeluar { get { return this.listkendaraankeluar; } }
        public ListPrintFormTahanan ListPrintFormTahanan { get { return this.listPrintFormTahanan; } }
        public ListPrintFormVisitor ListPrintFormVisitor { get { return this.listPrintFormVisitor; } }

        public FingerPrint_Inmate FingerPrint_Inmate { get { return this.fingerPrint_Inmate; } }
        public CaptureFingerPrint CaptureFingerPrint { get { return this.captureFingerPrint1; } }
        
        public StatusTahananDetail StatusTahananDetail { get { return this.statusTahananDetail; } }
        public TahananDetail TahananDetail { get { return this.tahananDetail; } }
        public TahananDetailCheckedOut TahananDetailCheckedOut { get { return this.tahananDetailCheckedOut; } }
        public VisitorDetail VisitorDetail { get { return this.visitorDetail; } }
        public VisitorVIPDetail VisitorVIPDetail { get { return this.visitorVIPDetail; } }
        public VisitDetail KunjunganDetail { get { return this.visitDetail; } }
        public UserDetail UserDetail { get { return this.userDetail; } }
        public CellDetail CellDetail { get { return this.cellDetail; } }
        public CellAllocationDetail CellAllocationDetail { get { return this.cellAllocationDetail; } }
		public BonTahananDetail BonTahananDetail { get { return this.bonTahananDetail; } }
        public KendaraanTahananDetail KendaraanTahananDetail { get { return this.kendaraanTahananDetail; } }
        public JadwalKunjunganDetail JadwalKunjunganDetail { get { return this.jadwalKunjunganDetail; } }

        public CheckedOutConfirm CheckedOut { get { return this.checkedOut; } }
        public CheckedOutInmatesConfirm CheckedOutInmates { get { return this.checkedOutinmates; } }
        public SearchData SearchData { get { return this.searchData; } }
        public FormPerpanjangan1 PerpanjanganTahanan { get { return this.perpanjangantahanan; } }

        public FormVisitorShort Visit { get { return this.visitor; } }
        public FormVisitorVIP VisitorVIP { get { return this.visitorVIP; } }
        public FormUser User { get { return this.user; } }
        public FormTahanan Tahanan { get { return this.tahanan; } }
        public FormCell Cell { get { return this.cell; } }
        public FormCellAllocation CellAllocation { get { return this.cellAllocation; } }
		public FormBonTahanan BonTahanan { get { return this.bonTahanan; } }
        public FormSerahTahanan BonSerahTahanan { get { return this.bonSerahTahanan; } }
        public FormTerimaTahanan BonTerimaTahanan { get { return this.bonTerimaTahanan; } }
        public FormJadwalKunjungan JadwalKunjungan { get { return this.jadwalKunjungan; } }
        public FormStatusTahanan StatusTahanan { get { return this.statusTahanan; } }

        public Opacity Opacity { get { return this.opacity; } }
        public CapturePhoto CapturePhoto { get { return this.capturePhoto; } }
        public PreviewDoc PreviewDoc { get { return this.previewDoc; } }

        private RadContextMenu menu = new RadContextMenu();
        private ListViewDataItem item = null;

        public ListSeizedAssets ListSeizedAssets { get { return this.listSeizedAssets; } }
        public FormSeizedAssets SeizedAssets { get { return this.seizedAssets; } }
        public SeizedAssetsDetail SeizedAssetsDetail { get { return this.seizedAssetsDetail; } }
        public NewSeizedAssets NewSeizedAssets { get { return this.newSeizedAssets; } }
        public CameraSource CameraSource { get { return this.cameraSource; } }
		public FormKeperluanBonTahanan FormKeperluanBonTahanan { get { return this.keperluanBonTahanan; } }
        public FormKendaraan KendaraanTahanan { get { return this.kendaraan; } }
        public ListJadwalKunjungan ListJadwalKunjungan { get { return this.listJadwalKunjungan; } }

        public PDFViewer PDFViewer { get { return this.pdfViewer; } }

        #endregion

        public Main()
        {
            //try
            //{
                InitializeComponent();
                PopulateTreeViewMenu();
                InitEvents();
                InitForm();
            //}
            //catch (Exception ex)
            //{
            //    UserFunction.MsgBox(TipeMsg.Error, ex.Message);
            //    Environment.Exit(0);
            //}
        }

        private void PopulateTreeViewMenu()
        {
            var color = Color.FromArgb(77, 77, 77);
            var color2 = Color.FromArgb(175, 202, 203);

            RadTreeNode homeNode = new RadTreeNode("BERANDA");
            homeNode.Name = "Home";
            homeNode.ForeColor = Color.White;
            homeNode.BackColor = color;
            radTreeView1.Nodes.Add(homeNode);


            AppUserService userserv = new AppUserService();
            var role = userserv.GetByUserName(GlobalVariables.UserID);


            if (role.UserManagementModul)
            { 
                //MASTER USER 
                RadTreeNode userNode = new RadTreeNode("MASTER PENGGUNA");
                userNode.Name = "Data";
                userNode.ForeColor = Color.White;
                //dataNode.FontStyle = FontStyle.Bold;
                userNode.BackColor = color;
                radTreeView1.Nodes.Add(userNode);

                RadTreeNode nd_pengguna = new RadTreeNode("Daftar Pengguna");
                nd_pengguna.Name = "ListPengguna";
                nd_pengguna.Tag = item;
                nd_pengguna.Image = Resources.user_color;
                nd_pengguna.BackColor = color;
                nd_pengguna.ForeColor = color2;
                userNode.Nodes.Add(nd_pengguna);
            }

            if (role.CellAllocationModul || role.JadwalKunjunganModul)
            {
                RadTreeNode cellAllocationNode = new RadTreeNode("ALOKASI");
                cellAllocationNode.ForeColor = Color.White;
                //kunjunganNode.TreeViewElement.CustomFontStyle = FontStyle.Bold;
                cellAllocationNode.BackColor = color;
                cellAllocationNode.Name = "Allocation";
                radTreeView1.Nodes.Add(cellAllocationNode);

                if (role.CellAllocationModul)
                {
                    RadTreeNode nd_cell = new RadTreeNode("Daftar Sel");
                    nd_cell.Name = "ListCell";
                    nd_cell.Tag = item;
                    nd_cell.Image = Resources.user_color;
                    nd_cell.BackColor = color;
                    nd_cell.ForeColor = color2;
                    cellAllocationNode.Nodes.Add(nd_cell);

                    RadTreeNode nd_allocation = new RadTreeNode("Alokasi Sel");
                    nd_allocation.Name = "ListCellAllocation";
                    nd_allocation.Image = Resources.keyboard;
                    nd_allocation.Tag = item;
                    nd_allocation.BackColor = color;
                    nd_allocation.ForeColor = color2;
                    cellAllocationNode.Nodes.Add(nd_allocation);
                }

                if (role.JadwalKunjunganModul)
                {
                    RadTreeNode nd_jadwalkunjungan = new RadTreeNode("Jadwal Kunjungan");
                    nd_jadwalkunjungan.Name = "ListJadwalKunjungan";
                    nd_jadwalkunjungan.Image = Resources.keyboard;
                    nd_jadwalkunjungan.Tag = item;
                    nd_jadwalkunjungan.BackColor = color;
                    nd_jadwalkunjungan.ForeColor = color2;
                    cellAllocationNode.Nodes.Add(nd_jadwalkunjungan);
                }
            }

            if (role.InmateModul || role.VisitorModul || role.KendaraanModul)
            { 
                RadTreeNode kunjunganNode = new RadTreeNode("REGISTRASI");
                kunjunganNode.ForeColor = Color.White;
                //kunjunganNode.TreeViewElement.CustomFontStyle = FontStyle.Bold;
                kunjunganNode.BackColor = color;
                kunjunganNode.Name = "Kunjungan";
                radTreeView1.Nodes.Add(kunjunganNode);


                if (role.InmateModul)
                {
                    RadTreeNode nd_tahanan = new RadTreeNode("Registrasi Tahanan");
                    nd_tahanan.Name = "ListTahanan";
                    nd_tahanan.Image = Resources.kunjungan_color;
                    nd_tahanan.Tag = item;
                    nd_tahanan.BackColor = color;
                    nd_tahanan.ForeColor = color2;
                    kunjunganNode.Nodes.Add(nd_tahanan);

                    RadTreeNode nd_checkedouttahanan = new RadTreeNode("Daftar Tahanan");
                    nd_checkedouttahanan.Name = "DaftarTahanan";
                    nd_checkedouttahanan.Image = Resources.kunjungan_color;
                    nd_checkedouttahanan.Tag = item;
                    nd_checkedouttahanan.BackColor = color;
                    nd_checkedouttahanan.ForeColor = color2;
                    kunjunganNode.Nodes.Add(nd_checkedouttahanan);

                    RadTreeNode nd_statustahanan = new RadTreeNode("Daftar Status Tahanan");
                    nd_statustahanan.Name = "ListStatusTahanan";
                    nd_statustahanan.Image = Resources.kunjungan_color;
                    nd_statustahanan.Tag = item;
                    nd_statustahanan.BackColor = color;
                    nd_statustahanan.ForeColor = color2;
                    kunjunganNode.Nodes.Add(nd_statustahanan);
                }

                if (role.VisitorModul)
                {
                    RadTreeNode nd_pengunjung = new RadTreeNode("Registrasi Pengunjung");
                    nd_pengunjung.Name = "ListPegunjung";
                    nd_pengunjung.Image = Resources.pegunjung_color;
                    nd_pengunjung.Tag = item;
                    nd_pengunjung.BackColor = color;
                    nd_pengunjung.ForeColor = color2;
                    kunjunganNode.Nodes.Add(nd_pengunjung);

                    RadTreeNode nd_visiting = new RadTreeNode("Daftar Kunjungan");
                    nd_visiting.Name = "ListKunjungan";
                    nd_visiting.Image = Resources.user_menu;
                    nd_visiting.Tag = item;
                    nd_visiting.BackColor = color;
                    nd_visiting.ForeColor = color2;
                    kunjunganNode.Nodes.Add(nd_visiting);

                    RadTreeNode nd_pengunjungVIP = new RadTreeNode("Pengunjung VIP");
                    nd_pengunjungVIP.Name = "ListPegunjungVIP";
                    nd_pengunjungVIP.Image = Resources.pegunjung_color;
                    nd_pengunjungVIP.Tag = item;
                    nd_pengunjungVIP.BackColor = color;
                    nd_pengunjungVIP.ForeColor = color2;
                    kunjunganNode.Nodes.Add(nd_pengunjungVIP);

                    RadTreeNode nd_visitingVIP = new RadTreeNode("Kunjungan VIP");
                    nd_visitingVIP.Name = "ListKunjunganVIP";
                    nd_visitingVIP.Image = Resources.user_menu;
                    nd_visitingVIP.Tag = item;
                    nd_visitingVIP.BackColor = color;
                    nd_visitingVIP.ForeColor = color2;
                    kunjunganNode.Nodes.Add(nd_visitingVIP);
                }

                if (role.KendaraanModul)
                {
                    RadTreeNode nd_kendaraan = new RadTreeNode("Daftar Kendaraan");
                    nd_kendaraan.Name = "ListKendaraan";
                    nd_kendaraan.Tag = item;
                    nd_kendaraan.Image = Resources.kendaraan;
                    nd_kendaraan.BackColor = color;
                    nd_kendaraan.ForeColor = color2;
                    kunjunganNode.Nodes.Add(nd_kendaraan);

                    RadTreeNode nd_kendaraankeluar = new RadTreeNode("Kendaraan Keluar");
                    nd_kendaraankeluar.Name = "ListKendaraanKeluar";
                    nd_kendaraankeluar.Tag = item;
                    nd_kendaraankeluar.Image = Resources.Kendaraankeluar;
                    nd_kendaraankeluar.BackColor = color;
                    nd_kendaraankeluar.ForeColor = color2;
                    kunjunganNode.Nodes.Add(nd_kendaraankeluar);
                }
            }

            if (role.SeizedAssetsModul || role.BonTahananModul)
            {
                RadTreeNode dokumentasiNode = new RadTreeNode("DOKUMENTASI");
                dokumentasiNode.ForeColor = Color.White;
                //dokumentasiNode.TreeViewElement.CustomFontStyle = FontStyle.Bold;
                dokumentasiNode.BackColor = color;
                dokumentasiNode.Name = "Dokumentasi";
                radTreeView1.Nodes.Add(dokumentasiNode);

                if (role.SeizedAssetsModul)
                {
                    RadTreeNode nd_assets = new RadTreeNode("Barang Bukti");
                    nd_assets.Name = "ListSeizedAssets";
                    nd_assets.Image = Resources.form;
                    nd_assets.Tag = item;
                    nd_assets.BackColor = color;
                    nd_assets.ForeColor = color2;
                    dokumentasiNode.Nodes.Add(nd_assets);
                }

                if (role.BonTahananModul)
                {
                    RadTreeNode nd_bonTahanan = new RadTreeNode("Bon Tahanan");
                    nd_bonTahanan.Name = "ListBonTahanan";
                    nd_bonTahanan.Image = Resources.form2;
                    nd_bonTahanan.Tag = item;
                    nd_bonTahanan.BackColor = color;
                    nd_bonTahanan.ForeColor = color2;
                    dokumentasiNode.Nodes.Add(nd_bonTahanan);

                    RadTreeNode nd_bonSerahTahanan = new RadTreeNode("Bon Serah Tahanan");
                    nd_bonSerahTahanan.Name = "ListBonSerahTahanan";
                    nd_bonSerahTahanan.Image = Resources.form2;
                    nd_bonSerahTahanan.Tag = item;
                    nd_bonSerahTahanan.BackColor = color;
                    nd_bonSerahTahanan.ForeColor = color2;
                    dokumentasiNode.Nodes.Add(nd_bonSerahTahanan);

                    RadTreeNode nd_bonTerimaTahanan = new RadTreeNode("Bon Terima Tahanan");
                    nd_bonTerimaTahanan.Name = "ListBonTerimaTahanan";
                    nd_bonTerimaTahanan.Image = Resources.form2;
                    nd_bonTerimaTahanan.Tag = item;
                    nd_bonTerimaTahanan.BackColor = color;
                    nd_bonTerimaTahanan.ForeColor = color2;
                    dokumentasiNode.Nodes.Add(nd_bonTerimaTahanan);
                }
			}

            if (role.ReportModul)
            {
                RadTreeNode reportNode = new RadTreeNode("LAPORAN/REPORT");
                reportNode.ForeColor = Color.White;
                //kunjunganNode.TreeViewElement.CustomFontStyle = FontStyle.Bold;
                reportNode.BackColor = color;
                reportNode.Name = "Report";
                radTreeView1.Nodes.Add(reportNode);

                RadTreeNode nd_reportsumm = new RadTreeNode("Rangkuman Laporan");
                nd_reportsumm.Name = "RepSummary";
                nd_reportsumm.Image = Resources.detail_data;
                nd_reportsumm.Tag = item;
                nd_reportsumm.BackColor = color;
                nd_reportsumm.ForeColor = color2;
                reportNode.Nodes.Add(nd_reportsumm);

                RadTreeNode nd_reportsumm_detail1 = new RadTreeNode("Sel Yang Ditempati");
                nd_reportsumm_detail1.Name = "CellOccupancy";
                nd_reportsumm_detail1.Image = Resources.file_text;
                nd_reportsumm_detail1.Tag = item;
                nd_reportsumm_detail1.BackColor = color;
                nd_reportsumm_detail1.ForeColor = color2;
                nd_reportsumm.Nodes.Add(nd_reportsumm_detail1);


                RadTreeNode nd_reportdet = new RadTreeNode("Detail");
                nd_reportdet.Name = "RepDetail";
                nd_reportdet.Image = Resources.detail_data;
                nd_reportdet.Tag = item;
                nd_reportdet.BackColor = color;
                nd_reportdet.ForeColor = color2;
                reportNode.Nodes.Add(nd_reportdet);

                RadTreeNode nd_reportinmates = new RadTreeNode("Tahanan");
                nd_reportinmates.Name = "RepInmate";
                nd_reportinmates.Image = Resources.file_text;
                nd_reportinmates.Tag = item;
                nd_reportinmates.BackColor = color;
                nd_reportinmates.ForeColor = color2;
                nd_reportdet.Nodes.Add(nd_reportinmates);

                RadTreeNode nd_reportcases = new RadTreeNode("Kasus");
                nd_reportcases.Name = "RepCases";
                nd_reportcases.Image = Resources.file_text;
                nd_reportcases.Tag = item;
                nd_reportcases.BackColor = color;
                nd_reportcases.ForeColor = color2;
                nd_reportdet.Nodes.Add(nd_reportcases);

                RadTreeNode nd_repStatustahanan = new RadTreeNode("Status Tahanan");
                nd_repStatustahanan.Name = "RepStatus";
                nd_repStatustahanan.Image = Resources.file_text;
                nd_repStatustahanan.Tag = item;
                nd_repStatustahanan.BackColor = color;
                nd_repStatustahanan.ForeColor = color2;
                nd_reportdet.Nodes.Add(nd_repStatustahanan);


                //RadTreeNode nd_reportblacklist = new RadTreeNode("Blacklist");
                //nd_reportblacklist.Name = "RepBlacklist";
                //nd_reportblacklist.Image = Resources.detail_data;
                //nd_reportblacklist.Tag = item;
                //nd_reportblacklist.BackColor = color;
                //nd_reportblacklist.ForeColor = color2;
                //reportNode.Nodes.Add(nd_reportblacklist);

                //RadTreeNode nd_reportcharts = new RadTreeNode("Charts");
                //nd_reportcharts.Name = "RepCharts";
                //nd_reportcharts.Image = Resources.detail_data;
                //nd_reportcharts.Tag = item;
                //nd_reportcharts.BackColor = color;
                //nd_reportcharts.ForeColor = color2;
                //reportNode.Nodes.Add(nd_reportcharts);

                //RadTreeNode nd_reportexception = new RadTreeNode("Exception");
                //nd_reportexception.Name = "RepException";
                //nd_reportexception.Image = Resources.detail_data;
                //nd_reportexception.Tag = item;
                //nd_reportexception.BackColor = color;
                //nd_reportexception.ForeColor = color2;
                //reportNode.Nodes.Add(nd_reportexception);
            }

            if (role.PrintFormModul)
            {
                RadTreeNode printformNode = new RadTreeNode("CETAK FORM");
                printformNode.ForeColor = Color.White;
                //audittrailNode.TreeViewElement.CustomFontStyle = FontStyle.Bold;
                printformNode.BackColor = color;
                printformNode.Name = "Print_Form";
                radTreeView1.Nodes.Add(printformNode);

                RadTreeNode nd_inmatesform = new RadTreeNode("Tahanan");
                nd_inmatesform.Name = "InmatesFormPrint";
                nd_inmatesform.Image = Resources.form3;
                nd_inmatesform.Tag = item;
                nd_inmatesform.BackColor = color;
                nd_inmatesform.ForeColor = color2;
                printformNode.Nodes.Add(nd_inmatesform);

                RadTreeNode nd_visitorform = new RadTreeNode("Pengunjung");
                nd_visitorform.Name = "VisitorFormPrint";
                nd_visitorform.Image = Resources.form3;
                nd_visitorform.Tag = item;
                nd_visitorform.BackColor = color;
                nd_visitorform.ForeColor = color2;
                printformNode.Nodes.Add(nd_visitorform);
            }

            if (role.AuditTrailModul)
            {
                RadTreeNode audittrailNode = new RadTreeNode("AUDIT TRAIL");
                audittrailNode.ForeColor = Color.White;
                //audittrailNode.TreeViewElement.CustomFontStyle = FontStyle.Bold;
                audittrailNode.BackColor = color;
                audittrailNode.Name = "Audit_Trail";
                radTreeView1.Nodes.Add(audittrailNode);

                RadTreeNode nd_audittrail = new RadTreeNode("Audit Trail");
                nd_audittrail.Name = "AuditTrail";
                nd_audittrail.Image = Resources.audit_trail;
                nd_audittrail.Tag = item;
                nd_audittrail.BackColor = color;
                nd_audittrail.ForeColor = color2;
                audittrailNode.Nodes.Add(nd_audittrail);

                RadTreeNode nd_rawdata = new RadTreeNode("Data Mentah");
                nd_rawdata.Name = "RawData";
                nd_rawdata.Image = Resources.raw;
                nd_rawdata.Tag = item;
                nd_rawdata.BackColor = color;
                nd_rawdata.ForeColor = color2;
                audittrailNode.Nodes.Add(nd_rawdata);
            }

            RadTreeNode userguideNode = new RadTreeNode("PANDUAN PENGGUNA");
            userguideNode.ForeColor = Color.White;
            //userguideNode.TreeViewElement.CustomFontStyle = FontStyle.Bold;
            userguideNode.BackColor = color;
            userguideNode.Name = "User_Guide";
            radTreeView1.Nodes.Add(userguideNode);

            RadTreeNode nd_panduan = new RadTreeNode("Petunjuk Penggunaan");
            nd_panduan.Name = "UserGuide";
            nd_panduan.Image = Resources.form;
            nd_panduan.Tag = item;
            nd_panduan.BackColor = color;
            nd_panduan.ForeColor = color2;
            userguideNode.Nodes.Add(nd_panduan);

            radTreeView1.ExpandAll();
            radTreeView1.RootElement.BackColor = color;
            radTreeView1.Nodes[0].Selected = true;

        }

        private void InitEvents()
        {
            this.topControl1.ViewLabel.TextChanged += ViewLabel_TextChanged;
            this.overviewView.VisualItemCreating += overviewView_VisualItemCreating;
            this.topControl1.LogOut.Click += LogOut_Click;
            this.topControl1.ChangePsw.Click += ChangePsw_Click;
            this.radCollapsiblePanel1.Collapsed += RadCollapsiblePanel1_Collapsed;
            this.radCollapsiblePanel1.Expanded += RadCollapsiblePanel1_Expanded;
            this.FormClosing += Main_FormClosing;
            this.Activated += form_Activated;
            this.radTreeView1.SelectedNodeChanged += RadTreeView1_SelectedNodeChanged;
            this.radTreeView1.Validating += radTreeView1_Validating;
            this.radTreeView1.Click += radTreeView_Click;
            this.dashboard.FullScreen.Click += dashboard_FullScreen_Click;
            this.AddDocView.MouseClick += AddDocView_Click;
            this.mainContainer.Pages[0].Item.Click += mainContainerPages0_Click;
            this.mainContainer.Pages[1].Item.Click += mainContainerPages1_Click;
        }

        private void mainContainerPages0_Click(object sender, EventArgs e)
        {
            //foreach (var item in mainContainer.Pages[1].Controls)
            //{
            //   UserFunction.ClearControls(((UserControl)item).Controls.Find("tableLayoutPanel1",true)[0]);
            //}
        }

        private void mainContainerPages1_Click(object sender, EventArgs e)
        {
            Console.Write("test");
        }

        private void AddDocView_Click(object sender, MouseEventArgs e)
        {
            if(AddDocView.Tag.ToString() == "Visitor")
            {
                
            }
        }

        private void dashboard_FullScreen_Click(object sender, EventArgs e)
        {
            DashboardFull dashboardfull = new DashboardFull();
            dashboardfull.ShowDialog();
        }

        private void ChangePsw_Click(object sender, EventArgs e)
        {
            changepsw.Show();
        }

        private void radTreeView1_Validating(object sender, CancelEventArgs e)
        {
            radTreeView1.SelectedNode.BackColor = Color.Black;
            radTreeView1.SelectedNode.ForeColor = Color.White;
        }

        private void RadTreeView1_SelectedNodeChanged(object sender, Telerik.WinControls.UI.RadTreeViewEventArgs e)
        {
            this.WaitingBar.AssociatedControl = null;
            mainContainer.SelectedPage = this.OverviewPage;

            if (e.Node == null)
                return;
            overviewView.Items.Clear();
            switch (e.Node.Name)
            {
                case "Home":
                    AddDocView.Tag = string.Empty;
                    AddDocView.Text = string.Empty;
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Collapsed;
                    listCellAllocation.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listCell.Hide();
                    listkendaraan.Hide();
                    listkendaraankeluar.Hide();
                    listaudittrail.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visit.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    cell.Hide();
                    kendaraan.Hide();
                    cellAllocation.Hide();
                    dashboard.Show();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
					seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
					listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();
                    break;
                case "ListPengguna":
                    AddDocView.Tag = string.Empty;
                    AddDocView.Text = Language.GetLanguageString(LanguangeId.ADD_NEW_USER);
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Visible;
                    listCellAllocation.Hide();
                    listUser.Show();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listCell.Hide();
                    listkendaraan.Hide();
                    listkendaraankeluar.Hide();
                    listaudittrail.Hide();
                    user.Show();
                    tahanan.Hide();
                    visit.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
					seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
					rptCases.Hide();
                    previewDoc.Hide();
					listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();

                    //clear data
                    user.Tag = null;
                    user.LoadData();
                    break;
                case "ListTahanan":
                    AddDocView.Tag = "Inmate";
                    AddDocView.Text = Language.GetLanguageString(LanguangeId.ADD_NEW_INMATES);
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Visible;
                    listCellAllocation.Hide();
                    listTahanan.Show();
                    daftarTahanan.Hide();
                    listUser.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraankeluar.Hide();
                    listkendaraan.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    tahanan.Show();
                    user.Hide();
                    visit.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
					seizedAssets.Hide();
					seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
					listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();

                    //clear data
                    tahanan.Tag = null;
                    tahanan.LoadData();
                    break;

                case "ListStatusTahanan":
                    AddDocView.Tag = "Inmate";
                    AddDocView.Text = Language.GetLanguageString(LanguangeId.ADD_NEW_STATUS_TAHANAN);
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Visible;
                    listCellAllocation.Hide();
                    listTahanan.Hide();
                    liststatustahanan.Show();
                    daftarTahanan.Hide();
                    listUser.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraankeluar.Hide();
                    listkendaraan.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    statusTahanan.Show();
                    tahanan.Hide();
                    user.Hide();
                    visit.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
                    seizedAssets.Hide();
                    seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Show();

                    //clear data
                    tahanan.Tag = null;
                    tahanan.LoadData();
                    break;
                case "DaftarTahanan":
                    AddDocView.Tag = "InmateIsCheckedOut";
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Hidden;
                    listCellAllocation.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Show();
                    listUser.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    listKunjungan.Hide();
                    listkendaraankeluar.Hide();
                    listkendaraan.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    tahanan.Hide();
                    user.Hide();
                    visit.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
                    seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();

                    //clear data
                    tahanan.Tag = null;
                    tahanan.LoadData();
                    break;
                case "ListPegunjung":
                    AddDocView.Tag = "Visitor";
                    AddDocView.Text = Language.GetLanguageString(LanguangeId.ADD_NEW_VISITOR);
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Visible;
                    listCellAllocation.Hide();
                    listVisitor.Show();
                    listVisitorVIP.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraankeluar.Hide();
                    listCell.Hide();
                    listkendaraan.Hide();
                    listaudittrail.Hide();
                    visitor.Show();
                    visitorVIP.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visit.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
					seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
					listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();

                    //clear data
                    visitor.Tag = null;
                    visitor.LoadData();
                    break;
                case "ListPegunjungVIP":
                    AddDocView.Tag = "Visitor VIP";
                    AddDocView.Text = Language.GetLanguageString(LanguangeId.ADD_NEW_VISITOR_VIP);
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Visible;
                    listCellAllocation.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Show();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraankeluar.Hide();
                    listCell.Hide();
                    listkendaraan.Hide();
                    listaudittrail.Hide();
                    visitor.Hide();
                    visitorVIP.Show();
                    user.Hide();
                    tahanan.Hide();
                    visit.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
                    seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();

                    //clear data
                    visitorVIP.Tag = null;
                    visitorVIP.LoadData();
                    break;
                case "ListKunjungan":
                    AddDocView.Tag = string.Empty;
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Hidden;
                    listCellAllocation.Hide();
                    listKunjungan.Show();
                    listKunjunganVIP.Hide();
                    listkendaraan.Hide();
                    listkendaraankeluar.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    visit.Show();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
					seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
					listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();
                    break;
                case "ListKunjunganVIP":
                    AddDocView.Tag = string.Empty;
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Hidden;
                    listCellAllocation.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Show();
                    listkendaraan.Hide();
                    listkendaraankeluar.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
                    seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();
                    break;
                case "ListCell":
                    AddDocView.Tag = string.Empty;
                    AddDocView.Text = Language.GetLanguageString(LanguangeId.ADD_NEW_CELL);
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Visible;
                    listCellAllocation.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraan.Hide();
                    listkendaraankeluar.Hide();
                    listCell.Show();
                    listaudittrail.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Show();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
					seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
					listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    statusTahanan.Hide();

                    //clear data
                    cell.Tag = null;
                    cell.LoadData();
                    break;
                case "ListKendaraan":
                    AddDocView.Tag = string.Empty;
                    AddDocView.Text = Language.GetLanguageString(LanguangeId.ADD_NEW_KENDARAAN);
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Visible;
                    listCellAllocation.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listCell.Hide();
                    listkendaraan.Show();
                    listkendaraankeluar.Hide();
                    listaudittrail.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    cell.Hide();
                    kendaraan.Show();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
                    seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();

                    //clear data
                    kendaraan.Tag = null;
                    kendaraan.LoadData();
                    break;

                case "ListKendaraanKeluar":
                    AddDocView.Tag = string.Empty;
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Hidden;
                    listCellAllocation.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listCell.Hide();
                    listkendaraan.Hide();
                    listkendaraankeluar.Show();
                    listaudittrail.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    cell.Hide();
                    kendaraan.Show();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
                    seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();

                    break;
                case "ListCellAllocation":
                    AddDocView.Tag = string.Empty;
                    AddDocView.Text = Language.GetLanguageString(LanguangeId.ADD_NEW_ALLOCATION);
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Visible;
                    listCellAllocation.Show();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraan.Hide();
                    listkendaraankeluar.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Show();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
					seizedAssets.Hide();	
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();

                    //clear data
                    cellAllocation.Tag = null;
                    cellAllocation.LoadData();
                    break;
                case "AuditTrail":
                    AddDocView.Tag = string.Empty;
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Hidden;
                    listCellAllocation.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraan.Hide();
                    listkendaraankeluar.Hide();
                    listCell.Hide();
                    listaudittrail.Show();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Show();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
					seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
					listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();
                    break;
                case "CellOccupancy":
                    AddDocView.Tag = string.Empty;
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Hidden;
                    listCellAllocation.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    listkendaraan.Hide();
                    listkendaraankeluar.Hide();

                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Show();
                    listSeizedAssets.Hide();
					seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
					listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();
                    break;
                case "ListSeizedAssets":
                    AddDocView.Tag = string.Empty;
                    AddDocView.Text = Language.GetLanguageString(LanguangeId.ADD_NEW_ASSETS);
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Visible;
                    listCellAllocation.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraan.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    listkendaraankeluar.Hide();

                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Show();
					seizedAssets.Show();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
					previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();

                    //clear data
                    seizedAssets.Tag = null;
                    seizedAssets.LoadData();
                    break;
                case "RawData":
                    UserFunction.ExportRawFile();
                    break;
                case "VisitorFormPrint":
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Hidden;
                    listCellAllocation.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listUser.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraan.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    listkendaraankeluar.Hide();
                    user.Hide();
                    visit.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
					seizedAssets.Hide();
                    listPrintFormVisitor.Show();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
					rptCases.Hide();
                    previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();
                    break;
                case "InmatesFormPrint":
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Hidden;
                    listCellAllocation.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listUser.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraan.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    listkendaraankeluar.Hide();
                    user.Hide();
                    visit.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
					seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Show();
                    rptInmates.Hide();
                    rptCases.Hide();
					previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();
                    break;
                case "RepInmate":
                    AddDocView.Tag = string.Empty;
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Hidden;
                    listCellAllocation.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraan.Hide();
                    listkendaraankeluar.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    statusTahanan.Hide();

                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
					seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Show();
                    rptCases.Hide();
                    previewDoc.Hide();
					listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    break;

                case "RepStatus":
                    AddDocView.Tag = string.Empty;
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Hidden;
                    listCellAllocation.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraan.Hide();
                    listkendaraankeluar.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();
                    statusTahanan.Hide();

                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
                    seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Show();
                    statusTahanan.Hide();
                    break;
                case "RepCases":
                    AddDocView.Tag = string.Empty;
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Hidden;
                    listCellAllocation.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraan.Hide();
                    listkendaraankeluar.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();

                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
					seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Show();
					previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    JadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();
                    statusTahanan.Hide();
                    break;
                case "ListBonTahanan":
                    AddDocView.Tag = string.Empty;
                    AddDocView.Text = Language.GetLanguageString(LanguangeId.ADD_NEW_BON_TAHANAN);
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Visible;
                    listCellAllocation.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraankeluar.Hide();
                    listkendaraan.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();

                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
                    seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
					previewDoc.Hide();
                    listBonTahanan.Show();
                    BonTahanan.Show();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    rptStatusTahanan.Hide();

                    //clear data
                    bonTahanan.Tag = null;
                    bonTahanan.LoadData();
                    liststatustahanan.Hide();
                    statusTahanan.Hide();
                    break;
                case "ListBonSerahTahanan":
                    AddDocView.Tag = string.Empty;
                    AddDocView.Text = Language.GetLanguageString(LanguangeId.ADD_NEW_BON_SERAH_TAHANAN);
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Visible;
                    listCellAllocation.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraan.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    listkendaraankeluar.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();

                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
                    seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Show();
                    listBonSerahTahanan.Show();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    JadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();

                    //clear data
                    bonTahanan.Tag = null;
                    bonTahanan.LoadData();
                    statusTahanan.Hide();
                    break;
                case "ListBonTerimaTahanan":
                    AddDocView.Tag = string.Empty;
                    AddDocView.Text = Language.GetLanguageString(LanguangeId.ADD_NEW_BON_TERIMA_TAHANAN);
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Visible;
                    listCellAllocation.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraan.Hide();
                    listkendaraankeluar.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();

                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
                    seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Show();
                    listBonTerimaTahanan.Show();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();

                    //clear data
                    bonTahanan.Tag = null;
                    bonTahanan.LoadData();
                    statusTahanan.Hide();
                    break;
                case "ListJadwalKunjungan":
                    AddDocView.Tag = string.Empty;
                    AddDocView.Text = Language.GetLanguageString(LanguangeId.ADD_NEW_JADWAL_KUNJUNGAN);
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Visible;
                    listCellAllocation.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraan.Hide();
                    listkendaraankeluar.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();

                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
                    seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Show();
                    jadwalKunjungan.Show();
                    pdfViewer.Hide();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();

                    //clear data
                    bonTahanan.Tag = null;
                    bonTahanan.LoadData();
                    statusTahanan.Hide();
                    break;

                case "UserGuide":
                    this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Hidden;
                    listCellAllocation.Hide();
                    listKunjungan.Hide();
                    listKunjunganVIP.Hide();
                    listkendaraan.Hide();
                    listkendaraankeluar.Hide();
                    listCell.Hide();
                    listaudittrail.Hide();
                    listUser.Hide();
                    listTahanan.Hide();
                    daftarTahanan.Hide();
                    listVisitor.Hide();
                    listVisitorVIP.Hide();

                    visit.Hide();
                    user.Hide();
                    tahanan.Hide();
                    visitor.Hide();
                    visitorVIP.Hide();
                    kendaraan.Hide();
                    cell.Hide();
                    dashboard.Hide();
                    cellAllocation.Hide();
                    rptCellOccupancy.Hide();
                    listSeizedAssets.Hide();
                    seizedAssets.Hide();
                    listPrintFormVisitor.Hide();
                    listPrintFormTahanan.Hide();
                    rptInmates.Hide();
                    rptCases.Hide();
                    previewDoc.Hide();
                    listBonTahanan.Hide();
                    BonTahanan.Hide();
                    BonSerahTahanan.Hide();
                    listBonSerahTahanan.Hide();
                    bonTerimaTahanan.Hide();
                    listBonTerimaTahanan.Hide();
                    listJadwalKunjungan.Hide();
                    jadwalKunjungan.Hide();
                    pdfViewer.Show();
                    liststatustahanan.Hide();
                    rptStatusTahanan.Hide();

                    pdfViewer.Tag = e.Node.Name;
                    pdfViewer.LoadData();
                    statusTahanan.Hide();
                    break;
            }
        }

        private void form_Activated(object sender, EventArgs e)
        {
            this.Activated -= form_Activated;
        }

        private void InitForm()
        {
            Opacity frmopacity = new Opacity();
            frmopacity.Show();


            AddDocView.Hide();
            MainForm.formMain = this;
            ThemeResolutionService.ApplicationThemeName = "Material";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.radCollapsiblePanel1.CollapsiblePanelElement.HeaderElement.ShowHeaderLine = false;
            this.mainContainer.SelectedPage = this.OverviewPage;
            this.mainContainer.Pages[1].Item.Visibility = ElementVisibility.Collapsed;
            this.radTreeView1.TreeViewElement.CustomFont = Global.MainFont;
            this.radTreeView1.TreeViewElement.CustomFontSize = 11.5f;
            this.radTreeView1.TreeViewElement.BackColor = Color.Transparent;
            
            this.synchDetail.Visible = false;
            this.pesanDetail.Visible = false;
            this.radBreadCrumb1.DefaultTreeView = radTreeView1;
            //this.Icon = Resources.Icon;
            this.Text = Application.ProductName;


            this.topControl1.ViewLabel.Font = new Font("Verdana", 20, FontStyle.Regular);
            this.topControl1.ViewLabel.Text = "";
            this.radCollapsiblePanel1.EnableAnimation = false;

            this.topControl1.ViewLabel.Text = String.Format("{0} {1}", Language.GetLanguageString(LanguangeId.WELCOME), Convert.ToString(Properties.Settings.Default.UserID));
            this.topLeft1.UserName.Text = String.Format("{0}", Convert.ToString(Properties.Settings.Default.UserID));
            this.topLeft1.LastLogin.Text = String.Format("{0}: {1}", Language.GetLanguageString(LanguangeId.LAST_LOGIN), Convert.ToString(Properties.Settings.Default.LastLogin));
            this.titleBarControl1.AppTitle.Text = string.Format("     {0} :: v{1}", Application.ProductName, Application.ProductVersion);


            TextPrimitive primitive = (TextPrimitive)lblTitle.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 15, FontStyle.Bold);
            primitive.ForeColor = Color.Yellow;
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(-1, 2), Color.Black);
            lblTitle.Text = Application.ProductName.ToUpper();
            lblVersion.Text = string.Format("{0} {1}",Language.GetLanguageString(LanguangeId.VERSION), Application.ProductVersion);
            lblVersion.ForeColor = Color.White;

            RadPageViewStripElement stripElement = this.mainContainer.ViewElement as RadPageViewStripElement;
            stripElement.ItemContainer.ButtonsPanel.Visibility = ElementVisibility.Hidden;

            //Overview
            this.overviewView.ShowGroups = true;
            this.overviewView.EnableGrouping = true;
            this.overviewView.ViewType = ListViewType.IconsView;
            this.overviewView.ItemSize = new Size(250, 120);
            this.overviewView.ItemSpacing = 20;
            this.overviewView.AllowEdit = false;
            this.overviewView.EnableFiltering = true;
            this.overviewView.HotTracking = true;

            this.overviewView.RootElement.BackColor = Color.Transparent;
            this.overviewView.BackColor = Color.Transparent;
            this.overviewView.ListViewElement.DrawFill = false;
            this.overviewView.ListViewElement.ViewElement.BackColor = Color.Transparent;
            this.overviewView.ListViewElement.Padding = new Padding(20, 20, 20, 20);
            this.overviewView.ListViewElement.BackColor = Color.Transparent;
            this.overviewView.ListViewElement.EnableElementShadow = false;
            this.overviewView.RootElement.EnableElementShadow = false;
            this.overviewView.GroupItemSize = new Size(0, 45);
            this.overviewView.BackgroundImage = Resources.Background;

            RadPageViewStripElement backStageElement = mainContainer.ViewElement as RadPageViewStripElement;
            backStageElement.ContentArea.BackColor = Color.FromArgb(77, 77, 77);
            

            StripViewItemLayout itemLayout = backStageElement.ItemContainer.ItemLayout as StripViewItemLayout;
            itemLayout.BackColor = Color.FromArgb(45, 45, 48);

            StripViewButtonsPanel buttonsPanel = backStageElement.ItemContainer.ButtonsPanel as StripViewButtonsPanel;
            buttonsPanel.Visibility = ElementVisibility.Hidden;
            buttonsPanel.DrawFill = false;
            buttonsPanel.BackColor = Color.FromArgb(45, 45, 48);


            //Center Of Screen
            tahananDetail.Location =
            new Point(ClientSize.Width / 2 - tahananDetail.Size.Width / 2,
              ClientSize.Height / 2 - tahananDetail.Size.Height / 2);

            cameraSource.Location =
            new Point(ClientSize.Width / 2 - cameraSource.Size.Width / 2,
              ClientSize.Height / 2 - cameraSource.Size.Height / 2);

            statusTahananDetail.Location =
            new Point(ClientSize.Width / 2 - statusTahananDetail.Size.Width / 2,
              ClientSize.Height / 2 - statusTahananDetail.Size.Height / 2);

            perpanjangantahanan.Location =
            new Point(ClientSize.Width / 2 - perpanjangantahanan.Size.Width / 2,
              ClientSize.Height / 2 - perpanjangantahanan.Size.Height / 2);

            tahananDetailCheckedOut.Location =
            new Point(ClientSize.Width / 2 - tahananDetailCheckedOut.Size.Width / 2,
              ClientSize.Height / 2 - tahananDetailCheckedOut.Size.Height / 2);

            visitorDetail.Location =
            new Point(ClientSize.Width / 2 - visitorDetail.Size.Width / 2,
              ClientSize.Height / 2 - visitorDetail.Size.Height / 2);

            visitorVIPDetail.Location =
            new Point(ClientSize.Width / 2 - visitorVIPDetail.Size.Width / 2,
              ClientSize.Height / 2 - visitorVIPDetail.Size.Height / 2);

            KunjunganDetail.Location =
            new Point(ClientSize.Width / 2 - KunjunganDetail.Size.Width / 2,
              ClientSize.Height / 2 - KunjunganDetail.Size.Height / 2);

            searchData.Location =
            new Point(ClientSize.Width / 2 - searchData.Size.Width / 2,
              ClientSize.Height / 2 - searchData.Size.Height / 2);

            settings.Location =
            new Point(ClientSize.Width / 2 - settings.Size.Width / 2,
              ClientSize.Height / 2 - settings.Size.Height / 2);

            userDetail.Location =
            new Point(ClientSize.Width / 2 - userDetail.Size.Width / 2,
              ClientSize.Height / 2 - userDetail.Size.Height / 2);

            cellDetail.Location =
            new Point(ClientSize.Width / 2 - cellDetail.Size.Width / 2,
              ClientSize.Height / 2 - cellDetail.Size.Height / 2);

            cellAllocationDetail.Location =
            new Point(ClientSize.Width / 2 - cellAllocationDetail.Size.Width / 2,
              ClientSize.Height / 2 - cellAllocationDetail.Size.Height / 2);

            changepsw.Location =
            new Point(ClientSize.Width / 2 - changepsw.Size.Width / 2,
              ClientSize.Height / 2 - changepsw.Size.Height / 2);

            fingerPrint_Inmate.Location =
            new Point(ClientSize.Width / 2 - fingerPrint_Inmate.Size.Width / 2,
              ClientSize.Height / 2 - fingerPrint_Inmate.Size.Height / 2);

            captureFingerPrint1.Location =
            new Point(ClientSize.Width / 2 - captureFingerPrint1.Size.Width / 2,
              ClientSize.Height / 2 - captureFingerPrint1.Size.Height / 2);

            checkedOut.Location =
            new Point(ClientSize.Width / 2 - checkedOut.Size.Width / 2,
              ClientSize.Height / 2 - checkedOut.Size.Height / 2);

            checkedOutinmates.Location =
            new Point(ClientSize.Width / 2 - checkedOutinmates.Size.Width / 2,
              ClientSize.Height / 2 - checkedOutinmates.Size.Height / 2);

            seizedAssetsDetail.Location =
            new Point(ClientSize.Width / 2 - seizedAssetsDetail.Size.Width / 2,
              ClientSize.Height / 2 - seizedAssetsDetail.Size.Height / 2);

            capturePhoto.Location =
            new Point(ClientSize.Width / 2 - capturePhoto.Size.Width / 2,
              ClientSize.Height / 2 - capturePhoto.Size.Height / 2);

			bonTahananDetail.Location =
            new Point(ClientSize.Width / 2 - bonTahananDetail.Size.Width / 2,
              ClientSize.Height / 2 - bonTahananDetail.Size.Height / 2);

            kendaraanTahananDetail.Location =
            new Point(ClientSize.Width / 2 - kendaraanTahananDetail.Size.Width / 2,
              ClientSize.Height / 2 - kendaraanTahananDetail.Size.Height / 2);

            //bonSerahTahananDetail.Location =
            //new Point(ClientSize.Width / 2 - bonSerahTahananDetail.Size.Width / 2,
            //  ClientSize.Height / 2 - bonSerahTahananDetail.Size.Height / 2);

            //bonTerimaTahananDetail.Location =
            //new Point(ClientSize.Width / 2 - bonTerimaTahananDetail.Size.Width / 2,
            //  ClientSize.Height / 2 - bonTerimaTahananDetail.Size.Height / 2);

            keperluanBonTahanan.Location =
            new Point(ClientSize.Width / 2 - keperluanBonTahanan.Size.Width / 2,
              ClientSize.Height / 2 - keperluanBonTahanan.Size.Height / 2);

            JadwalKunjunganDetail.Location =
            new Point(ClientSize.Width / 2 - JadwalKunjunganDetail.Size.Width / 2,
              ClientSize.Height / 2 - JadwalKunjunganDetail.Size.Height / 2);

            RadMenuItem menuHapus = new RadMenuItem("Hapus");
            menuHapus.Padding = new Padding(0);
            menuHapus.Margin = new Padding(0);
            menuHapus.CustomFont = Global.MainFont;
            menuHapus.CustomFontSize = 10.5f;
            menuHapus.Image = Resources.delete;

            RadMenuItem menuEdit = new RadMenuItem("Ubah/Edit");
            menuEdit.Padding = new Padding(0);
            menuEdit.Margin = new Padding(0);
            menuEdit.CustomFont = Global.MainFont;
            menuEdit.CustomFontSize = 10.5f;
            menuEdit.Image = Resources.edit;

            menu.Items.Add(menuEdit);
            menu.Items.Add(menuHapus);
        }


        private void radTreeView_Click(object sender, System.EventArgs e)
        {
            CheckTreeViewNodes(radTreeView1.Nodes);
        }

        private void CheckTreeViewNodes(IEnumerable<RadTreeNode> treeNodeCollection)
        {
            RadTreeNode nodeselected = radTreeView1.SelectedNode;
            foreach (var node in treeNodeCollection)
            {

                if (nodeselected.Name == node.Name)
                {
                    node.BackColor = Color.Black;
                    node.ForeColor = Color.Gold;
                }
                else
                {
                    node.BackColor = Color.FromArgb(77, 77, 77);
                    node.ForeColor = Color.FromArgb(175, 202, 203);
                }
                CheckTreeViewNodes(node.Nodes);
            }
        }



        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            FrmLogin frmlogin = GlobalForm.formlogin;
            frmlogin.Show();
            ThemeResolutionService.ApplicationThemeName = string.Empty;
            frmlogin.Refresh();
            Application.Exit();
        }


        private void overviewView_VisualItemCreating(object sender, ListViewVisualItemCreatingEventArgs e)
        {
            if (e.VisualItem is IconListViewVisualItem)
            {
                e.VisualItem = new OverviewListViewVisualItem();
            }
        }

        private void LogOut_Click(object sender, EventArgs e)
        {
            LoginService login = new LoginService();
            login.UpdateIsLoggedIn(GlobalVariables.UserID, false);

            this.Hide();
            FrmLogin frmlogin = GlobalForm.formlogin;
            frmlogin.Show();
            ThemeResolutionService.ApplicationThemeName = null;
            frmlogin.Refresh();
        }

        private void ViewLabel_TextChanged(object sender, EventArgs e)
        {
            radCollapsiblePanel1.HeaderText = topControl1.ViewLabel.Text;
        }

        private void RadCollapsiblePanel1_Expanded(object sender, EventArgs e)
        {
            tableLayoutPanel2.ColumnStyles[0].Width = 300;
            topLeft1.Logo.Visible = true;
        }

        private void RadCollapsiblePanel1_Collapsed(object sender, EventArgs e)
        {
            tableLayoutPanel2.ColumnStyles[0].Width = 33;
            topLeft1.Logo.Visible = false;
        }

        public RadPageView PageView
        {
            get
            {
                return this.mainContainer;
            }
        }
    }

    public class WinApi
    {
        [System.Runtime.InteropServices.DllImport("user32.dll", EntryPoint = "GetSystemMetrics")]
        public static extern int GetSystemMetrics(int which);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern void
            SetWindowPos(IntPtr hwnd, IntPtr hwndInsertAfter,
                         int X, int Y, int width, int height, uint flags);

        public static void SetWinFullScreen(IntPtr hwnd)
        {
            SetWindowPos(hwnd, IntPtr.Zero, 0, 0, GetSystemMetrics(0), GetSystemMetrics(1), 64);
        }
    }
}
