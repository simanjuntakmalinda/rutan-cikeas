﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.Export;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class Base : UserControl
    {
        public Base()
        {
            InitializeComponent();
        }

        public void tableLayoutPanel_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            if (e.Row % 2 == 0)
            {
                using (SolidBrush brush = new SolidBrush(Global.MainColorLight))
                    e.Graphics.FillRectangle(brush, e.CellBounds);
            }
            else
            {
                using (SolidBrush brush = new SolidBrush(Color.WhiteSmoke))
                    e.Graphics.FillRectangle(brush, e.CellBounds);
            }
               
        }

        public void radGridView_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e)
        {
            e.Cancel = true;
        }

        public void radGridView_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            if (e.CellElement is GridHeaderCellElement)
            {
                if (e.CellElement.ColumnIndex == 0)
                    e.CellElement.TextAlignment = ContentAlignment.MiddleRight;
                e.CellElement.DrawBorder = false;
                e.CellElement.BorderColor = Color.Black;
                e.CellElement.CustomFont = Global.MainFontMedium;
                e.CellElement.CustomFontSize = Global.CustomFontSizeMain;
                e.CellElement.BackColor = Color.WhiteSmoke;
                e.CellElement.TextAlignment = ContentAlignment.MiddleLeft;

                e.CellElement.ForeColor = Color.FromArgb(45,45,48);
                e.CellElement.BackColor = Color.WhiteSmoke;
                e.CellElement.GradientStyle = GradientStyles.Solid;
                e.CellElement.DrawBorder = false;
                e.CellElement.DrawFill = true;

            }

        }

        public void radGridView_RowFormatting(object sender, RowFormattingEventArgs e)
        {
            if (e.RowElement.RowInfo.IsOdd)
            {
                e.RowElement.DrawFill = true;
                e.RowElement.BackColor = Color.WhiteSmoke;
            }
            else
                e.RowElement.DrawFill = false;            
        }
    }
}
