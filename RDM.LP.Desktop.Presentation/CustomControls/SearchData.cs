﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class SearchData : Base
    {
        public string NamaForm;
        private Point MouseDownLocation;
        public RadGridView GvTahanan { get { return this.gvData; } }
        public SearchData()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvData.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvData.RowFormatting += radGridView_RowFormatting;
            gvData.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvData.CellClick += gvData_CellClick;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            this.VisibleChanged += Form_VisibleChanged;
            exit.Click += exit_Click;
            btnSearch.Click += btnSearch_Click;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            switch ((SearchType)Enum.Parse(typeof(SearchType), ddlkategori.Text, true))
            {
                case SearchType.REG_NO:
                    LoadDataToGrid(SearchType.REG_NO);
                    break;
                case SearchType.NAME:
                    LoadDataToGrid(SearchType.NAME);
                    break;
                case SearchType.ID:
                    LoadDataToGrid(SearchType.ID);
                    break;
            }
        }

        private void gvData_CellClick(object sender, GridViewCellEventArgs e)
        {

            if (e.Column == null)
                return;
            if (e.Column.Index == 0)
            {
                if (NamaForm == "FormBonTahanan")
                {
                    //1
                    var regid = MainForm.formMain.BonTahanan.DaftarTahanan.Text;
                    if (regid == string.Empty)
                    {
                        MainForm.formMain.BonTahanan.DaftarTahanan.Text = e.Row.Cells["RegID"].Value.ToString();
                        this.NamaForm = "";
                        this.Hide();
                    }
                    else
                    {
                        var str = MainForm.formMain.BonTahanan.DaftarTahanan.Text;
                        char[] separator = { ';' };
                        string[] strarr = null;
                        strarr = str.Split(separator);
                        var i = 0;

                        foreach (var item in strarr)
                        {
                            if (e.Row.Cells["RegID"].Value.ToString() == item)
                                i++;
                        }
                        if (i > 0)
                        {
                            UserFunction.MsgBox(TipeMsg.Info, "Tahanan sudah pernah ditambahkan!");
                        }
                        else
                        {
                            MainForm.formMain.BonTahanan.DaftarTahanan.Text = regid + ';' + e.Row.Cells["RegID"].Value.ToString();
                            this.NamaForm = "";
                            this.Hide();
                        }

                    }

                    //2
                    //ListViewItem lv = new ListViewItem();
                    //lv.Text = e.Row.Cells["FullName"].Value.ToString();
                    //lv.Tag = e.Row.Cells["RegID"].Value.ToString();

                    //MainForm.formMain.BonTahanan.NamaTahanan.Items.Add(lv.Tag);
                }
                else if (NamaForm == "FormJadwalKunjungan" || NamaForm == "FormJadwalKunjunganEdit")
                {
                    MainForm.formMain.JadwalKunjungan.TahananId.Text = e.Row.Cells["RegID"].Value.ToString();
                    MainForm.formMain.JadwalKunjungan.NamaTahanan.Text = e.Row.Cells["FullName"].Value.ToString();
                    MainForm.formMain.JadwalKunjungan.NoIdentitas.Text = e.Row.Cells["IDNo"].Value.ToString();
                    MainForm.formMain.JadwalKunjungan.NoSelTahanan.Text = e.Row.Cells["CellCode"].Value.ToString();
                    MainForm.formMain.JadwalKunjungan.Tersangka_Changed();
                    this.NamaForm = "";
                    this.Hide();
                }
                else if(NamaForm == "FormStatusTahanan")
                {
                    MainForm.formMain.StatusTahanan.TxtNama.Text = e.Row.Cells["FullName"].Value.ToString();
                    MainForm.formMain.StatusTahanan.TbRegId.Text = e.Row.Cells["RegID"].Value.ToString();
                    MainForm.formMain.StatusTahanan.NoSpKap.Text = e.Row.Cells["NoSuratPenangkapan"].Value.ToString();
                    if(MainForm.formMain.StatusTahanan.NoSpKap.Text != string.Empty)
                    {
                        MainForm.formMain.StatusTahanan.NoSpKap.ReadOnly = true;
                    }
                    else
                    {
                        MainForm.formMain.StatusTahanan.NoSpKap.ReadOnly = false;
                    }
                    MainForm.formMain.StatusTahanan.NoSpHan.Text = e.Row.Cells["NoSuratPenahanan"].Value.ToString();
                    if (MainForm.formMain.StatusTahanan.NoSpHan.Text != string.Empty)
                    {
                        MainForm.formMain.StatusTahanan.NoSpHan.ReadOnly = true;
                    }
                    else
                    {
                        MainForm.formMain.StatusTahanan.NoSpHan.ReadOnly = false;
                    }
                    MainForm.formMain.StatusTahanan.TglKap.Value = (DateTime)e.Row.Cells["TanggalPenangkapan"].Value;
                    MainForm.formMain.StatusTahanan.TglHan.Value = (DateTime)e.Row.Cells["TanggalPenahanan"].Value;
                    MainForm.formMain.StatusTahanan.TbNoSel.Text = e.Row.Cells["CellNumber"].Value.ToString();
                    this.NamaForm = "";
                    this.Hide();
                }
                else
                {
                    if (e.Row.Cells["CellAllocationStatusId"].Value != null && e.Row.Cells["CellAllocationStatusId"].Value.ToString() == "2")
                    {
                        UserFunction.MsgBox(TipeMsg.Info, "Tahanan adalah tahanan isolasi dan tidak dapat dikunjungi!");
                    }
                    else if (e.Row.Cells["BolehDikunjungi"].Value != null && e.Row.Cells["BolehDikunjungi"].Value.ToString() == "0")
                    {
                        UserFunction.MsgBox(TipeMsg.Info, "Tahanan tidak dapat dikunjungi!");
                    }
                    else if (e.Row.Cells["PeriodeKunjungan"].Value != null && e.Row.Cells["PeriodeKunjungan"].Value.ToString() == "PER MINGGU")
                    {
                        string day = DateTime.Now.DayOfWeek.ToString();
                        if (day == "Monday")
                            day = "SENIN";
                        else if (day == "Tuesday")
                            day = "SELASA";
                        else if (day == "Wednesday")
                            day = "RABU";
                        else if (day == "Thursday")
                            day = "KAMIS";
                        else if (day == "Friday")
                            day = "JUMAT";
                        else if (day == "Saturday")
                            day = "SABTU";
                        else if (day == "Sunday")
                            day = "MINGGU";

                        int i = 0;

                        var waktuKunjungan = (e.Row.Cells["WaktuKunjungan"].Value.ToString()).Split(';');

                        foreach (var _data in waktuKunjungan)
                        {
                            if (day == _data)
                            {
                                i++;
                            }
                        }

                        if (i == 0)
                        {
                            UserFunction.MsgBox(TipeMsg.Info, "Tahanan tidak dapat dikunjungi pada hari ini!");
                        }
                        else
                        {
                            MainForm.formMain.Visit.TahananId.Text = e.Row.Cells["Id"].Value.ToString();
                            MainForm.formMain.Visit.NamaTahanan.Text = e.Row.Cells["FullName"].Value.ToString();
                            this.Hide();
                        }
                    }
                    else if (e.Row.Cells["PeriodeKunjungan"].Value != null && e.Row.Cells["PeriodeKunjungan"].Value.ToString() == "PER BULAN")
                    {
                        var date = DateTime.Now.Day.ToString();
                        int i = 0;

                        var waktuKunjungan = (e.Row.Cells["WaktuKunjungan"].Value.ToString()).Split(';');

                        foreach (var _data in waktuKunjungan)
                        {
                            if (date == _data)
                            {
                                i++;
                            }
                        }

                        if (i == 0)
                        {
                            UserFunction.MsgBox(TipeMsg.Info, "Tahanan tidak dapat dikunjungi pada hari ini!");
                        }
                        else
                        {
                            MainForm.formMain.Visit.TahananId.Text = e.Row.Cells["Id"].Value.ToString();
                            MainForm.formMain.Visit.NamaTahanan.Text = e.Row.Cells["FullName"].Value.ToString();
                            this.Hide();
                        }
                    }
                    else
                    {
                        MainForm.formMain.Visit.TahananId.Text = e.Row.Cells["Id"].Value.ToString();
                        MainForm.formMain.Visit.NamaTahanan.Text = e.Row.Cells["FullName"].Value.ToString();
                        this.Hide();
                    }
                }
            }
        }

        private void UpdateTahanan()
        {
            throw new NotImplementedException();
        }

        private void DeleteTahanan(string Id)
        {
            using (InmatesService tahanan = new InmatesService())
            {
                tahanan.DeleteById(Id);
            }
        }

        private void InitForm()
        {
            this.btnSearch.RootElement.EnableElementShadow = false;
            this.lblTitle.Text = "     CARI DATA";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            this.lblCari.LabelElement.CustomFont = Global.MainFont;
            this.lblCari.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            this.txtSearch.TextBoxElement.CustomFont = Global.MainFont;
            this.txtSearch.TextBoxElement.CustomFontSize = 11.5f;


            this.ddlkategori.DropDownListElement.CustomFont = Global.MainFont;
            this.ddlkategori.DropDownListElement.CustomFontSize = Global.CustomFontSizeMain;

            this.txtSearch.TextBoxElement.CustomFont = Global.MainFont;
            this.txtSearch.TextBoxElement.CustomFontSize = Global.CustomFontSizeMain;
            this.btnSearch.RootElement.EnableElementShadow = false;

            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Width = 15;
            btnDetail.Name = "btnDetail";
            gvData.AutoGenerateColumns = false;
            gvData.Columns.Insert(0, btnDetail);
            gvData.Refresh();
            gvData.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            GridViewImageColumn btnUbah = new GridViewImageColumn();
            btnUbah.HeaderText = "";
            btnUbah.Name = "btnUbah";
            gvData.AutoGenerateColumns = false;
            gvData.Columns.Insert(1, btnUbah);
            gvData.Refresh();
            gvData.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnHapus = new GridViewImageColumn();
            btnHapus.HeaderText = "";
            btnHapus.Name = "btnHapus";
            gvData.AutoGenerateColumns = false;
            gvData.Columns.Insert(2, btnHapus);
            gvData.Refresh();
            gvData.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            UserFunction.SetInitGridView(gvData);
            ddlkategori.Items.Add("REG_NO");
            ddlkategori.Items.Add("NAME");
            ddlkategori.Items.Add("ID");
            ddlkategori.SelectedIndex = 1;
        }

        private void LoadDataToGrid(SearchType tipe)
        {
            gvData.DataSource = null;

            gvData.AutoGenerateColumns = true;
            var search = txtSearch.Text.Replace("'", "`");
            if (search == string.Empty)
                return;

            InmatesService tahanan = new InmatesService();
            switch (tipe)
            {
                case SearchType.REG_NO:

                    if (NamaForm == "FormJadwalKunjungan")
                    {
                        gvData.DataSource = tahanan.GetByRegIdJadwal(search);
                    }
                    else if(NamaForm == "FormStatusTahanan")
                    {
                        gvData.DataSource = tahanan.GetInmatesByReg(search);
                    }
                    else
                    {
                        gvData.DataSource = tahanan.GetByRegIdVisit(search);
                    }
                    break;
                case SearchType.NAME:
                    if (NamaForm == "FormJadwalKunjungan")
                    {
                        gvData.DataSource = tahanan.GetByNameJadwal(search);
                    }
                    else if (NamaForm == "FormStatusTahanan")
                    {
                        gvData.DataSource = tahanan.GetInmatesByName(search);
                    }
                    else
                    {
                        gvData.DataSource = tahanan.GetByNameVisit(search);
                    }
                    break;
                case SearchType.ID:
                    if (NamaForm == "FormJadwalKunjungan")
                    {
                        gvData.DataSource = tahanan.GetByKTPJadwal(search);
                    }
                    else if (NamaForm == "FormStatusTahanan")
                    {
                        gvData.DataSource = tahanan.GetInmatesByKTP(search);
                    }
                    else
                    {
                        gvData.DataSource = tahanan.GetByKTPVisit(search);
                    }
                    break;
            }

            gvData.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            foreach (GridViewDataColumn col in gvData.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvData.Columns[0].Width = 20;
            //gvData.Columns[1].Width = 20;
            //gvData.Columns[2].Width = 20;

            gvData.Columns[0].IsVisible = true;
            gvData.Columns[0].ReadOnly = false;
            gvData.Columns[0].NullValue = Resources.add_to_data;

            gvData.Columns["CellNumber"].IsVisible = false;
            gvData.Columns["NoSuratPenahanan"].IsVisible = false;
            gvData.Columns["NoSuratPenangkapan"].IsVisible = false;
            gvData.Columns["TanggalPenahanan"].IsVisible = false;
            gvData.Columns["TanggalPenangkapan"].IsVisible = false;

            gvData.Columns["RegID"].IsVisible = true;
            gvData.Columns["RegID"].HeaderText = "NO REGISTER";
            gvData.Columns["RegID"].Width = 100;

            gvData.Columns["FullName"].IsVisible = true;
            gvData.Columns["FullName"].HeaderText = "NAMA LENGKAP";
            gvData.Columns["FullName"].Width = 100;

            gvData.Columns["Gender"].IsVisible = true;
            gvData.Columns["Gender"].HeaderText = "JENIS KELAMIN";
            gvData.Columns["Gender"].Width = 100;

            gvData.Columns["IDType"].IsVisible = true;
            gvData.Columns["IDType"].HeaderText = "JENIS IDENTITAS";
            gvData.Columns["IDType"].Width = 100;

            gvData.Columns["IDNo"].IsVisible = true;
            gvData.Columns["IDNo"].HeaderText = "NO IDENTITAS";
            gvData.Columns["IDNo"].Width = 100;

        }


        private void exit_Click(object sender, EventArgs e)
        {
            this.NamaForm = "";
            this.Hide();
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (!this.Visible)
            {
                txtSearch.Text = string.Empty;
                gvData.DataSource = null;
                gvData.Refresh();
            }
        }

    }
}
