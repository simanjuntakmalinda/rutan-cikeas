﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class PreviewDoc
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PreviewDoc));
            this.pdfRenderer = new PdfiumViewer.PdfRenderer();
            this.lblDetailName = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.print = new System.Windows.Forms.Panel();
            this.zoomin = new System.Windows.Forms.Panel();
            this.zoomout = new System.Windows.Forms.Panel();
            this.copytext = new System.Windows.Forms.Panel();
            this.pdf = new System.Windows.Forms.Panel();
            this.word = new System.Windows.Forms.Panel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.back = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailName)).BeginInit();
            this.lblDetailName.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.SuspendLayout();
            // 
            // pdfRenderer
            // 
            this.pdfRenderer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pdfRenderer.Cursor = System.Windows.Forms.Cursors.Default;
            this.pdfRenderer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pdfRenderer.Location = new System.Drawing.Point(0, 106);
            this.pdfRenderer.Margin = new System.Windows.Forms.Padding(0);
            this.pdfRenderer.Name = "pdfRenderer";
            this.pdfRenderer.Page = 0;
            this.pdfRenderer.Rotation = PdfiumViewer.PdfRotation.Rotate0;
            this.pdfRenderer.Size = new System.Drawing.Size(1349, 690);
            this.pdfRenderer.TabIndex = 8;
            this.pdfRenderer.Text = "pdfRenderer1";
            this.pdfRenderer.ZoomMode = PdfiumViewer.PdfViewerZoomMode.FitHeight;
            // 
            // lblDetailName
            // 
            this.lblDetailName.AutoSize = false;
            this.lblDetailName.BackColor = System.Drawing.Color.Gray;
            this.lblDetailName.Controls.Add(this.back);
            this.lblDetailName.Controls.Add(this.tableLayoutPanel3);
            this.lblDetailName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailName.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.lblDetailName.Location = new System.Drawing.Point(0, 56);
            this.lblDetailName.Margin = new System.Windows.Forms.Padding(0);
            this.lblDetailName.Name = "lblDetailName";
            this.lblDetailName.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            // 
            // 
            // 
            this.lblDetailName.RootElement.CustomFontSize = 12F;
            this.lblDetailName.Size = new System.Drawing.Size(1349, 50);
            this.lblDetailName.TabIndex = 7;
            ((Telerik.WinControls.UI.RadLabelElement)(this.lblDetailName.GetChildAt(0))).ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            ((Telerik.WinControls.UI.RadLabelElement)(this.lblDetailName.GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadLabelElement)(this.lblDetailName.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.lblDetailName.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.lblDetailName.GetChildAt(0).GetChildAt(0))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblDetailName.GetChildAt(0).GetChildAt(1))).FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblDetailName.GetChildAt(0).GetChildAt(1))).Width = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblDetailName.GetChildAt(0).GetChildAt(1))).LeftWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblDetailName.GetChildAt(0).GetChildAt(1))).TopWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblDetailName.GetChildAt(0).GetChildAt(1))).RightWidth = 0F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblDetailName.GetChildAt(0).GetChildAt(1))).BottomWidth = 2F;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblDetailName.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(18)))), ((int)(((byte)(18)))));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblDetailName.GetChildAt(0).GetChildAt(1))).ForeColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblDetailName.GetChildAt(0).GetChildAt(1))).ForeColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblDetailName.GetChildAt(0).GetChildAt(1))).ForeColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblDetailName.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblDetailName.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.lblDetailName.GetChildAt(0).GetChildAt(1))).Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel3.ColumnCount = 6;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.print, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.zoomin, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.zoomout, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.copytext, 5, 0);
            this.tableLayoutPanel3.Controls.Add(this.pdf, 4, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(1014, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(330, 50);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // print
            // 
            this.print.BackColor = System.Drawing.Color.Transparent;
            this.print.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.print;
            this.print.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.print.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.print.Cursor = System.Windows.Forms.Cursors.Hand;
            this.print.Location = new System.Drawing.Point(153, 3);
            this.print.Name = "print";
            this.print.Size = new System.Drawing.Size(54, 44);
            this.print.TabIndex = 0;
            // 
            // zoomin
            // 
            this.zoomin.BackColor = System.Drawing.Color.Transparent;
            this.zoomin.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.zoom_in1;
            this.zoomin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.zoomin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.zoomin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.zoomin.Location = new System.Drawing.Point(3, 3);
            this.zoomin.Name = "zoomin";
            this.zoomin.Size = new System.Drawing.Size(54, 44);
            this.zoomin.TabIndex = 3;
            // 
            // zoomout
            // 
            this.zoomout.BackColor = System.Drawing.Color.Transparent;
            this.zoomout.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.zoom_out1;
            this.zoomout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.zoomout.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.zoomout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.zoomout.Location = new System.Drawing.Point(63, 3);
            this.zoomout.Name = "zoomout";
            this.zoomout.Size = new System.Drawing.Size(54, 44);
            this.zoomout.TabIndex = 4;
            // 
            // copytext
            // 
            this.copytext.BackColor = System.Drawing.Color.Transparent;
            this.copytext.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.copy_paste1;
            this.copytext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.copytext.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.copytext.Cursor = System.Windows.Forms.Cursors.Hand;
            this.copytext.Location = new System.Drawing.Point(273, 3);
            this.copytext.Name = "copytext";
            this.copytext.Size = new System.Drawing.Size(54, 44);
            this.copytext.TabIndex = 5;
            // 
            // pdf
            // 
            this.pdf.BackColor = System.Drawing.Color.Transparent;
            this.pdf.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.pdf;
            this.pdf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pdf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pdf.Location = new System.Drawing.Point(213, 3);
            this.pdf.Name = "pdf";
            this.pdf.Size = new System.Drawing.Size(54, 44);
            this.pdf.TabIndex = 2;
            // 
            // word
            // 
            this.word.BackColor = System.Drawing.Color.Transparent;
            this.word.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.word;
            this.word.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.word.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.word.Cursor = System.Windows.Forms.Cursors.Hand;
            this.word.Location = new System.Drawing.Point(1196, 234);
            this.word.Name = "word";
            this.word.Size = new System.Drawing.Size(54, 44);
            this.word.TabIndex = 1;
            this.word.Visible = false;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(1349, 56);
            this.lblTitle.TabIndex = 1;
            // 
            // back
            // 
            this.back.BackColor = System.Drawing.Color.Transparent;
            this.back.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.back_round1;
            this.back.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.back.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back.Location = new System.Drawing.Point(5, 3);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(93, 44);
            this.back.TabIndex = 5;
            // 
            // PreviewDoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pdfRenderer);
            this.Controls.Add(this.lblDetailName);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.word);
            this.Name = "PreviewDoc";
            this.Size = new System.Drawing.Size(1349, 796);
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailName)).EndInit();
            this.lblDetailName.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lblTitle;
        private PdfiumViewer.PdfRenderer pdfRenderer;
        private Telerik.WinControls.UI.RadLabel lblDetailName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel print;
        private System.Windows.Forms.Panel zoomin;
        private System.Windows.Forms.Panel zoomout;
        private System.Windows.Forms.Panel copytext;
        private System.Windows.Forms.Panel pdf;
        private System.Windows.Forms.Panel word;
        private System.Windows.Forms.Panel back;
    }
}
