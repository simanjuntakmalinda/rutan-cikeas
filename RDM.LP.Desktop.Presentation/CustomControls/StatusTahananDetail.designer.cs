﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class StatusTahananDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatusTahananDetail));
            this.universitas = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblDataCell = new Telerik.WinControls.UI.RadLabel();
            this.lblNoSkap = new Telerik.WinControls.UI.RadLabel();
            this.lblCellNumber = new Telerik.WinControls.UI.RadLabel();
            this.lblSpHan = new Telerik.WinControls.UI.RadLabel();
            this.lblCellType = new Telerik.WinControls.UI.RadLabel();
            this.lblTglUpdate = new Telerik.WinControls.UI.RadLabel();
            this.lblUpdater = new Telerik.WinControls.UI.RadLabel();
            this.lblTglInput = new Telerik.WinControls.UI.RadLabel();
            this.lblInputter = new Telerik.WinControls.UI.RadLabel();
            this.updatedate = new Telerik.WinControls.UI.RadLabel();
            this.updater = new Telerik.WinControls.UI.RadLabel();
            this.inputdate = new Telerik.WinControls.UI.RadLabel();
            this.inputter = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailData = new Telerik.WinControls.UI.RadLabel();
            this.lblCellStatus = new Telerik.WinControls.UI.RadLabel();
            this.lblTglSphan = new Telerik.WinControls.UI.RadLabel();
            this.lblCellDescription = new Telerik.WinControls.UI.RadLabel();
            this.lblTglKap = new Telerik.WinControls.UI.RadLabel();
            this.lblBuildingName = new Telerik.WinControls.UI.RadLabel();
            this.lblNoreg = new Telerik.WinControls.UI.RadLabel();
            this.lblCellFloor = new Telerik.WinControls.UI.RadLabel();
            this.lblNosel = new Telerik.WinControls.UI.RadLabel();
            this.lblCellCode = new Telerik.WinControls.UI.RadLabel();
            this.lblNama = new Telerik.WinControls.UI.RadLabel();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.tablePerpanjangan3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFileP1 = new Telerik.WinControls.UI.RadLabel();
            this.lblDurasiP1 = new Telerik.WinControls.UI.RadLabel();
            this.lblTglSuratP1 = new Telerik.WinControls.UI.RadLabel();
            this.lblNoSuratP1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFileP2 = new Telerik.WinControls.UI.RadLabel();
            this.lblDurasiP2 = new Telerik.WinControls.UI.RadLabel();
            this.lblTglSuratP2 = new Telerik.WinControls.UI.RadLabel();
            this.lblNosuratP2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radPageViewPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFileP3 = new Telerik.WinControls.UI.RadLabel();
            this.lblDurasiP3 = new Telerik.WinControls.UI.RadLabel();
            this.lblTglSuratP3 = new Telerik.WinControls.UI.RadLabel();
            this.lblNoSuratP3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnClose = new Telerik.WinControls.UI.RadButton();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataCell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoSkap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSpHan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUpdater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInputter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglSphan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglKap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuildingName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoreg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellFloor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNosel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            this.tablePerpanjangan3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblFileP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDurasiP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglSuratP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoSuratP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            this.radPageViewPage2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblFileP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDurasiP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglSuratP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNosuratP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            this.radPageViewPage3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblFileP3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDurasiP3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglSuratP3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoSuratP3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            this.SuspendLayout();
            // 
            // universitas
            // 
            this.universitas.AutoSize = false;
            this.universitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.universitas.Location = new System.Drawing.Point(504, 414);
            this.universitas.Margin = new System.Windows.Forms.Padding(4);
            this.universitas.Name = "universitas";
            this.universitas.Size = new System.Drawing.Size(728, 42);
            this.universitas.TabIndex = 53;
            this.universitas.Text = "Tahun Lulus";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 50);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(913, 564);
            this.radScrollablePanel1.Size = new System.Drawing.Size(932, 566);
            this.radScrollablePanel1.TabIndex = 15;
            this.radScrollablePanel1.VerticalScrollBarState = Telerik.WinControls.UI.ScrollState.AlwaysShow;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lblDataCell, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblNoSkap, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblCellNumber, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblSpHan, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblCellType, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblTglUpdate, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.lblUpdater, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.lblTglInput, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.lblInputter, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.updatedate, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.updater, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.inputdate, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.inputter, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailData, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.lblCellStatus, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblTglSphan, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblCellDescription, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblTglKap, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblBuildingName, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblNoreg, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblCellFloor, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblNosel, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblCellCode, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblNama, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPageView1, 0, 8);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 16;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 226F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(913, 931);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // lblDataCell
            // 
            this.lblDataCell.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDataCell, 2);
            this.lblDataCell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDataCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDataCell.Image = ((System.Drawing.Image)(resources.GetObject("lblDataCell.Image")));
            this.lblDataCell.Location = new System.Drawing.Point(14, 14);
            this.lblDataCell.Margin = new System.Windows.Forms.Padding(4);
            this.lblDataCell.Name = "lblDataCell";
            this.lblDataCell.Size = new System.Drawing.Size(392, 42);
            this.lblDataCell.TabIndex = 86;
            this.lblDataCell.Text = "      DETAIL TAHANAN";
            // 
            // lblNoSkap
            // 
            this.lblNoSkap.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblNoSkap, 2);
            this.lblNoSkap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoSkap.Location = new System.Drawing.Point(364, 214);
            this.lblNoSkap.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoSkap.Name = "lblNoSkap";
            this.lblNoSkap.Size = new System.Drawing.Size(535, 42);
            this.lblNoSkap.TabIndex = 60;
            // 
            // lblCellNumber
            // 
            this.lblCellNumber.AutoSize = false;
            this.lblCellNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCellNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCellNumber.Location = new System.Drawing.Point(14, 214);
            this.lblCellNumber.Margin = new System.Windows.Forms.Padding(4);
            this.lblCellNumber.Name = "lblCellNumber";
            this.lblCellNumber.Size = new System.Drawing.Size(342, 42);
            this.lblCellNumber.TabIndex = 11;
            this.lblCellNumber.Text = "NO SURAT PENANGKAPAN";
            // 
            // lblSpHan
            // 
            this.lblSpHan.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblSpHan, 2);
            this.lblSpHan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSpHan.Location = new System.Drawing.Point(364, 314);
            this.lblSpHan.Margin = new System.Windows.Forms.Padding(4);
            this.lblSpHan.Name = "lblSpHan";
            this.lblSpHan.Size = new System.Drawing.Size(535, 42);
            this.lblSpHan.TabIndex = 85;
            // 
            // lblCellType
            // 
            this.lblCellType.AutoSize = false;
            this.lblCellType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCellType.Location = new System.Drawing.Point(14, 314);
            this.lblCellType.Margin = new System.Windows.Forms.Padding(4);
            this.lblCellType.Name = "lblCellType";
            this.lblCellType.Size = new System.Drawing.Size(342, 42);
            this.lblCellType.TabIndex = 83;
            this.lblCellType.Text = "NO SURAT PENAHANAN";
            // 
            // lblTglUpdate
            // 
            this.lblTglUpdate.AutoSize = false;
            this.lblTglUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTglUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTglUpdate.Location = new System.Drawing.Point(14, 881);
            this.lblTglUpdate.Margin = new System.Windows.Forms.Padding(4);
            this.lblTglUpdate.Name = "lblTglUpdate";
            this.lblTglUpdate.Size = new System.Drawing.Size(342, 39);
            this.lblTglUpdate.TabIndex = 74;
            this.lblTglUpdate.Text = "DIUBAH TANGGAL";
            // 
            // lblUpdater
            // 
            this.lblUpdater.AutoSize = false;
            this.lblUpdater.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUpdater.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblUpdater.Location = new System.Drawing.Point(14, 839);
            this.lblUpdater.Margin = new System.Windows.Forms.Padding(4);
            this.lblUpdater.Name = "lblUpdater";
            this.lblUpdater.Size = new System.Drawing.Size(342, 34);
            this.lblUpdater.TabIndex = 73;
            this.lblUpdater.Text = "DIUBAH OLEH";
            // 
            // lblTglInput
            // 
            this.lblTglInput.AutoSize = false;
            this.lblTglInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTglInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTglInput.Location = new System.Drawing.Point(14, 787);
            this.lblTglInput.Margin = new System.Windows.Forms.Padding(4);
            this.lblTglInput.Name = "lblTglInput";
            this.lblTglInput.Size = new System.Drawing.Size(342, 44);
            this.lblTglInput.TabIndex = 72;
            this.lblTglInput.Text = "DIBUAT TANGGAL";
            // 
            // lblInputter
            // 
            this.lblInputter.AutoSize = false;
            this.lblInputter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInputter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblInputter.Location = new System.Drawing.Point(14, 736);
            this.lblInputter.Margin = new System.Windows.Forms.Padding(4);
            this.lblInputter.Name = "lblInputter";
            this.lblInputter.Size = new System.Drawing.Size(342, 43);
            this.lblInputter.TabIndex = 68;
            this.lblInputter.Text = "DIBUAT OLEH";
            // 
            // updatedate
            // 
            this.updatedate.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.updatedate, 2);
            this.updatedate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updatedate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updatedate.Location = new System.Drawing.Point(364, 881);
            this.updatedate.Margin = new System.Windows.Forms.Padding(4);
            this.updatedate.Name = "updatedate";
            this.updatedate.Size = new System.Drawing.Size(535, 39);
            this.updatedate.TabIndex = 79;
            // 
            // updater
            // 
            this.updater.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.updater, 2);
            this.updater.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updater.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updater.Location = new System.Drawing.Point(364, 839);
            this.updater.Margin = new System.Windows.Forms.Padding(4);
            this.updater.Name = "updater";
            this.updater.Size = new System.Drawing.Size(535, 34);
            this.updater.TabIndex = 80;
            // 
            // inputdate
            // 
            this.inputdate.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.inputdate, 2);
            this.inputdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputdate.Location = new System.Drawing.Point(364, 787);
            this.inputdate.Margin = new System.Windows.Forms.Padding(4);
            this.inputdate.Name = "inputdate";
            this.inputdate.Size = new System.Drawing.Size(535, 44);
            this.inputdate.TabIndex = 78;
            // 
            // inputter
            // 
            this.inputter.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.inputter, 2);
            this.inputter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputter.Location = new System.Drawing.Point(364, 736);
            this.inputter.Margin = new System.Windows.Forms.Padding(4);
            this.inputter.Name = "inputter";
            this.inputter.Size = new System.Drawing.Size(535, 43);
            this.inputter.TabIndex = 77;
            // 
            // lblDetailData
            // 
            this.lblDetailData.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailData, 2);
            this.lblDetailData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDetailData.Image = ((System.Drawing.Image)(resources.GetObject("lblDetailData.Image")));
            this.lblDetailData.Location = new System.Drawing.Point(14, 686);
            this.lblDetailData.Margin = new System.Windows.Forms.Padding(4);
            this.lblDetailData.Name = "lblDetailData";
            this.lblDetailData.Size = new System.Drawing.Size(392, 42);
            this.lblDetailData.TabIndex = 71;
            this.lblDetailData.Text = "        DETAIL DATA";
            // 
            // lblCellStatus
            // 
            this.lblCellStatus.AutoSize = false;
            this.lblCellStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCellStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCellStatus.Location = new System.Drawing.Point(14, 364);
            this.lblCellStatus.Margin = new System.Windows.Forms.Padding(4);
            this.lblCellStatus.Name = "lblCellStatus";
            this.lblCellStatus.Size = new System.Drawing.Size(342, 42);
            this.lblCellStatus.TabIndex = 82;
            this.lblCellStatus.Text = "TANGGAL PENAHANAN";
            // 
            // lblTglSphan
            // 
            this.lblTglSphan.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblTglSphan, 2);
            this.lblTglSphan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTglSphan.Location = new System.Drawing.Point(364, 364);
            this.lblTglSphan.Margin = new System.Windows.Forms.Padding(4);
            this.lblTglSphan.Name = "lblTglSphan";
            this.lblTglSphan.Size = new System.Drawing.Size(535, 42);
            this.lblTglSphan.TabIndex = 84;
            // 
            // lblCellDescription
            // 
            this.lblCellDescription.AutoSize = false;
            this.lblCellDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCellDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCellDescription.Location = new System.Drawing.Point(14, 264);
            this.lblCellDescription.Margin = new System.Windows.Forms.Padding(4);
            this.lblCellDescription.Name = "lblCellDescription";
            this.lblCellDescription.Size = new System.Drawing.Size(342, 42);
            this.lblCellDescription.TabIndex = 81;
            this.lblCellDescription.Text = "TANGGAL PENANGKAPAN";
            // 
            // lblTglKap
            // 
            this.lblTglKap.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblTglKap, 2);
            this.lblTglKap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTglKap.Location = new System.Drawing.Point(364, 264);
            this.lblTglKap.Margin = new System.Windows.Forms.Padding(4);
            this.lblTglKap.Name = "lblTglKap";
            this.lblTglKap.Size = new System.Drawing.Size(535, 42);
            this.lblTglKap.TabIndex = 83;
            // 
            // lblBuildingName
            // 
            this.lblBuildingName.AutoSize = false;
            this.lblBuildingName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblBuildingName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblBuildingName.Location = new System.Drawing.Point(14, 114);
            this.lblBuildingName.Margin = new System.Windows.Forms.Padding(4);
            this.lblBuildingName.Name = "lblBuildingName";
            this.lblBuildingName.Size = new System.Drawing.Size(342, 42);
            this.lblBuildingName.TabIndex = 19;
            this.lblBuildingName.Text = "NO REGISTRASI TAHANAN";
            // 
            // lblNoreg
            // 
            this.lblNoreg.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblNoreg, 2);
            this.lblNoreg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoreg.Location = new System.Drawing.Point(364, 114);
            this.lblNoreg.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoreg.Name = "lblNoreg";
            this.lblNoreg.Size = new System.Drawing.Size(535, 42);
            this.lblNoreg.TabIndex = 58;
            // 
            // lblCellFloor
            // 
            this.lblCellFloor.AutoSize = false;
            this.lblCellFloor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCellFloor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCellFloor.Location = new System.Drawing.Point(14, 164);
            this.lblCellFloor.Margin = new System.Windows.Forms.Padding(4);
            this.lblCellFloor.Name = "lblCellFloor";
            this.lblCellFloor.Size = new System.Drawing.Size(342, 42);
            this.lblCellFloor.TabIndex = 18;
            this.lblCellFloor.Text = "NO SEL TAHANAN";
            // 
            // lblNosel
            // 
            this.lblNosel.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblNosel, 2);
            this.lblNosel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNosel.Location = new System.Drawing.Point(364, 164);
            this.lblNosel.Margin = new System.Windows.Forms.Padding(4);
            this.lblNosel.Name = "lblNosel";
            this.lblNosel.Size = new System.Drawing.Size(535, 42);
            this.lblNosel.TabIndex = 59;
            // 
            // lblCellCode
            // 
            this.lblCellCode.AutoSize = false;
            this.lblCellCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCellCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCellCode.Location = new System.Drawing.Point(14, 64);
            this.lblCellCode.Margin = new System.Windows.Forms.Padding(4);
            this.lblCellCode.Name = "lblCellCode";
            this.lblCellCode.Size = new System.Drawing.Size(342, 42);
            this.lblCellCode.TabIndex = 10;
            this.lblCellCode.Text = "NAMA";
            // 
            // lblNama
            // 
            this.lblNama.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblNama, 2);
            this.lblNama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNama.Location = new System.Drawing.Point(364, 64);
            this.lblNama.Margin = new System.Windows.Forms.Padding(4);
            this.lblNama.Name = "lblNama";
            this.lblNama.Size = new System.Drawing.Size(535, 42);
            this.lblNama.TabIndex = 57;
            // 
            // radPageView1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPageView1, 3);
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.Controls.Add(this.radPageViewPage3);
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(13, 413);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage1;
            this.radPageView1.Size = new System.Drawing.Size(887, 220);
            this.radPageView1.TabIndex = 87;
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.tablePerpanjangan3);
            this.radPageViewPage1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(110F, 28F);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(866, 172);
            this.radPageViewPage1.Text = "Perpanjangan Ke 1";
            // 
            // tablePerpanjangan3
            // 
            this.tablePerpanjangan3.ColumnCount = 4;
            this.tablePerpanjangan3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tablePerpanjangan3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tablePerpanjangan3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 173F));
            this.tablePerpanjangan3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 273F));
            this.tablePerpanjangan3.Controls.Add(this.lblFileP1, 3, 1);
            this.tablePerpanjangan3.Controls.Add(this.lblDurasiP1, 2, 1);
            this.tablePerpanjangan3.Controls.Add(this.lblTglSuratP1, 1, 1);
            this.tablePerpanjangan3.Controls.Add(this.lblNoSuratP1, 0, 1);
            this.tablePerpanjangan3.Controls.Add(this.radLabel16, 3, 0);
            this.tablePerpanjangan3.Controls.Add(this.radLabel11, 2, 0);
            this.tablePerpanjangan3.Controls.Add(this.radLabel1, 0, 0);
            this.tablePerpanjangan3.Controls.Add(this.radLabel2, 1, 0);
            this.tablePerpanjangan3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePerpanjangan3.Location = new System.Drawing.Point(0, 0);
            this.tablePerpanjangan3.Name = "tablePerpanjangan3";
            this.tablePerpanjangan3.RowCount = 2;
            this.tablePerpanjangan3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tablePerpanjangan3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tablePerpanjangan3.Size = new System.Drawing.Size(866, 172);
            this.tablePerpanjangan3.TabIndex = 3;
            this.tablePerpanjangan3.Tag = "3";
            // 
            // lblFileP1
            // 
            this.lblFileP1.AutoSize = false;
            this.lblFileP1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFileP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblFileP1.Location = new System.Drawing.Point(597, 54);
            this.lblFileP1.Margin = new System.Windows.Forms.Padding(4);
            this.lblFileP1.Name = "lblFileP1";
            this.lblFileP1.Size = new System.Drawing.Size(265, 114);
            this.lblFileP1.TabIndex = 87;
            // 
            // lblDurasiP1
            // 
            this.lblDurasiP1.AutoSize = false;
            this.lblDurasiP1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDurasiP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDurasiP1.Location = new System.Drawing.Point(424, 54);
            this.lblDurasiP1.Margin = new System.Windows.Forms.Padding(4);
            this.lblDurasiP1.Name = "lblDurasiP1";
            this.lblDurasiP1.Size = new System.Drawing.Size(165, 114);
            this.lblDurasiP1.TabIndex = 86;
            // 
            // lblTglSuratP1
            // 
            this.lblTglSuratP1.AutoSize = false;
            this.lblTglSuratP1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTglSuratP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTglSuratP1.Location = new System.Drawing.Point(254, 54);
            this.lblTglSuratP1.Margin = new System.Windows.Forms.Padding(4);
            this.lblTglSuratP1.Name = "lblTglSuratP1";
            this.lblTglSuratP1.Size = new System.Drawing.Size(162, 114);
            this.lblTglSuratP1.TabIndex = 85;
            // 
            // lblNoSuratP1
            // 
            this.lblNoSuratP1.AutoSize = false;
            this.lblNoSuratP1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoSuratP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNoSuratP1.Location = new System.Drawing.Point(4, 54);
            this.lblNoSuratP1.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoSuratP1.Name = "lblNoSuratP1";
            this.lblNoSuratP1.Size = new System.Drawing.Size(242, 114);
            this.lblNoSuratP1.TabIndex = 84;
            // 
            // radLabel16
            // 
            this.radLabel16.AutoSize = false;
            this.radLabel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel16.Location = new System.Drawing.Point(596, 3);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(267, 44);
            this.radLabel16.TabIndex = 9;
            this.radLabel16.Text = "FILE";
            // 
            // radLabel11
            // 
            this.radLabel11.AutoSize = false;
            this.radLabel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel11.Location = new System.Drawing.Point(423, 3);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(167, 44);
            this.radLabel11.TabIndex = 3;
            this.radLabel11.Text = "DURASI (HARI)";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.Location = new System.Drawing.Point(3, 3);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(244, 44);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "NO SURAT";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel2.Location = new System.Drawing.Point(253, 3);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(164, 44);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "TANGGAL SURAT";
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.Controls.Add(this.tableLayoutPanel2);
            this.radPageViewPage2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(110F, 28F);
            this.radPageViewPage2.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(866, 172);
            this.radPageViewPage2.Text = "Perpanjangan Ke 2";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 176F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 270F));
            this.tableLayoutPanel2.Controls.Add(this.lblFileP2, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblDurasiP2, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblTglSuratP2, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblNosuratP2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.radLabel7, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabel8, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabel9, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabel10, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(866, 172);
            this.tableLayoutPanel2.TabIndex = 4;
            this.tableLayoutPanel2.Tag = "3";
            // 
            // lblFileP2
            // 
            this.lblFileP2.AutoSize = false;
            this.lblFileP2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFileP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblFileP2.Location = new System.Drawing.Point(600, 54);
            this.lblFileP2.Margin = new System.Windows.Forms.Padding(4);
            this.lblFileP2.Name = "lblFileP2";
            this.lblFileP2.Size = new System.Drawing.Size(262, 114);
            this.lblFileP2.TabIndex = 87;
            // 
            // lblDurasiP2
            // 
            this.lblDurasiP2.AutoSize = false;
            this.lblDurasiP2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDurasiP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDurasiP2.Location = new System.Drawing.Point(424, 54);
            this.lblDurasiP2.Margin = new System.Windows.Forms.Padding(4);
            this.lblDurasiP2.Name = "lblDurasiP2";
            this.lblDurasiP2.Size = new System.Drawing.Size(168, 114);
            this.lblDurasiP2.TabIndex = 86;
            // 
            // lblTglSuratP2
            // 
            this.lblTglSuratP2.AutoSize = false;
            this.lblTglSuratP2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTglSuratP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTglSuratP2.Location = new System.Drawing.Point(254, 54);
            this.lblTglSuratP2.Margin = new System.Windows.Forms.Padding(4);
            this.lblTglSuratP2.Name = "lblTglSuratP2";
            this.lblTglSuratP2.Size = new System.Drawing.Size(162, 114);
            this.lblTglSuratP2.TabIndex = 85;
            // 
            // lblNosuratP2
            // 
            this.lblNosuratP2.AutoSize = false;
            this.lblNosuratP2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNosuratP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNosuratP2.Location = new System.Drawing.Point(4, 54);
            this.lblNosuratP2.Margin = new System.Windows.Forms.Padding(4);
            this.lblNosuratP2.Name = "lblNosuratP2";
            this.lblNosuratP2.Size = new System.Drawing.Size(242, 114);
            this.lblNosuratP2.TabIndex = 84;
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel7.Location = new System.Drawing.Point(599, 3);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(264, 44);
            this.radLabel7.TabIndex = 9;
            this.radLabel7.Text = "FILE";
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel8.Location = new System.Drawing.Point(423, 3);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(170, 44);
            this.radLabel8.TabIndex = 3;
            this.radLabel8.Text = "DURASI (HARI)";
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel9.Location = new System.Drawing.Point(3, 3);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(244, 44);
            this.radLabel9.TabIndex = 1;
            this.radLabel9.Text = "NO SURAT";
            // 
            // radLabel10
            // 
            this.radLabel10.AutoSize = false;
            this.radLabel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel10.Location = new System.Drawing.Point(253, 3);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(164, 44);
            this.radLabel10.TabIndex = 2;
            this.radLabel10.Text = "TANGGAL SURAT";
            // 
            // radPageViewPage3
            // 
            this.radPageViewPage3.Controls.Add(this.tableLayoutPanel3);
            this.radPageViewPage3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPageViewPage3.ItemSize = new System.Drawing.SizeF(110F, 28F);
            this.radPageViewPage3.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage3.Name = "radPageViewPage3";
            this.radPageViewPage3.Size = new System.Drawing.Size(866, 172);
            this.radPageViewPage3.Text = "Perpanjangan Ke 3";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 219F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 227F));
            this.tableLayoutPanel3.Controls.Add(this.lblFileP3, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblDurasiP3, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblTglSuratP3, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblNoSuratP3, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.radLabel12, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.radLabel13, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.radLabel14, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.radLabel15, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(866, 172);
            this.tableLayoutPanel3.TabIndex = 4;
            this.tableLayoutPanel3.Tag = "3";
            // 
            // lblFileP3
            // 
            this.lblFileP3.AutoSize = false;
            this.lblFileP3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFileP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblFileP3.Location = new System.Drawing.Point(643, 54);
            this.lblFileP3.Margin = new System.Windows.Forms.Padding(4);
            this.lblFileP3.Name = "lblFileP3";
            this.lblFileP3.Size = new System.Drawing.Size(219, 114);
            this.lblFileP3.TabIndex = 87;
            // 
            // lblDurasiP3
            // 
            this.lblDurasiP3.AutoSize = false;
            this.lblDurasiP3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDurasiP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDurasiP3.Location = new System.Drawing.Point(424, 54);
            this.lblDurasiP3.Margin = new System.Windows.Forms.Padding(4);
            this.lblDurasiP3.Name = "lblDurasiP3";
            this.lblDurasiP3.Size = new System.Drawing.Size(211, 114);
            this.lblDurasiP3.TabIndex = 86;
            // 
            // lblTglSuratP3
            // 
            this.lblTglSuratP3.AutoSize = false;
            this.lblTglSuratP3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTglSuratP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTglSuratP3.Location = new System.Drawing.Point(254, 54);
            this.lblTglSuratP3.Margin = new System.Windows.Forms.Padding(4);
            this.lblTglSuratP3.Name = "lblTglSuratP3";
            this.lblTglSuratP3.Size = new System.Drawing.Size(162, 114);
            this.lblTglSuratP3.TabIndex = 85;
            // 
            // lblNoSuratP3
            // 
            this.lblNoSuratP3.AutoSize = false;
            this.lblNoSuratP3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoSuratP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNoSuratP3.Location = new System.Drawing.Point(4, 54);
            this.lblNoSuratP3.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoSuratP3.Name = "lblNoSuratP3";
            this.lblNoSuratP3.Size = new System.Drawing.Size(242, 114);
            this.lblNoSuratP3.TabIndex = 84;
            // 
            // radLabel12
            // 
            this.radLabel12.AutoSize = false;
            this.radLabel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel12.Location = new System.Drawing.Point(642, 3);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(221, 44);
            this.radLabel12.TabIndex = 9;
            this.radLabel12.Text = "FILE";
            // 
            // radLabel13
            // 
            this.radLabel13.AutoSize = false;
            this.radLabel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel13.Location = new System.Drawing.Point(423, 3);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(213, 44);
            this.radLabel13.TabIndex = 3;
            this.radLabel13.Text = "DURASI (HARI)";
            // 
            // radLabel14
            // 
            this.radLabel14.AutoSize = false;
            this.radLabel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel14.Location = new System.Drawing.Point(3, 3);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(244, 44);
            this.radLabel14.TabIndex = 1;
            this.radLabel14.Text = "NO SURAT";
            // 
            // radLabel15
            // 
            this.radLabel15.AutoSize = false;
            this.radLabel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel15.Location = new System.Drawing.Point(253, 3);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(164, 44);
            this.radLabel15.TabIndex = 2;
            this.radLabel15.Text = "TANGGAL SURAT";
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnClose);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 616);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(932, 122);
            this.lblButton.TabIndex = 86;
            // 
            // btnClose
            // 
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(30, 30);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(244, 62);
            this.btnClose.TabIndex = 28;
            this.btnClose.Tag = "";
            this.btnClose.Text = "&TUTUP";
            this.btnClose.ThemeName = "MaterialBlueGrey";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(932, 50);
            this.lblTitle.TabIndex = 85;
            this.lblTitle.Text = "           DETAIL STATUS TAHANAN";
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(867, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 50);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 2;
            this.exit.TabStop = false;
            // 
            // StatusTahananDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblTitle);
            this.Name = "StatusTahananDetail";
            this.Size = new System.Drawing.Size(932, 738);
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblDataCell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoSkap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSpHan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUpdater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInputter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglSphan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglKap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuildingName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoreg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellFloor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNosel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            this.tablePerpanjangan3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblFileP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDurasiP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglSuratP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoSuratP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            this.radPageViewPage2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblFileP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDurasiP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglSuratP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNosuratP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            this.radPageViewPage3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblFileP3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDurasiP3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglSuratP3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoSuratP3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel universitas;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel updater;
        private Telerik.WinControls.UI.RadLabel lblNosel;
        private Telerik.WinControls.UI.RadLabel lblNoreg;
        private Telerik.WinControls.UI.RadLabel lblNama;
        private Telerik.WinControls.UI.RadLabel lblCellFloor;
        private Telerik.WinControls.UI.RadLabel lblCellCode;
        private Telerik.WinControls.UI.RadLabel lblBuildingName;
        private Telerik.WinControls.UI.RadLabel lblInputter;
        private Telerik.WinControls.UI.RadLabel lblTglInput;
        private Telerik.WinControls.UI.RadLabel lblUpdater;
        private Telerik.WinControls.UI.RadLabel updatedate;
        private Telerik.WinControls.UI.RadLabel inputdate;
        private Telerik.WinControls.UI.RadLabel inputter;
        private Telerik.WinControls.UI.RadLabel lblDetailData;
        private Telerik.WinControls.UI.RadLabel lblTglUpdate;
        private Telerik.WinControls.UI.RadLabel lblCellDescription;
        private Telerik.WinControls.UI.RadLabel lblCellStatus;
        private Telerik.WinControls.UI.RadLabel lblTglKap;
        private Telerik.WinControls.UI.RadLabel lblTglSphan;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private System.Windows.Forms.PictureBox exit;
        private Telerik.WinControls.UI.RadLabel lblNoSkap;
        private Telerik.WinControls.UI.RadLabel lblCellNumber;
        private Telerik.WinControls.UI.RadLabel lblSpHan;
        private Telerik.WinControls.UI.RadLabel lblCellType;
        private Telerik.WinControls.UI.RadLabel lblDataCell;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnClose;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage3;
        private System.Windows.Forms.TableLayoutPanel tablePerpanjangan3;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel lblFileP1;
        private Telerik.WinControls.UI.RadLabel lblDurasiP1;
        private Telerik.WinControls.UI.RadLabel lblTglSuratP1;
        private Telerik.WinControls.UI.RadLabel lblNoSuratP1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel lblFileP2;
        private Telerik.WinControls.UI.RadLabel lblDurasiP2;
        private Telerik.WinControls.UI.RadLabel lblTglSuratP2;
        private Telerik.WinControls.UI.RadLabel lblNosuratP2;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Telerik.WinControls.UI.RadLabel lblFileP3;
        private Telerik.WinControls.UI.RadLabel lblDurasiP3;
        private Telerik.WinControls.UI.RadLabel lblTglSuratP3;
        private Telerik.WinControls.UI.RadLabel lblNoSuratP3;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel15;
    }
}
