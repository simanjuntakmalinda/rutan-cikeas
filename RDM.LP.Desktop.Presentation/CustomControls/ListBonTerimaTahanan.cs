﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListBonTerimaTahanan : Base
    {
        public RadGridView GvBonTerimaTahanan { get { return this.gvBonTerimaTahanan; } }
        public ListBonTerimaTahanan()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvBonTerimaTahanan.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvBonTerimaTahanan.RowFormatting += radGridView_RowFormatting;
            gvBonTerimaTahanan.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvBonTerimaTahanan.CellClick += gvBonTerimaTahanan_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarBonTerimaTahanan.pdf", "DAFTAR DATA BON TERIMA TAHANAN", gvBonTerimaTahanan);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarBonTerimaTahanan.csv", "DAFTAR DATA BON TERIMA TAHANAN", gvBonTerimaTahanan);
        }

        private void gvBonTerimaTahanan_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == null)
                return;
            switch (e.Column.Index)
            {
                case 0:
                    MainForm.formMain.BonTerimaTahanan.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.AddNew;
                    MainForm.formMain.BonTerimaTahanan.LoadData();
                    break;
                case 1:
                    var question  = UserFunction.Confirm("Hapus Bon Tahanan Ini?");
                    if (question == DialogResult.Yes)
                    {
                        DeleteBonTerimaTahanan(e.Row.Cells["Id"].Value.ToString());
                        UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Dihapus!");
                        UserFunction.LoadDataBonTerimaTahananToGrid(gvBonTerimaTahanan);
                    }
                    break;
                case 2:
                    MainForm.formMain.PreviewDoc.Tag = "BonTerimaTahanan-" + e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.PreviewDoc.Show();
                    this.Hide();
                    break;
            }
            
        }

        private void DeleteBonTerimaTahanan(string Id)
        {
            using (BonTerimaTahananService bonserv = new BonTerimaTahananService())
            {
                var data = bonserv.GetById(Convert.ToInt32(Id));
                bonserv.DeleteById(Convert.ToInt32(Id));
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERVISITOR.ToString(), Activities = "Delete BonTerimaTahanan, data=" + UserFunction.JsonString(data) });

                BonTahananService serv = new BonTahananService();
                var bon = new BonTahanan();
                bon.Id = Convert.ToInt32(Convert.ToInt32(data.IdBonTahanan));
                var databon = serv.GetById(bon.Id);
                bon.StatusTahanan = "Keluar";
                bon.TanggalMulai = databon.TanggalMulai;
                bon.TanggalSelesai = databon.TanggalSelesai;
                bon.UpdatedDate = DateTime.Now.ToLocalTime();
                bon.UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID);
                bon.UpdatedLocation = "0.0.0.0";
                serv.Update(bon);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Update BonTahanan, Data=" + UserFunction.JsonString(databon) });
            }
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DAFTAR BON TERIMA TAHANAN";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;


            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Width = 15;
            btnDetail.Name = "btnDetail";
            gvBonTerimaTahanan.AutoGenerateColumns = false;
            gvBonTerimaTahanan.Columns.Insert(0, btnDetail);
            gvBonTerimaTahanan.Refresh();
            gvBonTerimaTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            GridViewImageColumn btnUbah = new GridViewImageColumn();
            btnUbah.HeaderText = "";
            btnUbah.Name = "btnUbah";
            gvBonTerimaTahanan.AutoGenerateColumns = false;
            gvBonTerimaTahanan.Columns.Insert(1, btnUbah);
            gvBonTerimaTahanan.Refresh();
            gvBonTerimaTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnHapus = new GridViewImageColumn();
            btnHapus.HeaderText = "";
            btnHapus.Name = "btnHapus";
            gvBonTerimaTahanan.AutoGenerateColumns = false;
            gvBonTerimaTahanan.Columns.Insert(1, btnHapus);
            gvBonTerimaTahanan.Refresh();
            gvBonTerimaTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnPrint = new GridViewImageColumn();
            btnPrint.HeaderText = "";
            btnPrint.Name = "btnPrint";
            gvBonTerimaTahanan.AutoGenerateColumns = false;
            gvBonTerimaTahanan.Columns.Insert(1, btnPrint);
            gvBonTerimaTahanan.Refresh();
            gvBonTerimaTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            UserFunction.LoadDataBonTerimaTahananToGrid(gvBonTerimaTahanan);
            UserFunction.SetInitGridView(gvBonTerimaTahanan);

        }

    }
}
