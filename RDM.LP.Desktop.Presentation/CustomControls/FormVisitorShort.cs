﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.Desktop.Presentation;
using AForge.Video;
using AForge.Video.DirectShow;
using Telerik.WinControls.Primitives;
using System.IO;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormVisitorShort : Base
    {
        private string RegId = string.Empty;
        private bool ExisitingVisitor = false;
        private bool ExisitingVisit = false;
        private Int32 VisitorID = 0;
        private Int32 VisitID = 0;
        public Label TahananId { get { return this.tahananId; } set { this.tahananId = value; } }
        public RadTextBox NamaTahanan { get { return this.txtInmatesName; } set { this.txtInmatesName = value; } }
        public RadTextBox IdNo { get { return this.txtNoIdentitas; } }
        public PictureBox FingerPicture { get { return this.picFinger; } }
        public PictureBox VisitorPhoto { get { return this.picCanvas; } }

        public bool ubahfingerprint = false;
        private bool fingerprint = false;
        public FingerPrint FingerPrint;

        public FormVisitorShort()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
            LoadData();
        }

        public void LoadData()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            //ddl Agama
            AgamaService agamaserv = new AgamaService();
            ddlAgama.Items.Clear();
            ddlAgama.DisplayMember = "Nama";
            ddlAgama.ValueMember = "Id";
            ddlAgama.DataSource = agamaserv.Get();

            //ddl Visitor Type
            VisitorTypeService visitortypeserv = new VisitorTypeService();
            ddlVisitorType.Items.Clear();
            ddlVisitorType.DisplayMember = "Nama";
            ddlVisitorType.ValueMember = "Id";
            ddlVisitorType.DataSource = visitortypeserv.Get();

            //ddl Gender
            GenderService Genderserv = new GenderService();
            ddlGender.Items.Clear();
            ddlGender.DisplayMember = "Nama";
            ddlGender.ValueMember = "Id";
            ddlGender.DataSource = Genderserv.Get();

            //ddl Negara
            NegaraService negaraserv = new NegaraService();
            ddlNationality.Items.Clear();
            ddlNationality.DisplayMember = "Nama_Negara";
            ddlNationality.ValueMember = "Id_Negara";
            ddlNationality.DataSource = negaraserv.Get();

            //ddl Pekerjaan
            PekerjaanService pekerjaanserv = new PekerjaanService();
            ddlVocation.Items.Clear();
            ddlVocation.DisplayMember = "Nama";
            ddlVocation.ValueMember = "Id";
            ddlVocation.DataSource = pekerjaanserv.Get();

            //ddl Marital Status
            MaritalStatusService maritalserv = new MaritalStatusService();
            ddlMaritalStatus.Items.Clear();
            ddlMaritalStatus.DisplayMember = "Nama";
            ddlMaritalStatus.ValueMember = "Id";
            ddlMaritalStatus.DataSource = maritalserv.Get();

            //ddl Relation
            //VisitorRelationService Relationserv = new VisitorRelationService();
            //ddlRelation.Items.Clear();
            //ddlRelation.DisplayMember = "Nama";
            //ddlRelation.ValueMember = "Id";
            //ddlRelation.DataSource = Relationserv.Get();

            //ddlMulai.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            //ddlSelesai.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            //ddlVisitorType_SelectedIndexChanged(ddlVisitorType, null);
            ddlJnsIdentitas_SelectedIndexChanged(ddlJnsIdentitas, null);

            txtInmatesName.ReadOnly = true;

            picCanvas.Image = null;
            picFinger.Image = null;

            if (this.Tag != null)
            {
                if(this.Tag.ToString().Substring(0,5) == "Edit-")
                {
                    txtInmatesName.ReadOnly = true;
                    MainForm.formMain.AddNew.Text = "EDIT VISITOR";
                    MainForm.formMain.CaptureFingerPrint.Hide();
                    LoadDataByRegNo(this.Tag.ToString().Split('-')[1]);
                    //LoadVisitDetailByRegNo(this.Tag.ToString().Split('-')[1]);
                    ExisitingVisit = true;
                    this.Tag = null;
                    HideVisitDetails();
                }
                else
                {
                    ShowVisitDetails();
                }
            }
        }

        private void HideVisitDetails()
        {
            ddlVisitorType.Hide();
            //ddlRelation.Hide();
            txtRelation.Hide();
            btnSearchTahanan.Hide();
            txtBringgingItems.Hide();
            //btnTakePhoto.Hide();
            txtInmatesName.Hide();
            lblRelation.Hide();
            lblDetailVisit.Hide();
            lblType.Hide();
            lblInmateNama.Hide();
            lblBringging.Hide();
            radPanel3.Hide();
            radPanel4.Hide();
            radPanel5.Hide();
            txtRelation.Hide();
            radLabel5.Hide();
            radPanel6.Hide();
            datetimeStartVisit.Hide();
        }

        private void ShowVisitDetails()
        {
            ddlVisitorType.Show();
            //ddlRelation.Show();
            txtRelation.Show();
            btnSearchTahanan.Show();
            txtBringgingItems.Show();
            //btnTakePhoto.Show();
            txtInmatesName.Show();
            lblRelation.Show();
            lblDetailVisit.Show();
            lblType.Show();
            lblInmateNama.Show();
            lblBringging.Show();
            radPanel3.Show();
            radPanel4.Show();
            radPanel5.Show();
            txtRelation.Show();

            if (fingerprint == false)
            {
                radLabel5.Show();
                radPanel6.Show();
                datetimeStartVisit.Show();
            }
            else
            {
                radLabel5.Hide();
                radPanel6.Hide();
                datetimeStartVisit.Hide();
            }
        }

        private void InitEvents()
        {
            //Events
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            this.VisibleChanged += Form_VisibleChanged;
            btnSearchTahanan.Click += btnSearchTahanan_Click;
            txtNoIdentitas.TextChanged += TxtNoIdentitas_TextChanged;
            txtNoIdentitas.LostFocus += txtNoIdentitas_LostFocus;
            ddlJnsIdentitas.SelectedIndexChanged += ddlJnsIdentitas_SelectedIndexChanged;
            //ddlVisitorType.SelectedIndexChanged += ddlVisitorType_SelectedIndexChanged;
            btnTakePhoto.Click += btnTakePhoto_Click;
            btnUploadPhoto.Click += btnUploadPhoto_Click;
            btnUbahSidikJari.Click += btnUbahSidikJari_Click;

            ddlJnsIdentitas.KeyDown += Control_KeyDown;
            txtNoIdentitas.KeyDown += Control_KeyDown;
            txtNama.KeyDown += Control_KeyDown;
            txtAddress.KeyDown += Control_KeyDown;
            txtCity.KeyDown += Control_KeyDown;
            ddlAgama.KeyDown += Control_KeyDown;
            ddlMaritalStatus.KeyDown += Control_KeyDown;
            ddlVocation.KeyDown += Control_KeyDown;
            ddlNationality.KeyDown += Control_KeyDown;
            ddlGender.KeyDown += Control_KeyDown;
            txtTelp.KeyDown += Control_KeyDown;
            txtTelp.KeyUp += txtTelp_KeyUp;
            ddlVisitorType.KeyDown += Control_KeyDown;
            //ddlRelation.KeyDown += Control_KeyDown;
            btnSearchTahanan.KeyDown += Control_KeyDown;
            txtBringgingItems.KeyDown += Control_KeyDown;
            //ddlMulai.KeyDown += Control_KeyDown;
            //ddlMulaiJam.KeyDown += Control_KeyDown;
            //ddlSelesai.KeyDown += Control_KeyDown;
            //ddlSelesaiJam.KeyDown += Control_KeyDown;
            btnSave.KeyDown += Control_KeyDown;
            btnUbahSidikJari.KeyDown += Control_KeyDown;

            radPanel3.KeyDown += Control_KeyDown;
            radPanel4.KeyDown += Control_KeyDown;
            radPanel5.KeyDown += Control_KeyDown;
            //radPanel6.KeyDown += Control_KeyDown;
            radPanel7.KeyDown += Control_KeyDown;
            radPanel8.KeyDown += Control_KeyDown;
            radPanel9.KeyDown += Control_KeyDown;
            radPanel10.KeyDown += Control_KeyDown;
            radPanel11.KeyDown += Control_KeyDown;
            radPanel12.KeyDown += Control_KeyDown;
            radPanel14.KeyDown += Control_KeyDown;
            radPanel15.KeyDown += Control_KeyDown;
            radPanel16.KeyDown += Control_KeyDown;
            radPanel17.KeyDown += Control_KeyDown;
            radPanel18.KeyDown += Control_KeyDown;
            radPanel19.KeyDown += Control_KeyDown;
            radPanel21.KeyDown += Control_KeyDown;
            btnTakePhoto.KeyDown += Control_KeyDown;
            btnUploadPhoto.KeyDown += Control_KeyDown;
            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;
        }

        private void txtTelp_KeyUp(object sender, KeyEventArgs e)
        {
            if (!UserFunction.IsNumeric(txtTelp.Text))
            {
                txtTelp.Text = string.Empty;
            }
        }

        private void btnTakePhoto_Click(object sender, EventArgs e)
        {
            MainForm.formMain.CapturePhoto.Tag = "Visitor";
            MainForm.formMain.CapturePhoto.Show();
            MainForm.formMain.CapturePhoto.Height = 400;
            MainForm.formMain.CapturePhoto.PanelBorder.Show();
            MainForm.formMain.CapturePhoto.FrontPhoto_Click();
        }

        private void btnUploadPhoto_Click(object sender, EventArgs e)
        {
            String imageLocation = "";
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "jpg files(*.jpg)|*.jpg| PNG files(*.png)|*.png| All files(*.*)|*.*";

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    imageLocation = dialog.FileName;

                    picCanvas.ImageLocation = imageLocation;
                }
            }
            catch
            {
                MessageBox.Show("An Error Occured", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnUbahSidikJari_Click(object sender, EventArgs e)
        {
            GlobalVariables.isVisitor = true;
            this.ubahfingerprint = true;
            MainForm.formMain.CaptureFingerPrint.Show();
            MainForm.formMain.CaptureFingerPrint.ResetDevice();
            MainForm.formMain.Opacity.Show();
        }

        //private void ddlVisitorType_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        //{
        //    VisitorType_SelectedIndexChanged();
        //}

        //private void VisitorType_SelectedIndexChanged()
        //{
        //    var type = ddlVisitorType.SelectedIndex;
        //    var typeX = ddlVisitorType.SelectedItem;
        //
        //    VisitorRelationService Relationserv = new VisitorRelationService();
        //    ddlRelation.Items.Clear();
        //    ddlRelation.DisplayMember = "Nama";
        //    ddlRelation.ValueMember = "Id";
        //    ddlRelation.DataSource = Relationserv.Get();
        //
        //    if (ddlRelation.Items.Count < 3)
        //        return;
        //
        //    if (type != -1)
        //    {
        //        if (typeX.Text.ToUpper() == "VISITOR")
        //        {
        //            ddlRelation.Items.Remove((ddlRelation.Items.FirstOrDefault(x => x.Text == "CLIENT")));
        //            ddlRelation.Items.Remove((ddlRelation.Items.FirstOrDefault(x => x.Text == "-")));
        //        }
        //        else if (typeX.Text.ToUpper() == "LAWYER")
        //        {
        //            ddlRelation.Items.Remove((ddlRelation.Items.FirstOrDefault(x => x.Text == "FRIEND")));
        //            ddlRelation.Items.Remove((ddlRelation.Items.FirstOrDefault(x => x.Text == "FAMILY")));
        //            ddlRelation.Items.Remove((ddlRelation.Items.FirstOrDefault(x => x.Text == "-")));
        //            ddlRelation.SelectedText = "CLIENT";
        //        }
        //        else if (typeX.Text.ToUpper() == "OFFICER")
        //        {
        //            ddlRelation.Items.Remove((ddlRelation.Items.FirstOrDefault(x => x.Text == "FRIEND")));
        //            ddlRelation.Items.Remove((ddlRelation.Items.FirstOrDefault(x => x.Text == "CLIENT")));
        //            ddlRelation.Items.Remove((ddlRelation.Items.FirstOrDefault(x => x.Text == "FAMILY")));
        //            ddlRelation.SelectedText = "-";
        //        }
        //    }
        //}

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }

        private void ddlJnsIdentitas_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (ddlJnsIdentitas.SelectedValue != null)
            {
                if (ddlJnsIdentitas.SelectedValue.ToString() == "1")
                {
                    txtNoIdentitas.MaxLength = 16;
                    while (txtNoIdentitas.TextLength > 16)
                    {
                        txtNoIdentitas.Text = txtNoIdentitas.Text.Remove(txtNoIdentitas.Text.Length - 1);
                    }
                }
                else if (ddlJnsIdentitas.SelectedValue.ToString() == "2")
                {
                    txtNoIdentitas.MaxLength = 12;
                    while (txtNoIdentitas.TextLength > 12)
                    {
                        txtNoIdentitas.Text = txtNoIdentitas.Text.Remove(txtNoIdentitas.Text.Length - 1);
                    }
                }
                else if (ddlJnsIdentitas.SelectedValue.ToString() == "3")
                {
                    txtNoIdentitas.MaxLength = 16;
                    while (txtNoIdentitas.TextLength > 16)
                    {
                        txtNoIdentitas.Text = txtNoIdentitas.Text.Remove(txtNoIdentitas.Text.Length - 1);
                    }
                }
            }
        }

        private void TxtNoIdentitas_TextChanged(object sender, EventArgs e)
        {
            if (ddlJnsIdentitas.SelectedValue != null)
            {
                if (ddlJnsIdentitas.SelectedValue.ToString() != "3")
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(txtNoIdentitas.Text, "[^0-9]"))
                    {
                        txtNoIdentitas.Text = txtNoIdentitas.Text.Remove(txtNoIdentitas.Text.Length - 1);
                    }
                }
            }
        }

        public void LoadVisitDetailByRegNo(string id)
        {
            VisitService visitserv = new VisitService();
            var data = visitserv.GetDetailById(id);
            ddlVisitorType.SelectedText = data.Type;
            //VisitorType_SelectedIndexChanged();
            VisitID = data.Id;
            //ddlRelation.SelectedText = data.Relation;
            txtInmatesName.Text = data.InmatesName;
            tahananId.Text = data.InmatesId.ToString();
            txtBringgingItems.Text = data.BringingItems;
        }

        public void LoadDataByRegNo(string id)
        {
            VisitorService visitorserv = new VisitorService();
            var data = visitorserv.GetDetailById(id);
            LoadDataVisitor(data);
            txtNoIdentitas.Focus();
        }


        private void txtNoIdentitas_LostFocus(object sender, EventArgs e)
        {
            var idtype = ddlJnsIdentitas.SelectedItem.Text;
            var idno = txtNoIdentitas.Text;
            VisitorService visitorserv = new VisitorService();

            var data = visitorserv.GetDetailByIdentity(idtype,idno);
            if(data != null)
            {
                LoadDataVisitor(data);
            }

            //ddlMulaiJam.Value = DateTime.Now;
        }

        public void ClearDataVisitor()
        {
            txtNama.Text = string.Empty;
            txtPlaceofBirth.Text = string.Empty;
            dpDateofBirth.Value = DateTime.Now.ToLocalTime();
            txtAddress.Text = string.Empty;
            txtCity.Text = string.Empty;
            ddlAgama.SelectedText = string.Empty;
            ddlNationality.SelectedText = string.Empty;
            ddlMaritalStatus.SelectedText = string.Empty;
            ddlVocation.SelectedText = string.Empty;
            ddlGender.SelectedText = string.Empty;
            txtTelp.Text = string.Empty;
            picCanvas.Image = null;
        }

        private void LoadDataVisitor(Visitor data)
        {
            VisitorID = data.Id;
            RegId = data.RegID;
            ddlJnsIdentitas.SelectedText = data.IDType;
            txtNoIdentitas.Text = data.IDNo;
            txtPlaceofBirth.Text = data.PlaceOfBirth;
            dpDateofBirth.Value = data.DateOfBirth != null ? (DateTime)data.DateOfBirth : DateTime.Now.ToLocalTime();
            txtNama.Text = data.FullName;
            txtAddress.Text = data.Address;
            txtCity.Text = data.City;
            ddlAgama.SelectedValue = ddlAgama.Items.FirstOrDefault(x => x.Text == data.Religion).Value;
            ddlMaritalStatus.SelectedValue = data.MaritalStatus != null ? ddlMaritalStatus.Items.FirstOrDefault(x => x.Text == data.MaritalStatus).Value : null;
            ddlNationality.SelectedValue = data.Nationality != null ? ddlNationality.Items.FirstOrDefault(x => x.Text == data.Nationality).Value : null;
            ddlVocation.SelectedValue = data.Vocation != null ? ddlVocation.Items.FirstOrDefault(x => x.Text == data.Vocation).Value : null;
            ddlGender.SelectedValue = ddlGender.Items.FirstOrDefault(x => x.Text == data.Gender).Value;
            txtTelp.Text = data.TelpNo;
            LoadDataFoto(data);
            LoadDataFinger(data);
            ExisitingVisitor = true; 
        }

        private void LoadDataFinger(Visitor data)
        {
            FingerPrintService fingerserv = new FingerPrintService();
            var finger = fingerserv.GetByRegID(data.RegID);
            if(finger != null)
            {
                var imageBytes = (byte[])Convert.FromBase64String(finger.ImagePrint);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);
                picFinger.Image = image;
            }
        }

        private void LoadDataFoto(Visitor data)
        {
            PhotosService photoserv = new PhotosService();
            var foto = photoserv.GetByRegID(data.RegID);
            foreach (var item in foto)
            {
                var imageBytes = (byte[])Convert.FromBase64String(item.Data);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);
                picCanvas.Image = image;
            }
            
        }

        private void btnSearchTahanan_Click(object sender, EventArgs e)
        {
            MainForm.formMain.SearchData.Tag = SearchType.Tahanan.ToString();
            MainForm.formMain.SearchData.Show();
        }

        private void InitForm()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            this.btnSearchTahanan.RootElement.EnableElementShadow = false;
            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     VISITOR REGISTRATION FORM";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;
            this.lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 14f;
            this.lblDetailSurat.ForeColor = Color.White;
            this.lblDetailSurat.BackColor = Color.FromArgb(77, 77, 77);

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailVisitor.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailVisitor.LabelElement.CustomFontSize = 15.5f;

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);

            //ddlMulai.Format = DateTimePickerFormat.Custom;
            //ddlMulai.CustomFormat = "dd MMM yyyy";

            //ddlMulaiJam.Format = DateTimePickerFormat.Custom;
            //ddlMulaiJam.CustomFormat = "HH:mm";
            //ddlMulaiJam.ShowUpDown = true;

            //ddlSelesai.Format = DateTimePickerFormat.Custom;
            //ddlSelesai.CustomFormat = "dd MMM yyyy";

            //ddlSelesaiJam.Format = DateTimePickerFormat.Custom;
            //ddlSelesaiJam.CustomFormat = "HH:mm";
            //ddlSelesaiJam.ShowUpDown = true;
           
            lblVisitorPhoto.ForeColor = Color.White;
            lblVisitorPhoto.BackColor = Color.FromArgb(45,45,48);
            lblVisitorFinger.ForeColor = Color.White;
            lblVisitorFinger.BackColor = Color.FromArgb(45, 45, 48);

        }

        private void btnBack_Click(object sender, EventArgs e)
        {

            this.Tag = null;
            MainForm.formMain.AddNew.Text = "TAMBAH PENGUNJUNG BARU";
            this.lblTitle.Text = "     TAMBAH pENGUNJUNG BARU";
            UserFunction.ClearControls(tableLayoutPanel1);
            MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
            picCanvas.Image = null;
            picFinger.Image = null;

            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                if (ExisitingVisitor)
                {
                    UpdateVisitor();
                }
                else
                    SaveVisitor();

                if (ExisitingVisit)
                {
                    VisitorService visitorService = new VisitorService();
                    var visitor = visitorService.GetDetailById(VisitorID.ToString());
                    PhotosService photosService = new PhotosService();
                    photosService.DeleteLastPhotoByRegId(visitor.RegID);
                    SavePhotos();

                    if (FingerPrint != null)
                    {
                        UpdateFingerPrint(FingerPrint);
                    }
                }
                else
                {
                    SaveVisit();
                    SavePhotos();
                    if (FingerPrint != null)
                    {
                        SaveFingerPrint(FingerPrint);
                    }
                }
                ExisitingVisit = false;
                ExisitingVisitor = false;
                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Disimpan!");
                UserFunction.LoadDataPegunjungToGrid(MainForm.formMain.ListVisitor.GvVisitor);
                UserFunction.LoadDataKunjunganToGrid(MainForm.formMain.ListKunjungan.GvVisitor);
                UserFunction.LoadDataPrintVisitorToGrid(MainForm.formMain.ListPrintFormVisitor.GvVisitor);
                MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
                MainForm.formMain.AddNew.Text = "TAMBAH PENGUNJUNG BARU";
                this.lblTitle.Text = "     VISITOR REGISTRATION FORM";
                this.Tag = null;
                picCanvas.Image = null;
                picFinger.Image = null;
            }
        }

        private void SaveFingerPrint(FingerPrint fingerprint)
        {
            FingerPrintService fingerprintserv = new FingerPrintService();
            fingerprint.RegID = RegId;
            fingerprintserv.Post(fingerprint);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.VISITORFINGERPRINT.ToString(), Activities = "Add New FingerPrint, Data=" + UserFunction.JsonString(fingerprint) });
        }

        private void UpdateFingerPrint(FingerPrint fingerprint)
        {
            FingerPrintService fingerprintserv = new FingerPrintService();
            var datafp = fingerprintserv.GetByRegId(RegId);
            if (datafp == null)
            {
                SaveFingerPrint(fingerprint);
            }
            else
            {
                fingerprint.Id = datafp.Id;
                fingerprint.RegID = RegId;
                fingerprintserv.Update(fingerprint);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.VISITORFINGERPRINT.ToString(), Activities = "Add New FingerPrint, Data=" + UserFunction.JsonString(fingerprint) });
            }
        }

        private void SavePhotos()
        {
            if (picCanvas.Image != null)
            {
                MemoryStream ms = new MemoryStream();
                picCanvas.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                byte[] foto = ms.ToArray();
                ms.Close();

                string base64Foto = Convert.ToBase64String(foto);
                PhotosService fotoserv = new PhotosService();
                var data = new Photos
                {
                    RegID = RegId,
                    Type = "V",
                    Data = base64Foto,
                    CreatedDate = DateTime.Now.ToLocalTime(),
                    CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
                };
                fotoserv.Post(data);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.VISITORPHOTO.ToString(), Activities = "Add New Photo, Data=" + UserFunction.JsonString(data) });
            }
        }

        private void SaveVisit()
        {
            var dateVisit = DateTime.Now;
            if (fingerprint == false)
            {
                dateVisit = datetimeStartVisit.Value;
            }
            var data = new Visit
            {
                InmatesId = Convert.ToInt32(tahananId.Text),
                InmatesName = txtInmatesName.Text,
                VisitorId = VisitorID,
                VisitorName = txtNama.Text,
                StartVisit = dateVisit,
                //Relation = ddlRelation.SelectedItem.Text,
                Relation = "KELUARGA",
                Type = ddlVisitorType.SelectedItem.Text,
                IsCheckedOut = false,
                BringingItems = txtBringgingItems.Text,
                CreatedDate = DateTime.Now.ToLocalTime(),
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
            };
            VisitService visit = new VisitService();
            visit.Post(data);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.VISITORVISIT.ToString(), Activities = "Add New Visit, Data=" + UserFunction.JsonString(data) });
        }

        private void SaveVisitor()
        {
            VisitorLogService visitorlog = new VisitorLogService();
            RegId = visitorlog.GetRegID();

            VisitorService pengunjung = new VisitorService();
            var data = new Visitor
            {
                RegID = RegId,
                FullName = txtNama.Text,
                PlaceOfBirth = txtPlaceofBirth.Text,
                DateOfBirth = dpDateofBirth.Value,
                IDType = ddlJnsIdentitas.SelectedItem.Text,
                IDNo = txtNoIdentitas.Text,
                Address = txtAddress.Text,
                TelpNo = txtTelp.Text,
                Religion = ddlAgama.SelectedItem.Text,
                MaritalStatus = ddlMaritalStatus.SelectedItem.Text,
                Vocation = ddlVocation.SelectedItem.Text,
                Nationality = ddlNationality.SelectedItem.Text,
                Gender = ddlGender.SelectedItem.Text,
                City = txtCity.Text,
                DateReg = DateTime.Now.ToLocalTime(),
                CreatedDate = DateTime.Now.ToLocalTime(),
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
            };
            VisitorID = pengunjung.Post(data);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERVISITOR.ToString(), Activities = "Add New Visitor, Data=" + UserFunction.JsonString(data) });

            var datalog = new VisitorLog
            {
                RegID = RegId,
                CreatedDate = DateTime.Now.ToLocalTime(),
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
            };
            visitorlog.Post(datalog);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Add New VisitorLog, Data=" + UserFunction.JsonString(datalog) });

        }



        private void UpdateVisitor()
        {
            VisitorService pengunjung = new VisitorService();
            var data = new Visitor
            {
                Id = VisitorID,
                RegID = RegId,
                FullName = txtNama.Text,
                PlaceOfBirth = txtPlaceofBirth.Text,
                DateOfBirth = dpDateofBirth.Value,
                IDType = ddlJnsIdentitas.SelectedItem.Text,
                IDNo = txtNoIdentitas.Text,
                Address = txtAddress.Text,
                TelpNo = txtTelp.Text,
                Religion = ddlAgama.SelectedItem.Text,
                MaritalStatus = ddlMaritalStatus.SelectedItem.Text,
                Vocation = ddlVocation.SelectedItem.Text,
                Nationality = ddlNationality.SelectedItem.Text,
                Gender = ddlGender.SelectedItem.Text,
                City = txtCity.Text,
                UpdatedDate = DateTime.Now.ToLocalTime(),
                UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
            };
            pengunjung.Update(data);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERVISITOR.ToString(), Activities = "Update Existing Visitor, Data=" + UserFunction.JsonString(data) });

        }

        private void UpdateVisit()
        {
            var data = new Visit
            {
                Id = VisitID,
                InmatesId = Convert.ToInt32(tahananId.Text),
                InmatesName = txtInmatesName.Text,
                VisitorId = VisitorID,
                VisitorName = txtNama.Text,
                //Relation = ddlRelation.SelectedItem.Text,
                Relation = "KELUARGA",
                Type = ddlVisitorType.SelectedItem.Text,
                BringingItems = txtBringgingItems.Text,
                UpdatedDate = DateTime.Now.ToLocalTime(),
                UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
            };
            VisitService visit = new VisitService();
            visit.Update(data);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.VISITORVISIT.ToString(), Activities = "Update Existing Visit, Data=" + UserFunction.JsonString(data) });
        }

        private bool DataValid()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            if (this.ddlJnsIdentitas.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Pilih Jenis Identitas!";
                return false;
            }

            if (this.txtNoIdentitas.Text == string.Empty )
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi No Identitas!";
                return false;
            }
            if (this.txtNama.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Nama Lengkap!";
                return false;
            }
            if (this.txtPlaceofBirth.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tempat Lahir!";
                return false;
            }
            if (this.dpDateofBirth.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tanggal Lahir!";
                return false;
            }
            if (this.txtAddress.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Alamat!";
                return false;
            }
            if (this.txtTelp.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Nomor Telepon!";
                return false;
            }
            if (!ExisitingVisit)
            {
                if (this.txtInmatesName.Text == string.Empty)
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = "Mohon Isi Nama Tahanan!";
                    return false;
                }
                if (this.txtBringgingItems.Text == string.Empty)
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = "Mohon Isi Barang yang Dibawa!";
                    return false;
                }
            }

            if (this.picCanvas.Image == null && fingerprint == true)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Ambil Gambar(Foto) Pengunjung!";
                return false;
            }

            if (fingerprint == false && this.datetimeStartVisit.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tanggal Berkunjung!";
                return false;
            }

            //if (this.ddlMulaiJam.Text == this.ddlSelesaiJam.Text)
            //{
            //    this.lblError.Visible = true;
            //    this.lblError.Text = "Invalid  Time Of Visit!";
            //    this.ddlMulaiJam.Focus();
            //    return false;
            //}

            //if (this.ddlMulaiJam.Value >= this.ddlSelesaiJam.Value)
            //{
            //    this.lblError.Visible = true;
            //    this.lblError.Text = "Invalid  Time Of Visit!";
            //    this.ddlMulaiJam.Focus();
            //    return false;
            //}

            //if (this.ddlMulai.Value > this.ddlSelesai.Value)
            //{
            //    this.lblError.Visible = true;
            //    this.lblError.Text = "Invalid  Date Of Visit!";
            //    this.ddlMulai.Focus();
            //    return false;
            //}
            return true;
        }

        private void TambahTersangka_Load(object sender, EventArgs e)
        {
            JenisIdentitasService jnsidentitas = new JenisIdentitasService();
            this.ddlJnsIdentitas.DisplayMember = "Nama";
            this.ddlJnsIdentitas.ValueMember = "Id";
            this.ddlJnsIdentitas.DataSource = jnsidentitas.Get();

        }

        private void FinalFrame_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            picCanvas.Image = (Bitmap)eventArgs.Frame.Clone();
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (this.Tag == null)
                {
                    var question = UserFunction.Confirm("REGISTRASI DENGAN IDENTIFIKASI SIDIK JARI?");
                    if (question == DialogResult.Yes)
                    {
                        fingerprint = true;

                        cameraSource.Location =
                            new Point(this.Width / 2 - cameraSource.Size.Width / 2,
                            this.Height / 2 - cameraSource.Size.Height / 2);

                        GlobalVariables.isVisitor = true;
                        UserFunction.ClearControls(tableLayoutPanel1);
                        MainForm.formMain.CaptureFingerPrint.Show();
                        MainForm.formMain.CaptureFingerPrint.ResetDevice();
                    }
                    else
                    {
                        fingerprint = false;

                        //StopFrame();
                        if (MainForm.formMain != null)
                            MainForm.formMain.CaptureFingerPrint.CloseDevice();

                        UserFunction.ClearControls(tableLayoutPanel1);
                    }

                    ShowVisitDetails();
                }
                else
                {
                    //edit
                    MainForm.formMain.CameraSource.Location =
                        new Point(this.Width / 2 - MainForm.formMain.CameraSource.Size.Width / 2,
                          this.Height / 2 - MainForm.formMain.CameraSource.Size.Height / 2);


                    MainForm.formMain.CaptureFingerPrint.Show();
                    MainForm.formMain.CaptureFingerPrint.ResetDevice();
                    MainForm.formMain.CaptureFingerPrint.Hide();
                }
            }
            else
            {
                if (MainForm.formMain != null)
                    MainForm.formMain.CaptureFingerPrint.CloseDevice();

                UserFunction.ClearControls(tableLayoutPanel1);
            }
        }
    }
}
