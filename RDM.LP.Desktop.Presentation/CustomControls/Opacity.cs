﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.Export;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.ViewModel;

namespace RDM.LP.Desktop.Presentation.CustomControls
{

    public partial class Opacity : UserControl
    {
        private const int WS_EX_TRANSPARENT = 0x20;
        public Opacity()
        {
            InitializeComponent();
            //SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            SetStyle(ControlStyles.Opaque, true);
            //this.BringToFront();
        }

        public Opacity(IContainer con)
        {

            con.Add(this);
            InitializeComponent();

        }

        protected override CreateParams CreateParams
        {

            get
            {

                CreateParams cpar = base.CreateParams;

                cpar.ExStyle = cpar.ExStyle | WS_EX_TRANSPARENT;

                return cpar;

            }

        }

        protected override void OnPaint(PaintEventArgs e)
        {

            using (var brush = new SolidBrush(Color.FromArgb
               (50 * 255 / 100, this.BackColor)))
            {

                e.Graphics.FillRectangle(brush, this.ClientRectangle);

            }

            base.OnPaint(e);
        }

    }
}
