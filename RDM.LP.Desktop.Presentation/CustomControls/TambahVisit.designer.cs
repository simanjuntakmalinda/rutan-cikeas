﻿namespace RDM.CM.Desktop.Presentation.CustomControls
{
    partial class TambahVisit
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.chk18 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk3 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk2 = new Telerik.WinControls.UI.RadCheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbKawin = new Telerik.WinControls.UI.RadRadioButton();
            this.rbTidakKawin = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.ddlJnsIdentitas = new Telerik.WinControls.UI.RadDropDownList();
            this.txtAlamat = new Telerik.WinControls.UI.RadTextBox();
            this.txtEmail = new Telerik.WinControls.UI.RadTextBox();
            this.txtNoTelp = new Telerik.WinControls.UI.RadTextBox();
            this.txtAlias = new Telerik.WinControls.UI.RadTextBox();
            this.txtIbuKandung = new Telerik.WinControls.UI.RadTextBox();
            this.txtTempatLahir = new Telerik.WinControls.UI.RadTextBox();
            this.txtNama = new Telerik.WinControls.UI.RadTextBox();
            this.txtNoIdentitas = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel25 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.ddlAgama = new Telerik.WinControls.UI.RadDropDownList();
            this.ddlPendidikan = new Telerik.WinControls.UI.RadDropDownList();
            this.ddlPekerjaan = new Telerik.WinControls.UI.RadDropDownList();
            this.ddlNegara = new Telerik.WinControls.UI.RadDropDownList();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbLaki = new Telerik.WinControls.UI.RadRadioButton();
            this.rbPerempuan = new Telerik.WinControls.UI.RadRadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.rbIdentitas1 = new Telerik.WinControls.UI.RadRadioButton();
            this.rbIdentitas2 = new Telerik.WinControls.UI.RadRadioButton();
            this.chk25 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk19 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk5 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk6 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk7 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk9 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk11 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk12 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk13 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk14 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk15 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk17 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.txtNamaKasus = new Telerik.WinControls.UI.RadTextBox();
            this.txtKeterangan = new Telerik.WinControls.UI.RadTextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ddlTglLahir = new Telerik.WinControls.UI.RadDateTimePicker();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radCheckBox1 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox2 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox3 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox4 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox5 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox6 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox7 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox8 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox9 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox10 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox11 = new Telerik.WinControls.UI.RadCheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radRadioButton1 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton2 = new Telerik.WinControls.UI.RadRadioButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.radRadioButton3 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton4 = new Telerik.WinControls.UI.RadRadioButton();
            this.radDropDownList1 = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList2 = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList3 = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList4 = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel26 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel27 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel28 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel29 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel31 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel32 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel33 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel34 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel35 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel36 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel37 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox3 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox4 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox5 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox6 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox7 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox8 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox9 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox10 = new Telerik.WinControls.UI.RadTextBox();
            this.radDropDownList5 = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel38 = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.panel6 = new System.Windows.Forms.Panel();
            this.radRadioButton5 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton6 = new Telerik.WinControls.UI.RadRadioButton();
            this.radCheckBox13 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox14 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox15 = new Telerik.WinControls.UI.RadCheckBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel39 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel40 = new Telerik.WinControls.UI.RadLabel();
            this.rad = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel41 = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker3 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.btnSebelumnya = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbKawin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbTidakKawin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlJnsIdentitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAlamat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoTelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAlias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIbuKandung)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTempatLahir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoIdentitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlAgama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPendidikan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPekerjaan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlNegara)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbLaki)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbPerempuan)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbIdentitas1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbIdentitas2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaKasus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeterangan)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlTglLahir)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox11)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton2)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox15)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSebelumnya)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            this.SuspendLayout();
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 0);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            this.radScrollablePanel1.Padding = new System.Windows.Forms.Padding(0);
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tabControl1);
            this.radScrollablePanel1.PanelContainer.Location = new System.Drawing.Point(0, 0);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(1328, 1024);
            this.radScrollablePanel1.Size = new System.Drawing.Size(1328, 1024);
            this.radScrollablePanel1.TabIndex = 1;
            // 
            // chk18
            // 
            this.chk18.AutoSize = false;
            this.chk18.Location = new System.Drawing.Point(1062, 713);
            this.chk18.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.chk18.Name = "chk18";
            this.chk18.Size = new System.Drawing.Size(170, 44);
            this.chk18.TabIndex = 99;
            this.chk18.Text = "Tidak Tahu";
            // 
            // chk3
            // 
            this.chk3.AutoSize = false;
            this.chk3.Location = new System.Drawing.Point(1062, 113);
            this.chk3.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.chk3.Name = "chk3";
            this.chk3.Size = new System.Drawing.Size(170, 44);
            this.chk3.TabIndex = 98;
            this.chk3.Text = "Tidak Tahu";
            // 
            // chk2
            // 
            this.chk2.AutoSize = false;
            this.chk2.Location = new System.Drawing.Point(1062, 63);
            this.chk2.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.chk2.Name = "chk2";
            this.chk2.Size = new System.Drawing.Size(170, 44);
            this.chk2.TabIndex = 97;
            this.chk2.Text = "Tidak Tahu";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 2);
            this.panel1.Controls.Add(this.rbTidakKawin);
            this.panel1.Controls.Add(this.rbKawin);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(271, 413);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(768, 44);
            this.panel1.TabIndex = 93;
            // 
            // rbKawin
            // 
            this.rbKawin.AutoSize = false;
            this.rbKawin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rbKawin.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.rbKawin.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbKawin.Location = new System.Drawing.Point(0, 0);
            this.rbKawin.Name = "rbKawin";
            this.rbKawin.Size = new System.Drawing.Size(385, 44);
            this.rbKawin.TabIndex = 71;
            this.rbKawin.Text = "Kawin";
            this.rbKawin.ThemeName = "ControlDefault";
            this.rbKawin.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.rbKawin.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbKawin_ToggleStateChanged);
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.rbKawin.GetChildAt(0))).ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.rbKawin.GetChildAt(0))).DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.rbKawin.GetChildAt(0))).Text = "Kawin";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbKawin.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).PaintUsingParentShape = false;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbKawin.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFont = "Roboto";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbKawin.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbKawin.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbKawin.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbKawin.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            // 
            // rbTidakKawin
            // 
            this.rbTidakKawin.AutoSize = false;
            this.rbTidakKawin.Dock = System.Windows.Forms.DockStyle.Right;
            this.rbTidakKawin.Location = new System.Drawing.Point(387, 0);
            this.rbTidakKawin.Name = "rbTidakKawin";
            this.rbTidakKawin.Size = new System.Drawing.Size(381, 44);
            this.rbTidakKawin.TabIndex = 71;
            this.rbTidakKawin.TabStop = false;
            this.rbTidakKawin.Text = "Tidak Kawin";
            this.rbTidakKawin.ThemeName = "Material";
            this.rbTidakKawin.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbTidakKawin_ToggleStateChanged);
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.rbTidakKawin.GetChildAt(0))).Text = "Tidak Kawin";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbTidakKawin.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFont = "Roboto";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbTidakKawin.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbTidakKawin.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbTidakKawin.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSize = true;
            // 
            // radLabel23
            // 
            this.radLabel23.AutoSize = false;
            this.radLabel23.Location = new System.Drawing.Point(14, 14);
            this.radLabel23.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel23.Name = "radLabel23";
            this.radLabel23.Size = new System.Drawing.Size(249, 32);
            this.radLabel23.TabIndex = 10;
            this.radLabel23.Text = "<html>Status Identitas<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // ddlJnsIdentitas
            // 
            this.ddlJnsIdentitas.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.ddlJnsIdentitas, 2);
            this.ddlJnsIdentitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlJnsIdentitas.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlJnsIdentitas.DropDownHeight = 500;
            this.ddlJnsIdentitas.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlJnsIdentitas.Location = new System.Drawing.Point(271, 63);
            this.ddlJnsIdentitas.Name = "ddlJnsIdentitas";
            // 
            // 
            // 
            this.ddlJnsIdentitas.RootElement.CustomFont = "Roboto";
            this.ddlJnsIdentitas.RootElement.CustomFontSize = 13F;
            this.ddlJnsIdentitas.Size = new System.Drawing.Size(768, 44);
            this.ddlJnsIdentitas.TabIndex = 64;
            this.ddlJnsIdentitas.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlJnsIdentitas.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlJnsIdentitas.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlJnsIdentitas.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // txtAlamat
            // 
            this.txtAlamat.AutoSize = false;
            this.txtAlamat.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtAlamat, 2);
            this.txtAlamat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAlamat.Location = new System.Drawing.Point(268, 810);
            this.txtAlamat.Margin = new System.Windows.Forms.Padding(0);
            this.txtAlamat.Name = "txtAlamat";
            this.txtAlamat.Size = new System.Drawing.Size(774, 50);
            this.txtAlamat.TabIndex = 62;
            // 
            // txtEmail
            // 
            this.txtEmail.AutoSize = false;
            this.txtEmail.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtEmail, 2);
            this.txtEmail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEmail.Location = new System.Drawing.Point(268, 760);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(0);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(774, 50);
            this.txtEmail.TabIndex = 58;
            // 
            // txtNoTelp
            // 
            this.txtNoTelp.AutoSize = false;
            this.txtNoTelp.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtNoTelp, 2);
            this.txtNoTelp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoTelp.Location = new System.Drawing.Point(268, 710);
            this.txtNoTelp.Margin = new System.Windows.Forms.Padding(0);
            this.txtNoTelp.Name = "txtNoTelp";
            this.txtNoTelp.Size = new System.Drawing.Size(774, 50);
            this.txtNoTelp.TabIndex = 56;
            // 
            // txtAlias
            // 
            this.txtAlias.AutoSize = false;
            this.txtAlias.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtAlias, 2);
            this.txtAlias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAlias.Location = new System.Drawing.Point(268, 360);
            this.txtAlias.Margin = new System.Windows.Forms.Padding(0);
            this.txtAlias.Name = "txtAlias";
            this.txtAlias.Size = new System.Drawing.Size(774, 50);
            this.txtAlias.TabIndex = 48;
            // 
            // txtIbuKandung
            // 
            this.txtIbuKandung.AutoSize = false;
            this.txtIbuKandung.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtIbuKandung, 2);
            this.txtIbuKandung.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIbuKandung.Location = new System.Drawing.Point(268, 310);
            this.txtIbuKandung.Margin = new System.Windows.Forms.Padding(0);
            this.txtIbuKandung.Name = "txtIbuKandung";
            this.txtIbuKandung.Size = new System.Drawing.Size(774, 50);
            this.txtIbuKandung.TabIndex = 44;
            // 
            // txtTempatLahir
            // 
            this.txtTempatLahir.AutoSize = false;
            this.txtTempatLahir.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtTempatLahir, 2);
            this.txtTempatLahir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTempatLahir.Location = new System.Drawing.Point(268, 210);
            this.txtTempatLahir.Margin = new System.Windows.Forms.Padding(0);
            this.txtTempatLahir.Name = "txtTempatLahir";
            this.txtTempatLahir.Size = new System.Drawing.Size(774, 50);
            this.txtTempatLahir.TabIndex = 43;
            // 
            // txtNama
            // 
            this.txtNama.AutoSize = false;
            this.txtNama.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtNama, 2);
            this.txtNama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNama.Location = new System.Drawing.Point(268, 160);
            this.txtNama.Margin = new System.Windows.Forms.Padding(0);
            this.txtNama.Name = "txtNama";
            this.txtNama.Size = new System.Drawing.Size(774, 50);
            this.txtNama.TabIndex = 42;
            // 
            // txtNoIdentitas
            // 
            this.txtNoIdentitas.AutoSize = false;
            this.txtNoIdentitas.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtNoIdentitas, 2);
            this.txtNoIdentitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoIdentitas.Location = new System.Drawing.Point(268, 110);
            this.txtNoIdentitas.Margin = new System.Windows.Forms.Padding(0);
            this.txtNoIdentitas.Name = "txtNoIdentitas";
            this.txtNoIdentitas.Size = new System.Drawing.Size(774, 50);
            this.txtNoIdentitas.TabIndex = 41;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel22
            // 
            this.radLabel22.AutoSize = false;
            this.radLabel22.Location = new System.Drawing.Point(14, 814);
            this.radLabel22.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(249, 32);
            this.radLabel22.TabIndex = 38;
            this.radLabel22.Text = "<html>Alamat/Nama Jalan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel16
            // 
            this.radLabel16.AutoSize = false;
            this.radLabel16.Location = new System.Drawing.Point(14, 764);
            this.radLabel16.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(249, 32);
            this.radLabel16.TabIndex = 36;
            this.radLabel16.Text = "<html>Alamat Email<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel15
            // 
            this.radLabel15.AutoSize = false;
            this.radLabel15.Location = new System.Drawing.Point(14, 714);
            this.radLabel15.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(249, 32);
            this.radLabel15.TabIndex = 31;
            this.radLabel15.Text = "<html>No Telepon<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel14
            // 
            this.radLabel14.AutoSize = false;
            this.radLabel14.Location = new System.Drawing.Point(14, 664);
            this.radLabel14.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(249, 32);
            this.radLabel14.TabIndex = 29;
            this.radLabel14.Text = "Nama Negara";
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Location = new System.Drawing.Point(14, 314);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(249, 32);
            this.radLabel4.TabIndex = 28;
            this.radLabel4.Text = "Nama Ibu Kandung";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Location = new System.Drawing.Point(14, 264);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(249, 32);
            this.radLabel3.TabIndex = 26;
            this.radLabel3.Text = "<html>Tanggal Lahir<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel11
            // 
            this.radLabel11.AutoSize = false;
            this.radLabel11.Location = new System.Drawing.Point(14, 564);
            this.radLabel11.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(249, 32);
            this.radLabel11.TabIndex = 24;
            this.radLabel11.Text = "<html>Pendidikan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel10
            // 
            this.radLabel10.AutoSize = false;
            this.radLabel10.Location = new System.Drawing.Point(14, 514);
            this.radLabel10.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(249, 32);
            this.radLabel10.TabIndex = 25;
            this.radLabel10.Text = "<html>Agama<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Location = new System.Drawing.Point(14, 464);
            this.radLabel9.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(249, 32);
            this.radLabel9.TabIndex = 23;
            this.radLabel9.Text = "<html>Jenis Kelamin<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Location = new System.Drawing.Point(14, 414);
            this.radLabel8.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(249, 32);
            this.radLabel8.TabIndex = 20;
            this.radLabel8.Text = "Status Perkawinan";
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Location = new System.Drawing.Point(14, 364);
            this.radLabel6.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(249, 32);
            this.radLabel6.TabIndex = 22;
            this.radLabel6.Text = "Nama Alias";
            // 
            // radLabel24
            // 
            this.radLabel24.AutoSize = false;
            this.radLabel24.Location = new System.Drawing.Point(14, 64);
            this.radLabel24.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel24.Name = "radLabel24";
            this.radLabel24.Size = new System.Drawing.Size(249, 32);
            this.radLabel24.TabIndex = 19;
            this.radLabel24.Text = "<html>Jenis Identitas<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel25
            // 
            this.radLabel25.AutoSize = false;
            this.radLabel25.Location = new System.Drawing.Point(14, 114);
            this.radLabel25.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel25.Name = "radLabel25";
            this.radLabel25.Size = new System.Drawing.Size(249, 32);
            this.radLabel25.TabIndex = 18;
            this.radLabel25.Text = "<html>Nomor Identitas<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel12
            // 
            this.radLabel12.AutoSize = false;
            this.radLabel12.Location = new System.Drawing.Point(14, 614);
            this.radLabel12.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(249, 32);
            this.radLabel12.TabIndex = 21;
            this.radLabel12.Text = "<html>Pekerjaan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Location = new System.Drawing.Point(14, 164);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(249, 32);
            this.radLabel1.TabIndex = 32;
            this.radLabel1.Text = "<html>Nama Lengkap<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Location = new System.Drawing.Point(14, 214);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(249, 32);
            this.radLabel2.TabIndex = 21;
            this.radLabel2.Text = "<html>Tempat Lahir<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // ddlAgama
            // 
            this.ddlAgama.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.ddlAgama, 2);
            this.ddlAgama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlAgama.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlAgama.DropDownHeight = 500;
            this.ddlAgama.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlAgama.Location = new System.Drawing.Point(271, 513);
            this.ddlAgama.Name = "ddlAgama";
            this.ddlAgama.Size = new System.Drawing.Size(768, 44);
            this.ddlAgama.TabIndex = 85;
            this.ddlAgama.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlAgama.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlAgama.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlAgama.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlAgama.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlAgama.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlAgama.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlAgama.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlAgama.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlAgama.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // ddlPendidikan
            // 
            this.ddlPendidikan.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.ddlPendidikan, 2);
            this.ddlPendidikan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlPendidikan.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlPendidikan.DropDownHeight = 500;
            this.ddlPendidikan.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlPendidikan.Location = new System.Drawing.Point(271, 563);
            this.ddlPendidikan.Name = "ddlPendidikan";
            this.ddlPendidikan.Size = new System.Drawing.Size(768, 44);
            this.ddlPendidikan.TabIndex = 86;
            this.ddlPendidikan.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPendidikan.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPendidikan.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPendidikan.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPendidikan.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPendidikan.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPendidikan.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPendidikan.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPendidikan.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPendidikan.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // ddlPekerjaan
            // 
            this.ddlPekerjaan.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.ddlPekerjaan, 2);
            this.ddlPekerjaan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlPekerjaan.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlPekerjaan.DropDownHeight = 500;
            this.ddlPekerjaan.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlPekerjaan.Location = new System.Drawing.Point(271, 613);
            this.ddlPekerjaan.Name = "ddlPekerjaan";
            this.ddlPekerjaan.Size = new System.Drawing.Size(768, 44);
            this.ddlPekerjaan.TabIndex = 87;
            this.ddlPekerjaan.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPekerjaan.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPekerjaan.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPekerjaan.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPekerjaan.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPekerjaan.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPekerjaan.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPekerjaan.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPekerjaan.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPekerjaan.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // ddlNegara
            // 
            this.ddlNegara.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.ddlNegara, 2);
            this.ddlNegara.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlNegara.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlNegara.DropDownHeight = 500;
            this.ddlNegara.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlNegara.Location = new System.Drawing.Point(271, 663);
            this.ddlNegara.Name = "ddlNegara";
            this.ddlNegara.Size = new System.Drawing.Size(768, 44);
            this.ddlNegara.TabIndex = 88;
            this.ddlNegara.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlNegara.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlNegara.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlNegara.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlNegara.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlNegara.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlNegara.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlNegara.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlNegara.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlNegara.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.panel2, 2);
            this.panel2.Controls.Add(this.rbPerempuan);
            this.panel2.Controls.Add(this.rbLaki);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(271, 463);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(768, 44);
            this.panel2.TabIndex = 94;
            // 
            // rbLaki
            // 
            this.rbLaki.AutoSize = false;
            this.rbLaki.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rbLaki.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.rbLaki.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbLaki.Location = new System.Drawing.Point(0, 0);
            this.rbLaki.Name = "rbLaki";
            this.rbLaki.Size = new System.Drawing.Size(381, 44);
            this.rbLaki.TabIndex = 71;
            this.rbLaki.Text = "Laki-Laki";
            this.rbLaki.ThemeName = "ControlDefault";
            this.rbLaki.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.rbLaki.GetChildAt(0))).ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.rbLaki.GetChildAt(0))).DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.rbLaki.GetChildAt(0))).Text = "Laki-Laki";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbLaki.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).PaintUsingParentShape = false;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbLaki.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFont = "Roboto";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbLaki.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbLaki.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbLaki.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbLaki.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            // 
            // rbPerempuan
            // 
            this.rbPerempuan.AutoSize = false;
            this.rbPerempuan.Dock = System.Windows.Forms.DockStyle.Right;
            this.rbPerempuan.Location = new System.Drawing.Point(387, 0);
            this.rbPerempuan.Name = "rbPerempuan";
            this.rbPerempuan.Size = new System.Drawing.Size(381, 44);
            this.rbPerempuan.TabIndex = 71;
            this.rbPerempuan.TabStop = false;
            this.rbPerempuan.Text = "Perempuan";
            this.rbPerempuan.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.rbPerempuan.GetChildAt(0))).Text = "Perempuan";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbPerempuan.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFont = "Roboto";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbPerempuan.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbPerempuan.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbPerempuan.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSize = true;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.panel4, 2);
            this.panel4.Controls.Add(this.rbIdentitas2);
            this.panel4.Controls.Add(this.rbIdentitas1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(271, 13);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(768, 44);
            this.panel4.TabIndex = 96;
            // 
            // rbIdentitas1
            // 
            this.rbIdentitas1.AutoSize = false;
            this.rbIdentitas1.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.rbIdentitas1.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbIdentitas1.Location = new System.Drawing.Point(0, 0);
            this.rbIdentitas1.Name = "rbIdentitas1";
            this.rbIdentitas1.Size = new System.Drawing.Size(381, 44);
            this.rbIdentitas1.TabIndex = 71;
            this.rbIdentitas1.TabStop = false;
            this.rbIdentitas1.Text = "Hanya Diketahui Nama";
            this.rbIdentitas1.ThemeName = "ControlDefault";
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.rbIdentitas1.GetChildAt(0))).DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.rbIdentitas1.GetChildAt(0))).Text = "Hanya Diketahui Nama";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbIdentitas1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).PaintUsingParentShape = false;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbIdentitas1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFont = "Roboto";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbIdentitas1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbIdentitas1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbIdentitas1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbIdentitas1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbIdentitas1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // rbIdentitas2
            // 
            this.rbIdentitas2.AutoSize = false;
            this.rbIdentitas2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rbIdentitas2.Dock = System.Windows.Forms.DockStyle.Right;
            this.rbIdentitas2.Location = new System.Drawing.Point(387, 0);
            this.rbIdentitas2.Name = "rbIdentitas2";
            this.rbIdentitas2.Size = new System.Drawing.Size(381, 44);
            this.rbIdentitas2.TabIndex = 71;
            this.rbIdentitas2.Text = "Diketahui Nama dan Identitas";
            this.rbIdentitas2.ThemeName = "Material";
            this.rbIdentitas2.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.rbIdentitas2.GetChildAt(0))).ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.rbIdentitas2.GetChildAt(0))).Text = "Diketahui Nama dan Identitas";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbIdentitas2.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFont = "Roboto";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbIdentitas2.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbIdentitas2.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.rbIdentitas2.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSize = true;
            // 
            // chk25
            // 
            this.chk25.AutoSize = false;
            this.chk25.Location = new System.Drawing.Point(1062, 813);
            this.chk25.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.chk25.Name = "chk25";
            this.chk25.Size = new System.Drawing.Size(170, 44);
            this.chk25.TabIndex = 99;
            this.chk25.Text = "Tidak Tahu";
            // 
            // chk19
            // 
            this.chk19.AutoSize = false;
            this.chk19.Location = new System.Drawing.Point(1062, 763);
            this.chk19.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.chk19.Name = "chk19";
            this.chk19.Size = new System.Drawing.Size(170, 44);
            this.chk19.TabIndex = 100;
            this.chk19.Text = "Tidak Tahu";
            // 
            // chk5
            // 
            this.chk5.AutoSize = false;
            this.chk5.Location = new System.Drawing.Point(1062, 213);
            this.chk5.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.chk5.Name = "chk5";
            this.chk5.Size = new System.Drawing.Size(170, 44);
            this.chk5.TabIndex = 99;
            this.chk5.Text = "Tidak Tahu";
            // 
            // chk6
            // 
            this.chk6.AutoSize = false;
            this.chk6.Location = new System.Drawing.Point(1062, 263);
            this.chk6.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.chk6.Name = "chk6";
            this.chk6.Size = new System.Drawing.Size(170, 44);
            this.chk6.TabIndex = 99;
            this.chk6.Text = "Tidak Tahu";
            // 
            // chk7
            // 
            this.chk7.AutoSize = false;
            this.chk7.Location = new System.Drawing.Point(1062, 313);
            this.chk7.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.chk7.Name = "chk7";
            this.chk7.Size = new System.Drawing.Size(170, 44);
            this.chk7.TabIndex = 99;
            this.chk7.Text = "Tidak Tahu";
            // 
            // chk9
            // 
            this.chk9.AutoSize = false;
            this.chk9.Location = new System.Drawing.Point(1062, 363);
            this.chk9.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.chk9.Name = "chk9";
            this.chk9.Size = new System.Drawing.Size(170, 44);
            this.chk9.TabIndex = 99;
            this.chk9.Text = "Tidak Tahu";
            // 
            // chk11
            // 
            this.chk11.AutoSize = false;
            this.chk11.Location = new System.Drawing.Point(1062, 413);
            this.chk11.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.chk11.Name = "chk11";
            this.chk11.Size = new System.Drawing.Size(170, 44);
            this.chk11.TabIndex = 99;
            this.chk11.Text = "Tidak Tahu";
            this.chk11.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chk11_ToggleStateChanged);
            // 
            // chk12
            // 
            this.chk12.AutoSize = false;
            this.chk12.Location = new System.Drawing.Point(1062, 463);
            this.chk12.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.chk12.Name = "chk12";
            this.chk12.Size = new System.Drawing.Size(170, 44);
            this.chk12.TabIndex = 99;
            this.chk12.Text = "Tidak Tahu";
            // 
            // chk13
            // 
            this.chk13.AutoSize = false;
            this.chk13.Location = new System.Drawing.Point(1062, 513);
            this.chk13.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.chk13.Name = "chk13";
            this.chk13.Size = new System.Drawing.Size(170, 44);
            this.chk13.TabIndex = 103;
            this.chk13.Text = "Tidak Tahu";
            // 
            // chk14
            // 
            this.chk14.AutoSize = false;
            this.chk14.Location = new System.Drawing.Point(1062, 563);
            this.chk14.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.chk14.Name = "chk14";
            this.chk14.Size = new System.Drawing.Size(170, 44);
            this.chk14.TabIndex = 104;
            this.chk14.Text = "Tidak Tahu";
            // 
            // chk15
            // 
            this.chk15.AutoSize = false;
            this.chk15.Location = new System.Drawing.Point(1062, 613);
            this.chk15.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.chk15.Name = "chk15";
            this.chk15.Size = new System.Drawing.Size(170, 44);
            this.chk15.TabIndex = 105;
            this.chk15.Text = "Tidak Tahu";
            // 
            // chk17
            // 
            this.chk17.AutoSize = false;
            this.chk17.Location = new System.Drawing.Point(1062, 663);
            this.chk17.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.chk17.Name = "chk17";
            this.chk17.Size = new System.Drawing.Size(170, 44);
            this.chk17.TabIndex = 107;
            this.chk17.Text = "Tidak Tahu";
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Location = new System.Drawing.Point(14, 864);
            this.radLabel7.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(249, 32);
            this.radLabel7.TabIndex = 108;
            this.radLabel7.Text = "<html>Nama Kasus <span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel13
            // 
            this.radLabel13.AutoSize = false;
            this.radLabel13.Location = new System.Drawing.Point(14, 914);
            this.radLabel13.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(249, 32);
            this.radLabel13.TabIndex = 109;
            this.radLabel13.Text = "<html>Keterangan Tahanan <span style=\"color: #ff0000\"> *</span></html>";
            // 
            // txtNamaKasus
            // 
            this.txtNamaKasus.AutoSize = false;
            this.txtNamaKasus.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtNamaKasus, 2);
            this.txtNamaKasus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNamaKasus.Location = new System.Drawing.Point(268, 860);
            this.txtNamaKasus.Margin = new System.Windows.Forms.Padding(0);
            this.txtNamaKasus.Name = "txtNamaKasus";
            this.txtNamaKasus.Size = new System.Drawing.Size(774, 50);
            this.txtNamaKasus.TabIndex = 110;
            // 
            // txtKeterangan
            // 
            this.txtKeterangan.AutoSize = false;
            this.txtKeterangan.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtKeterangan, 2);
            this.txtKeterangan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtKeterangan.Location = new System.Drawing.Point(268, 910);
            this.txtKeterangan.Margin = new System.Windows.Forms.Padding(0);
            this.txtKeterangan.Name = "txtKeterangan";
            this.txtKeterangan.Size = new System.Drawing.Size(774, 120);
            this.txtKeterangan.TabIndex = 111;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.txtKeterangan, 1, 18);
            this.tableLayoutPanel1.Controls.Add(this.txtNamaKasus, 1, 17);
            this.tableLayoutPanel1.Controls.Add(this.radLabel13, 0, 18);
            this.tableLayoutPanel1.Controls.Add(this.radLabel7, 0, 17);
            this.tableLayoutPanel1.Controls.Add(this.chk17, 3, 13);
            this.tableLayoutPanel1.Controls.Add(this.chk15, 3, 12);
            this.tableLayoutPanel1.Controls.Add(this.chk14, 3, 11);
            this.tableLayoutPanel1.Controls.Add(this.chk13, 3, 10);
            this.tableLayoutPanel1.Controls.Add(this.chk12, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.chk11, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.chk9, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.chk7, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.chk6, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.chk5, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.chk19, 3, 15);
            this.tableLayoutPanel1.Controls.Add(this.chk25, 3, 16);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.ddlNegara, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.ddlPekerjaan, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.ddlPendidikan, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.ddlAgama, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.radLabel2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.radLabel12, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.radLabel25, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.radLabel24, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radLabel6, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.radLabel8, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.radLabel9, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.radLabel10, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.radLabel11, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.radLabel3, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.radLabel4, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.radLabel14, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.radLabel15, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.radLabel16, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.radLabel22, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.txtNoIdentitas, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtNama, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtTempatLahir, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtIbuKandung, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtAlias, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtNoTelp, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.txtEmail, 1, 15);
            this.tableLayoutPanel1.Controls.Add(this.txtAlamat, 1, 16);
            this.tableLayoutPanel1.Controls.Add(this.ddlJnsIdentitas, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.radLabel23, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ddlTglLahir, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.chk2, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.chk3, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.chk18, 3, 14);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 108);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 19;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1311, 1040);
            this.tableLayoutPanel1.TabIndex = 16;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // ddlTglLahir
            // 
            this.ddlTglLahir.AutoSize = false;
            this.ddlTglLahir.BackColor = System.Drawing.Color.Transparent;
            this.ddlTglLahir.CalendarSize = new System.Drawing.Size(290, 320);
            this.tableLayoutPanel1.SetColumnSpan(this.ddlTglLahir, 2);
            this.ddlTglLahir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlTglLahir.Location = new System.Drawing.Point(268, 260);
            this.ddlTglLahir.Margin = new System.Windows.Forms.Padding(0);
            this.ddlTglLahir.Name = "ddlTglLahir";
            this.ddlTglLahir.Size = new System.Drawing.Size(774, 50);
            this.ddlTglLahir.TabIndex = 76;
            this.ddlTglLahir.TabStop = false;
            this.ddlTglLahir.Text = "Thursday, June 28, 2018";
            this.ddlTglLahir.Value = new System.DateTime(2018, 6, 28, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.ddlTglLahir.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlTglLahir.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlTglLahir.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlTglLahir.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlTglLahir.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlTglLahir.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlTglLahir.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.ddlTglLahir.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "Thursday, June 28, 2018";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.ddlTglLahir.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 7);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1303, 999);
            this.tabControl1.TabIndex = 26;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel2);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1295, 970);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Informasi Visitor";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel3);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1295, 970);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Jadwal Visit";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Controls.Add(this.radCheckBox1, 3, 13);
            this.tableLayoutPanel2.Controls.Add(this.radCheckBox2, 3, 12);
            this.tableLayoutPanel2.Controls.Add(this.radCheckBox3, 3, 11);
            this.tableLayoutPanel2.Controls.Add(this.radCheckBox4, 3, 10);
            this.tableLayoutPanel2.Controls.Add(this.radCheckBox5, 3, 9);
            this.tableLayoutPanel2.Controls.Add(this.radCheckBox6, 3, 8);
            this.tableLayoutPanel2.Controls.Add(this.radCheckBox7, 3, 7);
            this.tableLayoutPanel2.Controls.Add(this.radCheckBox8, 3, 6);
            this.tableLayoutPanel2.Controls.Add(this.radCheckBox9, 3, 5);
            this.tableLayoutPanel2.Controls.Add(this.radCheckBox10, 3, 4);
            this.tableLayoutPanel2.Controls.Add(this.radCheckBox11, 3, 15);
            this.tableLayoutPanel2.Controls.Add(this.panel3, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel5, 1, 9);
            this.tableLayoutPanel2.Controls.Add(this.radDropDownList1, 1, 13);
            this.tableLayoutPanel2.Controls.Add(this.radDropDownList2, 1, 12);
            this.tableLayoutPanel2.Controls.Add(this.radDropDownList3, 1, 11);
            this.tableLayoutPanel2.Controls.Add(this.radDropDownList4, 1, 10);
            this.tableLayoutPanel2.Controls.Add(this.radLabel18, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.radLabel19, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.radLabel20, 0, 12);
            this.tableLayoutPanel2.Controls.Add(this.radLabel21, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.radLabel26, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.radLabel27, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.radLabel28, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.radLabel29, 0, 9);
            this.tableLayoutPanel2.Controls.Add(this.radLabel30, 0, 10);
            this.tableLayoutPanel2.Controls.Add(this.radLabel31, 0, 11);
            this.tableLayoutPanel2.Controls.Add(this.radLabel32, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.radLabel33, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.radLabel34, 0, 13);
            this.tableLayoutPanel2.Controls.Add(this.radLabel35, 0, 14);
            this.tableLayoutPanel2.Controls.Add(this.radLabel36, 0, 15);
            this.tableLayoutPanel2.Controls.Add(this.radLabel37, 0, 16);
            this.tableLayoutPanel2.Controls.Add(this.radTextBox3, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.radTextBox4, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.radTextBox5, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.radTextBox6, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.radTextBox7, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.radTextBox8, 1, 14);
            this.tableLayoutPanel2.Controls.Add(this.radTextBox9, 1, 15);
            this.tableLayoutPanel2.Controls.Add(this.radTextBox10, 1, 16);
            this.tableLayoutPanel2.Controls.Add(this.radDropDownList5, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.radLabel38, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radDateTimePicker1, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.panel6, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.radCheckBox13, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.radCheckBox14, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.radCheckBox15, 3, 14);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel2.RowCount = 17;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1289, 964);
            this.tableLayoutPanel2.TabIndex = 26;
            // 
            // radCheckBox1
            // 
            this.radCheckBox1.AutoSize = false;
            this.radCheckBox1.Location = new System.Drawing.Point(1043, 663);
            this.radCheckBox1.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.radCheckBox1.Name = "radCheckBox1";
            this.radCheckBox1.Size = new System.Drawing.Size(170, 44);
            this.radCheckBox1.TabIndex = 107;
            this.radCheckBox1.Text = "Tidak Tahu";
            // 
            // radCheckBox2
            // 
            this.radCheckBox2.AutoSize = false;
            this.radCheckBox2.Location = new System.Drawing.Point(1043, 613);
            this.radCheckBox2.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.radCheckBox2.Name = "radCheckBox2";
            this.radCheckBox2.Size = new System.Drawing.Size(170, 44);
            this.radCheckBox2.TabIndex = 105;
            this.radCheckBox2.Text = "Tidak Tahu";
            // 
            // radCheckBox3
            // 
            this.radCheckBox3.AutoSize = false;
            this.radCheckBox3.Location = new System.Drawing.Point(1043, 563);
            this.radCheckBox3.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.radCheckBox3.Name = "radCheckBox3";
            this.radCheckBox3.Size = new System.Drawing.Size(170, 44);
            this.radCheckBox3.TabIndex = 104;
            this.radCheckBox3.Text = "Tidak Tahu";
            // 
            // radCheckBox4
            // 
            this.radCheckBox4.AutoSize = false;
            this.radCheckBox4.Location = new System.Drawing.Point(1043, 513);
            this.radCheckBox4.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.radCheckBox4.Name = "radCheckBox4";
            this.radCheckBox4.Size = new System.Drawing.Size(170, 44);
            this.radCheckBox4.TabIndex = 103;
            this.radCheckBox4.Text = "Tidak Tahu";
            // 
            // radCheckBox5
            // 
            this.radCheckBox5.AutoSize = false;
            this.radCheckBox5.Location = new System.Drawing.Point(1043, 463);
            this.radCheckBox5.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.radCheckBox5.Name = "radCheckBox5";
            this.radCheckBox5.Size = new System.Drawing.Size(170, 44);
            this.radCheckBox5.TabIndex = 99;
            this.radCheckBox5.Text = "Tidak Tahu";
            // 
            // radCheckBox6
            // 
            this.radCheckBox6.AutoSize = false;
            this.radCheckBox6.Location = new System.Drawing.Point(1043, 413);
            this.radCheckBox6.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.radCheckBox6.Name = "radCheckBox6";
            this.radCheckBox6.Size = new System.Drawing.Size(170, 44);
            this.radCheckBox6.TabIndex = 99;
            this.radCheckBox6.Text = "Tidak Tahu";
            // 
            // radCheckBox7
            // 
            this.radCheckBox7.AutoSize = false;
            this.radCheckBox7.Location = new System.Drawing.Point(1043, 363);
            this.radCheckBox7.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.radCheckBox7.Name = "radCheckBox7";
            this.radCheckBox7.Size = new System.Drawing.Size(170, 44);
            this.radCheckBox7.TabIndex = 99;
            this.radCheckBox7.Text = "Tidak Tahu";
            // 
            // radCheckBox8
            // 
            this.radCheckBox8.AutoSize = false;
            this.radCheckBox8.Location = new System.Drawing.Point(1043, 313);
            this.radCheckBox8.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.radCheckBox8.Name = "radCheckBox8";
            this.radCheckBox8.Size = new System.Drawing.Size(170, 44);
            this.radCheckBox8.TabIndex = 99;
            this.radCheckBox8.Text = "Tidak Tahu";
            // 
            // radCheckBox9
            // 
            this.radCheckBox9.AutoSize = false;
            this.radCheckBox9.Location = new System.Drawing.Point(1043, 263);
            this.radCheckBox9.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.radCheckBox9.Name = "radCheckBox9";
            this.radCheckBox9.Size = new System.Drawing.Size(170, 44);
            this.radCheckBox9.TabIndex = 99;
            this.radCheckBox9.Text = "Tidak Tahu";
            // 
            // radCheckBox10
            // 
            this.radCheckBox10.AutoSize = false;
            this.radCheckBox10.Location = new System.Drawing.Point(1043, 213);
            this.radCheckBox10.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.radCheckBox10.Name = "radCheckBox10";
            this.radCheckBox10.Size = new System.Drawing.Size(170, 44);
            this.radCheckBox10.TabIndex = 99;
            this.radCheckBox10.Text = "Tidak Tahu";
            // 
            // radCheckBox11
            // 
            this.radCheckBox11.AutoSize = false;
            this.radCheckBox11.Location = new System.Drawing.Point(1043, 763);
            this.radCheckBox11.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.radCheckBox11.Name = "radCheckBox11";
            this.radCheckBox11.Size = new System.Drawing.Size(170, 44);
            this.radCheckBox11.TabIndex = 100;
            this.radCheckBox11.Text = "Tidak Tahu";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.panel3, 2);
            this.panel3.Controls.Add(this.radRadioButton1);
            this.panel3.Controls.Add(this.radRadioButton2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(266, 13);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(754, 44);
            this.panel3.TabIndex = 96;
            // 
            // radRadioButton1
            // 
            this.radRadioButton1.AutoSize = false;
            this.radRadioButton1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadioButton1.Dock = System.Windows.Forms.DockStyle.Right;
            this.radRadioButton1.Location = new System.Drawing.Point(373, 0);
            this.radRadioButton1.Name = "radRadioButton1";
            this.radRadioButton1.Size = new System.Drawing.Size(381, 44);
            this.radRadioButton1.TabIndex = 71;
            this.radRadioButton1.Text = "Diketahui Nama dan Identitas";
            this.radRadioButton1.ThemeName = "Material";
            this.radRadioButton1.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.radRadioButton1.GetChildAt(0))).ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.radRadioButton1.GetChildAt(0))).Text = "Diketahui Nama dan Identitas";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFont = "Roboto";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSize = true;
            // 
            // radRadioButton2
            // 
            this.radRadioButton2.AutoSize = false;
            this.radRadioButton2.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.radRadioButton2.Dock = System.Windows.Forms.DockStyle.Left;
            this.radRadioButton2.Location = new System.Drawing.Point(0, 0);
            this.radRadioButton2.Name = "radRadioButton2";
            this.radRadioButton2.Size = new System.Drawing.Size(381, 44);
            this.radRadioButton2.TabIndex = 71;
            this.radRadioButton2.TabStop = false;
            this.radRadioButton2.Text = "Hanya Diketahui Nama";
            this.radRadioButton2.ThemeName = "ControlDefault";
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.radRadioButton2.GetChildAt(0))).DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.radRadioButton2.GetChildAt(0))).Text = "Hanya Diketahui Nama";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton2.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).PaintUsingParentShape = false;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton2.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFont = "Roboto";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton2.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton2.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton2.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton2.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton2.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.panel5, 2);
            this.panel5.Controls.Add(this.radRadioButton3);
            this.panel5.Controls.Add(this.radRadioButton4);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(266, 463);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(754, 44);
            this.panel5.TabIndex = 94;
            // 
            // radRadioButton3
            // 
            this.radRadioButton3.AutoSize = false;
            this.radRadioButton3.Dock = System.Windows.Forms.DockStyle.Right;
            this.radRadioButton3.Location = new System.Drawing.Point(373, 0);
            this.radRadioButton3.Name = "radRadioButton3";
            this.radRadioButton3.Size = new System.Drawing.Size(381, 44);
            this.radRadioButton3.TabIndex = 71;
            this.radRadioButton3.TabStop = false;
            this.radRadioButton3.Text = "Perempuan";
            this.radRadioButton3.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.radRadioButton3.GetChildAt(0))).Text = "Perempuan";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton3.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFont = "Roboto";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton3.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton3.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton3.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSize = true;
            // 
            // radRadioButton4
            // 
            this.radRadioButton4.AutoSize = false;
            this.radRadioButton4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadioButton4.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.radRadioButton4.Dock = System.Windows.Forms.DockStyle.Left;
            this.radRadioButton4.Location = new System.Drawing.Point(0, 0);
            this.radRadioButton4.Name = "radRadioButton4";
            this.radRadioButton4.Size = new System.Drawing.Size(381, 44);
            this.radRadioButton4.TabIndex = 71;
            this.radRadioButton4.Text = "Laki-Laki";
            this.radRadioButton4.ThemeName = "ControlDefault";
            this.radRadioButton4.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.radRadioButton4.GetChildAt(0))).ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.radRadioButton4.GetChildAt(0))).DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.radRadioButton4.GetChildAt(0))).Text = "Laki-Laki";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton4.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).PaintUsingParentShape = false;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton4.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFont = "Roboto";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton4.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton4.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton4.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton4.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            // 
            // radDropDownList1
            // 
            this.radDropDownList1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.radDropDownList1, 2);
            this.radDropDownList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDropDownList1.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.radDropDownList1.DropDownHeight = 500;
            this.radDropDownList1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList1.Location = new System.Drawing.Point(266, 663);
            this.radDropDownList1.Name = "radDropDownList1";
            this.radDropDownList1.Size = new System.Drawing.Size(754, 50);
            this.radDropDownList1.TabIndex = 88;
            this.radDropDownList1.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList1.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList1.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList1.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList1.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList1.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList1.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList1.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList1.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radDropDownList2
            // 
            this.radDropDownList2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.radDropDownList2, 2);
            this.radDropDownList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDropDownList2.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.radDropDownList2.DropDownHeight = 500;
            this.radDropDownList2.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList2.Location = new System.Drawing.Point(266, 613);
            this.radDropDownList2.Name = "radDropDownList2";
            this.radDropDownList2.Size = new System.Drawing.Size(754, 50);
            this.radDropDownList2.TabIndex = 87;
            this.radDropDownList2.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList2.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList2.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList2.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList2.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList2.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList2.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList2.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList2.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radDropDownList3
            // 
            this.radDropDownList3.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.radDropDownList3, 2);
            this.radDropDownList3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDropDownList3.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.radDropDownList3.DropDownHeight = 500;
            this.radDropDownList3.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList3.Location = new System.Drawing.Point(266, 563);
            this.radDropDownList3.Name = "radDropDownList3";
            this.radDropDownList3.Size = new System.Drawing.Size(754, 50);
            this.radDropDownList3.TabIndex = 86;
            this.radDropDownList3.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList3.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList3.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList3.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList3.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList3.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList3.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList3.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList3.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radDropDownList4
            // 
            this.radDropDownList4.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.radDropDownList4, 2);
            this.radDropDownList4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDropDownList4.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.radDropDownList4.DropDownHeight = 500;
            this.radDropDownList4.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList4.Location = new System.Drawing.Point(266, 513);
            this.radDropDownList4.Name = "radDropDownList4";
            this.radDropDownList4.Size = new System.Drawing.Size(754, 50);
            this.radDropDownList4.TabIndex = 85;
            this.radDropDownList4.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList4.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList4.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList4.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList4.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList4.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList4.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList4.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList4.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList4.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radLabel18
            // 
            this.radLabel18.AutoSize = false;
            this.radLabel18.Location = new System.Drawing.Point(14, 214);
            this.radLabel18.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(245, 32);
            this.radLabel18.TabIndex = 21;
            this.radLabel18.Text = "<html>Tempat Lahir<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel19
            // 
            this.radLabel19.AutoSize = false;
            this.radLabel19.Location = new System.Drawing.Point(14, 164);
            this.radLabel19.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(245, 32);
            this.radLabel19.TabIndex = 32;
            this.radLabel19.Text = "<html>Nama Lengkap<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel20
            // 
            this.radLabel20.AutoSize = false;
            this.radLabel20.Location = new System.Drawing.Point(14, 614);
            this.radLabel20.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(245, 32);
            this.radLabel20.TabIndex = 21;
            this.radLabel20.Text = "<html>Pekerjaan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel21
            // 
            this.radLabel21.AutoSize = false;
            this.radLabel21.Location = new System.Drawing.Point(14, 114);
            this.radLabel21.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel21.Name = "radLabel21";
            this.radLabel21.Size = new System.Drawing.Size(245, 32);
            this.radLabel21.TabIndex = 18;
            this.radLabel21.Text = "<html>Nomor Identitas<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel26
            // 
            this.radLabel26.AutoSize = false;
            this.radLabel26.Location = new System.Drawing.Point(14, 64);
            this.radLabel26.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel26.Name = "radLabel26";
            this.radLabel26.Size = new System.Drawing.Size(245, 32);
            this.radLabel26.TabIndex = 19;
            this.radLabel26.Text = "<html>Jenis Identitas<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel27
            // 
            this.radLabel27.AutoSize = false;
            this.radLabel27.Location = new System.Drawing.Point(14, 364);
            this.radLabel27.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel27.Name = "radLabel27";
            this.radLabel27.Size = new System.Drawing.Size(245, 32);
            this.radLabel27.TabIndex = 22;
            this.radLabel27.Text = "Nama Alias";
            // 
            // radLabel28
            // 
            this.radLabel28.AutoSize = false;
            this.radLabel28.Location = new System.Drawing.Point(14, 414);
            this.radLabel28.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel28.Name = "radLabel28";
            this.radLabel28.Size = new System.Drawing.Size(245, 32);
            this.radLabel28.TabIndex = 20;
            this.radLabel28.Text = "Status Perkawinan";
            // 
            // radLabel29
            // 
            this.radLabel29.AutoSize = false;
            this.radLabel29.Location = new System.Drawing.Point(14, 464);
            this.radLabel29.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel29.Name = "radLabel29";
            this.radLabel29.Size = new System.Drawing.Size(245, 32);
            this.radLabel29.TabIndex = 23;
            this.radLabel29.Text = "<html>Jenis Kelamin<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel30
            // 
            this.radLabel30.AutoSize = false;
            this.radLabel30.Location = new System.Drawing.Point(14, 514);
            this.radLabel30.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel30.Name = "radLabel30";
            this.radLabel30.Size = new System.Drawing.Size(245, 32);
            this.radLabel30.TabIndex = 25;
            this.radLabel30.Text = "<html>Agama<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel31
            // 
            this.radLabel31.AutoSize = false;
            this.radLabel31.Location = new System.Drawing.Point(14, 564);
            this.radLabel31.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel31.Name = "radLabel31";
            this.radLabel31.Size = new System.Drawing.Size(245, 32);
            this.radLabel31.TabIndex = 24;
            this.radLabel31.Text = "<html>Pendidikan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel32
            // 
            this.radLabel32.AutoSize = false;
            this.radLabel32.Location = new System.Drawing.Point(14, 264);
            this.radLabel32.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel32.Name = "radLabel32";
            this.radLabel32.Size = new System.Drawing.Size(245, 32);
            this.radLabel32.TabIndex = 26;
            this.radLabel32.Text = "<html>Tanggal Lahir<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel33
            // 
            this.radLabel33.AutoSize = false;
            this.radLabel33.Location = new System.Drawing.Point(14, 314);
            this.radLabel33.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel33.Name = "radLabel33";
            this.radLabel33.Size = new System.Drawing.Size(245, 32);
            this.radLabel33.TabIndex = 28;
            this.radLabel33.Text = "Nama Ibu Kandung";
            // 
            // radLabel34
            // 
            this.radLabel34.AutoSize = false;
            this.radLabel34.Location = new System.Drawing.Point(14, 664);
            this.radLabel34.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel34.Name = "radLabel34";
            this.radLabel34.Size = new System.Drawing.Size(245, 32);
            this.radLabel34.TabIndex = 29;
            this.radLabel34.Text = "Nama Negara";
            // 
            // radLabel35
            // 
            this.radLabel35.AutoSize = false;
            this.radLabel35.Location = new System.Drawing.Point(14, 714);
            this.radLabel35.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel35.Name = "radLabel35";
            this.radLabel35.Size = new System.Drawing.Size(245, 32);
            this.radLabel35.TabIndex = 31;
            this.radLabel35.Text = "<html>No Telepon<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel36
            // 
            this.radLabel36.AutoSize = false;
            this.radLabel36.Location = new System.Drawing.Point(14, 764);
            this.radLabel36.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel36.Name = "radLabel36";
            this.radLabel36.Size = new System.Drawing.Size(245, 32);
            this.radLabel36.TabIndex = 36;
            this.radLabel36.Text = "<html>Alamat Email<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel37
            // 
            this.radLabel37.AutoSize = false;
            this.radLabel37.Location = new System.Drawing.Point(14, 814);
            this.radLabel37.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel37.Name = "radLabel37";
            this.radLabel37.Size = new System.Drawing.Size(245, 32);
            this.radLabel37.TabIndex = 38;
            this.radLabel37.Text = "<html>Alamat/Nama Jalan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radTextBox3
            // 
            this.radTextBox3.AutoSize = false;
            this.radTextBox3.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.radTextBox3, 2);
            this.radTextBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTextBox3.Location = new System.Drawing.Point(263, 110);
            this.radTextBox3.Margin = new System.Windows.Forms.Padding(0);
            this.radTextBox3.Name = "radTextBox3";
            this.radTextBox3.Size = new System.Drawing.Size(760, 50);
            this.radTextBox3.TabIndex = 41;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTextBox3.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTextBox3.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTextBox3.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radTextBox3.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radTextBox4
            // 
            this.radTextBox4.AutoSize = false;
            this.radTextBox4.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.radTextBox4, 2);
            this.radTextBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTextBox4.Location = new System.Drawing.Point(263, 160);
            this.radTextBox4.Margin = new System.Windows.Forms.Padding(0);
            this.radTextBox4.Name = "radTextBox4";
            this.radTextBox4.Size = new System.Drawing.Size(760, 50);
            this.radTextBox4.TabIndex = 42;
            // 
            // radTextBox5
            // 
            this.radTextBox5.AutoSize = false;
            this.radTextBox5.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.radTextBox5, 2);
            this.radTextBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTextBox5.Location = new System.Drawing.Point(263, 210);
            this.radTextBox5.Margin = new System.Windows.Forms.Padding(0);
            this.radTextBox5.Name = "radTextBox5";
            this.radTextBox5.Size = new System.Drawing.Size(760, 50);
            this.radTextBox5.TabIndex = 43;
            // 
            // radTextBox6
            // 
            this.radTextBox6.AutoSize = false;
            this.radTextBox6.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.radTextBox6, 2);
            this.radTextBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTextBox6.Location = new System.Drawing.Point(263, 310);
            this.radTextBox6.Margin = new System.Windows.Forms.Padding(0);
            this.radTextBox6.Name = "radTextBox6";
            this.radTextBox6.Size = new System.Drawing.Size(760, 50);
            this.radTextBox6.TabIndex = 44;
            // 
            // radTextBox7
            // 
            this.radTextBox7.AutoSize = false;
            this.radTextBox7.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.radTextBox7, 2);
            this.radTextBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTextBox7.Location = new System.Drawing.Point(263, 360);
            this.radTextBox7.Margin = new System.Windows.Forms.Padding(0);
            this.radTextBox7.Name = "radTextBox7";
            this.radTextBox7.Size = new System.Drawing.Size(760, 50);
            this.radTextBox7.TabIndex = 48;
            // 
            // radTextBox8
            // 
            this.radTextBox8.AutoSize = false;
            this.radTextBox8.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.radTextBox8, 2);
            this.radTextBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTextBox8.Location = new System.Drawing.Point(263, 710);
            this.radTextBox8.Margin = new System.Windows.Forms.Padding(0);
            this.radTextBox8.Name = "radTextBox8";
            this.radTextBox8.Size = new System.Drawing.Size(760, 50);
            this.radTextBox8.TabIndex = 56;
            // 
            // radTextBox9
            // 
            this.radTextBox9.AutoSize = false;
            this.radTextBox9.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.radTextBox9, 2);
            this.radTextBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTextBox9.Location = new System.Drawing.Point(263, 760);
            this.radTextBox9.Margin = new System.Windows.Forms.Padding(0);
            this.radTextBox9.Name = "radTextBox9";
            this.radTextBox9.Size = new System.Drawing.Size(760, 50);
            this.radTextBox9.TabIndex = 58;
            // 
            // radTextBox10
            // 
            this.radTextBox10.AutoSize = false;
            this.radTextBox10.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.radTextBox10, 2);
            this.radTextBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTextBox10.Location = new System.Drawing.Point(263, 810);
            this.radTextBox10.Margin = new System.Windows.Forms.Padding(0);
            this.radTextBox10.Name = "radTextBox10";
            this.radTextBox10.Size = new System.Drawing.Size(760, 144);
            this.radTextBox10.TabIndex = 62;
            // 
            // radDropDownList5
            // 
            this.radDropDownList5.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.radDropDownList5, 2);
            this.radDropDownList5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDropDownList5.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.radDropDownList5.DropDownHeight = 500;
            this.radDropDownList5.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList5.Location = new System.Drawing.Point(266, 63);
            this.radDropDownList5.Name = "radDropDownList5";
            // 
            // 
            // 
            this.radDropDownList5.RootElement.CustomFont = "Roboto";
            this.radDropDownList5.RootElement.CustomFontSize = 13F;
            this.radDropDownList5.Size = new System.Drawing.Size(754, 50);
            this.radDropDownList5.TabIndex = 64;
            this.radDropDownList5.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList5.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList5.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList5.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList5.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList5.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList5.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList5.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList5.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList5.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radLabel38
            // 
            this.radLabel38.AutoSize = false;
            this.radLabel38.Location = new System.Drawing.Point(14, 14);
            this.radLabel38.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel38.Name = "radLabel38";
            this.radLabel38.Size = new System.Drawing.Size(245, 32);
            this.radLabel38.TabIndex = 10;
            this.radLabel38.Text = "<html>Status Identitas<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radDateTimePicker1
            // 
            this.radDateTimePicker1.AutoSize = false;
            this.radDateTimePicker1.BackColor = System.Drawing.Color.Transparent;
            this.radDateTimePicker1.CalendarSize = new System.Drawing.Size(290, 320);
            this.tableLayoutPanel2.SetColumnSpan(this.radDateTimePicker1, 2);
            this.radDateTimePicker1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDateTimePicker1.Location = new System.Drawing.Point(263, 260);
            this.radDateTimePicker1.Margin = new System.Windows.Forms.Padding(0);
            this.radDateTimePicker1.Name = "radDateTimePicker1";
            this.radDateTimePicker1.Size = new System.Drawing.Size(760, 50);
            this.radDateTimePicker1.TabIndex = 76;
            this.radDateTimePicker1.TabStop = false;
            this.radDateTimePicker1.Text = "Thursday, June 28, 2018";
            this.radDateTimePicker1.Value = new System.DateTime(2018, 6, 28, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.radDateTimePicker1.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDateTimePicker1.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDateTimePicker1.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDateTimePicker1.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDateTimePicker1.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDateTimePicker1.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.radDateTimePicker1.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.radDateTimePicker1.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "Thursday, June 28, 2018";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.radDateTimePicker1.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.SetColumnSpan(this.panel6, 2);
            this.panel6.Controls.Add(this.radRadioButton5);
            this.panel6.Controls.Add(this.radRadioButton6);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(266, 413);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(754, 44);
            this.panel6.TabIndex = 93;
            // 
            // radRadioButton5
            // 
            this.radRadioButton5.AutoSize = false;
            this.radRadioButton5.Dock = System.Windows.Forms.DockStyle.Right;
            this.radRadioButton5.Location = new System.Drawing.Point(373, 0);
            this.radRadioButton5.Name = "radRadioButton5";
            this.radRadioButton5.Size = new System.Drawing.Size(381, 44);
            this.radRadioButton5.TabIndex = 71;
            this.radRadioButton5.TabStop = false;
            this.radRadioButton5.Text = "Tidak Kawin";
            this.radRadioButton5.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.radRadioButton5.GetChildAt(0))).Text = "Tidak Kawin";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton5.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFont = "Roboto";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton5.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton5.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton5.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSize = true;
            // 
            // radRadioButton6
            // 
            this.radRadioButton6.AutoSize = false;
            this.radRadioButton6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radRadioButton6.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.radRadioButton6.Dock = System.Windows.Forms.DockStyle.Left;
            this.radRadioButton6.Location = new System.Drawing.Point(0, 0);
            this.radRadioButton6.Name = "radRadioButton6";
            this.radRadioButton6.Size = new System.Drawing.Size(385, 44);
            this.radRadioButton6.TabIndex = 71;
            this.radRadioButton6.Text = "Kawin";
            this.radRadioButton6.ThemeName = "ControlDefault";
            this.radRadioButton6.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.radRadioButton6.GetChildAt(0))).ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.radRadioButton6.GetChildAt(0))).DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            ((Telerik.WinControls.UI.RadRadioButtonElement)(this.radRadioButton6.GetChildAt(0))).Text = "Kawin";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton6.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).PaintUsingParentShape = false;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton6.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFont = "Roboto";
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton6.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton6.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton6.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.RadioPrimitive)(this.radRadioButton6.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(0))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            // 
            // radCheckBox13
            // 
            this.radCheckBox13.AutoSize = false;
            this.radCheckBox13.Location = new System.Drawing.Point(1043, 63);
            this.radCheckBox13.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.radCheckBox13.Name = "radCheckBox13";
            this.radCheckBox13.Size = new System.Drawing.Size(170, 44);
            this.radCheckBox13.TabIndex = 97;
            this.radCheckBox13.Text = "Tidak Tahu";
            // 
            // radCheckBox14
            // 
            this.radCheckBox14.AutoSize = false;
            this.radCheckBox14.Location = new System.Drawing.Point(1043, 113);
            this.radCheckBox14.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.radCheckBox14.Name = "radCheckBox14";
            this.radCheckBox14.Size = new System.Drawing.Size(170, 44);
            this.radCheckBox14.TabIndex = 98;
            this.radCheckBox14.Text = "Tidak Tahu";
            // 
            // radCheckBox15
            // 
            this.radCheckBox15.AutoSize = false;
            this.radCheckBox15.Location = new System.Drawing.Point(1043, 713);
            this.radCheckBox15.Margin = new System.Windows.Forms.Padding(20, 3, 3, 3);
            this.radCheckBox15.Name = "radCheckBox15";
            this.radCheckBox15.Size = new System.Drawing.Size(170, 44);
            this.radCheckBox15.TabIndex = 99;
            this.radCheckBox15.Text = "Tidak Tahu";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 514F));
            this.tableLayoutPanel3.Controls.Add(this.radLabel41, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.rad, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.radLabel39, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.radLabel40, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.radDateTimePicker2, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.radDateTimePicker3, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.btnSebelumnya, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.btnSave, 2, 3);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 61.15702F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38.84298F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 696F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1289, 964);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // radLabel39
            // 
            this.radLabel39.AutoSize = false;
            this.radLabel39.Location = new System.Drawing.Point(4, 4);
            this.radLabel39.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel39.Name = "radLabel39";
            this.radLabel39.Size = new System.Drawing.Size(395, 32);
            this.radLabel39.TabIndex = 11;
            this.radLabel39.Text = "<html>Nama Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel40
            // 
            this.radLabel40.AutoSize = false;
            this.radLabel40.Location = new System.Drawing.Point(4, 86);
            this.radLabel40.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel40.Name = "radLabel40";
            this.radLabel40.Size = new System.Drawing.Size(395, 32);
            this.radLabel40.TabIndex = 12;
            this.radLabel40.Text = "<html>Mulai Visit<span style=\"color: #ff0000\"> *</span></html>";
            this.radLabel40.Click += new System.EventHandler(this.radLabel40_Click);
            // 
            // rad
            // 
            this.rad.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel3.SetColumnSpan(this.rad, 2);
            this.rad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rad.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.rad.DropDownHeight = 500;
            this.rad.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.rad.Location = new System.Drawing.Point(406, 3);
            this.rad.Name = "rad";
            this.rad.Size = new System.Drawing.Size(880, 50);
            this.rad.TabIndex = 86;
            this.rad.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.rad.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.rad.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.rad.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.rad.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.rad.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.rad.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.rad.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.rad.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.rad.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radLabel41
            // 
            this.radLabel41.AutoSize = false;
            this.radLabel41.Location = new System.Drawing.Point(4, 138);
            this.radLabel41.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel41.Name = "radLabel41";
            this.radLabel41.Size = new System.Drawing.Size(395, 32);
            this.radLabel41.TabIndex = 87;
            this.radLabel41.Text = "<html>Selesai Visit<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radDateTimePicker2
            // 
            this.radDateTimePicker2.CalendarSize = new System.Drawing.Size(290, 320);
            this.radDateTimePicker2.Location = new System.Drawing.Point(406, 85);
            this.radDateTimePicker2.Name = "radDateTimePicker2";
            this.radDateTimePicker2.Size = new System.Drawing.Size(366, 41);
            this.radDateTimePicker2.TabIndex = 88;
            this.radDateTimePicker2.TabStop = false;
            this.radDateTimePicker2.Text = "Wednesday, November 7, 2018";
            this.radDateTimePicker2.Value = new System.DateTime(2018, 11, 7, 16, 41, 12, 339);
            // 
            // radDateTimePicker3
            // 
            this.radDateTimePicker3.CalendarSize = new System.Drawing.Size(290, 320);
            this.radDateTimePicker3.Location = new System.Drawing.Point(406, 137);
            this.radDateTimePicker3.Name = "radDateTimePicker3";
            this.radDateTimePicker3.Size = new System.Drawing.Size(366, 41);
            this.radDateTimePicker3.TabIndex = 89;
            this.radDateTimePicker3.TabStop = false;
            this.radDateTimePicker3.Text = "Wednesday, November 7, 2018";
            this.radDateTimePicker3.Value = new System.DateTime(2018, 11, 7, 16, 41, 12, 339);
            // 
            // btnSebelumnya
            // 
            this.btnSebelumnya.Location = new System.Drawing.Point(406, 194);
            this.btnSebelumnya.Name = "btnSebelumnya";
            this.btnSebelumnya.Size = new System.Drawing.Size(366, 70);
            this.btnSebelumnya.TabIndex = 90;
            this.btnSebelumnya.Text = "Sebelumnya";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(778, 194);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(374, 70);
            this.btnSave.TabIndex = 91;
            this.btnSave.Text = "Simpan";
            // 
            // TambahVisit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.radScrollablePanel1);
            this.Name = "TambahVisit";
            this.Size = new System.Drawing.Size(1328, 1024);
            this.Load += new System.EventHandler(this.TambahTersangka_Load);
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbKawin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbTidakKawin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlJnsIdentitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAlamat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoTelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAlias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIbuKandung)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTempatLahir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoIdentitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlAgama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPendidikan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPekerjaan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlNegara)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbLaki)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbPerempuan)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbIdentitas1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbIdentitas2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaKasus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeterangan)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlTglLahir)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox11)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton2)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox15)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSebelumnya)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox1;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox2;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox3;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox4;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox5;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox6;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox7;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox8;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox9;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox10;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox11;
        private System.Windows.Forms.Panel panel3;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton1;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton2;
        private System.Windows.Forms.Panel panel5;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton3;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton4;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList1;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList2;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList3;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList4;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadLabel radLabel21;
        private Telerik.WinControls.UI.RadLabel radLabel26;
        private Telerik.WinControls.UI.RadLabel radLabel27;
        private Telerik.WinControls.UI.RadLabel radLabel28;
        private Telerik.WinControls.UI.RadLabel radLabel29;
        private Telerik.WinControls.UI.RadLabel radLabel30;
        private Telerik.WinControls.UI.RadLabel radLabel31;
        private Telerik.WinControls.UI.RadLabel radLabel32;
        private Telerik.WinControls.UI.RadLabel radLabel33;
        private Telerik.WinControls.UI.RadLabel radLabel34;
        private Telerik.WinControls.UI.RadLabel radLabel35;
        private Telerik.WinControls.UI.RadLabel radLabel36;
        private Telerik.WinControls.UI.RadLabel radLabel37;
        private Telerik.WinControls.UI.RadTextBox radTextBox3;
        private Telerik.WinControls.UI.RadTextBox radTextBox4;
        private Telerik.WinControls.UI.RadTextBox radTextBox5;
        private Telerik.WinControls.UI.RadTextBox radTextBox6;
        private Telerik.WinControls.UI.RadTextBox radTextBox7;
        private Telerik.WinControls.UI.RadTextBox radTextBox8;
        private Telerik.WinControls.UI.RadTextBox radTextBox9;
        private Telerik.WinControls.UI.RadTextBox radTextBox10;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList5;
        private Telerik.WinControls.UI.RadLabel radLabel38;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker1;
        private System.Windows.Forms.Panel panel6;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton5;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton6;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox13;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox14;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox15;
        private System.Windows.Forms.TabPage tabPage2;
        private Telerik.WinControls.UI.RadCheckBox chk18;
        private Telerik.WinControls.UI.RadCheckBox chk3;
        private Telerik.WinControls.UI.RadCheckBox chk2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadTextBox txtKeterangan;
        private Telerik.WinControls.UI.RadTextBox txtNamaKasus;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadCheckBox chk17;
        private Telerik.WinControls.UI.RadCheckBox chk15;
        private Telerik.WinControls.UI.RadCheckBox chk14;
        private Telerik.WinControls.UI.RadCheckBox chk13;
        private Telerik.WinControls.UI.RadCheckBox chk12;
        private Telerik.WinControls.UI.RadCheckBox chk11;
        private Telerik.WinControls.UI.RadCheckBox chk9;
        private Telerik.WinControls.UI.RadCheckBox chk7;
        private Telerik.WinControls.UI.RadCheckBox chk6;
        private Telerik.WinControls.UI.RadCheckBox chk5;
        private Telerik.WinControls.UI.RadCheckBox chk19;
        private Telerik.WinControls.UI.RadCheckBox chk25;
        private System.Windows.Forms.Panel panel4;
        private Telerik.WinControls.UI.RadRadioButton rbIdentitas2;
        private Telerik.WinControls.UI.RadRadioButton rbIdentitas1;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadRadioButton rbPerempuan;
        private Telerik.WinControls.UI.RadRadioButton rbLaki;
        private Telerik.WinControls.UI.RadDropDownList ddlNegara;
        private Telerik.WinControls.UI.RadDropDownList ddlPekerjaan;
        private Telerik.WinControls.UI.RadDropDownList ddlPendidikan;
        private Telerik.WinControls.UI.RadDropDownList ddlAgama;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel25;
        private Telerik.WinControls.UI.RadLabel radLabel24;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel radLabel22;
        private Telerik.WinControls.UI.RadTextBox txtNoIdentitas;
        private Telerik.WinControls.UI.RadTextBox txtNama;
        private Telerik.WinControls.UI.RadTextBox txtTempatLahir;
        private Telerik.WinControls.UI.RadTextBox txtIbuKandung;
        private Telerik.WinControls.UI.RadTextBox txtAlias;
        private Telerik.WinControls.UI.RadTextBox txtNoTelp;
        private Telerik.WinControls.UI.RadTextBox txtEmail;
        private Telerik.WinControls.UI.RadTextBox txtAlamat;
        private Telerik.WinControls.UI.RadDropDownList ddlJnsIdentitas;
        private Telerik.WinControls.UI.RadLabel radLabel23;
        private Telerik.WinControls.UI.RadDateTimePicker ddlTglLahir;
        private Telerik.WinControls.UI.RadRadioButton rbTidakKawin;
        private Telerik.WinControls.UI.RadRadioButton rbKawin;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Telerik.WinControls.UI.RadDropDownList rad;
        private Telerik.WinControls.UI.RadLabel radLabel39;
        private Telerik.WinControls.UI.RadLabel radLabel40;
        private Telerik.WinControls.UI.RadLabel radLabel41;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker2;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker3;
        private Telerik.WinControls.UI.RadButton btnSebelumnya;
        private Telerik.WinControls.UI.RadButton btnSave;
    }
}
