﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListBonSerahTahanan : Base
    {
        public RadGridView GvBonSerahTahanan { get { return this.gvBonSerahTahanan; } }
        public ListBonSerahTahanan()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvBonSerahTahanan.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvBonSerahTahanan.RowFormatting += radGridView_RowFormatting;
            gvBonSerahTahanan.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvBonSerahTahanan.CellClick += gvBonSerahTahanan_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarBonSerahTahanan.pdf", "DAFTAR DATA BON SERAH TAHANAN", gvBonSerahTahanan);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarBonSerahTahanan.csv", "DAFTAR DATA BON SERAH TAHANAN", gvBonSerahTahanan);
        }

        private void gvBonSerahTahanan_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == null)
                return;
            switch (e.Column.Index)
            {
                case 0:
                    MainForm.formMain.BonSerahTahanan.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.AddNew;
                    MainForm.formMain.BonSerahTahanan.LoadData();
                    break;
                case 1:
                    var question  = UserFunction.Confirm("Hapus Bon Tahanan Ini?");
                    if (question == DialogResult.Yes)
                    {
                        DeleteBonSerahTahanan(e.Row.Cells["Id"].Value.ToString());
                        UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Dihapus!");
                        UserFunction.LoadDataBonSerahTahananToGrid(gvBonSerahTahanan);
                    }
                    break;
                case 2:
                    MainForm.formMain.PreviewDoc.Tag = "BonSerahTahanan-" + e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.PreviewDoc.Show();
                    this.Hide();
                    break;
            }
            
        }

        private void DeleteBonSerahTahanan(string Id)
        {
            using (BonSerahTahananService bonserv = new BonSerahTahananService())
            {
                var data = bonserv.GetById(Convert.ToInt32(Id));
                bonserv.DeleteById(Convert.ToInt32(Id));
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERVISITOR.ToString(), Activities = "Delete BonSerahTahanan, data=" + UserFunction.JsonString(data) });

                BonTahananService serv = new BonTahananService();
                var bon = new BonTahanan();
                bon.Id = Convert.ToInt32(Convert.ToInt32(data.IdBonTahanan));
                var databon = serv.GetById(bon.Id);
                bon.StatusTahanan = "Diminta";
                bon.TanggalMulai = databon.TanggalMulai;
                bon.TanggalSelesai = databon.TanggalSelesai;
                bon.UpdatedDate = DateTime.Now.ToLocalTime();
                bon.UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID);
                bon.UpdatedLocation = "0.0.0.0";
                serv.Update(bon);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Update BonTahanan, Data=" + UserFunction.JsonString(databon) });
            }
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DAFTAR BON SERAH TAHANAN";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;


            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Width = 15;
            btnDetail.Name = "btnDetail";
            gvBonSerahTahanan.AutoGenerateColumns = false;
            gvBonSerahTahanan.Columns.Insert(0, btnDetail);
            gvBonSerahTahanan.Refresh();
            gvBonSerahTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            GridViewImageColumn btnUbah = new GridViewImageColumn();
            btnUbah.HeaderText = "";
            btnUbah.Name = "btnUbah";
            gvBonSerahTahanan.AutoGenerateColumns = false;
            gvBonSerahTahanan.Columns.Insert(1, btnUbah);
            gvBonSerahTahanan.Refresh();
            gvBonSerahTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnHapus = new GridViewImageColumn();
            btnHapus.HeaderText = "";
            btnHapus.Name = "btnHapus";
            gvBonSerahTahanan.AutoGenerateColumns = false;
            gvBonSerahTahanan.Columns.Insert(1, btnHapus);
            gvBonSerahTahanan.Refresh();
            gvBonSerahTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnPrint = new GridViewImageColumn();
            btnPrint.HeaderText = "";
            btnPrint.Name = "btnPrint";
            gvBonSerahTahanan.AutoGenerateColumns = false;
            gvBonSerahTahanan.Columns.Insert(1, btnPrint);
            gvBonSerahTahanan.Refresh();
            gvBonSerahTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            UserFunction.LoadDataBonSerahTahananToGrid(gvBonSerahTahanan);
            UserFunction.SetInitGridView(gvBonSerahTahanan);

        }

    }
}
