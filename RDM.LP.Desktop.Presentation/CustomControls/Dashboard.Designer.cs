﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class Dashboard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.CartesianArea cartesianArea1 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CartesianArea cartesianArea2 = new Telerik.WinControls.UI.CartesianArea();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.PanelDashBoard = new Telerik.WinControls.UI.RadPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblStat2 = new Telerik.WinControls.UI.RadLabel();
            this.radChartVisitor = new Telerik.WinControls.UI.RadChartView();
            this.Info2 = new Telerik.WinControls.UI.RadLabel();
            this.lblVisitor = new Telerik.WinControls.UI.RadLabel();
            this.lblVisitorToday = new Telerik.WinControls.UI.RadLabel();
            this.lblTotVisitor = new Telerik.WinControls.UI.RadLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblStat1 = new Telerik.WinControls.UI.RadLabel();
            this.radChartInmates = new Telerik.WinControls.UI.RadChartView();
            this.Info1 = new Telerik.WinControls.UI.RadLabel();
            this.lblInmates = new Telerik.WinControls.UI.RadLabel();
            this.lblInmatesToday = new Telerik.WinControls.UI.RadLabel();
            this.lblTotInmates = new Telerik.WinControls.UI.RadLabel();
            this.Navigator = new System.Windows.Forms.TableLayoutPanel();
            this.next = new System.Windows.Forms.PictureBox();
            this.prev = new System.Windows.Forms.PictureBox();
            this.full = new System.Windows.Forms.PictureBox();
            this.PanelDashBoard2 = new Telerik.WinControls.UI.RadPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.WCM = new Telerik.WinControls.UI.RadLabel();
            this.radLabel28 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel29 = new Telerik.WinControls.UI.RadLabel();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.radLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel31 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel32 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel33 = new Telerik.WinControls.UI.RadLabel();
            this.WCA = new Telerik.WinControls.UI.RadLabel();
            this.WCO = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailC = new Telerik.WinControls.UI.RadLabel();
            this.WardC = new Telerik.WinControls.UI.RadLabel();
            this.TotalWardC = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.radLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel25 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel26 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel27 = new Telerik.WinControls.UI.RadLabel();
            this.WBA = new Telerik.WinControls.UI.RadLabel();
            this.WBO = new Telerik.WinControls.UI.RadLabel();
            this.WBM = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailB = new Telerik.WinControls.UI.RadLabel();
            this.WardB = new Telerik.WinControls.UI.RadLabel();
            this.TotalWardB = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.WAM = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.WAA = new Telerik.WinControls.UI.RadLabel();
            this.WAO = new Telerik.WinControls.UI.RadLabel();
            this.radLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailA = new Telerik.WinControls.UI.RadLabel();
            this.WardA = new Telerik.WinControls.UI.RadLabel();
            this.TotalWardA = new Telerik.WinControls.UI.RadLabel();
            this.timerDashboard = new System.Windows.Forms.Timer(this.components);
            this.panelDasboard = new Telerik.WinControls.UI.RadPanel();
            ((System.ComponentModel.ISupportInitialize)(this.PanelDashBoard)).BeginInit();
            this.PanelDashBoard.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblStat2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartVisitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Info2)).BeginInit();
            this.Info2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblVisitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVisitorToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotVisitor)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblStat1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartInmates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Info1)).BeginInit();
            this.Info1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblInmates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInmatesToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotInmates)).BeginInit();
            this.Navigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.next)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.full)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelDashBoard2)).BeginInit();
            this.PanelDashBoard2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            this.radLabel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            this.radLabel15.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WCM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WCA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WCO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WardC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalWardC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            this.radLabel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            this.radLabel14.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WBA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WBO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WBM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WardB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalWardB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            this.radLabel7.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WAM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WAA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WAO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WardA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalWardA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelDasboard)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelDashBoard
            // 
            this.PanelDashBoard.BackColor = System.Drawing.Color.Transparent;
            this.PanelDashBoard.Controls.Add(this.panel2);
            this.PanelDashBoard.Controls.Add(this.panel1);
            this.PanelDashBoard.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelDashBoard.Location = new System.Drawing.Point(0, 0);
            this.PanelDashBoard.Margin = new System.Windows.Forms.Padding(0);
            this.PanelDashBoard.Name = "PanelDashBoard";
            this.PanelDashBoard.Padding = new System.Windows.Forms.Padding(50);
            // 
            // 
            // 
            this.PanelDashBoard.RootElement.Opacity = 0D;
            this.PanelDashBoard.Size = new System.Drawing.Size(1239, 689);
            this.PanelDashBoard.TabIndex = 7;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard.GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard.GetChildAt(0))).ShadowColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard.GetChildAt(0))).RippleAnimationColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard.GetChildAt(0))).FocusBorderColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard.GetChildAt(0))).HighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard.GetChildAt(0))).BorderHighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard.GetChildAt(0))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(50);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard.GetChildAt(0).GetChildAt(1))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard.GetChildAt(0).GetChildAt(1))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard.GetChildAt(0).GetChildAt(1))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard.GetChildAt(0).GetChildAt(1))).InnerColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard.GetChildAt(0).GetChildAt(1))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard.GetChildAt(0).GetChildAt(1))).InnerColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard.GetChildAt(0).GetChildAt(1))).InnerColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(44)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblStat2);
            this.panel2.Controls.Add(this.radChartVisitor);
            this.panel2.Controls.Add(this.Info2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(50, 339);
            this.panel2.Margin = new System.Windows.Forms.Padding(30);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(30);
            this.panel2.Size = new System.Drawing.Size(1139, 300);
            this.panel2.TabIndex = 4;
            // 
            // lblStat2
            // 
            this.lblStat2.AutoSize = false;
            this.lblStat2.BackColor = System.Drawing.Color.White;
            this.lblStat2.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblStat2.Location = new System.Drawing.Point(397, 30);
            this.lblStat2.Name = "lblStat2";
            this.lblStat2.Size = new System.Drawing.Size(710, 33);
            this.lblStat2.TabIndex = 3;
            this.lblStat2.Text = "PERIODE STATISTIK DALAM 30 HARI TERAKHIR";
            this.lblStat2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radChartVisitor
            // 
            this.radChartVisitor.AreaDesign = cartesianArea1;
            this.radChartVisitor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radChartVisitor.Location = new System.Drawing.Point(397, 45);
            this.radChartVisitor.Name = "radChartVisitor";
            this.radChartVisitor.ShowGrid = false;
            this.radChartVisitor.Size = new System.Drawing.Size(710, 223);
            this.radChartVisitor.TabIndex = 2;
            // 
            // Info2
            // 
            this.Info2.AutoSize = false;
            this.Info2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(124)))), ((int)(((byte)(39)))));
            this.Info2.Controls.Add(this.lblVisitor);
            this.Info2.Controls.Add(this.lblVisitorToday);
            this.Info2.Controls.Add(this.lblTotVisitor);
            this.Info2.Dock = System.Windows.Forms.DockStyle.Left;
            this.Info2.ForeColor = System.Drawing.Color.White;
            this.Info2.Location = new System.Drawing.Point(30, 30);
            this.Info2.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.Info2.Name = "Info2";
            this.Info2.Padding = new System.Windows.Forms.Padding(15);
            this.Info2.Size = new System.Drawing.Size(367, 238);
            this.Info2.TabIndex = 0;
            this.Info2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadLabelElement)(this.Info2.GetChildAt(0))).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadLabelElement)(this.Info2.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(15);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.Info2.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(124)))), ((int)(((byte)(39)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.Info2.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(76)))), ((int)(((byte)(53)))));
            // 
            // lblVisitor
            // 
            this.lblVisitor.AutoSize = false;
            this.lblVisitor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblVisitor.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.lblVisitor.Location = new System.Drawing.Point(15, 164);
            this.lblVisitor.Name = "lblVisitor";
            this.lblVisitor.Size = new System.Drawing.Size(337, 29);
            this.lblVisitor.TabIndex = 6;
            this.lblVisitor.Text = "PENGUNJUNG TERDAFTAR";
            this.lblVisitor.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblVisitorToday
            // 
            this.lblVisitorToday.AutoSize = false;
            this.lblVisitorToday.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblVisitorToday.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.lblVisitorToday.Location = new System.Drawing.Point(15, 193);
            this.lblVisitorToday.Name = "lblVisitorToday";
            this.lblVisitorToday.Size = new System.Drawing.Size(337, 30);
            this.lblVisitorToday.TabIndex = 5;
            this.lblVisitorToday.Text = "HARI INI";
            this.lblVisitorToday.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTotVisitor
            // 
            this.lblTotVisitor.AutoSize = false;
            this.lblTotVisitor.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTotVisitor.ForeColor = System.Drawing.Color.White;
            this.lblTotVisitor.Location = new System.Drawing.Point(15, 15);
            this.lblTotVisitor.Name = "lblTotVisitor";
            this.lblTotVisitor.Size = new System.Drawing.Size(337, 143);
            this.lblTotVisitor.TabIndex = 4;
            this.lblTotVisitor.Text = "93";
            this.lblTotVisitor.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(44)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblStat1);
            this.panel1.Controls.Add(this.radChartInmates);
            this.panel1.Controls.Add(this.Info1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(50, 50);
            this.panel1.Margin = new System.Windows.Forms.Padding(30);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(30);
            this.panel1.Size = new System.Drawing.Size(1139, 300);
            this.panel1.TabIndex = 3;
            // 
            // lblStat1
            // 
            this.lblStat1.AutoSize = false;
            this.lblStat1.BackColor = System.Drawing.Color.White;
            this.lblStat1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblStat1.Location = new System.Drawing.Point(397, 30);
            this.lblStat1.Name = "lblStat1";
            this.lblStat1.Size = new System.Drawing.Size(710, 33);
            this.lblStat1.TabIndex = 2;
            this.lblStat1.Text = "PERIODE STATISTIK DALAM 30 HARI TERAKHIR";
            this.lblStat1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radChartInmates
            // 
            this.radChartInmates.AreaDesign = cartesianArea2;
            this.radChartInmates.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radChartInmates.Location = new System.Drawing.Point(397, 48);
            this.radChartInmates.Name = "radChartInmates";
            this.radChartInmates.ShowGrid = false;
            this.radChartInmates.Size = new System.Drawing.Size(710, 220);
            this.radChartInmates.TabIndex = 1;
            // 
            // Info1
            // 
            this.Info1.AutoSize = false;
            this.Info1.BackColor = System.Drawing.Color.Gray;
            this.Info1.Controls.Add(this.lblInmates);
            this.Info1.Controls.Add(this.lblInmatesToday);
            this.Info1.Controls.Add(this.lblTotInmates);
            this.Info1.Dock = System.Windows.Forms.DockStyle.Left;
            this.Info1.ForeColor = System.Drawing.Color.White;
            this.Info1.Location = new System.Drawing.Point(30, 30);
            this.Info1.Name = "Info1";
            this.Info1.Padding = new System.Windows.Forms.Padding(15);
            this.Info1.Size = new System.Drawing.Size(367, 238);
            this.Info1.TabIndex = 0;
            this.Info1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadLabelElement)(this.Info1.GetChildAt(0))).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadLabelElement)(this.Info1.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(15);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.Info1.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.Gray;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.Info1.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            // 
            // lblInmates
            // 
            this.lblInmates.AutoSize = false;
            this.lblInmates.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblInmates.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.lblInmates.Location = new System.Drawing.Point(15, 164);
            this.lblInmates.Name = "lblInmates";
            this.lblInmates.Size = new System.Drawing.Size(337, 29);
            this.lblInmates.TabIndex = 3;
            this.lblInmates.Text = "TAHANAN TERDAFTAR";
            this.lblInmates.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInmatesToday
            // 
            this.lblInmatesToday.AutoSize = false;
            this.lblInmatesToday.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblInmatesToday.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.lblInmatesToday.Location = new System.Drawing.Point(15, 193);
            this.lblInmatesToday.Name = "lblInmatesToday";
            this.lblInmatesToday.Size = new System.Drawing.Size(337, 30);
            this.lblInmatesToday.TabIndex = 2;
            this.lblInmatesToday.Text = "HARI INI";
            this.lblInmatesToday.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTotInmates
            // 
            this.lblTotInmates.AutoSize = false;
            this.lblTotInmates.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTotInmates.ForeColor = System.Drawing.Color.White;
            this.lblTotInmates.Location = new System.Drawing.Point(15, 15);
            this.lblTotInmates.Name = "lblTotInmates";
            this.lblTotInmates.Size = new System.Drawing.Size(337, 143);
            this.lblTotInmates.TabIndex = 1;
            this.lblTotInmates.Text = "11";
            this.lblTotInmates.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Navigator
            // 
            this.Navigator.ColumnCount = 3;
            this.Navigator.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.Navigator.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.Navigator.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.Navigator.Controls.Add(this.next, 0, 0);
            this.Navigator.Controls.Add(this.prev, 0, 0);
            this.Navigator.Controls.Add(this.full, 1, 0);
            this.Navigator.Location = new System.Drawing.Point(0, 0);
            this.Navigator.Name = "Navigator";
            this.Navigator.RowCount = 1;
            this.Navigator.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Navigator.Size = new System.Drawing.Size(148, 47);
            this.Navigator.TabIndex = 12;
            // 
            // next
            // 
            this.next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.next.Dock = System.Windows.Forms.DockStyle.Right;
            this.next.Image = ((System.Drawing.Image)(resources.GetObject("next.Image")));
            this.next.Location = new System.Drawing.Point(55, 3);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(40, 41);
            this.next.TabIndex = 12;
            this.next.TabStop = false;
            // 
            // prev
            // 
            this.prev.Cursor = System.Windows.Forms.Cursors.Hand;
            this.prev.Dock = System.Windows.Forms.DockStyle.Left;
            this.prev.Image = ((System.Drawing.Image)(resources.GetObject("prev.Image")));
            this.prev.Location = new System.Drawing.Point(3, 3);
            this.prev.Name = "prev";
            this.prev.Size = new System.Drawing.Size(40, 41);
            this.prev.TabIndex = 11;
            this.prev.TabStop = false;
            // 
            // full
            // 
            this.full.Cursor = System.Windows.Forms.Cursors.Hand;
            this.full.Dock = System.Windows.Forms.DockStyle.Right;
            this.full.Image = ((System.Drawing.Image)(resources.GetObject("full.Image")));
            this.full.Location = new System.Drawing.Point(105, 3);
            this.full.Name = "full";
            this.full.Size = new System.Drawing.Size(40, 41);
            this.full.TabIndex = 10;
            this.full.TabStop = false;
            // 
            // PanelDashBoard2
            // 
            this.PanelDashBoard2.BackColor = System.Drawing.Color.Transparent;
            this.PanelDashBoard2.Controls.Add(this.panel4);
            this.PanelDashBoard2.Location = new System.Drawing.Point(0, 0);
            this.PanelDashBoard2.Margin = new System.Windows.Forms.Padding(0);
            this.PanelDashBoard2.Name = "PanelDashBoard2";
            this.PanelDashBoard2.Padding = new System.Windows.Forms.Padding(20, 50, 20, 20);
            // 
            // 
            // 
            this.PanelDashBoard2.RootElement.Opacity = 0D;
            this.PanelDashBoard2.Size = new System.Drawing.Size(1239, 689);
            this.PanelDashBoard2.TabIndex = 13;
            this.PanelDashBoard2.Visible = false;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard2.GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard2.GetChildAt(0))).ShadowColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard2.GetChildAt(0))).RippleAnimationColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard2.GetChildAt(0))).FocusBorderColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard2.GetChildAt(0))).HighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard2.GetChildAt(0))).BorderHighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard2.GetChildAt(0))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelDashBoard2.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(20, 50, 20, 20);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard2.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard2.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard2.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard2.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard2.GetChildAt(0).GetChildAt(1))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard2.GetChildAt(0).GetChildAt(1))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard2.GetChildAt(0).GetChildAt(1))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard2.GetChildAt(0).GetChildAt(1))).InnerColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard2.GetChildAt(0).GetChildAt(1))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard2.GetChildAt(0).GetChildAt(1))).InnerColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard2.GetChildAt(0).GetChildAt(1))).InnerColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard2.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelDashBoard2.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(44)))));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.tableLayoutPanel2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(20, 50);
            this.panel4.Margin = new System.Windows.Forms.Padding(10);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(30);
            this.panel4.Size = new System.Drawing.Size(1199, 619);
            this.panel4.TabIndex = 3;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabel5, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabel7, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(30, 30);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1137, 557);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.BackColor = System.Drawing.Color.Transparent;
            this.radLabel1.Controls.Add(this.radLabel15);
            this.radLabel1.Controls.Add(this.lblDetailC);
            this.radLabel1.Controls.Add(this.WardC);
            this.radLabel1.Controls.Add(this.TotalWardC);
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.ForeColor = System.Drawing.Color.White;
            this.radLabel1.Location = new System.Drawing.Point(761, 3);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Padding = new System.Windows.Forms.Padding(5);
            this.radLabel1.Size = new System.Drawing.Size(373, 551);
            this.radLabel1.TabIndex = 3;
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel15
            // 
            this.radLabel15.AutoSize = false;
            this.radLabel15.Controls.Add(this.tableLayoutPanel4);
            this.radLabel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel15.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel15.Location = new System.Drawing.Point(5, 392);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(363, 164);
            this.radLabel15.TabIndex = 5;
            this.radLabel15.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.Controls.Add(this.WCM, 3, 2);
            this.tableLayoutPanel4.Controls.Add(this.radLabel28, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.radLabel29, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.pictureBox7, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.pictureBox8, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.pictureBox9, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.radLabel30, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.radLabel31, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.radLabel32, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.radLabel33, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.WCA, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.WCO, 3, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(363, 164);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // WCM
            // 
            this.WCM.AutoSize = false;
            this.WCM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WCM.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.WCM.Location = new System.Drawing.Point(266, 111);
            this.WCM.Name = "WCM";
            this.WCM.Size = new System.Drawing.Size(94, 50);
            this.WCM.TabIndex = 15;
            this.WCM.Text = "0";
            this.WCM.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radLabel28
            // 
            this.radLabel28.AutoSize = false;
            this.radLabel28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel28.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel28.Location = new System.Drawing.Point(216, 3);
            this.radLabel28.Name = "radLabel28";
            this.radLabel28.Size = new System.Drawing.Size(44, 48);
            this.radLabel28.TabIndex = 7;
            this.radLabel28.Text = ":";
            this.radLabel28.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel29
            // 
            this.radLabel29.AutoSize = false;
            this.radLabel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel29.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel29.Location = new System.Drawing.Point(63, 3);
            this.radLabel29.Name = "radLabel29";
            this.radLabel29.Size = new System.Drawing.Size(147, 48);
            this.radLabel29.TabIndex = 4;
            this.radLabel29.Text = "TERSEDIA";
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox7.Location = new System.Drawing.Point(10, 10);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(10);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(40, 34);
            this.pictureBox7.TabIndex = 0;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Red;
            this.pictureBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox8.Location = new System.Drawing.Point(10, 64);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(10);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(40, 34);
            this.pictureBox8.TabIndex = 1;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Yellow;
            this.pictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox9.Location = new System.Drawing.Point(10, 118);
            this.pictureBox9.Margin = new System.Windows.Forms.Padding(10);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(40, 36);
            this.pictureBox9.TabIndex = 2;
            this.pictureBox9.TabStop = false;
            // 
            // radLabel30
            // 
            this.radLabel30.AutoSize = false;
            this.radLabel30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel30.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel30.Location = new System.Drawing.Point(63, 57);
            this.radLabel30.Name = "radLabel30";
            this.radLabel30.Size = new System.Drawing.Size(147, 48);
            this.radLabel30.TabIndex = 5;
            this.radLabel30.Text = "DITEMPATI";
            // 
            // radLabel31
            // 
            this.radLabel31.AutoSize = false;
            this.radLabel31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel31.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel31.Location = new System.Drawing.Point(63, 111);
            this.radLabel31.Name = "radLabel31";
            this.radLabel31.Size = new System.Drawing.Size(147, 50);
            this.radLabel31.TabIndex = 6;
            this.radLabel31.Text = "DALAM PEMELIHARAAN";
            // 
            // radLabel32
            // 
            this.radLabel32.AutoSize = false;
            this.radLabel32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel32.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel32.Location = new System.Drawing.Point(216, 57);
            this.radLabel32.Name = "radLabel32";
            this.radLabel32.Size = new System.Drawing.Size(44, 48);
            this.radLabel32.TabIndex = 8;
            this.radLabel32.Text = ":";
            this.radLabel32.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel33
            // 
            this.radLabel33.AutoSize = false;
            this.radLabel33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel33.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel33.Location = new System.Drawing.Point(216, 111);
            this.radLabel33.Name = "radLabel33";
            this.radLabel33.Size = new System.Drawing.Size(44, 50);
            this.radLabel33.TabIndex = 9;
            this.radLabel33.Text = ":";
            this.radLabel33.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WCA
            // 
            this.WCA.AutoSize = false;
            this.WCA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WCA.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.WCA.Location = new System.Drawing.Point(266, 3);
            this.WCA.Name = "WCA";
            this.WCA.Size = new System.Drawing.Size(94, 48);
            this.WCA.TabIndex = 13;
            this.WCA.Text = "0";
            this.WCA.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // WCO
            // 
            this.WCO.AutoSize = false;
            this.WCO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WCO.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.WCO.Location = new System.Drawing.Point(266, 57);
            this.WCO.Name = "WCO";
            this.WCO.Size = new System.Drawing.Size(94, 48);
            this.WCO.TabIndex = 12;
            this.WCO.Text = "0";
            this.WCO.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDetailC
            // 
            this.lblDetailC.AutoSize = false;
            this.lblDetailC.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailC.ForeColor = System.Drawing.Color.White;
            this.lblDetailC.Location = new System.Drawing.Point(5, 177);
            this.lblDetailC.Name = "lblDetailC";
            this.lblDetailC.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.lblDetailC.Size = new System.Drawing.Size(363, 215);
            this.lblDetailC.TabIndex = 4;
            this.lblDetailC.Text = "HARI INI";
            this.lblDetailC.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // WardC
            // 
            this.WardC.AutoSize = false;
            this.WardC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(124)))), ((int)(((byte)(39)))));
            this.WardC.Dock = System.Windows.Forms.DockStyle.Top;
            this.WardC.Enabled = false;
            this.WardC.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.WardC.Location = new System.Drawing.Point(5, 142);
            this.WardC.Margin = new System.Windows.Forms.Padding(3, 3, 3, 8);
            this.WardC.Name = "WardC";
            this.WardC.Size = new System.Drawing.Size(363, 35);
            this.WardC.TabIndex = 3;
            this.WardC.Text = "WARD C";
            this.WardC.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // TotalWardC
            // 
            this.TotalWardC.AutoSize = false;
            this.TotalWardC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(124)))), ((int)(((byte)(39)))));
            this.TotalWardC.Dock = System.Windows.Forms.DockStyle.Top;
            this.TotalWardC.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.TotalWardC.Location = new System.Drawing.Point(5, 5);
            this.TotalWardC.Name = "TotalWardC";
            this.TotalWardC.Size = new System.Drawing.Size(363, 137);
            this.TotalWardC.TabIndex = 1;
            this.TotalWardC.Text = "0";
            this.TotalWardC.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadLabelElement)(this.TotalWardC.GetChildAt(0))).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadLabelElement)(this.TotalWardC.GetChildAt(0))).Text = "0";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.TotalWardC.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(124)))), ((int)(((byte)(39)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.TotalWardC.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(76)))), ((int)(((byte)(53)))));
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.BackColor = System.Drawing.Color.Transparent;
            this.radLabel5.Controls.Add(this.radLabel14);
            this.radLabel5.Controls.Add(this.lblDetailB);
            this.radLabel5.Controls.Add(this.WardB);
            this.radLabel5.Controls.Add(this.TotalWardB);
            this.radLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel5.ForeColor = System.Drawing.Color.White;
            this.radLabel5.Location = new System.Drawing.Point(382, 3);
            this.radLabel5.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Padding = new System.Windows.Forms.Padding(5);
            this.radLabel5.Size = new System.Drawing.Size(351, 551);
            this.radLabel5.TabIndex = 2;
            this.radLabel5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel14
            // 
            this.radLabel14.AutoSize = false;
            this.radLabel14.Controls.Add(this.tableLayoutPanel3);
            this.radLabel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel14.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel14.Location = new System.Drawing.Point(5, 392);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(341, 164);
            this.radLabel14.TabIndex = 5;
            this.radLabel14.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.Controls.Add(this.radLabel22, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.radLabel23, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.pictureBox4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.pictureBox5, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.pictureBox6, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.radLabel24, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.radLabel25, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.radLabel26, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.radLabel27, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.WBA, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.WBO, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.WBM, 3, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(341, 164);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // radLabel22
            // 
            this.radLabel22.AutoSize = false;
            this.radLabel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel22.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel22.Location = new System.Drawing.Point(194, 3);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(44, 48);
            this.radLabel22.TabIndex = 7;
            this.radLabel22.Text = ":";
            this.radLabel22.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel23
            // 
            this.radLabel23.AutoSize = false;
            this.radLabel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel23.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel23.Location = new System.Drawing.Point(63, 3);
            this.radLabel23.Name = "radLabel23";
            this.radLabel23.Size = new System.Drawing.Size(125, 48);
            this.radLabel23.TabIndex = 4;
            this.radLabel23.Text = "TERSEDIA";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox4.Location = new System.Drawing.Point(10, 10);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(10);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(40, 34);
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Red;
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox5.Location = new System.Drawing.Point(10, 64);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(10);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(40, 34);
            this.pictureBox5.TabIndex = 1;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Yellow;
            this.pictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox6.Location = new System.Drawing.Point(10, 118);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(10);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(40, 36);
            this.pictureBox6.TabIndex = 2;
            this.pictureBox6.TabStop = false;
            // 
            // radLabel24
            // 
            this.radLabel24.AutoSize = false;
            this.radLabel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel24.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel24.Location = new System.Drawing.Point(63, 57);
            this.radLabel24.Name = "radLabel24";
            this.radLabel24.Size = new System.Drawing.Size(125, 48);
            this.radLabel24.TabIndex = 5;
            this.radLabel24.Text = "DITEMPATI";
            // 
            // radLabel25
            // 
            this.radLabel25.AutoSize = false;
            this.radLabel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel25.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel25.Location = new System.Drawing.Point(63, 111);
            this.radLabel25.Name = "radLabel25";
            this.radLabel25.Size = new System.Drawing.Size(125, 50);
            this.radLabel25.TabIndex = 6;
            this.radLabel25.Text = "DALAM PEMELIHARAAN";
            // 
            // radLabel26
            // 
            this.radLabel26.AutoSize = false;
            this.radLabel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel26.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel26.Location = new System.Drawing.Point(194, 57);
            this.radLabel26.Name = "radLabel26";
            this.radLabel26.Size = new System.Drawing.Size(44, 48);
            this.radLabel26.TabIndex = 8;
            this.radLabel26.Text = ":";
            this.radLabel26.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel27
            // 
            this.radLabel27.AutoSize = false;
            this.radLabel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel27.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel27.Location = new System.Drawing.Point(194, 111);
            this.radLabel27.Name = "radLabel27";
            this.radLabel27.Size = new System.Drawing.Size(44, 50);
            this.radLabel27.TabIndex = 9;
            this.radLabel27.Text = ":";
            this.radLabel27.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WBA
            // 
            this.WBA.AutoSize = false;
            this.WBA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WBA.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.WBA.Location = new System.Drawing.Point(244, 3);
            this.WBA.Name = "WBA";
            this.WBA.Size = new System.Drawing.Size(94, 48);
            this.WBA.TabIndex = 11;
            this.WBA.Text = "0";
            this.WBA.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // WBO
            // 
            this.WBO.AutoSize = false;
            this.WBO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WBO.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.WBO.Location = new System.Drawing.Point(244, 57);
            this.WBO.Name = "WBO";
            this.WBO.Size = new System.Drawing.Size(94, 48);
            this.WBO.TabIndex = 12;
            this.WBO.Text = "0";
            this.WBO.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // WBM
            // 
            this.WBM.AutoSize = false;
            this.WBM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WBM.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.WBM.Location = new System.Drawing.Point(244, 111);
            this.WBM.Name = "WBM";
            this.WBM.Size = new System.Drawing.Size(94, 50);
            this.WBM.TabIndex = 15;
            this.WBM.Text = "0";
            this.WBM.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDetailB
            // 
            this.lblDetailB.AutoSize = false;
            this.lblDetailB.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailB.ForeColor = System.Drawing.Color.White;
            this.lblDetailB.Location = new System.Drawing.Point(5, 177);
            this.lblDetailB.Name = "lblDetailB";
            this.lblDetailB.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.lblDetailB.Size = new System.Drawing.Size(341, 215);
            this.lblDetailB.TabIndex = 4;
            this.lblDetailB.Text = "HARI INI";
            this.lblDetailB.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // WardB
            // 
            this.WardB.AutoSize = false;
            this.WardB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(124)))), ((int)(((byte)(39)))));
            this.WardB.Dock = System.Windows.Forms.DockStyle.Top;
            this.WardB.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.WardB.Location = new System.Drawing.Point(5, 142);
            this.WardB.Margin = new System.Windows.Forms.Padding(3, 3, 3, 8);
            this.WardB.Name = "WardB";
            this.WardB.Size = new System.Drawing.Size(341, 35);
            this.WardB.TabIndex = 3;
            this.WardB.Text = "WARD B";
            this.WardB.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // TotalWardB
            // 
            this.TotalWardB.AutoSize = false;
            this.TotalWardB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(124)))), ((int)(((byte)(39)))));
            this.TotalWardB.Dock = System.Windows.Forms.DockStyle.Top;
            this.TotalWardB.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.TotalWardB.Location = new System.Drawing.Point(5, 5);
            this.TotalWardB.Name = "TotalWardB";
            this.TotalWardB.Size = new System.Drawing.Size(341, 137);
            this.TotalWardB.TabIndex = 1;
            this.TotalWardB.Text = "0";
            this.TotalWardB.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadLabelElement)(this.TotalWardB.GetChildAt(0))).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadLabelElement)(this.TotalWardB.GetChildAt(0))).Text = "0";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.TotalWardB.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(124)))), ((int)(((byte)(39)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.TotalWardB.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(76)))), ((int)(((byte)(53)))));
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.BackColor = System.Drawing.Color.Transparent;
            this.radLabel7.Controls.Add(this.tableLayoutPanel1);
            this.radLabel7.Controls.Add(this.lblDetailA);
            this.radLabel7.Controls.Add(this.WardA);
            this.radLabel7.Controls.Add(this.TotalWardA);
            this.radLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel7.ForeColor = System.Drawing.Color.White;
            this.radLabel7.Location = new System.Drawing.Point(3, 3);
            this.radLabel7.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Padding = new System.Windows.Forms.Padding(5);
            this.radLabel7.Size = new System.Drawing.Size(351, 551);
            this.radLabel7.TabIndex = 1;
            this.radLabel7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadLabelElement)(this.radLabel7.GetChildAt(0))).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadLabelElement)(this.radLabel7.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(5);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radLabel7.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radLabel7.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radLabel7.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(31)))), ((int)(((byte)(13)))), ((int)(((byte)(13)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radLabel7.GetChildAt(0).GetChildAt(0))).GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Controls.Add(this.WAM, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.radLabel19, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.radLabel16, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.radLabel17, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.radLabel18, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.radLabel20, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.WAA, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.WAO, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.radLabel21, 2, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 382);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(341, 164);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // WAM
            // 
            this.WAM.AutoSize = false;
            this.WAM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WAM.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.WAM.Location = new System.Drawing.Point(244, 111);
            this.WAM.Name = "WAM";
            this.WAM.Size = new System.Drawing.Size(94, 50);
            this.WAM.TabIndex = 11;
            this.WAM.Text = "0";
            this.WAM.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radLabel19
            // 
            this.radLabel19.AutoSize = false;
            this.radLabel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel19.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel19.Location = new System.Drawing.Point(194, 3);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(44, 48);
            this.radLabel19.TabIndex = 7;
            this.radLabel19.Text = ":";
            this.radLabel19.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel16
            // 
            this.radLabel16.AutoSize = false;
            this.radLabel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel16.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel16.Location = new System.Drawing.Point(63, 3);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(125, 48);
            this.radLabel16.TabIndex = 4;
            this.radLabel16.Text = "TERSEDIA";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(10, 10);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 34);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Red;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Location = new System.Drawing.Point(10, 64);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 34);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Yellow;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox3.Location = new System.Drawing.Point(10, 118);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(10);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(40, 36);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // radLabel17
            // 
            this.radLabel17.AutoSize = false;
            this.radLabel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel17.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel17.Location = new System.Drawing.Point(63, 57);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(125, 48);
            this.radLabel17.TabIndex = 5;
            this.radLabel17.Text = "DITEMPATI";
            // 
            // radLabel18
            // 
            this.radLabel18.AutoSize = false;
            this.radLabel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel18.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel18.Location = new System.Drawing.Point(63, 111);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(125, 50);
            this.radLabel18.TabIndex = 6;
            this.radLabel18.Text = "DALAM PEMELIHARAAN";
            // 
            // radLabel20
            // 
            this.radLabel20.AutoSize = false;
            this.radLabel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel20.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel20.Location = new System.Drawing.Point(194, 57);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(44, 48);
            this.radLabel20.TabIndex = 8;
            this.radLabel20.Text = ":";
            this.radLabel20.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WAA
            // 
            this.WAA.AutoSize = false;
            this.WAA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WAA.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.WAA.Location = new System.Drawing.Point(244, 3);
            this.WAA.Name = "WAA";
            this.WAA.Size = new System.Drawing.Size(94, 48);
            this.WAA.TabIndex = 10;
            this.WAA.Text = "0";
            this.WAA.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // WAO
            // 
            this.WAO.AutoSize = false;
            this.WAO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WAO.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.WAO.Location = new System.Drawing.Point(244, 57);
            this.WAO.Name = "WAO";
            this.WAO.Size = new System.Drawing.Size(94, 48);
            this.WAO.TabIndex = 12;
            this.WAO.Text = "0";
            this.WAO.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radLabel21
            // 
            this.radLabel21.AutoSize = false;
            this.radLabel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel21.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel21.Location = new System.Drawing.Point(194, 111);
            this.radLabel21.Name = "radLabel21";
            this.radLabel21.Size = new System.Drawing.Size(44, 50);
            this.radLabel21.TabIndex = 9;
            this.radLabel21.Text = ":";
            this.radLabel21.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDetailA
            // 
            this.lblDetailA.AutoSize = false;
            this.lblDetailA.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailA.ForeColor = System.Drawing.Color.White;
            this.lblDetailA.Location = new System.Drawing.Point(5, 177);
            this.lblDetailA.Name = "lblDetailA";
            this.lblDetailA.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.lblDetailA.Size = new System.Drawing.Size(341, 215);
            this.lblDetailA.TabIndex = 5;
            this.lblDetailA.Text = "TAHANAN TERDAFTAR";
            this.lblDetailA.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // WardA
            // 
            this.WardA.AutoSize = false;
            this.WardA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(124)))), ((int)(((byte)(39)))));
            this.WardA.Dock = System.Windows.Forms.DockStyle.Top;
            this.WardA.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.WardA.Location = new System.Drawing.Point(5, 142);
            this.WardA.Margin = new System.Windows.Forms.Padding(3, 3, 3, 8);
            this.WardA.Name = "WardA";
            this.WardA.Size = new System.Drawing.Size(341, 35);
            this.WardA.TabIndex = 2;
            this.WardA.Text = "WARD A";
            this.WardA.TextAlignment = System.Drawing.ContentAlignment.TopCenter;
            // 
            // TotalWardA
            // 
            this.TotalWardA.AutoSize = false;
            this.TotalWardA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(124)))), ((int)(((byte)(39)))));
            this.TotalWardA.Dock = System.Windows.Forms.DockStyle.Top;
            this.TotalWardA.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.TotalWardA.Location = new System.Drawing.Point(5, 5);
            this.TotalWardA.Name = "TotalWardA";
            this.TotalWardA.Size = new System.Drawing.Size(341, 137);
            this.TotalWardA.TabIndex = 1;
            this.TotalWardA.Text = "0";
            this.TotalWardA.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadLabelElement)(this.TotalWardA.GetChildAt(0))).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadLabelElement)(this.TotalWardA.GetChildAt(0))).Text = "0";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.TotalWardA.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(124)))), ((int)(((byte)(39)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.TotalWardA.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(76)))), ((int)(((byte)(53)))));
            // 
            // timerDashboard
            // 
            this.timerDashboard.Enabled = true;
            this.timerDashboard.Interval = 20000;
            // 
            // panelDasboard
            // 
            this.panelDasboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.panelDasboard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDasboard.Location = new System.Drawing.Point(0, 689);
            this.panelDasboard.Margin = new System.Windows.Forms.Padding(0);
            this.panelDasboard.Name = "panelDasboard";
            this.panelDasboard.Size = new System.Drawing.Size(1239, 0);
            this.panelDasboard.TabIndex = 17;
            this.panelDasboard.Visible = false;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panelDasboard.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panelDasboard.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Controls.Add(this.panelDasboard);
            this.Controls.Add(this.Navigator);
            this.Controls.Add(this.PanelDashBoard2);
            this.Controls.Add(this.PanelDashBoard);
            this.Name = "Dashboard";
            this.Size = new System.Drawing.Size(1239, 689);
            ((System.ComponentModel.ISupportInitialize)(this.PanelDashBoard)).EndInit();
            this.PanelDashBoard.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblStat2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartVisitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Info2)).EndInit();
            this.Info2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblVisitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVisitorToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotVisitor)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblStat1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartInmates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Info1)).EndInit();
            this.Info1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblInmates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInmatesToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotInmates)).EndInit();
            this.Navigator.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.next)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.full)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelDashBoard2)).EndInit();
            this.PanelDashBoard2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            this.radLabel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            this.radLabel15.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WCM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WCA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WCO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WardC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalWardC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            this.radLabel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            this.radLabel14.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WBA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WBO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WBM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WardB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalWardB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            this.radLabel7.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WAM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WAA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WAO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WardA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalWardA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelDasboard)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel PanelDashBoard;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadLabel Info2;
        private Telerik.WinControls.UI.RadLabel Info1;
        private Telerik.WinControls.UI.RadLabel lblTotInmates;
        private Telerik.WinControls.UI.RadLabel lblInmates;
        private Telerik.WinControls.UI.RadLabel lblInmatesToday;
        private Telerik.WinControls.UI.RadLabel lblVisitor;
        private Telerik.WinControls.UI.RadLabel lblVisitorToday;
        private Telerik.WinControls.UI.RadLabel lblTotVisitor;
        private Telerik.WinControls.UI.RadChartView radChartInmates;
        private Telerik.WinControls.UI.RadLabel lblStat1;
        private System.Windows.Forms.TableLayoutPanel Navigator;
        private System.Windows.Forms.PictureBox prev;
        private System.Windows.Forms.PictureBox full;
        private Telerik.WinControls.UI.RadLabel lblStat2;
        private Telerik.WinControls.UI.RadChartView radChartVisitor;
        private Telerik.WinControls.UI.RadPanel PanelDashBoard2;
        private System.Windows.Forms.Panel panel4;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private Telerik.WinControls.UI.RadLabel radLabel28;
        private Telerik.WinControls.UI.RadLabel radLabel29;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private Telerik.WinControls.UI.RadLabel radLabel30;
        private Telerik.WinControls.UI.RadLabel radLabel31;
        private Telerik.WinControls.UI.RadLabel radLabel32;
        private Telerik.WinControls.UI.RadLabel radLabel33;
        private Telerik.WinControls.UI.RadLabel lblDetailC;
        private Telerik.WinControls.UI.RadLabel WardC;
        private Telerik.WinControls.UI.RadLabel TotalWardC;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Telerik.WinControls.UI.RadLabel radLabel22;
        private Telerik.WinControls.UI.RadLabel radLabel23;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private Telerik.WinControls.UI.RadLabel radLabel24;
        private Telerik.WinControls.UI.RadLabel radLabel25;
        private Telerik.WinControls.UI.RadLabel radLabel26;
        private Telerik.WinControls.UI.RadLabel radLabel27;
        private Telerik.WinControls.UI.RadLabel lblDetailB;
        private Telerik.WinControls.UI.RadLabel WardB;
        private Telerik.WinControls.UI.RadLabel TotalWardB;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel WardA;
        private Telerik.WinControls.UI.RadLabel TotalWardA;
        private Telerik.WinControls.UI.RadLabel WCM;
        private Telerik.WinControls.UI.RadLabel WCA;
        private Telerik.WinControls.UI.RadLabel WCO;
        private Telerik.WinControls.UI.RadLabel WBA;
        private Telerik.WinControls.UI.RadLabel WBO;
        private Telerik.WinControls.UI.RadLabel WBM;
        private System.Windows.Forms.Timer timerDashboard;
        private System.Windows.Forms.PictureBox next;
        private Telerik.WinControls.UI.RadPanel panelDasboard;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel WAM;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadLabel WAA;
        private Telerik.WinControls.UI.RadLabel WAO;
        private Telerik.WinControls.UI.RadLabel radLabel21;
        private Telerik.WinControls.UI.RadLabel lblDetailA;
    }
}
