﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListUser : Base
    {
        public RadGridView GvUser { get { return this.gvUser; } }
        public ListUser()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvUser.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvUser.RowFormatting += radGridView_RowFormatting;
            gvUser.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvUser.CellClick += gvUser_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
            picRefresh.Click += PicRefresh_Click;
        }

        private void PicRefresh_Click(object sender, EventArgs e)
        {
            UserFunction.LoadDataUserToGrid(gvUser);
            gvUser.Refresh();
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarPengguna.pdf", "USER LIST DATA", gvUser);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarPengguna.csv", "USER LIST DATA", gvUser);
        }

        private void gvUser_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == null)
                return;

            switch (e.Column.Index)
            {
                case 0:
                    MainForm.formMain.UserDetail.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.UserDetail.Show();
                    break;
                case 1:
                    MainForm.formMain.User.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.AddNew;
                    MainForm.formMain.User.LoadData();
                    break;
                case 2:
                    var question  = UserFunction.Confirm("Hapus pengguna ?");
                    if (question == DialogResult.Yes)
                    { 
                        DeleteUser(e.Row.Cells["Username"].Value.ToString());
                        UserFunction.MsgBox(TipeMsg.Info, "Pengguna telah dihapus !");
                        UserFunction.LoadDataUserToGrid(gvUser);
                    }
                    break;
            }
            
        }

        private void DeleteUser(string userName)
        {
            using (AppUserService user = new AppUserService())
            {
                var data = user.GetByUserName(userName);
                user.DeleteByUserName(userName);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERUSER.ToString(), Activities = "Delete User, Data =" + UserFunction.JsonString(data) });
            }
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DAFTAR PENGGUNA";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;
            this.lblDetail.BackColor = Color.FromArgb(77,77,77);
            this.lblDetail.ForeColor = Color.White;

            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Width = 15;
            btnDetail.Name = "btnDetail";
            gvUser.AutoGenerateColumns = false;
            gvUser.Columns.Insert(0, btnDetail);
            gvUser.Refresh();
            gvUser.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            GridViewImageColumn btnUbah = new GridViewImageColumn();
            btnUbah.HeaderText = "";
            btnUbah.Name = "btnUbah";
            gvUser.AutoGenerateColumns = false;
            gvUser.Columns.Insert(1, btnUbah);
            gvUser.Refresh();
            gvUser.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnHapus = new GridViewImageColumn();
            btnHapus.HeaderText = "";
            btnHapus.Name = "btnHapus";
            gvUser.AutoGenerateColumns = false;
            gvUser.Columns.Insert(2, btnHapus);
            gvUser.Refresh();
            gvUser.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            UserFunction.LoadDataUserToGrid(gvUser);
            UserFunction.SetInitGridView(gvUser);

        }

        private void gvUser_Click(object sender, EventArgs e)
        {

        }

        private void lblDetail_Click(object sender, EventArgs e)
        {

        }

        private void lblTitle__Click(object sender, EventArgs e)
        {

        }
    }
}
