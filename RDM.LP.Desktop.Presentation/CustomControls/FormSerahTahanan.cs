﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.Desktop.Presentation;
using AForge.Video;
using AForge.Video.DirectShow;
using Telerik.WinControls.Primitives;
using System.IO;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormSerahTahanan : Base
    {
        private BonSerahTahanan OldData;

        public FormSerahTahanan()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
            LoadData();
        }

        public void LoadData()
        {
            this.lblTitle.Text = "     TAMBAH FORM SERAH TAHANAN";

            ClearForm();
            
            if (this.Tag != null)
            {
                this.lblTitle.Text = "     UBAH FORM SERAH TAHANAN";
                MainForm.formMain.AddNew.Text = "UBAH FORM SERAH TAHANAN";

                BonSerahTahananService serv = new BonSerahTahananService();
                InmateBonTahananService iboserv = new InmateBonTahananService();
                var data = serv.GetById(Convert.ToInt32(this.Tag));
                var datainmate = iboserv.GetByBonTahananId(data.IdBonTahanan);

                if (data == null)
                {
                    ddlPangkatYangMenerima.SelectedIndex = 0;
                    ddlPangkatYangMenyerahkan.SelectedIndex = 0;
                    ddlPangkatYangMenyetujui.SelectedIndex = 0;
                    return;
                }
                else
                {
                    OldData = data;

                    ddlBonTahanan.SelectedIndex = Convert.ToInt32(data.IdBonTahanan);
                    txtNamaTahanan.Items.Clear();
                    txtNoCell.Items.Clear();
                    ListViewItem lv = new ListViewItem();
                    foreach (var item in datainmate)
                    {
                        lv.Text = item.RegID + '-' + item.FullName;
                        lv.Tag = item.RegID;
                        txtNamaTahanan.Items.Add(lv.Text);
                        txtNoCell.Items.Add(item.FullName + "-" + item.BuildingName + "-" + item.CellNo);
                    }
                    //txtNamaTahanan.Text = data.NamaTahanan;
                    //txtNoCell.Text = data.BuildingName + " / " + data.CellNumber;
                    dpTanggalTerima.Value = data.TanggalTerima != null ? (DateTime)data.TanggalTerima : DateTime.Now.ToLocalTime();
                    tpDiterima.Value = data.JamTerima != null ? (DateTime)data.JamTerima : DateTime.Now.ToLocalTime();
                    //ddlZonaWaktu.Text = data.ZonaWaktuTerima;

                    txtNamaYangMenyerahkan.Text = data.NamaYangMenyerahkan;
                    txtNrpYangMenyerahkan.Text = data.NrpYangMenyerahkan;
                    ddlPangkatYangMenyerahkan.SelectedIndex = Convert.ToInt32(data.IdPangkatMenyerahkan);
                    txtSatkerYangMenyerahkan.Text = data.SatkerYangMenyerahkan;

                    txtNamaYangMenerima.Text = data.NamaYangMenerima;
                    txtNrpYangMenerima.Text = data.NrpYangMenerima;
                    ddlPangkatYangMenerima.SelectedIndex = Convert.ToInt32(data.IdPangkatMenerima);
                    txtSatkerYangMenerima.Text = data.SatkerYangMenerima;

                    txtNamaYangMenyetujui.Text = data.NamaYangMenyetujui;
                    txtNrpYangMenyetujui.Text = data.NrpYangMenyetujui;
                    ddlPangkatYangMenyetujui.SelectedIndex = Convert.ToInt32(data.IdPangkatMenyetujui);
                    txtSatkerYangMenyetujui.Text = data.SatkerYangMenyetujui;
                }
            }
        }

        public void ClearForm()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            dpTanggalTerima.Value = Convert.ToDateTime(null);
            txtNamaTahanan.Enabled = false;
            txtNoCell.Enabled = false;

            //ddl Bon Tahanan
            BonTahananService bonserv = new BonTahananService();
            ddlBonTahanan.DisplayMember = "NoBonTahanan";
            ddlBonTahanan.ValueMember = "Id";
            ddlBonTahanan.DataSource = bonserv.GetOut();
            ddlBonTahanan.SelectedIndex = -1;

            if (this.Tag != null)
            {
                ddlBonTahanan.Items.Clear();
                ddlBonTahanan.DisplayMember = "NoBonTahanan";
                ddlBonTahanan.ValueMember = "Id";
                ddlBonTahanan.DataSource = bonserv.Get();
                ddlBonTahanan.Enabled = false;
            }

            //ddl Zona Waktu
            //ddlZonaWaktu.Items.Clear();
            //ddlZonaWaktu.Items.Add("WIB");
            //ddlZonaWaktu.Items.Add("WIT");
            //ddlZonaWaktu.Items.Add("WITA");

            //ddl Pangkat Yang Menerima
            PangkatPolisiService pangkatserv = new PangkatPolisiService();
            ddlPangkatYangMenerima.Items.Clear();
            ddlPangkatYangMenerima.DisplayMember = "Nama";
            ddlPangkatYangMenerima.ValueMember = "Id";
            ddlPangkatYangMenerima.DataSource = pangkatserv.Get();

            //ddl Pangkat Yang Menyetujui
            ddlPangkatYangMenyetujui.Items.Clear();
            ddlPangkatYangMenyetujui.DisplayMember = "Nama";
            ddlPangkatYangMenyetujui.ValueMember = "Id";
            ddlPangkatYangMenyetujui.DataSource = pangkatserv.Get();

            //ddl Pangkat Yang Menyerahkan
            ddlPangkatYangMenyerahkan.Items.Clear();
            ddlPangkatYangMenyerahkan.DisplayMember = "Nama";
            ddlPangkatYangMenyerahkan.ValueMember = "Id";
            ddlPangkatYangMenyerahkan.DataSource = pangkatserv.Get();

            //ddlBonTahanan_SelectedIndexChanged(ddlBonTahanan, null);

            ddlPangkatYangMenerima.SelectedIndex = -1;
            ddlPangkatYangMenyetujui.SelectedIndex = -1;
            ddlPangkatYangMenyerahkan.SelectedIndex = -1;
        }

        private void InitEvents()
        {
            //Events
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            this.VisibleChanged += Form_VisibleChanged;

            ddlBonTahanan.KeyDown += Control_KeyDown;
            txtNamaTahanan.KeyDown += Control_KeyDown;

            txtNamaYangMenyerahkan.KeyDown += Control_KeyDown;
            txtNrpYangMenyerahkan.KeyDown += Control_KeyDown;
            ddlPangkatYangMenyerahkan.KeyDown += Control_KeyDown;
            txtSatkerYangMenyerahkan.KeyDown += Control_KeyDown;

            txtNamaYangMenerima.KeyDown += Control_KeyDown;
            txtNrpYangMenerima.KeyDown += Control_KeyDown;
            ddlPangkatYangMenerima.KeyDown += Control_KeyDown;
            txtSatkerYangMenerima.KeyDown += Control_KeyDown;

            txtNamaYangMenyetujui.KeyDown += Control_KeyDown;
            txtNrpYangMenyetujui.KeyDown += Control_KeyDown;
            ddlPangkatYangMenyetujui.KeyDown += Control_KeyDown;
            txtSatkerYangMenyetujui.KeyDown += Control_KeyDown;

            txtNrpYangMenyerahkan.KeyUp += TxtNrpYangMenyerahkan_KeyUp;
            txtNrpYangMenerima.KeyUp += TxtNrpYangMenerima_KeyUp;
            txtNrpYangMenyetujui.KeyUp += TxtNrpYangMenyetujui_KeyUp;
            btnSave.KeyDown += Control_KeyDown;
            
            ddlBonTahanan.SelectedValueChanged += DdlBonTahanan_SelectedValueChanged;

            radPanel19.KeyDown += Control_KeyDown;
            radPanel18.KeyDown += Control_KeyDown;
            radPanel17.KeyDown += Control_KeyDown;
            radPanel16.KeyDown += Control_KeyDown;
            radPanel1.KeyDown += Control_KeyDown;
            radPanel10.KeyDown += Control_KeyDown;
            radPanel11.KeyDown += Control_KeyDown;
            radPanel12.KeyDown += Control_KeyDown;
            radPanel15.KeyDown += Control_KeyDown;

            radPanel2.KeyDown += Control_KeyDown;
            radPanel20.KeyDown += Control_KeyDown;
            radPanel21.KeyDown += Control_KeyDown;
            radPanel22.KeyDown += Control_KeyDown;
            radPanel3.KeyDown += Control_KeyDown;
            //radPanel4.KeyDown += Control_KeyDown;
            radPanel6.KeyDown += Control_KeyDown;
            radPanel7.KeyDown += Control_KeyDown;
            radPanel9.KeyDown += Control_KeyDown;

            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;
        }

        private void DdlBonTahanan_SelectedValueChanged(object sender, EventArgs e)
        {
            var bon = ddlBonTahanan.SelectedValue;
            if (bon != null)
            {
                InmateBonTahananService bonserv = new InmateBonTahananService();
                var dataItem = bonserv.GetByBonTahananId((int)bon);
                ListViewItem lv = new ListViewItem();
                foreach (var item in dataItem)
                {
                    lv.Text = item.RegID+'-'+item.FullName;
                    lv.Tag = item.RegID;
                    txtNamaTahanan.Items.Add(lv.Text);
                    txtNoCell.Items.Add(item.FullName + "-" +item.BuildingName + "-" + item.CellNo);
                }
            }
            else
            {
                txtNamaTahanan.Items.Clear();
                txtNoCell.Items.Clear();
            }
        }

        private void TxtNrpYangMenyetujui_KeyUp(object sender, KeyEventArgs e)
        {
            if (!UserFunction.IsNumeric(txtNrpYangMenyetujui.Text))
            {
                txtNrpYangMenyetujui.Text = string.Empty;
            }
        }

        private void TxtNrpYangMenerima_KeyUp(object sender, KeyEventArgs e)
        {
            if (!UserFunction.IsNumeric(txtNrpYangMenerima.Text))
            {
                txtNrpYangMenerima.Text = string.Empty;
            }
        }

        private void TxtNrpYangMenyerahkan_KeyUp(object sender, KeyEventArgs e)
        {
            if (!UserFunction.IsNumeric(txtNrpYangMenyerahkan.Text))
            {
                txtNrpYangMenyerahkan.Text = string.Empty;
            }
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SendKeys.Send("{TAB}");
        }

        private void InitForm()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     FORM BON SERAH TAHANAN";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;
            this.lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 14f;
            this.lblDetailSurat.ForeColor = Color.White;
            this.lblDetailSurat.BackColor = Color.FromArgb(77, 77, 77);

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailVisitor.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailVisitor.LabelElement.CustomFontSize = 15.5f;

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ClearForm();
            this.Tag = null;
            MainForm.formMain.AddNew.Text = "TAMBAH BON SERAH TAHANAN";
            this.lblTitle.Text = "     TAMBAH BON SERAH TAHANAN";
            UserFunction.ClearControls(tableLayoutPanel1);
            MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                if (this.Tag == null)
                    SaveData();
                else
                    UpdateData();

                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Disimpan!");
                UserFunction.LoadDataBonSerahTahananToGrid(MainForm.formMain.ListBonSerahTahanan.GvBonSerahTahanan);
                MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
                MainForm.formMain.AddNew.Text = "TAMBAH BON SERAH TAHANAN";
                this.lblTitle.Text = "     TAMBAH FORM SERAH TAHANAN";
                this.Tag = null;

                ClearForm();
            }
            
        }

        private void SaveData()
        {
            BonSerahTahananService bonserahserv = new BonSerahTahananService();
            var bonserah = new BonSerahTahanan();
            bonserah.NoBonTahanan = ddlBonTahanan.SelectedValue.ToString();
            bonserah.TanggalTerima = dpTanggalTerima.Value;
            bonserah.JamTerima = tpDiterima.Value;
            //bonserah.ZonaWaktuTerima = ddlZonaWaktu.Text;

            bonserah.NamaYangMenyerahkan = txtNamaYangMenyerahkan.Text;
            bonserah.NrpYangMenyerahkan = txtNrpYangMenyerahkan.Text;
            bonserah.PangkatYangMenyerahkan = ddlPangkatYangMenyerahkan.SelectedValue.ToString();
            bonserah.SatkerYangMenyerahkan = txtSatkerYangMenyerahkan.Text;

            bonserah.NamaYangMenerima = txtNamaYangMenerima.Text;
            bonserah.NrpYangMenerima = txtNrpYangMenerima.Text;
            bonserah.PangkatYangMenerima = ddlPangkatYangMenerima.SelectedValue.ToString();
            bonserah.SatkerYangMenerima = txtSatkerYangMenerima.Text;

            bonserah.NamaYangMenyetujui = txtNamaYangMenyetujui.Text;
            bonserah.NrpYangMenyetujui = txtNrpYangMenyetujui.Text;
            bonserah.PangkatYangMenyetujui = ddlPangkatYangMenyetujui.SelectedValue.ToString();
            bonserah.SatkerYangMenyetujui = txtSatkerYangMenyetujui.Text;

            bonserah.CreatedDate = DateTime.Now.ToLocalTime();
            bonserah.CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID);
            bonserah.CreatedLocation = "0.0.0.0";
            bonserahserv.Post(bonserah);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Add New BonSerahTahanan, Data=" + UserFunction.JsonString(bonserah) });

            BonTahananService bonserv = new BonTahananService();
            var bon = new BonTahanan();
            bon.Id = Convert.ToInt32(ddlBonTahanan.SelectedValue);
            var databon = bonserv.GetById(bon.Id);
            bon.StatusTahanan = "Keluar";
            bon.TanggalMulai = databon.TanggalMulai;
            bon.TanggalSelesai = databon.TanggalSelesai;
            bon.UpdatedDate = DateTime.Now.ToLocalTime();
            bon.UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID);
            bon.UpdatedLocation = "0.0.0.0";
            bonserv.Update(bon);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Update BonTahanan, Data=" + UserFunction.JsonString(databon) });

        }

        private void UpdateData()
        {
            BonSerahTahananService serv = new BonSerahTahananService();
            var bon = new BonSerahTahanan();
            bon.Id = Convert.ToInt32(this.Tag);
            bon.NoBonTahanan = ddlBonTahanan.SelectedValue.ToString();
            bon.TanggalTerima = dpTanggalTerima.Value;
            bon.JamTerima = tpDiterima.Value;
            //bon.ZonaWaktuTerima = ddlZonaWaktu.Text;

            bon.NamaYangMenyerahkan = txtNamaYangMenyerahkan.Text;
            bon.NrpYangMenyerahkan = txtNrpYangMenyerahkan.Text;
            bon.PangkatYangMenyerahkan = ddlPangkatYangMenyerahkan.SelectedValue.ToString();
            bon.SatkerYangMenyerahkan = txtSatkerYangMenyerahkan.Text;

            bon.NamaYangMenerima = txtNamaYangMenerima.Text;
            bon.NrpYangMenerima = txtNrpYangMenerima.Text;
            bon.PangkatYangMenerima = ddlPangkatYangMenerima.SelectedValue.ToString();
            bon.SatkerYangMenerima = txtSatkerYangMenerima.Text;

            bon.NamaYangMenyetujui = txtNamaYangMenyetujui.Text;
            bon.NrpYangMenyetujui = txtNrpYangMenyetujui.Text;
            bon.PangkatYangMenyetujui = ddlPangkatYangMenyetujui.SelectedValue.ToString();
            bon.SatkerYangMenyetujui = txtSatkerYangMenyetujui.Text;

            bon.UpdatedDate = DateTime.Now.ToLocalTime();
            bon.UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID);
            bon.UpdatedLocation = "0.0.0.0";
            serv.Update(bon);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Update BonSerahTahanan, Old Data=" + UserFunction.JsonString(OldData) + ",  New Data=" + UserFunction.JsonString(bon) });

        }

        private bool DataValid()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            if (this.ddlBonTahanan.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Memilih Bon Tahanan!";
                return false;
            }
            if (this.dpTanggalTerima.Value.ToLongDateString() == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi Tanggal Terima!";
                return false;
            }
            if (this.tpDiterima.Value.ToString() == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi Jam Terima!";
                return false;
            }
            //if (this.ddlZonaWaktu.Text == string.Empty)
            //{
            //    this.lblError.Visible = true;
            //    this.lblError.Text = "Mohon Mengisi Zona Waktu Terima!";
            //    return false;
            //}

            if (this.txtNamaYangMenerima.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi Nama Yang Menerima!";
                return false;
            }
            if (this.txtNrpYangMenerima.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi NRP Yang Menerima!";
                return false;
            }
            if (this.ddlPangkatYangMenerima.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Memilih Pangkat Yang Menerima!";
                return false;
            }
            if (this.txtSatkerYangMenerima.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi Satuan Kerja Yang Menerima!";
                return false;
            }

            //if (this.txtNamaYangMenyerahkan.Text == string.Empty)
            //{
            //    this.lblError.Visible = true;
            //    this.lblError.Text = "Mohon Mengisi Nama Yang Menyerahkan!";
            //    return false;
            //}
            //if (this.txtNrpYangMenyerahkan.Text == string.Empty)
            //{
            //    this.lblError.Visible = true;
            //    this.lblError.Text = "Mohon Mengisi NRP Yang Menyerahkan!";
            //    return false;
            //}
            //if (this.ddlPangkatYangMenyerahkan.Text == string.Empty)
            //{
            //    this.lblError.Visible = true;
            //    this.lblError.Text = "Mohon Memilih Pangkat Yang Menyerahkan!";
            //    return false;
            //}
            //if (this.txtSatkerYangMenyerahkan.Text == string.Empty)
            //{
            //    this.lblError.Visible = true;
            //    this.lblError.Text = "Mohon Mengisi Satuan Kerja Yang Menyerahkan!";
            //}

            if (this.txtNamaYangMenyetujui.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi Nama Yang Menyetujui!";
                return false;
            }
            if (this.txtNrpYangMenyetujui.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi NRP Yang Menyetujui!";
                return false;
            }
            if (this.ddlPangkatYangMenyetujui.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Memilih Pangkat Yang Menyetujui!";
                return false;
            }
            if (this.txtSatkerYangMenyetujui.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi Satuan Kerja Yang Menyetujui!";
                return false;
            }
            return true;
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            UserFunction.ClearControls(tableLayoutPanel1);

            if (this.Visible)
            {
                LoadData();
            }
            else
            {

            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
