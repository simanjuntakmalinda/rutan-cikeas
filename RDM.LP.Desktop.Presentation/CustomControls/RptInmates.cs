﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;
using Telerik.WinControls.Export;
using System.IO;
using System.Threading;
using Telerik.WinControls.UI.Export;
using Telerik.Windows.Documents.Media;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class RptInmates : Base
    {
        public RadGridView GvCell { get { return this.gvCell; } }
        public RptInmates()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvCell.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvCell.RowFormatting += radGridView_RowFormatting;
            gvCell.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvCell.CellClick += gvCell_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
            this.VisibleChanged += Form_VisibleChanged;
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DetailTahanan.pdf", "INMATES DETAIL", gvCell);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DetailTahanan.csv", "INMATES DETAIL", gvCell);
        }

        private void gvCell_CellClick(object sender, GridViewCellEventArgs e)
        {
                 
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DETAIL DATA TAHANAN";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;
            this.lblDetail.BackColor = Color.FromArgb(77, 77, 77);
            this.lblDetail.ForeColor = Color.White;

            UserFunction.LoadDataRptInmateToGrid(gvCell);
            UserFunction.SetInitGridView(gvCell);

        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                InitForm();
            }
        }

    }
}
