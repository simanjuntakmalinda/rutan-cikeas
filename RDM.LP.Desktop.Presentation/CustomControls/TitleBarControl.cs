﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using RDM.LP.Desktop.Presentation.Properties;
using Telerik.WinControls.UI;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class TitleBarControl : UserControl
    {
        public RadPanel AppTitle { get { return this.radPanel1; } }
        //public Timer TimerAnimasi { get { return this.timerAnimasi; } }

        public TitleBarControl()
        {
            InitializeComponent();
            radPanel1.ElementTree.EnableApplicationThemeName = false;
            radTitleBar1.ElementTree.EnableApplicationThemeName = false;
            radTitleBar1.TitleBarElement.BorderPrimitive.Visibility = ElementVisibility.Collapsed;
            //radPanel1.BackgroundImage = Resources.logo;

            radPanel1.BackgroundImageLayout = ImageLayout.None;
            radPanel1.PanelElement.PanelBorder.Visibility = ElementVisibility.Collapsed;
            radPanel1.Text = string.Format("{0}",Application.ProductName);
            radPanel1.BackColor = Color.FromArgb(31, 22, 35);
            radTitleBar1.BackColor = Color.FromArgb(31, 22, 35);
            this.BackColor = Color.FromArgb(31, 22, 35);

            toolTip2.SetToolTip(this.setting, "Pengaturan Parameter");
            toolTip3.SetToolTip(this.about, "Tentang " + Application.ProductName);
            toolTip4.SetToolTip(this.help, "Buku Panduan Pengguna " + Application.ProductName);

            about.Click += about_Click;
            help.Click += help_Click;
            setting.Click += setting_Click;

        }

        private void setting_Click(object sender, EventArgs e)
        {
            //Get Setting
            MainForm.formMain.setting.Show();
        }

        private void help_Click(object sender, EventArgs e)
        {
            Help frm = new Help();
            frm.Show();
        }

        private void about_Click(object sender, EventArgs e)
        {
            About frm = new About();
            frm.ShowDialog();
        }
    }
}
