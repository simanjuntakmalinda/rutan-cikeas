﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using System.Threading;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class SeizedAssetsDetail : Base
    {
        private Point MouseDownLocation;
        public SeizedAssetsDetail()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void LoadData(string id)
        {
            CasesService caseserv = new CasesService();
            var data = caseserv.GetbyId(Convert.ToInt32(id));
            if (data != null)
            {
                caseName.Text = data.Name.ToUpper();
                inputter.Text = data.CreatedBy;
                inputdate.Text = data.CreatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss", new System.Globalization.CultureInfo("id-ID"));
                updater.Text = data.UpdatedBy;
                updatedate.Text = data.UpdatedDate==null?string.Empty:data.UpdatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss", new System.Globalization.CultureInfo("id-ID"));
            }
            UserFunction.LoadDataSeizedAssetsCaseToGrid(gvSeizedAssets, Convert.ToInt32(id));
        }

        private void InitForm()
        {
            ToolTip toolTip = new ToolTip();
            toolTip.ToolTipIcon = ToolTipIcon.Info;
            toolTip.ToolTipTitle = "Info";
            toolTip.SetToolTip(exit, "Tutup Detail Benda Sitaan");

            btnClose.ButtonElement.CustomFontStyle = FontStyle.Bold;
            btnClose.ButtonElement.CustomFont = Global.MainFont;
            btnClose.ButtonElement.CustomFontSize = Global.CustomFontSizeMain;
            lblButton.BackColor = Global.MainColor;
            lblTitle.BackColor = Global.MainColor;
            lblTitle.Text = "      DETAIL BENDA SITAAN";
            lblTitle.ForeColor = Color.White;
            lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;
            lblTitle.LabelElement.CustomFont = Global.MainFont;
            lblTitle.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
            lblCaseName.ForeColor = Color.FromArgb(192, 0, 0);
            caseName.ForeColor = Color.FromArgb(192, 0, 0);

            UserFunction.LoadDataSeizedAssetsCaseToGrid(gvSeizedAssets, Convert.ToInt32(this.Tag));
            UserFunction.SetInitGridView(gvSeizedAssets);
        }

        private void InitEvents()
        {
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            exit.Click += exit_Click;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            this.VisibleChanged += Form_VisibleChanged;
            btnClose.Click += btnClose_Click;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (this.Tag != null)
                    LoadData(this.Tag.ToString());
                else
                    this.Hide();
            }
        }
    }
}
