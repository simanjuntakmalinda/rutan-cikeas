﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.DataAccess.Helper;
using Microsoft.VisualBasic;
using System.IO;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormStatusTahanan : Base
    {
        private Point MouseDownLocation;
        private StatusTahanan OldData;
        private string filename1 = string.Empty;
        private string filename2 = string.Empty;
        private string filename3 = string.Empty;
        private string OpenDIalogfilename1 = string.Empty;
        private string OpenDIalogfilename2 = string.Empty;
        private string OpenDIalogfilename3 = string.Empty;
        public RadTextBox TxtNama { get { return this.txtNamatahanan; } set { this.txtNamatahanan = value; } }
        public RadTextBox TbRegId { get { return this.tbNoReg; } set { this.tbNoReg = value; } }
        public RadTextBox TbNoSel { get { return this.tbNoSel; } set { this.tbNoSel = value; } }
        public RadTextBox NoSpKap { get { return this.tbNoSuratTangkap; } set { this.tbNoSuratTangkap = value; } }
        public RadTextBox NoSpHan { get { return this.tbNoSuratTahan; } set { this.tbNoSuratTahan = value; } }
        public RadDateTimePicker TglHan { get { return this.dtTanggalPenahanan; } set { this.dtTanggalPenahanan = value; } }
        public RadDateTimePicker TglKap { get { return this.dtTanggalPenangkapan; } set { this.dtTanggalPenangkapan = value; } }
        public FormStatusTahanan()
        {
            InitializeComponent();
            InitForm();
            LoadData();
            InitEvents();
        }

        class ComboItem
        {
            public int ID { get; set; }
            public string Text { get; set; }
        }

        public void LoadData()
        {
            this.lblTitle.Text = "     TAMBAH DATA BARU";

            ClearForm();

            if (this.Tag != null && this.Tag != string.Empty)
            {
                StatusTahananService cellserv = new StatusTahananService();
                var data = cellserv.GetById(Convert.ToInt32(this.Tag));
                if (data == null)
                {
                    return;
                }
                else
                {
                    MainForm.formMain.AddNew.Text = "UBAH DATA ";
                    this.lblTitle.Text = "     UBAH DATA ";
                    OldData = data;

                    txtNamatahanan.Text = data.FullName;
                    tbNoReg.Text = data.RegID;
                    tbNoSel.Text = data.NoSel;
                    tbNoSuratTahan.Text = data.NoSuratPenahanan;
                    tbNoSuratTangkap.Text = data.NoSuratPenangkapan;
                    dtTanggalPenahanan.Value = data.TanggalPenahanan.Value; 
                    dtTanggalPenangkapan.Value = data.TanggalPenangkapan.Value;

                    PerpanjanganTahananService prp = new PerpanjanganTahananService();
                    var dt = prp.GetByStatusTahananId(Convert.ToInt32(this.Tag));
                    if(dt!= null)
                    {
                        foreach(var item in dt)
                        {
                            if(item.PerpanjanganKe == "1")
                            {
                                var p1 = prp.GetByPerpanjanganKe("1");
                                if(p1 != null)
                                {
                                    tbNoSuratP1.Text = p1.NoSurat;
                                    dtTanggalSuratP1.Value = p1.TanggalPerpanjangan.Value;
                                    ddlDurasi1.Text = p1.Durasi.ToString();
                                    tbFileP1.Text = p1.Dokumen;
                                }
                            }
                            else if (item.PerpanjanganKe == "2")
                            {
                                var p2 = prp.GetByPerpanjanganKe("2");
                                if(p2 != null)
                                {
                                    tbNoSuratP2.Text = p2.NoSurat;
                                    dtTanggalSuratP2.Value = p2.TanggalPerpanjangan.Value;
                                    ddlDurasi2.Text = p2.Durasi.ToString();
                                    tbFileP2.Text = p2.Dokumen;
                                }
                            }
                            else
                            {
                                var p3 = prp.GetByPerpanjanganKe("3");
                                if(p3 != null)
                                {
                                    tbNosuratP3.Text = p3.NoSurat;
                                    dtTanggalSuratP3.Value = p3.TanggalPerpanjangan.Value;
                                    ddlDurasi3.Text = p3.Durasi.ToString();
                                    txtFileP3.Text = p3.Dokumen;
                                }
                            }
                        }
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }

        private void ClearForm()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            txtNamatahanan.Text = string.Empty;
            tbNoReg.Text = string.Empty;
            tbNoSel.Text = string.Empty;
            tbNoSuratTahan.Text = string.Empty;
            tbNoSuratTangkap.Text = string.Empty;
            dtTanggalPenangkapan.Value = DateTime.Now.ToLocalTime();
            dtTanggalPenahanan.Value = DateTime.Now.ToLocalTime();
            tbNoSuratP1.Text = string.Empty;
            tbNoSuratP2.Text = string.Empty;
            tbNosuratP3.Text = string.Empty;
            ddlDurasi1.Text = string.Empty;
            ddlDurasi2.Text = string.Empty;
            ddlDurasi3.Text = string.Empty;
            tbFileP1.Text = string.Empty;
            tbFileP2.Text = string.Empty;
            txtFileP3.Text = string.Empty;
            tablePerpanjangan1.Tag = 0;
            tablePerpanjangan2.Tag = 0;
            tablePerpanjangan3.Tag = 0;
            radPageView1.SelectedPage = radPageViewPage1;
        }

        private void InitEvents()
        {
            //Events
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;

            btnSearch.Click += BtnSearch_Click;

            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;

            tbNoSuratP1.TextChanged += TbNoSuratP1_TextChanged;
            ddlDurasi1.SelectedIndexChanged += DdlDurasi1_SelectedIndexChanged;
            tbFileP1.TextChanged += TbFileP1_TextChanged;

            tbNoSuratP2.TextChanged += TbNoSuratP2_TextChanged;
            ddlDurasi2.SelectedIndexChanged += DdlDurasi2_SelectedIndexChanged;
            tbFileP2.TextChanged += TbFileP2_TextChanged;

            tbNosuratP3.TextChanged += TbNoSuratP3_TextChanged;
            ddlDurasi3.SelectedIndexChanged += DdlDurasi3_SelectedIndexChanged;
            txtFileP3.TextChanged += TxtFile3_TextChanged;

            //btnAddPerpanjangan1.Click += BtnAddPerpanjangan1_Click;
            //btnAddPerpanjangan2.Click += BtnAddPerpanjangan2_Click;
            //btnAddPerpanjangan3.Click += BtnAddPerpanjangan3_Click;
            btnBrowseP1.Click += Browse_Click1;
            btnBrowseP2.Click += Browse_Click2;
            btnBrowseP3.Click += Browse_Click3;
        }

        private void TbFileP1_TextChanged(object sender, EventArgs e)
        {
            if (tbFileP1.Text != string.Empty || ddlDurasi1.Text != string.Empty || tbNoSuratP1.Text != string.Empty)
            {
                tablePerpanjangan1.Tag = 1;
            }
            else
            {
                tablePerpanjangan1.Tag = 0;
            }
        }

        private void DdlDurasi1_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (tbFileP1.Text != string.Empty || ddlDurasi1.Text != string.Empty || tbNoSuratP1.Text != string.Empty)
            {
                tablePerpanjangan1.Tag = 1;
            }
            else
            {
                tablePerpanjangan1.Tag = 0;
            }
        }

        private void TbNoSuratP1_TextChanged(object sender, EventArgs e)
        {
            if(tbFileP1.Text != string.Empty || ddlDurasi1.Text != string.Empty || tbNoSuratP1.Text != string.Empty)
            {
                tablePerpanjangan1.Tag = 1;
            }
            else
            {
                tablePerpanjangan1.Tag = 0;
            }
        }

        private void TbFileP2_TextChanged(object sender, EventArgs e)
        {
            if (tbFileP2.Text != string.Empty || ddlDurasi2.Text != string.Empty || tbNoSuratP2.Text != string.Empty)
            {
                tablePerpanjangan2.Tag = 1;
            }
            else
            {
                tablePerpanjangan2.Tag = 0;
            }
        }

        private void DdlDurasi2_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (tbFileP2.Text != string.Empty || ddlDurasi2.Text != string.Empty || tbNoSuratP2.Text != string.Empty)
            {
                tablePerpanjangan2.Tag = 1;
            }
            else
            {
                tablePerpanjangan2.Tag = 0;
            }
        }

        private void TbNoSuratP2_TextChanged(object sender, EventArgs e)
        {
            if (tbFileP2.Text != string.Empty || ddlDurasi2.Text != string.Empty || tbNoSuratP2.Text != string.Empty)
            {
                tablePerpanjangan2.Tag = 1;
            }
            else
            {
                tablePerpanjangan2.Tag = 0;
            }
        }

        private void TxtFile3_TextChanged(object sender, EventArgs e)
        {
            if (txtFileP3.Text != string.Empty || ddlDurasi3.Text != string.Empty || tbNosuratP3.Text != string.Empty)
            {
                tablePerpanjangan3.Tag = 1;
            }
            else
            {
                tablePerpanjangan3.Tag = 0;
            }
        }

        private void DdlDurasi3_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (txtFileP3.Text != string.Empty || ddlDurasi3.Text != string.Empty || tbNosuratP3.Text != string.Empty)
            {
                tablePerpanjangan3.Tag = 1;
            }
            else
            {
                tablePerpanjangan3.Tag = 0;
            }
        }

        private void TbNoSuratP3_TextChanged(object sender, EventArgs e)
        {
            if (txtFileP3.Text != string.Empty || ddlDurasi3.Text != string.Empty || tbNosuratP3.Text != string.Empty)
            {
                tablePerpanjangan3.Tag = 1;
            }
            else
            {
                tablePerpanjangan3.Tag = 0;
            }
        }

        private void Browse_Click1(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            //To where your opendialog box get starting location. My initial directory location is desktop.
            openFileDialog1.InitialDirectory = "C://Desktop";
            //Your opendialog box title name.
            openFileDialog1.Title = "Pilih file untuk diupload";
            //which type file format you want to upload in database. just add them.
            openFileDialog1.Filter = "Pilih Dokumen Valid(*.pdf; *.doc; *.xlsx; *.html)|*.pdf; *.docx; *.xlsx; *.html";
            //FilterIndex property represents the index of the filter currently selected in the file dialog box.
            openFileDialog1.FilterIndex = 1;
            try
            {
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (openFileDialog1.CheckFileExists)
                    {
                        string path = System.IO.Path.GetFullPath(openFileDialog1.FileName);
                        tbFileP1.Text = path;
                        filename1 = System.IO.Path.GetFileName(openFileDialog1.FileName);
                        OpenDIalogfilename1 = openFileDialog1.FileName;
                    }
                }
                else
                {
                    MessageBox.Show("Mohon upload Dokumen");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Browse_Click2(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            //To where your opendialog box get starting location. My initial directory location is desktop.
            openFileDialog1.InitialDirectory = "C://Desktop";
            //Your opendialog box title name.
            openFileDialog1.Title = "Pilih file untuk diupload";
            //which type file format you want to upload in database. just add them.
            openFileDialog1.Filter = "Pilih Dokumen Valid(*.pdf; *.doc; *.xlsx; *.html)|*.pdf; *.docx; *.xlsx; *.html";
            //FilterIndex property represents the index of the filter currently selected in the file dialog box.
            openFileDialog1.FilterIndex = 1;
            try
            {
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (openFileDialog1.CheckFileExists)
                    {
                        string path = System.IO.Path.GetFullPath(openFileDialog1.FileName);
                        tbFileP2.Text = path;
                        filename2 = System.IO.Path.GetFileName(openFileDialog1.FileName);
                        OpenDIalogfilename2 = openFileDialog1.FileName;
                    }
                }
                else
                {
                    MessageBox.Show("Mohon upload Dokumen");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Browse_Click3(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            //To where your opendialog box get starting location. My initial directory location is desktop.
            openFileDialog1.InitialDirectory = "C://Desktop";
            //Your opendialog box title name.
            openFileDialog1.Title = "Pilih file untuk diupload";
            //which type file format you want to upload in database. just add them.
            openFileDialog1.Filter = "Pilih Dokumen Valid(*.pdf; *.doc; *.xlsx; *.html)|*.pdf; *.docx; *.xlsx; *.html";
            //FilterIndex property represents the index of the filter currently selected in the file dialog box.
            openFileDialog1.FilterIndex = 1;
            try
            {
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (openFileDialog1.CheckFileExists)
                    {
                        string path = System.IO.Path.GetFullPath(openFileDialog1.FileName);
                        txtFileP3.Text = path;
                        filename3 = System.IO.Path.GetFileName(openFileDialog1.FileName);
                        OpenDIalogfilename3 = openFileDialog1.FileName;
                    }
                }
                else
                {
                    MessageBox.Show("Mohon upload Dokumen");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void BtnAddPerpanjangan3_Click(object sender, EventArgs e)
        {
            MainForm.formMain.PerpanjanganTahanan.Show();
        }

        private void BtnAddPerpanjangan2_Click(object sender, EventArgs e)
        {
            MainForm.formMain.PerpanjanganTahanan.Show();
        }

        private void BtnAddPerpanjangan1_Click(object sender, EventArgs e)
        {
            MainForm.formMain.PerpanjanganTahanan.Show();
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            MainForm.formMain.SearchData.NamaForm = "FormStatusTahanan";
            MainForm.formMain.SearchData.Tag = SearchType.Tahanan.ToString();
            MainForm.formMain.SearchData.Show();
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {

                if ((int)tablePerpanjangan1.Tag == 0 && (int)tablePerpanjangan2.Tag == 0 && (int)tablePerpanjangan3.Tag == 0)
                {
                    if (this.Tag == null)
                    {
                        if (CheckFileUpload())
                        {
                            SaveStatusTahanan();
                        }
                    }
                    else
                        UpdatedData();
                }
                if ((int)tablePerpanjangan1.Tag == 1 && (int)tablePerpanjangan2.Tag == 0 && (int)tablePerpanjangan3.Tag == 0)
                {
                    if (ValidasiPerpanjangan1())
                    {
                        if (this.Tag == null)
                        {
                            if (CheckFileUpload())
                            {
                                SaveStatusTahanan();
                            }
                        }
                        else
                            UpdatedData();
                    }
                }
                if ((int)tablePerpanjangan1.Tag == 1 && (int)tablePerpanjangan2.Tag == 1 && (int)tablePerpanjangan3.Tag == 0)
                {
                    if (ValidasiPerpanjangan1() && ValidasiPerpanjangan2())
                    {
                        if (this.Tag == null)
                        {
                            if (CheckFileUpload())
                            {
                                SaveStatusTahanan();
                            }
                        }
                        else
                            UpdatedData();
                    }
                }
                if ((int)tablePerpanjangan1.Tag == 1 && (int)tablePerpanjangan2.Tag == 1 && (int)tablePerpanjangan3.Tag == 1)
                {
                    if (ValidasiPerpanjangan1() && ValidasiPerpanjangan2() && ValidasiPerpanjangan3())
                    {
                        if (this.Tag == null)
                        {
                            if (CheckFileUpload())
                            {
                                SaveStatusTahanan();
                            }
                        }
                        else
                            UpdatedData();
                    }
                }

            }
        }

        private bool CheckFileUpload()
        {
            string path = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
            string name1 = Path.Combine(path + "\\Document\\", filename1);
            string name2 = Path.Combine(path + "\\Document\\", filename2);
            string name3 = Path.Combine(path + "\\Document\\", filename3);
            if (File.Exists(name1) && File.Exists(name2) && File.Exists(name3))
            {
                UserFunction.MsgBox(TipeMsg.Info, "File Perpanjangan 1, 2 dan 3 sudah ada dalam sistem !");
                return false;
            }
            else if (File.Exists(name1) && File.Exists(name2) && !File.Exists(name3))
            {
                UserFunction.MsgBox(TipeMsg.Info, "File Perpanjangan 1 dan 2 sudah ada dalam sistem !");
                return false;
            }
            else if (File.Exists(name1) && !File.Exists(name2) && !File.Exists(name3))
            {
                UserFunction.MsgBox(TipeMsg.Info, "File Perpanjangan 1 sudah ada dalam sistem !");
                return false;
            }
            else if (!File.Exists(name1) && File.Exists(name2) && !File.Exists(name3))
            {
                UserFunction.MsgBox(TipeMsg.Info, "File Perpanjangan 2 sudah ada dalam sistem !");
                return false;
            }
            else if (!File.Exists(name1) && !File.Exists(name2) && File.Exists(name3))
            {
                UserFunction.MsgBox(TipeMsg.Info, "File Perpanjangan 3 sudah ada dalam sistem !");
                return false;
            }
            else if (File.Exists(name1) && !File.Exists(name2) && File.Exists(name3))
            {
                UserFunction.MsgBox(TipeMsg.Info, "File Perpanjangan 1 dan 3 sudah ada dalam sistem !");
                return false;
            }
            else if (!File.Exists(name1) && File.Exists(name2) && File.Exists(name3))
            {
                UserFunction.MsgBox(TipeMsg.Info, "File Perpanjangan 2 dan 3 sudah ada dalam sistem !");
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool ValidasiPerpanjangan1()
        {
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;
            if ((string)this.tbNoSuratP1.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi No Surat Perpanjangan 1!";
                return false;
            }
            if ((string)this.dtTanggalSuratP1.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tanggal Perpanjangan 1!";
                return false;
            }
            if ((string)this.ddlDurasi1.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Durasi Perpanjangan 1!";
                return false;
            }
            if ((string)this.tbFileP1.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Upload File Perpanjangan 1!";
                return false;
            }
            return true;
        }

        private bool ValidasiPerpanjangan2()
        {
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;
            if ((string)this.tbNoSuratP2.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi No Surat Perpanjangan 2!";
                return false;
            }
            if ((string)this.dtTanggalSuratP2.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tanggal Perpanjangan 2!";
                return false;
            }
            if ((string)this.ddlDurasi2.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Durasi Perpanjangan 2!";
                return false;
            }
            if ((string)this.tbFileP2.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Upload File Perpanjangan 2!";
                return false;
            }
            return true;
        }

        private bool ValidasiPerpanjangan3()
        {
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;
            if ((string)this.tbNosuratP3.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi No Surat Perpanjangan 3!";
                return false;
            }
            if ((string)this.dtTanggalSuratP3.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tanggal Perpanjangan 3!";
                return false;
            }
            if ((string)this.ddlDurasi3.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Durasi Perpanjangan 3!";
                return false;
            }
            if ((string)this.txtFileP3.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Upload File Perpanjangan 3!";
                return false;
            }
            return true;
        }

        private void SaveStatusTahanan()
        {
            if (this.Tag == null)
            {
                StatusTahananService statusTahananService = new StatusTahananService();
                var data = statusTahananService.GetCountByRegId(tbNoReg.Text);
                if (data == 0)
                {
                    InsertData();
                    UserFunction.ClearControls(tableLayoutPanel1);
                    UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Disimpan !");
                    ClearForm();
                    UserFunction.LoadDataStatusTahananToGrid(MainForm.formMain.ListStatusTahanan.GvTahanan);
                    MainForm.formMain.ListStatusTahanan.GvTahanan.Refresh();
                    MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
                    MainForm.formMain.AddNew.Text = "TAMBAH DATA BARU";
                    this.lblTitle.Text = "     TAMBAH DATA BARU";
                    this.Tag = null;
                }
                else
                {
                    UserFunction.MsgBox(TipeMsg.Gagal, "Data Tahanan Sudah pernah Disimpan");
                }
            }
        }

        private void UpdatedData()
        {
            CheckToInmateMaster(tbNoReg.Text);
            StatusTahananService serv = new StatusTahananService();
            var status = new StatusTahanan
            {
                Id = Convert.ToInt32(this.Tag),
                RegID = tbNoReg.Text,
                UpdatedDate = DateTime.Now.ToLocalTime(),
                UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
            };
            serv.Update(status);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.STATUSTAHANAN.ToString(), Activities = "Update Status Tahanan, Data=" + UserFunction.JsonString(status) });

            //Update Perpanjangan
            PerpanjanganTahananService pserv = new PerpanjanganTahananService();

            //Perpanjangan 1
            if (tbNoSuratP1.Text != string.Empty && ddlDurasi1.Text != string.Empty && tbFileP1.Text != string.Empty)
            {
                var pp = pserv.GetByPerpanjanganKeDanStatusId("1", Convert.ToInt32(this.Tag));
                if(pp == null)
                {
                    SavePerpanjangan1(pserv);
                }
                else
                {
                    EditPerpanjangan1(pp, pserv);
                }
            }

            //Perpanjangan 2
            if (tbNoSuratP2.Text != string.Empty&& ddlDurasi2.Text != string.Empty && tbFileP2.Text != string.Empty)
            {
                var pp = pserv.GetByPerpanjanganKeDanStatusId("2", Convert.ToInt32(this.Tag));
                if(pp == null)
                {
                    SavePerpanjangan2(pserv);
                }
                else
                {
                    EditPerpanjangan2(pp, pserv);
                }
                
            }

            //perpanjangan 3
            if (tbNosuratP3.Text != string.Empty && ddlDurasi3.Text != string.Empty && txtFileP3.Text != string.Empty)
            {
                var pp = pserv.GetByPerpanjanganKeDanStatusId("3", Convert.ToInt32(this.Tag));
                if(pp == null)
                {
                    SavePerpanjangan3(pserv);
                }
                else
                {
                    EditPerpanjangan3(pp, pserv);
                }
                
            }

            UserFunction.ClearControls(tableLayoutPanel1);
            UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Disimpan !");
            UserFunction.LoadDataStatusTahananToGrid(MainForm.formMain.ListStatusTahanan.GvTahanan);
            ClearForm();
            MainForm.formMain.ListStatusTahanan.GvTahanan.Refresh();
            MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
            MainForm.formMain.AddNew.Text = "TAMBAH DATA BARU";
            this.lblTitle.Text = "     TAMBAH DATA BARU";
            this.Tag = null;

        }

        private void EditPerpanjangan1(PerpanjanganTahanan pp, PerpanjanganTahananService pserv)
        {
            string dok = string.Empty;
            if (tbFileP1.Text == pp.Dokumen)
                dok = pp.Dokumen;
            else
                dok = "\\Document\\" + filename1;

            var perpanjangan = new PerpanjanganTahanan
            {
                Id = pp.Id,
                StatusTahananId = Convert.ToInt32(this.Tag),
                PerpanjanganKe = "1",
                NoSurat = tbNoSuratP1.Text,
                TanggalPerpanjangan = dtTanggalSuratP1.Value.ToLocalTime(),
                Durasi = Int32.Parse(ddlDurasi1.Text),
                Dokumen = dok,
                UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
                UpdatedDate = DateTime.Now.ToLocalTime()
            };
            pserv.Update(perpanjangan);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.STATUSTAHANAN.ToString(), Activities = "Update Perpanjangan Tahanan, Data=" + UserFunction.JsonString(perpanjangan) });

            if(dok != pp.Dokumen)
            {
                string path = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                System.IO.File.Copy(OpenDIalogfilename1, path + dok);
            }
        }

        private void SavePerpanjangan1(PerpanjanganTahananService pserv)
        {            
            StatusTahananService st = new StatusTahananService();
            var statusTahanan = st.GetByRegId(tbNoReg.Text);
            var perpanjangan = new PerpanjanganTahanan
            {
                StatusTahananId = statusTahanan.Id,
                PerpanjanganKe = "1",
                NoSurat = tbNoSuratP1.Text,
                TanggalPerpanjangan = dtTanggalSuratP1.Value.ToLocalTime(),
                Durasi = Int32.Parse(ddlDurasi1.Text),
                Dokumen = "\\Document\\" + filename1,
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
                CreatedDate = DateTime.Now.ToLocalTime()
            };
            pserv.Post(perpanjangan);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.STATUSTAHANAN.ToString(), Activities = "Add New Perpanjangan Tahanan, Data=" + UserFunction.JsonString(perpanjangan) });
                            
        }

        private void EditPerpanjangan2(PerpanjanganTahanan pp, PerpanjanganTahananService pserv)
        {
            string dok = string.Empty;
            if (tbFileP2.Text == pp.Dokumen)
                dok = pp.Dokumen;
            else
                dok = "\\Document\\" + filename2;

            var perpanjangan = new PerpanjanganTahanan
            {
                Id = pp.Id,
                StatusTahananId = Convert.ToInt32(this.Tag),
                PerpanjanganKe = "2",
                NoSurat = tbNoSuratP2.Text,
                TanggalPerpanjangan = dtTanggalSuratP2.Value.ToLocalTime(),
                Durasi = Int32.Parse(ddlDurasi2.Text),
                Dokumen = "\\Document\\" + filename2,
                UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
                UpdatedDate = DateTime.Now.ToLocalTime()
            };
            pserv.Update(perpanjangan);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.STATUSTAHANAN.ToString(), Activities = "Update Perpanjangan Tahanan, Data=" + UserFunction.JsonString(perpanjangan) });

            if (dok != pp.Dokumen)
            {
                string path = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                System.IO.File.Copy(OpenDIalogfilename2, path + dok);
            }
        }

        private void SavePerpanjangan2(PerpanjanganTahananService pserv)
        {
            StatusTahananService st = new StatusTahananService();
            var statusTahanan = st.GetByRegId(tbNoReg.Text);
            var perp = new PerpanjanganTahanan
            {
                StatusTahananId = statusTahanan.Id,
                PerpanjanganKe = "2",
                NoSurat = tbNoSuratP2.Text,
                TanggalPerpanjangan = dtTanggalSuratP2.Value.ToLocalTime(),
                Durasi = Int32.Parse(ddlDurasi2.Text),
                Dokumen = "\\Document\\" + filename2,
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
                CreatedDate = DateTime.Now.ToLocalTime()
            };
            pserv.Post(perp);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.STATUSTAHANAN.ToString(), Activities = "Add New Perpanjangan Tahanan, Data=" + UserFunction.JsonString(perp) });

            string pathperp = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
            System.IO.File.Copy(OpenDIalogfilename2, pathperp + "\\Document\\" + filename2);
        }

        private void EditPerpanjangan3(PerpanjanganTahanan pp, PerpanjanganTahananService pserv)
        {
            string dok = string.Empty;
            if (txtFileP3.Text == pp.Dokumen)
                dok = pp.Dokumen;
            else
                dok = "\\Document\\" + filename3;

            var perpanjangan = new PerpanjanganTahanan
            {
                Id = pp.Id,
                StatusTahananId = Convert.ToInt32(this.Tag),
                PerpanjanganKe = "3",
                NoSurat = tbNosuratP3.Text,
                TanggalPerpanjangan = dtTanggalSuratP3.Value.ToLocalTime(),
                Durasi = Int32.Parse(ddlDurasi3.Text),
                Dokumen = "\\Document\\" + filename3,
                UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
                UpdatedDate = DateTime.Now.ToLocalTime()
            };
            pserv.Update(perpanjangan);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.STATUSTAHANAN.ToString(), Activities = "Update Perpanjangan Tahanan, Data=" + UserFunction.JsonString(perpanjangan) });

            if (dok != pp.Dokumen)
            {
                string path = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
                System.IO.File.Copy(OpenDIalogfilename3, path + dok);
            }
        }

        private void SavePerpanjangan3(PerpanjanganTahananService pserv)
        {

            StatusTahananService st = new StatusTahananService();
            var statusTahanan = st.GetByRegId(tbNoReg.Text);
            var perpanjangan = new PerpanjanganTahanan
            {
                StatusTahananId = statusTahanan.Id,
                PerpanjanganKe = "3",
                NoSurat = tbNosuratP3.Text,
                TanggalPerpanjangan = dtTanggalSuratP3.Value.ToLocalTime(),
                Durasi = Int32.Parse(ddlDurasi3.Text),
                Dokumen = "\\Document\\" + filename3,
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
                CreatedDate = DateTime.Now.ToLocalTime()
            };
            pserv.Post(perpanjangan);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.STATUSTAHANAN.ToString(), Activities = "Add New Perpanjangan Tahanan, Data=" + UserFunction.JsonString(perpanjangan) });

            string path = Application.StartupPath.Substring(0, (Application.StartupPath.Length - 10));
            System.IO.File.Copy(OpenDIalogfilename3, path + "\\Document\\" + filename3);
        }

        private void InsertData()
        {
            CheckToInmateMaster(tbNoReg.Text);
            CheckDoubleData();
            StatusTahananService serv = new StatusTahananService();
            var status = new StatusTahanan
            {
                RegID = tbNoReg.Text,
                CreatedDate = DateTime.Now.ToLocalTime(),
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
            };
            serv.Post(status);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.STATUSTAHANAN.ToString(), Activities = "Add New Status Tahanan, Data=" + UserFunction.JsonString(status) });

            //Save Perpanjangan
            PerpanjanganTahananService pserv = new PerpanjanganTahananService();

            //Perpanjangan 1
            if(tbNoSuratP1.Text != string.Empty && ddlDurasi1.Text != string.Empty && tbFileP1.Text != string.Empty)
            {
                SavePerpanjangan1(pserv);
            }

            //Perpanjangan 2
            if (tbNoSuratP2.Text != string.Empty && ddlDurasi2.Text != string.Empty && tbFileP2.Text != string.Empty)
            {
                SavePerpanjangan2(pserv);
            }

            //perpanjangan 3
            if (tbNosuratP3.Text != string.Empty && ddlDurasi3.Text != string.Empty && txtFileP3.Text != string.Empty)
            {
                SavePerpanjangan3(pserv);
            }
        }

        private void CheckDoubleData()
        {
            
        }

        private void CheckToInmateMaster(string text)
        {
            string nosuratpenahanan = string.Empty;
            string nosuratpenangkapan = string.Empty;
            DateTime tanggalpenahanan = DateTime.Now;
            DateTime tanggalpenangkapan = DateTime.Now;
            int id = 0;
            InmatesService ss = new InmatesService();
            var x = ss.GetByRegisterId(text);
            id = x.FirstOrDefault().Id;

            var inn = new Inmates();
            inn.Id = id;
            inn.DateOfBirth = x.FirstOrDefault().DateOfBirth;
            inn.DateReg = x.FirstOrDefault().DateReg;
            inn.TanggalKejadian = x.FirstOrDefault().TanggalKejadian;
            inn.NoSuratPenahanan = x.FirstOrDefault().NoSuratPenahanan == null ? tbNoSuratTahan.Text : x.FirstOrDefault().NoSuratPenahanan;
            inn.NoSuratPenangkapan = x.FirstOrDefault().NoSuratPenangkapan == null ? tbNoSuratTangkap.Text : x.FirstOrDefault().NoSuratPenangkapan;
            inn.TanggalPenahanan = x.FirstOrDefault().TanggalPenahanan == dtTanggalPenahanan.Value ? x.FirstOrDefault().TanggalPenahanan : dtTanggalPenahanan.Value;
            inn.TanggalPenangkapan = x.FirstOrDefault().TanggalPenangkapan == dtTanggalPenangkapan.Value ? x.FirstOrDefault().TanggalPenangkapan : dtTanggalPenangkapan.Value;
            inn.UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID);
            inn.UpdatedDate = DateTime.Now.ToLocalTime();

            ss.Update(inn);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Update Inmates, Data=" + UserFunction.JsonString(inn) });
            
        }

        private void InitForm()
        {
            this.lblError.Visible = false;
            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     TAMBAH DATA BARU";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;
            this.lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 15.5f;

            this.lblRequired.LabelElement.CustomFont = Global.MainFont;
            this.lblRequired.LabelElement.CustomFontSize = 14f;
            this.lblRequired.ForeColor = Color.White;
            this.lblRequired.BackColor = Color.FromArgb(77, 77, 77);

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            ddlDurasi1.Items.Add("20");
            ddlDurasi1.Items.Add("30");
            ddlDurasi1.Items.Add("60");
            ddlDurasi1.Items.Add("90");
            ddlDurasi1.Items.Add("120");
            ddlDurasi1.Items.Add("180");

            ddlDurasi2.Items.Add("20");
            ddlDurasi2.Items.Add("30");
            ddlDurasi2.Items.Add("60");
            ddlDurasi2.Items.Add("90");
            ddlDurasi2.Items.Add("120");
            ddlDurasi2.Items.Add("180");

            ddlDurasi3.Items.Add("20");
            ddlDurasi3.Items.Add("30");
            ddlDurasi3.Items.Add("60");
            ddlDurasi3.Items.Add("90");
            ddlDurasi3.Items.Add("120");
            ddlDurasi3.Items.Add("180");
            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ClearForm();
            this.Tag = null;
            MainForm.formMain.AddNew.Text = "TAMBAH DATA BARU";
            this.lblTitle.Text = "     TAMBAH DATA BARU";
            UserFunction.ClearControls(tableLayoutPanel1);
            MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
        }

        private bool DataValid()
        {
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;
            if ((string)this.TxtNama.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Nama!";
                return false;
            }
            if ((string)this.tbNoReg.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi No Registrasi!";
                return false;
            }
            if ((string)this.tbNoSel.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi No Sel!";
                return false;
            }
            if ((string)this.tbNoSuratTangkap.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi No Surat Penangkapan!";
                return false;
            }
            if ((string)this.dtTanggalPenangkapan.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tanggal Penangkapan!";
                return false;
            }
            if ((string)this.tbNoSuratTahan.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi No Surat Penahanan!";
                return false;
            }
            if ((string)this.dtTanggalPenahanan.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tanggal Penahanan!";
                return false;
            }
            return true;
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void tablePerpanjangan3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tablePerpanjangan2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
