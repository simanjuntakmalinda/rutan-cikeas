﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class FormKendaraan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormKendaraan));
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnBack = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            this.headerPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.lbl6 = new Telerik.WinControls.UI.RadLabel();
            this.lbl3 = new Telerik.WinControls.UI.RadLabel();
            this.lbl2 = new Telerik.WinControls.UI.RadLabel();
            this.lbl1 = new Telerik.WinControls.UI.RadLabel();
            this.lbl5 = new Telerik.WinControls.UI.RadLabel();
            this.lbl4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.tableFormCell = new System.Windows.Forms.TableLayoutPanel();
            this.lblDetail = new Telerik.WinControls.UI.RadLabel();
            this.PanelContainer = new Telerik.WinControls.UI.RadScrollablePanelContainer();
            this.lblRequired = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel7 = new Telerik.WinControls.UI.RadPanel();
            this.tpMasuk = new Telerik.WinControls.UI.RadTimePicker();
            this.radPanel6 = new Telerik.WinControls.UI.RadPanel();
            this.txtCatatan = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel5 = new Telerik.WinControls.UI.RadPanel();
            this.txtKeperluan = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.dtMasuk = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.txtPengemudi = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.txtNoPlat = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel8 = new Telerik.WinControls.UI.RadPanel();
            this.ddlKategoriKendaraan = new Telerik.WinControls.UI.RadDropDownList();
            this.lblDetailSurat = new Telerik.WinControls.UI.RadLabel();
            this.btnAddKategori = new Telerik.WinControls.UI.RadButton();
            this.radPanel9 = new Telerik.WinControls.UI.RadPanel();
            this.ddlZonaWaktu = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanelError = new Telerik.WinControls.UI.RadPanel();
            this.lblError = new Telerik.WinControls.UI.RadLabel();
            this.kategoriKendaraan = new RDM.LP.Desktop.Presentation.CustomControls.FormKategoriKendaraan();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).BeginInit();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            this.tableFormCell.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).BeginInit();
            this.radPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tpMasuk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).BeginInit();
            this.radPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatatan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).BeginInit();
            this.radPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeperluan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtMasuk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPengemudi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            this.radPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoPlat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).BeginInit();
            this.radPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlKategoriKendaraan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddKategori)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel9)).BeginInit();
            this.radPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlZonaWaktu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).BeginInit();
            this.radPanelError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).BeginInit();
            this.SuspendLayout();
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnBack);
            this.lblButton.Controls.Add(this.btnSave);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 618);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(998, 118);
            this.lblButton.TabIndex = 74;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.btnBack_Image;
            this.btnBack.Location = new System.Drawing.Point(30, 30);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(244, 58);
            this.btnBack.TabIndex = 11;
            this.btnBack.Tag = "";
            this.btnBack.Text = "&BATAL";
            this.btnBack.ThemeName = "MaterialBlueGrey";
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(663, 30);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(305, 58);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "&SIMPAN";
            this.btnSave.ThemeName = "MaterialBlueGrey";
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.lblTitle);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Margin = new System.Windows.Forms.Padding(4);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(998, 56);
            this.headerPanel.TabIndex = 81;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(998, 56);
            this.lblTitle.TabIndex = 8;
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = false;
            this.lbl6.Location = new System.Drawing.Point(14, 414);
            this.lbl6.Margin = new System.Windows.Forms.Padding(4);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(317, 42);
            this.lbl6.TabIndex = 120;
            this.lbl6.Text = "<html>Cell Status<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = false;
            this.lbl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl3.Location = new System.Drawing.Point(14, 164);
            this.lbl3.Margin = new System.Windows.Forms.Padding(4);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(317, 42);
            this.lbl3.TabIndex = 65;
            this.lbl3.Text = "<html>Cell Floor<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = false;
            this.lbl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl2.Location = new System.Drawing.Point(14, 114);
            this.lbl2.Margin = new System.Windows.Forms.Padding(4);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(317, 42);
            this.lbl2.TabIndex = 118;
            this.lbl2.Text = "<html>Building Name<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = false;
            this.lbl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl1.Location = new System.Drawing.Point(14, 64);
            this.lbl1.Margin = new System.Windows.Forms.Padding(4);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(317, 42);
            this.lbl1.TabIndex = 19;
            this.lbl1.Text = "<html>Cell Code<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = false;
            this.lbl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl5.Location = new System.Drawing.Point(14, 264);
            this.lbl5.Margin = new System.Windows.Forms.Padding(4);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(317, 42);
            this.lbl5.TabIndex = 66;
            this.lbl5.Text = "<html>Cell Description<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = false;
            this.lbl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl4.Location = new System.Drawing.Point(14, 214);
            this.lbl4.Margin = new System.Windows.Forms.Padding(4);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(317, 42);
            this.lbl4.TabIndex = 20;
            this.lbl4.Text = "<html>Cell Number<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel5.Location = new System.Drawing.Point(14, 364);
            this.radLabel5.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(317, 42);
            this.radLabel5.TabIndex = 121;
            this.radLabel5.Text = "<html>Cell Type<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // tableFormCell
            // 
            this.tableFormCell.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tableFormCell.ColumnCount = 3;
            this.tableFormCell.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tableFormCell.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableFormCell.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableFormCell.Controls.Add(this.lblDetail, 0, 0);
            this.tableFormCell.Controls.Add(this.radLabel5, 0, 7);
            this.tableFormCell.Controls.Add(this.lbl4, 0, 4);
            this.tableFormCell.Controls.Add(this.lbl5, 0, 5);
            this.tableFormCell.Controls.Add(this.lbl1, 0, 1);
            this.tableFormCell.Controls.Add(this.lbl2, 0, 2);
            this.tableFormCell.Controls.Add(this.lbl3, 0, 3);
            this.tableFormCell.Controls.Add(this.lbl6, 0, 8);
            this.tableFormCell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableFormCell.Location = new System.Drawing.Point(0, 0);
            this.tableFormCell.Margin = new System.Windows.Forms.Padding(10);
            this.tableFormCell.Name = "tableFormCell";
            this.tableFormCell.Padding = new System.Windows.Forms.Padding(10);
            this.tableFormCell.RowCount = 10;
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.Size = new System.Drawing.Size(996, 530);
            this.tableFormCell.TabIndex = 17;
            // 
            // lblDetail
            // 
            this.lblDetail.AutoSize = false;
            this.tableFormCell.SetColumnSpan(this.lblDetail, 3);
            this.lblDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetail.Image = ((System.Drawing.Image)(resources.GetObject("lblDetail.Image")));
            this.lblDetail.Location = new System.Drawing.Point(13, 13);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetail.Size = new System.Drawing.Size(970, 44);
            this.lblDetail.TabIndex = 130;
            this.lblDetail.Text = "         DETAIL CELL DATA";
            // 
            // PanelContainer
            // 
            this.PanelContainer.AutoScroll = false;
            this.PanelContainer.Dock = System.Windows.Forms.DockStyle.None;
            this.PanelContainer.Location = new System.Drawing.Point(0, 0);
            this.PanelContainer.Size = new System.Drawing.Size(979, 548);
            // 
            // lblRequired
            // 
            this.lblRequired.AutoSize = false;
            this.lblRequired.BackColor = System.Drawing.SystemColors.Control;
            this.lblRequired.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblRequired.Location = new System.Drawing.Point(0, 56);
            this.lblRequired.Name = "lblRequired";
            this.lblRequired.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblRequired.Size = new System.Drawing.Size(998, 52);
            this.lblRequired.TabIndex = 84;
            this.lblRequired.Text = "Mohon Isi seluruh Kolom yang Diperlukan";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.BackColor = System.Drawing.SystemColors.Control;
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 108);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(996, 448);
            this.radScrollablePanel1.Size = new System.Drawing.Size(998, 450);
            this.radScrollablePanel1.TabIndex = 85;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.03291F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.62069F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.3464F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 234F));
            this.tableLayoutPanel1.Controls.Add(this.radPanel7, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.radPanel6, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.radPanel5, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.radLabel3, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.radLabel4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.radLabel6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.radLabel7, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radLabel8, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.radLabel9, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel8, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailSurat, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnAddKategori, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel9, 3, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(996, 448);
            this.tableLayoutPanel1.TabIndex = 71;
            // 
            // radPanel7
            // 
            this.radPanel7.Controls.Add(this.tpMasuk);
            this.radPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel7.Location = new System.Drawing.Point(507, 213);
            this.radPanel7.Name = "radPanel7";
            this.radPanel7.Size = new System.Drawing.Size(241, 44);
            this.radPanel7.TabIndex = 6;
            // 
            // tpMasuk
            // 
            this.tpMasuk.AutoSize = false;
            this.tpMasuk.Culture = new System.Globalization.CultureInfo("id-ID");
            this.tpMasuk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tpMasuk.Location = new System.Drawing.Point(0, 0);
            this.tpMasuk.MaxValue = new System.DateTime(9999, 12, 31, 23, 59, 59, 0);
            this.tpMasuk.MinValue = new System.DateTime(((long)(0)));
            this.tpMasuk.Name = "tpMasuk";
            this.tpMasuk.Size = new System.Drawing.Size(241, 44);
            this.tpMasuk.TabIndex = 6;
            this.tpMasuk.TabStop = false;
            this.tpMasuk.Value = new System.DateTime(2019, 2, 1, 9, 47, 0, 0);
            // 
            // radPanel6
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel6, 2);
            this.radPanel6.Controls.Add(this.txtCatatan);
            this.radPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel6.Location = new System.Drawing.Point(258, 313);
            this.radPanel6.Name = "radPanel6";
            this.radPanel6.Size = new System.Drawing.Size(490, 94);
            this.radPanel6.TabIndex = 9;
            // 
            // txtCatatan
            // 
            this.txtCatatan.AutoSize = false;
            this.txtCatatan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCatatan.Location = new System.Drawing.Point(0, 0);
            this.txtCatatan.Multiline = true;
            this.txtCatatan.Name = "txtCatatan";
            this.txtCatatan.Size = new System.Drawing.Size(490, 94);
            this.txtCatatan.TabIndex = 9;
            // 
            // radPanel5
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel5, 2);
            this.radPanel5.Controls.Add(this.txtKeperluan);
            this.radPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel5.Location = new System.Drawing.Point(258, 263);
            this.radPanel5.Name = "radPanel5";
            this.radPanel5.Size = new System.Drawing.Size(490, 44);
            this.radPanel5.TabIndex = 8;
            // 
            // txtKeperluan
            // 
            this.txtKeperluan.AutoSize = false;
            this.txtKeperluan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtKeperluan.Location = new System.Drawing.Point(0, 0);
            this.txtKeperluan.Multiline = true;
            this.txtKeperluan.Name = "txtKeperluan";
            this.txtKeperluan.Size = new System.Drawing.Size(490, 44);
            this.txtKeperluan.TabIndex = 8;
            // 
            // radPanel3
            // 
            this.radPanel3.Controls.Add(this.dtMasuk);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Location = new System.Drawing.Point(258, 213);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(243, 44);
            this.radPanel3.TabIndex = 5;
            // 
            // dtMasuk
            // 
            this.dtMasuk.AutoSize = false;
            this.dtMasuk.Culture = new System.Globalization.CultureInfo("id-ID");
            this.dtMasuk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtMasuk.Location = new System.Drawing.Point(0, 0);
            this.dtMasuk.Name = "dtMasuk";
            this.dtMasuk.Size = new System.Drawing.Size(243, 44);
            this.dtMasuk.TabIndex = 5;
            this.dtMasuk.TabStop = false;
            this.dtMasuk.Text = "Senin, 11 Februari 2019";
            this.dtMasuk.Value = new System.DateTime(2019, 2, 11, 13, 55, 33, 666);
            // 
            // radPanel2
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel2, 2);
            this.radPanel2.Controls.Add(this.txtPengemudi);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(258, 163);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(490, 44);
            this.radPanel2.TabIndex = 4;
            // 
            // txtPengemudi
            // 
            this.txtPengemudi.AutoSize = false;
            this.txtPengemudi.Location = new System.Drawing.Point(0, 0);
            this.txtPengemudi.Multiline = true;
            this.txtPengemudi.Name = "txtPengemudi";
            this.txtPengemudi.Size = new System.Drawing.Size(496, 44);
            this.txtPengemudi.TabIndex = 4;
            // 
            // radPanel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel1, 2);
            this.radPanel1.Controls.Add(this.radPanel4);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(258, 113);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(490, 44);
            this.radPanel1.TabIndex = 3;
            // 
            // radPanel4
            // 
            this.radPanel4.Controls.Add(this.txtNoPlat);
            this.radPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel4.Location = new System.Drawing.Point(0, 0);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(490, 44);
            this.radPanel4.TabIndex = 3;
            // 
            // txtNoPlat
            // 
            this.txtNoPlat.AutoSize = false;
            this.txtNoPlat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoPlat.Location = new System.Drawing.Point(0, 0);
            this.txtNoPlat.Multiline = true;
            this.txtNoPlat.Name = "txtNoPlat";
            this.txtNoPlat.Size = new System.Drawing.Size(490, 44);
            this.txtNoPlat.TabIndex = 3;
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel3.Location = new System.Drawing.Point(14, 314);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(237, 92);
            this.radLabel3.TabIndex = 121;
            this.radLabel3.Text = "<html>Catatan</html>";
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel4.Location = new System.Drawing.Point(14, 214);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(237, 42);
            this.radLabel4.TabIndex = 20;
            this.radLabel4.Text = "<html>Tanggal dan Waktu Masuk<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel6.Location = new System.Drawing.Point(14, 264);
            this.radLabel6.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(237, 42);
            this.radLabel6.TabIndex = 66;
            this.radLabel6.Text = "<html>Keperluan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel7.Location = new System.Drawing.Point(14, 64);
            this.radLabel7.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(237, 42);
            this.radLabel7.TabIndex = 19;
            this.radLabel7.Text = "<html>Kategori Kendaraan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel8.Location = new System.Drawing.Point(14, 114);
            this.radLabel8.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(237, 42);
            this.radLabel8.TabIndex = 118;
            this.radLabel8.Text = "<html>Nomor Plat<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel9.Location = new System.Drawing.Point(14, 164);
            this.radLabel9.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(237, 42);
            this.radLabel9.TabIndex = 65;
            this.radLabel9.Text = "<html>Pengemudi<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel8
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel8, 2);
            this.radPanel8.Controls.Add(this.ddlKategoriKendaraan);
            this.radPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel8.Location = new System.Drawing.Point(258, 63);
            this.radPanel8.Name = "radPanel8";
            this.radPanel8.Size = new System.Drawing.Size(490, 44);
            this.radPanel8.TabIndex = 1;
            // 
            // ddlKategoriKendaraan
            // 
            this.ddlKategoriKendaraan.AutoSize = false;
            this.ddlKategoriKendaraan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlKategoriKendaraan.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Tag = "1";
            radListDataItem1.Text = "User";
            radListDataItem2.Tag = "2";
            radListDataItem2.Text = "Superuser";
            this.ddlKategoriKendaraan.Items.Add(radListDataItem1);
            this.ddlKategoriKendaraan.Items.Add(radListDataItem2);
            this.ddlKategoriKendaraan.Location = new System.Drawing.Point(0, 0);
            this.ddlKategoriKendaraan.Name = "ddlKategoriKendaraan";
            this.ddlKategoriKendaraan.Size = new System.Drawing.Size(490, 44);
            this.ddlKategoriKendaraan.TabIndex = 1;
            // 
            // lblDetailSurat
            // 
            this.lblDetailSurat.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailSurat, 5);
            this.lblDetailSurat.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailSurat.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.kendaraan;
            this.lblDetailSurat.Location = new System.Drawing.Point(13, 13);
            this.lblDetailSurat.Name = "lblDetailSurat";
            this.lblDetailSurat.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetailSurat.Size = new System.Drawing.Size(970, 44);
            this.lblDetailSurat.TabIndex = 138;
            this.lblDetailSurat.Text = "          DETAIL KENDARAAN";
            // 
            // btnAddKategori
            // 
            this.btnAddKategori.AutoSize = true;
            this.btnAddKategori.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAddKategori.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.add_doc;
            this.btnAddKategori.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAddKategori.Location = new System.Drawing.Point(754, 63);
            this.btnAddKategori.Name = "btnAddKategori";
            this.btnAddKategori.Size = new System.Drawing.Size(27, 27);
            this.btnAddKategori.TabIndex = 2;
            // 
            // radPanel9
            // 
            this.radPanel9.Controls.Add(this.ddlZonaWaktu);
            this.radPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel9.Location = new System.Drawing.Point(754, 213);
            this.radPanel9.Name = "radPanel9";
            this.radPanel9.Size = new System.Drawing.Size(229, 44);
            this.radPanel9.TabIndex = 7;
            // 
            // ddlZonaWaktu
            // 
            this.ddlZonaWaktu.AutoSize = false;
            this.ddlZonaWaktu.BackColor = System.Drawing.Color.White;
            this.ddlZonaWaktu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlZonaWaktu.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlZonaWaktu.DropDownHeight = 200;
            this.ddlZonaWaktu.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlZonaWaktu.Location = new System.Drawing.Point(0, 0);
            this.ddlZonaWaktu.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlZonaWaktu.Name = "ddlZonaWaktu";
            // 
            // 
            // 
            this.ddlZonaWaktu.RootElement.CustomFont = "Roboto";
            this.ddlZonaWaktu.RootElement.CustomFontSize = 13F;
            this.ddlZonaWaktu.Size = new System.Drawing.Size(229, 44);
            this.ddlZonaWaktu.TabIndex = 7;
            this.ddlZonaWaktu.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlZonaWaktu.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlZonaWaktu.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlZonaWaktu.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlZonaWaktu.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlZonaWaktu.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlZonaWaktu.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlZonaWaktu.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlZonaWaktu.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlZonaWaktu.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlZonaWaktu.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanelError
            // 
            this.radPanelError.Controls.Add(this.lblError);
            this.radPanelError.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanelError.Location = new System.Drawing.Point(0, 558);
            this.radPanelError.Margin = new System.Windows.Forms.Padding(0);
            this.radPanelError.Name = "radPanelError";
            this.radPanelError.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radPanelError.Size = new System.Drawing.Size(998, 43);
            this.radPanelError.TabIndex = 86;
            // 
            // lblError
            // 
            this.lblError.AutoSize = false;
            this.lblError.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.warning;
            this.lblError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblError.Location = new System.Drawing.Point(15, 0);
            this.lblError.Name = "lblError";
            this.lblError.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.lblError.Size = new System.Drawing.Size(983, 39);
            this.lblError.TabIndex = 24;
            // 
            // kategoriKendaraan
            // 
            this.kategoriKendaraan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.kategoriKendaraan.Location = new System.Drawing.Point(1000, 1000);
            this.kategoriKendaraan.Name = "kategoriKendaraan";
            this.kategoriKendaraan.Padding = new System.Windows.Forms.Padding(5);
            this.kategoriKendaraan.Size = new System.Drawing.Size(791, 409);
            this.kategoriKendaraan.TabIndex = 83;
            // 
            // FormKendaraan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.kategoriKendaraan);
            this.Controls.Add(this.radPanelError);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblRequired);
            this.Controls.Add(this.headerPanel);
            this.Controls.Add(this.lblButton);
            this.Name = "FormKendaraan";
            this.Size = new System.Drawing.Size(998, 736);
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).EndInit();
            this.headerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            this.tableFormCell.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRequired)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).EndInit();
            this.radPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tpMasuk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).EndInit();
            this.radPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCatatan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).EndInit();
            this.radPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtKeperluan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtMasuk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPengemudi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            this.radPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNoPlat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).EndInit();
            this.radPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlKategoriKendaraan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddKategori)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel9)).EndInit();
            this.radPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlZonaWaktu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).EndInit();
            this.radPanelError.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnBack;
        private Telerik.WinControls.UI.RadButton btnSave;
        private Telerik.WinControls.UI.RadPanel headerPanel;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel lbl6;
        private System.Windows.Forms.TableLayoutPanel tableFormCell;
        private Telerik.WinControls.UI.RadLabel lblDetail;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel lbl4;
        private Telerik.WinControls.UI.RadLabel lbl5;
        private Telerik.WinControls.UI.RadLabel lbl1;
        private Telerik.WinControls.UI.RadLabel lbl2;
        private Telerik.WinControls.UI.RadLabel lbl3;
        private Telerik.WinControls.UI.RadLabel lblRequired;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private Telerik.WinControls.UI.RadPanel radPanelError;
        private Telerik.WinControls.UI.RadLabel lblError;
        private Telerik.WinControls.UI.RadScrollablePanelContainer PanelContainer;
        private FormKategoriKendaraan kategoriKendaraan;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel7;
        private Telerik.WinControls.UI.RadTimePicker tpMasuk;
        private Telerik.WinControls.UI.RadPanel radPanel6;
        private Telerik.WinControls.UI.RadTextBox txtCatatan;
        private Telerik.WinControls.UI.RadPanel radPanel5;
        private Telerik.WinControls.UI.RadTextBox txtKeperluan;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadDateTimePicker dtMasuk;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadTextBox txtPengemudi;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadTextBox txtNoPlat;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadPanel radPanel8;
        private Telerik.WinControls.UI.RadDropDownList ddlKategoriKendaraan;
        private Telerik.WinControls.UI.RadLabel lblDetailSurat;
        private Telerik.WinControls.UI.RadButton btnAddKategori;
        private Telerik.WinControls.UI.RadPanel radPanel9;
        private Telerik.WinControls.UI.RadDropDownList ddlZonaWaktu;
    }
}
