﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class ListKunjungan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListKunjungan));
            this.lblDetail = new Telerik.WinControls.UI.RadLabel();
            this.fingerCheckOut = new System.Windows.Forms.Button();
            this.pdf = new System.Windows.Forms.PictureBox();
            this.excel = new System.Windows.Forms.PictureBox();
            this.gvVisit = new Telerik.WinControls.UI.RadGridView();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.picRefresh = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).BeginInit();
            this.lblDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisit.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRefresh)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDetail
            // 
            this.lblDetail.AutoSize = false;
            this.lblDetail.BackColor = System.Drawing.Color.Gainsboro;
            this.lblDetail.Controls.Add(this.picRefresh);
            this.lblDetail.Controls.Add(this.fingerCheckOut);
            this.lblDetail.Controls.Add(this.pdf);
            this.lblDetail.Controls.Add(this.excel);
            this.lblDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetail.Location = new System.Drawing.Point(0, 56);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Padding = new System.Windows.Forms.Padding(5, 5, 0, 5);
            this.lblDetail.Size = new System.Drawing.Size(1349, 58);
            this.lblDetail.TabIndex = 16;
            // 
            // fingerCheckOut
            // 
            this.fingerCheckOut.BackColor = System.Drawing.Color.White;
            this.fingerCheckOut.Dock = System.Windows.Forms.DockStyle.Left;
            this.fingerCheckOut.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fingerCheckOut.Image = ((System.Drawing.Image)(resources.GetObject("fingerCheckOut.Image")));
            this.fingerCheckOut.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.fingerCheckOut.Location = new System.Drawing.Point(5, 5);
            this.fingerCheckOut.Name = "fingerCheckOut";
            this.fingerCheckOut.Padding = new System.Windows.Forms.Padding(5);
            this.fingerCheckOut.Size = new System.Drawing.Size(551, 48);
            this.fingerCheckOut.TabIndex = 94;
            this.fingerCheckOut.Text = "    KLIK DISINI, UNTUK KELUAR DENGAN MEMINDAI SIDIK JARI";
            this.fingerCheckOut.UseVisualStyleBackColor = false;
            // 
            // pdf
            // 
            this.pdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pdf.Dock = System.Windows.Forms.DockStyle.Right;
            this.pdf.Image = ((System.Drawing.Image)(resources.GetObject("pdf.Image")));
            this.pdf.Location = new System.Drawing.Point(1263, 5);
            this.pdf.Name = "pdf";
            this.pdf.Size = new System.Drawing.Size(43, 48);
            this.pdf.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pdf.TabIndex = 5;
            this.pdf.TabStop = false;
            // 
            // excel
            // 
            this.excel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.excel.Dock = System.Windows.Forms.DockStyle.Right;
            this.excel.Image = ((System.Drawing.Image)(resources.GetObject("excel.Image")));
            this.excel.Location = new System.Drawing.Point(1306, 5);
            this.excel.Name = "excel";
            this.excel.Size = new System.Drawing.Size(43, 48);
            this.excel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.excel.TabIndex = 4;
            this.excel.TabStop = false;
            // 
            // gvVisit
            // 
            this.gvVisit.AutoScroll = true;
            this.gvVisit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvVisit.Location = new System.Drawing.Point(0, 114);
            // 
            // 
            // 
            this.gvVisit.MasterTemplate.AllowAddNewRow = false;
            this.gvVisit.MasterTemplate.AllowCellContextMenu = false;
            this.gvVisit.MasterTemplate.AllowColumnChooser = false;
            this.gvVisit.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.gvVisit.MasterTemplate.AllowColumnReorder = false;
            this.gvVisit.MasterTemplate.AllowDragToGroup = false;
            this.gvVisit.MasterTemplate.EnablePaging = true;
            this.gvVisit.MasterTemplate.PageSize = 10;
            this.gvVisit.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gvVisit.Name = "gvVisit";
            this.gvVisit.Size = new System.Drawing.Size(1349, 407);
            this.gvVisit.TabIndex = 58;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(1349, 56);
            this.lblTitle.TabIndex = 1;
            // 
            // picRefresh
            // 
            this.picRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picRefresh.Dock = System.Windows.Forms.DockStyle.Right;
            this.picRefresh.Image = ((System.Drawing.Image)(resources.GetObject("picRefresh.Image")));
            this.picRefresh.Location = new System.Drawing.Point(1220, 5);
            this.picRefresh.Name = "picRefresh";
            this.picRefresh.Size = new System.Drawing.Size(43, 48);
            this.picRefresh.TabIndex = 95;
            this.picRefresh.TabStop = false;
            // 
            // ListKunjungan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gvVisit);
            this.Controls.Add(this.lblDetail);
            this.Controls.Add(this.lblTitle);
            this.Name = "ListKunjungan";
            this.Size = new System.Drawing.Size(1349, 521);
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).EndInit();
            this.lblDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisit.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRefresh)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel lblDetail;
        private Telerik.WinControls.UI.RadGridView gvVisit;
        private System.Windows.Forms.PictureBox pdf;
        private System.Windows.Forms.PictureBox excel;
        private System.Windows.Forms.Button fingerCheckOut;
        private System.Windows.Forms.PictureBox picRefresh;
    }
}
