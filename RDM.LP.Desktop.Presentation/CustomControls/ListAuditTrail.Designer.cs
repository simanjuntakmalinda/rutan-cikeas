﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class ListAuditTrail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListAuditTrail));
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.ddlStart = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.ddlUser = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.btnSubmit = new Telerik.WinControls.UI.RadButton();
            this.radPanel8 = new Telerik.WinControls.UI.RadPanel();
            this.ddlEnd = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.gvAuditTrail = new Telerik.WinControls.UI.RadGridView();
            this.lblDetail = new Telerik.WinControls.UI.RadLabel();
            this.pdf = new System.Windows.Forms.PictureBox();
            this.excel = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSubmit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).BeginInit();
            this.radPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvAuditTrail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAuditTrail.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).BeginInit();
            this.lblDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excel)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(1349, 56);
            this.lblTitle.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 111F));
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.radLabel24, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.radLabel3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnSubmit, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel8, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 56);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1349, 248);
            this.tableLayoutPanel1.TabIndex = 59;
            // 
            // radPanel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel3, 2);
            this.radPanel3.Controls.Add(this.ddlStart);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Location = new System.Drawing.Point(418, 63);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(806, 44);
            this.radPanel3.TabIndex = 193;
            // 
            // ddlStart
            // 
            this.ddlStart.AutoSize = false;
            this.ddlStart.BackColor = System.Drawing.Color.Transparent;
            this.ddlStart.CalendarSize = new System.Drawing.Size(290, 320);
            this.ddlStart.Culture = new System.Globalization.CultureInfo("id-ID");
            this.ddlStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlStart.Location = new System.Drawing.Point(0, 0);
            this.ddlStart.Margin = new System.Windows.Forms.Padding(0);
            this.ddlStart.Name = "ddlStart";
            this.ddlStart.Size = new System.Drawing.Size(806, 44);
            this.ddlStart.TabIndex = 130;
            this.ddlStart.TabStop = false;
            this.ddlStart.Text = "Kamis, 28 Juni 2018";
            this.ddlStart.Value = new System.DateTime(2018, 6, 28, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.ddlStart.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlStart.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlStart.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlStart.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlStart.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlStart.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlStart.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.ddlStart.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "Kamis, 28 Juni 2018";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.ddlStart.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // radPanel2
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel2, 2);
            this.radPanel2.Controls.Add(this.ddlUser);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(418, 13);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(806, 44);
            this.radPanel2.TabIndex = 193;
            // 
            // ddlUser
            // 
            this.ddlUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlUser.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Tag = "1";
            radListDataItem1.Text = "User";
            radListDataItem2.Tag = "2";
            radListDataItem2.Text = "Superuser";
            this.ddlUser.Items.Add(radListDataItem1);
            this.ddlUser.Items.Add(radListDataItem2);
            this.ddlUser.Location = new System.Drawing.Point(0, 0);
            this.ddlUser.Name = "ddlUser";
            this.ddlUser.Size = new System.Drawing.Size(806, 44);
            this.ddlUser.TabIndex = 132;
            // 
            // radLabel24
            // 
            this.radLabel24.AutoSize = false;
            this.radLabel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel24.Location = new System.Drawing.Point(14, 14);
            this.radLabel24.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel24.Name = "radLabel24";
            this.radLabel24.Size = new System.Drawing.Size(397, 42);
            this.radLabel24.TabIndex = 19;
            this.radLabel24.Text = "<html>Nama Pengguna<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.Location = new System.Drawing.Point(14, 114);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(397, 42);
            this.radLabel1.TabIndex = 65;
            this.radLabel1.Text = "<html>Tanggal Berakhir<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel3.Location = new System.Drawing.Point(14, 64);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(397, 42);
            this.radLabel3.TabIndex = 118;
            this.radLabel3.Text = "<html>Tanggal Mulai<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSubmit.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.if_checked_2_309094;
            this.btnSubmit.Location = new System.Drawing.Point(1004, 163);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(220, 44);
            this.btnSubmit.TabIndex = 133;
            this.btnSubmit.Text = "&Tampilkan";
            this.btnSubmit.ThemeName = "MaterialBlueGrey";
            // 
            // radPanel8
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel8, 2);
            this.radPanel8.Controls.Add(this.ddlEnd);
            this.radPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel8.Location = new System.Drawing.Point(418, 113);
            this.radPanel8.Name = "radPanel8";
            this.radPanel8.Size = new System.Drawing.Size(806, 44);
            this.radPanel8.TabIndex = 192;
            // 
            // ddlEnd
            // 
            this.ddlEnd.AutoSize = false;
            this.ddlEnd.BackColor = System.Drawing.Color.Transparent;
            this.ddlEnd.CalendarSize = new System.Drawing.Size(290, 320);
            this.ddlEnd.Culture = new System.Globalization.CultureInfo("id-ID");
            this.ddlEnd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlEnd.Location = new System.Drawing.Point(0, 0);
            this.ddlEnd.Margin = new System.Windows.Forms.Padding(0);
            this.ddlEnd.Name = "ddlEnd";
            this.ddlEnd.Size = new System.Drawing.Size(806, 44);
            this.ddlEnd.TabIndex = 131;
            this.ddlEnd.TabStop = false;
            this.ddlEnd.Text = "Kamis, 28 Juni 2018";
            this.ddlEnd.Value = new System.DateTime(2018, 6, 28, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.ddlEnd.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlEnd.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlEnd.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlEnd.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlEnd.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlEnd.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlEnd.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.ddlEnd.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "Kamis, 28 Juni 2018";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.ddlEnd.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.gvAuditTrail);
            this.radPanel1.Controls.Add(this.lblDetail);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 304);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Padding = new System.Windows.Forms.Padding(10, 10, 10, 0);
            this.radPanel1.Size = new System.Drawing.Size(1349, 473);
            this.radPanel1.TabIndex = 60;
            // 
            // gvAuditTrail
            // 
            this.gvAuditTrail.AutoScroll = true;
            this.gvAuditTrail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvAuditTrail.Location = new System.Drawing.Point(10, 62);
            this.gvAuditTrail.Margin = new System.Windows.Forms.Padding(0);
            // 
            // 
            // 
            this.gvAuditTrail.MasterTemplate.AllowAddNewRow = false;
            this.gvAuditTrail.MasterTemplate.AllowCellContextMenu = false;
            this.gvAuditTrail.MasterTemplate.AllowColumnChooser = false;
            this.gvAuditTrail.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.gvAuditTrail.MasterTemplate.AllowColumnReorder = false;
            this.gvAuditTrail.MasterTemplate.AllowDragToGroup = false;
            this.gvAuditTrail.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gvAuditTrail.Name = "gvAuditTrail";
            // 
            // 
            // 
            this.gvAuditTrail.RootElement.BorderHighlightColor = System.Drawing.Color.Black;
            this.gvAuditTrail.RootElement.FocusBorderColor = System.Drawing.Color.Black;
            this.gvAuditTrail.Size = new System.Drawing.Size(1329, 411);
            this.gvAuditTrail.TabIndex = 65;
            // 
            // lblDetail
            // 
            this.lblDetail.AutoSize = false;
            this.lblDetail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.lblDetail.Controls.Add(this.pdf);
            this.lblDetail.Controls.Add(this.excel);
            this.lblDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetail.ForeColor = System.Drawing.Color.White;
            this.lblDetail.Location = new System.Drawing.Point(10, 10);
            this.lblDetail.Margin = new System.Windows.Forms.Padding(10);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDetail.Size = new System.Drawing.Size(1329, 52);
            this.lblDetail.TabIndex = 64;
            this.lblDetail.Text = "DETAIL DATA";
            // 
            // pdf
            // 
            this.pdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pdf.Dock = System.Windows.Forms.DockStyle.Right;
            this.pdf.Image = ((System.Drawing.Image)(resources.GetObject("pdf.Image")));
            this.pdf.Location = new System.Drawing.Point(1243, 10);
            this.pdf.Name = "pdf";
            this.pdf.Size = new System.Drawing.Size(43, 32);
            this.pdf.TabIndex = 3;
            this.pdf.TabStop = false;
            // 
            // excel
            // 
            this.excel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.excel.Dock = System.Windows.Forms.DockStyle.Right;
            this.excel.Image = ((System.Drawing.Image)(resources.GetObject("excel.Image")));
            this.excel.Location = new System.Drawing.Point(1286, 10);
            this.excel.Name = "excel";
            this.excel.Size = new System.Drawing.Size(43, 32);
            this.excel.TabIndex = 2;
            this.excel.TabStop = false;
            // 
            // ListAuditTrail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radPanel1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.lblTitle);
            this.Name = "ListAuditTrail";
            this.Size = new System.Drawing.Size(1349, 777);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            this.radPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSubmit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).EndInit();
            this.radPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvAuditTrail.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAuditTrail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).EndInit();
            this.lblDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lblTitle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel24;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadDateTimePicker ddlStart;
        private Telerik.WinControls.UI.RadDateTimePicker ddlEnd;
        private Telerik.WinControls.UI.RadDropDownList ddlUser;
        private Telerik.WinControls.UI.RadButton btnSubmit;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadGridView gvAuditTrail;
        private Telerik.WinControls.UI.RadLabel lblDetail;
        private System.Windows.Forms.PictureBox pdf;
        private System.Windows.Forms.PictureBox excel;
        private Telerik.WinControls.UI.RadPanel radPanel8;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadPanel radPanel2;
    }
}
