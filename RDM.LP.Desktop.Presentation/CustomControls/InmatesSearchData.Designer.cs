﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class InmatesSearchData
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InmatesSearchData));
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.lblDetail = new Telerik.WinControls.UI.RadLabel();
            this.txtSearch = new Telerik.WinControls.UI.RadTextBox();
            this.btnSearch = new Telerik.WinControls.UI.RadButton();
            this.lblCari = new Telerik.WinControls.UI.RadLabel();
            this.gvData = new Telerik.WinControls.UI.RadGridView();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).BeginInit();
            this.lblDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearch)).BeginInit();
            this.txtSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCari)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvData.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDetail
            // 
            this.lblDetail.AutoSize = false;
            this.lblDetail.BackColor = System.Drawing.Color.Gainsboro;
            this.lblDetail.Controls.Add(this.txtSearch);
            this.lblDetail.Controls.Add(this.lblCari);
            this.lblDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetail.Location = new System.Drawing.Point(0, 70);
            this.lblDetail.Margin = new System.Windows.Forms.Padding(0);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Padding = new System.Windows.Forms.Padding(300, 15, 90, 15);
            this.lblDetail.Size = new System.Drawing.Size(888, 62);
            this.lblDetail.TabIndex = 16;
            // 
            // txtSearch
            // 
            this.txtSearch.AutoSize = false;
            this.txtSearch.Controls.Add(this.btnSearch);
            this.txtSearch.Dock = System.Windows.Forms.DockStyle.Right;
            this.txtSearch.Location = new System.Drawing.Point(412, 15);
            this.txtSearch.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Padding = new System.Windows.Forms.Padding(3, 3, 100, 3);
            this.txtSearch.Size = new System.Drawing.Size(386, 32);
            this.txtSearch.TabIndex = 134;
            // 
            // btnSearch
            // 
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(329, 0);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(0);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(57, 36);
            this.btnSearch.TabIndex = 131;
            this.btnSearch.ThemeName = "MaterialBlueGrey";
            // 
            // lblCari
            // 
            this.lblCari.Location = new System.Drawing.Point(10, 22);
            this.lblCari.Name = "lblCari";
            this.lblCari.Size = new System.Drawing.Size(150, 18);
            this.lblCari.TabIndex = 128;
            this.lblCari.Text = "PILIH KATEGORI PENCARIAN";
            // 
            // gvData
            // 
            this.gvData.AutoScroll = true;
            this.gvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvData.Location = new System.Drawing.Point(0, 132);
            // 
            // 
            // 
            this.gvData.MasterTemplate.AllowAddNewRow = false;
            this.gvData.MasterTemplate.AllowCellContextMenu = false;
            this.gvData.MasterTemplate.AllowColumnChooser = false;
            this.gvData.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.gvData.MasterTemplate.AllowColumnReorder = false;
            this.gvData.MasterTemplate.AllowDragToGroup = false;
            this.gvData.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gvData.Name = "gvData";
            this.gvData.Size = new System.Drawing.Size(888, 404);
            this.gvData.TabIndex = 58;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lblTitle.Size = new System.Drawing.Size(888, 70);
            this.lblTitle.TabIndex = 1;
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(818, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 70);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 2;
            this.exit.TabStop = false;
            // 
            // InmatesSearchData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.gvData);
            this.Controls.Add(this.lblDetail);
            this.Controls.Add(this.lblTitle);
            this.Name = "InmatesSearchData";
            this.Size = new System.Drawing.Size(888, 536);
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).EndInit();
            this.lblDetail.ResumeLayout(false);
            this.lblDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearch)).EndInit();
            this.txtSearch.ResumeLayout(false);
            this.txtSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCari)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvData.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel lblDetail;
        private Telerik.WinControls.UI.RadGridView gvData;
        private System.Windows.Forms.PictureBox exit;
        private Telerik.WinControls.UI.RadLabel lblCari;
        private Telerik.WinControls.UI.RadTextBox txtSearch;
        private Telerik.WinControls.UI.RadButton btnSearch;
    }
}
