﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.Export;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.ViewModel;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class CameraSource : UserControl
    {
        public RadDropDownList ddlCameraSource { get { return this.ddlSourceCamera; } }
        public RadButton CameraSourceOK { get { return this.OK; } }
        private Point MouseDownLocation;
        public CameraSource()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitForm()
        {
            lblSource.RootElement.CustomFont = Global.MainFont;
            lblSource.RootElement.CustomFontSize = 15f;
            lblHeader.RootElement.CustomFont = Global.MainFont;
            lblHeader.RootElement.CustomFontSize = 13f;
        }

        private void InitEvents()
        {
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            exit.Click += exit_Click;
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
