﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class BonTahananDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BonTahananDetail));
            this.universitas = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.NamaTahanan = new Telerik.WinControls.UI.RadListView();
            this.lblNoBonTahanan = new Telerik.WinControls.UI.RadLabel();
            this.NoBonTahanan = new Telerik.WinControls.UI.RadLabel();
            this.radLabelNRPPemohon = new Telerik.WinControls.UI.RadLabel();
            this.lblDataVisit = new Telerik.WinControls.UI.RadLabel();
            this.lblNoHpPemohon = new Telerik.WinControls.UI.RadLabel();
            this.NoHpPemohon = new Telerik.WinControls.UI.RadLabel();
            this.DetailPemohon = new Telerik.WinControls.UI.RadLabel();
            this.lblNamaPemohon = new Telerik.WinControls.UI.RadLabel();
            this.NamaPemohon = new Telerik.WinControls.UI.RadLabel();
            this.lblPemohon = new Telerik.WinControls.UI.RadLabel();
            this.lblKeterangan = new Telerik.WinControls.UI.RadLabel();
            this.Keterangan = new Telerik.WinControls.UI.RadLabel();
            this.lblLokasi = new Telerik.WinControls.UI.RadLabel();
            this.Lokasi = new Telerik.WinControls.UI.RadLabel();
            this.lblDurasi = new Telerik.WinControls.UI.RadLabel();
            this.Durasi = new Telerik.WinControls.UI.RadLabel();
            this.lblKeperluan = new Telerik.WinControls.UI.RadLabel();
            this.Keperluan = new Telerik.WinControls.UI.RadLabel();
            this.lblNamaTahanan = new Telerik.WinControls.UI.RadLabel();
            this.lblPenanggungJawab = new Telerik.WinControls.UI.RadLabel();
            this.lblNamaPJ = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.lblNoHpPJ = new Telerik.WinControls.UI.RadLabel();
            this.NamaPJ = new Telerik.WinControls.UI.RadLabel();
            this.DetailPJ = new Telerik.WinControls.UI.RadLabel();
            this.NoHpPJ = new Telerik.WinControls.UI.RadLabel();
            this.lblDisetujui = new Telerik.WinControls.UI.RadLabel();
            this.lblNamaDisetujui = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.NamaDisetujui = new Telerik.WinControls.UI.RadLabel();
            this.DetailDisetujui = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailData = new Telerik.WinControls.UI.RadLabel();
            this.lblInputter = new Telerik.WinControls.UI.RadLabel();
            this.lblInputDate = new Telerik.WinControls.UI.RadLabel();
            this.lblUpdater = new Telerik.WinControls.UI.RadLabel();
            this.lblUpdateDate = new Telerik.WinControls.UI.RadLabel();
            this.inputter = new Telerik.WinControls.UI.RadLabel();
            this.inputdate = new Telerik.WinControls.UI.RadLabel();
            this.updater = new Telerik.WinControls.UI.RadLabel();
            this.updatedate = new Telerik.WinControls.UI.RadLabel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnClose = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NamaTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoBonTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoBonTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelNRPPemohon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoHpPemohon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoHpPemohon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailPemohon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaPemohon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NamaPemohon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPemohon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKeterangan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Keterangan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLokasi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lokasi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDurasi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Durasi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKeperluan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Keperluan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPenanggungJawab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaPJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoHpPJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NamaPJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailPJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoHpPJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDisetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaDisetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NamaDisetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailDisetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInputter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInputDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUpdater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUpdateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            this.SuspendLayout();
            // 
            // universitas
            // 
            this.universitas.AutoSize = false;
            this.universitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.universitas.Location = new System.Drawing.Point(504, 414);
            this.universitas.Margin = new System.Windows.Forms.Padding(4);
            this.universitas.Name = "universitas";
            this.universitas.Size = new System.Drawing.Size(728, 42);
            this.universitas.TabIndex = 53;
            this.universitas.Text = "Tahun Lulus";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 56);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(1003, 588);
            this.radScrollablePanel1.Size = new System.Drawing.Size(1022, 590);
            this.radScrollablePanel1.TabIndex = 15;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 385F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 600F));
            this.tableLayoutPanel1.Controls.Add(this.NamaTahanan, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblNoBonTahanan, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.NoBonTahanan, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.radLabelNRPPemohon, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.lblDataVisit, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblNoHpPemohon, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.NoHpPemohon, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.DetailPemohon, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.lblNamaPemohon, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.NamaPemohon, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblPemohon, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblKeterangan, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.Keterangan, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblLokasi, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.Lokasi, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblDurasi, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.Durasi, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblKeperluan, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.Keperluan, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblNamaTahanan, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblPenanggungJawab, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.lblNamaPJ, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.radLabel2, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.lblNoHpPJ, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.NamaPJ, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.DetailPJ, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.NoHpPJ, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.lblDisetujui, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.lblNamaDisetujui, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.radLabel3, 0, 17);
            this.tableLayoutPanel1.Controls.Add(this.NamaDisetujui, 1, 16);
            this.tableLayoutPanel1.Controls.Add(this.DetailDisetujui, 1, 17);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailData, 0, 19);
            this.tableLayoutPanel1.Controls.Add(this.lblInputter, 0, 20);
            this.tableLayoutPanel1.Controls.Add(this.lblInputDate, 0, 21);
            this.tableLayoutPanel1.Controls.Add(this.lblUpdater, 0, 22);
            this.tableLayoutPanel1.Controls.Add(this.lblUpdateDate, 0, 23);
            this.tableLayoutPanel1.Controls.Add(this.inputter, 1, 20);
            this.tableLayoutPanel1.Controls.Add(this.inputdate, 1, 21);
            this.tableLayoutPanel1.Controls.Add(this.updater, 1, 22);
            this.tableLayoutPanel1.Controls.Add(this.updatedate, 1, 23);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Symbol", 8.25F);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 25;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1003, 1350);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // NamaTahanan
            // 
            this.NamaTahanan.AllowColumnReorder = false;
            this.NamaTahanan.AllowColumnResize = false;
            this.NamaTahanan.AllowDragDrop = true;
            this.NamaTahanan.AllowEdit = false;
            this.NamaTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NamaTahanan.EnableCodedUITests = true;
            this.NamaTahanan.EnableColumnSort = true;
            this.NamaTahanan.EnableCustomGrouping = true;
            this.NamaTahanan.EnableFiltering = true;
            this.NamaTahanan.EnableGrouping = true;
            this.NamaTahanan.EnableKeyMap = true;
            this.NamaTahanan.EnableLassoSelection = true;
            this.NamaTahanan.EnableSorting = true;
            this.NamaTahanan.Location = new System.Drawing.Point(398, 113);
            this.NamaTahanan.MultiSelect = true;
            this.NamaTahanan.Name = "NamaTahanan";
            this.NamaTahanan.Size = new System.Drawing.Size(594, 144);
            this.NamaTahanan.TabIndex = 98;
            ((Telerik.WinControls.UI.RadListViewElement)(this.NamaTahanan.GetChildAt(0))).HorizontalLineWidth = 0;
            ((Telerik.WinControls.UI.RadListViewElement)(this.NamaTahanan.GetChildAt(0))).FocusBorderWidth = 0;
            ((Telerik.WinControls.UI.RadListViewElement)(this.NamaTahanan.GetChildAt(0))).BorderHighlightThickness = 0;
            // 
            // lblNoBonTahanan
            // 
            this.lblNoBonTahanan.AutoSize = false;
            this.lblNoBonTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoBonTahanan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNoBonTahanan.Location = new System.Drawing.Point(14, 64);
            this.lblNoBonTahanan.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoBonTahanan.Name = "lblNoBonTahanan";
            this.lblNoBonTahanan.Size = new System.Drawing.Size(377, 42);
            this.lblNoBonTahanan.TabIndex = 11;
            this.lblNoBonTahanan.Text = "NO BON TAHANAN";
            // 
            // NoBonTahanan
            // 
            this.NoBonTahanan.AutoSize = false;
            this.NoBonTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NoBonTahanan.Location = new System.Drawing.Point(399, 64);
            this.NoBonTahanan.Margin = new System.Windows.Forms.Padding(4);
            this.NoBonTahanan.Name = "NoBonTahanan";
            this.NoBonTahanan.Size = new System.Drawing.Size(592, 42);
            this.NoBonTahanan.TabIndex = 58;
            // 
            // radLabelNRPPemohon
            // 
            this.radLabelNRPPemohon.AutoSize = false;
            this.radLabelNRPPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabelNRPPemohon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabelNRPPemohon.Location = new System.Drawing.Point(14, 564);
            this.radLabelNRPPemohon.Margin = new System.Windows.Forms.Padding(4);
            this.radLabelNRPPemohon.Name = "radLabelNRPPemohon";
            this.radLabelNRPPemohon.Size = new System.Drawing.Size(377, 42);
            this.radLabelNRPPemohon.TabIndex = 36;
            this.radLabelNRPPemohon.Text = "NRP / PANGKAT / SATKER";
            // 
            // lblDataVisit
            // 
            this.lblDataVisit.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDataVisit, 2);
            this.lblDataVisit.Cursor = System.Windows.Forms.Cursors.No;
            this.lblDataVisit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDataVisit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDataVisit.Image = ((System.Drawing.Image)(resources.GetObject("lblDataVisit.Image")));
            this.lblDataVisit.Location = new System.Drawing.Point(14, 14);
            this.lblDataVisit.Margin = new System.Windows.Forms.Padding(4);
            this.lblDataVisit.Name = "lblDataVisit";
            this.lblDataVisit.Size = new System.Drawing.Size(977, 42);
            this.lblDataVisit.TabIndex = 62;
            this.lblDataVisit.Text = "       DETAIL BON TAHANAN";
            // 
            // lblNoHpPemohon
            // 
            this.lblNoHpPemohon.AutoSize = false;
            this.lblNoHpPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoHpPemohon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNoHpPemohon.Location = new System.Drawing.Point(14, 614);
            this.lblNoHpPemohon.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoHpPemohon.Name = "lblNoHpPemohon";
            this.lblNoHpPemohon.Size = new System.Drawing.Size(377, 42);
            this.lblNoHpPemohon.TabIndex = 35;
            this.lblNoHpPemohon.Text = "NO HP";
            // 
            // NoHpPemohon
            // 
            this.NoHpPemohon.AutoSize = false;
            this.NoHpPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NoHpPemohon.Location = new System.Drawing.Point(399, 614);
            this.NoHpPemohon.Margin = new System.Windows.Forms.Padding(4);
            this.NoHpPemohon.Name = "NoHpPemohon";
            this.NoHpPemohon.Size = new System.Drawing.Size(592, 42);
            this.NoHpPemohon.TabIndex = 57;
            // 
            // DetailPemohon
            // 
            this.DetailPemohon.AutoSize = false;
            this.DetailPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DetailPemohon.Location = new System.Drawing.Point(399, 564);
            this.DetailPemohon.Margin = new System.Windows.Forms.Padding(4);
            this.DetailPemohon.Name = "DetailPemohon";
            this.DetailPemohon.Size = new System.Drawing.Size(592, 42);
            this.DetailPemohon.TabIndex = 56;
            // 
            // lblNamaPemohon
            // 
            this.lblNamaPemohon.AutoSize = false;
            this.lblNamaPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNamaPemohon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNamaPemohon.Location = new System.Drawing.Point(14, 514);
            this.lblNamaPemohon.Margin = new System.Windows.Forms.Padding(4);
            this.lblNamaPemohon.Name = "lblNamaPemohon";
            this.lblNamaPemohon.Size = new System.Drawing.Size(377, 42);
            this.lblNamaPemohon.TabIndex = 35;
            this.lblNamaPemohon.Text = "NAMA";
            // 
            // NamaPemohon
            // 
            this.NamaPemohon.AutoSize = false;
            this.NamaPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NamaPemohon.Location = new System.Drawing.Point(399, 514);
            this.NamaPemohon.Margin = new System.Windows.Forms.Padding(4);
            this.NamaPemohon.Name = "NamaPemohon";
            this.NamaPemohon.Size = new System.Drawing.Size(592, 42);
            this.NamaPemohon.TabIndex = 60;
            // 
            // lblPemohon
            // 
            this.lblPemohon.AutoSize = false;
            this.lblPemohon.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tableLayoutPanel1.SetColumnSpan(this.lblPemohon, 2);
            this.lblPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPemohon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblPemohon.Location = new System.Drawing.Point(14, 464);
            this.lblPemohon.Margin = new System.Windows.Forms.Padding(4);
            this.lblPemohon.Name = "lblPemohon";
            this.lblPemohon.Size = new System.Drawing.Size(977, 42);
            this.lblPemohon.TabIndex = 18;
            this.lblPemohon.Text = "YANG MENERIMA";
            // 
            // lblKeterangan
            // 
            this.lblKeterangan.AutoSize = false;
            this.lblKeterangan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKeterangan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblKeterangan.Location = new System.Drawing.Point(14, 414);
            this.lblKeterangan.Margin = new System.Windows.Forms.Padding(4);
            this.lblKeterangan.Name = "lblKeterangan";
            this.lblKeterangan.Size = new System.Drawing.Size(377, 42);
            this.lblKeterangan.TabIndex = 95;
            this.lblKeterangan.Text = "KETERANGAN";
            // 
            // Keterangan
            // 
            this.Keterangan.AutoSize = false;
            this.Keterangan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Keterangan.Location = new System.Drawing.Point(399, 414);
            this.Keterangan.Margin = new System.Windows.Forms.Padding(4);
            this.Keterangan.Name = "Keterangan";
            this.Keterangan.Size = new System.Drawing.Size(592, 42);
            this.Keterangan.TabIndex = 97;
            // 
            // lblLokasi
            // 
            this.lblLokasi.AutoSize = false;
            this.lblLokasi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLokasi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblLokasi.Location = new System.Drawing.Point(14, 364);
            this.lblLokasi.Margin = new System.Windows.Forms.Padding(4);
            this.lblLokasi.Name = "lblLokasi";
            this.lblLokasi.Size = new System.Drawing.Size(377, 42);
            this.lblLokasi.TabIndex = 94;
            this.lblLokasi.Text = "LOKASI";
            // 
            // Lokasi
            // 
            this.Lokasi.AutoSize = false;
            this.Lokasi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Lokasi.Location = new System.Drawing.Point(399, 364);
            this.Lokasi.Margin = new System.Windows.Forms.Padding(4);
            this.Lokasi.Name = "Lokasi";
            this.Lokasi.Size = new System.Drawing.Size(592, 42);
            this.Lokasi.TabIndex = 96;
            // 
            // lblDurasi
            // 
            this.lblDurasi.AutoSize = false;
            this.lblDurasi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDurasi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDurasi.Location = new System.Drawing.Point(14, 314);
            this.lblDurasi.Margin = new System.Windows.Forms.Padding(4);
            this.lblDurasi.Name = "lblDurasi";
            this.lblDurasi.Size = new System.Drawing.Size(377, 42);
            this.lblDurasi.TabIndex = 95;
            this.lblDurasi.Text = "DURASI";
            // 
            // Durasi
            // 
            this.Durasi.AutoSize = false;
            this.Durasi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Durasi.Location = new System.Drawing.Point(399, 314);
            this.Durasi.Margin = new System.Windows.Forms.Padding(4);
            this.Durasi.Name = "Durasi";
            this.Durasi.Size = new System.Drawing.Size(592, 42);
            this.Durasi.TabIndex = 97;
            // 
            // lblKeperluan
            // 
            this.lblKeperluan.AutoSize = false;
            this.lblKeperluan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKeperluan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblKeperluan.Location = new System.Drawing.Point(14, 264);
            this.lblKeperluan.Margin = new System.Windows.Forms.Padding(4);
            this.lblKeperluan.Name = "lblKeperluan";
            this.lblKeperluan.Size = new System.Drawing.Size(377, 42);
            this.lblKeperluan.TabIndex = 19;
            this.lblKeperluan.Text = "KEPERLUAN BON TAHANAN";
            // 
            // Keperluan
            // 
            this.Keperluan.AutoSize = false;
            this.Keperluan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Keperluan.Location = new System.Drawing.Point(399, 264);
            this.Keperluan.Margin = new System.Windows.Forms.Padding(4);
            this.Keperluan.Name = "Keperluan";
            this.Keperluan.Size = new System.Drawing.Size(592, 42);
            this.Keperluan.TabIndex = 58;
            // 
            // lblNamaTahanan
            // 
            this.lblNamaTahanan.AutoSize = false;
            this.lblNamaTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNamaTahanan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNamaTahanan.Location = new System.Drawing.Point(14, 114);
            this.lblNamaTahanan.Margin = new System.Windows.Forms.Padding(4);
            this.lblNamaTahanan.Name = "lblNamaTahanan";
            this.lblNamaTahanan.Size = new System.Drawing.Size(377, 142);
            this.lblNamaTahanan.TabIndex = 10;
            this.lblNamaTahanan.Text = "NAMA TAHANAN";
            // 
            // lblPenanggungJawab
            // 
            this.lblPenanggungJawab.AutoSize = false;
            this.lblPenanggungJawab.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tableLayoutPanel1.SetColumnSpan(this.lblPenanggungJawab, 2);
            this.lblPenanggungJawab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPenanggungJawab.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblPenanggungJawab.Location = new System.Drawing.Point(14, 664);
            this.lblPenanggungJawab.Margin = new System.Windows.Forms.Padding(4);
            this.lblPenanggungJawab.Name = "lblPenanggungJawab";
            this.lblPenanggungJawab.Size = new System.Drawing.Size(977, 42);
            this.lblPenanggungJawab.TabIndex = 19;
            this.lblPenanggungJawab.Text = "YANG MENJEMPUT";
            // 
            // lblNamaPJ
            // 
            this.lblNamaPJ.AutoSize = false;
            this.lblNamaPJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNamaPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNamaPJ.Location = new System.Drawing.Point(14, 714);
            this.lblNamaPJ.Margin = new System.Windows.Forms.Padding(4);
            this.lblNamaPJ.Name = "lblNamaPJ";
            this.lblNamaPJ.Size = new System.Drawing.Size(377, 42);
            this.lblNamaPJ.TabIndex = 36;
            this.lblNamaPJ.Text = "NAMA";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel2.Location = new System.Drawing.Point(14, 764);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(377, 42);
            this.radLabel2.TabIndex = 37;
            this.radLabel2.Text = "NRP / PANGKAT / SATKER";
            // 
            // lblNoHpPJ
            // 
            this.lblNoHpPJ.AutoSize = false;
            this.lblNoHpPJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoHpPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNoHpPJ.Location = new System.Drawing.Point(14, 814);
            this.lblNoHpPJ.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoHpPJ.Name = "lblNoHpPJ";
            this.lblNoHpPJ.Size = new System.Drawing.Size(377, 42);
            this.lblNoHpPJ.TabIndex = 36;
            this.lblNoHpPJ.Text = "NO HP";
            // 
            // NamaPJ
            // 
            this.NamaPJ.AutoSize = false;
            this.NamaPJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NamaPJ.Location = new System.Drawing.Point(399, 714);
            this.NamaPJ.Margin = new System.Windows.Forms.Padding(4);
            this.NamaPJ.Name = "NamaPJ";
            this.NamaPJ.Size = new System.Drawing.Size(592, 42);
            this.NamaPJ.TabIndex = 61;
            // 
            // DetailPJ
            // 
            this.DetailPJ.AutoSize = false;
            this.DetailPJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DetailPJ.Location = new System.Drawing.Point(399, 764);
            this.DetailPJ.Margin = new System.Windows.Forms.Padding(4);
            this.DetailPJ.Name = "DetailPJ";
            this.DetailPJ.Size = new System.Drawing.Size(592, 42);
            this.DetailPJ.TabIndex = 57;
            // 
            // NoHpPJ
            // 
            this.NoHpPJ.AutoSize = false;
            this.NoHpPJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NoHpPJ.Location = new System.Drawing.Point(399, 814);
            this.NoHpPJ.Margin = new System.Windows.Forms.Padding(4);
            this.NoHpPJ.Name = "NoHpPJ";
            this.NoHpPJ.Size = new System.Drawing.Size(592, 42);
            this.NoHpPJ.TabIndex = 57;
            // 
            // lblDisetujui
            // 
            this.lblDisetujui.AutoSize = false;
            this.lblDisetujui.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDisetujui, 2);
            this.lblDisetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDisetujui.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDisetujui.Location = new System.Drawing.Point(14, 864);
            this.lblDisetujui.Margin = new System.Windows.Forms.Padding(4);
            this.lblDisetujui.Name = "lblDisetujui";
            this.lblDisetujui.Size = new System.Drawing.Size(977, 42);
            this.lblDisetujui.TabIndex = 20;
            this.lblDisetujui.Text = "YANG MENYETUJUI";
            // 
            // lblNamaDisetujui
            // 
            this.lblNamaDisetujui.AutoSize = false;
            this.lblNamaDisetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNamaDisetujui.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNamaDisetujui.Location = new System.Drawing.Point(14, 914);
            this.lblNamaDisetujui.Margin = new System.Windows.Forms.Padding(4);
            this.lblNamaDisetujui.Name = "lblNamaDisetujui";
            this.lblNamaDisetujui.Size = new System.Drawing.Size(377, 42);
            this.lblNamaDisetujui.TabIndex = 37;
            this.lblNamaDisetujui.Text = "NAMA";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel3.Location = new System.Drawing.Point(14, 964);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(377, 42);
            this.radLabel3.TabIndex = 38;
            this.radLabel3.Text = "NRP / PANGKAT / SATKER";
            // 
            // NamaDisetujui
            // 
            this.NamaDisetujui.AutoSize = false;
            this.NamaDisetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NamaDisetujui.Location = new System.Drawing.Point(399, 914);
            this.NamaDisetujui.Margin = new System.Windows.Forms.Padding(4);
            this.NamaDisetujui.Name = "NamaDisetujui";
            this.NamaDisetujui.Size = new System.Drawing.Size(592, 42);
            this.NamaDisetujui.TabIndex = 62;
            // 
            // DetailDisetujui
            // 
            this.DetailDisetujui.AutoSize = false;
            this.DetailDisetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DetailDisetujui.Location = new System.Drawing.Point(399, 964);
            this.DetailDisetujui.Margin = new System.Windows.Forms.Padding(4);
            this.DetailDisetujui.Name = "DetailDisetujui";
            this.DetailDisetujui.Size = new System.Drawing.Size(592, 42);
            this.DetailDisetujui.TabIndex = 58;
            // 
            // lblDetailData
            // 
            this.lblDetailData.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailData, 2);
            this.lblDetailData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDetailData.Image = ((System.Drawing.Image)(resources.GetObject("lblDetailData.Image")));
            this.lblDetailData.Location = new System.Drawing.Point(14, 1064);
            this.lblDetailData.Margin = new System.Windows.Forms.Padding(4);
            this.lblDetailData.Name = "lblDetailData";
            this.lblDetailData.Size = new System.Drawing.Size(977, 42);
            this.lblDetailData.TabIndex = 71;
            this.lblDetailData.Text = "      DATA LOG";
            // 
            // lblInputter
            // 
            this.lblInputter.AutoSize = false;
            this.lblInputter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInputter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblInputter.Location = new System.Drawing.Point(14, 1114);
            this.lblInputter.Margin = new System.Windows.Forms.Padding(4);
            this.lblInputter.Name = "lblInputter";
            this.lblInputter.Size = new System.Drawing.Size(377, 42);
            this.lblInputter.TabIndex = 68;
            this.lblInputter.Text = "INPUTTER";
            // 
            // lblInputDate
            // 
            this.lblInputDate.AutoSize = false;
            this.lblInputDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInputDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblInputDate.Location = new System.Drawing.Point(14, 1164);
            this.lblInputDate.Margin = new System.Windows.Forms.Padding(4);
            this.lblInputDate.Name = "lblInputDate";
            this.lblInputDate.Size = new System.Drawing.Size(377, 42);
            this.lblInputDate.TabIndex = 72;
            this.lblInputDate.Text = "INPUT DATE";
            // 
            // lblUpdater
            // 
            this.lblUpdater.AutoSize = false;
            this.lblUpdater.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUpdater.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblUpdater.Location = new System.Drawing.Point(14, 1214);
            this.lblUpdater.Margin = new System.Windows.Forms.Padding(4);
            this.lblUpdater.Name = "lblUpdater";
            this.lblUpdater.Size = new System.Drawing.Size(377, 42);
            this.lblUpdater.TabIndex = 73;
            this.lblUpdater.Text = "UPDATER";
            // 
            // lblUpdateDate
            // 
            this.lblUpdateDate.AutoSize = false;
            this.lblUpdateDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUpdateDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblUpdateDate.Location = new System.Drawing.Point(14, 1264);
            this.lblUpdateDate.Margin = new System.Windows.Forms.Padding(4);
            this.lblUpdateDate.Name = "lblUpdateDate";
            this.lblUpdateDate.Size = new System.Drawing.Size(377, 42);
            this.lblUpdateDate.TabIndex = 74;
            this.lblUpdateDate.Text = "UPDATE DATE";
            // 
            // inputter
            // 
            this.inputter.AutoSize = false;
            this.inputter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputter.Location = new System.Drawing.Point(399, 1114);
            this.inputter.Margin = new System.Windows.Forms.Padding(4);
            this.inputter.Name = "inputter";
            this.inputter.Size = new System.Drawing.Size(592, 42);
            this.inputter.TabIndex = 77;
            // 
            // inputdate
            // 
            this.inputdate.AutoSize = false;
            this.inputdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputdate.Location = new System.Drawing.Point(399, 1164);
            this.inputdate.Margin = new System.Windows.Forms.Padding(4);
            this.inputdate.Name = "inputdate";
            this.inputdate.Size = new System.Drawing.Size(592, 42);
            this.inputdate.TabIndex = 78;
            // 
            // updater
            // 
            this.updater.AutoSize = false;
            this.updater.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updater.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updater.Location = new System.Drawing.Point(399, 1214);
            this.updater.Margin = new System.Windows.Forms.Padding(4);
            this.updater.Name = "updater";
            this.updater.Size = new System.Drawing.Size(592, 42);
            this.updater.TabIndex = 80;
            // 
            // updatedate
            // 
            this.updatedate.AutoSize = false;
            this.updatedate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updatedate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updatedate.Location = new System.Drawing.Point(399, 1264);
            this.updatedate.Margin = new System.Windows.Forms.Padding(4);
            this.updatedate.Name = "updatedate";
            this.updatedate.Size = new System.Drawing.Size(592, 42);
            this.updatedate.TabIndex = 79;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1022, 56);
            this.lblTitle.TabIndex = 8;
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(957, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 56);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 1;
            this.exit.TabStop = false;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnClose);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 661);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(1022, 122);
            this.lblButton.TabIndex = 89;
            // 
            // btnClose
            // 
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(30, 30);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(244, 62);
            this.btnClose.TabIndex = 28;
            this.btnClose.Tag = "";
            this.btnClose.Text = "&CLOSE";
            this.btnClose.ThemeName = "MaterialBlueGrey";
            // 
            // BonTahananDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblTitle);
            this.Name = "BonTahananDetail";
            this.Size = new System.Drawing.Size(1022, 783);
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NamaTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoBonTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoBonTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelNRPPemohon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoHpPemohon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoHpPemohon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailPemohon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaPemohon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NamaPemohon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPemohon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKeterangan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Keterangan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLokasi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lokasi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDurasi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Durasi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKeperluan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Keperluan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPenanggungJawab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaPJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoHpPJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NamaPJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailPJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoHpPJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDisetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaDisetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NamaDisetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DetailDisetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInputter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInputDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUpdater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUpdateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel universitas;
        private System.Windows.Forms.PictureBox exit;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel updater;
        private Telerik.WinControls.UI.RadLabel inputdate;
        private Telerik.WinControls.UI.RadLabel NamaPemohon;
        private Telerik.WinControls.UI.RadLabel Keperluan;
        private Telerik.WinControls.UI.RadLabel DetailPemohon;
        private Telerik.WinControls.UI.RadLabel lblNamaPemohon;
        private Telerik.WinControls.UI.RadLabel lblPemohon;
        private Telerik.WinControls.UI.RadLabel lblNamaTahanan;
        private Telerik.WinControls.UI.RadLabel lblKeperluan;
        private Telerik.WinControls.UI.RadLabel lblDataVisit;
        private Telerik.WinControls.UI.RadLabel lblInputter;
        private Telerik.WinControls.UI.RadLabel lblInputDate;
        private Telerik.WinControls.UI.RadLabel lblUpdater;
        private Telerik.WinControls.UI.RadLabel lblUpdateDate;
        private Telerik.WinControls.UI.RadLabel inputter;
        private Telerik.WinControls.UI.RadLabel lblDetailData;
        private Telerik.WinControls.UI.RadLabel updatedate;
        private Telerik.WinControls.UI.RadLabel lblLokasi;
        private Telerik.WinControls.UI.RadLabel lblKeterangan;
        private Telerik.WinControls.UI.RadLabel Lokasi;
        private Telerik.WinControls.UI.RadLabel Keterangan;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnClose;
        private Telerik.WinControls.UI.RadLabel lblNoHpPemohon;
        private Telerik.WinControls.UI.RadLabel lblPenanggungJawab;
        private Telerik.WinControls.UI.RadLabel lblNoHpPJ;
        private Telerik.WinControls.UI.RadLabel lblNamaPJ;
        private Telerik.WinControls.UI.RadLabel NoHpPemohon;
        private Telerik.WinControls.UI.RadLabel NoHpPJ;
        private Telerik.WinControls.UI.RadLabel DetailPJ;
        private Telerik.WinControls.UI.RadLabel NamaPJ;
        private Telerik.WinControls.UI.RadLabel lblDisetujui;
        private Telerik.WinControls.UI.RadLabel DetailDisetujui;
        private Telerik.WinControls.UI.RadLabel NamaDisetujui;
        private Telerik.WinControls.UI.RadLabel lblNamaDisetujui;
        private Telerik.WinControls.UI.RadLabel lblDurasi;
        private Telerik.WinControls.UI.RadLabel Durasi;
        private Telerik.WinControls.UI.RadLabel radLabelNRPPemohon;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel lblNoBonTahanan;
        private Telerik.WinControls.UI.RadLabel NoBonTahanan;
        private Telerik.WinControls.UI.RadListView NamaTahanan;
    }
}
