﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using System.Threading;
using System.IO;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class CellAllocationDetail : Base
    {
        private Point MouseDownLocation;
        public CellAllocationDetail()
        {
            InitializeComponent();
            InitForm();
            InitEvents();

        }

        private void LoadData(string id)
        {
            CellAllocationService serv = new CellAllocationService();
            InmatesService tahanan = new InmatesService();
            CellService cellserv = new CellService();
            var data = serv.GetById(Convert.ToInt32(id));
            if (data != null)
            {
                var imgSource = tahanan.GetImage(data.InmatesRegCode);
                var fingerSource = tahanan.GetFingerPrint(data.InmatesRegCode);
                var roommate = cellserv.GetRoommateDetail(data.InmatesRegCode, data.CellCode);
                lblRegID.Text = data.InmatesRegCode.ToUpper();
                txtCellCode.Text = data.CellCode.ToUpper();
                txtBuildName.Text = data.BuildingName.ToUpper();
                txtTotalFloor.Text = data.CellFloor.ToUpper();
                //txtTypeCell.Enabled = false;
                txtTypeCell.CheckState = CheckState.Checked;
                if (data.CellType == "1") { 
                    txtTypeCell.Text = "SEL TUNGGAL";
                }
                else { 
                    txtTypeCell.Text = "SEL GANDA";
                }
                txtTotalInused.Text = data.InUseCounter;
                txtDateEnter.Text = data.DateEnter.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
                txtAllocationStatus.Text = data.CellAllocationStatus;
                DisplayInmatesPhoto(imgSource);
                DisplayFingerPrint(fingerSource);
                txtFullname.Text = data.FullName.ToUpper();
                txtIdentity.Text = data.IDInmates.ToUpper();
                txtGender.Text = data.Gender.ToUpper();
                txtReligion.Text = data.Religion.ToUpper();
                txtType.Text = data.Type.ToUpper();
                txtCategory.Text = data.Category.ToUpper();
                txtNetwork.Text = data.Network.ToUpper();
                txtPeran.Text = data.Peran == null ? "-" : data.Peran.ToUpper();
                lblLokasiPenangkapan.Text = data.LokasiPenangkapan == null ? "-" : data.LokasiPenangkapan.ToUpper();
                lblTanggalKejadian.Text = data.TanggalKejadian.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
                lblTanggalPenangkapan.Text = data.TanggalPenangkapan.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));

                if(roommate != null)
                {
                    txtRoommate.Text = roommate.Roommate?.ToUpper() + " (" + roommate.RegRoommate + ")";
                    txtRoommate.Tag = roommate.InmatesRegCode;
                    txtRoommate.ForeColor = Color.Navy;
                    txtRoommate.Enabled = true;
                }
                else
                {
                    txtRoommate.Text = "TIDAK ADA";
                    txtRoommate.ForeColor = Color.Navy;
                    txtRoommate.Enabled = false;
                }
                

                UserFunction.SetInitGridView(gvHistory);
                UserFunction.LoadDataHistoryCelAllocationToGrid(gvHistory, data.InmatesRegCode);
            }
        }

        private void DisplayFingerPrint(IEnumerable<FingerPrint> fingerSource)
        {
            foreach (var item in fingerSource)
            {
                var imageBytes = (byte[])Convert.FromBase64String(item.ImagePrint);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);

                if (item.FingerId == "R1")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, picCanvas4.Width, picCanvas4.Height);
                    /* Clear any existing image in the PictureBox. */
                    picCanvas4.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    picCanvas4.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    picCanvas4.Image = imgOutput;
                }
            }
        }

        private void DisplayInmatesPhoto(IEnumerable<Photos> datatemp)
        {
            foreach (var item in datatemp)
            {
                var imageBytes = (byte[])Convert.FromBase64String(item.Data);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);
                if (item.Note == "FRONT")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, picCanvas1.Width, picCanvas1.Height);
                    /* Clear any existing image in the PictureBox. */
                    picCanvas1.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    picCanvas1.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    picCanvas1.Image = imgOutput;

                }
                if (item.Note == "LEFT")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, picCanvas2.Width, picCanvas2.Height);
                    /* Clear any existing image in the PictureBox. */
                    picCanvas2.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    picCanvas2.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    picCanvas2.Image = imgOutput;
                }
                if (item.Note == "RIGHT")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, picCanvas3.Width, picCanvas3.Height);
                    /* Clear any existing image in the PictureBox. */
                    picCanvas3.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    picCanvas3.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    picCanvas3.Image = imgOutput;
                }
            }
        }

        private Size getScaledImageDimensions(int width1, int height1, int width2, int height2)
        {
            /*Determine if Image is Portrait or Landscape. */
            double scaleImageMultiplier = 0;
            if (width1 > height1)    /* Image is Portrait */
            {
                /* Calculate the multiplier based on the heights. */
                if (height2 > width2)
                {
                    scaleImageMultiplier = (double)width2 / (double)width1;
                }

                else
                {
                    scaleImageMultiplier = (double)height2 / (double)height1;
                }
            }

            else /* Image is Landscape */
            {
                /* Calculate the multiplier based on the widths. */
                if (height2 >= width2)
                {
                    scaleImageMultiplier = (double)width2 / (double)width1;
                }

                else
                {
                    scaleImageMultiplier = (double)height2 / (double)height1;
                }
            }

            /* Generate and return the new scaled dimensions.

            * Essentially, we multiply each dimension of the original image
            * by the multiplier calculated above to yield the dimensions
            * of the scaled image. The scaled image can be larger or smaller
            * than the original.
            */

            return new Size((int)(width1 * scaleImageMultiplier), (int)(height1 * scaleImageMultiplier));
        }
        
        private void InitForm()
        {
            ToolTip toolTip = new ToolTip();
            toolTip.ToolTipIcon = ToolTipIcon.Info;
            toolTip.ToolTipTitle = "Info";
            toolTip.SetToolTip(exit, "Tutup Alokasi Sel");

            
            lblTitle.BackColor = Global.MainColor;
            lblTitle.Text = "      Detail Alokasi Sel";
            lblTitle.ForeColor = Color.White;
            lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;
            lblTitle.LabelElement.CustomFont = Global.MainFont;
            lblTitle.LabelElement.CustomFontSize = Global.CustomFontSizeMain;
            btnClose.ButtonElement.CustomFontStyle = FontStyle.Bold;
            btnClose.ButtonElement.CustomFont = Global.MainFont;
            btnClose.ButtonElement.CustomFontSize = Global.CustomFontSizeMain;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);

            lblphoto.ForeColor = Color.White;
            lblfront.ForeColor = Color.White;
            lblright.ForeColor = Color.White;
            lblleft.ForeColor = Color.White;
            lblrightthumb.ForeColor = Color.White;
            lblRegID.ForeColor = Color.White;
        }

        private void InitEvents()
        {
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            exit.Click += exit_Click;
            btnClose.Click += exit_Click;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            this.VisibleChanged += Form_VisibleChanged;
            picCanvas1.Click += picCanvas1_Click;
            picCanvas2.Click += picCanvas2_Click;
            picCanvas3.Click += picCanvas3_Click;
            picCanvas4.Click += picFinger_Click;
            closefull.Click += closefull_Click;
        }

        private void txtRoommate_MouseHover(object sender, EventArgs e)
        {
            txtRoommate.ForeColor = Color.DarkTurquoise;
        }

        private void txtRoommate_MouseLeave(object sender, EventArgs e)
        {
            txtRoommate.ForeColor = Color.Navy;
        }

        private void closefull_Click(object sender, EventArgs e)
        {
            fullPic.Image = null;
            fullpicPanel.Hide();
        }

        private void picCanvas1_Click(object sender, EventArgs e)
        {
            fullpicPanel.Show();
            fullPic.Image = picCanvas1.Image;
        }

        private void picCanvas2_Click(object sender, EventArgs e)
        {
            fullpicPanel.Show();
            fullPic.Image = picCanvas2.Image;
        }

        private void picCanvas3_Click(object sender, EventArgs e)
        {
            fullpicPanel.Show();
            fullPic.Image = picCanvas3.Image;
        }

        private void picFinger_Click(object sender, EventArgs e)
        {
            fullpicPanel.Show();
            fullPic.Image = picCanvas4.Image;
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (this.Tag != null)
                { 
                    LoadData(this.Tag.ToString());
                    fullpicPanel.Location =
                    new Point(this.Width / 2 - fullpicPanel.Size.Width / 2,
                      this.Height / 2 - fullpicPanel.Size.Height / 2);
                }
                else
                    this.Hide();
            }
        }

        private void txtRoommate_Click(object sender, EventArgs e)
        {
            if (txtRoommate.Text != "(-)")
            {
                InmatesService inmate = new InmatesService();
                var x = inmate.GetByInmatesRegId(txtRoommate.Tag.ToString());
                MainForm.formMain.TahananDetail.Tag = x.FirstOrDefault().Id;
                MainForm.formMain.TahananDetail.Show();
                MainForm.formMain.TahananDetail.BringToFront();
            }
        }
    }
}
