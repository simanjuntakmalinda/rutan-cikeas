﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class FormVisit
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormVisit));
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ddlMulai = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.ddlSelesai = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.IdPengunjung = new System.Windows.Forms.Label();
            this.IdTahanan = new System.Windows.Forms.Label();
            this.btnSearchTahanan = new Telerik.WinControls.UI.RadButton();
            this.btnSearchVisitor = new Telerik.WinControls.UI.RadButton();
            this.btnAddVisitor = new Telerik.WinControls.UI.RadButton();
            this.btnAddTahanan = new Telerik.WinControls.UI.RadButton();
            this.ddlSelesaiJam = new Telerik.WinControls.UI.RadDateTimePicker();
            this.txtKeperluan = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel25 = new Telerik.WinControls.UI.RadLabel();
            this.ddlMulaiJam = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblDetailSurat = new Telerik.WinControls.UI.RadLabel();
            this.headerPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnBack = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            this.lblError = new Telerik.WinControls.UI.RadLabel();
            this.radPanelError = new Telerik.WinControls.UI.RadPanel();
            this.radPanel17 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel5 = new Telerik.WinControls.UI.RadPanel();
            this.radPanel6 = new Telerik.WinControls.UI.RadPanel();
            this.txtNamaTahanan = new Telerik.WinControls.UI.RadTextBox();
            this.txtNamaPegunjung = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlMulai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSelesai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).BeginInit();
            this.radLabel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearchTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearchVisitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddVisitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSelesaiJam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeperluan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlMulaiJam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).BeginInit();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).BeginInit();
            this.radPanelError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel17)).BeginInit();
            this.radPanel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            this.radPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).BeginInit();
            this.radPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).BeginInit();
            this.radPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaPegunjung)).BeginInit();
            this.SuspendLayout();
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 0);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.radPanelError);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.lblDetailSurat);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.headerPanel);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(1326, 613);
            this.radScrollablePanel1.Size = new System.Drawing.Size(1328, 615);
            this.radScrollablePanel1.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel1.Controls.Add(this.radPanel6, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.radPanel5, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel4, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.radLabel5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.radLabel4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.radLabel24, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radLabel23, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSearchTahanan, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnSearchVisitor, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnAddVisitor, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnAddTahanan, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.radLabel25, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.radPanel17, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 108);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1326, 458);
            this.tableLayoutPanel1.TabIndex = 161;
            // 
            // ddlMulai
            // 
            this.ddlMulai.AutoSize = false;
            this.ddlMulai.BackColor = System.Drawing.Color.Transparent;
            this.ddlMulai.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ddlMulai.BackgroundImage")));
            this.ddlMulai.CalendarSize = new System.Drawing.Size(290, 320);
            this.ddlMulai.Culture = new System.Globalization.CultureInfo("id-ID");
            this.ddlMulai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlMulai.Location = new System.Drawing.Point(0, 0);
            this.ddlMulai.Margin = new System.Windows.Forms.Padding(0);
            this.ddlMulai.Name = "ddlMulai";
            this.ddlMulai.Size = new System.Drawing.Size(411, 44);
            this.ddlMulai.TabIndex = 95;
            this.ddlMulai.TabStop = false;
            this.ddlMulai.Text = "Kamis, 28 Juni 2018";
            this.ddlMulai.Value = new System.DateTime(2018, 6, 28, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.ddlMulai.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlMulai.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlMulai.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlMulai.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlMulai.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlMulai.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlMulai.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.ddlMulai.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "Kamis, 28 Juni 2018";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.ddlMulai.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel5.Location = new System.Drawing.Point(14, 264);
            this.radLabel5.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(270, 142);
            this.radLabel5.TabIndex = 111;
            this.radLabel5.Text = "<html>Keperluan<span style=\"color: #ff0000\"> *</span></html>";
            this.radLabel5.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel4.Location = new System.Drawing.Point(14, 214);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(270, 42);
            this.radLabel4.TabIndex = 110;
            this.radLabel4.Text = "<html>Waktu Selesai<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.Location = new System.Drawing.Point(14, 164);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(270, 42);
            this.radLabel1.TabIndex = 321;
            this.radLabel1.Text = "<html>Waktu Mulai<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel24
            // 
            this.radLabel24.AutoSize = false;
            this.radLabel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel24.Location = new System.Drawing.Point(14, 64);
            this.radLabel24.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel24.Name = "radLabel24";
            this.radLabel24.Size = new System.Drawing.Size(270, 42);
            this.radLabel24.TabIndex = 191;
            this.radLabel24.Text = "<html>Nama Pengunjung<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // ddlSelesai
            // 
            this.ddlSelesai.AutoSize = false;
            this.ddlSelesai.BackColor = System.Drawing.Color.Transparent;
            this.ddlSelesai.CalendarSize = new System.Drawing.Size(290, 320);
            this.ddlSelesai.Culture = new System.Globalization.CultureInfo("id-ID");
            this.ddlSelesai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlSelesai.Location = new System.Drawing.Point(0, 0);
            this.ddlSelesai.Margin = new System.Windows.Forms.Padding(0);
            this.ddlSelesai.Name = "ddlSelesai";
            this.ddlSelesai.Size = new System.Drawing.Size(411, 44);
            this.ddlSelesai.TabIndex = 98;
            this.ddlSelesai.TabStop = false;
            this.ddlSelesai.Text = "Kamis, 28 Juni 2018";
            this.ddlSelesai.Value = new System.DateTime(2018, 6, 28, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.ddlSelesai.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlSelesai.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlSelesai.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlSelesai.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlSelesai.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlSelesai.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlSelesai.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.ddlSelesai.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "Kamis, 28 Juni 2018";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.ddlSelesai.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // radLabel23
            // 
            this.radLabel23.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.radLabel23, 4);
            this.radLabel23.Controls.Add(this.IdPengunjung);
            this.radLabel23.Controls.Add(this.IdTahanan);
            this.radLabel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel23.Location = new System.Drawing.Point(13, 13);
            this.radLabel23.Name = "radLabel23";
            this.radLabel23.Size = new System.Drawing.Size(1176, 44);
            this.radLabel23.TabIndex = 108;
            this.radLabel23.Text = "Masukkan Data Pengunjung Dibawah Ini";
            // 
            // IdPengunjung
            // 
            this.IdPengunjung.AutoSize = true;
            this.IdPengunjung.Location = new System.Drawing.Point(742, 14);
            this.IdPengunjung.Name = "IdPengunjung";
            this.IdPengunjung.Size = new System.Drawing.Size(0, 19);
            this.IdPengunjung.TabIndex = 130;
            this.IdPengunjung.Visible = false;
            // 
            // IdTahanan
            // 
            this.IdTahanan.AutoSize = true;
            this.IdTahanan.Location = new System.Drawing.Point(685, 14);
            this.IdTahanan.Name = "IdTahanan";
            this.IdTahanan.Size = new System.Drawing.Size(0, 19);
            this.IdTahanan.TabIndex = 131;
            this.IdTahanan.Visible = false;
            // 
            // btnSearchTahanan
            // 
            this.btnSearchTahanan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearchTahanan.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSearchTahanan.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchTahanan.Image")));
            this.btnSearchTahanan.Location = new System.Drawing.Point(1135, 113);
            this.btnSearchTahanan.Name = "btnSearchTahanan";
            this.btnSearchTahanan.Size = new System.Drawing.Size(54, 44);
            this.btnSearchTahanan.TabIndex = 30;
            this.btnSearchTahanan.ThemeName = "MaterialBlueGrey";
            // 
            // btnSearchVisitor
            // 
            this.btnSearchVisitor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearchVisitor.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSearchVisitor.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchVisitor.Image")));
            this.btnSearchVisitor.Location = new System.Drawing.Point(1135, 63);
            this.btnSearchVisitor.Name = "btnSearchVisitor";
            this.btnSearchVisitor.Size = new System.Drawing.Size(54, 44);
            this.btnSearchVisitor.TabIndex = 20;
            this.btnSearchVisitor.ThemeName = "MaterialBlueGrey";
            // 
            // btnAddVisitor
            // 
            this.btnAddVisitor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddVisitor.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAddVisitor.Image = ((System.Drawing.Image)(resources.GetObject("btnAddVisitor.Image")));
            this.btnAddVisitor.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAddVisitor.Location = new System.Drawing.Point(1205, 63);
            this.btnAddVisitor.Name = "btnAddVisitor";
            this.btnAddVisitor.Size = new System.Drawing.Size(54, 44);
            this.btnAddVisitor.TabIndex = 21;
            this.btnAddVisitor.ThemeName = "MaterialBlueGrey";
            // 
            // btnAddTahanan
            // 
            this.btnAddTahanan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddTahanan.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAddTahanan.Image = ((System.Drawing.Image)(resources.GetObject("btnAddTahanan.Image")));
            this.btnAddTahanan.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAddTahanan.Location = new System.Drawing.Point(1205, 113);
            this.btnAddTahanan.Name = "btnAddTahanan";
            this.btnAddTahanan.Size = new System.Drawing.Size(54, 44);
            this.btnAddTahanan.TabIndex = 31;
            this.btnAddTahanan.ThemeName = "MaterialBlueGrey";
            // 
            // ddlSelesaiJam
            // 
            this.ddlSelesaiJam.AutoSize = false;
            this.ddlSelesaiJam.BackColor = System.Drawing.Color.Transparent;
            this.ddlSelesaiJam.CalendarSize = new System.Drawing.Size(290, 320);
            this.ddlSelesaiJam.CustomFormat = "";
            this.ddlSelesaiJam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlSelesaiJam.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.ddlSelesaiJam.Location = new System.Drawing.Point(0, 0);
            this.ddlSelesaiJam.Margin = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.ddlSelesaiJam.Name = "ddlSelesaiJam";
            this.ddlSelesaiJam.Size = new System.Drawing.Size(411, 44);
            this.ddlSelesaiJam.TabIndex = 97;
            this.ddlSelesaiJam.TabStop = false;
            this.ddlSelesaiJam.Text = "12:00:00 AM";
            this.ddlSelesaiJam.Value = new System.DateTime(2019, 4, 25, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.ddlSelesaiJam.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlSelesaiJam.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlSelesaiJam.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlSelesaiJam.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlSelesaiJam.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlSelesaiJam.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlSelesaiJam.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.ddlSelesaiJam.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "12:00:00 AM";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.ddlSelesaiJam.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // txtKeperluan
            // 
            this.txtKeperluan.AutoSize = false;
            this.txtKeperluan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtKeperluan.Location = new System.Drawing.Point(0, 0);
            this.txtKeperluan.Multiline = true;
            this.txtKeperluan.Name = "txtKeperluan";
            this.txtKeperluan.Size = new System.Drawing.Size(828, 144);
            this.txtKeperluan.TabIndex = 100;
            // 
            // radLabel25
            // 
            this.radLabel25.AutoSize = false;
            this.radLabel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel25.Location = new System.Drawing.Point(14, 114);
            this.radLabel25.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel25.Name = "radLabel25";
            this.radLabel25.Size = new System.Drawing.Size(270, 42);
            this.radLabel25.TabIndex = 181;
            this.radLabel25.Text = "<html>Nama Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // ddlMulaiJam
            // 
            this.ddlMulaiJam.AutoSize = false;
            this.ddlMulaiJam.BackColor = System.Drawing.Color.Transparent;
            this.ddlMulaiJam.CalendarSize = new System.Drawing.Size(290, 320);
            this.ddlMulaiJam.CustomFormat = "";
            this.ddlMulaiJam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlMulaiJam.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.ddlMulaiJam.Location = new System.Drawing.Point(0, 0);
            this.ddlMulaiJam.Margin = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.ddlMulaiJam.Name = "ddlMulaiJam";
            this.ddlMulaiJam.Size = new System.Drawing.Size(411, 44);
            this.ddlMulaiJam.TabIndex = 99;
            this.ddlMulaiJam.TabStop = false;
            this.ddlMulaiJam.Text = "12:00:00 AM";
            this.ddlMulaiJam.Value = new System.DateTime(2019, 4, 25, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.ddlMulaiJam.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlMulaiJam.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlMulaiJam.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlMulaiJam.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlMulaiJam.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlMulaiJam.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.ddlMulaiJam.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.ddlMulaiJam.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "12:00:00 AM";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.ddlMulaiJam.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // lblDetailSurat
            // 
            this.lblDetailSurat.AutoSize = false;
            this.lblDetailSurat.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailSurat.Location = new System.Drawing.Point(0, 56);
            this.lblDetailSurat.Name = "lblDetailSurat";
            this.lblDetailSurat.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDetailSurat.Size = new System.Drawing.Size(1326, 52);
            this.lblDetailSurat.TabIndex = 151;
            this.lblDetailSurat.Text = "Pendaftaran Pengunjung";
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.lblTitle);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Margin = new System.Windows.Forms.Padding(4);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1326, 56);
            this.headerPanel.TabIndex = 14;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(1326, 56);
            this.lblTitle.TabIndex = 81;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnBack);
            this.lblButton.Controls.Add(this.btnSave);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblButton.Location = new System.Drawing.Point(0, 615);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(1328, 116);
            this.lblButton.TabIndex = 67;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.back;
            this.btnBack.Location = new System.Drawing.Point(30, 30);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(244, 56);
            this.btnBack.TabIndex = 9;
            this.btnBack.Tag = "";
            this.btnBack.Text = "&BATAL";
            this.btnBack.ThemeName = "MaterialBlueGrey";
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.if_checked_2_309094;
            this.btnSave.Location = new System.Drawing.Point(993, 30);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(305, 56);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "&SIMPAN";
            this.btnSave.ThemeName = "MaterialBlueGrey";
            // 
            // lblError
            // 
            this.lblError.AutoSize = false;
            this.lblError.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.warning;
            this.lblError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblError.Location = new System.Drawing.Point(15, 0);
            this.lblError.Name = "lblError";
            this.lblError.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.lblError.Size = new System.Drawing.Size(1311, 39);
            this.lblError.TabIndex = 24;
            // 
            // radPanelError
            // 
            this.radPanelError.Controls.Add(this.lblError);
            this.radPanelError.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanelError.Location = new System.Drawing.Point(0, 566);
            this.radPanelError.Margin = new System.Windows.Forms.Padding(0);
            this.radPanelError.Name = "radPanelError";
            this.radPanelError.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radPanelError.Size = new System.Drawing.Size(1326, 45);
            this.radPanelError.TabIndex = 162;
            // 
            // radPanel17
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel17, 2);
            this.radPanel17.Controls.Add(this.txtNamaPegunjung);
            this.radPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel17.Location = new System.Drawing.Point(291, 63);
            this.radPanel17.Name = "radPanel17";
            this.radPanel17.Size = new System.Drawing.Size(828, 44);
            this.radPanel17.TabIndex = 1;
            // 
            // radPanel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel1, 2);
            this.radPanel1.Controls.Add(this.txtKeperluan);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(291, 263);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(828, 144);
            this.radPanel1.TabIndex = 7;
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.ddlMulaiJam);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(708, 213);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(411, 44);
            this.radPanel2.TabIndex = 6;
            // 
            // radPanel3
            // 
            this.radPanel3.Controls.Add(this.ddlSelesai);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Location = new System.Drawing.Point(291, 213);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(411, 44);
            this.radPanel3.TabIndex = 5;
            // 
            // radPanel4
            // 
            this.radPanel4.Controls.Add(this.ddlSelesaiJam);
            this.radPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel4.Location = new System.Drawing.Point(708, 163);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(411, 44);
            this.radPanel4.TabIndex = 4;
            // 
            // radPanel5
            // 
            this.radPanel5.Controls.Add(this.ddlMulai);
            this.radPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel5.Location = new System.Drawing.Point(291, 163);
            this.radPanel5.Name = "radPanel5";
            this.radPanel5.Size = new System.Drawing.Size(411, 44);
            this.radPanel5.TabIndex = 3;
            // 
            // radPanel6
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel6, 2);
            this.radPanel6.Controls.Add(this.txtNamaTahanan);
            this.radPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel6.Location = new System.Drawing.Point(291, 113);
            this.radPanel6.Name = "radPanel6";
            this.radPanel6.Size = new System.Drawing.Size(828, 44);
            this.radPanel6.TabIndex = 2;
            // 
            // txtNamaTahanan
            // 
            this.txtNamaTahanan.AutoSize = false;
            this.txtNamaTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNamaTahanan.Location = new System.Drawing.Point(0, 0);
            this.txtNamaTahanan.Multiline = true;
            this.txtNamaTahanan.Name = "txtNamaTahanan";
            this.txtNamaTahanan.Size = new System.Drawing.Size(828, 44);
            this.txtNamaTahanan.TabIndex = 101;
            // 
            // txtNamaPegunjung
            // 
            this.txtNamaPegunjung.AutoSize = false;
            this.txtNamaPegunjung.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNamaPegunjung.Location = new System.Drawing.Point(0, 0);
            this.txtNamaPegunjung.Multiline = true;
            this.txtNamaPegunjung.Name = "txtNamaPegunjung";
            this.txtNamaPegunjung.Size = new System.Drawing.Size(828, 44);
            this.txtNamaPegunjung.TabIndex = 101;
            // 
            // FormVisit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radScrollablePanel1);
            this.Name = "FormVisit";
            this.Size = new System.Drawing.Size(1328, 733);
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlMulai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSelesai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).EndInit();
            this.radLabel23.ResumeLayout(false);
            this.radLabel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearchTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearchVisitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddVisitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSelesaiJam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeperluan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlMulaiJam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).EndInit();
            this.headerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).EndInit();
            this.radPanelError.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel17)).EndInit();
            this.radPanel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            this.radPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).EndInit();
            this.radPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).EndInit();
            this.radPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaPegunjung)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel25;
        private Telerik.WinControls.UI.RadLabel lblDetailSurat;
        private Telerik.WinControls.UI.RadPanel headerPanel;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel radLabel24;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDateTimePicker ddlSelesai;
        private Telerik.WinControls.UI.RadLabel radLabel23;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadButton btnSearchTahanan;
        private Telerik.WinControls.UI.RadButton btnSearchVisitor;
        private Telerik.WinControls.UI.RadButton btnAddVisitor;
        private Telerik.WinControls.UI.RadButton btnAddTahanan;
        private System.Windows.Forms.Label IdPengunjung;
        private System.Windows.Forms.Label IdTahanan;
        private Telerik.WinControls.UI.RadDateTimePicker ddlSelesaiJam;
        private Telerik.WinControls.UI.RadTextBox txtKeperluan;
        private Telerik.WinControls.UI.RadDateTimePicker ddlMulai;
        private Telerik.WinControls.UI.RadDateTimePicker ddlMulaiJam;
        private Telerik.WinControls.UI.RadPanel radPanelError;
        private Telerik.WinControls.UI.RadLabel lblError;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnBack;
        private Telerik.WinControls.UI.RadButton btnSave;
        private Telerik.WinControls.UI.RadPanel radPanel17;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadPanel radPanel5;
        private Telerik.WinControls.UI.RadPanel radPanel6;
        private Telerik.WinControls.UI.RadTextBox txtNamaTahanan;
        private Telerik.WinControls.UI.RadTextBox txtNamaPegunjung;
    }
}
