﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.Desktop.Presentation.Properties;
using AForge.Video.DirectShow;
using AForge.Video;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class CapturePhoto : Base
    {
        private Point MouseDownLocation;

        public PictureBox FrontPhoto { get { return this.frontPhoto; } set { } }
        public PictureBox RightPhoto { get { return this.rightPhoto; } set { } }
        public PictureBox LeftPhoto { get { return this.leftPhoto; } set { } }
        public Panel PanelBorder { get { return this.panelBorder; } set { } }

        private bool CamOn = false;
        private bool recapture;
        int currentPhoto = 0;
        public VideoCaptureDevice FinalFrame;
        public FilterInfoCollection CaptureDevice;

        public CapturePhoto()
        {
            InitializeComponent();
            InitEvents();
            LoadData();
        }

        private void LoadData()
        {
            frontPhoto.Image = null;
            rightPhoto.Image = null;
            LeftPhoto.Image = null;
        }

        private void InitEvents()
        {
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            btnSetting.Click += btnSetting_Click;
            CamOnOff.Click += CamOnOff_Click;
            btnCapture.Click += btnCapture_Click;
            frontPhoto.Click += frontPhoto_Click;
            rightPhoto.Click += rightPhoto_Click;
            leftPhoto.Click += leftPhoto_Click;
            btnSave.Click += btnSave_Click;
            exit.Click += exit_Click;
            this.Leave += Form_Leave;
            this.VisibleChanged += Form_VisibleChanged;
        }

        private void Form_Leave(object sender, EventArgs e)
        {
            StopFrame();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            CamOn = true;
            OnOffCamera();
            this.Hide();
            ClearCapturePhoto();
        }

        private void ClearCapturePhoto()
        {
            frontPhoto.Image = null;
            rightPhoto.Image = null;
            leftPhoto.Image = null;
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
            MainForm.formMain.CameraSource.Visible = true;
            MainForm.formMain.CameraSource.Show();
            MainForm.formMain.CameraSource.BringToFront();
        }

        private void CamOnOff_Click(object sender, EventArgs e)
        {
            OnOffCamera();
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            if (!CamOn)
                return;

            if (currentPhoto != 0)
            {
                recapture = false;
            }


            if(this.Tag != null) //Capture Visitor
            {
                MainForm.formMain.Visit.VisitorPhoto.Image = picCanvas.Image;
                this.Tag = null;
                CamOn = true;
                OnOffCamera();
                this.Hide();
                return;
            }


            if (!recapture)
            {
                StopFrame();
                recapture = true;
                btnCapture.Image = Resources.camera_off;
                AddNewPhoto();
            }
            else
            {
                recapture = false;
                btnCapture.Image = Resources.camera_on;
                StartFrame();
                PhotoPosititionCapture();
            }
        }

        private void OnOffCamera()
        {
            if (!CamOn)
            {
                StartFrame();
                CamOnOff.Image = Resources.CamOn;
                btnCapture.Image = Resources.camera_on;
                CamOn = true;
                btnCapture.Enabled = true;
                btnSetting.Enabled = true;
            }
            else
            {
                StopFrame();
                btnCapture.Enabled = false;
                btnSetting.Enabled = false;
                CamOnOff.Image = Resources.CamOff;
                btnCapture.Image = Resources.camera_off;
                picCanvas.Image = null;
                CamOn = false;

                if(this.Tag == null) { 
                    if (MainForm.formMain.Tahanan.PhotoList.Count != 0)
                        picCanvas.Image = MainForm.formMain.Tahanan.PhotoList[0].Image;
                    else
                        picCanvas.Image = null;
                }
            }
        }

        private void StartFrame()
        {
            CaptureDevice = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            MainForm.formMain.CameraSource.ddlCameraSource.Items.Clear();
            if (CaptureDevice.Count > 0)
            {
                foreach (FilterInfo Device in CaptureDevice)
                {
                    MainForm.formMain.CameraSource.ddlCameraSource.Items.Add(Device.Name);
                }
                MainForm.formMain.CameraSource.ddlCameraSource.SelectedIndex = 0;
                FinalFrame = new VideoCaptureDevice();
                picCanvas.Image = null;
                FinalFrame = new VideoCaptureDevice(CaptureDevice[MainForm.formMain.CameraSource.ddlCameraSource.SelectedIndex].MonikerString);
                FinalFrame.NewFrame += new NewFrameEventHandler(FinalFrame_NewFrame);
                FinalFrame.Start();
            }
            else {
                MessageBox.Show("Perangkat kamera tidak ditemukan atau bermasalah \nsilahkan periksa kembali perangkat kamera\ndengan cara melepas dan pasang kembali.","Perangkat Bermasalah",MessageBoxButtons.OK,MessageBoxIcon.Error);
                CamOn = false;
                this.Hide();
            }

        }

        public void StopFrame()
        {
            if (FinalFrame == null)
                return;

            if (FinalFrame.IsRunning == true)
            {
                FinalFrame.Stop();
            }
        }

        private void AddNewPhoto()
        {
            switch (currentPhoto)
            {
                case 1:
                    if (frontPhoto.Image == null)
                    {
                        MainForm.formMain.Tahanan.PhotoList.Add(new PictureBox());
                        MainForm.formMain.Tahanan.totalPhoto++;
                    }
                    break;
                case 2:
                    if (rightPhoto.Image == null)
                    {
                        MainForm.formMain.Tahanan.PhotoList.Add(new PictureBox());
                        MainForm.formMain.Tahanan.totalPhoto++;
                    }
                    break;
                case 3:
                    if (leftPhoto.Image == null)
                    {
                        MainForm.formMain.Tahanan.PhotoList.Add(new PictureBox());
                        MainForm.formMain.Tahanan.totalPhoto++;
                    }
                    break;
                default:
                    break;
            }

            MainForm.formMain.Tahanan.PhotoList[currentPhoto - 1].Image = picCanvas.Image;
            PhotoPosititionCapture();

            if (MainForm.formMain.Tahanan.totalPhoto >= 3)
            {
                this.btnSave.BackColor = SystemColors.ControlDark;
                this.btnSave.Enabled = true;
            }
        }

        private void PhotoPosititionCapture()
        {
            switch (currentPhoto)
            {
                case 1:
                    frontPhoto.Image = picCanvas.Image;
                    RightPhoto_Click();
                    break;
                case 2:
                    rightPhoto.Image = picCanvas.Image;
                    LeftPhoto_Click();
                    break;
                case 3:
                    leftPhoto.Image = picCanvas.Image;
                    break;
                default:
                    break;
            }
        }

        public void FinalFrame_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            picCanvas.Image = (Bitmap)eventArgs.Frame.Clone();
        }

        private void LoadPhoto()
        {
            if (MainForm.formMain.Tahanan.totalPhoto <= 0)
            {
                picCanvas.Image = null;
                currentPhoto = 0;
            }
            else
                picCanvas.Image = MainForm.formMain.Tahanan.PhotoList[currentPhoto - 1].Image;

            PhotoPosititionCapture();
        }

        private void frontPhoto_Click(object sender, EventArgs e)
        {
            FrontPhoto_Click();
        }

        private void rightPhoto_Click(object sender, EventArgs e)
        {
            RightPhoto_Click();
        }

        private void leftPhoto_Click(object sender, EventArgs e)
        {
            LeftPhoto_Click();
        }

        public void FrontPhoto_Click()
        {
            currentPhoto = 1;
            panelFrontPhoto.BackColor = Color.Chartreuse;
            panelRightPhoto.BackColor = Color.Silver;
            panelLeftPhoto.BackColor = Color.Silver;
            CamOn = false;
            recapture = false;
            OnOffCamera();
        }

        public void RightPhoto_Click()
        {
            currentPhoto = 2;
            panelFrontPhoto.BackColor = Color.Silver;
            panelRightPhoto.BackColor = Color.Chartreuse;
            panelLeftPhoto.BackColor = Color.Silver;
            CamOn = false;
            recapture = false;
            OnOffCamera();
        }

        public void LeftPhoto_Click()
        {
            currentPhoto = 3;
            panelFrontPhoto.BackColor = Color.Silver;
            panelRightPhoto.BackColor = Color.Silver;
            panelLeftPhoto.BackColor = Color.Chartreuse;
            CamOn = false;
            recapture = false;
            OnOffCamera();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            CamOn = true;
            OnOffCamera();
            this.Hide();
            MainForm.formMain.Tahanan.PicCancas.Image = MainForm.formMain.Tahanan.PhotoList[0].Image;
            MainForm.formMain.Tahanan.lblFront_Click(this,EventArgs.Empty);
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.Height = 660;
                panelBorder.Visible = false;
                LoadData();
            }
            else
            {

            }
        }

    }
}
