﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.DataAccess.Helper;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormNetwork : Base
    {
        public TableLayoutPanel table1 { get { return this.tableLayoutPanel1; } }
        private Point MouseDownLocation;
        public FormNetwork()
        {
            InitializeComponent();
            InitForm();
            LoadData();
            InitEvents();    
        }

        public void LoadData()
        {
            this.lblTitle.Text = "     TAMBAH DATA JARINGAN BARU";

        }

        private void InitEvents()
        {
            //Events
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;

            radPanel6.KeyDown += Control_KeyDown;
            txtNama.KeyDown += Control_KeyDown;
            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;
        }


        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                InsertData();

                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Jaringan Tahanan Berhasil Disimpan!");
                UserFunction.LoadDDLNetwork(MainForm.formMain.Tahanan.DDLNetwork);
                MainForm.formMain.Tahanan.DDLNetwork.Refresh();
                this.Hide();
                this.Tag = null;
                //if (this.Tag == null)
                //    SaveNetwork();
                //else
                //    UpdatetData();
            }
        }

        //private void SaveNetwork()
        //{
        //    if(this.Tag == null)
        //        InsertData();
        //    else
        //        UpdateData();
        //}

        private void UpdateData()
        {
            //var username = txtUserName.Text;
            //AppUserService userserv = new AppUserService();
            //var user = new AspnetUsers
            //{
            //    Id = Convert.ToInt32(this.Tag),
            //    UserName = txtUserName.Text.ToUpper(),
            //    IsLocked = Convert.ToInt32(ddlLocked.SelectedItem.Tag),
            //    InmateModul = chk1.Checked,
            //    VisitorModul = chk6.Checked,
            //    CellAllocationModul = chk2.Checked,
            //    UserManagementModul = chk3.Checked,
            //    ReportModul = chk4.Checked,
            //    AuditTrailModul = chk5.Checked,
            //    UpdatedDate = DateTime.Now.ToLocalTime(),
            //    UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
            //};
            //userserv.Update(user);
            //UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERUSER.ToString(), Activities = "Update User, Old Data=" + UserFunction.JsonString(OldData) + ",  New Data=" + UserFunction.JsonString(user) });
        }

        private void InsertData()
        {
            var name = txtNama.Text;
            NetworkService netserv = new NetworkService();
            var net = new Network
            {
                Name = name,
                CreatedDate = DateTime.Now.ToLocalTime(),
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
            };
            netserv.Post(net);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERNETWORK.ToString(), Activities = "Add New Network, Data=" + UserFunction.JsonString(net) });
        }

        private void InitForm()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     TAMBAH JARINGAN BARU";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;
            this.lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 14f;
            this.lblDetailSurat.ForeColor = Color.White;
            this.lblDetailSurat.BackColor = Color.FromArgb(77,77,77);

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            txtNama.Text = null;
            this.Hide();
            lblError.Text = String.Empty;
        }

        private bool DataValid()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            if (this.txtNama.Text == string.Empty)
            {
                radPanelError.Show();
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Nama Jaringan!";
                return false;
            }

            return true;
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void lblIDType_Click(object sender, EventArgs e)
        {

        }

    }
}
