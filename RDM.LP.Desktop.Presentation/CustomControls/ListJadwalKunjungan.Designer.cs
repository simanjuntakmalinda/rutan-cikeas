﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class ListJadwalKunjungan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListJadwalKunjungan));
            this.gvJadwalKunjungan = new Telerik.WinControls.UI.RadGridView();
            this.lblDetail = new Telerik.WinControls.UI.RadLabel();
            this.pdf = new System.Windows.Forms.PictureBox();
            this.excel = new System.Windows.Forms.PictureBox();
            this.Title_ = new Telerik.WinControls.UI.RadLabel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.picRefresh = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.gvJadwalKunjungan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvJadwalKunjungan.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).BeginInit();
            this.lblDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title_)).BeginInit();
            this.Title_.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRefresh)).BeginInit();
            this.SuspendLayout();
            // 
            // gvJadwalKunjungan
            // 
            this.gvJadwalKunjungan.AutoScroll = true;
            this.gvJadwalKunjungan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvJadwalKunjungan.Location = new System.Drawing.Point(0, 108);
            // 
            // 
            // 
            this.gvJadwalKunjungan.MasterTemplate.AllowAddNewRow = false;
            this.gvJadwalKunjungan.MasterTemplate.AllowCellContextMenu = false;
            this.gvJadwalKunjungan.MasterTemplate.AllowColumnChooser = false;
            this.gvJadwalKunjungan.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.gvJadwalKunjungan.MasterTemplate.AllowColumnReorder = false;
            this.gvJadwalKunjungan.MasterTemplate.AllowDragToGroup = false;
            this.gvJadwalKunjungan.MasterTemplate.EnableFiltering = true;
            this.gvJadwalKunjungan.MasterTemplate.EnablePaging = true;
            this.gvJadwalKunjungan.MasterTemplate.PageSize = 10;
            this.gvJadwalKunjungan.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gvJadwalKunjungan.Name = "gvJadwalKunjungan";
            this.gvJadwalKunjungan.Size = new System.Drawing.Size(1349, 413);
            this.gvJadwalKunjungan.TabIndex = 58;
            // 
            // lblDetail
            // 
            this.lblDetail.AutoSize = false;
            this.lblDetail.BackColor = System.Drawing.Color.Gainsboro;
            this.lblDetail.Controls.Add(this.picRefresh);
            this.lblDetail.Controls.Add(this.pdf);
            this.lblDetail.Controls.Add(this.excel);
            this.lblDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetail.Location = new System.Drawing.Point(0, 56);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDetail.Size = new System.Drawing.Size(1349, 52);
            this.lblDetail.TabIndex = 16;
            // 
            // pdf
            // 
            this.pdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pdf.Dock = System.Windows.Forms.DockStyle.Right;
            this.pdf.Image = ((System.Drawing.Image)(resources.GetObject("pdf.Image")));
            this.pdf.Location = new System.Drawing.Point(1263, 10);
            this.pdf.Name = "pdf";
            this.pdf.Size = new System.Drawing.Size(43, 32);
            this.pdf.TabIndex = 5;
            this.pdf.TabStop = false;
            // 
            // excel
            // 
            this.excel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.excel.Dock = System.Windows.Forms.DockStyle.Right;
            this.excel.Image = ((System.Drawing.Image)(resources.GetObject("excel.Image")));
            this.excel.Location = new System.Drawing.Point(1306, 10);
            this.excel.Name = "excel";
            this.excel.Size = new System.Drawing.Size(43, 32);
            this.excel.TabIndex = 4;
            this.excel.TabStop = false;
            // 
            // Title_
            // 
            this.Title_.AutoSize = false;
            this.Title_.BackColor = System.Drawing.Color.White;
            this.Title_.Controls.Add(this.lblTitle);
            this.Title_.Dock = System.Windows.Forms.DockStyle.Top;
            this.Title_.Location = new System.Drawing.Point(0, 0);
            this.Title_.Margin = new System.Windows.Forms.Padding(0);
            this.Title_.Name = "Title_";
            this.Title_.Padding = new System.Windows.Forms.Padding(3);
            this.Title_.Size = new System.Drawing.Size(1349, 56);
            this.Title_.TabIndex = 1;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(3, 3);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(1);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(1343, 50);
            this.lblTitle.TabIndex = 61;
            // 
            // picRefresh
            // 
            this.picRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picRefresh.Dock = System.Windows.Forms.DockStyle.Right;
            this.picRefresh.Image = ((System.Drawing.Image)(resources.GetObject("picRefresh.Image")));
            this.picRefresh.Location = new System.Drawing.Point(1220, 10);
            this.picRefresh.Name = "picRefresh";
            this.picRefresh.Size = new System.Drawing.Size(43, 32);
            this.picRefresh.TabIndex = 6;
            this.picRefresh.TabStop = false;
            // 
            // ListJadwalKunjungan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gvJadwalKunjungan);
            this.Controls.Add(this.lblDetail);
            this.Controls.Add(this.Title_);
            this.Name = "ListJadwalKunjungan";
            this.Size = new System.Drawing.Size(1349, 521);
            ((System.ComponentModel.ISupportInitialize)(this.gvJadwalKunjungan.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvJadwalKunjungan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).EndInit();
            this.lblDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title_)).EndInit();
            this.Title_.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRefresh)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView gvJadwalKunjungan;
        private System.Windows.Forms.PictureBox excel;
        private System.Windows.Forms.PictureBox pdf;
        private Telerik.WinControls.UI.RadLabel lblDetail;
        private Telerik.WinControls.UI.RadLabel Title_;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private System.Windows.Forms.PictureBox picRefresh;
    }
}
