﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using System.Threading;
using System.IO;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class TahananDetail : Base
    {
        int countImages = 0;
        int totalImages = 0;
        List<PictureBox> ImagesList = new List<PictureBox>();
        private Point MouseDownLocation;
        public TahananDetail()
        {
            InitializeComponent();
            InitForm();
            InitEvents();

        }

        private void LoadData(string id)
        {
            picCanvas.Image = null;
            picFinger.Image = null;
            totalImages = 0;
            countImages = 0;
            ImagesList.Clear();

            InmatesService tahanan = new InmatesService();
            CasesService cases = new CasesService();
            var data = tahanan.GetDetailById(id);
            if (data != null)
            {
                regID.Text = data.RegID;
                namalengkap.Text = data.FullName;
                PDOfBirth.Text = (data.PlaceOfBirth == null ? " - " : data.PlaceOfBirth) + ", " + (data.DateOfBirth == null ? " - " : data.DateOfBirth.ToString("dd MMMM yyyy"));
                jeniskelamin.Text = data.Gender;
                jenisidentitas.Text = data.IDType;
                noidentitas.Text = data.IDNo;
                telp.Text = data.TelpNo;
                religion.Text = data.Religion;
                MaritalStatus.Text = data.MaritalStatus;
                Nationality.Text = data.Nationality;
                Vocation.Text = data.Vocation;
                city.Text = data.City;
                address.Text = data.Address;
                lblCategory.Text = data.Category;
                lblType.Text = data.Type;
                lblNetwork.Text = data.Network;
                txtCase.Text = cases.GetCaseByRegId(data.RegID);
                lblDateReg.Text = data.DateReg.Value.ToString("dd MMMM yyyy");
                lblSource.Text = data.SourceLocation;
                lblNote.Text = data.Notes;
                lblPeranTahanan.Text = data.Peran;
                lblTglPenangkapan.Text = data.TanggalPenangkapan.ToString("dd MMMM yyyy");
                lblTglKejadian.Text = data.TanggalKejadian.ToString("dd MMMM yyyy");
                lblLokasiPenangkapan.Text = data.LokasiPenangkapan;
                beratbadan.Text = data.BeratBadan == null ? "-" : data.BeratBadan;
                tinggibadan.Text = data.TinggiBadan == null ? "-" : data.TinggiBadan;
                lblNoSuratPenangkapan.Text = data.NoSuratPenangkapan;
                lblNoSuratPenahanan.Text = data.NoSuratPenahanan;
                lblTanggalPenahanan.Text = data.TanggalPenahanan.ToString("dd MMMM yyyy");
                inputter.Text = data.CreatedBy;
                inputdate.Text = data.CreatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss");
                updater.Text = data.UpdatedBy;
                updatedate.Text = data.UpdatedDate == null ? string.Empty : data.UpdatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss");
                LoadPhoto(data);
                LoadDataFinger(data);
                UserFunction.LoadDataHistoryTahananToGrid(gvHistory, data.RegID);
                UserFunction.SetInitGridView(gvHistory);
            }
        }

        private void LoadDataFinger(Inmates data)
        {
            FingerPrintService fingerserv = new FingerPrintService();
            var finger = fingerserv.GetByRegID(data.RegID);
            if (finger != null)
            {
                var imageBytes = (byte[])Convert.FromBase64String(finger.ImagePrint);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);
                picFinger.Image = image;
            }
        }

        private void LoadPhoto(Inmates data)
        {
            PhotosService photoserv = new PhotosService();
            var foto = photoserv.GetByRegID(data.RegID);
            foreach (var item in foto)
            {
                var imageBytes = (byte[])Convert.FromBase64String(item.Data);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);

                ImagesList.Add(new PictureBox());
                ImagesList[ImagesList.Count - 1].Image = image;

                totalImages++;
                countImages++;
                picCanvas.Image = ImagesList[0].Image;
            }
            tot.Text = string.Format("{0}/{1}", countImages, totalImages);
        }

        private void InitForm()
        {
            ToolTip toolTip = new ToolTip();
            toolTip.ToolTipIcon = ToolTipIcon.Info;
            toolTip.ToolTipTitle = "Info";
            toolTip.SetToolTip(exit, "Tutup Profil Tahanan");
            lblButton.BackColor = Global.MainColor;
            lblTitle.BackColor = Global.MainColor;
            lblTitle.Text = "      PROFIL TAHANAN";
            lblTitle.ForeColor = Color.White;
            lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;
            lblTitle.LabelElement.CustomFont = Global.MainFont;
            lblTitle.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            btnClose.ButtonElement.CustomFontStyle = FontStyle.Bold;
            btnClose.ButtonElement.CustomFont = Global.MainFont;
            btnClose.ButtonElement.CustomFontSize = Global.CustomFontSizeMain;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);

            lblRegID.ForeColor = Color.FromArgb(192, 0, 0);
            regID.ForeColor = Color.FromArgb(192, 0, 0);
            lblInmatePhoto.BackColor = Global.MainColor;
            lblInmatePhoto.ForeColor = Color.White;
            lblVisitorFinger.BackColor = Global.MainColor;
            lblVisitorFinger.ForeColor = Color.White;
        }

        private void InitEvents()
        {
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            exit.Click += exit_Click;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            this.VisibleChanged += Form_VisibleChanged;
            btnClose.Click += btnClose_Click;
            next.Click += next_Click;
            prev.Click += prev_Click;
            picCanvas.Click += picCanvas_Click;
            picFinger.Click += picFinger_Click;
            closefull.Click += closefull_Click;
            gvHistory.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvHistory.RowFormatting += radGridView_RowFormatting;
            gvHistory.ContextMenuOpening += radGridView_ContextMenuOpening;
        }

        private void closefull_Click(object sender, EventArgs e)
        {
            fullPic.Image = null;
            fullpicPanel.Hide();
        }

        private void picCanvas_Click(object sender, EventArgs e)
        {
            fullpicPanel.Show();
            fullPic.Image = picCanvas.Image;
        }

        private void picFinger_Click(object sender, EventArgs e)
        {
            fullpicPanel.Show();
            fullPic.Image = picFinger.Image;
        }

        private void prev_Click(object sender, EventArgs e)
        {
            if (countImages == 0)
            {
                return;
            }
            else
            {
                LoadPhoto();
                countImages--;
            }
        }

        private void next_Click(object sender, EventArgs e)
        {
            if (countImages >= ImagesList.Count)
                return;
            else
                countImages++;
            LoadPhoto();
        }

        private void LoadPhoto()
        {
            if (countImages <= 0)
                picCanvas.Image = null;
            else
                picCanvas.Image = ImagesList[countImages - 1].Image;
            tot.Text = string.Format("{0}/{1}", countImages, totalImages);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (this.Tag != null)
                {
                    LoadData(this.Tag.ToString());
                    fullpicPanel.Location =
                    new Point(this.Width / 2 - fullpicPanel.Size.Width / 2,
                      this.Height / 2 - fullpicPanel.Size.Height / 2);
                }
                else
                    this.Hide();
            }
        }

        private void lblType_Click(object sender, EventArgs e)
        {

        }

        private void lblNetwork_Click(object sender, EventArgs e)
        {

        }

        private void lblSource_Click(object sender, EventArgs e)
        {

        }

        private void lblLokasiPenangkapan_Click(object sender, EventArgs e)
        {

        }

        private void lblDateReg_Click(object sender, EventArgs e)
        {

        }

        private void lblPeranTahanan_Click(object sender, EventArgs e)
        {

        }

        private void lblTglKejadian_Click(object sender, EventArgs e)
        {

        }
    }
}
