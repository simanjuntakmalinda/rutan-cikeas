﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListVisitor : Base
    {
        public RadGridView GvVisitor { get { return this.gvVisitor; } }
        public ListVisitor()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvVisitor.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvVisitor.RowFormatting += radGridView_RowFormatting;
            gvVisitor.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvVisitor.CellClick += gvVisitor_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
            picRefresh.Click += PicRefresh_Click;
        }

        private void PicRefresh_Click(object sender, EventArgs e)
        {
            UserFunction.LoadDataPegunjungToGrid(gvVisitor);
            gvVisitor.Refresh();
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarPengunjung.pdf", "VISITOR LIST DATA", gvVisitor);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarPengunjung.csv", "VISITOR LIST DATA", gvVisitor);
        }

        private void gvVisitor_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == null)
                return;
            switch (e.Column.Index)
            {
                case 0:
                    MainForm.formMain.VisitorDetail.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.VisitorDetail.Show();
                    break;
                case 1:
                    MainForm.formMain.Visit.Tag = "Edit-"  + e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.AddNew;
                    MainForm.formMain.Visit.LoadData();
                    break;
                case 2:
                    var question  = UserFunction.Confirm("Hapus Pengunjung ?");
                    if (question == DialogResult.Yes)
                    { 
                        DeleteVisitor(e.Row.Cells["Id"].Value.ToString());
                    }
                    break;
            }
            
        }

        private void DeleteVisitor(string Id)
        {
            VisitorService pengunjung = new VisitorService();
            var data = pengunjung.GetDetailById(Id);

            VisitService visiting = new VisitService();
            var datavisiting = visiting.GetVisitorCheckout(Id);
            if (datavisiting != null)
            {
                RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "Pengunjung Belum Check Out Dari DAFTAR KUNJUNGAN, Silakan Check Out Terlebih Dahulu", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }

            pengunjung.DeleteById(Id);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERVISITOR.ToString(), Activities = "Delete Visitor, data=" + UserFunction.JsonString(data) });
            UserFunction.MsgBox(TipeMsg.Info, "Data Dihapus!");
            UserFunction.LoadDataPegunjungToGrid(gvVisitor);

            VisitorLogService logserv = new VisitorLogService();
            var logdata = logserv.GetDetailByRegID(data.RegID);
            if (logdata != null)
            {
                var log = new VisitorLog
                {
                    Id = logdata.Id,
                    DeletedDate = DateTime.Now.ToLocalTime(),
                    DeletedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
                };
                logserv.Update(log);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERVISITOR.ToString(), Activities = "Update VisitorLog, Old Data=" + UserFunction.JsonString(logdata) + ",  New Data=" + UserFunction.JsonString(log) });
            }
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DAFTAR PENGUNJUNG";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;


            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Width = 15;
            btnDetail.Name = "btnDetail";
            gvVisitor.AutoGenerateColumns = false;
            gvVisitor.Columns.Insert(0, btnDetail);
            gvVisitor.Refresh();
            gvVisitor.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            GridViewImageColumn btnUbah = new GridViewImageColumn();
            btnUbah.HeaderText = "";
            btnUbah.Name = "btnUbah";
            gvVisitor.AutoGenerateColumns = false;
            gvVisitor.Columns.Insert(1, btnUbah);
            gvVisitor.Refresh();
            gvVisitor.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnHapus = new GridViewImageColumn();
            btnHapus.HeaderText = "";
            btnHapus.Name = "btnHapus";
            gvVisitor.AutoGenerateColumns = false;
            gvVisitor.Columns.Insert(1, btnHapus);
            gvVisitor.Refresh();
            gvVisitor.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            UserFunction.LoadDataPegunjungToGrid(gvVisitor);
            UserFunction.SetInitGridView(gvVisitor);

        }

    }
}
