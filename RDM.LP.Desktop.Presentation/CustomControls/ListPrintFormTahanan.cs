﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListPrintFormTahanan : Base
    {
        public RadGridView GvTahanan { get { return this.gvPrintFormTahanan; } }
        public ListPrintFormTahanan()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }
         
        private void InitEvents()
        {
            gvPrintFormTahanan.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvPrintFormTahanan.RowFormatting += radGridView_RowFormatting;
            gvPrintFormTahanan.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvPrintFormTahanan.CellClick += gvTahanan_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarTahanan.pdf", "INMATES LIST DATA", gvPrintFormTahanan);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarTahanan.csv", "INMATES LIST DATA", gvPrintFormTahanan);
        }

        private void gvTahanan_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == null)
                return;

            switch (e.Column.Index)
            {
                case 0:
                    MainForm.formMain.PreviewDoc.Tag = "Inmates-" + e.Row.Cells["RegID"].Value.ToString();
                    MainForm.formMain.PreviewDoc.Show();
                    break;
            }
            
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     INMATES FORM PRINT";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;


            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Width = 15;
            btnDetail.Name = "btnDetail";
            gvPrintFormTahanan.AutoGenerateColumns = false;
            gvPrintFormTahanan.Columns.Insert(0, btnDetail);
            gvPrintFormTahanan.Refresh();
            gvPrintFormTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            UserFunction.LoadDataPrintFormTahananToGrid(gvPrintFormTahanan);
            UserFunction.SetInitGridView(gvPrintFormTahanan);

        }

    }
}
