﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;
using Telerik.WinControls.Export;
using System.IO;
using System.Threading;
using Telerik.WinControls.UI.Export;
using Telerik.Windows.Documents.Media;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListSeizedAssets : Base
    {
        public RadGridView GvSeizedAssets { get { return this.gvSeizedAssets; } }
        public ListSeizedAssets()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvSeizedAssets.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvSeizedAssets.RowFormatting += radGridView_RowFormatting;
            gvSeizedAssets.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvSeizedAssets.CellClick += gvSeizedAssets_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarBendaSitaan.pdf", "SEIZED ASSETS LIST DATA", gvSeizedAssets);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarBendaSitaan.csv", "SEIZED ASSETS LIST DATA", gvSeizedAssets);
        }

        private void gvSeizedAssets_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == null)
                return;

            switch (e.Column.Index)
            {
                case 0:
                    MainForm.formMain.SeizedAssetsDetail.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.SeizedAssetsDetail.Show();
                    break;
                case 1:
                    MainForm.formMain.SeizedAssets.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.AddNew;
                    MainForm.formMain.SeizedAssets.LoadData();
                    break;
                case 2:
                    var question  = UserFunction.Confirm("Hapus Benda Sitaan?");
                    if (question == DialogResult.Yes)
                    {
                        DeleteSeizedAssets(e.Row.Cells["Id"].Value.ToString());
                        UserFunction.MsgBox(TipeMsg.Info, "Benda Sitaan berhasil Dihapus !");
                        UserFunction.LoadDataSeizedAssetsToGrid(gvSeizedAssets);
                    }
                    break;
            }
            
        }

        private void DeleteSeizedAssets(string id)
        {
            using (SeizedAssetsService assets = new SeizedAssetsService())
            {
                SeizedAssetsService assetsserv = new SeizedAssetsService();
                var Data = assetsserv.GetByCaseId(Convert.ToInt32(id));
                foreach (var _data in Data)
                {
                    assetsserv.DeleteById(_data.Id);
                }
                assetsserv.DeleteSeizedAssetsCase(Convert.ToInt32(id));
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Delete Seized Assets, Data=" + UserFunction.JsonString(assets) });
            }
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DAFTAR BENDA YANG DISITA";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;


            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Name = "btnDetail";
            gvSeizedAssets.AutoGenerateColumns = false;
            gvSeizedAssets.Columns.Insert(0, btnDetail);
            gvSeizedAssets.Refresh();
            gvSeizedAssets.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnUbah = new GridViewImageColumn();
            btnUbah.HeaderText = "";
            btnUbah.Name = "btnUbah";
            gvSeizedAssets.AutoGenerateColumns = false;
            gvSeizedAssets.Columns.Insert(1, btnUbah);
            gvSeizedAssets.Refresh();
            gvSeizedAssets.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnHapus = new GridViewImageColumn();
            btnHapus.HeaderText = "";
            btnHapus.Name = "btnHapus";
            gvSeizedAssets.AutoGenerateColumns = false;
            gvSeizedAssets.Columns.Insert(2, btnHapus);
            gvSeizedAssets.Refresh();
            gvSeizedAssets.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            UserFunction.LoadDataSeizedAssetsToGrid(gvSeizedAssets);
            UserFunction.SetInitGridView(gvSeizedAssets);
        }
    }
}
