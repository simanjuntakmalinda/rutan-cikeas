﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;
using Telerik.WinControls.Export;
using System.IO;
using System.Threading;
using Telerik.WinControls.UI.Export;
using Telerik.Windows.Documents.Media;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListKendaraanKeluar : Base
    {
        public RadGridView GvCell { get { return this.gvKendaraan; } }
        private KendaraanTahanan data;
        public ListKendaraanKeluar()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvKendaraan.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvKendaraan.RowFormatting += radGridView_RowFormatting;
            gvKendaraan.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvKendaraan.CellClick += gvCell_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
            picRefresh.Click += PicRefresh_Click;
        }

        private void PicRefresh_Click(object sender, EventArgs e)
        {
            UserFunction.LoadDataKendaraanKeluarToGrid(gvKendaraan);
            gvKendaraan.Refresh();
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarKendaraanKeluar.pdf", "DAFTAR DATA KENDARAAN KELUAR",gvKendaraan);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarKendaraanKeluar.csv", "DAFTAR DATA KENDARAAN KELUAR", gvKendaraan);
        }

        private void gvCell_CellClick(object sender, GridViewCellEventArgs e)
        {
            KendaraanTahananService bonserv = new KendaraanTahananService();
            data = bonserv.GetById(Convert.ToInt32(e.Row.Cells["Id"].Value));
            if (e.Column == null)
            {
                return;
            }
            else { 
                    MainForm.formMain.KendaraanTahananDetail.Tag = e.Row.Cells["Id"].Value.ToString();
                MainForm.formMain.KendaraanTahananDetail.btncheckout.Hide();
                    MainForm.formMain.KendaraanTahananDetail.Show();
            }
        }

        private void DeleteCell(int id)
        {
            using (KendaraanTahananService cell = new KendaraanTahananService())
            {
                var data = cell.GetById(id);
                cell.DeleteById(id);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERCELL.ToString(), Activities = "Delete Master Cell, Data=" + UserFunction.JsonString(data) });
            }
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DAFTAR DATA KENDARAAN KELUAR";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;
            this.lblDetail.BackColor = Color.FromArgb(77, 77, 77);
            this.lblDetail.ForeColor = Color.White;

            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Name = "btnDetail";
            gvKendaraan.AutoGenerateColumns = false;
            gvKendaraan.Columns.Insert(0, btnDetail);
            gvKendaraan.Refresh();
            gvKendaraan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            UserFunction.LoadDataKendaraanKeluarToGrid(gvKendaraan);
            UserFunction.SetInitGridView(gvKendaraan);

        }
    }
}
