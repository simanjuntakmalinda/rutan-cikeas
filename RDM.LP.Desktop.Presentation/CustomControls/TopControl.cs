﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.Themes;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;
using System.Reflection;
using Telerik.WinControls.Analytics;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class TopControl : UserControl
    {
        public RadLabel ViewLabel { get { return this.viewLabel; } }
        public PictureBox LogOut { get { return this.pictureBox1; } }
        public PictureBox ChangePsw { get { return this.pictureBox2; } }
        public TopControl()
        {
            InitializeComponent();
            ThemeResolutionService.ApplicationThemeName = "Material";
            toolTip1.SetToolTip(this.pictureBox1, Language.GetLanguageString(LanguangeId.LOGOUT_FROM_SYSTEM));
            toolTip2.SetToolTip(this.pictureBox2, Language.GetLanguageString(LanguangeId.CHANGE_PASSWORD));
        }
    }
}
