﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class FormBonTahanan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBonTahanan));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.cameraSource = new RDM.LP.Desktop.Presentation.CustomControls.CameraSource();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnBack = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            this.radPanelError = new Telerik.WinControls.UI.RadPanel();
            this.lblError = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.PanelRb = new Telerik.WinControls.UI.RadPanel();
            this.rbJaksa = new Telerik.WinControls.UI.RadRadioButton();
            this.rbPersonel = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel19 = new Telerik.WinControls.UI.RadPanel();
            this.txtNoBonTahanan = new Telerik.WinControls.UI.RadTextBox();
            this.lblDetailVisitor = new Telerik.WinControls.UI.RadLabel();
            this.lblNrpDisetujui = new Telerik.WinControls.UI.RadLabel();
            this.lblNamaDisetujui = new Telerik.WinControls.UI.RadLabel();
            this.radPanel8 = new Telerik.WinControls.UI.RadPanel();
            this.txtNrpDisetujui = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel13 = new Telerik.WinControls.UI.RadPanel();
            this.ddlPangkatDisetujui = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel14 = new Telerik.WinControls.UI.RadPanel();
            this.txtSatkerDisetujui = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.txtNamaDisetujui = new Telerik.WinControls.UI.RadTextBox();
            this.lblDisetujui = new Telerik.WinControls.UI.RadLabel();
            this.lblNoHpPJ = new Telerik.WinControls.UI.RadLabel();
            this.lblNrpPJ = new Telerik.WinControls.UI.RadLabel();
            this.lblNamaPJ = new Telerik.WinControls.UI.RadLabel();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.txtNohpPJ = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.txtNrpPJ = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel6 = new Telerik.WinControls.UI.RadPanel();
            this.ddlPangkatPJ = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel7 = new Telerik.WinControls.UI.RadPanel();
            this.txtSatkerPJ = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel5 = new Telerik.WinControls.UI.RadPanel();
            this.txtNamaPJ = new Telerik.WinControls.UI.RadTextBox();
            this.lblPenanggungJawab = new Telerik.WinControls.UI.RadLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNoHpPemohon = new Telerik.WinControls.UI.RadLabel();
            this.lblNrpPemohon = new Telerik.WinControls.UI.RadLabel();
            this.lblNamaPemohon = new Telerik.WinControls.UI.RadLabel();
            this.radPanelNoHp = new Telerik.WinControls.UI.RadPanel();
            this.txtNohpPemohon = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel10 = new Telerik.WinControls.UI.RadPanel();
            this.txtNrpPemohon = new Telerik.WinControls.UI.RadTextBox();
            this.radPanelPangkatPemohon = new Telerik.WinControls.UI.RadPanel();
            this.ddlPangkatPemohon = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanelSatkerPemohon = new Telerik.WinControls.UI.RadPanel();
            this.txtSatkerPemohon = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel11 = new Telerik.WinControls.UI.RadPanel();
            this.txtNamaPemohon = new Telerik.WinControls.UI.RadTextBox();
            this.lblKeterangan = new Telerik.WinControls.UI.RadLabel();
            this.lblLokasi = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.lblKeperluan = new Telerik.WinControls.UI.RadLabel();
            this.lblNamaTahanan = new Telerik.WinControls.UI.RadLabel();
            this.radPanel15 = new Telerik.WinControls.UI.RadPanel();
            this.txtKeterangan = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel16 = new Telerik.WinControls.UI.RadPanel();
            this.txtLokasi = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel20 = new Telerik.WinControls.UI.RadPanel();
            this.dpTanggalSelesai = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radPanel17 = new Telerik.WinControls.UI.RadPanel();
            this.dpTanggalMulai = new Telerik.WinControls.UI.RadDateTimePicker();
            this.btnAddKeperluan = new Telerik.WinControls.UI.RadButton();
            this.radPanel18 = new Telerik.WinControls.UI.RadPanel();
            this.ddlKeperluanBon = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.radPanel21 = new Telerik.WinControls.UI.RadPanel();
            this.txtListTahanan = new Telerik.WinControls.UI.RadTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSearchTahanan = new Telerik.WinControls.UI.RadButton();
            this.btnClearTahanan = new Telerik.WinControls.UI.RadButton();
            this.lblDetailSurat = new Telerik.WinControls.UI.RadLabel();
            this.headerPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).BeginInit();
            this.radPanelError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelRb)).BeginInit();
            this.PanelRb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbJaksa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbPersonel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel19)).BeginInit();
            this.radPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoBonTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNrpDisetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaDisetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).BeginInit();
            this.radPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNrpDisetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel13)).BeginInit();
            this.radPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPangkatDisetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel14)).BeginInit();
            this.radPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSatkerDisetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaDisetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDisetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoHpPJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNrpPJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaPJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            this.radPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNohpPJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNrpPJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).BeginInit();
            this.radPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPangkatPJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).BeginInit();
            this.radPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSatkerPJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).BeginInit();
            this.radPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaPJ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPenanggungJawab)).BeginInit();
            this.lblPenanggungJawab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoHpPemohon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNrpPemohon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaPemohon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelNoHp)).BeginInit();
            this.radPanelNoHp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNohpPemohon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel10)).BeginInit();
            this.radPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNrpPemohon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelPangkatPemohon)).BeginInit();
            this.radPanelPangkatPemohon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPangkatPemohon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelSatkerPemohon)).BeginInit();
            this.radPanelSatkerPemohon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSatkerPemohon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel11)).BeginInit();
            this.radPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaPemohon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKeterangan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLokasi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKeperluan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel15)).BeginInit();
            this.radPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeterangan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel16)).BeginInit();
            this.radPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLokasi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel20)).BeginInit();
            this.radPanel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dpTanggalSelesai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel17)).BeginInit();
            this.radPanel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dpTanggalMulai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddKeperluan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel18)).BeginInit();
            this.radPanel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlKeperluanBon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            this.radLabel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel21)).BeginInit();
            this.radPanel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtListTahanan)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearchTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClearTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).BeginInit();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // cameraSource
            // 
            this.cameraSource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cameraSource.Location = new System.Drawing.Point(32700, 320);
            this.cameraSource.Name = "cameraSource";
            this.cameraSource.Size = new System.Drawing.Size(581, 186);
            this.cameraSource.TabIndex = 82;
            this.cameraSource.Visible = false;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnBack);
            this.lblButton.Controls.Add(this.btnSave);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 642);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(20);
            this.lblButton.Size = new System.Drawing.Size(1183, 94);
            this.lblButton.TabIndex = 81;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.btnBack_Image;
            this.btnBack.Location = new System.Drawing.Point(20, 20);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(244, 54);
            this.btnBack.TabIndex = 25;
            this.btnBack.Tag = "";
            this.btnBack.Text = "&Batal";
            this.btnBack.ThemeName = "MaterialBlueGrey";
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.if_checked_2_309094;
            this.btnSave.Location = new System.Drawing.Point(858, 20);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(305, 54);
            this.btnSave.TabIndex = 24;
            this.btnSave.Text = "&Simpan";
            this.btnSave.ThemeName = "MaterialBlueGrey";
            // 
            // radPanelError
            // 
            this.radPanelError.Controls.Add(this.lblError);
            this.radPanelError.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanelError.Location = new System.Drawing.Point(0, 598);
            this.radPanelError.Margin = new System.Windows.Forms.Padding(0);
            this.radPanelError.Name = "radPanelError";
            this.radPanelError.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radPanelError.Size = new System.Drawing.Size(1183, 43);
            this.radPanelError.TabIndex = 80;
            // 
            // lblError
            // 
            this.lblError.AutoSize = false;
            this.lblError.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.warning;
            this.lblError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblError.Location = new System.Drawing.Point(15, 0);
            this.lblError.Name = "lblError";
            this.lblError.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.lblError.Size = new System.Drawing.Size(1168, 39);
            this.lblError.TabIndex = 24;
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("radScrollablePanel1.BackgroundImage")));
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 108);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(1164, 488);
            this.radScrollablePanel1.Size = new System.Drawing.Size(1183, 490);
            this.radScrollablePanel1.TabIndex = 78;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 380F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutPanel1.Controls.Add(this.PanelRb, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.radLabel4, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.radLabel3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel19, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailVisitor, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblNoHpPemohon, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.lblNrpPemohon, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.lblNamaPemohon, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.radPanelNoHp, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.radPanel10, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.radPanelPangkatPemohon, 2, 11);
            this.tableLayoutPanel1.Controls.Add(this.radPanelSatkerPemohon, 3, 11);
            this.tableLayoutPanel1.Controls.Add(this.radPanel11, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.lblKeterangan, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblLokasi, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.radLabel2, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblKeperluan, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblNamaTahanan, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.radPanel15, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.radPanel16, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.radPanel20, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.radPanel17, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnAddKeperluan, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel18, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.radLabel5, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.radPanel21, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblPenanggungJawab, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.lblNamaPJ, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.radPanel5, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.lblNrpPJ, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 1, 15);
            this.tableLayoutPanel1.Controls.Add(this.radPanel6, 2, 15);
            this.tableLayoutPanel1.Controls.Add(this.radPanel7, 3, 15);
            this.tableLayoutPanel1.Controls.Add(this.lblNoHpPJ, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.radPanel4, 1, 16);
            this.tableLayoutPanel1.Controls.Add(this.lblDisetujui, 0, 17);
            this.tableLayoutPanel1.Controls.Add(this.lblNamaDisetujui, 0, 18);
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 1, 18);
            this.tableLayoutPanel1.Controls.Add(this.lblNrpDisetujui, 0, 19);
            this.tableLayoutPanel1.Controls.Add(this.radPanel8, 1, 19);
            this.tableLayoutPanel1.Controls.Add(this.radPanel13, 2, 19);
            this.tableLayoutPanel1.Controls.Add(this.radPanel14, 3, 19);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 21;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1164, 1200);
            this.tableLayoutPanel1.TabIndex = 16;
            // 
            // PanelRb
            // 
            this.PanelRb.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.SetColumnSpan(this.PanelRb, 2);
            this.PanelRb.Controls.Add(this.rbJaksa);
            this.PanelRb.Controls.Add(this.rbPersonel);
            this.PanelRb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelRb.Location = new System.Drawing.Point(393, 613);
            this.PanelRb.Name = "PanelRb";
            this.PanelRb.Size = new System.Drawing.Size(464, 44);
            this.PanelRb.TabIndex = 199;
            // 
            // rbJaksa
            // 
            this.rbJaksa.AutoSize = false;
            this.rbJaksa.Dock = System.Windows.Forms.DockStyle.Right;
            this.rbJaksa.Location = new System.Drawing.Point(158, 0);
            this.rbJaksa.Name = "rbJaksa";
            this.rbJaksa.Size = new System.Drawing.Size(306, 44);
            this.rbJaksa.TabIndex = 2;
            this.rbJaksa.TabStop = false;
            this.rbJaksa.Text = "JAKSA";
            // 
            // rbPersonel
            // 
            this.rbPersonel.AutoSize = false;
            this.rbPersonel.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbPersonel.Location = new System.Drawing.Point(0, 0);
            this.rbPersonel.Name = "rbPersonel";
            this.rbPersonel.Size = new System.Drawing.Size(151, 44);
            this.rbPersonel.TabIndex = 3;
            this.rbPersonel.TabStop = false;
            this.rbPersonel.Text = "PERSONEL";
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel4.ForeColor = System.Drawing.Color.Black;
            this.radLabel4.Location = new System.Drawing.Point(14, 614);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(372, 42);
            this.radLabel4.TabIndex = 131;
            this.radLabel4.Text = "<html>Personel / Jaksa<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel3.ForeColor = System.Drawing.Color.Black;
            this.radLabel3.Location = new System.Drawing.Point(14, 64);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(372, 42);
            this.radLabel3.TabIndex = 34;
            this.radLabel3.Text = "<html>No Bon Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel19
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel19, 2);
            this.radPanel19.Controls.Add(this.txtNoBonTahanan);
            this.radPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel19.Location = new System.Drawing.Point(393, 63);
            this.radPanel19.Name = "radPanel19";
            this.radPanel19.Size = new System.Drawing.Size(464, 44);
            this.radPanel19.TabIndex = 1;
            // 
            // txtNoBonTahanan
            // 
            this.txtNoBonTahanan.AutoSize = false;
            this.txtNoBonTahanan.BackColor = System.Drawing.Color.White;
            this.txtNoBonTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoBonTahanan.Location = new System.Drawing.Point(0, 0);
            this.txtNoBonTahanan.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNoBonTahanan.Name = "txtNoBonTahanan";
            this.txtNoBonTahanan.Size = new System.Drawing.Size(464, 44);
            this.txtNoBonTahanan.TabIndex = 1;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoBonTahanan.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoBonTahanan.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoBonTahanan.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoBonTahanan.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // lblDetailVisitor
            // 
            this.lblDetailVisitor.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailVisitor, 4);
            this.lblDetailVisitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailVisitor.Location = new System.Drawing.Point(13, 13);
            this.lblDetailVisitor.Name = "lblDetailVisitor";
            this.lblDetailVisitor.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDetailVisitor.Size = new System.Drawing.Size(1079, 44);
            this.lblDetailVisitor.TabIndex = 134;
            this.lblDetailVisitor.Text = "BON TAHANAN DETAILS";
            // 
            // lblNrpDisetujui
            // 
            this.lblNrpDisetujui.AutoSize = false;
            this.lblNrpDisetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNrpDisetujui.ForeColor = System.Drawing.Color.Black;
            this.lblNrpDisetujui.Location = new System.Drawing.Point(14, 1114);
            this.lblNrpDisetujui.Margin = new System.Windows.Forms.Padding(4);
            this.lblNrpDisetujui.Name = "lblNrpDisetujui";
            this.lblNrpDisetujui.Size = new System.Drawing.Size(372, 42);
            this.lblNrpDisetujui.TabIndex = 139;
            this.lblNrpDisetujui.Text = "<html>NRP / Pangkat / Satuan Kerja<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblNamaDisetujui
            // 
            this.lblNamaDisetujui.AutoSize = false;
            this.lblNamaDisetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNamaDisetujui.ForeColor = System.Drawing.Color.Black;
            this.lblNamaDisetujui.Location = new System.Drawing.Point(14, 1064);
            this.lblNamaDisetujui.Margin = new System.Windows.Forms.Padding(4);
            this.lblNamaDisetujui.Name = "lblNamaDisetujui";
            this.lblNamaDisetujui.Size = new System.Drawing.Size(372, 42);
            this.lblNamaDisetujui.TabIndex = 138;
            this.lblNamaDisetujui.Text = "<html>Nama <span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel8
            // 
            this.radPanel8.Controls.Add(this.txtNrpDisetujui);
            this.radPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel8.Location = new System.Drawing.Point(393, 1113);
            this.radPanel8.Name = "radPanel8";
            this.radPanel8.Size = new System.Drawing.Size(229, 44);
            this.radPanel8.TabIndex = 21;
            // 
            // txtNrpDisetujui
            // 
            this.txtNrpDisetujui.AutoSize = false;
            this.txtNrpDisetujui.BackColor = System.Drawing.Color.White;
            this.txtNrpDisetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNrpDisetujui.Location = new System.Drawing.Point(0, 0);
            this.txtNrpDisetujui.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNrpDisetujui.MaxLength = 8;
            this.txtNrpDisetujui.Name = "txtNrpDisetujui";
            this.txtNrpDisetujui.Size = new System.Drawing.Size(229, 44);
            this.txtNrpDisetujui.TabIndex = 21;
            // 
            // radPanel13
            // 
            this.radPanel13.Controls.Add(this.ddlPangkatDisetujui);
            this.radPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel13.Location = new System.Drawing.Point(628, 1113);
            this.radPanel13.Name = "radPanel13";
            this.radPanel13.Size = new System.Drawing.Size(229, 44);
            this.radPanel13.TabIndex = 22;
            // 
            // ddlPangkatDisetujui
            // 
            this.ddlPangkatDisetujui.AutoSize = false;
            this.ddlPangkatDisetujui.BackColor = System.Drawing.Color.White;
            this.ddlPangkatDisetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlPangkatDisetujui.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlPangkatDisetujui.DropDownHeight = 400;
            this.ddlPangkatDisetujui.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlPangkatDisetujui.Location = new System.Drawing.Point(0, 0);
            this.ddlPangkatDisetujui.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlPangkatDisetujui.Name = "ddlPangkatDisetujui";
            // 
            // 
            // 
            this.ddlPangkatDisetujui.RootElement.CustomFont = "Roboto";
            this.ddlPangkatDisetujui.RootElement.CustomFontSize = 13F;
            this.ddlPangkatDisetujui.Size = new System.Drawing.Size(229, 44);
            this.ddlPangkatDisetujui.TabIndex = 22;
            this.ddlPangkatDisetujui.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatDisetujui.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatDisetujui.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatDisetujui.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatDisetujui.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatDisetujui.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatDisetujui.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatDisetujui.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatDisetujui.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPangkatDisetujui.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPangkatDisetujui.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel14
            // 
            this.radPanel14.Controls.Add(this.txtSatkerDisetujui);
            this.radPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel14.Location = new System.Drawing.Point(863, 1113);
            this.radPanel14.Name = "radPanel14";
            this.radPanel14.Size = new System.Drawing.Size(229, 44);
            this.radPanel14.TabIndex = 23;
            // 
            // txtSatkerDisetujui
            // 
            this.txtSatkerDisetujui.AutoSize = false;
            this.txtSatkerDisetujui.BackColor = System.Drawing.Color.White;
            this.txtSatkerDisetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSatkerDisetujui.Location = new System.Drawing.Point(0, 0);
            this.txtSatkerDisetujui.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtSatkerDisetujui.Name = "txtSatkerDisetujui";
            this.txtSatkerDisetujui.Size = new System.Drawing.Size(229, 44);
            this.txtSatkerDisetujui.TabIndex = 23;
            // 
            // radPanel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel3, 2);
            this.radPanel3.Controls.Add(this.txtNamaDisetujui);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Location = new System.Drawing.Point(393, 1063);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(464, 44);
            this.radPanel3.TabIndex = 20;
            // 
            // txtNamaDisetujui
            // 
            this.txtNamaDisetujui.AutoSize = false;
            this.txtNamaDisetujui.BackColor = System.Drawing.Color.White;
            this.txtNamaDisetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNamaDisetujui.Location = new System.Drawing.Point(0, 0);
            this.txtNamaDisetujui.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNamaDisetujui.Name = "txtNamaDisetujui";
            this.txtNamaDisetujui.Size = new System.Drawing.Size(464, 44);
            this.txtNamaDisetujui.TabIndex = 20;
            // 
            // lblDisetujui
            // 
            this.lblDisetujui.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDisetujui, 4);
            this.lblDisetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDisetujui.Location = new System.Drawing.Point(13, 1013);
            this.lblDisetujui.Name = "lblDisetujui";
            this.lblDisetujui.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDisetujui.Size = new System.Drawing.Size(1079, 44);
            this.lblDisetujui.TabIndex = 135;
            this.lblDisetujui.Text = "YANG MENYETUJUI";
            // 
            // lblNoHpPJ
            // 
            this.lblNoHpPJ.AutoSize = false;
            this.lblNoHpPJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoHpPJ.ForeColor = System.Drawing.Color.Black;
            this.lblNoHpPJ.Location = new System.Drawing.Point(14, 964);
            this.lblNoHpPJ.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoHpPJ.Name = "lblNoHpPJ";
            this.lblNoHpPJ.Size = new System.Drawing.Size(372, 42);
            this.lblNoHpPJ.TabIndex = 139;
            this.lblNoHpPJ.Text = "<html>No. Hp<span style=\"color: #ff0000\"></span></html>";
            // 
            // lblNrpPJ
            // 
            this.lblNrpPJ.AutoSize = false;
            this.lblNrpPJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNrpPJ.ForeColor = System.Drawing.Color.Black;
            this.lblNrpPJ.Location = new System.Drawing.Point(14, 914);
            this.lblNrpPJ.Margin = new System.Windows.Forms.Padding(4);
            this.lblNrpPJ.Name = "lblNrpPJ";
            this.lblNrpPJ.Size = new System.Drawing.Size(372, 42);
            this.lblNrpPJ.TabIndex = 136;
            this.lblNrpPJ.Text = "<html>NRP / Pangkat / Satuan Kerja<span style=\"color: #ff0000\"></span></html>";
            // 
            // lblNamaPJ
            // 
            this.lblNamaPJ.AutoSize = false;
            this.lblNamaPJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNamaPJ.ForeColor = System.Drawing.Color.Black;
            this.lblNamaPJ.Location = new System.Drawing.Point(14, 864);
            this.lblNamaPJ.Margin = new System.Windows.Forms.Padding(4);
            this.lblNamaPJ.Name = "lblNamaPJ";
            this.lblNamaPJ.Size = new System.Drawing.Size(372, 42);
            this.lblNamaPJ.TabIndex = 137;
            this.lblNamaPJ.Text = "<html>Nama <span style=\"color: #ff0000\"></span></html>";
            // 
            // radPanel4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel4, 2);
            this.radPanel4.Controls.Add(this.txtNohpPJ);
            this.radPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel4.Location = new System.Drawing.Point(393, 963);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(464, 44);
            this.radPanel4.TabIndex = 19;
            // 
            // txtNohpPJ
            // 
            this.txtNohpPJ.AutoSize = false;
            this.txtNohpPJ.BackColor = System.Drawing.Color.White;
            this.txtNohpPJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNohpPJ.Enabled = false;
            this.txtNohpPJ.Location = new System.Drawing.Point(0, 0);
            this.txtNohpPJ.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNohpPJ.Name = "txtNohpPJ";
            this.txtNohpPJ.Size = new System.Drawing.Size(464, 44);
            this.txtNohpPJ.TabIndex = 19;
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.txtNrpPJ);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(393, 913);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(229, 44);
            this.radPanel2.TabIndex = 16;
            // 
            // txtNrpPJ
            // 
            this.txtNrpPJ.AutoSize = false;
            this.txtNrpPJ.BackColor = System.Drawing.Color.White;
            this.txtNrpPJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNrpPJ.Location = new System.Drawing.Point(0, 0);
            this.txtNrpPJ.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNrpPJ.MaxLength = 8;
            this.txtNrpPJ.Name = "txtNrpPJ";
            this.txtNrpPJ.Size = new System.Drawing.Size(229, 44);
            this.txtNrpPJ.TabIndex = 16;
            // 
            // radPanel6
            // 
            this.radPanel6.Controls.Add(this.ddlPangkatPJ);
            this.radPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel6.Location = new System.Drawing.Point(628, 913);
            this.radPanel6.Name = "radPanel6";
            this.radPanel6.Size = new System.Drawing.Size(229, 44);
            this.radPanel6.TabIndex = 17;
            // 
            // ddlPangkatPJ
            // 
            this.ddlPangkatPJ.AutoSize = false;
            this.ddlPangkatPJ.BackColor = System.Drawing.Color.White;
            this.ddlPangkatPJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlPangkatPJ.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlPangkatPJ.DropDownHeight = 400;
            this.ddlPangkatPJ.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlPangkatPJ.Location = new System.Drawing.Point(0, 0);
            this.ddlPangkatPJ.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlPangkatPJ.Name = "ddlPangkatPJ";
            // 
            // 
            // 
            this.ddlPangkatPJ.RootElement.CustomFont = "Roboto";
            this.ddlPangkatPJ.RootElement.CustomFontSize = 13F;
            this.ddlPangkatPJ.Size = new System.Drawing.Size(229, 44);
            this.ddlPangkatPJ.TabIndex = 17;
            this.ddlPangkatPJ.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatPJ.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatPJ.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatPJ.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatPJ.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatPJ.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatPJ.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatPJ.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatPJ.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPangkatPJ.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPangkatPJ.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel7
            // 
            this.radPanel7.Controls.Add(this.txtSatkerPJ);
            this.radPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel7.Location = new System.Drawing.Point(863, 913);
            this.radPanel7.Name = "radPanel7";
            this.radPanel7.Size = new System.Drawing.Size(229, 44);
            this.radPanel7.TabIndex = 18;
            // 
            // txtSatkerPJ
            // 
            this.txtSatkerPJ.AutoSize = false;
            this.txtSatkerPJ.BackColor = System.Drawing.Color.White;
            this.txtSatkerPJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSatkerPJ.Location = new System.Drawing.Point(0, 0);
            this.txtSatkerPJ.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtSatkerPJ.Name = "txtSatkerPJ";
            this.txtSatkerPJ.Size = new System.Drawing.Size(229, 44);
            this.txtSatkerPJ.TabIndex = 18;
            // 
            // radPanel5
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel5, 2);
            this.radPanel5.Controls.Add(this.txtNamaPJ);
            this.radPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel5.Location = new System.Drawing.Point(393, 863);
            this.radPanel5.Name = "radPanel5";
            this.radPanel5.Size = new System.Drawing.Size(464, 44);
            this.radPanel5.TabIndex = 15;
            // 
            // txtNamaPJ
            // 
            this.txtNamaPJ.AutoSize = false;
            this.txtNamaPJ.BackColor = System.Drawing.Color.White;
            this.txtNamaPJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNamaPJ.Location = new System.Drawing.Point(0, 0);
            this.txtNamaPJ.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNamaPJ.Name = "txtNamaPJ";
            this.txtNamaPJ.Size = new System.Drawing.Size(464, 44);
            this.txtNamaPJ.TabIndex = 15;
            // 
            // lblPenanggungJawab
            // 
            this.lblPenanggungJawab.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblPenanggungJawab, 4);
            this.lblPenanggungJawab.Controls.Add(this.label2);
            this.lblPenanggungJawab.Location = new System.Drawing.Point(13, 813);
            this.lblPenanggungJawab.Name = "lblPenanggungJawab";
            this.lblPenanggungJawab.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblPenanggungJawab.Size = new System.Drawing.Size(1079, 44);
            this.lblPenanggungJawab.TabIndex = 195;
            this.lblPenanggungJawab.Text = "YANG MENJEMPUT";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(1077, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 44);
            this.label2.TabIndex = 167;
            this.label2.Visible = false;
            // 
            // lblNoHpPemohon
            // 
            this.lblNoHpPemohon.AutoSize = false;
            this.lblNoHpPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoHpPemohon.ForeColor = System.Drawing.Color.Black;
            this.lblNoHpPemohon.Location = new System.Drawing.Point(14, 764);
            this.lblNoHpPemohon.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoHpPemohon.Name = "lblNoHpPemohon";
            this.lblNoHpPemohon.Size = new System.Drawing.Size(372, 42);
            this.lblNoHpPemohon.TabIndex = 131;
            this.lblNoHpPemohon.Text = "<html>No. Hp<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblNrpPemohon
            // 
            this.lblNrpPemohon.AutoSize = false;
            this.lblNrpPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNrpPemohon.ForeColor = System.Drawing.Color.Black;
            this.lblNrpPemohon.Location = new System.Drawing.Point(14, 714);
            this.lblNrpPemohon.Margin = new System.Windows.Forms.Padding(4);
            this.lblNrpPemohon.Name = "lblNrpPemohon";
            this.lblNrpPemohon.Size = new System.Drawing.Size(372, 42);
            this.lblNrpPemohon.TabIndex = 131;
            this.lblNrpPemohon.Text = "<html>NRP / Pangkat / Satuan Kerja<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblNamaPemohon
            // 
            this.lblNamaPemohon.AutoSize = false;
            this.lblNamaPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNamaPemohon.ForeColor = System.Drawing.Color.Black;
            this.lblNamaPemohon.Location = new System.Drawing.Point(14, 664);
            this.lblNamaPemohon.Margin = new System.Windows.Forms.Padding(4);
            this.lblNamaPemohon.Name = "lblNamaPemohon";
            this.lblNamaPemohon.Size = new System.Drawing.Size(372, 42);
            this.lblNamaPemohon.TabIndex = 130;
            this.lblNamaPemohon.Text = "<html>Nama<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanelNoHp
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanelNoHp, 2);
            this.radPanelNoHp.Controls.Add(this.txtNohpPemohon);
            this.radPanelNoHp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanelNoHp.Location = new System.Drawing.Point(393, 763);
            this.radPanelNoHp.Name = "radPanelNoHp";
            this.radPanelNoHp.Size = new System.Drawing.Size(464, 44);
            this.radPanelNoHp.TabIndex = 14;
            // 
            // txtNohpPemohon
            // 
            this.txtNohpPemohon.AutoSize = false;
            this.txtNohpPemohon.BackColor = System.Drawing.Color.White;
            this.txtNohpPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNohpPemohon.Location = new System.Drawing.Point(0, 0);
            this.txtNohpPemohon.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNohpPemohon.Name = "txtNohpPemohon";
            this.txtNohpPemohon.Size = new System.Drawing.Size(464, 44);
            this.txtNohpPemohon.TabIndex = 14;
            // 
            // radPanel10
            // 
            this.radPanel10.Controls.Add(this.txtNrpPemohon);
            this.radPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel10.Location = new System.Drawing.Point(393, 713);
            this.radPanel10.Name = "radPanel10";
            this.radPanel10.Size = new System.Drawing.Size(229, 44);
            this.radPanel10.TabIndex = 11;
            // 
            // txtNrpPemohon
            // 
            this.txtNrpPemohon.AutoSize = false;
            this.txtNrpPemohon.BackColor = System.Drawing.Color.White;
            this.txtNrpPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNrpPemohon.Location = new System.Drawing.Point(0, 0);
            this.txtNrpPemohon.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNrpPemohon.MaxLength = 18;
            this.txtNrpPemohon.Name = "txtNrpPemohon";
            this.txtNrpPemohon.Size = new System.Drawing.Size(229, 44);
            this.txtNrpPemohon.TabIndex = 11;
            // 
            // radPanelPangkatPemohon
            // 
            this.radPanelPangkatPemohon.Controls.Add(this.ddlPangkatPemohon);
            this.radPanelPangkatPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanelPangkatPemohon.Location = new System.Drawing.Point(628, 713);
            this.radPanelPangkatPemohon.Name = "radPanelPangkatPemohon";
            this.radPanelPangkatPemohon.Size = new System.Drawing.Size(229, 44);
            this.radPanelPangkatPemohon.TabIndex = 12;
            // 
            // ddlPangkatPemohon
            // 
            this.ddlPangkatPemohon.AutoSize = false;
            this.ddlPangkatPemohon.BackColor = System.Drawing.Color.White;
            this.ddlPangkatPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlPangkatPemohon.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlPangkatPemohon.DropDownHeight = 400;
            this.ddlPangkatPemohon.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlPangkatPemohon.Location = new System.Drawing.Point(0, 0);
            this.ddlPangkatPemohon.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlPangkatPemohon.Name = "ddlPangkatPemohon";
            // 
            // 
            // 
            this.ddlPangkatPemohon.RootElement.CustomFont = "Roboto";
            this.ddlPangkatPemohon.RootElement.CustomFontSize = 13F;
            this.ddlPangkatPemohon.Size = new System.Drawing.Size(229, 44);
            this.ddlPangkatPemohon.TabIndex = 12;
            this.ddlPangkatPemohon.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatPemohon.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatPemohon.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatPemohon.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatPemohon.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatPemohon.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatPemohon.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatPemohon.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatPemohon.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPangkatPemohon.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPangkatPemohon.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanelSatkerPemohon
            // 
            this.radPanelSatkerPemohon.Controls.Add(this.txtSatkerPemohon);
            this.radPanelSatkerPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanelSatkerPemohon.Location = new System.Drawing.Point(863, 713);
            this.radPanelSatkerPemohon.Name = "radPanelSatkerPemohon";
            this.radPanelSatkerPemohon.Size = new System.Drawing.Size(229, 44);
            this.radPanelSatkerPemohon.TabIndex = 13;
            // 
            // txtSatkerPemohon
            // 
            this.txtSatkerPemohon.AutoSize = false;
            this.txtSatkerPemohon.BackColor = System.Drawing.Color.White;
            this.txtSatkerPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSatkerPemohon.Location = new System.Drawing.Point(0, 0);
            this.txtSatkerPemohon.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtSatkerPemohon.Name = "txtSatkerPemohon";
            this.txtSatkerPemohon.Size = new System.Drawing.Size(229, 44);
            this.txtSatkerPemohon.TabIndex = 13;
            // 
            // radPanel11
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel11, 2);
            this.radPanel11.Controls.Add(this.txtNamaPemohon);
            this.radPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel11.Location = new System.Drawing.Point(393, 663);
            this.radPanel11.Name = "radPanel11";
            this.radPanel11.Size = new System.Drawing.Size(464, 44);
            this.radPanel11.TabIndex = 10;
            // 
            // txtNamaPemohon
            // 
            this.txtNamaPemohon.AutoSize = false;
            this.txtNamaPemohon.BackColor = System.Drawing.Color.White;
            this.txtNamaPemohon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNamaPemohon.Location = new System.Drawing.Point(0, 0);
            this.txtNamaPemohon.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNamaPemohon.Name = "txtNamaPemohon";
            this.txtNamaPemohon.Size = new System.Drawing.Size(464, 44);
            this.txtNamaPemohon.TabIndex = 10;
            // 
            // lblKeterangan
            // 
            this.lblKeterangan.AutoSize = false;
            this.lblKeterangan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKeterangan.ForeColor = System.Drawing.Color.Black;
            this.lblKeterangan.Location = new System.Drawing.Point(14, 414);
            this.lblKeterangan.Margin = new System.Windows.Forms.Padding(4);
            this.lblKeterangan.Name = "lblKeterangan";
            this.lblKeterangan.Size = new System.Drawing.Size(372, 142);
            this.lblKeterangan.TabIndex = 32;
            this.lblKeterangan.Text = "<html>Keterangan<span style=\"color: #ff0000\"> *</span></html>";
            this.lblKeterangan.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // lblLokasi
            // 
            this.lblLokasi.AutoSize = false;
            this.lblLokasi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLokasi.ForeColor = System.Drawing.Color.Black;
            this.lblLokasi.Location = new System.Drawing.Point(14, 364);
            this.lblLokasi.Margin = new System.Windows.Forms.Padding(4);
            this.lblLokasi.Name = "lblLokasi";
            this.lblLokasi.Size = new System.Drawing.Size(372, 42);
            this.lblLokasi.TabIndex = 33;
            this.lblLokasi.Text = "<html>Lokasi<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel2.ForeColor = System.Drawing.Color.Black;
            this.radLabel2.Location = new System.Drawing.Point(14, 314);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(372, 42);
            this.radLabel2.TabIndex = 34;
            this.radLabel2.Text = "<html>Tanggal Selesai<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.ForeColor = System.Drawing.Color.Black;
            this.radLabel1.Location = new System.Drawing.Point(14, 264);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(372, 42);
            this.radLabel1.TabIndex = 34;
            this.radLabel1.Text = "<html>Tanggal Mulai<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblKeperluan
            // 
            this.lblKeperluan.AutoSize = false;
            this.lblKeperluan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKeperluan.ForeColor = System.Drawing.Color.Black;
            this.lblKeperluan.Location = new System.Drawing.Point(14, 214);
            this.lblKeperluan.Margin = new System.Windows.Forms.Padding(4);
            this.lblKeperluan.Name = "lblKeperluan";
            this.lblKeperluan.Size = new System.Drawing.Size(372, 42);
            this.lblKeperluan.TabIndex = 18;
            this.lblKeperluan.Text = "<html>Keperluan Bon Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblNamaTahanan
            // 
            this.lblNamaTahanan.AutoSize = false;
            this.lblNamaTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNamaTahanan.ForeColor = System.Drawing.Color.Black;
            this.lblNamaTahanan.Location = new System.Drawing.Point(14, 114);
            this.lblNamaTahanan.Margin = new System.Windows.Forms.Padding(4);
            this.lblNamaTahanan.Name = "lblNamaTahanan";
            this.lblNamaTahanan.Size = new System.Drawing.Size(372, 92);
            this.lblNamaTahanan.TabIndex = 19;
            this.lblNamaTahanan.Text = "<html>Nama Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            this.lblNamaTahanan.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radPanel15
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel15, 3);
            this.radPanel15.Controls.Add(this.txtKeterangan);
            this.radPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel15.Location = new System.Drawing.Point(393, 413);
            this.radPanel15.Name = "radPanel15";
            this.radPanel15.Size = new System.Drawing.Size(699, 144);
            this.radPanel15.TabIndex = 9;
            // 
            // txtKeterangan
            // 
            this.txtKeterangan.AutoSize = false;
            this.txtKeterangan.BackColor = System.Drawing.Color.White;
            this.txtKeterangan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtKeterangan.Location = new System.Drawing.Point(0, 0);
            this.txtKeterangan.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtKeterangan.Multiline = true;
            this.txtKeterangan.Name = "txtKeterangan";
            this.txtKeterangan.Size = new System.Drawing.Size(699, 144);
            this.txtKeterangan.TabIndex = 9;
            // 
            // radPanel16
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel16, 2);
            this.radPanel16.Controls.Add(this.txtLokasi);
            this.radPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel16.Location = new System.Drawing.Point(393, 363);
            this.radPanel16.Name = "radPanel16";
            this.radPanel16.Size = new System.Drawing.Size(464, 44);
            this.radPanel16.TabIndex = 8;
            // 
            // txtLokasi
            // 
            this.txtLokasi.AutoSize = false;
            this.txtLokasi.BackColor = System.Drawing.Color.White;
            this.txtLokasi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLokasi.Location = new System.Drawing.Point(0, 0);
            this.txtLokasi.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtLokasi.Name = "txtLokasi";
            this.txtLokasi.Size = new System.Drawing.Size(464, 44);
            this.txtLokasi.TabIndex = 8;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtLokasi.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtLokasi.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtLokasi.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtLokasi.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radPanel20
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel20, 2);
            this.radPanel20.Controls.Add(this.dpTanggalSelesai);
            this.radPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel20.Location = new System.Drawing.Point(393, 313);
            this.radPanel20.Name = "radPanel20";
            this.radPanel20.Size = new System.Drawing.Size(464, 44);
            this.radPanel20.TabIndex = 7;
            // 
            // dpTanggalSelesai
            // 
            this.dpTanggalSelesai.AutoSize = false;
            this.dpTanggalSelesai.BackColor = System.Drawing.Color.Transparent;
            this.dpTanggalSelesai.CalendarSize = new System.Drawing.Size(290, 320);
            this.dpTanggalSelesai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dpTanggalSelesai.Location = new System.Drawing.Point(0, 0);
            this.dpTanggalSelesai.Name = "dpTanggalSelesai";
            this.dpTanggalSelesai.Size = new System.Drawing.Size(464, 44);
            this.dpTanggalSelesai.TabIndex = 7;
            this.dpTanggalSelesai.TabStop = false;
            this.dpTanggalSelesai.Text = "Friday, December 21, 2018";
            this.dpTanggalSelesai.Value = new System.DateTime(2018, 12, 21, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.dpTanggalSelesai.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dpTanggalSelesai.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dpTanggalSelesai.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpTanggalSelesai.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpTanggalSelesai.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpTanggalSelesai.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpTanggalSelesai.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dpTanggalSelesai.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "Friday, December 21, 2018";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dpTanggalSelesai.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // radPanel17
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel17, 2);
            this.radPanel17.Controls.Add(this.dpTanggalMulai);
            this.radPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel17.Location = new System.Drawing.Point(393, 263);
            this.radPanel17.Name = "radPanel17";
            this.radPanel17.Size = new System.Drawing.Size(464, 44);
            this.radPanel17.TabIndex = 6;
            // 
            // dpTanggalMulai
            // 
            this.dpTanggalMulai.AutoSize = false;
            this.dpTanggalMulai.BackColor = System.Drawing.Color.Transparent;
            this.dpTanggalMulai.CalendarSize = new System.Drawing.Size(290, 320);
            this.dpTanggalMulai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dpTanggalMulai.Location = new System.Drawing.Point(0, 0);
            this.dpTanggalMulai.Name = "dpTanggalMulai";
            this.dpTanggalMulai.Size = new System.Drawing.Size(464, 44);
            this.dpTanggalMulai.TabIndex = 6;
            this.dpTanggalMulai.TabStop = false;
            this.dpTanggalMulai.Text = "Friday, December 21, 2018";
            this.dpTanggalMulai.Value = new System.DateTime(2018, 12, 21, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.dpTanggalMulai.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dpTanggalMulai.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dpTanggalMulai.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpTanggalMulai.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpTanggalMulai.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpTanggalMulai.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpTanggalMulai.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dpTanggalMulai.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "Friday, December 21, 2018";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dpTanggalMulai.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // btnAddKeperluan
            // 
            this.btnAddKeperluan.AutoSize = true;
            this.btnAddKeperluan.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.add_doc;
            this.btnAddKeperluan.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAddKeperluan.Location = new System.Drawing.Point(863, 213);
            this.btnAddKeperluan.Name = "btnAddKeperluan";
            this.btnAddKeperluan.Size = new System.Drawing.Size(27, 27);
            this.btnAddKeperluan.TabIndex = 5;
            // 
            // radPanel18
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel18, 2);
            this.radPanel18.Controls.Add(this.ddlKeperluanBon);
            this.radPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel18.Location = new System.Drawing.Point(393, 213);
            this.radPanel18.Name = "radPanel18";
            this.radPanel18.Size = new System.Drawing.Size(464, 44);
            this.radPanel18.TabIndex = 4;
            // 
            // ddlKeperluanBon
            // 
            this.ddlKeperluanBon.AutoSize = false;
            this.ddlKeperluanBon.BackColor = System.Drawing.Color.White;
            this.ddlKeperluanBon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlKeperluanBon.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlKeperluanBon.DropDownHeight = 400;
            this.ddlKeperluanBon.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlKeperluanBon.Location = new System.Drawing.Point(0, 0);
            this.ddlKeperluanBon.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlKeperluanBon.Name = "ddlKeperluanBon";
            // 
            // 
            // 
            this.ddlKeperluanBon.RootElement.CustomFont = "Roboto";
            this.ddlKeperluanBon.RootElement.CustomFontSize = 13F;
            this.ddlKeperluanBon.Size = new System.Drawing.Size(464, 44);
            this.ddlKeperluanBon.TabIndex = 4;
            this.ddlKeperluanBon.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlKeperluanBon.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlKeperluanBon.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlKeperluanBon.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlKeperluanBon.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlKeperluanBon.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlKeperluanBon.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlKeperluanBon.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlKeperluanBon.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlKeperluanBon.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlKeperluanBon.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.radLabel5, 4);
            this.radLabel5.Controls.Add(this.label1);
            this.radLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel5.Location = new System.Drawing.Point(13, 563);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.radLabel5.Size = new System.Drawing.Size(1079, 44);
            this.radLabel5.TabIndex = 194;
            this.radLabel5.Text = "YANG MEMOHON";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(1077, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 44);
            this.label1.TabIndex = 167;
            this.label1.Visible = false;
            // 
            // radPanel21
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel21, 2);
            this.radPanel21.Controls.Add(this.txtListTahanan);
            this.radPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel21.Location = new System.Drawing.Point(393, 113);
            this.radPanel21.Name = "radPanel21";
            this.radPanel21.Size = new System.Drawing.Size(464, 94);
            this.radPanel21.TabIndex = 200;
            // 
            // txtListTahanan
            // 
            this.txtListTahanan.AutoSize = false;
            this.txtListTahanan.BackColor = System.Drawing.Color.White;
            this.txtListTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtListTahanan.Location = new System.Drawing.Point(0, 0);
            this.txtListTahanan.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtListTahanan.Multiline = true;
            this.txtListTahanan.Name = "txtListTahanan";
            this.txtListTahanan.ReadOnly = true;
            this.txtListTahanan.Size = new System.Drawing.Size(464, 94);
            this.txtListTahanan.TabIndex = 2;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtListTahanan.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtListTahanan.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtListTahanan.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtListTahanan.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSearchTahanan);
            this.panel1.Controls.Add(this.btnClearTahanan);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(863, 113);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(229, 94);
            this.panel1.TabIndex = 203;
            // 
            // btnSearchTahanan
            // 
            this.btnSearchTahanan.BackColor = System.Drawing.Color.DimGray;
            this.btnSearchTahanan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearchTahanan.ForeColor = System.Drawing.Color.White;
            this.btnSearchTahanan.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchTahanan.Image")));
            this.btnSearchTahanan.Location = new System.Drawing.Point(4, 1);
            this.btnSearchTahanan.Margin = new System.Windows.Forms.Padding(5);
            this.btnSearchTahanan.Name = "btnSearchTahanan";
            this.btnSearchTahanan.Size = new System.Drawing.Size(143, 40);
            this.btnSearchTahanan.TabIndex = 202;
            this.btnSearchTahanan.Text = "       Tambah";
            this.btnSearchTahanan.ThemeName = "MaterialBlueGrey";
            // 
            // btnClearTahanan
            // 
            this.btnClearTahanan.BackColor = System.Drawing.Color.DimGray;
            this.btnClearTahanan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClearTahanan.ForeColor = System.Drawing.Color.White;
            this.btnClearTahanan.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.delete1;
            this.btnClearTahanan.Location = new System.Drawing.Point(5, 49);
            this.btnClearTahanan.Margin = new System.Windows.Forms.Padding(5);
            this.btnClearTahanan.Name = "btnClearTahanan";
            this.btnClearTahanan.Size = new System.Drawing.Size(142, 40);
            this.btnClearTahanan.TabIndex = 201;
            this.btnClearTahanan.Text = "       Bersihkan";
            this.btnClearTahanan.ThemeName = "MaterialBlueGrey";
            // 
            // lblDetailSurat
            // 
            this.lblDetailSurat.AutoSize = false;
            this.lblDetailSurat.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailSurat.Location = new System.Drawing.Point(0, 56);
            this.lblDetailSurat.Name = "lblDetailSurat";
            this.lblDetailSurat.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetailSurat.Size = new System.Drawing.Size(1183, 52);
            this.lblDetailSurat.TabIndex = 75;
            this.lblDetailSurat.Text = "Please Fill All Required Fields.";
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.lblTitle);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Margin = new System.Windows.Forms.Padding(4);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1183, 56);
            this.headerPanel.TabIndex = 71;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(1183, 56);
            this.lblTitle.TabIndex = 71;
            // 
            // FormBonTahanan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.cameraSource);
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radPanelError);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblDetailSurat);
            this.Controls.Add(this.headerPanel);
            this.Name = "FormBonTahanan";
            this.Size = new System.Drawing.Size(1183, 736);
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).EndInit();
            this.radPanelError.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelRb)).EndInit();
            this.PanelRb.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbJaksa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbPersonel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel19)).EndInit();
            this.radPanel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNoBonTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNrpDisetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaDisetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).EndInit();
            this.radPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNrpDisetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel13)).EndInit();
            this.radPanel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlPangkatDisetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel14)).EndInit();
            this.radPanel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSatkerDisetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaDisetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDisetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoHpPJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNrpPJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaPJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            this.radPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNohpPJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNrpPJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).EndInit();
            this.radPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlPangkatPJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).EndInit();
            this.radPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSatkerPJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).EndInit();
            this.radPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaPJ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPenanggungJawab)).EndInit();
            this.lblPenanggungJawab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblNoHpPemohon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNrpPemohon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaPemohon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelNoHp)).EndInit();
            this.radPanelNoHp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNohpPemohon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel10)).EndInit();
            this.radPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNrpPemohon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelPangkatPemohon)).EndInit();
            this.radPanelPangkatPemohon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlPangkatPemohon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelSatkerPemohon)).EndInit();
            this.radPanelSatkerPemohon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSatkerPemohon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel11)).EndInit();
            this.radPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaPemohon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKeterangan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLokasi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKeperluan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel15)).EndInit();
            this.radPanel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtKeterangan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel16)).EndInit();
            this.radPanel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtLokasi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel20)).EndInit();
            this.radPanel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dpTanggalSelesai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel17)).EndInit();
            this.radPanel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dpTanggalMulai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddKeperluan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel18)).EndInit();
            this.radPanel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlKeperluanBon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            this.radLabel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel21)).EndInit();
            this.radPanel21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtListTahanan)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnSearchTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClearTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).EndInit();
            this.headerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList imageList;
        private Telerik.WinControls.UI.RadPanel headerPanel;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel lblDetailSurat;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel lblKeterangan;
        private Telerik.WinControls.UI.RadLabel lblKeperluan;
        private Telerik.WinControls.UI.RadLabel lblNamaTahanan;
        private Telerik.WinControls.UI.RadLabel lblNamaPemohon;
        private Telerik.WinControls.UI.RadLabel lblDetailVisitor;
        private Telerik.WinControls.UI.RadLabel lblNamaPJ;
        private Telerik.WinControls.UI.RadLabel lblNrpPJ;
        private Telerik.WinControls.UI.RadLabel lblNoHpPJ;
        private Telerik.WinControls.UI.RadLabel lblDisetujui;
        private Telerik.WinControls.UI.RadTextBox txtNohpPJ;
        private Telerik.WinControls.UI.RadPanel radPanelError;
        private Telerik.WinControls.UI.RadLabel lblError;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnBack;
        private Telerik.WinControls.UI.RadButton btnSave;
        private CameraSource cameraSource;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadPanel radPanel6;
        private Telerik.WinControls.UI.RadPanel radPanel5;
        private Telerik.WinControls.UI.RadLabel lblNoHpPemohon;
        private Telerik.WinControls.UI.RadLabel lblNrpPemohon;
        private Telerik.WinControls.UI.RadPanel radPanel15;
        private Telerik.WinControls.UI.RadLabel lblLokasi;
        private Telerik.WinControls.UI.RadPanel radPanel16;
        private Telerik.WinControls.UI.RadTextBox txtLokasi;
        private Telerik.WinControls.UI.RadPanel radPanel18;
        private Telerik.WinControls.UI.RadPanel radPanel19;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadPanel radPanelPangkatPemohon;
        private Telerik.WinControls.UI.RadPanel radPanel11;
        private Telerik.WinControls.UI.RadPanel radPanel10;
        private Telerik.WinControls.UI.RadPanel radPanelNoHp;
        private Telerik.WinControls.UI.RadLabel lblPenanggungJawab;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadPanel radPanelSatkerPemohon;
        private Telerik.WinControls.UI.RadTextBox txtKeterangan;
        private Telerik.WinControls.UI.RadPanel radPanel7;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadLabel lblNrpDisetujui;
        private Telerik.WinControls.UI.RadLabel lblNamaDisetujui;
        private Telerik.WinControls.UI.RadPanel radPanel14;
        private Telerik.WinControls.UI.RadPanel radPanel13;
        private Telerik.WinControls.UI.RadPanel radPanel8;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadTextBox txtSatkerDisetujui;
        private Telerik.WinControls.UI.RadTextBox txtNamaPemohon;
        private Telerik.WinControls.UI.RadTextBox txtNrpPemohon;
        private Telerik.WinControls.UI.RadTextBox txtSatkerPemohon;
        private Telerik.WinControls.UI.RadTextBox txtNohpPemohon;
        private Telerik.WinControls.UI.RadTextBox txtNamaPJ;
        private Telerik.WinControls.UI.RadTextBox txtNrpPJ;
        private Telerik.WinControls.UI.RadTextBox txtSatkerPJ;
        private Telerik.WinControls.UI.RadTextBox txtNamaDisetujui;
        private Telerik.WinControls.UI.RadTextBox txtNrpDisetujui;
        private Telerik.WinControls.UI.RadButton btnAddKeperluan;
        private Telerik.WinControls.UI.RadDropDownList ddlKeperluanBon;
        private Telerik.WinControls.UI.RadDropDownList ddlPangkatDisetujui;
        private Telerik.WinControls.UI.RadDropDownList ddlPangkatPJ;
        private Telerik.WinControls.UI.RadDropDownList ddlPangkatPemohon;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadPanel radPanel20;
        private Telerik.WinControls.UI.RadPanel radPanel17;
        private Telerik.WinControls.UI.RadDateTimePicker dpTanggalMulai;
        private Telerik.WinControls.UI.RadDateTimePicker dpTanggalSelesai;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox txtNoBonTahanan;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadPanel PanelRb;
        private Telerik.WinControls.UI.RadRadioButton rbJaksa;
        private Telerik.WinControls.UI.RadRadioButton rbPersonel;
        private Telerik.WinControls.UI.RadPanel radPanel21;
        private Telerik.WinControls.UI.RadButton btnClearTahanan;
        private Telerik.WinControls.UI.RadTextBox txtListTahanan;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadButton btnSearchTahanan;
    }
}
