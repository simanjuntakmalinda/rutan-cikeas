﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class FormVisitorShort
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormVisitorShort));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnBack = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            this.radPanelError = new Telerik.WinControls.UI.RadPanel();
            this.lblError = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.lblRelation = new Telerik.WinControls.UI.RadLabel();
            this.radPanel19 = new Telerik.WinControls.UI.RadPanel();
            this.ddlJnsIdentitas = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel18 = new Telerik.WinControls.UI.RadPanel();
            this.txtNama = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel16 = new Telerik.WinControls.UI.RadPanel();
            this.txtPlaceofBirth = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel15 = new Telerik.WinControls.UI.RadPanel();
            this.txtAddress = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel14 = new Telerik.WinControls.UI.RadPanel();
            this.txtCity = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel12 = new Telerik.WinControls.UI.RadPanel();
            this.ddlGender = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel11 = new Telerik.WinControls.UI.RadPanel();
            this.ddlAgama = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel10 = new Telerik.WinControls.UI.RadPanel();
            this.ddlMaritalStatus = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.lblVocation = new Telerik.WinControls.UI.RadLabel();
            this.radPanel8 = new Telerik.WinControls.UI.RadPanel();
            this.ddlNationality = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel7 = new Telerik.WinControls.UI.RadPanel();
            this.txtTelp = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel5 = new Telerik.WinControls.UI.RadPanel();
            this.ddlVisitorType = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.txtInmatesName = new Telerik.WinControls.UI.RadTextBox();
            this.lblVisitorPhoto = new Telerik.WinControls.UI.RadLabel();
            this.lblFullName = new Telerik.WinControls.UI.RadLabel();
            this.lblIDType = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailVisitor = new Telerik.WinControls.UI.RadLabel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.picCanvas = new System.Windows.Forms.PictureBox();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.txtBringgingItems = new Telerik.WinControls.UI.RadTextBox();
            this.lblBringging = new Telerik.WinControls.UI.RadLabel();
            this.btnSearchTahanan = new Telerik.WinControls.UI.RadButton();
            this.lblInmateNama = new Telerik.WinControls.UI.RadLabel();
            this.lblType = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailVisit = new Telerik.WinControls.UI.RadLabel();
            this.tahananId = new System.Windows.Forms.Label();
            this.lblPhone = new Telerik.WinControls.UI.RadLabel();
            this.lblNationality = new Telerik.WinControls.UI.RadLabel();
            this.radPanel9 = new Telerik.WinControls.UI.RadPanel();
            this.ddlVocation = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.lblCity = new Telerik.WinControls.UI.RadLabel();
            this.lblAddress = new Telerik.WinControls.UI.RadLabel();
            this.radPanel21 = new Telerik.WinControls.UI.RadPanel();
            this.dpDateofBirth = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radPanel17 = new Telerik.WinControls.UI.RadPanel();
            this.txtNoIdentitas = new Telerik.WinControls.UI.RadTextBox();
            this.btnTakePhoto = new System.Windows.Forms.Button();
            this.lblVisitorFinger = new Telerik.WinControls.UI.RadLabel();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.btnUbahSidikJari = new System.Windows.Forms.Button();
            this.picFinger = new System.Windows.Forms.PictureBox();
            this.txtRelation = new Telerik.WinControls.UI.RadPanel();
            this.btnUploadPhoto = new System.Windows.Forms.Button();
            this.radPanel6 = new Telerik.WinControls.UI.RadPanel();
            this.datetimeStartVisit = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblDetailSurat = new Telerik.WinControls.UI.RadLabel();
            this.headerPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.cameraSource = new RDM.LP.Desktop.Presentation.CustomControls.CameraSource();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).BeginInit();
            this.radPanelError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRelation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel19)).BeginInit();
            this.radPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlJnsIdentitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel18)).BeginInit();
            this.radPanel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel16)).BeginInit();
            this.radPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlaceofBirth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel15)).BeginInit();
            this.radPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel14)).BeginInit();
            this.radPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel12)).BeginInit();
            this.radPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlGender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel11)).BeginInit();
            this.radPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlAgama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel10)).BeginInit();
            this.radPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlMaritalStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).BeginInit();
            this.radPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlNationality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).BeginInit();
            this.radPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).BeginInit();
            this.radPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlVisitorType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            this.radPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInmatesName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVisitorPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFullName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIDType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBringgingItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBringging)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearchTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInmateNama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisit)).BeginInit();
            this.lblDetailVisit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNationality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel9)).BeginInit();
            this.radPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlVocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel21)).BeginInit();
            this.radPanel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dpDateofBirth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel17)).BeginInit();
            this.radPanel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoIdentitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVisitorFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRelation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).BeginInit();
            this.radPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datetimeStartVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).BeginInit();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnBack);
            this.lblButton.Controls.Add(this.btnSave);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 642);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(20);
            this.lblButton.Size = new System.Drawing.Size(1516, 94);
            this.lblButton.TabIndex = 81;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.btnBack_Image;
            this.btnBack.Location = new System.Drawing.Point(20, 20);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(244, 54);
            this.btnBack.TabIndex = 20;
            this.btnBack.Tag = "";
            this.btnBack.Text = "&BATAL";
            this.btnBack.ThemeName = "MaterialBlueGrey";
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.if_checked_2_309094;
            this.btnSave.Location = new System.Drawing.Point(1191, 20);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(305, 54);
            this.btnSave.TabIndex = 19;
            this.btnSave.Text = "&SIMPAN";
            this.btnSave.ThemeName = "MaterialBlueGrey";
            // 
            // radPanelError
            // 
            this.radPanelError.Controls.Add(this.lblError);
            this.radPanelError.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanelError.Location = new System.Drawing.Point(0, 598);
            this.radPanelError.Margin = new System.Windows.Forms.Padding(0);
            this.radPanelError.Name = "radPanelError";
            this.radPanelError.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radPanelError.Size = new System.Drawing.Size(1516, 43);
            this.radPanelError.TabIndex = 80;
            // 
            // lblError
            // 
            this.lblError.AutoSize = false;
            this.lblError.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.warning;
            this.lblError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblError.Location = new System.Drawing.Point(15, 0);
            this.lblError.Name = "lblError";
            this.lblError.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.lblError.Size = new System.Drawing.Size(1501, 39);
            this.lblError.TabIndex = 24;
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("radScrollablePanel1.BackgroundImage")));
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 108);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(1497, 488);
            this.radScrollablePanel1.Size = new System.Drawing.Size(1516, 490);
            this.radScrollablePanel1.TabIndex = 78;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 242F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 258F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 165F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel5, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.lblRelation, 2, 13);
            this.tableLayoutPanel1.Controls.Add(this.radPanel19, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel18, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.radLabel4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel16, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel15, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.radPanel14, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.radPanel12, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.radPanel11, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.radPanel10, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.radLabel3, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblVocation, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.radPanel8, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.radPanel7, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.radPanel5, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.radPanel4, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.lblVisitorPhoto, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblFullName, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblIDType, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailVisitor, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 1, 15);
            this.tableLayoutPanel1.Controls.Add(this.lblBringging, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.btnSearchTahanan, 3, 14);
            this.tableLayoutPanel1.Controls.Add(this.lblInmateNama, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.lblType, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailVisit, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.lblPhone, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.lblNationality, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.radPanel9, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.radLabel2, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblCity, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblAddress, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.radPanel21, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel17, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnTakePhoto, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblVisitorFinger, 4, 8);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 4, 9);
            this.tableLayoutPanel1.Controls.Add(this.txtRelation, 3, 13);
            this.tableLayoutPanel1.Controls.Add(this.btnUploadPhoto, 5, 6);
            this.tableLayoutPanel1.Controls.Add(this.radPanel6, 1, 16);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 18;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1497, 1070);
            this.tableLayoutPanel1.TabIndex = 161;
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel5.ForeColor = System.Drawing.Color.Black;
            this.radLabel5.Location = new System.Drawing.Point(14, 964);
            this.radLabel5.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(242, 42);
            this.radLabel5.TabIndex = 137;
            this.radLabel5.Text = "<html>Tanggal Kunjungan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblRelation
            // 
            this.lblRelation.AutoSize = false;
            this.lblRelation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRelation.ForeColor = System.Drawing.Color.Black;
            this.lblRelation.Location = new System.Drawing.Point(646, 764);
            this.lblRelation.Margin = new System.Windows.Forms.Padding(4);
            this.lblRelation.Name = "lblRelation";
            this.lblRelation.Size = new System.Drawing.Size(234, 42);
            this.lblRelation.TabIndex = 138;
            this.lblRelation.Text = "<html>Hubungan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel19
            // 
            this.radPanel19.Controls.Add(this.ddlJnsIdentitas);
            this.radPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel19.Location = new System.Drawing.Point(263, 63);
            this.radPanel19.Name = "radPanel19";
            this.radPanel19.Size = new System.Drawing.Size(376, 44);
            this.radPanel19.TabIndex = 1;
            // 
            // ddlJnsIdentitas
            // 
            this.ddlJnsIdentitas.AutoSize = false;
            this.ddlJnsIdentitas.BackColor = System.Drawing.Color.White;
            this.ddlJnsIdentitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlJnsIdentitas.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlJnsIdentitas.DropDownHeight = 400;
            this.ddlJnsIdentitas.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlJnsIdentitas.Location = new System.Drawing.Point(0, 0);
            this.ddlJnsIdentitas.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlJnsIdentitas.Name = "ddlJnsIdentitas";
            // 
            // 
            // 
            this.ddlJnsIdentitas.RootElement.CustomFont = "Roboto";
            this.ddlJnsIdentitas.RootElement.CustomFontSize = 13F;
            this.ddlJnsIdentitas.Size = new System.Drawing.Size(376, 44);
            this.ddlJnsIdentitas.TabIndex = 100;
            this.ddlJnsIdentitas.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlJnsIdentitas.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlJnsIdentitas.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlJnsIdentitas.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlJnsIdentitas.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel18
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel18, 3);
            this.radPanel18.Controls.Add(this.txtNama);
            this.radPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel18.Location = new System.Drawing.Point(263, 113);
            this.radPanel18.Name = "radPanel18";
            this.radPanel18.Size = new System.Drawing.Size(876, 44);
            this.radPanel18.TabIndex = 3;
            // 
            // txtNama
            // 
            this.txtNama.AutoSize = false;
            this.txtNama.BackColor = System.Drawing.Color.White;
            this.txtNama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNama.Location = new System.Drawing.Point(0, 0);
            this.txtNama.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNama.Name = "txtNama";
            this.txtNama.Size = new System.Drawing.Size(876, 44);
            this.txtNama.TabIndex = 102;
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel4.ForeColor = System.Drawing.Color.Black;
            this.radLabel4.Location = new System.Drawing.Point(14, 164);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(242, 42);
            this.radLabel4.TabIndex = 331;
            this.radLabel4.Text = "<html>Tempat / Tanggal Lahir<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel16
            // 
            this.radPanel16.Controls.Add(this.txtPlaceofBirth);
            this.radPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel16.Location = new System.Drawing.Point(263, 163);
            this.radPanel16.Name = "radPanel16";
            this.radPanel16.Size = new System.Drawing.Size(376, 44);
            this.radPanel16.TabIndex = 4;
            // 
            // txtPlaceofBirth
            // 
            this.txtPlaceofBirth.AutoSize = false;
            this.txtPlaceofBirth.BackColor = System.Drawing.Color.White;
            this.txtPlaceofBirth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPlaceofBirth.Location = new System.Drawing.Point(0, 0);
            this.txtPlaceofBirth.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtPlaceofBirth.MaxLength = 16;
            this.txtPlaceofBirth.Name = "txtPlaceofBirth";
            this.txtPlaceofBirth.Size = new System.Drawing.Size(376, 44);
            this.txtPlaceofBirth.TabIndex = 103;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtPlaceofBirth.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtPlaceofBirth.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtPlaceofBirth.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtPlaceofBirth.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radPanel15
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel15, 3);
            this.radPanel15.Controls.Add(this.txtAddress);
            this.radPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel15.Location = new System.Drawing.Point(263, 213);
            this.radPanel15.Name = "radPanel15";
            this.radPanel15.Size = new System.Drawing.Size(876, 144);
            this.radPanel15.TabIndex = 6;
            // 
            // txtAddress
            // 
            this.txtAddress.AutoSize = false;
            this.txtAddress.BackColor = System.Drawing.Color.White;
            this.txtAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAddress.Location = new System.Drawing.Point(0, 0);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(876, 144);
            this.txtAddress.TabIndex = 105;
            // 
            // radPanel14
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel14, 3);
            this.radPanel14.Controls.Add(this.txtCity);
            this.radPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel14.Location = new System.Drawing.Point(263, 363);
            this.radPanel14.Name = "radPanel14";
            this.radPanel14.Size = new System.Drawing.Size(876, 44);
            this.radPanel14.TabIndex = 7;
            // 
            // txtCity
            // 
            this.txtCity.AutoSize = false;
            this.txtCity.BackColor = System.Drawing.Color.White;
            this.txtCity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCity.Location = new System.Drawing.Point(0, 0);
            this.txtCity.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtCity.Name = "txtCity";
            this.txtCity.Padding = new System.Windows.Forms.Padding(5);
            this.txtCity.Size = new System.Drawing.Size(876, 44);
            this.txtCity.TabIndex = 106;
            // 
            // radPanel12
            // 
            this.radPanel12.Controls.Add(this.ddlGender);
            this.radPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel12.Location = new System.Drawing.Point(887, 413);
            this.radPanel12.Name = "radPanel12";
            this.radPanel12.Size = new System.Drawing.Size(252, 44);
            this.radPanel12.TabIndex = 9;
            // 
            // ddlGender
            // 
            this.ddlGender.AutoSize = false;
            this.ddlGender.BackColor = System.Drawing.Color.White;
            this.ddlGender.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlGender.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlGender.DropDownHeight = 400;
            this.ddlGender.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlGender.Location = new System.Drawing.Point(0, 0);
            this.ddlGender.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlGender.Name = "ddlGender";
            // 
            // 
            // 
            this.ddlGender.RootElement.CustomFont = "Roboto";
            this.ddlGender.RootElement.CustomFontSize = 13F;
            this.ddlGender.Size = new System.Drawing.Size(252, 44);
            this.ddlGender.TabIndex = 108;
            this.ddlGender.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlGender.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlGender.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlGender.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlGender.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlGender.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlGender.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlGender.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlGender.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlGender.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlGender.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel11
            // 
            this.radPanel11.Controls.Add(this.ddlAgama);
            this.radPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel11.Location = new System.Drawing.Point(263, 413);
            this.radPanel11.Name = "radPanel11";
            this.radPanel11.Size = new System.Drawing.Size(376, 44);
            this.radPanel11.TabIndex = 8;
            // 
            // ddlAgama
            // 
            this.ddlAgama.AutoSize = false;
            this.ddlAgama.BackColor = System.Drawing.Color.White;
            this.ddlAgama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlAgama.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlAgama.DropDownHeight = 400;
            this.ddlAgama.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlAgama.Location = new System.Drawing.Point(0, 0);
            this.ddlAgama.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlAgama.Name = "ddlAgama";
            // 
            // 
            // 
            this.ddlAgama.RootElement.CustomFont = "Roboto";
            this.ddlAgama.RootElement.CustomFontSize = 13F;
            this.ddlAgama.Size = new System.Drawing.Size(376, 44);
            this.ddlAgama.TabIndex = 107;
            this.ddlAgama.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlAgama.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlAgama.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlAgama.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlAgama.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlAgama.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlAgama.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlAgama.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlAgama.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlAgama.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlAgama.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel10
            // 
            this.radPanel10.Controls.Add(this.ddlMaritalStatus);
            this.radPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel10.Location = new System.Drawing.Point(263, 463);
            this.radPanel10.Name = "radPanel10";
            this.radPanel10.Size = new System.Drawing.Size(376, 44);
            this.radPanel10.TabIndex = 10;
            // 
            // ddlMaritalStatus
            // 
            this.ddlMaritalStatus.AutoSize = false;
            this.ddlMaritalStatus.BackColor = System.Drawing.Color.White;
            this.ddlMaritalStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlMaritalStatus.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlMaritalStatus.DropDownHeight = 400;
            this.ddlMaritalStatus.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlMaritalStatus.Location = new System.Drawing.Point(0, 0);
            this.ddlMaritalStatus.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlMaritalStatus.Name = "ddlMaritalStatus";
            // 
            // 
            // 
            this.ddlMaritalStatus.RootElement.CustomFont = "Roboto";
            this.ddlMaritalStatus.RootElement.CustomFontSize = 13F;
            this.ddlMaritalStatus.Size = new System.Drawing.Size(376, 44);
            this.ddlMaritalStatus.TabIndex = 110;
            this.ddlMaritalStatus.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlMaritalStatus.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlMaritalStatus.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlMaritalStatus.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlMaritalStatus.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlMaritalStatus.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlMaritalStatus.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlMaritalStatus.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlMaritalStatus.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlMaritalStatus.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlMaritalStatus.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel3.ForeColor = System.Drawing.Color.Black;
            this.radLabel3.Location = new System.Drawing.Point(14, 464);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(242, 42);
            this.radLabel3.TabIndex = 131;
            this.radLabel3.Text = "<html>Status Pernikahan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblVocation
            // 
            this.lblVocation.AutoSize = false;
            this.lblVocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblVocation.ForeColor = System.Drawing.Color.Black;
            this.lblVocation.Location = new System.Drawing.Point(14, 514);
            this.lblVocation.Margin = new System.Windows.Forms.Padding(4);
            this.lblVocation.Name = "lblVocation";
            this.lblVocation.Size = new System.Drawing.Size(242, 42);
            this.lblVocation.TabIndex = 131;
            this.lblVocation.Text = "<html>Pekerjaan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel8
            // 
            this.radPanel8.Controls.Add(this.ddlNationality);
            this.radPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel8.Location = new System.Drawing.Point(263, 563);
            this.radPanel8.Name = "radPanel8";
            this.radPanel8.Size = new System.Drawing.Size(376, 44);
            this.radPanel8.TabIndex = 12;
            // 
            // ddlNationality
            // 
            this.ddlNationality.AutoSize = false;
            this.ddlNationality.BackColor = System.Drawing.Color.White;
            this.ddlNationality.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlNationality.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlNationality.DropDownHeight = 400;
            this.ddlNationality.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlNationality.Location = new System.Drawing.Point(0, 0);
            this.ddlNationality.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlNationality.Name = "ddlNationality";
            // 
            // 
            // 
            this.ddlNationality.RootElement.CustomFont = "Roboto";
            this.ddlNationality.RootElement.CustomFontSize = 13F;
            this.ddlNationality.Size = new System.Drawing.Size(376, 44);
            this.ddlNationality.TabIndex = 112;
            this.ddlNationality.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlNationality.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlNationality.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlNationality.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlNationality.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlNationality.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlNationality.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlNationality.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlNationality.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlNationality.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlNationality.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel7
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel7, 3);
            this.radPanel7.Controls.Add(this.txtTelp);
            this.radPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel7.Location = new System.Drawing.Point(263, 613);
            this.radPanel7.Name = "radPanel7";
            this.radPanel7.Size = new System.Drawing.Size(876, 44);
            this.radPanel7.TabIndex = 13;
            // 
            // txtTelp
            // 
            this.txtTelp.AutoSize = false;
            this.txtTelp.BackColor = System.Drawing.Color.White;
            this.txtTelp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTelp.Location = new System.Drawing.Point(0, 0);
            this.txtTelp.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtTelp.MaxLength = 12;
            this.txtTelp.Name = "txtTelp";
            this.txtTelp.Padding = new System.Windows.Forms.Padding(5);
            this.txtTelp.Size = new System.Drawing.Size(876, 44);
            this.txtTelp.TabIndex = 113;
            // 
            // radPanel5
            // 
            this.radPanel5.Controls.Add(this.ddlVisitorType);
            this.radPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel5.Location = new System.Drawing.Point(263, 763);
            this.radPanel5.Name = "radPanel5";
            this.radPanel5.Size = new System.Drawing.Size(376, 44);
            this.radPanel5.TabIndex = 14;
            // 
            // ddlVisitorType
            // 
            this.ddlVisitorType.AutoSize = false;
            this.ddlVisitorType.BackColor = System.Drawing.Color.White;
            this.ddlVisitorType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlVisitorType.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlVisitorType.DropDownHeight = 400;
            this.ddlVisitorType.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlVisitorType.Location = new System.Drawing.Point(0, 0);
            this.ddlVisitorType.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlVisitorType.Name = "ddlVisitorType";
            // 
            // 
            // 
            this.ddlVisitorType.RootElement.CustomFont = "Roboto";
            this.ddlVisitorType.RootElement.CustomFontSize = 13F;
            this.ddlVisitorType.Size = new System.Drawing.Size(376, 44);
            this.ddlVisitorType.TabIndex = 114;
            this.ddlVisitorType.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlVisitorType.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlVisitorType.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlVisitorType.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlVisitorType.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlVisitorType.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlVisitorType.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlVisitorType.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlVisitorType.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlVisitorType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlVisitorType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel4, 2);
            this.radPanel4.Controls.Add(this.txtInmatesName);
            this.radPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel4.Location = new System.Drawing.Point(263, 813);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(618, 44);
            this.radPanel4.TabIndex = 16;
            // 
            // txtInmatesName
            // 
            this.txtInmatesName.AutoSize = false;
            this.txtInmatesName.BackColor = System.Drawing.Color.White;
            this.txtInmatesName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtInmatesName.Enabled = false;
            this.txtInmatesName.Location = new System.Drawing.Point(0, 0);
            this.txtInmatesName.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtInmatesName.Name = "txtInmatesName";
            this.txtInmatesName.Size = new System.Drawing.Size(618, 44);
            this.txtInmatesName.TabIndex = 117;
            // 
            // lblVisitorPhoto
            // 
            this.lblVisitorPhoto.AutoSize = false;
            this.lblVisitorPhoto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.tableLayoutPanel1.SetColumnSpan(this.lblVisitorPhoto, 2);
            this.lblVisitorPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblVisitorPhoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.lblVisitorPhoto.ForeColor = System.Drawing.Color.White;
            this.lblVisitorPhoto.Image = ((System.Drawing.Image)(resources.GetObject("lblVisitorPhoto.Image")));
            this.lblVisitorPhoto.Location = new System.Drawing.Point(1142, 10);
            this.lblVisitorPhoto.Margin = new System.Windows.Forms.Padding(0);
            this.lblVisitorPhoto.Name = "lblVisitorPhoto";
            this.lblVisitorPhoto.Padding = new System.Windows.Forms.Padding(8);
            this.lblVisitorPhoto.Size = new System.Drawing.Size(345, 50);
            this.lblVisitorPhoto.TabIndex = 180;
            this.lblVisitorPhoto.Text = "         FOTO PENGUNJUNG";
            // 
            // lblFullName
            // 
            this.lblFullName.AutoSize = false;
            this.lblFullName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFullName.ForeColor = System.Drawing.Color.Black;
            this.lblFullName.Location = new System.Drawing.Point(14, 114);
            this.lblFullName.Margin = new System.Windows.Forms.Padding(4);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.Size = new System.Drawing.Size(242, 42);
            this.lblFullName.TabIndex = 181;
            this.lblFullName.Text = "<html>Nama Lengkap<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblIDType
            // 
            this.lblIDType.AutoSize = false;
            this.lblIDType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIDType.ForeColor = System.Drawing.Color.Black;
            this.lblIDType.Location = new System.Drawing.Point(14, 64);
            this.lblIDType.Margin = new System.Windows.Forms.Padding(4);
            this.lblIDType.Name = "lblIDType";
            this.lblIDType.Size = new System.Drawing.Size(242, 42);
            this.lblIDType.TabIndex = 191;
            this.lblIDType.Text = "<html>Jenis Identitas / No<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblDetailVisitor
            // 
            this.lblDetailVisitor.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailVisitor, 4);
            this.lblDetailVisitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailVisitor.Location = new System.Drawing.Point(13, 13);
            this.lblDetailVisitor.Name = "lblDetailVisitor";
            this.lblDetailVisitor.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDetailVisitor.Size = new System.Drawing.Size(1126, 44);
            this.lblDetailVisitor.TabIndex = 134;
            this.lblDetailVisitor.Text = "DETAIL PEGUNJUNG";
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel1, 2);
            this.radPanel1.Controls.Add(this.picCanvas);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(1142, 60);
            this.radPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.tableLayoutPanel1.SetRowSpan(this.radPanel1, 5);
            this.radPanel1.Size = new System.Drawing.Size(345, 350);
            this.radPanel1.TabIndex = 144;
            // 
            // picCanvas
            // 
            this.picCanvas.BackColor = System.Drawing.Color.Silver;
            this.picCanvas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picCanvas.Location = new System.Drawing.Point(5, 0);
            this.picCanvas.Margin = new System.Windows.Forms.Padding(8);
            this.picCanvas.Name = "picCanvas";
            this.picCanvas.Size = new System.Drawing.Size(335, 345);
            this.picCanvas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCanvas.TabIndex = 129;
            this.picCanvas.TabStop = false;
            // 
            // radPanel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel3, 3);
            this.radPanel3.Controls.Add(this.txtBringgingItems);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Location = new System.Drawing.Point(263, 863);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(876, 94);
            this.radPanel3.TabIndex = 17;
            // 
            // txtBringgingItems
            // 
            this.txtBringgingItems.AutoSize = false;
            this.txtBringgingItems.BackColor = System.Drawing.Color.White;
            this.txtBringgingItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBringgingItems.Location = new System.Drawing.Point(0, 0);
            this.txtBringgingItems.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtBringgingItems.Multiline = true;
            this.txtBringgingItems.Name = "txtBringgingItems";
            this.txtBringgingItems.Padding = new System.Windows.Forms.Padding(5);
            this.txtBringgingItems.Size = new System.Drawing.Size(876, 94);
            this.txtBringgingItems.TabIndex = 118;
            // 
            // lblBringging
            // 
            this.lblBringging.AutoSize = false;
            this.lblBringging.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblBringging.ForeColor = System.Drawing.Color.Black;
            this.lblBringging.Location = new System.Drawing.Point(14, 864);
            this.lblBringging.Margin = new System.Windows.Forms.Padding(4);
            this.lblBringging.Name = "lblBringging";
            this.lblBringging.Size = new System.Drawing.Size(242, 92);
            this.lblBringging.TabIndex = 139;
            this.lblBringging.Text = "<html>Catatan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // btnSearchTahanan
            // 
            this.btnSearchTahanan.BackColor = System.Drawing.Color.DimGray;
            this.btnSearchTahanan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearchTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSearchTahanan.ForeColor = System.Drawing.Color.White;
            this.btnSearchTahanan.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchTahanan.Image")));
            this.btnSearchTahanan.Location = new System.Drawing.Point(889, 815);
            this.btnSearchTahanan.Margin = new System.Windows.Forms.Padding(5);
            this.btnSearchTahanan.Name = "btnSearchTahanan";
            this.btnSearchTahanan.Size = new System.Drawing.Size(248, 40);
            this.btnSearchTahanan.TabIndex = 116;
            this.btnSearchTahanan.Text = "       Cari";
            this.btnSearchTahanan.ThemeName = "MaterialBlueGrey";
            // 
            // lblInmateNama
            // 
            this.lblInmateNama.AutoSize = false;
            this.lblInmateNama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInmateNama.ForeColor = System.Drawing.Color.Black;
            this.lblInmateNama.Location = new System.Drawing.Point(14, 814);
            this.lblInmateNama.Margin = new System.Windows.Forms.Padding(4);
            this.lblInmateNama.Name = "lblInmateNama";
            this.lblInmateNama.Size = new System.Drawing.Size(242, 42);
            this.lblInmateNama.TabIndex = 136;
            this.lblInmateNama.Text = "<html>Nama Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblType
            // 
            this.lblType.AutoSize = false;
            this.lblType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblType.ForeColor = System.Drawing.Color.Black;
            this.lblType.Location = new System.Drawing.Point(14, 764);
            this.lblType.Margin = new System.Windows.Forms.Padding(4);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(242, 42);
            this.lblType.TabIndex = 137;
            this.lblType.Text = "<html>Tipe Pengunjung <span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblDetailVisit
            // 
            this.lblDetailVisit.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailVisit, 4);
            this.lblDetailVisit.Controls.Add(this.tahananId);
            this.lblDetailVisit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailVisit.Location = new System.Drawing.Point(13, 713);
            this.lblDetailVisit.Name = "lblDetailVisit";
            this.lblDetailVisit.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDetailVisit.Size = new System.Drawing.Size(1126, 44);
            this.lblDetailVisit.TabIndex = 135;
            this.lblDetailVisit.Text = "DETAIL KUNJUNGAN";
            // 
            // tahananId
            // 
            this.tahananId.Location = new System.Drawing.Point(1077, 0);
            this.tahananId.Name = "tahananId";
            this.tahananId.Size = new System.Drawing.Size(52, 44);
            this.tahananId.TabIndex = 167;
            this.tahananId.Visible = false;
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = false;
            this.lblPhone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPhone.ForeColor = System.Drawing.Color.Black;
            this.lblPhone.Location = new System.Drawing.Point(14, 614);
            this.lblPhone.Margin = new System.Windows.Forms.Padding(4);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(242, 42);
            this.lblPhone.TabIndex = 111;
            this.lblPhone.Text = "<html>Nomor Telepon<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblNationality
            // 
            this.lblNationality.AutoSize = false;
            this.lblNationality.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNationality.ForeColor = System.Drawing.Color.Black;
            this.lblNationality.Location = new System.Drawing.Point(14, 564);
            this.lblNationality.Margin = new System.Windows.Forms.Padding(4);
            this.lblNationality.Name = "lblNationality";
            this.lblNationality.Size = new System.Drawing.Size(242, 42);
            this.lblNationality.TabIndex = 131;
            this.lblNationality.Text = "<html>Kewarganegaraan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel9
            // 
            this.radPanel9.Controls.Add(this.ddlVocation);
            this.radPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel9.Location = new System.Drawing.Point(263, 513);
            this.radPanel9.Name = "radPanel9";
            this.radPanel9.Size = new System.Drawing.Size(376, 44);
            this.radPanel9.TabIndex = 11;
            // 
            // ddlVocation
            // 
            this.ddlVocation.AutoSize = false;
            this.ddlVocation.BackColor = System.Drawing.Color.White;
            this.ddlVocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlVocation.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlVocation.DropDownHeight = 400;
            this.ddlVocation.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlVocation.Location = new System.Drawing.Point(0, 0);
            this.ddlVocation.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlVocation.Name = "ddlVocation";
            this.ddlVocation.Size = new System.Drawing.Size(376, 44);
            this.ddlVocation.TabIndex = 111;
            this.ddlVocation.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlVocation.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlVocation.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlVocation.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlVocation.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlVocation.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlVocation.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlVocation.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlVocation.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlVocation.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlVocation.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel2.ForeColor = System.Drawing.Color.Black;
            this.radLabel2.Location = new System.Drawing.Point(14, 414);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(242, 42);
            this.radLabel2.TabIndex = 130;
            this.radLabel2.Text = "<html>Agama<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.ForeColor = System.Drawing.Color.Black;
            this.radLabel1.Location = new System.Drawing.Point(646, 414);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radLabel1.Size = new System.Drawing.Size(234, 42);
            this.radLabel1.TabIndex = 132;
            this.radLabel1.Text = "<html>Jenis Kelamin<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = false;
            this.lblCity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCity.ForeColor = System.Drawing.Color.Black;
            this.lblCity.Location = new System.Drawing.Point(14, 364);
            this.lblCity.Margin = new System.Windows.Forms.Padding(4);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(242, 42);
            this.lblCity.TabIndex = 110;
            this.lblCity.Text = "<html>Kota<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = false;
            this.lblAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAddress.ForeColor = System.Drawing.Color.Black;
            this.lblAddress.Location = new System.Drawing.Point(14, 214);
            this.lblAddress.Margin = new System.Windows.Forms.Padding(4);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(242, 142);
            this.lblAddress.TabIndex = 321;
            this.lblAddress.Text = "<html>Alamat<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel21
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel21, 2);
            this.radPanel21.Controls.Add(this.dpDateofBirth);
            this.radPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel21.Location = new System.Drawing.Point(645, 163);
            this.radPanel21.Name = "radPanel21";
            this.radPanel21.Size = new System.Drawing.Size(494, 44);
            this.radPanel21.TabIndex = 5;
            // 
            // dpDateofBirth
            // 
            this.dpDateofBirth.AutoSize = false;
            this.dpDateofBirth.BackColor = System.Drawing.Color.Transparent;
            this.dpDateofBirth.CalendarSize = new System.Drawing.Size(290, 320);
            this.dpDateofBirth.Culture = new System.Globalization.CultureInfo("id-ID");
            this.dpDateofBirth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dpDateofBirth.Location = new System.Drawing.Point(0, 0);
            this.dpDateofBirth.Margin = new System.Windows.Forms.Padding(0);
            this.dpDateofBirth.Name = "dpDateofBirth";
            this.dpDateofBirth.Size = new System.Drawing.Size(494, 44);
            this.dpDateofBirth.TabIndex = 104;
            this.dpDateofBirth.TabStop = false;
            this.dpDateofBirth.Text = "Jumat, 21 Desember 2018";
            this.dpDateofBirth.Value = new System.DateTime(2018, 12, 21, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.dpDateofBirth.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dpDateofBirth.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dpDateofBirth.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpDateofBirth.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpDateofBirth.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpDateofBirth.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpDateofBirth.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dpDateofBirth.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "Jumat, 21 Desember 2018";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dpDateofBirth.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // radPanel17
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel17, 2);
            this.radPanel17.Controls.Add(this.txtNoIdentitas);
            this.radPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel17.Location = new System.Drawing.Point(645, 63);
            this.radPanel17.Name = "radPanel17";
            this.radPanel17.Size = new System.Drawing.Size(494, 44);
            this.radPanel17.TabIndex = 2;
            // 
            // txtNoIdentitas
            // 
            this.txtNoIdentitas.AutoSize = false;
            this.txtNoIdentitas.BackColor = System.Drawing.Color.White;
            this.txtNoIdentitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoIdentitas.Location = new System.Drawing.Point(0, 0);
            this.txtNoIdentitas.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNoIdentitas.MaxLength = 16;
            this.txtNoIdentitas.Name = "txtNoIdentitas";
            this.txtNoIdentitas.Size = new System.Drawing.Size(494, 44);
            this.txtNoIdentitas.TabIndex = 101;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // btnTakePhoto
            // 
            this.btnTakePhoto.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnTakePhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTakePhoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTakePhoto.Location = new System.Drawing.Point(1145, 413);
            this.btnTakePhoto.Name = "btnTakePhoto";
            this.btnTakePhoto.Size = new System.Drawing.Size(174, 44);
            this.btnTakePhoto.TabIndex = 18;
            this.btnTakePhoto.Text = "AMBIL FOTO";
            this.btnTakePhoto.UseVisualStyleBackColor = false;
            // 
            // lblVisitorFinger
            // 
            this.lblVisitorFinger.AutoSize = false;
            this.lblVisitorFinger.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.tableLayoutPanel1.SetColumnSpan(this.lblVisitorFinger, 2);
            this.lblVisitorFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblVisitorFinger.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.lblVisitorFinger.ForeColor = System.Drawing.Color.White;
            this.lblVisitorFinger.Image = ((System.Drawing.Image)(resources.GetObject("lblVisitorFinger.Image")));
            this.lblVisitorFinger.Location = new System.Drawing.Point(1142, 510);
            this.lblVisitorFinger.Margin = new System.Windows.Forms.Padding(0);
            this.lblVisitorFinger.Name = "lblVisitorFinger";
            this.lblVisitorFinger.Padding = new System.Windows.Forms.Padding(8);
            this.lblVisitorFinger.Size = new System.Drawing.Size(345, 50);
            this.lblVisitorFinger.TabIndex = 181;
            this.lblVisitorFinger.Text = "         SIDIK JARI PENGUNJUNG";
            // 
            // radPanel2
            // 
            this.radPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel2, 2);
            this.radPanel2.Controls.Add(this.btnUbahSidikJari);
            this.radPanel2.Controls.Add(this.picFinger);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(1142, 560);
            this.radPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.tableLayoutPanel1.SetRowSpan(this.radPanel2, 7);
            this.radPanel2.Size = new System.Drawing.Size(345, 400);
            this.radPanel2.TabIndex = 182;
            // 
            // btnUbahSidikJari
            // 
            this.btnUbahSidikJari.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnUbahSidikJari.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnUbahSidikJari.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUbahSidikJari.Location = new System.Drawing.Point(5, 345);
            this.btnUbahSidikJari.Name = "btnUbahSidikJari";
            this.btnUbahSidikJari.Size = new System.Drawing.Size(335, 50);
            this.btnUbahSidikJari.TabIndex = 26;
            this.btnUbahSidikJari.Text = "UBAH SIDIK JARI";
            this.btnUbahSidikJari.UseVisualStyleBackColor = false;
            // 
            // picFinger
            // 
            this.picFinger.BackColor = System.Drawing.Color.Silver;
            this.picFinger.Dock = System.Windows.Forms.DockStyle.Top;
            this.picFinger.Location = new System.Drawing.Point(5, 0);
            this.picFinger.Margin = new System.Windows.Forms.Padding(8, 0, 8, 8);
            this.picFinger.Name = "picFinger";
            this.picFinger.Size = new System.Drawing.Size(335, 350);
            this.picFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFinger.TabIndex = 176;
            this.picFinger.TabStop = false;
            // 
            // txtRelation
            // 
            this.txtRelation.BackColor = System.Drawing.Color.White;
            this.txtRelation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRelation.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRelation.ForeColor = System.Drawing.Color.Blue;
            this.txtRelation.Location = new System.Drawing.Point(887, 763);
            this.txtRelation.Name = "txtRelation";
            this.txtRelation.Size = new System.Drawing.Size(252, 44);
            this.txtRelation.TabIndex = 332;
            this.txtRelation.Text = "  KELUARGA";
            // 
            // btnUploadPhoto
            // 
            this.btnUploadPhoto.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnUploadPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUploadPhoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnUploadPhoto.Location = new System.Drawing.Point(1325, 413);
            this.btnUploadPhoto.Name = "btnUploadPhoto";
            this.btnUploadPhoto.Size = new System.Drawing.Size(159, 44);
            this.btnUploadPhoto.TabIndex = 334;
            this.btnUploadPhoto.Text = "UNGGAH FOTO";
            this.btnUploadPhoto.UseVisualStyleBackColor = false;
            // 
            // radPanel6
            // 
            this.radPanel6.Controls.Add(this.datetimeStartVisit);
            this.radPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel6.Location = new System.Drawing.Point(263, 963);
            this.radPanel6.Name = "radPanel6";
            this.radPanel6.Size = new System.Drawing.Size(376, 44);
            this.radPanel6.TabIndex = 105;
            // 
            // datetimeStartVisit
            // 
            this.datetimeStartVisit.AutoSize = false;
            this.datetimeStartVisit.BackColor = System.Drawing.Color.Transparent;
            this.datetimeStartVisit.CalendarSize = new System.Drawing.Size(290, 320);
            this.datetimeStartVisit.Culture = new System.Globalization.CultureInfo("id-ID");
            this.datetimeStartVisit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datetimeStartVisit.Location = new System.Drawing.Point(0, 0);
            this.datetimeStartVisit.Margin = new System.Windows.Forms.Padding(0);
            this.datetimeStartVisit.Name = "datetimeStartVisit";
            this.datetimeStartVisit.Size = new System.Drawing.Size(376, 44);
            this.datetimeStartVisit.TabIndex = 104;
            this.datetimeStartVisit.TabStop = false;
            this.datetimeStartVisit.Text = "Jumat, 21 Desember 2018";
            this.datetimeStartVisit.Value = new System.DateTime(2018, 12, 21, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.datetimeStartVisit.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.datetimeStartVisit.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.datetimeStartVisit.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.datetimeStartVisit.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.datetimeStartVisit.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.datetimeStartVisit.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.datetimeStartVisit.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.datetimeStartVisit.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "Jumat, 21 Desember 2018";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.datetimeStartVisit.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // lblDetailSurat
            // 
            this.lblDetailSurat.AutoSize = false;
            this.lblDetailSurat.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailSurat.Location = new System.Drawing.Point(0, 56);
            this.lblDetailSurat.Name = "lblDetailSurat";
            this.lblDetailSurat.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetailSurat.Size = new System.Drawing.Size(1516, 52);
            this.lblDetailSurat.TabIndex = 751;
            this.lblDetailSurat.Text = "Mohon Isi seluruh Kolom yang Diperlukan";
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.lblTitle);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Margin = new System.Windows.Forms.Padding(4);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1516, 56);
            this.headerPanel.TabIndex = 71;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(1516, 56);
            this.lblTitle.TabIndex = 711;
            // 
            // cameraSource
            // 
            this.cameraSource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cameraSource.Location = new System.Drawing.Point(32700, 320);
            this.cameraSource.Name = "cameraSource";
            this.cameraSource.Size = new System.Drawing.Size(581, 186);
            this.cameraSource.TabIndex = 82;
            this.cameraSource.Visible = false;
            // 
            // FormVisitorShort
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.cameraSource);
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radPanelError);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblDetailSurat);
            this.Controls.Add(this.headerPanel);
            this.Name = "FormVisitorShort";
            this.Size = new System.Drawing.Size(1516, 736);
            this.Load += new System.EventHandler(this.TambahTersangka_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).EndInit();
            this.radPanelError.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRelation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel19)).EndInit();
            this.radPanel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlJnsIdentitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel18)).EndInit();
            this.radPanel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel16)).EndInit();
            this.radPanel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPlaceofBirth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel15)).EndInit();
            this.radPanel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel14)).EndInit();
            this.radPanel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel12)).EndInit();
            this.radPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlGender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel11)).EndInit();
            this.radPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlAgama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel10)).EndInit();
            this.radPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlMaritalStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).EndInit();
            this.radPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlNationality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).EndInit();
            this.radPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).EndInit();
            this.radPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlVisitorType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            this.radPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtInmatesName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVisitorPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFullName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIDType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtBringgingItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBringging)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearchTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInmateNama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisit)).EndInit();
            this.lblDetailVisit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNationality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel9)).EndInit();
            this.radPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlVocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel21)).EndInit();
            this.radPanel21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dpDateofBirth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel17)).EndInit();
            this.radPanel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNoIdentitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVisitorFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRelation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).EndInit();
            this.radPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.datetimeStartVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).EndInit();
            this.headerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList imageList;
        private Telerik.WinControls.UI.RadPanel headerPanel;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel lblDetailSurat;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadTextBox txtCity;
        private Telerik.WinControls.UI.RadTextBox txtTelp;
        private Telerik.WinControls.UI.RadLabel lblPhone;
        private Telerik.WinControls.UI.RadLabel lblCity;
        private Telerik.WinControls.UI.RadLabel lblAddress;
        private Telerik.WinControls.UI.RadLabel lblFullName;
        private Telerik.WinControls.UI.RadLabel lblIDType;
        private Telerik.WinControls.UI.RadTextBox txtNoIdentitas;
        private Telerik.WinControls.UI.RadTextBox txtNama;
        private Telerik.WinControls.UI.RadTextBox txtAddress;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel lblDetailVisitor;
        private Telerik.WinControls.UI.RadLabel lblType;
        private Telerik.WinControls.UI.RadLabel lblInmateNama;
        private Telerik.WinControls.UI.RadLabel lblBringging;
        private Telerik.WinControls.UI.RadTextBox txtBringgingItems;
        private Telerik.WinControls.UI.RadLabel lblDetailVisit;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private System.Windows.Forms.PictureBox picCanvas;
        private Telerik.WinControls.UI.RadButton btnSearchTahanan;
        private Telerik.WinControls.UI.RadTextBox txtInmatesName;
        private System.Windows.Forms.Label tahananId;
        private Telerik.WinControls.UI.RadPanel radPanelError;
        private Telerik.WinControls.UI.RadLabel lblError;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnBack;
        private Telerik.WinControls.UI.RadButton btnSave;
        private CameraSource cameraSource;
        private Telerik.WinControls.UI.RadLabel lblVisitorPhoto;
        private Telerik.WinControls.UI.RadLabel lblVisitorFinger;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private System.Windows.Forms.PictureBox picFinger;
        private System.Windows.Forms.Button btnTakePhoto;
        private Telerik.WinControls.UI.RadPanel radPanel21;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadPanel radPanel5;
        private Telerik.WinControls.UI.RadPanel radPanel7;
        private Telerik.WinControls.UI.RadPanel radPanel8;
        private Telerik.WinControls.UI.RadPanel radPanel9;
        private Telerik.WinControls.UI.RadLabel lblNationality;
        private Telerik.WinControls.UI.RadDropDownList ddlNationality;
        private Telerik.WinControls.UI.RadDropDownList ddlVocation;
        private Telerik.WinControls.UI.RadLabel lblVocation;
        private Telerik.WinControls.UI.RadPanel radPanel10;
        private Telerik.WinControls.UI.RadDropDownList ddlMaritalStatus;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadPanel radPanel11;
        private Telerik.WinControls.UI.RadDropDownList ddlAgama;
        private Telerik.WinControls.UI.RadPanel radPanel12;
        private Telerik.WinControls.UI.RadDropDownList ddlGender;
        private Telerik.WinControls.UI.RadPanel radPanel14;
        private Telerik.WinControls.UI.RadPanel radPanel15;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadPanel radPanel16;
        private Telerik.WinControls.UI.RadDateTimePicker dpDateofBirth;
        private Telerik.WinControls.UI.RadTextBox txtPlaceofBirth;
        private Telerik.WinControls.UI.RadPanel radPanel18;
        private Telerik.WinControls.UI.RadPanel radPanel17;
        private Telerik.WinControls.UI.RadPanel radPanel19;
        private Telerik.WinControls.UI.RadDropDownList ddlJnsIdentitas;
        private Telerik.WinControls.UI.RadDropDownList ddlVisitorType;
        private Telerik.WinControls.UI.RadLabel lblRelation;
        private Telerik.WinControls.UI.RadPanel txtRelation;
        private System.Windows.Forms.Button btnUbahSidikJari;
        private System.Windows.Forms.Button btnUploadPhoto;
        private Telerik.WinControls.UI.RadPanel radPanel6;
        private Telerik.WinControls.UI.RadDateTimePicker datetimeStartVisit;
        private Telerik.WinControls.UI.RadLabel radLabel5;
    }
}
