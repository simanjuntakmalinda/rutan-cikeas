﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class FormJadwalKunjungan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormJadwalKunjungan));
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnBack = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            this.headerPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.lbl6 = new Telerik.WinControls.UI.RadLabel();
            this.lbl3 = new Telerik.WinControls.UI.RadLabel();
            this.lbl2 = new Telerik.WinControls.UI.RadLabel();
            this.lbl1 = new Telerik.WinControls.UI.RadLabel();
            this.lbl5 = new Telerik.WinControls.UI.RadLabel();
            this.lbl4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.tableFormCell = new System.Windows.Forms.TableLayoutPanel();
            this.lblDetail = new Telerik.WinControls.UI.RadLabel();
            this.PanelContainer = new Telerik.WinControls.UI.RadScrollablePanelContainer();
            this.lblRequired = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.rbTidak = new Telerik.WinControls.UI.RadRadioButton();
            this.rbYa = new Telerik.WinControls.UI.RadRadioButton();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.lblNoSell = new Telerik.WinControls.UI.RadLabel();
            this.lblRegID = new Telerik.WinControls.UI.RadLabel();
            this.radPanel7 = new Telerik.WinControls.UI.RadPanel();
            this.txtNamaTahanan = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.lblTahananId = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailSurat = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.ddlJumlahKunjungan = new Telerik.WinControls.UI.RadDropDownList();
            this.txtWaktuKunjungan = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.ddlWaktuKunjungan = new Telerik.WinControls.UI.RadCheckedDropDownList();
            this.radPanel5 = new Telerik.WinControls.UI.RadPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblphoto = new Telerik.WinControls.UI.RadLabel();
            this.picCanvas4 = new System.Windows.Forms.PictureBox();
            this.picCanvas3 = new System.Windows.Forms.PictureBox();
            this.picCanvas2 = new System.Windows.Forms.PictureBox();
            this.picCanvas1 = new System.Windows.Forms.PictureBox();
            this.lblNoID = new Telerik.WinControls.UI.RadLabel();
            this.btnSearchTahanan = new Telerik.WinControls.UI.RadButton();
            this.radPanelError = new Telerik.WinControls.UI.RadPanel();
            this.lblError = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).BeginInit();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            this.tableFormCell.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbTidak)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbYa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            this.radPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).BeginInit();
            this.radPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTahananId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlJumlahKunjungan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWaktuKunjungan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            this.radLabel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlWaktuKunjungan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).BeginInit();
            this.radPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblphoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearchTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).BeginInit();
            this.radPanelError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).BeginInit();
            this.SuspendLayout();
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnBack);
            this.lblButton.Controls.Add(this.btnSave);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 649);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(1290, 118);
            this.lblButton.TabIndex = 74;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.btnBack_Image;
            this.btnBack.Location = new System.Drawing.Point(30, 30);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(244, 58);
            this.btnBack.TabIndex = 9;
            this.btnBack.Tag = "";
            this.btnBack.Text = "&BATAL";
            this.btnBack.ThemeName = "MaterialBlueGrey";
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(955, 30);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(305, 58);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "&SIMPAN";
            this.btnSave.ThemeName = "MaterialBlueGrey";
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.lblTitle);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Margin = new System.Windows.Forms.Padding(4);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1290, 56);
            this.headerPanel.TabIndex = 81;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(1290, 56);
            this.lblTitle.TabIndex = 8;
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = false;
            this.lbl6.Location = new System.Drawing.Point(14, 414);
            this.lbl6.Margin = new System.Windows.Forms.Padding(4);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(317, 42);
            this.lbl6.TabIndex = 120;
            this.lbl6.Text = "<html>Cell Status<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = false;
            this.lbl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl3.Location = new System.Drawing.Point(14, 164);
            this.lbl3.Margin = new System.Windows.Forms.Padding(4);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(317, 42);
            this.lbl3.TabIndex = 65;
            this.lbl3.Text = "<html>Cell Floor<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = false;
            this.lbl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl2.Location = new System.Drawing.Point(14, 114);
            this.lbl2.Margin = new System.Windows.Forms.Padding(4);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(317, 42);
            this.lbl2.TabIndex = 118;
            this.lbl2.Text = "<html>Building Name<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = false;
            this.lbl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl1.Location = new System.Drawing.Point(14, 64);
            this.lbl1.Margin = new System.Windows.Forms.Padding(4);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(317, 42);
            this.lbl1.TabIndex = 19;
            this.lbl1.Text = "<html>Cell Code<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = false;
            this.lbl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl5.Location = new System.Drawing.Point(14, 264);
            this.lbl5.Margin = new System.Windows.Forms.Padding(4);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(317, 42);
            this.lbl5.TabIndex = 66;
            this.lbl5.Text = "<html>Cell Description<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = false;
            this.lbl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl4.Location = new System.Drawing.Point(14, 214);
            this.lbl4.Margin = new System.Windows.Forms.Padding(4);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(317, 42);
            this.lbl4.TabIndex = 20;
            this.lbl4.Text = "<html>Cell Number<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel5.Location = new System.Drawing.Point(14, 364);
            this.radLabel5.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(317, 42);
            this.radLabel5.TabIndex = 121;
            this.radLabel5.Text = "<html>Cell Type<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // tableFormCell
            // 
            this.tableFormCell.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tableFormCell.ColumnCount = 3;
            this.tableFormCell.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tableFormCell.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableFormCell.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableFormCell.Controls.Add(this.lblDetail, 0, 0);
            this.tableFormCell.Controls.Add(this.radLabel5, 0, 7);
            this.tableFormCell.Controls.Add(this.lbl4, 0, 4);
            this.tableFormCell.Controls.Add(this.lbl5, 0, 5);
            this.tableFormCell.Controls.Add(this.lbl1, 0, 1);
            this.tableFormCell.Controls.Add(this.lbl2, 0, 2);
            this.tableFormCell.Controls.Add(this.lbl3, 0, 3);
            this.tableFormCell.Controls.Add(this.lbl6, 0, 8);
            this.tableFormCell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableFormCell.Location = new System.Drawing.Point(0, 0);
            this.tableFormCell.Margin = new System.Windows.Forms.Padding(10);
            this.tableFormCell.Name = "tableFormCell";
            this.tableFormCell.Padding = new System.Windows.Forms.Padding(10);
            this.tableFormCell.RowCount = 10;
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.Size = new System.Drawing.Size(996, 530);
            this.tableFormCell.TabIndex = 17;
            // 
            // lblDetail
            // 
            this.lblDetail.AutoSize = false;
            this.tableFormCell.SetColumnSpan(this.lblDetail, 3);
            this.lblDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetail.Image = ((System.Drawing.Image)(resources.GetObject("lblDetail.Image")));
            this.lblDetail.Location = new System.Drawing.Point(13, 13);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetail.Size = new System.Drawing.Size(970, 44);
            this.lblDetail.TabIndex = 130;
            this.lblDetail.Text = "         DETAIL CELL DATA";
            // 
            // PanelContainer
            // 
            this.PanelContainer.AutoScroll = false;
            this.PanelContainer.Dock = System.Windows.Forms.DockStyle.None;
            this.PanelContainer.Location = new System.Drawing.Point(0, 0);
            this.PanelContainer.Size = new System.Drawing.Size(979, 548);
            // 
            // lblRequired
            // 
            this.lblRequired.AutoSize = false;
            this.lblRequired.BackColor = System.Drawing.SystemColors.Control;
            this.lblRequired.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblRequired.Location = new System.Drawing.Point(0, 56);
            this.lblRequired.Name = "lblRequired";
            this.lblRequired.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblRequired.Size = new System.Drawing.Size(1290, 52);
            this.lblRequired.TabIndex = 84;
            this.lblRequired.Text = "Mohon Isi seluruh Kolom yang Diperlukan";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.BackColor = System.Drawing.SystemColors.Control;
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 108);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(1271, 496);
            this.radScrollablePanel1.Size = new System.Drawing.Size(1290, 498);
            this.radScrollablePanel1.TabIndex = 85;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.33026F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.3364F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.3464F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 127F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 412F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel6, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.radPanel4, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblRegID, 5, 11);
            this.tableLayoutPanel1.Controls.Add(this.radPanel7, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailSurat, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radLabel7, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtWaktuKunjungan, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.radLabel9, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.radLabel8, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.radLabel3, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.radPanel5, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblNoID, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnSearchTahanan, 3, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 14;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1271, 600);
            this.tableLayoutPanel1.TabIndex = 70;
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel6.Location = new System.Drawing.Point(14, 164);
            this.radLabel6.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(220, 42);
            this.radLabel6.TabIndex = 120;
            this.radLabel6.Text = "<html>No Sel Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.Location = new System.Drawing.Point(14, 114);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(220, 42);
            this.radLabel1.TabIndex = 119;
            this.radLabel1.Text = "<html>No Registrasi Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel3, 2);
            this.radPanel3.Controls.Add(this.rbTidak);
            this.radPanel3.Controls.Add(this.rbYa);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Location = new System.Drawing.Point(241, 213);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(411, 44);
            this.radPanel3.TabIndex = 5;
            // 
            // rbTidak
            // 
            this.rbTidak.AutoSize = false;
            this.rbTidak.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbTidak.Location = new System.Drawing.Point(207, 0);
            this.rbTidak.Name = "rbTidak";
            this.rbTidak.Size = new System.Drawing.Size(220, 44);
            this.rbTidak.TabIndex = 5;
            this.rbTidak.Text = "TIDAK";
            // 
            // rbYa
            // 
            this.rbYa.AutoSize = false;
            this.rbYa.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbYa.Location = new System.Drawing.Point(0, 0);
            this.rbYa.Name = "rbYa";
            this.rbYa.Size = new System.Drawing.Size(207, 44);
            this.rbYa.TabIndex = 5;
            this.rbYa.TabStop = false;
            this.rbYa.Text = "YA";
            // 
            // radPanel4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel4, 2);
            this.radPanel4.Controls.Add(this.lblNoSell);
            this.radPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel4.Location = new System.Drawing.Point(241, 163);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(411, 44);
            this.radPanel4.TabIndex = 4;
            // 
            // lblNoSell
            // 
            this.lblNoSell.AutoSize = false;
            this.lblNoSell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoSell.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblNoSell.Location = new System.Drawing.Point(0, 0);
            this.lblNoSell.Name = "lblNoSell";
            this.lblNoSell.Size = new System.Drawing.Size(411, 44);
            this.lblNoSell.TabIndex = 4;
            // 
            // lblRegID
            // 
            this.lblRegID.AutoSize = false;
            this.lblRegID.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegID.ForeColor = System.Drawing.Color.White;
            this.lblRegID.Location = new System.Drawing.Point(811, 563);
            this.lblRegID.Name = "lblRegID";
            this.lblRegID.Size = new System.Drawing.Size(406, 44);
            this.lblRegID.TabIndex = 139;
            this.lblRegID.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblRegID.Visible = false;
            // 
            // radPanel7
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel7, 2);
            this.radPanel7.Controls.Add(this.txtNamaTahanan);
            this.radPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel7.Location = new System.Drawing.Point(241, 63);
            this.radPanel7.Name = "radPanel7";
            this.radPanel7.Size = new System.Drawing.Size(411, 44);
            this.radPanel7.TabIndex = 2;
            // 
            // txtNamaTahanan
            // 
            this.txtNamaTahanan.AutoSize = false;
            this.txtNamaTahanan.BackColor = System.Drawing.Color.White;
            this.txtNamaTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNamaTahanan.Enabled = false;
            this.txtNamaTahanan.Location = new System.Drawing.Point(0, 0);
            this.txtNamaTahanan.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNamaTahanan.Name = "txtNamaTahanan";
            this.txtNamaTahanan.Size = new System.Drawing.Size(411, 44);
            this.txtNamaTahanan.TabIndex = 1;
            // 
            // radPanel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel1, 2);
            this.radPanel1.Controls.Add(this.lblTahananId);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(241, 113);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(411, 44);
            this.radPanel1.TabIndex = 3;
            // 
            // lblTahananId
            // 
            this.lblTahananId.AutoSize = false;
            this.lblTahananId.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTahananId.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblTahananId.Location = new System.Drawing.Point(0, 0);
            this.lblTahananId.Name = "lblTahananId";
            this.lblTahananId.Size = new System.Drawing.Size(411, 44);
            this.lblTahananId.TabIndex = 3;
            // 
            // lblDetailSurat
            // 
            this.lblDetailSurat.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailSurat, 5);
            this.lblDetailSurat.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailSurat.Image = ((System.Drawing.Image)(resources.GetObject("lblDetailSurat.Image")));
            this.lblDetailSurat.Location = new System.Drawing.Point(13, 13);
            this.lblDetailSurat.Name = "lblDetailSurat";
            this.lblDetailSurat.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetailSurat.Size = new System.Drawing.Size(792, 44);
            this.lblDetailSurat.TabIndex = 138;
            this.lblDetailSurat.Text = "         DETAIL JADWAL KUNJUNGAN";
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel7.Location = new System.Drawing.Point(14, 64);
            this.radLabel7.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(220, 42);
            this.radLabel7.TabIndex = 19;
            this.radLabel7.Text = "<html>Nama Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel2
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel2, 2);
            this.radPanel2.Controls.Add(this.ddlJumlahKunjungan);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(241, 263);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(411, 44);
            this.radPanel2.TabIndex = 6;
            // 
            // ddlJumlahKunjungan
            // 
            this.ddlJumlahKunjungan.AutoSize = false;
            this.ddlJumlahKunjungan.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ddlJumlahKunjungan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlJumlahKunjungan.DropDownHeight = 300;
            this.ddlJumlahKunjungan.Location = new System.Drawing.Point(0, 0);
            this.ddlJumlahKunjungan.Name = "ddlJumlahKunjungan";
            this.ddlJumlahKunjungan.Size = new System.Drawing.Size(411, 44);
            this.ddlJumlahKunjungan.TabIndex = 6;
            // 
            // txtWaktuKunjungan
            // 
            this.txtWaktuKunjungan.AutoSize = false;
            this.txtWaktuKunjungan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtWaktuKunjungan.Location = new System.Drawing.Point(14, 314);
            this.txtWaktuKunjungan.Margin = new System.Windows.Forms.Padding(4);
            this.txtWaktuKunjungan.Name = "txtWaktuKunjungan";
            this.txtWaktuKunjungan.Size = new System.Drawing.Size(220, 42);
            this.txtWaktuKunjungan.TabIndex = 120;
            this.txtWaktuKunjungan.Text = "<html>Waktu Kunjungan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel9.Location = new System.Drawing.Point(14, 264);
            this.radLabel9.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(220, 42);
            this.radLabel9.TabIndex = 65;
            this.radLabel9.Text = "<html>Intensitas Kunjungan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel8.Location = new System.Drawing.Point(14, 214);
            this.radLabel8.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(220, 42);
            this.radLabel8.TabIndex = 118;
            this.radLabel8.Text = "<html>Apakah Boleh Dikunjungi<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.radLabel3, 2);
            this.radLabel3.Controls.Add(this.ddlWaktuKunjungan);
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.radLabel3.Location = new System.Drawing.Point(241, 313);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(411, 44);
            this.radLabel3.TabIndex = 7;
            // 
            // ddlWaktuKunjungan
            // 
            this.ddlWaktuKunjungan.AutoSize = false;
            this.ddlWaktuKunjungan.BackColor = System.Drawing.Color.White;
            this.ddlWaktuKunjungan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlWaktuKunjungan.Location = new System.Drawing.Point(0, 0);
            this.ddlWaktuKunjungan.Name = "ddlWaktuKunjungan";
            this.ddlWaktuKunjungan.Size = new System.Drawing.Size(411, 44);
            this.ddlWaktuKunjungan.TabIndex = 7;
            ((Telerik.WinControls.UI.RadCheckedDropDownListElement)(this.ddlWaktuKunjungan.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlWaktuKunjungan.GetChildAt(0).GetChildAt(2))).MinSize = new System.Drawing.Size(0, 50);
            // 
            // radPanel5
            // 
            this.radPanel5.BackColor = System.Drawing.Color.Black;
            this.radPanel5.Controls.Add(this.label4);
            this.radPanel5.Controls.Add(this.label5);
            this.radPanel5.Controls.Add(this.label3);
            this.radPanel5.Controls.Add(this.label2);
            this.radPanel5.Controls.Add(this.label1);
            this.radPanel5.Controls.Add(this.lblphoto);
            this.radPanel5.Controls.Add(this.picCanvas4);
            this.radPanel5.Controls.Add(this.picCanvas3);
            this.radPanel5.Controls.Add(this.picCanvas2);
            this.radPanel5.Controls.Add(this.picCanvas1);
            this.radPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel5.Location = new System.Drawing.Point(808, 60);
            this.radPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.radPanel5.Name = "radPanel5";
            this.tableLayoutPanel1.SetRowSpan(this.radPanel5, 10);
            this.radPanel5.Size = new System.Drawing.Size(412, 500);
            this.radPanel5.TabIndex = 502;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(238, 471);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 19);
            this.label4.TabIndex = 147;
            this.label4.Text = "IBU JARI KANAN";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(46, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(184, 19);
            this.label5.TabIndex = 147;
            this.label5.Text = "FOTO DAN SIDIK JARI";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(86, 471);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 19);
            this.label3.TabIndex = 147;
            this.label3.Text = "KIRI";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(70, 245);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 19);
            this.label2.TabIndex = 146;
            this.label2.Text = "DEPAN";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(272, 245);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 19);
            this.label1.TabIndex = 145;
            this.label1.Text = "KANAN";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblphoto
            // 
            this.lblphoto.AutoSize = false;
            this.lblphoto.BackColor = System.Drawing.Color.Transparent;
            this.lblphoto.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblphoto.ForeColor = System.Drawing.Color.White;
            this.lblphoto.Image = ((System.Drawing.Image)(resources.GetObject("lblphoto.Image")));
            this.lblphoto.Location = new System.Drawing.Point(0, 0);
            this.lblphoto.Name = "lblphoto";
            this.lblphoto.Size = new System.Drawing.Size(48, 47);
            this.lblphoto.TabIndex = 144;
            this.lblphoto.Text = "       ";
            // 
            // picCanvas4
            // 
            this.picCanvas4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.picCanvas4.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picCanvas4.Location = new System.Drawing.Point(210, 274);
            this.picCanvas4.Margin = new System.Windows.Forms.Padding(0);
            this.picCanvas4.Name = "picCanvas4";
            this.picCanvas4.Size = new System.Drawing.Size(194, 189);
            this.picCanvas4.TabIndex = 137;
            this.picCanvas4.TabStop = false;
            // 
            // picCanvas3
            // 
            this.picCanvas3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.picCanvas3.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picCanvas3.Location = new System.Drawing.Point(7, 274);
            this.picCanvas3.Margin = new System.Windows.Forms.Padding(0);
            this.picCanvas3.Name = "picCanvas3";
            this.picCanvas3.Size = new System.Drawing.Size(197, 188);
            this.picCanvas3.TabIndex = 136;
            this.picCanvas3.TabStop = false;
            // 
            // picCanvas2
            // 
            this.picCanvas2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.picCanvas2.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picCanvas2.Location = new System.Drawing.Point(210, 50);
            this.picCanvas2.Margin = new System.Windows.Forms.Padding(0);
            this.picCanvas2.Name = "picCanvas2";
            this.picCanvas2.Size = new System.Drawing.Size(196, 186);
            this.picCanvas2.TabIndex = 135;
            this.picCanvas2.TabStop = false;
            // 
            // picCanvas1
            // 
            this.picCanvas1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.picCanvas1.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picCanvas1.Location = new System.Drawing.Point(8, 50);
            this.picCanvas1.Margin = new System.Windows.Forms.Padding(0);
            this.picCanvas1.Name = "picCanvas1";
            this.picCanvas1.Size = new System.Drawing.Size(197, 186);
            this.picCanvas1.TabIndex = 134;
            this.picCanvas1.TabStop = false;
            // 
            // lblNoID
            // 
            this.lblNoID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoID.Location = new System.Drawing.Point(658, 113);
            this.lblNoID.Name = "lblNoID";
            this.lblNoID.Size = new System.Drawing.Size(2, 2);
            this.lblNoID.TabIndex = 528;
            this.lblNoID.Visible = false;
            // 
            // btnSearchTahanan
            // 
            this.btnSearchTahanan.BackColor = System.Drawing.Color.DimGray;
            this.btnSearchTahanan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearchTahanan.ForeColor = System.Drawing.Color.White;
            this.btnSearchTahanan.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchTahanan.Image")));
            this.btnSearchTahanan.Location = new System.Drawing.Point(660, 65);
            this.btnSearchTahanan.Margin = new System.Windows.Forms.Padding(5);
            this.btnSearchTahanan.Name = "btnSearchTahanan";
            this.btnSearchTahanan.Size = new System.Drawing.Size(117, 40);
            this.btnSearchTahanan.TabIndex = 1;
            this.btnSearchTahanan.Text = "       Cari";
            this.btnSearchTahanan.ThemeName = "MaterialBlueGrey";
            // 
            // radPanelError
            // 
            this.radPanelError.Controls.Add(this.lblError);
            this.radPanelError.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanelError.Location = new System.Drawing.Point(0, 606);
            this.radPanelError.Margin = new System.Windows.Forms.Padding(0);
            this.radPanelError.Name = "radPanelError";
            this.radPanelError.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radPanelError.Size = new System.Drawing.Size(1290, 43);
            this.radPanelError.TabIndex = 86;
            // 
            // lblError
            // 
            this.lblError.AutoSize = false;
            this.lblError.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.warning;
            this.lblError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblError.Location = new System.Drawing.Point(15, 0);
            this.lblError.Name = "lblError";
            this.lblError.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.lblError.Size = new System.Drawing.Size(1275, 39);
            this.lblError.TabIndex = 24;
            // 
            // FormJadwalKunjungan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.radPanelError);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblRequired);
            this.Controls.Add(this.headerPanel);
            this.Controls.Add(this.lblButton);
            this.Name = "FormJadwalKunjungan";
            this.Size = new System.Drawing.Size(1290, 767);
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).EndInit();
            this.headerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            this.tableFormCell.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRequired)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbTidak)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbYa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            this.radPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblNoSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).EndInit();
            this.radPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTahananId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlJumlahKunjungan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWaktuKunjungan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            this.radLabel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlWaktuKunjungan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).EndInit();
            this.radPanel5.ResumeLayout(false);
            this.radPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblphoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearchTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).EndInit();
            this.radPanelError.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnBack;
        private Telerik.WinControls.UI.RadButton btnSave;
        private Telerik.WinControls.UI.RadPanel headerPanel;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel lbl6;
        private System.Windows.Forms.TableLayoutPanel tableFormCell;
        private Telerik.WinControls.UI.RadLabel lblDetail;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel lbl4;
        private Telerik.WinControls.UI.RadLabel lbl5;
        private Telerik.WinControls.UI.RadLabel lbl1;
        private Telerik.WinControls.UI.RadLabel lbl2;
        private Telerik.WinControls.UI.RadLabel lbl3;
        private Telerik.WinControls.UI.RadLabel lblRequired;
        private Telerik.WinControls.UI.RadScrollablePanelContainer PanelContainer;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private Telerik.WinControls.UI.RadPanel radPanelError;
        private Telerik.WinControls.UI.RadLabel lblError;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel lblDetailSurat;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadPanel radPanel7;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadRadioButton rbYa;
        private Telerik.WinControls.UI.RadRadioButton rbTidak;
        private Telerik.WinControls.UI.RadPanel radPanel5;
        private Telerik.WinControls.UI.RadLabel lblphoto;
        private System.Windows.Forms.PictureBox picCanvas4;
        private System.Windows.Forms.PictureBox picCanvas3;
        private System.Windows.Forms.PictureBox picCanvas2;
        private System.Windows.Forms.PictureBox picCanvas1;
        private Telerik.WinControls.UI.RadDropDownList ddlJumlahKunjungan;
        private Telerik.WinControls.UI.RadCheckedDropDownList ddlWaktuKunjungan;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadLabel txtWaktuKunjungan;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel lblNoSell;
        private Telerik.WinControls.UI.RadLabel lblTahananId;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox txtNamaTahanan;
        private Telerik.WinControls.UI.RadButton btnSearchTahanan;
        private Telerik.WinControls.UI.RadLabel lblNoID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private Telerik.WinControls.UI.RadLabel lblRegID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}
