﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class CapturePhoto
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CapturePhoto));
            this.panelcapture = new Telerik.WinControls.UI.RadPanel();
            this.panelBorder = new System.Windows.Forms.Panel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.panelbiru = new Telerik.WinControls.UI.RadPanel();
            this.btnSetting = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.panelLeftPhoto = new Telerik.WinControls.UI.RadPanel();
            this.leftPhoto = new System.Windows.Forms.PictureBox();
            this.lblLeftPict = new System.Windows.Forms.Label();
            this.panelRightPhoto = new Telerik.WinControls.UI.RadPanel();
            this.rightPhoto = new System.Windows.Forms.PictureBox();
            this.lblFrontPict = new System.Windows.Forms.Label();
            this.panelFrontPhoto = new Telerik.WinControls.UI.RadPanel();
            this.frontPhoto = new System.Windows.Forms.PictureBox();
            this.lblRightPict = new System.Windows.Forms.Label();
            this.panelcamera = new Telerik.WinControls.UI.RadPanel();
            this.CamOnOff = new Telerik.WinControls.UI.RadButton();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.picCanvas = new System.Windows.Forms.PictureBox();
            this.btnCapture = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.panelcapture)).BeginInit();
            this.panelcapture.SuspendLayout();
            this.panelBorder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelbiru)).BeginInit();
            this.panelbiru.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelLeftPhoto)).BeginInit();
            this.panelLeftPhoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leftPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelRightPhoto)).BeginInit();
            this.panelRightPhoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rightPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelFrontPhoto)).BeginInit();
            this.panelFrontPhoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frontPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelcamera)).BeginInit();
            this.panelcamera.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CamOnOff)).BeginInit();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCapture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            this.SuspendLayout();
            // 
            // panelcapture
            // 
            this.panelcapture.Controls.Add(this.panelBorder);
            this.panelcapture.Controls.Add(this.panelbiru);
            this.panelcapture.Controls.Add(this.lblTitle);
            this.panelcapture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelcapture.Location = new System.Drawing.Point(0, 0);
            this.panelcapture.Name = "panelcapture";
            this.panelcapture.Size = new System.Drawing.Size(523, 805);
            this.panelcapture.TabIndex = 0;
            // 
            // panelBorder
            // 
            this.panelBorder.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelBorder.Controls.Add(this.radLabel1);
            this.panelBorder.Location = new System.Drawing.Point(5, 479);
            this.panelBorder.Name = "panelBorder";
            this.panelBorder.Size = new System.Drawing.Size(513, 17);
            this.panelBorder.TabIndex = 89;
            this.panelBorder.Visible = false;
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel1.Location = new System.Drawing.Point(0, 0);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(513, 6);
            this.radLabel1.TabIndex = 7;
            // 
            // panelbiru
            // 
            this.panelbiru.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelbiru.Controls.Add(this.btnSetting);
            this.panelbiru.Controls.Add(this.tableLayoutPanel3);
            this.panelbiru.Controls.Add(this.btnSave);
            this.panelbiru.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelbiru.Location = new System.Drawing.Point(0, 50);
            this.panelbiru.Name = "panelbiru";
            this.panelbiru.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.panelbiru.Size = new System.Drawing.Size(523, 755);
            this.panelbiru.TabIndex = 88;
            this.panelbiru.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panelbiru.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panelbiru.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panelbiru.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // btnSetting
            // 
            this.btnSetting.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetting.Image = ((System.Drawing.Image)(resources.GetObject("btnSetting.Image")));
            this.btnSetting.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSetting.Location = new System.Drawing.Point(5, 696);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnSetting.Size = new System.Drawing.Size(168, 49);
            this.btnSetting.TabIndex = 183;
            this.btnSetting.Text = "   &PENGATURAN";
            this.btnSetting.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(5, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.Padding = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(513, 690);
            this.tableLayoutPanel3.TabIndex = 89;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.Silver;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 495F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.panelcamera, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(9, 9);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 420F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(495, 672);
            this.tableLayoutPanel4.TabIndex = 176;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel5.ColumnCount = 3;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.Controls.Add(this.panelLeftPhoto, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblLeftPict, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.panelRightPhoto, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblFrontPict, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.panelFrontPhoto, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblRightPict, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 423);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.85944F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85.14056F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(489, 246);
            this.tableLayoutPanel5.TabIndex = 176;
            // 
            // panelLeftPhoto
            // 
            this.panelLeftPhoto.BackColor = System.Drawing.Color.DimGray;
            this.panelLeftPhoto.Controls.Add(this.leftPhoto);
            this.panelLeftPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeftPhoto.Location = new System.Drawing.Point(329, 39);
            this.panelLeftPhoto.Name = "panelLeftPhoto";
            this.panelLeftPhoto.Padding = new System.Windows.Forms.Padding(4);
            this.panelLeftPhoto.Size = new System.Drawing.Size(157, 204);
            this.panelLeftPhoto.TabIndex = 136;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panelLeftPhoto.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(4);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panelLeftPhoto.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panelLeftPhoto.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // leftPhoto
            // 
            this.leftPhoto.BackColor = System.Drawing.Color.Gray;
            this.leftPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftPhoto.InitialImage = null;
            this.leftPhoto.Location = new System.Drawing.Point(4, 4);
            this.leftPhoto.Margin = new System.Windows.Forms.Padding(0);
            this.leftPhoto.Name = "leftPhoto";
            this.leftPhoto.Size = new System.Drawing.Size(149, 196);
            this.leftPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.leftPhoto.TabIndex = 97;
            this.leftPhoto.TabStop = false;
            this.leftPhoto.Tag = "1";
            // 
            // lblLeftPict
            // 
            this.lblLeftPict.AutoSize = true;
            this.lblLeftPict.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.lblLeftPict.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLeftPict.ForeColor = System.Drawing.Color.White;
            this.lblLeftPict.Location = new System.Drawing.Point(329, 0);
            this.lblLeftPict.Name = "lblLeftPict";
            this.lblLeftPict.Size = new System.Drawing.Size(157, 36);
            this.lblLeftPict.TabIndex = 139;
            this.lblLeftPict.Text = "KIRI";
            this.lblLeftPict.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelRightPhoto
            // 
            this.panelRightPhoto.BackColor = System.Drawing.Color.DimGray;
            this.panelRightPhoto.Controls.Add(this.rightPhoto);
            this.panelRightPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRightPhoto.Location = new System.Drawing.Point(166, 39);
            this.panelRightPhoto.Name = "panelRightPhoto";
            this.panelRightPhoto.Padding = new System.Windows.Forms.Padding(4);
            this.panelRightPhoto.Size = new System.Drawing.Size(157, 204);
            this.panelRightPhoto.TabIndex = 135;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panelRightPhoto.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(4);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panelRightPhoto.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panelRightPhoto.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // rightPhoto
            // 
            this.rightPhoto.BackColor = System.Drawing.Color.Gray;
            this.rightPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightPhoto.InitialImage = null;
            this.rightPhoto.Location = new System.Drawing.Point(4, 4);
            this.rightPhoto.Margin = new System.Windows.Forms.Padding(0);
            this.rightPhoto.Name = "rightPhoto";
            this.rightPhoto.Size = new System.Drawing.Size(149, 196);
            this.rightPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.rightPhoto.TabIndex = 97;
            this.rightPhoto.TabStop = false;
            this.rightPhoto.Tag = "1";
            // 
            // lblFrontPict
            // 
            this.lblFrontPict.AutoSize = true;
            this.lblFrontPict.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.lblFrontPict.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFrontPict.ForeColor = System.Drawing.Color.White;
            this.lblFrontPict.Location = new System.Drawing.Point(3, 0);
            this.lblFrontPict.Name = "lblFrontPict";
            this.lblFrontPict.Size = new System.Drawing.Size(157, 36);
            this.lblFrontPict.TabIndex = 137;
            this.lblFrontPict.Text = "DEPAN";
            this.lblFrontPict.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelFrontPhoto
            // 
            this.panelFrontPhoto.BackColor = System.Drawing.Color.DimGray;
            this.panelFrontPhoto.Controls.Add(this.frontPhoto);
            this.panelFrontPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFrontPhoto.Location = new System.Drawing.Point(3, 39);
            this.panelFrontPhoto.Name = "panelFrontPhoto";
            this.panelFrontPhoto.Padding = new System.Windows.Forms.Padding(4);
            this.panelFrontPhoto.Size = new System.Drawing.Size(157, 204);
            this.panelFrontPhoto.TabIndex = 134;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panelFrontPhoto.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(4);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panelFrontPhoto.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panelFrontPhoto.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // frontPhoto
            // 
            this.frontPhoto.BackColor = System.Drawing.Color.Gray;
            this.frontPhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frontPhoto.InitialImage = null;
            this.frontPhoto.Location = new System.Drawing.Point(4, 4);
            this.frontPhoto.Margin = new System.Windows.Forms.Padding(0);
            this.frontPhoto.Name = "frontPhoto";
            this.frontPhoto.Size = new System.Drawing.Size(149, 196);
            this.frontPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.frontPhoto.TabIndex = 97;
            this.frontPhoto.TabStop = false;
            this.frontPhoto.Tag = "1";
            // 
            // lblRightPict
            // 
            this.lblRightPict.AutoSize = true;
            this.lblRightPict.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.lblRightPict.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRightPict.ForeColor = System.Drawing.Color.White;
            this.lblRightPict.Location = new System.Drawing.Point(166, 0);
            this.lblRightPict.Name = "lblRightPict";
            this.lblRightPict.Size = new System.Drawing.Size(157, 36);
            this.lblRightPict.TabIndex = 138;
            this.lblRightPict.Text = "KANAN";
            this.lblRightPict.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelcamera
            // 
            this.panelcamera.BackColor = System.Drawing.Color.Silver;
            this.panelcamera.Controls.Add(this.CamOnOff);
            this.panelcamera.Controls.Add(this.tableLayoutPanel7);
            this.panelcamera.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelcamera.Location = new System.Drawing.Point(3, 3);
            this.panelcamera.Name = "panelcamera";
            this.panelcamera.Padding = new System.Windows.Forms.Padding(100, 0, 100, 0);
            this.panelcamera.Size = new System.Drawing.Size(489, 414);
            this.panelcamera.TabIndex = 178;
            this.panelcamera.TextAlignment = System.Drawing.ContentAlignment.BottomLeft;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panelcamera.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(100, 0, 100, 0);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panelcamera.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panelcamera.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // CamOnOff
            // 
            this.CamOnOff.BackColor = System.Drawing.Color.White;
            this.CamOnOff.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CamOnOff.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.CamOff;
            this.CamOnOff.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.CamOnOff.Location = new System.Drawing.Point(448, 11);
            this.CamOnOff.Margin = new System.Windows.Forms.Padding(0);
            this.CamOnOff.Name = "CamOnOff";
            this.CamOnOff.Size = new System.Drawing.Size(36, 36);
            this.CamOnOff.TabIndex = 178;
            this.CamOnOff.TabStop = false;
            this.CamOnOff.ThemeName = "MaterialBlueGrey";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.BackColor = System.Drawing.Color.DimGray;
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.picCanvas, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.btnCapture, 0, 1);
            this.tableLayoutPanel7.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 5);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.5F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 59F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(486, 406);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // picCanvas
            // 
            this.picCanvas.BackColor = System.Drawing.Color.Gray;
            this.picCanvas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picCanvas.Location = new System.Drawing.Point(2, 2);
            this.picCanvas.Margin = new System.Windows.Forms.Padding(2, 2, 2, 0);
            this.picCanvas.Name = "picCanvas";
            this.picCanvas.Size = new System.Drawing.Size(482, 345);
            this.picCanvas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCanvas.TabIndex = 176;
            this.picCanvas.TabStop = false;
            // 
            // btnCapture
            // 
            this.btnCapture.BackColor = System.Drawing.Color.White;
            this.btnCapture.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCapture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCapture.Enabled = false;
            this.btnCapture.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.camera_off;
            this.btnCapture.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnCapture.Location = new System.Drawing.Point(4, 347);
            this.btnCapture.Margin = new System.Windows.Forms.Padding(4, 0, 4, 4);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(478, 55);
            this.btnCapture.TabIndex = 172;
            this.btnCapture.TabStop = false;
            this.btnCapture.ThemeName = "MaterialBlueGrey";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnSave.Enabled = false;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(371, 698);
            this.btnSave.Name = "btnSave";
            this.btnSave.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnSave.Size = new System.Drawing.Size(144, 49);
            this.btnSave.TabIndex = 182;
            this.btnSave.Text = "&SIMPAN";
            this.btnSave.UseVisualStyleBackColor = false;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(523, 50);
            this.lblTitle.TabIndex = 87;
            this.lblTitle.Text = "           PENGAMBILAN FOTO";
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(458, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 50);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 3;
            this.exit.TabStop = false;
            // 
            // CapturePhoto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelcapture);
            this.Name = "CapturePhoto";
            this.Size = new System.Drawing.Size(523, 805);
            ((System.ComponentModel.ISupportInitialize)(this.panelcapture)).EndInit();
            this.panelcapture.ResumeLayout(false);
            this.panelBorder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelbiru)).EndInit();
            this.panelbiru.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelLeftPhoto)).EndInit();
            this.panelLeftPhoto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leftPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelRightPhoto)).EndInit();
            this.panelRightPhoto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rightPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelFrontPhoto)).EndInit();
            this.panelFrontPhoto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.frontPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelcamera)).EndInit();
            this.panelcamera.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CamOnOff)).EndInit();
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCapture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel panelcapture;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadPanel panelbiru;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private Telerik.WinControls.UI.RadPanel panelLeftPhoto;
        private System.Windows.Forms.PictureBox leftPhoto;
        private Telerik.WinControls.UI.RadPanel panelRightPhoto;
        private System.Windows.Forms.PictureBox rightPhoto;
        private Telerik.WinControls.UI.RadPanel panelFrontPhoto;
        private System.Windows.Forms.PictureBox frontPhoto;
        private System.Windows.Forms.Label lblFrontPict;
        private System.Windows.Forms.Label lblLeftPict;
        private System.Windows.Forms.Label lblRightPict;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private Telerik.WinControls.UI.RadPanel panelcamera;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.PictureBox picCanvas;
        private Telerik.WinControls.UI.RadButton btnCapture;
        private System.Windows.Forms.PictureBox exit;
        private System.Windows.Forms.Button btnSetting;
        private System.Windows.Forms.Button btnSave;
        private Telerik.WinControls.UI.RadButton CamOnOff;
        private System.Windows.Forms.Panel panelBorder;
        private Telerik.WinControls.UI.RadLabel radLabel1;
    }
}
