﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class KendaraanTahananDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KendaraanTahananDetail));
            this.universitas = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDiubahTanggal = new Telerik.WinControls.UI.RadLabel();
            this.txtDiubahOleh = new Telerik.WinControls.UI.RadLabel();
            this.txtDibuatTanggal = new Telerik.WinControls.UI.RadLabel();
            this.txtDibuatOleh = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.lblNoBonTahanan = new Telerik.WinControls.UI.RadLabel();
            this.txtKategori = new Telerik.WinControls.UI.RadLabel();
            this.lblDataVisit = new Telerik.WinControls.UI.RadLabel();
            this.lblKeterangan = new Telerik.WinControls.UI.RadLabel();
            this.txtCatatan = new Telerik.WinControls.UI.RadLabel();
            this.lblLokasi = new Telerik.WinControls.UI.RadLabel();
            this.txtKeperluan = new Telerik.WinControls.UI.RadLabel();
            this.lblDurasi = new Telerik.WinControls.UI.RadLabel();
            this.txtTanggalMasuk = new Telerik.WinControls.UI.RadLabel();
            this.lblKeperluan = new Telerik.WinControls.UI.RadLabel();
            this.txtPengemudi = new Telerik.WinControls.UI.RadLabel();
            this.lblNamaTahanan = new Telerik.WinControls.UI.RadLabel();
            this.txtNoplat = new Telerik.WinControls.UI.RadLabel();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnClose = new Telerik.WinControls.UI.RadButton();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.txtWaktuMasuk = new Telerik.WinControls.UI.RadLabel();
            this.txtZonaWaktu = new Telerik.WinControls.UI.RadLabel();
            this.txtWaktuKeluar = new Telerik.WinControls.UI.RadLabel();
            this.btnCheckOut = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiubahTanggal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiubahOleh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDibuatTanggal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDibuatOleh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoBonTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKategori)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKeterangan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatatan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLokasi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeperluan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDurasi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanggalMasuk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKeperluan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPengemudi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoplat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWaktuMasuk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZonaWaktu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWaktuKeluar)).BeginInit();
            this.SuspendLayout();
            // 
            // universitas
            // 
            this.universitas.AutoSize = false;
            this.universitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.universitas.Location = new System.Drawing.Point(504, 414);
            this.universitas.Margin = new System.Windows.Forms.Padding(4);
            this.universitas.Name = "universitas";
            this.universitas.Size = new System.Drawing.Size(728, 42);
            this.universitas.TabIndex = 53;
            this.universitas.Text = "Tahun Lulus";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 56);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(1003, 529);
            this.radScrollablePanel1.Size = new System.Drawing.Size(1022, 531);
            this.radScrollablePanel1.TabIndex = 15;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 385F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 286F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 88F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 179F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel1.Controls.Add(this.btnCheckOut, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtWaktuKeluar, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtZonaWaktu, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtWaktuMasuk, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.radLabel6, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.radLabel5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtDiubahTanggal, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.txtDiubahOleh, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.txtDibuatTanggal, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.txtDibuatOleh, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.radLabel4, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.radLabel3, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.radLabel2, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.lblNoBonTahanan, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtKategori, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDataVisit, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblKeterangan, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtCatatan, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblLokasi, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtKeperluan, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblDurasi, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtTanggalMasuk, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblKeperluan, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtPengemudi, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblNamaTahanan, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtNoplat, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Symbol", 8.25F);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 13;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1003, 623);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // txtDiubahTanggal
            // 
            this.txtDiubahTanggal.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtDiubahTanggal, 3);
            this.txtDiubahTanggal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDiubahTanggal.Location = new System.Drawing.Point(399, 614);
            this.txtDiubahTanggal.Margin = new System.Windows.Forms.Padding(4);
            this.txtDiubahTanggal.Name = "txtDiubahTanggal";
            this.txtDiubahTanggal.Size = new System.Drawing.Size(545, 42);
            this.txtDiubahTanggal.TabIndex = 107;
            // 
            // txtDiubahOleh
            // 
            this.txtDiubahOleh.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtDiubahOleh, 3);
            this.txtDiubahOleh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDiubahOleh.Location = new System.Drawing.Point(399, 564);
            this.txtDiubahOleh.Margin = new System.Windows.Forms.Padding(4);
            this.txtDiubahOleh.Name = "txtDiubahOleh";
            this.txtDiubahOleh.Size = new System.Drawing.Size(545, 42);
            this.txtDiubahOleh.TabIndex = 106;
            // 
            // txtDibuatTanggal
            // 
            this.txtDibuatTanggal.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtDibuatTanggal, 3);
            this.txtDibuatTanggal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDibuatTanggal.Location = new System.Drawing.Point(399, 514);
            this.txtDibuatTanggal.Margin = new System.Windows.Forms.Padding(4);
            this.txtDibuatTanggal.Name = "txtDibuatTanggal";
            this.txtDibuatTanggal.Size = new System.Drawing.Size(545, 42);
            this.txtDibuatTanggal.TabIndex = 105;
            // 
            // txtDibuatOleh
            // 
            this.txtDibuatOleh.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtDibuatOleh, 3);
            this.txtDibuatOleh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDibuatOleh.Location = new System.Drawing.Point(399, 464);
            this.txtDibuatOleh.Margin = new System.Windows.Forms.Padding(4);
            this.txtDibuatOleh.Name = "txtDibuatOleh";
            this.txtDibuatOleh.Size = new System.Drawing.Size(545, 42);
            this.txtDibuatOleh.TabIndex = 104;
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel4.Location = new System.Drawing.Point(14, 614);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(377, 42);
            this.radLabel4.TabIndex = 103;
            this.radLabel4.Text = "DIUBAH TANGGAL";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel3.Location = new System.Drawing.Point(14, 564);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(377, 42);
            this.radLabel3.TabIndex = 102;
            this.radLabel3.Text = "DIUBAH OLEH";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel2.Location = new System.Drawing.Point(14, 514);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(377, 42);
            this.radLabel2.TabIndex = 101;
            this.radLabel2.Text = "DIBUAT TANGGAL";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel1.Location = new System.Drawing.Point(14, 464);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(377, 42);
            this.radLabel1.TabIndex = 100;
            this.radLabel1.Text = "DIBUAT OLEH";
            // 
            // lblNoBonTahanan
            // 
            this.lblNoBonTahanan.AutoSize = false;
            this.lblNoBonTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoBonTahanan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNoBonTahanan.Location = new System.Drawing.Point(14, 64);
            this.lblNoBonTahanan.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoBonTahanan.Name = "lblNoBonTahanan";
            this.lblNoBonTahanan.Size = new System.Drawing.Size(377, 42);
            this.lblNoBonTahanan.TabIndex = 11;
            this.lblNoBonTahanan.Text = "KATEGORI KENDARAAN";
            // 
            // txtKategori
            // 
            this.txtKategori.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtKategori, 2);
            this.txtKategori.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtKategori.Location = new System.Drawing.Point(399, 64);
            this.txtKategori.Margin = new System.Windows.Forms.Padding(4);
            this.txtKategori.Name = "txtKategori";
            this.txtKategori.Size = new System.Drawing.Size(366, 42);
            this.txtKategori.TabIndex = 58;
            // 
            // lblDataVisit
            // 
            this.lblDataVisit.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDataVisit, 4);
            this.lblDataVisit.Cursor = System.Windows.Forms.Cursors.No;
            this.lblDataVisit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDataVisit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDataVisit.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.kendaraan;
            this.lblDataVisit.Location = new System.Drawing.Point(14, 14);
            this.lblDataVisit.Margin = new System.Windows.Forms.Padding(4);
            this.lblDataVisit.Name = "lblDataVisit";
            this.lblDataVisit.Size = new System.Drawing.Size(930, 42);
            this.lblDataVisit.TabIndex = 62;
            this.lblDataVisit.Text = "         DETAIL KENDARAAN TAHANAN";
            // 
            // lblKeterangan
            // 
            this.lblKeterangan.AutoSize = false;
            this.lblKeterangan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKeterangan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblKeterangan.Location = new System.Drawing.Point(14, 414);
            this.lblKeterangan.Margin = new System.Windows.Forms.Padding(4);
            this.lblKeterangan.Name = "lblKeterangan";
            this.lblKeterangan.Size = new System.Drawing.Size(377, 42);
            this.lblKeterangan.TabIndex = 95;
            this.lblKeterangan.Text = "CATATAN";
            // 
            // txtCatatan
            // 
            this.txtCatatan.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtCatatan, 3);
            this.txtCatatan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCatatan.Location = new System.Drawing.Point(399, 414);
            this.txtCatatan.Margin = new System.Windows.Forms.Padding(4);
            this.txtCatatan.Name = "txtCatatan";
            this.txtCatatan.Size = new System.Drawing.Size(545, 42);
            this.txtCatatan.TabIndex = 97;
            // 
            // lblLokasi
            // 
            this.lblLokasi.AutoSize = false;
            this.lblLokasi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLokasi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblLokasi.Location = new System.Drawing.Point(14, 364);
            this.lblLokasi.Margin = new System.Windows.Forms.Padding(4);
            this.lblLokasi.Name = "lblLokasi";
            this.lblLokasi.Size = new System.Drawing.Size(377, 42);
            this.lblLokasi.TabIndex = 94;
            this.lblLokasi.Text = "KEPERLUAN";
            // 
            // txtKeperluan
            // 
            this.txtKeperluan.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtKeperluan, 3);
            this.txtKeperluan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtKeperluan.Location = new System.Drawing.Point(399, 364);
            this.txtKeperluan.Margin = new System.Windows.Forms.Padding(4);
            this.txtKeperluan.Name = "txtKeperluan";
            this.txtKeperluan.Size = new System.Drawing.Size(545, 42);
            this.txtKeperluan.TabIndex = 96;
            // 
            // lblDurasi
            // 
            this.lblDurasi.AutoSize = false;
            this.lblDurasi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDurasi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDurasi.Location = new System.Drawing.Point(14, 214);
            this.lblDurasi.Margin = new System.Windows.Forms.Padding(4);
            this.lblDurasi.Name = "lblDurasi";
            this.lblDurasi.Size = new System.Drawing.Size(377, 42);
            this.lblDurasi.TabIndex = 95;
            this.lblDurasi.Text = "TANGGAL/WAKTU MASUK";
            // 
            // txtTanggalMasuk
            // 
            this.txtTanggalMasuk.AutoSize = false;
            this.txtTanggalMasuk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTanggalMasuk.Location = new System.Drawing.Point(399, 214);
            this.txtTanggalMasuk.Margin = new System.Windows.Forms.Padding(4);
            this.txtTanggalMasuk.Name = "txtTanggalMasuk";
            this.txtTanggalMasuk.Size = new System.Drawing.Size(278, 42);
            this.txtTanggalMasuk.TabIndex = 97;
            // 
            // lblKeperluan
            // 
            this.lblKeperluan.AutoSize = false;
            this.lblKeperluan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblKeperluan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblKeperluan.Location = new System.Drawing.Point(14, 164);
            this.lblKeperluan.Margin = new System.Windows.Forms.Padding(4);
            this.lblKeperluan.Name = "lblKeperluan";
            this.lblKeperluan.Size = new System.Drawing.Size(377, 42);
            this.lblKeperluan.TabIndex = 19;
            this.lblKeperluan.Text = "PENGEMUDI";
            // 
            // txtPengemudi
            // 
            this.txtPengemudi.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtPengemudi, 3);
            this.txtPengemudi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPengemudi.Location = new System.Drawing.Point(399, 164);
            this.txtPengemudi.Margin = new System.Windows.Forms.Padding(4);
            this.txtPengemudi.Name = "txtPengemudi";
            this.txtPengemudi.Size = new System.Drawing.Size(545, 42);
            this.txtPengemudi.TabIndex = 58;
            // 
            // lblNamaTahanan
            // 
            this.lblNamaTahanan.AutoSize = false;
            this.lblNamaTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNamaTahanan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNamaTahanan.Location = new System.Drawing.Point(14, 114);
            this.lblNamaTahanan.Margin = new System.Windows.Forms.Padding(4);
            this.lblNamaTahanan.Name = "lblNamaTahanan";
            this.lblNamaTahanan.Size = new System.Drawing.Size(377, 42);
            this.lblNamaTahanan.TabIndex = 10;
            this.lblNamaTahanan.Text = "NO PLAT";
            // 
            // txtNoplat
            // 
            this.txtNoplat.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtNoplat, 3);
            this.txtNoplat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoplat.Location = new System.Drawing.Point(399, 114);
            this.txtNoplat.Margin = new System.Windows.Forms.Padding(4);
            this.txtNoplat.Name = "txtNoplat";
            this.txtNoplat.Size = new System.Drawing.Size(545, 42);
            this.txtNoplat.TabIndex = 57;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnClose);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 617);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(1022, 122);
            this.lblButton.TabIndex = 89;
            // 
            // btnClose
            // 
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(30, 30);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(244, 62);
            this.btnClose.TabIndex = 28;
            this.btnClose.Tag = "";
            this.btnClose.Text = "&CLOSE";
            this.btnClose.ThemeName = "MaterialBlueGrey";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1022, 56);
            this.lblTitle.TabIndex = 8;
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(957, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 56);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 1;
            this.exit.TabStop = false;
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel5.Location = new System.Drawing.Point(14, 264);
            this.radLabel5.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(377, 42);
            this.radLabel5.TabIndex = 108;
            this.radLabel5.Text = "WAKTU MASUK";
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel6.Location = new System.Drawing.Point(14, 314);
            this.radLabel6.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(377, 42);
            this.radLabel6.TabIndex = 109;
            this.radLabel6.Text = "WAKTU KELUAR";
            // 
            // txtWaktuMasuk
            // 
            this.txtWaktuMasuk.AutoSize = false;
            this.txtWaktuMasuk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtWaktuMasuk.Location = new System.Drawing.Point(399, 264);
            this.txtWaktuMasuk.Margin = new System.Windows.Forms.Padding(4);
            this.txtWaktuMasuk.Name = "txtWaktuMasuk";
            this.txtWaktuMasuk.Size = new System.Drawing.Size(278, 42);
            this.txtWaktuMasuk.TabIndex = 110;
            // 
            // txtZonaWaktu
            // 
            this.txtZonaWaktu.AutoSize = false;
            this.txtZonaWaktu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtZonaWaktu.Location = new System.Drawing.Point(685, 264);
            this.txtZonaWaktu.Margin = new System.Windows.Forms.Padding(4);
            this.txtZonaWaktu.Name = "txtZonaWaktu";
            this.txtZonaWaktu.Size = new System.Drawing.Size(80, 42);
            this.txtZonaWaktu.TabIndex = 111;
            // 
            // txtWaktuKeluar
            // 
            this.txtWaktuKeluar.AutoSize = false;
            this.txtWaktuKeluar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtWaktuKeluar.Location = new System.Drawing.Point(399, 314);
            this.txtWaktuKeluar.Margin = new System.Windows.Forms.Padding(4);
            this.txtWaktuKeluar.Name = "txtWaktuKeluar";
            this.txtWaktuKeluar.Size = new System.Drawing.Size(278, 42);
            this.txtWaktuKeluar.TabIndex = 112;
            // 
            // btnCheckOut
            // 
            this.btnCheckOut.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tableLayoutPanel1.SetColumnSpan(this.btnCheckOut, 2);
            this.btnCheckOut.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckOut.Image = ((System.Drawing.Image)(resources.GetObject("btnCheckOut.Image")));
            this.btnCheckOut.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCheckOut.Location = new System.Drawing.Point(684, 313);
            this.btnCheckOut.Name = "btnCheckOut";
            this.btnCheckOut.Padding = new System.Windows.Forms.Padding(5);
            this.btnCheckOut.Size = new System.Drawing.Size(207, 44);
            this.btnCheckOut.TabIndex = 113;
            this.btnCheckOut.Text = "    KELUAR";
            this.btnCheckOut.UseVisualStyleBackColor = false;
            // 
            // KendaraanTahananDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblTitle);
            this.Name = "KendaraanTahananDetail";
            this.Size = new System.Drawing.Size(1022, 739);
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDiubahTanggal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiubahOleh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDibuatTanggal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDibuatOleh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoBonTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKategori)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKeterangan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCatatan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLokasi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeperluan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDurasi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanggalMasuk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblKeperluan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPengemudi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoplat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWaktuMasuk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZonaWaktu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWaktuKeluar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel universitas;
        private System.Windows.Forms.PictureBox exit;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel txtPengemudi;
        private Telerik.WinControls.UI.RadLabel txtNoplat;
        private Telerik.WinControls.UI.RadLabel lblNamaTahanan;
        private Telerik.WinControls.UI.RadLabel lblKeperluan;
        private Telerik.WinControls.UI.RadLabel lblDataVisit;
        private Telerik.WinControls.UI.RadLabel lblLokasi;
        private Telerik.WinControls.UI.RadLabel lblKeterangan;
        private Telerik.WinControls.UI.RadLabel txtKeperluan;
        private Telerik.WinControls.UI.RadLabel txtCatatan;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnClose;
        private Telerik.WinControls.UI.RadLabel lblDurasi;
        private Telerik.WinControls.UI.RadLabel txtTanggalMasuk;
        private Telerik.WinControls.UI.RadLabel lblNoBonTahanan;
        private Telerik.WinControls.UI.RadLabel txtKategori;
        private Telerik.WinControls.UI.RadLabel txtDiubahTanggal;
        private Telerik.WinControls.UI.RadLabel txtDiubahOleh;
        private Telerik.WinControls.UI.RadLabel txtDibuatTanggal;
        private Telerik.WinControls.UI.RadLabel txtDibuatOleh;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel txtWaktuKeluar;
        private Telerik.WinControls.UI.RadLabel txtZonaWaktu;
        private Telerik.WinControls.UI.RadLabel txtWaktuMasuk;
        private System.Windows.Forms.Button btnCheckOut;
    }
}
