﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.DataAccess.Helper;
using Microsoft.VisualBasic;
using Microsoft.Win32;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class RptStatusTahanan : Base
    {
        public RptStatusTahanan()
        {
            InitializeComponent();
            InitForm();
            LoadData();
            InitEvents();    
        }

        private void InitForm()
        {
            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "      STATUS TAHANAN";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            webBrowser.IsWebBrowserContextMenuEnabled = false;
            webBrowser.AllowWebBrowserDrop = false;
        }


        public void LoadData()
        {
            StatusTahananService statusTahanan = new StatusTahananService();
            var countTahanan = statusTahanan.GetCount();

            var tahanan = statusTahanan.GetAll();
            string body = string.Empty;

            if(tahanan != null)
            {
                foreach (var item in tahanan)
                {
                    PerpanjanganTahananService pserv = new PerpanjanganTahananService();
                    var tglP1 = string.Empty;
                    var sP1 = string.Empty;
                    var tglP2 = string.Empty;
                    var sP2 = string.Empty;
                    var tglP3 = string.Empty;
                    var sP3 = string.Empty;
                    int d1 = 0;
                    int d2 = 0;
                    int d3 = 0;
                    var EXT1 = string.Empty;
                    var EXT2 = string.Empty;
                    var EXT3 = string.Empty;
                    var totalpenahanan = string.Empty;
                    var sisapenahanan = string.Empty;
                    var sel = string.Empty;

                    var data = pserv.GetByStatusTahananId(item.Id);
                    if (data != null)
                    {
                        foreach (var x in data)
                        {
                            if (x.PerpanjanganKe == "1")
                            {
                                tglP1 = x.TanggalPerpanjangan.Value == null? "-": x.TanggalPerpanjangan.Value.ToString("dd MMMM yyyy");
                                sP1 = x.NoSurat == null? "-": x.NoSurat;
                                d1 = x.Durasi == null? 0 : x.Durasi;
                                EXT1 = tglP1 + "<br />" + sP1 + "<br /><b>(" + d1 + " hari)</b>";
                            }
                            else if (x.PerpanjanganKe == "2")
                            {
                                tglP2 = x.TanggalPerpanjangan.Value == null ?"-": x.TanggalPerpanjangan.Value.ToString("dd MMMM yyyy");
                                sP2 = x.NoSurat == null ? "-" : x.NoSurat;
                                d2 = x.Durasi == null ? 0 : x.Durasi;
                                EXT2 = tglP2 + "<br />" + sP2 + "<br /><b>(" + d2 + " hari)</b>";
                            }
                            else
                            {
                                tglP3 = x.TanggalPerpanjangan.Value == null ?"-": x.TanggalPerpanjangan.Value.ToString("dd MMMM yyyy");
                                sP3 = x.NoSurat == null ? "-" : x.NoSurat;
                                d3 = x.Durasi == null ? 0 : x.Durasi;
                                EXT3 = tglP3 + "<br />" + sP3 + "<br /><b>(" + d3 + " hari)</b>";
                            }
                        }
                        var xx = (DateTime.Now - item.TanggalPenahanan.Value).Days;
                        var y = (20 + d1 + d2 + d3)-xx;
                        totalpenahanan = xx.ToString();
                        sel = item.CellCode + " sel " + item.NoSel;

                        if (y == 1)
                            sisapenahanan = "<b style='background-color: red;'>" + y.ToString() + " Hari</b>";
                        else if (y < 7)
                            sisapenahanan = "<b style='background-color: yellow;'>" + y.ToString() + " Hari</b>";
                        else
                            sisapenahanan = y.ToString() + " Hari";

                        body += "<tr><td class='tg-6qw1'>" + item.FullName + "<br />" + item.RegID + "<br />" + sel + "</td><td class='tg-6qw1'>" + item.NoSuratPenangkapan + "<br />" + item.TanggalPenangkapan.Value.ToString("dd MMMM yyyy") + "<br />" + "</td><td class='tg-6qw1'>" + item.NoSuratPenahanan + "<br />" + item.TanggalPenahanan.Value.ToString("dd MMMM yyyy") + "<br/><b> (20 Hari)</b>" + "</td><td class='tg-6qw1'>" + EXT1 + "</td> <td class='tg-6qw1'>" + EXT2 + "</td><td class='tg-6qw1'>" + EXT3 + "</td><td class='tg-6qw1'>" + totalpenahanan + " Hari </td><td class='tg-6qw1'>" + sisapenahanan + "</td>";
                        
                    }
                }
            }

            this.lblTitle.Text = "     STATUS TAHANAN";
            webBrowser.DocumentText = @"<style type='text/css'>
                                        h3 {font-family:Arial, sans-serif;}
                                        .tg  {border-collapse:collapse;border-spacing:0;}
                                        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
                                        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
                                        .tg .tg-0x09{background-color:#9b9b9b;text-align:left;vertical-align:top}
                                        .tg .tg-baqh{text-align:center;vertical-align:top}
                                        .tg .tg-ttta{background-color:#ffcb2f;border-color:#333333;text-align:center;vertical-align:top}
                                        .tg .tg-gt4d{font-weight:bold;background-color:#ffcb2f;border-color:#333333;text-align:center;vertical-align:top}
                                        .tg .tg-nro3{background-color:#ffcb2f;text-align:center;vertical-align:top}
                                        .tg .tg-0gle{font-weight:bold;background-color:#ffcb2f;text-align:center;vertical-align:top}
                                        .tg .tg-pykm{font-weight:bold;background-color:#ffcb2f;text-align:left;vertical-align:top}
                                        .tg .tg-rfuw{font-weight:bold;background-color:#9b9b9b;border-color:#656565;text-align:center;vertical-align:top}
                                        .tg .tg-6qw1{background-color:#c0c0c0;text-align:left;vertical-align:top}
                                        @media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>
                                        <div class='tg-wrap'><h3>SUMMARY STATUS TAHANAN</h1><table class='tg' width = '100%'>
                                          <tr>
                                            <th class='tg-pykm'>NO REG/NAMA/SEL</th>
                                            <th class='tg-pykm'>PENANGKAPAN</th>
                                            <th class='tg-pykm'>PENAHANAN</th>
                                            <th class='tg-pykm'>EXT 1</th>
                                            <th class='tg-pykm'>EXT 2</th>
                                            <th class='tg-pykm'>EXT 3</th>
                                            <th class='tg-pykm'>TOTAL HARI PENAHANAN</th>
                                            <th class='tg-pykm'>SISA WAKTU PENAHANAN</th>
                                          </tr>" + body+@"";


        }
        
        private void InitEvents()
        {
            //Events
            tableFormCell.CellPaint += tableLayoutPanel_CellPaint;
            print.Click += print_Click;
            this.VisibleChanged += Form_VisibleChanged;
        }

        private void print_Click(object sender, EventArgs e)
        {
            try
            {
                string strKey = "Software\\Microsoft\\Internet Explorer\\PageSetup";
                bool bolWritable = true;
                string strName = "footer";
                object oValue = Application.ProductName + "(Ver " + Application.ProductVersion + ")";
                RegistryKey oKey = Registry.CurrentUser.OpenSubKey(strKey, bolWritable);
                oKey.SetValue(strName, oValue);
                strName = "header";
                oValue = string.Empty;
                oKey = Registry.CurrentUser.OpenSubKey(strKey, bolWritable);
                oKey.SetValue(strName, oValue);
                oKey.Close();
                webBrowser.ShowPrintDialog();
            }
            catch (Exception)
            {
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                LoadData();
            }
        }
    }
}
