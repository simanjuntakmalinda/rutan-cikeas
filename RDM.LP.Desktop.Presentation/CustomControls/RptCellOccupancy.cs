﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.DataAccess.Helper;
using Microsoft.VisualBasic;
using Microsoft.Win32;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class RptCellOccupancy : Base
    {
        public RptCellOccupancy()
        {
            InitializeComponent();
            InitForm();
            LoadData();
            InitEvents();    
        }

        private void InitForm()
        {
            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     REGISTRASI PENGUNJUNG";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            webBrowser.IsWebBrowserContextMenuEnabled = false;
            webBrowser.AllowWebBrowserDrop = false;
        }


        public void LoadData()
        {

            CellAllocationService cellallocationserv = new CellAllocationService();
            var TotalWardA = cellallocationserv.GetCountByWardType(1).ToString();
            var TotalWardB = cellallocationserv.GetCountByWardType(2).ToString();
            var TotalWardC = cellallocationserv.GetCountByWardType(3).ToString();
                               
            var groupGenderA = cellallocationserv.GetCountByWardTypeGroupByGender(1);
            var groupGenderB = cellallocationserv.GetCountByWardTypeGroupByGender(2);
            var groupGenderC = cellallocationserv.GetCountByWardTypeGroupByGender(3);


            var grouReleigionA = cellallocationserv.GetCountByWardTypeGroupByReligion(1);
            var grouReleigionB = cellallocationserv.GetCountByWardTypeGroupByReligion(2);
            var grouReleigionC = cellallocationserv.GetCountByWardTypeGroupByReligion(3);



            var WAA = cellallocationserv.GetCountByWardTypeStatusAvailable(1).ToString();
            var WAO = cellallocationserv.GetCountByWardTypeStatusOccupied(1).ToString();
            var WAM = cellallocationserv.GetCountByWardTypeStatusMaintenance(1).ToString();


            var WBA = cellallocationserv.GetCountByWardTypeStatusAvailable(2).ToString();
            var WBO = cellallocationserv.GetCountByWardTypeStatusOccupied(2).ToString();
            var WBM = cellallocationserv.GetCountByWardTypeStatusMaintenance(2).ToString();


            var WCA = cellallocationserv.GetCountByWardTypeStatusAvailable(3).ToString();
            var WCO = cellallocationserv.GetCountByWardTypeStatusOccupied(3).ToString();
            var WCM = cellallocationserv.GetCountByWardTypeStatusMaintenance(3).ToString();
                        
            this.lblTitle.Text = "     SUMMARY CELL OCCUPANCY";
            webBrowser.DocumentText = @"<style type='text/css'>
                                        h3 {font-family:Arial, sans-serif;}
                                        .tg  {border-collapse:collapse;border-spacing:0;}
                                        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
                                        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
                                        .tg .tg-0x09{background-color:#9b9b9b;text-align:left;vertical-align:top}
                                        .tg .tg-baqh{text-align:center;vertical-align:top}
                                        .tg .tg-ttta{background-color:#ffcb2f;border-color:#333333;text-align:center;vertical-align:top}
                                        .tg .tg-gt4d{font-weight:bold;background-color:#ffcb2f;border-color:#333333;text-align:center;vertical-align:top}
                                        .tg .tg-nro3{background-color:#ffcb2f;text-align:center;vertical-align:top}
                                        .tg .tg-0gle{font-weight:bold;background-color:#ffcb2f;text-align:center;vertical-align:top}
                                        .tg .tg-pykm{font-weight:bold;background-color:#9b9b9b;text-align:center;vertical-align:top}
                                        .tg .tg-rfuw{font-weight:bold;background-color:#9b9b9b;border-color:#656565;text-align:center;vertical-align:top}
                                        .tg .tg-6qw1{background-color:#c0c0c0;text-align:center;vertical-align:top}
                                        @media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>
                                        <div class='tg-wrap'><h3>SUMMARY CELL OCCUPANCY</h1><table class='tg' width = '100%'>
                                          <tr>
                                            <th class='tg-0x09'></th>
                                            <th class='tg-pykm'>DITEMPATI</th>
                                            <th class='tg-pykm'>TERSEDIA</th>
                                            <th class='tg-pykm'>DALAM PEMELIHARAAN</th>
                                            <th class='tg-0gle'>TOTAL CELL</th>
                                            <th class='tg-pykm'>PRIA</th>
                                            <th class='tg-pykm'>WANITA</th>
                                            <th class='tg-pykm'>MUSLIM</th>
                                            <th class='tg-pykm'>NON MUSLIM</th>
                                          </tr>
                                          <tr>
                                            <td class='tg-rfuw'>BLOK A</td>
                                            <td class='tg-6qw1'>" + WAO + @"</td>
                                            <td class='tg-6qw1'>"+ WAA + @"</td>
                                            <td class='tg-6qw1'>" + WAM + @"</td>
                                            <td class='tg-nro3'>"+ TotalWardA + @"</td>
                                            <td class='tg-baqh'>"+ groupGenderA.ElementAtOrDefault(1).Total + @"</td>
                                            <td class='tg-baqh'>" + groupGenderA.ElementAtOrDefault(0).Total + @"</td>
                                            <td class='tg-baqh'>" + grouReleigionA.Where(x => x.Nama == "MUSLIM").Select(x=>x.Total).Sum() + @"</td>
                                            <td class='tg-baqh'>" + grouReleigionA.Where(x => x.Nama != "MUSLIM").Select(x => x.Total).Sum() + @"</td>
                                          </tr>
                                          <tr>
                                            <td class='tg-rfuw'>BLOK B</td>
                                            <td class='tg-6qw1'>" + WBO + @"</td>
                                            <td class='tg-6qw1'>" + WBA + @"</td>
                                            <td class='tg-6qw1'>" + WBM + @"</td>
                                            <td class='tg-nro3'>" + TotalWardB + @"</td>
                                            <td class='tg-baqh'>" + groupGenderB.ElementAtOrDefault(1).Total + @"</td>
                                            <td class='tg-baqh'>" + groupGenderB.ElementAtOrDefault(0).Total + @"</td>
                                            <td class='tg-baqh'>" + grouReleigionB.Where(x => x.Nama == "MUSLIM").Select(x => x.Total).Sum() + @"</td>
                                            <td class='tg-baqh'>" + grouReleigionB.Where(x => x.Nama != "MUSLIM").Select(x => x.Total).Sum() + @"</td>
                                          </tr>
                                          <tr>
                                            <td class='tg-rfuw'>BLOK C</td>
                                            <td class='tg-6qw1'>" + WCO + @"</td>
                                            <td class='tg-6qw1'>" + WCA + @"</td>
                                            <td class='tg-6qw1'>" + WCM + @"</td>
                                            <td class='tg-nro3'>" + TotalWardC + @"</td>
                                            <td class='tg-baqh'>" + groupGenderC.ElementAtOrDefault(1).Total + @"</td>
                                            <td class='tg-baqh'>" + groupGenderC.ElementAtOrDefault(0).Total + @"</td>
                                            <td class='tg-baqh'>" + grouReleigionC.Where(x => x.Nama == "MUSLIM").Select(x => x.Total).Sum() + @"</td>
                                            <td class='tg-baqh'>" + grouReleigionC.Where(x => x.Nama != "MUSLIM").Select(x => x.Total).Sum() + @"</td>
                                          </tr>
                                          <tr>
                                            <td class='tg-gt4d'>TOTAL</td>
                                            <td class='tg-ttta'>" + (Convert.ToInt32(WAO) + Convert.ToInt32(WBO) + Convert.ToInt32(WCO)).ToString() + @"</td>
                                            <td class='tg-ttta'>" + (Convert.ToInt32(WAA) + Convert.ToInt32(WBA) + Convert.ToInt32(WCA)).ToString() + @"</td>
                                            <td class='tg-ttta'>" + (Convert.ToInt32(WAM) + Convert.ToInt32(WBM) + Convert.ToInt32(WCM)).ToString() + @"</td>
                                            <td class='tg-ttta'>" + (Convert.ToInt32(TotalWardA) + Convert.ToInt32(TotalWardB) + Convert.ToInt32(TotalWardC)).ToString() + @"</td>
                                            <td class='tg-ttta'>" + (Convert.ToInt32(groupGenderA.ElementAtOrDefault(1).Total) + Convert.ToInt32(groupGenderB.ElementAtOrDefault(1).Total) + Convert.ToInt32(groupGenderC.ElementAtOrDefault(1).Total)).ToString() + @"</td>
                                            <td class='tg-ttta'>" + (Convert.ToInt32(groupGenderA.ElementAtOrDefault(0).Total) + Convert.ToInt32(groupGenderB.ElementAtOrDefault(0).Total) + Convert.ToInt32(groupGenderC.ElementAtOrDefault(0).Total)).ToString() + @"</td>
                                            <td class='tg-ttta'>" + (Convert.ToInt32(grouReleigionA.Where(x => x.Nama == "MUSLIM").Select(x => x.Total).Sum()) + Convert.ToInt32(grouReleigionB.Where(x => x.Nama == "MUSLIM").Select(x => x.Total).Sum()) + Convert.ToInt32(grouReleigionC.Where(x => x.Nama == "MUSLIM").Select(x => x.Total).Sum())).ToString() + @"</td>
                                            <td class='tg-ttta'>" + (Convert.ToInt32(grouReleigionA.Where(x => x.Nama != "MUSLIM").Select(x => x.Total).Sum()) + Convert.ToInt32(grouReleigionB.Where(x => x.Nama != "MUSLIM").Select(x => x.Total).Sum()) + Convert.ToInt32(grouReleigionC.Where(x => x.Nama != "MUSLIM").Select(x => x.Total).Sum())).ToString() + @"</td>
                                          </tr>
                                        </table></div>";

        }
        
        private void InitEvents()
        {
            //Events
            tableFormCell.CellPaint += tableLayoutPanel_CellPaint;
            print.Click += print_Click;
            this.VisibleChanged += Form_VisibleChanged;
        }

        private void print_Click(object sender, EventArgs e)
        {
            try
            {
                string strKey = "Software\\Microsoft\\Internet Explorer\\PageSetup";
                bool bolWritable = true;
                string strName = "footer";
                object oValue = Application.ProductName + "(Ver " + Application.ProductVersion + ")";
                RegistryKey oKey = Registry.CurrentUser.OpenSubKey(strKey, bolWritable);
                oKey.SetValue(strName, oValue);
                strName = "header";
                oValue = string.Empty;
                oKey = Registry.CurrentUser.OpenSubKey(strKey, bolWritable);
                oKey.SetValue(strName, oValue);
                oKey.Close();
                webBrowser.ShowPrintDialog();
            }
            catch (Exception)
            {
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                LoadData();
            }
        }
    }
}
