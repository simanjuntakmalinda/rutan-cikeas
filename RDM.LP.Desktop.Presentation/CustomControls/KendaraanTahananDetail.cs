﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using System.Threading;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class KendaraanTahananDetail : Base
    {
        public Button btncheckout { get { return this.btnCheckOut; } }
        private Point MouseDownLocation;
        private KendaraanTahanan data;
        public KendaraanTahananDetail()
        {
            InitializeComponent();
            InitForm();
            InitEvents();

        }

        private void LoadData(string id)
        {
            KendaraanTahananService bonserv = new KendaraanTahananService();
            data = bonserv.GetById(Convert.ToInt32(id));
            if (data != null)
            {
                txtKategori.Text = data.NamaKategoriKendaraan.ToUpper();
                txtNoplat.Text = data.NoPlat.ToUpper();
                txtPengemudi.Text = data.Pengemudi.ToUpper();
                txtTanggalMasuk.Text = data.TanggalMasuk.ToString("dd MMMM yyyy").ToUpper();
                txtWaktuMasuk.Text = data.WaktuMasuk.Value.ToString("HH:mm");
                txtZonaWaktu.Text = data.ZonaWaktuMasuk.ToUpper();
                txtKeperluan.Text = data.Keperluan;
                txtCatatan.Text = data.Catatan;
                txtWaktuKeluar.Text = data.WaktuKeluar == null? string.Empty: data.WaktuKeluar.Value.ToString("HH:mm");
                
                txtDibuatOleh.Text = data.CreatedBy;
                txtDibuatTanggal.Text = data.CreatedDate.Value.ToString("dd MMMM yyyy").ToUpper();
                txtDiubahOleh.Text = data.UpdatedBy;
                txtDiubahTanggal.Text = data.UpdatedDate==null?string.Empty:data.UpdatedDate.Value.ToString("dd MMMM yyyy").ToUpper();
            }
        }

        private void InitForm()
        {
            ToolTip toolTip = new ToolTip();
            toolTip.ToolTipIcon = ToolTipIcon.Info;
            toolTip.ToolTipTitle = "Info";
            toolTip.SetToolTip(exit, "Tutup Informasi Kendaraan");
            lblButton.BackColor = Global.MainColor;
            lblTitle.BackColor = Global.MainColor;
            lblTitle.Text = "      INFORMASI KENDARAAN TAHANAN";
            lblTitle.ForeColor = Color.White;
            lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;
            lblTitle.LabelElement.CustomFont = Global.MainFont;
            lblTitle.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            btnClose.ButtonElement.CustomFontStyle = FontStyle.Bold;
            btnClose.ButtonElement.CustomFont = Global.MainFont;
            btnClose.ButtonElement.CustomFontSize = Global.CustomFontSizeMain;
            txtZonaWaktu.Hide();

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
        }

        private void InitEvents()
        {
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            exit.Click += exit_Click;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            this.VisibleChanged += Form_VisibleChanged;
            btnCheckOut.Click += BtnCheckOut_Click;
            btnClose.Click += btnClose_Click;
        }

        private void BtnCheckOut_Click(object sender, EventArgs e)
        {
            KendaraanTahananService ken = new KendaraanTahananService();
            var kendata = ken.GetById(data.Id);

            if (RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "<html>Check Out Kendaraan :<br>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;" + kendata.NamaKategoriKendaraan + "<br>Pengemudi&nbsp;&nbsp;:&nbsp;&nbsp;" + kendata.Pengemudi, "Kendaraan Checkout", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes)
                if (RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "Check Out Kendaraan (" + data.NamaKategoriKendaraan + ") ?", "Kendaraan Checkout", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes)
                {
                    KendaraanTahananService kenserv = new KendaraanTahananService();
                    kenserv.CheckOutKendaraan(data.Id, GlobalVariables.UserID);
                    UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERKENDARAAN.ToString(), Activities = "CheckOut Kendaraan, Data=" + UserFunction.JsonString(data) });
                    UserFunction.MsgBox(TipeMsg.Info, "Berhasil Keluar !");
                    UserFunction.LoadDataKendaraanToGrid(MainForm.formMain.ListKendaraan.GvCell);
                    UserFunction.SetInitGridView(MainForm.formMain.ListKendaraan.GvCell);
                    LoadData(this.Tag.ToString());
                    btnCheckOut.Hide();
                }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (this.Tag != null)
                    LoadData(this.Tag.ToString());
                else
                    this.Hide();
            }
        }
    }
}
