﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms; 
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;
using System.IO;
using System.Diagnostics;
using PdfiumViewer;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class PreviewDoc : Base
    {   
        private SaveFileDialog saveFileDialog;
        public PreviewDoc()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            zoomin.Click += zoomin_Click;
            zoomout.Click += zoomout_Click;
            pdf.Click += pdfform_Click;
            word.Click += wordform_Click;
            print.Click += print_Click;
            copytext.Click += copytext_Click;
            back.Click += back_Click;
            this.VisibleChanged += Form_VisibleChanged;

        }

        private void back_Click(object sender, EventArgs e)
        {
            this.Hide();
			if (this.Tag.ToString().Split('-')[0] == "BonTahanan")
            {
                MainForm.formMain.ListBonTahanan.Show();
            }
            if (this.Tag.ToString().Split('-')[0] == "BonSerahTahanan")
            {
                MainForm.formMain.ListBonSerahTahanan.Show();
            }
            if (this.Tag.ToString().Split('-')[0] == "BonTerimaTahanan")
            {
                MainForm.formMain.ListBonTerimaTahanan.Show();
            }
        }

        private void copytext_Click(object sender, EventArgs e)
        {
            int page = pdfRenderer.Page;
            string text = string.Empty;
            for (int i = 0; i < pdfRenderer.Document.PageCount; i++)
            {
                text = text + Environment.NewLine + pdfRenderer.Document.GetPdfText(page);
            }
            Clipboard.SetText(text);
            UserFunction.MsgBox(TipeMsg.Info, "Semua Teks Telah Disalin pada Papan Klip ...!");
        }

        private void wordform_Click(object sender, EventArgs e)
        {
            InitializeSaveWordDialog((DateTime.Now.ToString("yyyyMMddhhMMss") + "List.doc"));
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                var source = Application.StartupPath + "\\templates\\temp\\inmates.html";
                var dest = saveFileDialog.FileName;
                if (File.Exists(dest))
                    File.Delete(dest);
                File.Copy(source, dest);
                UserFunction.MsgBox(TipeMsg.Info, String.Format("File {0} Berhasil Disimpan...{2}File Location: {1}", saveFileDialog.FileName.Split('\\')[saveFileDialog.FileName.Split('\\').GetUpperBound(0)], saveFileDialog.FileName, Environment.NewLine));

                Process process = new Process();
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = string.Format("WINWORD.EXE");
                startInfo.Arguments = string.Format("\"{0}\"", dest);
                process.StartInfo.UseShellExecute = false;
                process.StartInfo = startInfo;
                try
                {
                    process.Start();
                }
                catch (Exception)
                {
                }


            }
        }

        private void print_Click(object sender, EventArgs e)
        {
            using (var form = new PrintDialog())
            using (var document = pdfRenderer.Document.CreatePrintDocument(PdfPrintMode.CutMargin))
            {
                form.AllowSomePages = true;
                form.Document = document;
                form.UseEXDialog = true;
                form.Document.PrinterSettings.FromPage = 1;
                form.Document.PrinterSettings.ToPage = pdfRenderer.Document.PageCount;
                if (form.ShowDialog(FindForm()) == DialogResult.OK)
                {
                    try
                    {
                        if (form.Document.PrinterSettings.FromPage <= pdfRenderer.Document.PageCount)
                            form.Document.Print();
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void pdfform_Click(object sender, EventArgs e)
        {
            InitializeSavePDFDialog((DateTime.Now.ToString("yyyyMMddhhMMss") + "List.pdf"));
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                pdfRenderer.Document.Save(saveFileDialog.FileName);
                UserFunction.MsgBox(TipeMsg.Info, String.Format("File {0} Berhasil Disimpan...{2}File Location: {1}", saveFileDialog.FileName.Split('\\')[saveFileDialog.FileName.Split('\\').GetUpperBound(0)], saveFileDialog.FileName, Environment.NewLine));
            }
        }

        private void InitializeSaveWordDialog(string FileName)
        {
            saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = "doc";
            saveFileDialog.Filter = "word Files (*.doc)|*.doc|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 1;
            saveFileDialog.Title = "File Save Location";
            saveFileDialog.FileName = FileName;
            saveFileDialog.InitialDirectory = "C:\\";
        }

        private void InitializeSavePDFDialog(string FileName)
        {
            saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = "pdf";
            saveFileDialog.Filter = "pdf Files (*.pdf)|*.pdf|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 1;
            saveFileDialog.Title = "File Save Location";
            saveFileDialog.FileName = FileName;
            saveFileDialog.InitialDirectory = "C:\\";
        }

        private void zoomout_Click(object sender, EventArgs e)
        {
            pdfRenderer.ZoomOut();
        }

        private void zoomin_Click(object sender, EventArgs e)
        {
            pdfRenderer.ZoomIn();
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     PREVIEW DOKUMEN";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Width = 25;
            btnDetail.Name = "btnDetail";
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if(this.Tag != null)
                {
                    LoadData();
                }
            }
            else
            {

            }
        }

        private void LoadData()
        {
            if (this.pdfRenderer.Document != null)
                this.pdfRenderer.Document.Dispose();

            if (File.Exists(Application.StartupPath + "\\templates\\temp\\inmates.pdf"))
                File.Delete(Application.StartupPath + "\\templates\\temp\\inmates.pdf");

            switch (this.Tag.ToString().Split('-')[0])
            {
                case "Inmates":
                    UserFunction.GeneratePdf("Inmates", this.Tag.ToString().Split('-')[1]);
                    this.pdfRenderer.Load(PdfDocument.Load(Application.StartupPath + "\\templates\\temp\\inmates.pdf"));
                    this.pdfRenderer.ZoomMode = PdfViewerZoomMode.FitWidth;
                    zoomout_Click(zoomout ,EventArgs.Empty);
                    zoomout_Click(zoomout, EventArgs.Empty);
                    this.word.Tag = "Inmates";
                    this.pdf.Tag = "Inmates";
                    break;
                case "Visitor":
                    UserFunction.GeneratePdf("Visitor", this.Tag.ToString().Split('-')[1]);
                    this.pdfRenderer.Load(PdfDocument.Load(Application.StartupPath + "\\templates\\temp\\visitor.pdf"));
                    this.pdfRenderer.ZoomMode = PdfViewerZoomMode.FitWidth;
                    zoomout_Click(zoomout, EventArgs.Empty);
                    zoomout_Click(zoomout, EventArgs.Empty);
                    this.word.Tag = "Visitor";
                    this.pdf.Tag = "Visitor";
                    break;
				case "BonTahanan":
                    UserFunction.GeneratePdf("BonTahanan", this.Tag.ToString().Split('-')[1]);
                    this.pdfRenderer.Load(PdfDocument.Load(Application.StartupPath + "\\templates\\temp\\bontahanan.pdf"));
                    this.pdfRenderer.ZoomMode = PdfViewerZoomMode.FitWidth;
                    zoomout_Click(zoomout, EventArgs.Empty);
                    zoomout_Click(zoomout, EventArgs.Empty);
                    this.word.Tag = "BonTahanan";
                    this.pdf.Tag = "BonTahanan";
                    break;
                case "BonSerahTahanan":
                    UserFunction.GeneratePdf("BonSerahTahanan", this.Tag.ToString().Split('-')[1]);
                    this.pdfRenderer.Load(PdfDocument.Load(Application.StartupPath + "\\templates\\temp\\bonserahtahanan.pdf"));
                    this.pdfRenderer.ZoomMode = PdfViewerZoomMode.FitWidth;
                    zoomout_Click(zoomout, EventArgs.Empty);
                    zoomout_Click(zoomout, EventArgs.Empty);
                    this.word.Tag = "BonSerahTahanan";
                    this.pdf.Tag = "BonSerahTahanan";
                    break;
                case "BonTerimaTahanan":
                    UserFunction.GeneratePdf("BonTerimaTahanan", this.Tag.ToString().Split('-')[1]);
                    this.pdfRenderer.Load(PdfDocument.Load(Application.StartupPath + "\\templates\\temp\\bonterimatahanan.pdf"));
                    this.pdfRenderer.ZoomMode = PdfViewerZoomMode.FitWidth;
                    zoomout_Click(zoomout, EventArgs.Empty);
                    zoomout_Click(zoomout, EventArgs.Empty);
                    this.word.Tag = "BonTerimaTahanan";
                    this.pdf.Tag = "BonTerimaTahanan";
                    break;
            }


        }
    }
}
