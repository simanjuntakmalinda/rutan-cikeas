﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using System.Threading;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class CheckedOutInmatesConfirm : Base
    {
        private Point MouseDownLocation;
        public CheckedOutInmatesConfirm()
        {
            InitializeComponent();
            InitForm();
            InitEvents();

        }

        private void InitForm()
        {
            txtNote.Text = string.Empty;

            ToolTip toolTip = new ToolTip();
            toolTip.ToolTipIcon = ToolTipIcon.Info;
            toolTip.ToolTipTitle = "Info";
            toolTip.SetToolTip(exit, "Tutup Konfirmasi");
            txtNote.TextBoxElement.CustomFont = Global.MainFont;
            txtNote.TextBoxElement.CustomFontSize = Global.CustomFontSizeMain;

            this.btnConfirm.ButtonElement.CustomFont = Global.MainFont;
            this.btnConfirm.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            lblTitle.BackColor = Global.MainColor;
            lblTitle.Text = "      KONFIRMASI TAHANAN KELUAR";
            lblTitle.ForeColor = Color.White;
            lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;
            lblTitle.LabelElement.CustomFont = Global.MainFont;
            lblTitle.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            ddlCheckedOutDate.Value = DateTime.Today.ToLocalTime();

            //GlobalFunction.InvisibleControlDetail(tableLayoutPanel1, 4);
            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;
            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
        }

        private void InitEvents()
        {
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            exit.Click += exit_Click;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            this.VisibleChanged += Form_VisibleChanged;
            btnConfirm.Click += BtnConfirm_Click;
            btnBack.Click += BtnBack_Click;
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void BtnConfirm_Click(object sender, EventArgs e)
        {
            
            if (DataValid())
            {
                SaveData();
                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Berhasil Keluar!");
                this.Hide();
                UserFunction.LoadDataTahananCheckedOutToGrid(MainForm.formMain.DaftarTahanan.GvVisitor);
                MainForm.formMain.DaftarTahanan.GvVisitor.Refresh();
                UserFunction.LoadDataAllocationToGrid(MainForm.formMain.ListCellAllocation.GvAllocation);
                MainForm.formMain.ListCellAllocation.GvAllocation.Refresh();
                UserFunction.LoadDataCellToGrid(MainForm.formMain.ListCell.GvCell);
                MainForm.formMain.ListCell.GvCell.Refresh();
                MainForm.formMain.TahananDetailCheckedOut.Hide();
                MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
            }

            ClearData();
        }

        private void ClearData()
        {
            this.txtNote.Text = string.Empty;
            this.ddlCheckedOutDate.Value = DateTime.Today.ToLocalTime();
        }

        private bool DataValid()
        {
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;
            if (this.txtNote.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Catatan !";
                return false;
            }

            if (this.ddlCheckedOutDate.Text == string.Empty)
            {
                GlobalFunction.VisibleControlDetail(tableLayoutPanel1, 4);
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tanggal Keluar !";
                return false;
            }

            return true;
        }

        private void SaveData()
        {
            InmatesIsCheckedOutService checkout = new InmatesIsCheckedOutService();
            var checkoutinmate = checkout.GetDetailById(Convert.ToInt32(this.Tag));

            if (checkoutinmate != null)
            {
                var data = new InmatesIsCheckedOut
                {
                    Id = Convert.ToInt32(this.Tag),
                    InmatesId = checkoutinmate.InmatesId,
                    IsCheckedOut = true,
                    DateCheckedOut = ddlCheckedOutDate.Value,
                    Notes = txtNote.Text == null ? "-" : txtNote.Text,
                    UpdatedDate = DateTime.Now.ToLocalTime(),
                    UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
                };
                checkout.Update(data);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "CheckOut Inmates, InmatesId=" + data.InmatesId + ", RegID=" + data.RegID + ", InmatesName=" + data.InmatesName });

            }
        }

        private void exit_Click(object sender, EventArgs e)
        {
            ClearData();
            this.Hide();
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (this.Tag != null)
                    LoadData(Convert.ToInt32(this.Tag));
                else
                    this.Hide();
            }
        }

        private void LoadData(int id)
        {
            InmatesIsCheckedOutService serv = new InmatesIsCheckedOutService();
            var data = serv.GetDetailById(id);

            ddlCheckedOutDate.Value = DateTime.Now;
            ddlCheckedOutDate.MaxDate = DateTime.Now;
            ddlCheckedOutDate.MinDate = (DateTime)data.DateReg == null ? DateTime.Now : (DateTime)data.DateReg;
        }
    }
}
