﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;
using Telerik.WinControls.Export;
using System.IO;
using System.Threading;
using Telerik.WinControls.UI.Export;
using Telerik.Windows.Documents.Media;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListCell : Base
    {
        public RadGridView GvCell { get { return this.gvCell; } }
        public ListCell()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvCell.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvCell.RowFormatting += radGridView_RowFormatting;
            gvCell.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvCell.CellClick += gvCell_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
            picRefresh.Click += PicRefresh_Click;
        }

        private void PicRefresh_Click(object sender, EventArgs e)
        {
            UserFunction.LoadDataCellToGrid(gvCell);
            gvCell.Refresh();
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarSel.pdf", "CELL LIST DATA",gvCell);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarSel.csv", "CELL LIST DATA", gvCell);
        }

        private void gvCell_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == null)
                return;

            switch (e.Column.Index)
            {
                case 0:
                    MainForm.formMain.CellDetail.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.CellDetail.Show();
                    break;
                case 1:
                    MainForm.formMain.Cell.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.AddNew;
                    MainForm.formMain.Cell.LoadData();
                    break;
                case 2:
                    var question  = UserFunction.Confirm("Hapus Sel?");
                    if (question == DialogResult.Yes)
                    {
                        CellService cellserv = new CellService();
                        var c = cellserv.GetByCellCode(e.Row.Cells["CellCode"].Value.ToString());
                        if (c.InUseCounter.Equals(0))
                        {
                            DeleteCell(e.Row.Cells["CellCode"].Value.ToString());
                        }
                        else
                        {
                            UserFunction.MsgBox(TipeMsg.Info, "Sel tidak bisa dihapus karena masih ada tahanan!");
                        }
                    }
                    break;
            }
            
        }

        private void DeleteCell(string cellCode)
        {
            using (CellService cell = new CellService())
            {
                var data = cell.GetByCellCode(cellCode);
                if (data != null)
                {
                    CellAllocationService allserv = new CellAllocationService();
                    var allocation = allserv.GetInmatesById(data.Id);
                    if (allocation != null)
                    {
                        InmatesIsCheckedOutService checkedout = new InmatesIsCheckedOutService();
                        var outdata = checkedout.GetInmatesCheckoutCell(allocation.InmatesRegCode);
                        if( outdata != null)
                        {
                            UserFunction.MsgBox(TipeMsg.Info, "Sel tidak bisa dihapus karena masih terdaftar di DAFTAR TAHANAN!");
                            return;
                        }
                    }

                    cell.DeleteByCellCode(cellCode);
                    UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERCELL.ToString(), Activities = "Delete Master Cell, Data=" + UserFunction.JsonString(data) });

                    UserFunction.MsgBox(TipeMsg.Info, "Sel berhasil dihapus !");
                    UserFunction.LoadDataCellToGrid(gvCell);
                }
                else
                {
                    UserFunction.MsgBox(TipeMsg.Info, "Sel tidak ditemukan!");
                }
            }
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DAFTAR DATA SEL";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;
            this.lblDetail.BackColor = Color.FromArgb(77, 77, 77);
            this.lblDetail.ForeColor = Color.White;

            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Name = "btnDetail";
            gvCell.AutoGenerateColumns = false;
            gvCell.Columns.Insert(0, btnDetail);
            gvCell.Refresh();
            gvCell.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            GridViewImageColumn btnUbah = new GridViewImageColumn();
            btnUbah.HeaderText = "";
            btnUbah.Name = "btnUbah";
            gvCell.AutoGenerateColumns = false;
            gvCell.Columns.Insert(1, btnUbah);
            gvCell.Refresh();
            gvCell.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnHapus = new GridViewImageColumn();
            btnHapus.HeaderText = "";
            btnHapus.Name = "btnHapus";
            gvCell.AutoGenerateColumns = false;
            gvCell.Columns.Insert(2, btnHapus);
            gvCell.Refresh();
            gvCell.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            gvCell.EnablePaging = true;
            gvCell.PageSize = 10;

            UserFunction.LoadDataCellToGrid(gvCell);
            UserFunction.SetInitGridView(gvCell);



        }

    }
}
