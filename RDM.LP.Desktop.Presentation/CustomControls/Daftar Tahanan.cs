﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class DaftarTahanan : Base
    {
        public RadGridView GvVisitor { get { return this.gvVisit; } }
        public DaftarTahanan()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvVisit.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvVisit.RowFormatting += radGridView_RowFormatting;
            gvVisit.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvVisit.CellClick += gvVisit_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
            fingerCheckOut.Click += fingerCheckOut_Click;
        }

        private void fingerCheckOut_Click(object sender, EventArgs e)
        {
            GlobalVariables.isVisitor = false;
            MainForm.formMain.CaptureFingerPrint.Tag = "CheckOutTahanan";
            MainForm.formMain.CaptureFingerPrint.Show();
            MainForm.formMain.CaptureFingerPrint.ResetDevice();
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarTahanan.pdf", "INMATES LIST DATA", gvVisit);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarTahanan.csv", "INMATES LIST DATA", gvVisit);
        }

        private void gvVisit_CellClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Index)
            {
                case 0:
                    MainForm.formMain.TahananDetailCheckedOut.Tag = e.Row.Cells["Id"].Value.ToString(); 
                    MainForm.formMain.TahananDetailCheckedOut.Show();
                    break;
                //case 1:
                //    UserFunction.MsgBox(TipeMsg.Info,"Ubah");
                //    UpdateVisitor();
                //    break;
                //case 2:
                //    var question  = UserFunction.Confirm("Apakah Data Kunjungan Ini Akan Dihapus ?");
                //    if (question == DialogResult.Yes)
                //    { 
                //        DeleteVisitor(e.Row.Cells["Id"].Value.ToString());
                //        UserFunction.MsgBox(TipeMsg.Info, "Data Kunjungan Berhasil Dihapus !");
                //        UserFunction.LoadDataPegunjungToGrid(gvVisit);
                //    }
                //    break;
            }
            
        }

        private void UpdateVisitor()
        {
            throw new NotImplementedException();
        }

        private void DeleteVisitor(string Id)
        {
            using (VisitorService pegunjung = new VisitorService())
            {
                pegunjung.DeleteById(Id);
            }
            
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DAFTAR TAHANAN";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;
            this.lblDetail.ForeColor = Color.Navy;

            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Width = 25;
            btnDetail.Name = "btnDetail";
            gvVisit.AutoGenerateColumns = false;
            gvVisit.Columns.Insert(0, btnDetail);
            gvVisit.Refresh();
            gvVisit.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            gvVisit.AllowSearchRow = true;

            //GridViewImageColumn btnUbah = new GridViewImageColumn();
            //btnUbah.HeaderText = "";
            //btnUbah.Name = "btnUbah";
            //gvVisit.AutoGenerateColumns = false;
            //gvVisit.Columns.Insert(1, btnUbah);
            //gvVisit.Refresh();
            //gvVisit.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            //GridViewImageColumn btnHapus = new GridViewImageColumn();
            //btnHapus.HeaderText = "";
            //btnHapus.Name = "btnHapus";
            //gvVisit.AutoGenerateColumns = false;
            //gvVisit.Columns.Insert(2, btnHapus);
            //gvVisit.Refresh();
            //gvVisit.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            UserFunction.LoadDataTahananCheckedOutToGrid(gvVisit);
            UserFunction.SetInitGridView(gvVisit);

        }

    }
}
