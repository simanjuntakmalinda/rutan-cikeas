﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;
using System.IO;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class InmatesSearchData : Base
    {
        
        public RadTextBox TxtSearch { get { return this.txtSearch; } }
        public RadGridView GvData { get { return this.gvData; } }
        private Point MouseDownLocation;
        public RadGridView GvTahanan { get { return this.gvData; } }
        public InmatesSearchData()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvData.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvData.RowFormatting += radGridView_RowFormatting;
            gvData.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvData.CellClick += gvData_CellClick;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            this.VisibleChanged += Form_VisibleChanged;
            exit.Click += exit_Click;
            txtSearch.KeyPress += TxtSearch_KeyPress;
            btnSearch.Click += btnSearch_Click;
        }

        private void TxtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals((char)13))
            {
                btnSearch_Click(sender, e);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadDataToGrid();
        }

        private void gvData_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == null)
                return;
            if (e.Column.Index == 0)
            {
                InmatesService tahanan = new InmatesService();
                var datatemp = tahanan.GetImage(e.Row.Cells["RegID"].Value.ToString());
                var fingerSource = tahanan.GetFingerPrint(e.Row.Cells["RegID"].Value.ToString());
                DateTime dt = (DateTime)e.Row.Cells["DateReg"].Value;
                //MainForm.formMain.CellAllocation.RegID.Text = e.Row.Cells["RegID"].Value.ToString();
                MainForm.formMain.CellAllocation.LblRegID.Text = e.Row.Cells["RegID"].Value.ToString().ToUpper();
                MainForm.formMain.CellAllocation.FullName.Text = e.Row.Cells["FullName"].Value.ToString();
                MainForm.formMain.CellAllocation.Religion.Text = e.Row.Cells["Religion"].Value.ToString();
                MainForm.formMain.CellAllocation.Gender.Text = e.Row.Cells["Gender"].Value.ToString();
                MainForm.formMain.CellAllocation.Identity.Text = e.Row.Cells["IDType"].Value.ToString() + "/" + e.Row.Cells["IDNo"].Value.ToString();
                MainForm.formMain.CellAllocation.DateRegistered.Text = dt.ToString("dd MMMM yyyy");
                MainForm.formMain.CellAllocation.InmatesType.Text = e.Row.Cells["Type"].Value.ToString();
                MainForm.formMain.CellAllocation.InmatesCategory.Text = e.Row.Cells["Category"].Value.ToString();
                MainForm.formMain.CellAllocation.Network.Text = e.Row.Cells["Network"].Value.ToString();
                DisplayInmatesPhoto(datatemp);
                DisplayFingerPrint(fingerSource);

                this.Hide();
            }

        }

        private void DisplayFingerPrint(IEnumerable<FingerPrint> fingerSource)
        {
            foreach (var item in fingerSource)
            {
                var imageBytes = (byte[])Convert.FromBase64String(item.ImagePrint);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);

                if (item.FingerId == "R1")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, MainForm.formMain.CellAllocation.PicFingerPrint.Width, MainForm.formMain.CellAllocation.PicFingerPrint.Height);
                    /* Clear any existing image in the PictureBox. */
                    MainForm.formMain.CellAllocation.PicFingerPrint.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    MainForm.formMain.CellAllocation.PicFingerPrint.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    MainForm.formMain.CellAllocation.PicFingerPrint.Image = imgOutput;
                }
            }
        }

        private void DisplayInmatesPhoto(IEnumerable<Photos> datatemp)
        {
            foreach (var item in datatemp)
            {
                var imageBytes = (byte[])Convert.FromBase64String(item.Data);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);
                if (item.Note.ToUpper() == "FRONT")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, MainForm.formMain.CellAllocation.PicInmates1.Width, MainForm.formMain.CellAllocation.PicInmates1.Height);
                    /* Clear any existing image in the PictureBox. */
                    MainForm.formMain.CellAllocation.PicInmates1.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    MainForm.formMain.CellAllocation.PicInmates1.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    MainForm.formMain.CellAllocation.PicInmates1.Image = imgOutput;

                }
                else if (item.Note.ToUpper() == "LEFT")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, MainForm.formMain.CellAllocation.PicInmates2.Width, MainForm.formMain.CellAllocation.PicInmates2.Height);
                    /* Clear any existing image in the PictureBox. */
                    MainForm.formMain.CellAllocation.PicInmates2.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    MainForm.formMain.CellAllocation.PicInmates2.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    MainForm.formMain.CellAllocation.PicInmates2.Image = imgOutput;
                }
                else if (item.Note.ToUpper() == "RIGHT")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, MainForm.formMain.CellAllocation.PicInmates3.Width, MainForm.formMain.CellAllocation.PicInmates3.Height);
                    /* Clear any existing image in the PictureBox. */
                    MainForm.formMain.CellAllocation.PicInmates3.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    MainForm.formMain.CellAllocation.PicInmates3.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    MainForm.formMain.CellAllocation.PicInmates3.Image = imgOutput;
                }

            }
        }

        private Size getScaledImageDimensions(int width1, int height1, int width2, int height2)
        {
            /*Determine if Image is Portrait or Landscape. */
            double scaleImageMultiplier = 0;
            if (width1 > height1)    /* Image is Portrait */
            {
                /* Calculate the multiplier based on the heights. */
                if (height2 > width2)
                {
                    scaleImageMultiplier = (double)width2 / (double)width1;
                }

                else
                {
                    scaleImageMultiplier = (double)height2 / (double)height1;
                }
            }

            else /* Image is Landscape */
            {
                /* Calculate the multiplier based on the widths. */
                if (height2 >= width2)
                {
                    scaleImageMultiplier = (double)width2 / (double)width1;
                }

                else
                {
                    scaleImageMultiplier = (double)height2 / (double)height1;
                }
            }

            /* Generate and return the new scaled dimensions.

            * Essentially, we multiply each dimension of the original image
            * by the multiplier calculated above to yield the dimensions
            * of the scaled image. The scaled image can be larger or smaller
            * than the original.
            */

            return new Size((int)(width1 * scaleImageMultiplier),(int)(height1 * scaleImageMultiplier));
        }

        private void UpdateTahanan()
        {
            throw new NotImplementedException();
        }

        private void DeleteTahanan(string Id)
        {
            using (InmatesService tahanan = new InmatesService())
            {
                tahanan.DeleteById(Id);
            }
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     CARI TAHANAN";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            this.lblCari.LabelElement.CustomFont = Global.MainFont;
            this.lblCari.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            //this.ddlkategori.DropDownListElement.CustomFont = Global.MainFont;
            //this.ddlkategori.DropDownListElement.CustomFontSize = Global.CustomFontSizeMain;

            this.txtSearch.TextBoxElement.CustomFont = Global.MainFont;
            this.txtSearch.TextBoxElement.CustomFontSize = Global.CustomFontSizeMain;
            this.btnSearch.RootElement.EnableElementShadow = false;

            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Width = 15;
            btnDetail.Name = "btnDetail";
            gvData.AutoGenerateColumns = false;
            gvData.Columns.Insert(0, btnDetail);
            gvData.Refresh();
            gvData.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            GridViewImageColumn btnUbah = new GridViewImageColumn();
            btnUbah.HeaderText = "";
            btnUbah.Name = "btnUbah";
            gvData.AutoGenerateColumns = false;
            gvData.Columns.Insert(1, btnUbah);
            gvData.Refresh();
            gvData.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnHapus = new GridViewImageColumn();
            btnHapus.HeaderText = "";
            btnHapus.Name = "btnHapus";
            gvData.AutoGenerateColumns = false;
            gvData.Columns.Insert(2, btnHapus);
            gvData.Refresh();
            gvData.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            UserFunction.SetInitGridView(gvData);

            //ddlkategori.Items.Add("REG_NO");
            //ddlkategori.Items.Add("NAME");
            //ddlkategori.Items.Add("ID");
            //ddlkategori.SelectedIndex = 1;

        }

        private void LoadDataToGrid()
        {
            gvData.DataSource = null;
            
            gvData.AutoGenerateColumns = true;
            var search = txtSearch.Text.Replace("'", "`");
            if (search == string.Empty)
                return;

            InmatesService tahanan = new InmatesService();

            gvData.DataSource = tahanan.GetBySearch(search);

            gvData.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            foreach (GridViewDataColumn col in gvData.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvData.Columns[0].Width = 20;
            gvData.Columns[1].Width = 20;
            gvData.Columns[2].Width = 20;

            gvData.Columns[0].IsVisible = true;
            gvData.Columns[0].ReadOnly = false;
            gvData.Columns[0].NullValue = Resources.add_to_data;

            gvData.Columns["RegID"].IsVisible = true;
            gvData.Columns["RegID"].HeaderText = "ID DAFTAR";
            gvData.Columns["RegID"].Width = 75;

            gvData.Columns["FullName"].IsVisible = true;
            gvData.Columns["FullName"].HeaderText = "NAMA LENGKAP";
            gvData.Columns["FullName"].Width = 200;

            gvData.Columns["Gender"].IsVisible = true;
            gvData.Columns["Gender"].HeaderText = "JENIS KELAMIN";
            gvData.Columns["Gender"].Width = 50;

            gvData.Columns["IDType"].IsVisible = true;
            gvData.Columns["IDType"].HeaderText = "JENIS IDENTITAS";
            gvData.Columns["IDType"].Width = 100;

            gvData.Columns["IDNo"].IsVisible = true;
            gvData.Columns["IDNo"].HeaderText = "NO IDENTITAS";
            gvData.Columns["IDNo"].Width = 100;

            //gvData.Columns["Religion"].IsVisible = false;
            //gvData.Columns["DateReg"].IsVisible = false;
            //gvData.Columns["Type"].IsVisible = false;
            //gvData.Columns["Category"].IsVisible = false;
            //gvData.Columns["Network"].IsVisible = false;
            //gvData.Columns["Image"].IsVisible = false;
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (!this.Visible)
            {
                txtSearch.Text = string.Empty;
                gvData.DataSource = null;
                gvData.Refresh();
            }
        }
    }
}
