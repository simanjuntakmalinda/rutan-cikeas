﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;
using Telerik.WinControls.Export;
using System.IO;
using System.Threading;
using Telerik.WinControls.UI.Export;
using Telerik.Windows.Documents.Media;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListKendaraan : Base
    {
        public RadGridView GvCell { get { return this.gvKendaraan; } }
        private KendaraanTahanan data;
        public ListKendaraan()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvKendaraan.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvKendaraan.RowFormatting += radGridView_RowFormatting;
            gvKendaraan.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvKendaraan.CellClick += gvCell_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
            picRefresh.Click += PicRefresh_Click;
        }

        private void PicRefresh_Click(object sender, EventArgs e)
        {
            UserFunction.LoadDataKendaraanToGrid(gvKendaraan);
            gvKendaraan.Refresh();
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarKendaraan.pdf", "DAFTAR DATA KENDARAAN",gvKendaraan);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarKendaraan.csv", "DAFTAR DATA KENDARAAN", gvKendaraan);
        }

        private void gvCell_CellClick(object sender, GridViewCellEventArgs e)
        {
            KendaraanTahananService bonserv = new KendaraanTahananService();
            data = bonserv.GetById(Convert.ToInt32(e.Row.Cells["Id"].Value));
            if (e.Column == null)
                return;

            switch (e.Column.Index)
            {
                case 0:
                    MainForm.formMain.KendaraanTahananDetail.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.KendaraanTahananDetail.Show();
                    break;
                case 1:
                    MainForm.formMain.KendaraanTahanan.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.AddNew;
                    MainForm.formMain.KendaraanTahanan.LoadData();
                    break;
                case 2:
                    KendaraanTahananService ken = new KendaraanTahananService();
                    var kendata = ken.GetById(data.Id);

                    if (RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "<html>Check Out Kendaraan :<br>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;" + kendata.NamaKategoriKendaraan + "<br>Pengemudi&nbsp;&nbsp;:&nbsp;&nbsp;" + kendata.Pengemudi , "Kendaraan Checkout", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes)
                        if (RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "Check Out Kendaraan (" + data.NamaKategoriKendaraan + ") ?", "Kendaraan Checkout", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes)
                        {
                            KendaraanTahananService kenserv = new KendaraanTahananService();
                            kenserv.CheckOutKendaraan((int)e.Row.Cells["Id"].Value, GlobalVariables.UserID);
                            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERKENDARAAN.ToString(), Activities = "CheckOut Kendaraan, Data=" + UserFunction.JsonString(data) });
                            UserFunction.MsgBox(TipeMsg.Info, "Berhasil Keluar !");
                            UserFunction.LoadDataKendaraanToGrid(MainForm.formMain.ListKendaraan.GvCell);
                            UserFunction.SetInitGridView(MainForm.formMain.ListKendaraan.GvCell);
                            UserFunction.LoadDataKendaraanKeluarToGrid(MainForm.formMain.ListKendaraanKeluar.GvCell);
                            UserFunction.SetInitGridView(MainForm.formMain.ListKendaraanKeluar.GvCell);
                            MainForm.formMain.KendaraanTahanan.LoadData();
                        }
                    //var question  = UserFunction.Confirm("Hapus Kendaraan?");
                    //if (question == DialogResult.Yes)
                    //{
                    //    KendaraanTahananService cellallocationserv = new KendaraanTahananService();
                    //    var exist = cellallocationserv.Get();
                    //    if (exist.Count() == 0)
                    //    {
                    //        DeleteCell((int)e.Row.Cells["Id"].Value);
                    //        UserFunction.MsgBox(TipeMsg.Info, "Kendaraan berhasil dihapus !");
                    //        UserFunction.LoadDataKendaraanToGrid(gvKendaraan);
                    //    }
                    //    else
                    //    {
                    //        UserFunction.MsgBox(TipeMsg.Info, "Kendaraan tidak bisa dihapus !");
                    //    }
                    //}
                    break;
            }
            
        }

        private void DeleteCell(int id)
        {
            using (KendaraanTahananService cell = new KendaraanTahananService())
            {
                var data = cell.GetById(id);
                cell.DeleteById(id);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERCELL.ToString(), Activities = "Delete Master Cell, Data=" + UserFunction.JsonString(data) });
            }
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DAFTAR DATA KENDARAAN";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;
            this.lblDetail.BackColor = Color.FromArgb(77, 77, 77);
            this.lblDetail.ForeColor = Color.White;

            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Name = "btnDetail";
            gvKendaraan.AutoGenerateColumns = false;
            gvKendaraan.Columns.Insert(0, btnDetail);
            gvKendaraan.Refresh();
            gvKendaraan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            GridViewImageColumn btnUbah = new GridViewImageColumn();
            btnUbah.HeaderText = "";
            btnUbah.Name = "btnUbah";
            gvKendaraan.AutoGenerateColumns = false;
            gvKendaraan.Columns.Insert(1, btnUbah);
            gvKendaraan.Refresh();
            gvKendaraan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnHapus = new GridViewImageColumn();
            btnHapus.HeaderText = "";
            btnHapus.Name = "btnHapus";
            gvKendaraan.AutoGenerateColumns = false;
            gvKendaraan.Columns.Insert(2, btnHapus);
            gvKendaraan.Refresh();
            gvKendaraan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            UserFunction.LoadDataKendaraanToGrid(gvKendaraan);
            UserFunction.SetInitGridView(gvKendaraan);

        }
    }
}
