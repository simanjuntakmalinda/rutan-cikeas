﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Helper;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ChangeSettings : Base
    {
        private string dbpassword;
        private string rawpassword;
        private Point MouseDownLocation;
        public RadTextBox DasboardIntervalRefresh { get { return this.txtInterval; } }
        public RadTextBox DBDataSource { get { return this.txtDBDataSource; } }
        public RadTextBox DBUserName { get { return this.txtUserName; } }
        public RadTextBox DBPassword { get { return this.pswPassword; } }
        public RadTextBox RawPassword { get { return this.pswRawPassword; } }

        public ChangeSettings()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
            
        }

        private void InitEvents()
        {
            //Events
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            btnBack.Click += btnBack_Click;
            btnSave.Click += btnSave_Click;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            exit.Click += exit_Click;
            txtSeperator1.KeyUp += txtSeperator1_KeyUp;
            txtSeperator2.KeyUp += txtSeperator2_KeyUp;
            txtInterval.KeyUp += txtInterval_KeyUp;
            txtPagingSize.KeyUp += txtPagingSize_KeyUp;
            pswRawPassword.KeyUp += txtRawPassword_KeyUp;
            pswPassword.KeyUp += txtPassword_KeyUp;
            this.VisibleChanged += Form_VisibleChanged;
        }

        private void txtPassword_KeyUp(object sender, KeyEventArgs e)
        {
            dbpassword = pswPassword.Text;
        }

        private void txtRawPassword_KeyUp(object sender, KeyEventArgs e)
        {
            rawpassword = pswRawPassword.Text;
        }

        private void txtPagingSize_KeyUp(object sender, KeyEventArgs e)
        {
            if (!UserFunction.IsNumeric(txtPagingSize.Text))
            {
                txtPagingSize.Text = string.Empty;
            }
        }

        private void txtInterval_KeyUp(object sender, KeyEventArgs e)
        {
            if (!UserFunction.IsNumeric(txtInterval.Text))
            {
                txtInterval.Text = string.Empty;
            }
        }

        private void txtSeperator2_KeyUp(object sender, KeyEventArgs e)
        {
            if (UserFunction.IsNumeric(txtSeperator2.Text))
            {
                txtSeperator2.Text = string.Empty;
            }
        }

        private void txtSeperator1_KeyUp(object sender, KeyEventArgs e)
        {
            if (UserFunction.IsNumeric(txtSeperator1.Text))
            {
                txtSeperator1.Text = string.Empty;
            }
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void InitForm()
        {
            this.lblError.Visible = false;

            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "    " + Language.GetLanguageString(LanguangeId.SETTINGS);
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 13.5f;

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = Global.CustomFontSizeMain;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = Global.CustomFontSizeMain;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                var password1 = SimpleRSA.Encrypt(dbpassword);
                var password2 = SimpleRSA.Encrypt(rawpassword);
                UserFunction.SaveSettings(SaveSettingsType.DashboardRefresinterval, txtInterval.Text.Trim());
                UserFunction.SaveSettings(SaveSettingsType.DBDataSource, txtDBDataSource.Text.ToUpper().Trim());
                UserFunction.SaveSettings(SaveSettingsType.DBUserName, txtUserName.Text.ToUpper().Trim());
                UserFunction.SaveSettings(SaveSettingsType.DBPassword, password1);
                UserFunction.SaveSettings(SaveSettingsType.CellCodeSeperator1, txtSeperator1.Text.Trim());
                UserFunction.SaveSettings(SaveSettingsType.CellCodeSeperator2, txtSeperator2.Text.Trim());
                UserFunction.SaveSettings(SaveSettingsType.GridPagingSize, txtPagingSize.Text);
                UserFunction.SaveSettings(SaveSettingsType.RawDataPassword, password2);
                UserFunction.MsgBox(TipeMsg.Info, "Data Saved Successfully !");
                this.Hide();
            }
        }

        private void GetSettings()
        {
            dbpassword = SimpleRSA.Decrypt(Properties.Settings.Default.DBPassword);
            rawpassword = UserFunction.GetSettings(SaveSettingsType.RawDataPassword) == string.Empty ? "123456" : SimpleRSA.Decrypt(UserFunction.GetSettings(SaveSettingsType.RawDataPassword));
            txtInterval.Text =  UserFunction.GetSettings(SaveSettingsType.DashboardRefresinterval).ToString();
            txtDBDataSource.Text = UserFunction.GetSettings(SaveSettingsType.DBDataSource);
            txtUserName.Text = UserFunction.GetSettings(SaveSettingsType.DBUserName);
            txtSeperator1.Text = UserFunction.GetSettings(SaveSettingsType.CellCodeSeperator1);
            txtSeperator2.Text = UserFunction.GetSettings(SaveSettingsType.CellCodeSeperator2);
            txtPagingSize.Text = UserFunction.GetSettings(SaveSettingsType.GridPagingSize);
            pswRawPassword.Text = "PasswornyaBoonganYa";
            pswPassword.Text = "PasswornyaBoonganYa";
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private bool DataValid()
        {
            if (txtInterval.Text == string.Empty || !UserFunction.IsNumeric(txtInterval.Text))
            {
                lblError.Visible = true;
                lblError.Text = "Nilai Interval Refresh Dashboard tidak Valid !";
                return false;
            }

            if (txtDBDataSource.Text == string.Empty)
            {
                lblError.Visible = true;
                lblError.Text = "Sumber Data Basis Data tidak Valid !";
                return false;
            }

            if (txtUserName.Text == string.Empty)
            {
                lblError.Visible = true;
                lblError.Text = "Nama Pengguna Basis Data tidak Valid !";
                return false;
            }

            if (pswPassword.Text == string.Empty)
            {
                lblError.Visible = true;
                lblError.Text = "Kata Sandi Basis Data tidak Valid !";
                return false;
            }


            //if (txtSeperator1.Text == string.Empty)
            //{
            //    lblError.Visible = true;
            //    lblError.Text = "Kode Sel (Dibuat otomatis) Pemisah 1 (1 karakter) tidak valid !";
            //    return false;
            //}

            //if (txtSeperator2.Text == string.Empty)
            //{
            //    lblError.Visible = true;
            //    lblError.Text = "Kode Sel (Dibuat otomatis) Pemisah 2 (1 karakter) tidak valid !";
            //    return false;
            //}

            if (txtPagingSize.Text == string.Empty)
            {
                lblError.Visible = true;
                lblError.Text = "Nilai Ukuran Halaman Grid tidak valid !";
                return false;
            }
            if (pswRawPassword.Text == string.Empty)
            {
                lblError.Visible = true;
                lblError.Text = "Password Unduh Data Mentah (default:123456) tidak valid !";
                return false;
            }
            lblError.Visible = false;
            return true;
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                GetSettings();
            }

        }

    }
}
