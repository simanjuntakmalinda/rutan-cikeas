﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class UserDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDetail));
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            this.universitas = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.chk10 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk9 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk8 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.chk7 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.chk6 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.chk5 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk4 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk3 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk2 = new Telerik.WinControls.UI.RadCheckBox();
            this.chk1 = new Telerik.WinControls.UI.RadCheckBox();
            this.status = new Telerik.WinControls.UI.RadLabel();
            this.username = new Telerik.WinControls.UI.RadLabel();
            this.lblJenisKelamin = new Telerik.WinControls.UI.RadLabel();
            this.lblNama = new Telerik.WinControls.UI.RadLabel();
            this.lblDataUser = new Telerik.WinControls.UI.RadLabel();
            this.lblDataModul = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.lastlogin = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.updatedate = new Telerik.WinControls.UI.RadLabel();
            this.updater = new Telerik.WinControls.UI.RadLabel();
            this.inputdate = new Telerik.WinControls.UI.RadLabel();
            this.inputter = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailData = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.chk11 = new Telerik.WinControls.UI.RadCheckBox();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnClose = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.username)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJenisKelamin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataModul)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lastlogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(995, 56);
            this.lblTitle.TabIndex = 8;
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(930, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 56);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 1;
            this.exit.TabStop = false;
            // 
            // universitas
            // 
            this.universitas.AutoSize = false;
            this.universitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.universitas.Location = new System.Drawing.Point(504, 414);
            this.universitas.Margin = new System.Windows.Forms.Padding(4);
            this.universitas.Name = "universitas";
            this.universitas.Size = new System.Drawing.Size(728, 42);
            this.universitas.TabIndex = 53;
            this.universitas.Text = "Tahun Lulus";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoScroll = true;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel16, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.chk10, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.chk9, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.chk8, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.radLabel11, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.radLabel5, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.chk7, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.radLabel4, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.chk6, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.radLabel3, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.chk5, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.chk4, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.chk3, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.chk2, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.chk1, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.status, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.username, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblJenisKelamin, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblNama, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDataUser, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblDataModul, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.radLabel6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.radLabel2, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.radLabel8, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.radLabel9, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.radLabel10, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.lastlogin, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.radLabel14, 0, 20);
            this.tableLayoutPanel1.Controls.Add(this.radLabel13, 0, 19);
            this.tableLayoutPanel1.Controls.Add(this.radLabel12, 0, 18);
            this.tableLayoutPanel1.Controls.Add(this.radLabel7, 0, 17);
            this.tableLayoutPanel1.Controls.Add(this.updatedate, 1, 20);
            this.tableLayoutPanel1.Controls.Add(this.updater, 1, 19);
            this.tableLayoutPanel1.Controls.Add(this.inputdate, 1, 18);
            this.tableLayoutPanel1.Controls.Add(this.inputter, 1, 17);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailData, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.radLabel15, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.chk11, 1, 15);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 56);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 22;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(995, 580);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // radLabel16
            // 
            this.radLabel16.AutoSize = false;
            this.radLabel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel16.Location = new System.Drawing.Point(14, 714);
            this.radLabel16.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(342, 42);
            this.radLabel16.TabIndex = 146;
            this.radLabel16.Text = "<html>Modul Kendaraan</html>";
            // 
            // chk10
            // 
            this.chk10.AutoSize = false;
            this.chk10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk10.Enabled = false;
            this.chk10.Location = new System.Drawing.Point(390, 720);
            this.chk10.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk10.Name = "chk10";
            this.chk10.Size = new System.Drawing.Size(571, 37);
            this.chk10.TabIndex = 147;
            // 
            // chk9
            // 
            this.chk9.AutoSize = false;
            this.chk9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk9.Enabled = false;
            this.chk9.Location = new System.Drawing.Point(390, 670);
            this.chk9.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk9.Name = "chk9";
            this.chk9.Size = new System.Drawing.Size(571, 37);
            this.chk9.TabIndex = 145;
            // 
            // chk8
            // 
            this.chk8.AutoSize = false;
            this.chk8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk8.Enabled = false;
            this.chk8.Location = new System.Drawing.Point(390, 620);
            this.chk8.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk8.Name = "chk8";
            this.chk8.Size = new System.Drawing.Size(571, 37);
            this.chk8.TabIndex = 145;
            // 
            // radLabel11
            // 
            this.radLabel11.AutoSize = false;
            this.radLabel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel11.Location = new System.Drawing.Point(14, 664);
            this.radLabel11.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(342, 42);
            this.radLabel11.TabIndex = 144;
            this.radLabel11.Text = "<html>Modul Bon Tahanan</html>";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel5.Location = new System.Drawing.Point(14, 614);
            this.radLabel5.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(342, 42);
            this.radLabel5.TabIndex = 144;
            this.radLabel5.Text = "<html>Modul Barang Bukti</html>";
            // 
            // chk7
            // 
            this.chk7.AutoSize = false;
            this.chk7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk7.Enabled = false;
            this.chk7.Location = new System.Drawing.Point(390, 570);
            this.chk7.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk7.Name = "chk7";
            this.chk7.Size = new System.Drawing.Size(571, 37);
            this.chk7.TabIndex = 144;
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel4.Location = new System.Drawing.Point(14, 564);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(342, 42);
            this.radLabel4.TabIndex = 143;
            this.radLabel4.Text = "<html>Modul Pencetakan Form</html>";
            // 
            // chk6
            // 
            this.chk6.AutoSize = false;
            this.chk6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk6.Enabled = false;
            this.chk6.Location = new System.Drawing.Point(390, 320);
            this.chk6.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk6.Name = "chk6";
            // 
            // 
            // 
            this.chk6.RootElement.AutoSize = false;
            this.chk6.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            this.chk6.Size = new System.Drawing.Size(571, 37);
            this.chk6.TabIndex = 142;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk6.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).CheckPrimitiveStyle = Telerik.WinControls.Enumerations.CheckPrimitiveStyleEnum.Mac;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk6.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk6.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk6.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel3.Location = new System.Drawing.Point(14, 314);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(342, 42);
            this.radLabel3.TabIndex = 141;
            this.radLabel3.Text = "<html>Modul Pengunjung</html>";
            // 
            // chk5
            // 
            this.chk5.AutoSize = false;
            this.chk5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk5.Enabled = false;
            this.chk5.Location = new System.Drawing.Point(390, 520);
            this.chk5.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk5.Name = "chk5";
            this.chk5.Size = new System.Drawing.Size(571, 37);
            this.chk5.TabIndex = 140;
            // 
            // chk4
            // 
            this.chk4.AutoSize = false;
            this.chk4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk4.Enabled = false;
            this.chk4.Location = new System.Drawing.Point(390, 470);
            this.chk4.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk4.Name = "chk4";
            this.chk4.Size = new System.Drawing.Size(571, 37);
            this.chk4.TabIndex = 139;
            // 
            // chk3
            // 
            this.chk3.AutoSize = false;
            this.chk3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk3.Enabled = false;
            this.chk3.Location = new System.Drawing.Point(390, 420);
            this.chk3.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk3.Name = "chk3";
            this.chk3.Size = new System.Drawing.Size(571, 37);
            this.chk3.TabIndex = 138;
            // 
            // chk2
            // 
            this.chk2.AutoSize = false;
            this.chk2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk2.Enabled = false;
            this.chk2.Location = new System.Drawing.Point(390, 370);
            this.chk2.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk2.Name = "chk2";
            this.chk2.Size = new System.Drawing.Size(571, 37);
            this.chk2.TabIndex = 137;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk2.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chk1
            // 
            this.chk1.AutoSize = false;
            this.chk1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk1.Enabled = false;
            this.chk1.Location = new System.Drawing.Point(390, 270);
            this.chk1.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk1.Name = "chk1";
            // 
            // 
            // 
            this.chk1.RootElement.AutoSize = false;
            this.chk1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            this.chk1.Size = new System.Drawing.Size(571, 37);
            this.chk1.TabIndex = 136;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).CheckPrimitiveStyle = Telerik.WinControls.Enumerations.CheckPrimitiveStyleEnum.Mac;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // status
            // 
            this.status.AutoSize = false;
            this.status.Dock = System.Windows.Forms.DockStyle.Fill;
            this.status.Location = new System.Drawing.Point(364, 114);
            this.status.Margin = new System.Windows.Forms.Padding(4);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(596, 42);
            this.status.TabIndex = 59;
            // 
            // username
            // 
            this.username.AutoSize = false;
            this.username.Dock = System.Windows.Forms.DockStyle.Fill;
            this.username.Location = new System.Drawing.Point(364, 64);
            this.username.Margin = new System.Windows.Forms.Padding(4);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(596, 42);
            this.username.TabIndex = 57;
            // 
            // lblJenisKelamin
            // 
            this.lblJenisKelamin.AutoSize = false;
            this.lblJenisKelamin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblJenisKelamin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblJenisKelamin.Location = new System.Drawing.Point(14, 114);
            this.lblJenisKelamin.Margin = new System.Windows.Forms.Padding(4);
            this.lblJenisKelamin.Name = "lblJenisKelamin";
            this.lblJenisKelamin.Size = new System.Drawing.Size(342, 42);
            this.lblJenisKelamin.TabIndex = 18;
            this.lblJenisKelamin.Text = "STATUS PENGGUNA";
            // 
            // lblNama
            // 
            this.lblNama.AutoSize = false;
            this.lblNama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNama.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNama.Location = new System.Drawing.Point(14, 64);
            this.lblNama.Margin = new System.Windows.Forms.Padding(4);
            this.lblNama.Name = "lblNama";
            this.lblNama.Size = new System.Drawing.Size(342, 42);
            this.lblNama.TabIndex = 10;
            this.lblNama.Text = "NAMA PENGGUNA";
            // 
            // lblDataUser
            // 
            this.lblDataUser.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDataUser, 2);
            this.lblDataUser.Cursor = System.Windows.Forms.Cursors.No;
            this.lblDataUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDataUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDataUser.Image = ((System.Drawing.Image)(resources.GetObject("lblDataUser.Image")));
            this.lblDataUser.Location = new System.Drawing.Point(14, 14);
            this.lblDataUser.Margin = new System.Windows.Forms.Padding(4);
            this.lblDataUser.Name = "lblDataUser";
            this.lblDataUser.Size = new System.Drawing.Size(946, 42);
            this.lblDataUser.TabIndex = 62;
            this.lblDataUser.Text = "       DETAIL PENGGUNA";
            // 
            // lblDataModul
            // 
            this.lblDataModul.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDataModul, 2);
            this.lblDataModul.Cursor = System.Windows.Forms.Cursors.No;
            this.lblDataModul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDataModul.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDataModul.Image = ((System.Drawing.Image)(resources.GetObject("lblDataModul.Image")));
            this.lblDataModul.Location = new System.Drawing.Point(14, 214);
            this.lblDataModul.Margin = new System.Windows.Forms.Padding(4);
            this.lblDataModul.Name = "lblDataModul";
            this.lblDataModul.Size = new System.Drawing.Size(946, 42);
            this.lblDataModul.TabIndex = 83;
            this.lblDataModul.Text = "       DETAIL MODUL PENGGUNA";
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel6.Location = new System.Drawing.Point(14, 264);
            this.radLabel6.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(342, 42);
            this.radLabel6.TabIndex = 133;
            this.radLabel6.Text = "<html>Modul Tahanan</html>";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel2.Location = new System.Drawing.Point(14, 364);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(342, 42);
            this.radLabel2.TabIndex = 132;
            this.radLabel2.Text = "<html>Modul Alokasi Sel</html>";
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel8.Location = new System.Drawing.Point(14, 414);
            this.radLabel8.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(342, 42);
            this.radLabel8.TabIndex = 131;
            this.radLabel8.Text = "<html>Modul Pengelolaan Pengguna</html>";
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel9.Location = new System.Drawing.Point(14, 464);
            this.radLabel9.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(342, 42);
            this.radLabel9.TabIndex = 134;
            this.radLabel9.Text = "<html>Modul Pelaporan</html>";
            // 
            // radLabel10
            // 
            this.radLabel10.AutoSize = false;
            this.radLabel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel10.Location = new System.Drawing.Point(14, 514);
            this.radLabel10.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(342, 42);
            this.radLabel10.TabIndex = 135;
            this.radLabel10.Text = "<html>Modul Audit Trail</html>";
            // 
            // lastlogin
            // 
            this.lastlogin.AutoSize = false;
            this.lastlogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lastlogin.Location = new System.Drawing.Point(364, 164);
            this.lastlogin.Margin = new System.Windows.Forms.Padding(4);
            this.lastlogin.Name = "lastlogin";
            this.lastlogin.Size = new System.Drawing.Size(596, 42);
            this.lastlogin.TabIndex = 82;
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel1.Location = new System.Drawing.Point(14, 164);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(342, 42);
            this.radLabel1.TabIndex = 81;
            this.radLabel1.Text = "TERAKHIR LOGIN";
            // 
            // radLabel14
            // 
            this.radLabel14.AutoSize = false;
            this.radLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel14.Location = new System.Drawing.Point(14, 1014);
            this.radLabel14.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(342, 42);
            this.radLabel14.TabIndex = 74;
            this.radLabel14.Text = "TANGGAL UBAH";
            // 
            // radLabel13
            // 
            this.radLabel13.AutoSize = false;
            this.radLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel13.Location = new System.Drawing.Point(14, 964);
            this.radLabel13.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(342, 42);
            this.radLabel13.TabIndex = 73;
            this.radLabel13.Text = "DIUBAH OLEH";
            // 
            // radLabel12
            // 
            this.radLabel12.AutoSize = false;
            this.radLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel12.Location = new System.Drawing.Point(14, 914);
            this.radLabel12.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(342, 42);
            this.radLabel12.TabIndex = 72;
            this.radLabel12.Text = "TANGGAL BUAT";
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel7.Location = new System.Drawing.Point(14, 864);
            this.radLabel7.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(342, 42);
            this.radLabel7.TabIndex = 68;
            this.radLabel7.Text = "DIBUAT OLEH";
            // 
            // updatedate
            // 
            this.updatedate.AutoSize = false;
            this.updatedate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updatedate.Location = new System.Drawing.Point(364, 1014);
            this.updatedate.Margin = new System.Windows.Forms.Padding(4);
            this.updatedate.Name = "updatedate";
            this.updatedate.Size = new System.Drawing.Size(596, 42);
            this.updatedate.TabIndex = 79;
            // 
            // updater
            // 
            this.updater.AutoSize = false;
            this.updater.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updater.Location = new System.Drawing.Point(364, 964);
            this.updater.Margin = new System.Windows.Forms.Padding(4);
            this.updater.Name = "updater";
            this.updater.Size = new System.Drawing.Size(596, 42);
            this.updater.TabIndex = 80;
            // 
            // inputdate
            // 
            this.inputdate.AutoSize = false;
            this.inputdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputdate.Location = new System.Drawing.Point(364, 914);
            this.inputdate.Margin = new System.Windows.Forms.Padding(4);
            this.inputdate.Name = "inputdate";
            this.inputdate.Size = new System.Drawing.Size(596, 42);
            this.inputdate.TabIndex = 78;
            // 
            // inputter
            // 
            this.inputter.AutoSize = false;
            this.inputter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputter.Location = new System.Drawing.Point(364, 864);
            this.inputter.Margin = new System.Windows.Forms.Padding(4);
            this.inputter.Name = "inputter";
            this.inputter.Size = new System.Drawing.Size(596, 42);
            this.inputter.TabIndex = 77;
            // 
            // lblDetailData
            // 
            this.lblDetailData.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailData, 2);
            this.lblDetailData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDetailData.Image = ((System.Drawing.Image)(resources.GetObject("lblDetailData.Image")));
            this.lblDetailData.Location = new System.Drawing.Point(14, 814);
            this.lblDetailData.Margin = new System.Windows.Forms.Padding(4);
            this.lblDetailData.Name = "lblDetailData";
            this.lblDetailData.Size = new System.Drawing.Size(946, 42);
            this.lblDetailData.TabIndex = 71;
            this.lblDetailData.Text = "        LOG DATA";
            // 
            // radLabel15
            // 
            this.radLabel15.AutoSize = false;
            this.radLabel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel15.Location = new System.Drawing.Point(14, 764);
            this.radLabel15.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(342, 42);
            this.radLabel15.TabIndex = 145;
            this.radLabel15.Text = "<html>Modul Jadwal Kunjungan</html>";
            // 
            // chk11
            // 
            this.chk11.AutoSize = false;
            this.chk11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk11.Enabled = false;
            this.chk11.Location = new System.Drawing.Point(390, 770);
            this.chk11.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk11.Name = "chk11";
            this.chk11.Size = new System.Drawing.Size(571, 37);
            this.chk11.TabIndex = 146;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnClose);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 640);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(995, 122);
            this.lblButton.TabIndex = 87;
            // 
            // btnClose
            // 
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(30, 30);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(244, 62);
            this.btnClose.TabIndex = 28;
            this.btnClose.Tag = "";
            this.btnClose.Text = "&TUTUP";
            this.btnClose.ThemeName = "MaterialBlueGrey";
            // 
            // UserDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.lblTitle);
            this.Margin = new System.Windows.Forms.Padding(15);
            this.Name = "UserDetail";
            this.Size = new System.Drawing.Size(995, 762);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.username)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJenisKelamin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataModul)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lastlogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel universitas;
        private System.Windows.Forms.PictureBox exit;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel lblJenisKelamin;
        private Telerik.WinControls.UI.RadLabel lblNama;
        private Telerik.WinControls.UI.RadLabel lblDataUser;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel lblDetailData;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel lblDataModul;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnClose;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadCheckBox chk7;
        private Telerik.WinControls.UI.RadCheckBox chk6;
        private Telerik.WinControls.UI.RadCheckBox chk5;
        private Telerik.WinControls.UI.RadCheckBox chk4;
        private Telerik.WinControls.UI.RadCheckBox chk3;
        private Telerik.WinControls.UI.RadCheckBox chk2;
        private Telerik.WinControls.UI.RadCheckBox chk1;
        private Telerik.WinControls.UI.RadLabel status;
        private Telerik.WinControls.UI.RadLabel username;
        private Telerik.WinControls.UI.RadLabel lastlogin;
        private Telerik.WinControls.UI.RadLabel updatedate;
        private Telerik.WinControls.UI.RadLabel updater;
        private Telerik.WinControls.UI.RadLabel inputdate;
        private Telerik.WinControls.UI.RadLabel inputter;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadCheckBox chk9;
        private Telerik.WinControls.UI.RadCheckBox chk8;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadCheckBox chk11;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadCheckBox chk10;
    }
}
