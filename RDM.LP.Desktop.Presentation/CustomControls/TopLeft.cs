﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class TopLeft : UserControl
    {
        public RadLabel UserName { get { return this.lblUserName; } }
        public RadLabel LastLogin { get { return this.lblLastLogin; } }
        public PictureBox Logo { get { return this.pictureBox1; } }
        public TopLeft()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblLastLogin_Click(object sender, EventArgs e)
        {

        }
    }
}
