﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class CameraSource
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CameraSource));
            this.lblTitle = new System.Windows.Forms.Panel();
            this.exit = new System.Windows.Forms.PictureBox();
            this.lblHeader = new Telerik.WinControls.UI.RadLabel();
            this.lblSource = new Telerik.WinControls.UI.RadLabel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.OK = new Telerik.WinControls.UI.RadButton();
            this.ddlSourceCamera = new Telerik.WinControls.UI.RadDropDownList();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSourceCamera)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Controls.Add(this.lblHeader);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(581, 49);
            this.lblTitle.TabIndex = 0;
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(521, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 49);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 2;
            this.exit.TabStop = false;
            // 
            // lblHeader
            // 
            this.lblHeader.ForeColor = System.Drawing.Color.White;
            this.lblHeader.Image = ((System.Drawing.Image)(resources.GetObject("lblHeader.Image")));
            this.lblHeader.Location = new System.Drawing.Point(14, 12);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(150, 30);
            this.lblHeader.TabIndex = 0;
            this.lblHeader.Text = "        PENGATURAN KAMERA";
            // 
            // lblSource
            // 
            this.lblSource.AutoSize = false;
            this.lblSource.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSource.Location = new System.Drawing.Point(0, 49);
            this.lblSource.Margin = new System.Windows.Forms.Padding(4);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(581, 42);
            this.lblSource.TabIndex = 11;
            this.lblSource.Text = "    Pilih Sumber Kamera:";
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.OK);
            this.radPanel1.Controls.Add(this.ddlSourceCamera);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel1.Location = new System.Drawing.Point(0, 96);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Padding = new System.Windows.Forms.Padding(25);
            this.radPanel1.Size = new System.Drawing.Size(581, 90);
            this.radPanel1.TabIndex = 12;
            // 
            // OK
            // 
            this.OK.BackColor = System.Drawing.Color.White;
            this.OK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.OK.Dock = System.Windows.Forms.DockStyle.Right;
            this.OK.Image = ((System.Drawing.Image)(resources.GetObject("OK.Image")));
            this.OK.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.OK.Location = new System.Drawing.Point(510, 25);
            this.OK.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.OK.Name = "OK";
            this.OK.Padding = new System.Windows.Forms.Padding(7);
            this.OK.Size = new System.Drawing.Size(46, 40);
            this.OK.TabIndex = 172;
            this.OK.ThemeName = "MaterialBlueGrey";
            // 
            // ddlSourceCamera
            // 
            this.ddlSourceCamera.BackColor = System.Drawing.Color.White;
            this.ddlSourceCamera.Dock = System.Windows.Forms.DockStyle.Left;
            this.ddlSourceCamera.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlSourceCamera.DropDownHeight = 500;
            this.ddlSourceCamera.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlSourceCamera.Location = new System.Drawing.Point(25, 25);
            this.ddlSourceCamera.Name = "ddlSourceCamera";
            // 
            // 
            // 
            this.ddlSourceCamera.RootElement.CustomFont = "Roboto";
            this.ddlSourceCamera.RootElement.CustomFontSize = 13F;
            this.ddlSourceCamera.Size = new System.Drawing.Size(477, 40);
            this.ddlSourceCamera.TabIndex = 143;
            this.ddlSourceCamera.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlSourceCamera.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlSourceCamera.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlSourceCamera.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlSourceCamera.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlSourceCamera.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlSourceCamera.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlSourceCamera.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlSourceCamera.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlSourceCamera.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // CameraSource
            // 
            this.Controls.Add(this.radPanel1);
            this.Controls.Add(this.lblSource);
            this.Controls.Add(this.lblTitle);
            this.Name = "CameraSource";
            this.Size = new System.Drawing.Size(581, 186);
            this.lblTitle.ResumeLayout(false);
            this.lblTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSourceCamera)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel lblTitle;
        private Telerik.WinControls.UI.RadLabel lblHeader;
        private System.Windows.Forms.PictureBox exit;
        private Telerik.WinControls.UI.RadLabel lblSource;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadDropDownList ddlSourceCamera;
        private Telerik.WinControls.UI.RadButton OK;
    }
}
