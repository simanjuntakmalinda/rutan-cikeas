﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.Desktop.Presentation;
using AForge.Video;
using AForge.Video.DirectShow;
using Telerik.WinControls.Primitives;
using System.IO;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormVisitorVIP : Base
    {
        private VisitorVIP OldData;
        public FormVisitorVIP()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
            LoadData();
        }

        public void LoadData()
        {
            this.lblTitle.Text = "     TAMBAH DATA PENGUNJUNG VIP BARU";

            ClearData();

            JenisIdentitasService jnsidentitas = new JenisIdentitasService();
            this.ddlJnsIdentitas.DisplayMember = "Nama";
            this.ddlJnsIdentitas.ValueMember = "Nama";
            this.ddlJnsIdentitas.DataSource = jnsidentitas.Get();

            if (this.Tag != null)
            {
                VisitorVIPService visitorVIPServ = new VisitorVIPService();
                var data = visitorVIPServ.GetDetailById(Convert.ToString(this.Tag));
                if (data == null)
                {
                    ddlJnsIdentitas.SelectedIndex = 0;
                    ddlJnsIdentitas.Enabled = true;
                    return;
                }
                else
                {
                    MainForm.formMain.AddNew.Text = "UBAH DATA PENGUNJUNG VIP";
                    this.lblTitle.Text = "     UBAH DATA PENGUNJUNG VIP";
                    OldData = data;

                    ddlJnsIdentitas.SelectedValue = data.IDType;
                    txtNoIdentitas.Text = data.IDNo;
                    txtNama.Text = data.FullName;
                    txtAddress.Text = data.Address;
                    txtPurpose.Text = data.Purpose;
                    txtNotes.Text = data.Notes;
                }
            }
            ddlJnsIdentitas_SelectedIndexChanged(ddlJnsIdentitas, null);
        }

        private void InitEvents()
        {
            //Events
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            this.VisibleChanged += Form_VisibleChanged;
            txtNoIdentitas.TextChanged += TxtNoIdentitas_TextChanged;
            txtNoIdentitas.LostFocus += txtNoIdentitas_LostFocus;
            ddlJnsIdentitas.SelectedIndexChanged += ddlJnsIdentitas_SelectedIndexChanged;
            ddlJnsIdentitas.KeyDown += Control_KeyDown;
            txtNoIdentitas.KeyDown += Control_KeyDown;
            txtNama.KeyDown += Control_KeyDown;
            txtPurpose.KeyDown += Control_KeyDown;
            txtNotes.KeyDown += Control_KeyDown;
            //ddlMulai.KeyDown += Control_KeyDown;
            //ddlMulaiJam.KeyDown += Control_KeyDown;
            //ddlSelesai.KeyDown += Control_KeyDown;
            //ddlSelesaiJam.KeyDown += Control_KeyDown;

            radPanel14.KeyDown += Control_KeyDown;
            radPanel15.KeyDown += Control_KeyDown;
            radPanel17.KeyDown += Control_KeyDown;
            radPanel18.KeyDown += Control_KeyDown;
            radPanel19.KeyDown += Control_KeyDown;

            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }

        private void ddlJnsIdentitas_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (ddlJnsIdentitas.SelectedValue != null)
            {
                if (ddlJnsIdentitas.SelectedValue.ToString() == "1")
                {
                    txtNoIdentitas.MaxLength = 16;
                    while (txtNoIdentitas.TextLength > 16)
                    {
                        txtNoIdentitas.Text = txtNoIdentitas.Text.Remove(txtNoIdentitas.Text.Length - 1);
                    }
                }
                else if (ddlJnsIdentitas.SelectedValue.ToString() == "2")
                {
                    txtNoIdentitas.MaxLength = 12;
                    while (txtNoIdentitas.TextLength > 12)
                    {
                        txtNoIdentitas.Text = txtNoIdentitas.Text.Remove(txtNoIdentitas.Text.Length - 1);
                    }
                }
                else if (ddlJnsIdentitas.SelectedValue.ToString() == "3")
                {
                    txtNoIdentitas.MaxLength = 16;
                    while (txtNoIdentitas.TextLength > 16)
                    {
                        txtNoIdentitas.Text = txtNoIdentitas.Text.Remove(txtNoIdentitas.Text.Length - 1);
                    }
                }
                else if (ddlJnsIdentitas.SelectedValue.ToString() == "4")
                {
                    txtNoIdentitas.Text = "--";
                    txtNoIdentitas.Enabled = false;
                }
            }
        }

        private void TxtNoIdentitas_TextChanged(object sender, EventArgs e)
        {
            if (ddlJnsIdentitas.SelectedValue != null)
            {
                if (ddlJnsIdentitas.SelectedValue.ToString() != "3")
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(txtNoIdentitas.Text, "[^0-9]"))
                    {
                        txtNoIdentitas.Text = txtNoIdentitas.Text.Remove(txtNoIdentitas.Text.Length - 1);
                    }
                }
            }
        }

        public void LoadDataByRegNo(string id)
        {
            VisitorService visitorserv = new VisitorService();
            var data = visitorserv.GetDetailById(id);
            LoadDataVisitor(data);
            txtNoIdentitas.Focus();
        }


        private void txtNoIdentitas_LostFocus(object sender, EventArgs e)
        {
            var idtype = ddlJnsIdentitas.SelectedItem.Text;
            var idno = txtNoIdentitas.Text;
            VisitorService visitorserv = new VisitorService();

            var data = visitorserv.GetDetailByIdentity(idtype,idno);
            if(data != null)
            {
                LoadDataVisitor(data);
            }

            //ddlMulaiJam.Value = DateTime.Now;
        }

        public void ClearData()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            txtNama.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtPurpose.Text = string.Empty;
            txtNotes.Text = string.Empty;
        }

        private void LoadDataVisitor(Visitor data)
        {
            ddlJnsIdentitas.SelectedText = data.IDType;
            txtNoIdentitas.Text = data.IDNo;
            txtNama.Text = data.FullName;
            txtPurpose.Text = data.Address;
            txtNotes.Text = data.City;
        }

        private void InitForm()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     VISITOR VIP REGISTRATION FORM";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;
            this.lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 14f;
            this.lblDetailSurat.ForeColor = Color.White;
            this.lblDetailSurat.BackColor = Color.FromArgb(77, 77, 77);

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailVisitor.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailVisitor.LabelElement.CustomFontSize = 15.5f;

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);

            //ddlMulai.Format = DateTimePickerFormat.Custom;
            //ddlMulai.CustomFormat = "dd MMM yyyy";

            //ddlMulaiJam.Format = DateTimePickerFormat.Custom;
            //ddlMulaiJam.CustomFormat = "HH:mm";
            //ddlMulaiJam.ShowUpDown = true;

            //ddlSelesai.Format = DateTimePickerFormat.Custom;
            //ddlSelesai.CustomFormat = "dd MMM yyyy";

            //ddlSelesaiJam.Format = DateTimePickerFormat.Custom;
            //ddlSelesaiJam.CustomFormat = "HH:mm";
            //ddlSelesaiJam.ShowUpDown = true;

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ClearData();
            this.Tag = null;
            MainForm.formMain.AddNew.Text = "TAMBAH PENGUNJUNG VIP BARU";
            this.lblTitle.Text = "     TAMBAH DATA PENGUNJUNG VIP BARU";
            UserFunction.ClearControls(tableLayoutPanel1);
            MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                if (this.Tag == null)
                    SaveData();
                else
                    UpdatedData();

                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Disimpan!");
                UserFunction.LoadDataPegunjungVIPToGrid(MainForm.formMain.ListVisitorVIP.GvVisitorVIP);
                UserFunction.LoadDataKunjunganVIPToGrid(MainForm.formMain.ListKunjunganVIP.GvKunjungan);
                MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
                MainForm.formMain.AddNew.Text = "TAMBAH PENGUNJUNG VIP BARU";
                this.lblTitle.Text = "     VISITOR VIP REGISTRATION FORM";
                this.Tag = null;
            }
        }

        private void SaveData()
        {
            VisitorVIPService visitorVIP = new VisitorVIPService();
            var data = new VisitorVIP
            {
                FullName = txtNama.Text,
                IDType = ddlJnsIdentitas.Text,
                IDNo = txtNoIdentitas.Text,
                Address = txtAddress.Text,
                Purpose = txtPurpose.Text,
                Notes = txtNotes.Text,
                StartVisit = DateTime.Now,
                IsCheckedOut = false,
                CreatedDate = DateTime.Now.ToLocalTime(),
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
            };

            visitorVIP.Post(data);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.VISITORVISIT.ToString(), Activities = "Add New VisitorVIP, Data=" + UserFunction.JsonString(data) });
        }

        private void UpdatedData()
        {
            VisitorVIPService visitorVIP = new VisitorVIPService();
            var data = new VisitorVIP
            {
                Id = Convert.ToInt32(this.Tag),
                FullName = txtNama.Text,
                IDType = ddlJnsIdentitas.Text,
                IDNo = txtNoIdentitas.Text,
                Address = txtAddress.Text,
                Purpose = txtPurpose.Text,
                Notes = txtNotes.Text,
                UpdatedDate = DateTime.Now.ToLocalTime(),
                UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
            };

            visitorVIP.Update(data);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.VISITORVISIT.ToString(), Activities = "Update VisitorVIP, Old Data=" + UserFunction.JsonString(OldData) + ",  New Data=" + UserFunction.JsonString(data) });
        }

        private bool DataValid()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            if (this.ddlJnsIdentitas.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Pilih Jenis Identitas!";
                return false;
            }

            if (this.txtNoIdentitas.Text == string.Empty )
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi No Identitas!";
                return false;
            }
            if (this.txtNama.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Nama Lengkap!";
                return false;
            }
            if (this.txtAddress.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Alamat!";
                return false;
            }
            if (this.txtPurpose.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tujuan Kunjungan!";
                return false;
            }
            if (this.txtNotes.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Catatan!";
                return false;
            }
            return true;
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {

            }
            else
            {

            }
        }
    }
}
