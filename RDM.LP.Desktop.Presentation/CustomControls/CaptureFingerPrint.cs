﻿using System;
using System.Drawing;
using System.Windows.Forms;
using libzkfpcsharp;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO;
using RDM.LP.DataAccess.Service;
using RDM.LP.DataAccess.ViewModel;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class CaptureFingerPrint : UserControl
    {
        IntPtr mDevHandle = IntPtr.Zero;
        IntPtr mDBHandle = IntPtr.Zero;
        IntPtr FormHandle = IntPtr.Zero;
        IntPtr dbHandle = IntPtr.Zero;
        bool bIsTimeToDie = false;
        byte[] FPBuffer;
        int indexDevice = -1;
        int ret = zkfp.ZKFP_ERR_OK;

        bool isRegister = false;
        
        byte[] RegTmp = new byte[2048];
        byte[] CapTmp = new byte[2048];
        int cbCapTmp = 2048;

        private int mfpWidth = 0;
        private int mfpHeight = 0;
        const int MESSAGE_CAPTURED_OK = 0x0400 + 6;

        private Point MouseDownLocation;

        [DllImport("user32.dll", EntryPoint = "SendMessageA")]
        public static extern int SendMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);
                
        public CaptureFingerPrint()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            this.Load += CaptureFingerPrint_Load;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            exit.Click += exit_Click;
            btnRefresh.Click += btnRefresh_Click;
            this.VisibleChanged += Form_VisibleChanged;
        }

        private void InitDevice()
        {
            if ((ret = zkfp2.Init()) == zkfperrdef.ZKFP_ERR_OK)
            {
                int nCount = zkfp2.GetDeviceCount();
                if (nCount > 0)
                {
                    indexDevice = 0;
                    lblMessage.Text = "Perangkat Tersambung!";
                }
                else
                {
                    zkfp2.Terminate();
                    lblMessage.Text = "Tidak Ada Perangkat Tersambung!";
                }
            }
            else
            {
                lblMessage.Text = "Tidak Ada Perangkat Tersambung!";
            }
        }

        private void OpenDevice(int _indexDevice)
        {
            ClearFingerPrint();

            if (IntPtr.Zero == (mDevHandle = zkfp2.OpenDevice(_indexDevice)))
            {
                lblMessage.Text = "Gagal Membuka Perangkat!";
                return;
            }
            if (IntPtr.Zero == (mDBHandle = zkfp2.DBInit()))
            {
                //MessageBox.Show("Init DB fail");
                zkfp2.CloseDevice(mDevHandle);
                mDevHandle = IntPtr.Zero;
                return;
            }
            
            byte[] paramValue = new byte[4];
            int size = 4;
            zkfp2.GetParameters(mDevHandle, 1, paramValue, ref size);
            zkfp2.ByteArray2Int(paramValue, ref mfpWidth);

            size = 4;
            zkfp2.GetParameters(mDevHandle, 2, paramValue, ref size);
            zkfp2.ByteArray2Int(paramValue, ref mfpHeight);

            FPBuffer = new byte[mfpWidth * mfpHeight];

            Thread captureThread = new Thread(new ThreadStart(DoCapture));
            captureThread.IsBackground = true;
            captureThread.Start();
            bIsTimeToDie = false;
            lblMessage.Text = "Siap Memindai!";


            for (int i = 0; i < 10; i++)
            {
                GlobalVariables.RegTmps[i] = new byte[2048];
            }
        }

        private void DoCapture()
        {
            try
            {
                while (!bIsTimeToDie)
                {
                    cbCapTmp = 2048;
                    int ret = zkfp2.AcquireFingerprint(mDevHandle, FPBuffer, CapTmp, ref cbCapTmp);

                    if (ret == zkfp.ZKFP_ERR_OK)
                    {
                        SendMessage(FormHandle, MESSAGE_CAPTURED_OK, IntPtr.Zero, IntPtr.Zero);
                    }
                    Thread.Sleep(300);
                }
            }
            catch (Exception Ex)
            {
            }
        }

        private void InitForm()
        {
            base.CreateParams.ExStyle |= 0x20;
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            this.BackColor = Color.FromArgb(0x80, 0xFF, 0xCC, 0x33);
        }

        protected override void DefWndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case MESSAGE_CAPTURED_OK:

                    MemoryStream ms = new MemoryStream();
                    BitmapFormat.GetBitmap(FPBuffer, mfpWidth, mfpHeight, ref ms);
                    Bitmap bmp = new Bitmap(ms);
                    byte[] byteImage = ms.ToArray();
                    string base64String = Convert.ToBase64String(byteImage);
                    this.imgFingerPrint.Image = bmp;

                    int ret = zkfp.ZKFP_ERR_OK;
                    int fid = 0, score = 0;
                    ret = zkfp2.DBIdentify(mDBHandle, CapTmp, ref fid, ref score);

                    if (GlobalVariables.isVisitor == false) {

                        /////////////////////////////////////////////////// INMATE ///////////////////////////////////////////////////////////

                        FingerPrintService fpserv = new FingerPrintService();
                        IEnumerable<FingerPrint> data = fpserv.GetInmates();
                        IEnumerable<FingerPrint> dataout = null;

                        int countDB = 0;
                        int countCH = 0;
                        int countCHIndex = 0;
                        int InmateId = 0;
                        string InmateRegID = "";
                        string InmateName = "";


                        if (this.Tag != null)
                        {
                            if (this.Tag.ToString() == "CheckOutTahanan")
                            {
                                dataout = fpserv.GetNotCheckeoutInmatesFingerPrint();

                                foreach (FingerPrint d in dataout)
                                {
                                    GlobalVariables.RegTmps[0] = (byte[])Convert.FromBase64String(d.ByteImagePrint);
                                    if (zkfp2.DBMatch(mDBHandle, CapTmp, GlobalVariables.RegTmps[0]) > 0)
                                    {
                                        InmateId = d.InmateId;
                                        InmateRegID = d.RegID;
                                        InmateName = d.FullName;
                                        countDB++;
                                    }
                                }

                                if (countDB > 0)
                                {
                                    InmatesIsCheckedOutService outserv = new InmatesIsCheckedOutService();
                                    var datax = outserv.GetInmatesCheckoutCell(InmateRegID);

                                    if (RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "<html>Check Out Visitor :<br>NAMA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;" + InmateName + "<br>NO. REGISTER&nbsp;&nbsp;:&nbsp;&nbsp;" + InmateRegID, "Visitor Checkout", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes)
                                    {
                                        MainForm.formMain.TahananDetailCheckedOut.btnCheckOutx.Tag = InmateRegID;
                                        if (MainForm.formMain.TahananDetailCheckedOut.InmateCanCheckedOut())
                                        {
                                            this.Hide();
                                            ClearFingerPrint();
                                            MainForm.formMain.CheckedOutInmates.Tag = datax.Id;
                                            MainForm.formMain.CheckedOutInmates.Show();
                                        }
                                    }
                                }
                                else
                                {
                                    UserFunction.MsgBox(TipeMsg.Info, "Data Tidak Ditemukan!");
                                    ClearFingerPrint();
                                    ResetDevice();
                                    return;
                                }
                            }
                            this.Tag = null;
                            ClearFingerPrint();
                            return;
                        }

                        if (MainForm.formMain.FingerPrint_Inmate.Tag != null)
                        {
                            if (MainForm.formMain.Tahanan.temp == true)
                            {
                                GlobalVariables.RegisterCount = Convert.ToInt32(MainForm.formMain.FingerPrint_Inmate.Tag);
                            }
                            MainForm.formMain.Tahanan.temp = false;
                            InsertFingerPrint(countCHIndex, countCH, base64String, bmp);
                        }
                        else
                        {

                            foreach (FingerPrint d in data)
                            {
                                GlobalVariables.RegTmps[0] = (byte[])Convert.FromBase64String(d.ByteImagePrint);
                                if (zkfp2.DBMatch(mDBHandle, CapTmp, GlobalVariables.RegTmps[0]) > 0)
                                {
                                    InmateId = d.InmateId;
                                    InmateRegID = d.RegID;
                                    InmateName = d.FullName;
                                    countDB++;
                                }
                            }

                            if (GlobalVariables.RegisterCount + 1 > GlobalVariables.REGISTER_FINGER_COUNT && GlobalVariables.PanelAktif == 0)
                            {
                                return;
                            }

                            if (countDB > 0)
                            {
                                //sudah pernah terdaftar, dan belum keluar dari daftar tahanan
                                InmatesIsCheckedOutService checkedoutserv = new InmatesIsCheckedOutService();
                                var checkedout = checkedoutserv.GetInmatesCheckoutCell(InmateRegID);
                                if (checkedout != null)
                                {
                                    RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "Tahanan sudah Terdaftar di DAFTAR TAHANAN, Silakan Check Out Terlebih Dahulu", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
                                    ClearFingerPrint();
                                    ResetDevice();
                                    return;
                                }

                                isRegister = false;

                                ClearFingerPrint();
                                this.Hide();

                                //MainForm.formMain.FingerPrint_Inmate.CleanFingerPrint_Inmate();
                                MainForm.formMain.FingerPrint_Inmate.Hide();
                                MainForm.formMain.Tahanan.LoadDataById(InmateId.ToString());
                            }
                            else
                            {
                                isRegister = true;
                            }

                            if (isRegister)
                            {
                                if (GlobalVariables.RegisterCount == -1)
                                {
                                    this.Hide();
                                    //MainForm.formMain.FingerPrint_Inmate.CleanFingerPrint_Inmate();
                                    MainForm.formMain.FingerPrint_Inmate.Show();

                                    MainForm.formMain.FingerPrint_Inmate.RightThumb_Click();

                                    for (int i = 0; i < GlobalVariables.REGISTER_FINGER_COUNT; i++)
                                    {
                                        GlobalVariables.RegisterList.Insert(i, null);
                                        GlobalVariables.FPImageList.Insert(i, null);
                                    }

                                    GlobalVariables.RegisterCount++;
                                    return;
                                }

                                InsertFingerPrint(countCHIndex, countCH, base64String, bmp);

                                return;
                            }
                        }
                    }
                    else
                    {
                        if (MainForm.formMain.Visit.ubahfingerprint == true)
                        {
                            var question = RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "Ubah Sidik Jari?", "Visitor", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                            if (question == DialogResult.Yes)
                            {
                                var fingerprint = new FingerPrint
                                {
                                    ImagePrint = base64String,
                                    ByteImagePrint = Convert.ToBase64String(CapTmp),
                                    FingerId = String.Empty,
                                    IsVisitor = true,
                                    CreatedDate = DateTime.Now.ToLocalTime(),
                                    CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
                                };
                                MainForm.formMain.Visit.FingerPrint = fingerprint;
                                MainForm.formMain.Visit.FingerPicture.Image = imgFingerPrint.Image;
                                MainForm.formMain.Visit.IdNo.Focus();
                                ClearFingerPrint();
                                this.Hide();

                            }
                            else
                            {
                                ClearFingerPrint();
                                return;
                            }

                            MainForm.formMain.Visit.ubahfingerprint = false;
                            return;
                        }

                        FingerPrintService fpserv = new FingerPrintService();
                        IEnumerable<FingerPrint> data = fpserv.GetVisitor();
                        if (this.Tag != null) { 
                            if (this.Tag.ToString() == "CheckOutVisit")
                            {
                                data = fpserv.GetNotCheckeoutVisitorFingerPrint();
                            }
                        }
                        int countDB = 0;

                        int VisitorId = 0;
                        string VisitorRegID = "";
                        string VisitorName = "";
                        string VisitorIDType = "";
                        string VisitorIDNo = "";


                        foreach (FingerPrint d in data)
                        {
                            if (d.ByteImagePrint != null){
                                GlobalVariables.RegTmps[0] = (byte[])Convert.FromBase64String(d.ByteImagePrint);
                                if (zkfp2.DBMatch(mDBHandle, CapTmp, GlobalVariables.RegTmps[0]) > 0)
                                {
                                    VisitorId = d.VisitorId;
                                    VisitorRegID = d.RegID;
                                    VisitorName = d.FullName;
                                    countDB++;
                                }
                            }
                        }

                        if (countDB > 0 && VisitorId != 0)
                        {
                            if (this.Tag != null)
                            {
                                if (this.Tag.ToString() == "CheckOutVisit")
                                {
                                    VisitorService visitor = new VisitorService();
                                    var visitordata = visitor.GetDetailById(VisitorId.ToString());

                                    if (RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "<html>Check Out Visitor :<br>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;" + VisitorName + "<br>ID Type/No&nbsp;&nbsp;:&nbsp;&nbsp;" + visitordata.IDType + "/" + visitordata.IDNo, "Visitor Checkout", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes)
                                    {
                                        VisitService visitserv = new VisitService();
                                        visitserv.CheckOutVisitorByVisitorId(visitordata.Id, GlobalVariables.UserID);
                                        UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.VISITORCHECKOUT.ToString(), Activities = "CheckOut Visitor, VisitorId=" + VisitorId + ", RegID=" + VisitorRegID + ", VisitorName=" + VisitorName });
                                        UserFunction.LoadDataKunjunganToGrid(MainForm.formMain.ListKunjungan.GvVisitor);
                                        UserFunction.SetInitGridView(MainForm.formMain.ListKunjungan.GvVisitor);
                                        UserFunction.MsgBox(TipeMsg.Info, "Berhasil Keluar!");
                                        this.Tag = string.Empty;
                                    }
                                    isRegister = false;
                                    ClearFingerPrint();
                                    this.Hide();
                                    return;
                                }
                            }

                            MainForm.formMain.Visit.LoadDataByRegNo(VisitorId.ToString());
                            isRegister = false;
                            ClearFingerPrint();
                            this.Hide();
                        }
                        else
                        {
                            if (this.Tag != null)
                            {
                                if (this.Tag.ToString() == "CheckOutVisit")
                                {
                                    if (VisitorId != 0)
                                    {
                                        UserFunction.MsgBox(TipeMsg.Error, "Data tidak Ditemukan!");
                                        ClearFingerPrint();
                                        ResetDevice();
                                        return;
                                    }
                                }
                            }
                            isRegister = true;
                        }

                        if (isRegister)
                        {
                            var question = RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "Data Tidak Ditemukan, Lanjutkan untuk Registrasi?", "Visitor", MessageBoxButtons.YesNo, RadMessageIcon.Question);
                            if (question == DialogResult.Yes)
                            {
                                var fingerprint = new FingerPrint
                                {
                                    ImagePrint = base64String,
                                    ByteImagePrint = Convert.ToBase64String(CapTmp),
                                    FingerId = String.Empty,
                                    IsVisitor = true,
                                    CreatedDate = DateTime.Now.ToLocalTime(),
                                    CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
                                };
                                MainForm.formMain.Visit.FingerPrint = fingerprint;
                                MainForm.formMain.Visit.FingerPicture.Image = imgFingerPrint.Image;
                                MainForm.formMain.Visit.IdNo.Focus(); 
                                ClearFingerPrint();
                                this.Hide();

                            }
                            else
                            {
                                ClearFingerPrint();
                                return;
                            }

                            return;
                        }

                        GlobalVariables.isVisitor = false;

                    }
                    break;

                default:
                    base.DefWndProc(ref m);
                    break;
            }
        }

        public void InsertFingerPrint(int countCHIndex, int countCH, string base64String, Bitmap bmp)
        {
            for (int x = 0; x < GlobalVariables.REGISTER_FINGER_COUNT; x++)
            {
                if (GlobalVariables.RegisterCount > 0 && zkfp2.DBMatch(mDBHandle, CapTmp, GlobalVariables.RegTmps[x]) > 0)
                {
                    countCHIndex = x + 1;
                    countCH++;
                    break;
                }
            }

            if (countCH > 0 && countCHIndex != GlobalVariables.PanelAktif)
            {
                MainForm.formMain.FingerPrint_Inmate.Message.Text = "MOHON TEKAN JARI LAIN";
                return;
            }

            Array.Copy(CapTmp, GlobalVariables.RegTmps[GlobalVariables.PanelAktif - 1], cbCapTmp);
            String strBase64 = zkfp2.BlobToBase64(CapTmp, cbCapTmp);
            byte[] blob = zkfp2.Base64ToBlob(strBase64);

            GlobalVariables.RegisterList[GlobalVariables.PanelAktif - 1] = Convert.ToBase64String(CapTmp);
            GlobalVariables.FPImageList[GlobalVariables.PanelAktif - 1] = base64String;

            if (GlobalVariables.RegisterCount >= 0 && GlobalVariables.RegisterCount <= GlobalVariables.REGISTER_FINGER_COUNT)
            {
                if (GlobalVariables.PanelAktif == 1)
                {
                    if (MainForm.formMain.FingerPrint_Inmate.RightThumbFinger.Image == null)
                    {
                        GlobalVariables.RegisterCount++;
                    }

                    MainForm.formMain.FingerPrint_Inmate.RightThumbFinger.Image = bmp;
                    MainForm.formMain.FingerPrint_Inmate.RightIndexFinger_Click();
                }
                else if (GlobalVariables.PanelAktif == 2)
                {
                    if (MainForm.formMain.FingerPrint_Inmate.RightIndexFinger.Image == null)
                    {
                        GlobalVariables.RegisterCount++;
                    }

                    MainForm.formMain.FingerPrint_Inmate.RightIndexFinger.Image = bmp;
                    MainForm.formMain.FingerPrint_Inmate.RightMiddleFinger_Click();
                }
                else if (GlobalVariables.PanelAktif == 3)
                {
                    if (MainForm.formMain.FingerPrint_Inmate.RightMiddleFinger.Image == null)
                    {
                        GlobalVariables.RegisterCount++;
                    }

                    MainForm.formMain.FingerPrint_Inmate.RightMiddleFinger.Image = bmp;
                    MainForm.formMain.FingerPrint_Inmate.RightRingFinger_Click();
                }
                else if (GlobalVariables.PanelAktif == 4)
                {
                    if (MainForm.formMain.FingerPrint_Inmate.RightRingFinger.Image == null)
                    {
                        GlobalVariables.RegisterCount++;
                    }

                    MainForm.formMain.FingerPrint_Inmate.RightRingFinger.Image = bmp;
                    MainForm.formMain.FingerPrint_Inmate.RightLittleFinger_Click();
                }
                else if (GlobalVariables.PanelAktif == 5)
                {
                    if (MainForm.formMain.FingerPrint_Inmate.RightLittleFinger.Image == null)
                    {
                        GlobalVariables.RegisterCount++;
                    }

                    MainForm.formMain.FingerPrint_Inmate.RightLittleFinger.Image = bmp;
                    MainForm.formMain.FingerPrint_Inmate.LeftThumbFinger_Click();
                }
                else if (GlobalVariables.PanelAktif == 6)
                {
                    if (MainForm.formMain.FingerPrint_Inmate.LeftThumbFinger.Image == null)
                    {
                        GlobalVariables.RegisterCount++;
                    }

                    MainForm.formMain.FingerPrint_Inmate.LeftThumbFinger.Image = bmp;
                    MainForm.formMain.FingerPrint_Inmate.LeftIndexFinger_Click();
                }
                else if (GlobalVariables.PanelAktif == 7)
                {
                    if (MainForm.formMain.FingerPrint_Inmate.LeftIndexFinger.Image == null)
                    {
                        GlobalVariables.RegisterCount++;
                    }

                    MainForm.formMain.FingerPrint_Inmate.LeftIndexFinger.Image = bmp;
                    MainForm.formMain.FingerPrint_Inmate.LeftMiddleFinger_Click();
                }
                else if (GlobalVariables.PanelAktif == 8)
                {
                    if (MainForm.formMain.FingerPrint_Inmate.LeftMiddleFinger.Image == null)
                    {
                        GlobalVariables.RegisterCount++;
                    }

                    MainForm.formMain.FingerPrint_Inmate.LeftMiddleFinger.Image = bmp;
                    MainForm.formMain.FingerPrint_Inmate.LeftRingFinger_Click();
                }
                else if (GlobalVariables.PanelAktif == 9)
                {
                    if (MainForm.formMain.FingerPrint_Inmate.LeftRingFinger.Image == null)
                    {
                        GlobalVariables.RegisterCount++;
                    }

                    MainForm.formMain.FingerPrint_Inmate.LeftRingFinger.Image = bmp;
                    MainForm.formMain.FingerPrint_Inmate.LeftLittleFinger_Click();
                }
                else if (GlobalVariables.PanelAktif == 10)
                {
                    if (MainForm.formMain.FingerPrint_Inmate.LeftLittleFinger.Image == null)
                    {
                        GlobalVariables.RegisterCount++;
                    }

                    MainForm.formMain.FingerPrint_Inmate.LeftLittleFinger.Image = bmp;
                }
            }

            if (GlobalVariables.RegisterCount == GlobalVariables.REGISTER_FINGER_COUNT)
            {
                MainForm.formMain.FingerPrint_Inmate.EnableSaveButton();
                MainForm.formMain.FingerPrint_Inmate.Message.Text = "KLIK TOMBOL LANJUTKAN UNTUK MELANJUTKAN";
            }
        }

        private void CaptureFingerPrint_Load(object sender, EventArgs e)
        {
            FormHandle = this.Handle;
        }

        private void exit_Click(object sender, EventArgs e)
        {
            if (MainForm.formMain.Visit.ubahfingerprint == true)
            {
                MainForm.formMain.Visit.ubahfingerprint = false;
                ClearFingerPrint();
                this.Hide();
                return;
            }

            ClearFingerPrint();
            this.Hide();
            MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        public void ClearFingerPrint()
        {
            GlobalVariables.RegisterCount = -1;
            lblMessage.Text = "Siap Memindai!";
            this.imgFingerPrint.Image = null;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ResetDevice();
            UserFunction.MsgBox(TipeMsg.Info, "Hubungkan Ulang Berhasil!");
        }

        public void ResetDevice()
        {
            cbCapTmp = 2048;
            zkfp2.CloseDevice(mDevHandle);
            zkfp2.Terminate();
            InitDevice();
            OpenDevice(indexDevice);
        }

        public void CloseDevice()
        {
            zkfp2.CloseDevice(mDevHandle);
            mDevHandle = IntPtr.Zero;
            zkfp2.Terminate();
        }


        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (MainForm.formMain.FingerPrint_Inmate.Tag == null && MainForm.formMain.Visit.ubahfingerprint == false)
                {
                    MainForm.formMain.Visit.ClearDataVisitor();
                    MainForm.formMain.Tahanan.ClearDataInmates();
                    MainForm.formMain.Tahanan.Network.Hide();
                    MainForm.formMain.Opacity.Show();
                }

                this.Location = new Point(MainForm.formMain.Width / 2 - this.Size.Width / 2,
                    MainForm.formMain.Height / 2 - this.Size.Height / 2);
                
            }
            else
            {
                CloseDevice();
                if (MainForm.formMain != null) { 
                    MainForm.formMain.CaptureFingerPrint.Tag = null;
                    MainForm.formMain.Opacity.Hide();
                }
            }
        }

    }
}
