﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.Desktop.Presentation;
using AForge.Video;
using AForge.Video.DirectShow;
using Telerik.WinControls.Primitives;
using System.IO;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormBonTahanan : Base
    {
        public RadDropDownList DDLKeperluanBon { get { return this.ddlKeperluanBon; } }
        //public RadDropDownList DdlTahanan { get { return this.ddlNama; } }
        public RadTextBox DaftarTahanan { get { return this.txtListTahanan; } }

        private BonTahanan OldData;

        public FormBonTahanan()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
            LoadData();
        }

        public void LoadData()
        {
            this.lblTitle.Text = "     TAMBAH BON TAHANAN";

            ClearForm();

            rbPersonel.CheckState = CheckState.Checked;
            rbJaksa.CheckState = CheckState.Unchecked;

            if (this.Tag != null)
            {
                this.lblTitle.Text = "     EDIT BON TAHANAN";
                MainForm.formMain.AddNew.Text = "EDIT BON TAHANAN";

                BonTahananService serv = new BonTahananService();
                InmateBonTahananService iboserv = new InmateBonTahananService();
                var data = serv.GetById(Convert.ToInt32(this.Tag));
                var datainmate = iboserv.GetByBonTahananId(Convert.ToInt32(this.Tag));

                if (data == null)
                {
                    ddlKeperluanBon.SelectedIndex = 0;
                    ddlPangkatPemohon.SelectedIndex = 0;
                    ddlPangkatPJ.SelectedIndex = 0;
                    ddlPangkatDisetujui.SelectedIndex = 0;
                    return;
                }
                else
                {
                    OldData = data;

                    txtNoBonTahanan.Text = data.NoBonTahanan;
                    //tahananId.Text = data.RegID;
                    //LoadDataNama(datainmate);
                    //ddlKeperluanBon.SelectedIndex = Convert.ToInt32(data.Keperluan);
                    
                    LoadDataTahanan(datainmate);
                    ddlKeperluanBon.SelectedValue = Convert.ToInt32(data.Keperluan);

					dpTanggalMulai.Value = data.TanggalMulai != null ? (DateTime)data.TanggalMulai : DateTime.Now.ToLocalTime();
                    dpTanggalSelesai.Value = data.TanggalSelesai != null ? (DateTime)data.TanggalSelesai : DateTime.Now.ToLocalTime();
                    txtLokasi.Text = data.Lokasi;
                    txtKeterangan.Text = data.Keterangan;

                    if (data.TipePemohon == "Jaksa")
                    {
                        rbJaksa.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        rbPersonel.CheckState = CheckState.Checked;
                        ddlPangkatPemohon.SelectedValue = Convert.ToInt32(data.PangkatPemohon);
                        txtSatkerPemohon.Text = data.SatkerPemohon;
                    }
                    txtNamaPemohon.Text = data.NamaPemohon;
                    txtNrpPemohon.Text = data.NrpPemohon;
                    //ddlPangkatPemohhon.SelectedIndex = Convert.ToInt32(data.PangkatPemohon);
                    txtNohpPemohon.Text = data.NoHpPemohon;

                    txtNamaPJ.Text = data.NamaPenanggungJawab;
                    txtNrpPJ.Text = data.NrpPenanggungJawab;
                    //ddlPangkatPJ.SelectedIndex = Convert.ToInt32(data.PangkatPenanggungJawab);
					ddlPangkatPJ.SelectedValue = Convert.ToInt32(data.PangkatPenanggungJawab);
					txtSatkerPJ.Text = data.SatkerPenanggungJawab;
                    txtNohpPJ.Text = data.NoHpPenanggungJawab;

                    txtNamaDisetujui.Text = data.NamaPenyetuju;
                    txtNrpDisetujui.Text = data.NrpPenyetuju;
					//ddlPangkatDisetujui.SelectedIndex = Convert.ToInt32(data.PangkatPenyetuju);
					ddlPangkatDisetujui.SelectedValue = Convert.ToInt32(data.PangkatPenyetuju);
					txtSatkerDisetujui.Text = data.SatkerPenyetuju;
                }
            }
        }

        private void CheckStateChanged_click(object sender, EventArgs e)
        {
            txtNamaPemohon.Text = string.Empty;
            txtNrpPemohon.Text = string.Empty;
            ddlPangkatPemohon.SelectedIndex = 0;
            txtSatkerPemohon.Text = string.Empty;
            if (rbPersonel.CheckState == CheckState.Checked)
            {
                ddlPangkatPemohon.Show();
                radPanelPangkatPemohon.Show();
                txtSatkerPemohon.Show();
                radPanelSatkerPemohon.Show();
                lblNrpPemohon.Text = "NRP / Pangkat / Satuan Kerja *";
            }
            else
            {
                ddlPangkatPemohon.Hide();
                radPanelPangkatPemohon.Hide();
                txtSatkerPemohon.Hide();
                radPanelSatkerPemohon.Hide();
                lblNrpPemohon.Text = "NIP *";
            }
        }

        //private void LoadDataNama(IEnumerable<GetInmateBontahanan> datainmate)
        //{
        //    foreach (var _data in datainmate)
        //    {
        //        foreach (var _item in ddlNama.Items)
        //        {
        //            if (((RadCheckedListDataItem)_item).Text == _data.NamaLengkap)
        //            {
        //                ((RadCheckedListDataItem)_item).Checked = true;
        //            }
        //        }
        //    }
        //}

        private void LoadDataTahanan(IEnumerable<GetInmateBontahanan> datainmate)
        {
            var count = datainmate.Count();
            var regid = String.Empty;
            var i = 0;
            if (count == 1) {
                foreach (var _data in datainmate)
                {
                    regid = _data.RegID;
                }
            }
            else
            {
                foreach (var _data in datainmate)
                {
                    if (i == 0)
                    {
                        regid = _data.RegID;
                        i++;
                    }
                    else
                    {
                        regid = regid + ';' + _data.RegID;
                    }
                }
            }

            txtListTahanan.Text = regid;
        }

        public void ClearForm()
        {
            //dpTanggalMulai.Value = Convert.ToDateTime(null);
            //dpTanggalSelesai.Value = Convert.ToDateTime(null);

            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            //ddlNama
            //InmatesService inserv = new InmatesService();
            //ddlNama.Items.Clear();
            //ddlNama.DisplayMember = "FullName";
            //ddlNama.ValueMember = "RegID";
            //ddlNama.DataSource = inserv.GetAllNameTahanan();

            txtListTahanan.Text = String.Empty;

            //ddl KeperluanBon
            KeperluanBonTahananService keperluanserv = new KeperluanBonTahananService();
            ddlKeperluanBon.Items.Clear();
            ddlKeperluanBon.DisplayMember = "Nama";
            ddlKeperluanBon.ValueMember = "Id";
            ddlKeperluanBon.DataSource = keperluanserv.Get();

            //ddl KeperluanBon
            PangkatPolisiService pangkatserv = new PangkatPolisiService();
            ddlPangkatPemohon.Items.Clear();
            ddlPangkatPemohon.DisplayMember = "Nama";
            ddlPangkatPemohon.ValueMember = "Id";
            ddlPangkatPemohon.DataSource = pangkatserv.Get();

            //ddl KeperluanBon
            ddlPangkatPJ.Items.Clear();
            ddlPangkatPJ.DisplayMember = "Nama";
            ddlPangkatPJ.ValueMember = "Id";
            ddlPangkatPJ.DataSource = pangkatserv.Get();

            //ddl KeperluanBon
            ddlPangkatDisetujui.Items.Clear();
            ddlPangkatDisetujui.DisplayMember = "Nama";
            ddlPangkatDisetujui.ValueMember = "Id";
            ddlPangkatDisetujui.DataSource = pangkatserv.Get();

            ddlKeperluanBon.SelectedIndex = -1;
            ddlPangkatPemohon.SelectedIndex = -1;
            ddlPangkatPJ.SelectedIndex = -1;
            ddlPangkatDisetujui.SelectedIndex = -1;

        }

        private void InitEvents()
        {
            //Events
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            this.VisibleChanged += Form_VisibleChanged;
            btnAddKeperluan.Click += BtnAddKeperluan_Click;
            rbPersonel.CheckStateChanged += CheckStateChanged_click;
            rbJaksa.CheckStateChanged += CheckStateChanged_click;
            btnSearchTahanan.Click += btnSearchTahanan_Click;
            btnClearTahanan.Click += btnClearTahanan_Click;

            //ddlNama.KeyDown += Control_KeyDown;
            //ddlNama.KeyDown += Control_KeyDown;

            txtNoBonTahanan.KeyDown += Control_KeyDown;
            //ddlNama.KeyDown += Control_KeyDown;
            ddlKeperluanBon.KeyDown += Control_KeyDown;
            ddlPangkatPemohon.KeyDown += Control_KeyDown;
            ddlPangkatPJ.KeyDown += Control_KeyDown;
            ddlPangkatDisetujui.KeyDown += Control_KeyDown;
            txtLokasi.KeyDown += Control_KeyDown;
            txtKeterangan.KeyDown += Control_KeyDown;
            txtNamaPemohon.KeyDown += Control_KeyDown;
            txtNrpPemohon.KeyDown += Control_KeyDown;
            ddlPangkatPemohon.KeyDown += Control_KeyDown;
            txtSatkerPemohon.KeyDown += Control_KeyDown;
            txtNohpPemohon.KeyDown += Control_KeyDown;
            txtNamaPJ.KeyDown += Control_KeyDown;
            txtNrpPJ.KeyDown += Control_KeyDown;
            ddlPangkatPJ.KeyDown += Control_KeyDown;
            txtSatkerPJ.KeyDown += Control_KeyDown;
            txtNohpPJ.KeyDown += Control_KeyDown;
            txtNamaDisetujui.KeyDown += Control_KeyDown;
            txtNrpDisetujui.KeyDown += Control_KeyDown;
            ddlPangkatDisetujui.KeyDown += Control_KeyDown;
            txtSatkerDisetujui.KeyDown += Control_KeyDown;
            btnSearchTahanan.KeyDown += Control_KeyDown;
            btnClearTahanan.KeyDown += Control_KeyDown;

            radPanelSatkerPemohon.KeyDown += Control_KeyDown;
            radPanel2.KeyDown += Control_KeyDown;
            radPanel3.KeyDown += Control_KeyDown;
            radPanel4.KeyDown += Control_KeyDown;
            radPanel5.KeyDown += Control_KeyDown;
            radPanel6.KeyDown += Control_KeyDown;
            radPanel7.KeyDown += Control_KeyDown;
            radPanel8.KeyDown += Control_KeyDown;
            radPanelNoHp.KeyDown += Control_KeyDown;
            radPanel10.KeyDown += Control_KeyDown;
            radPanel11.KeyDown += Control_KeyDown;
            radPanelPangkatPemohon.KeyDown += Control_KeyDown;
            radPanel13.KeyDown += Control_KeyDown;
            radPanel14.KeyDown += Control_KeyDown;
            radPanel15.KeyDown += Control_KeyDown;
            radPanel16.KeyDown += Control_KeyDown;
            radPanel17.KeyDown += Control_KeyDown;
            radPanel18.KeyDown += Control_KeyDown;
            radPanel19.KeyDown += Control_KeyDown;
            radPanel20.KeyDown += Control_KeyDown;
            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;

            btnSave.KeyDown += Control_KeyDown;

            txtNohpPemohon.KeyUp += TxtNohpPemohon_KeyUp;
            txtNohpPJ.KeyUp += TxtNohpPJ_KeyUp;
            txtNrpPemohon.KeyUp += TxtNrpPemohon_KeyUp;
            txtNrpDisetujui.KeyUp += TxtNrpDisetujui_KeyUp;
            txtNrpPJ.KeyUp += TxtNrpPJ_KeyUp;
        }
        
        private void BtnAddKeperluan_Click(object sender, EventArgs e)
        {
            MainForm.formMain.FormKeperluanBonTahanan.ClearForm();
            MainForm.formMain.FormKeperluanBonTahanan.Show();
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }

        private void TxtNrpPJ_KeyUp(object sender, KeyEventArgs e)
        {
            if (!UserFunction.IsNumeric(txtNrpPJ.Text))
            {
                txtNrpPJ.Text = string.Empty;
            }
        }

        private void TxtNrpDisetujui_KeyUp(object sender, KeyEventArgs e)
        {
            if (!UserFunction.IsNumeric(txtNrpDisetujui.Text))
            {
                txtNrpDisetujui.Text = string.Empty;
            }
        }

        private void TxtNrpPemohon_KeyUp(object sender, KeyEventArgs e)
        {
            if (!UserFunction.IsNumeric(txtNrpPemohon.Text))
            {
                txtNrpPemohon.Text = string.Empty;
            }
        }

        private void TxtNohpPemohon_KeyUp(object sender, KeyEventArgs e)
        {
            if (!UserFunction.IsNumeric(txtNohpPemohon.Text))
            {
                txtNohpPemohon.Text = string.Empty;
            }
        }

        private void TxtNohpPJ_KeyUp(object sender, KeyEventArgs e)
        {
            if (!UserFunction.IsNumeric(txtNohpPJ.Text))
            {
                txtNohpPJ.Text = string.Empty;
            }
        }

        private void btnSearchTahanan_Click(object sender, EventArgs e)
        {
            MainForm.formMain.SearchData.NamaForm = "FormBonTahanan";
            MainForm.formMain.SearchData.Tag = SearchType.Tahanan.ToString();
            MainForm.formMain.SearchData.Show();
        }

        private void btnClearTahanan_Click(object sender, EventArgs e)
        {
            txtListTahanan.Text = String.Empty;
        }

        private void InitForm()
        {
            this.lblError.Visible = false;
            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;
            this.btnSearchTahanan.RootElement.EnableElementShadow = false;
            this.btnClearTahanan.RootElement.EnableElementShadow = false;

            this.lblTitle.Text = "     FORM BON TAHANAN";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;
            this.lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 14f;
            this.lblDetailSurat.ForeColor = Color.White;
            this.lblDetailSurat.BackColor = Color.FromArgb(77, 77, 77);

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailVisitor.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailVisitor.LabelElement.CustomFontSize = 15.5f;

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ClearForm();
            this.Tag = null;
            MainForm.formMain.AddNew.Text = "TAMBAH BON TAHANAN";
            this.lblTitle.Text = "     TAMBAH BON TAHANAN";
            UserFunction.ClearControls(tableLayoutPanel1);
            MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                if (this.Tag == null) {                    
                    SaveData();
                }
                else{ 
                    UpdateData();
                }
                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Disimpan!");
                UserFunction.LoadDataBonTahananToGrid(MainForm.formMain.ListBonTahanan.GvBonTahanan);
                MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
                MainForm.formMain.AddNew.Text = "TAMBAH BON TAHANAN";
                this.lblTitle.Text = "     TAMBAH BON TAHANAN";
                this.Tag = null;

                ClearForm();
            }
        }

        private void SaveData()
        {
            BonTahananService serv = new BonTahananService();
            var bon = new BonTahanan();
            bon.NoBonTahanan = txtNoBonTahanan.Text;
            bon.Keperluan = ddlKeperluanBon.SelectedValue.ToString();
            bon.TanggalMulai = dpTanggalMulai.Value;
            bon.TanggalSelesai = dpTanggalSelesai.Value;
            bon.Lokasi = txtLokasi.Text;
            bon.Keterangan = txtKeterangan.Text;

            if (rbPersonel.CheckState == CheckState.Checked)
            {
                bon.TipePemohon = "Personel";
                bon.PangkatPemohon = ddlPangkatPemohon.SelectedValue.ToString();
                bon.SatkerPemohon = txtSatkerPemohon.Text;
            }
            else
            {
                bon.TipePemohon = "Jaksa";
                bon.PangkatPemohon = null;
                bon.SatkerPemohon = null;
            }

            bon.NamaPemohon = txtNamaPemohon.Text;
            bon.NrpPemohon = txtNrpPemohon.Text;
            bon.NoHpPemohon = txtNohpPemohon.Text;

            bon.NamaPenanggungJawab = txtNamaPJ.Text;
            bon.NrpPenanggungJawab = txtNrpPJ.Text;
            if (txtNrpPJ.Text != String.Empty)
            {
                bon.PangkatPenanggungJawab = ddlPangkatPJ.SelectedValue.ToString();
            }
            bon.SatkerPenanggungJawab = txtSatkerPJ.Text;
            bon.NoHpPenanggungJawab = txtNohpPJ.Text;

            bon.NamaPenyetuju = txtNamaDisetujui.Text;
            bon.NrpPenyetuju = txtNrpDisetujui.Text;
            bon.PangkatPenyetuju = ddlPangkatDisetujui.SelectedValue.ToString();
            bon.SatkerPenyetuju = txtSatkerDisetujui.Text;

            bon.StatusTahanan = "Diminta";

            bon.CreatedDate = DateTime.Now.ToLocalTime();
            bon.CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID);
            bon.CreatedLocation = "0.0.0.0";
            serv.Post(bon);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.BONTAHANAN.ToString(), Activities = "Add New BonTahanan, Data=" + UserFunction.JsonString(bon) });

            InmateBonTahananService ib = new InmateBonTahananService();
            var ibon = new InmateBonTahanan();
            var bont = serv.GetIdByNo(txtNoBonTahanan.Text);

            var str = txtListTahanan.Text;
            char[] separator = { ';' };
            string[] strarr = null;
            strarr = str.Split(separator);

            //foreach (var item in ddlNama.CheckedItems)
            foreach (var item in strarr)
            {
                //ibon.RegID = item.Value.ToString();
                ibon.RegID = item;
                ibon.BonTahananId = bont.Id;
                ibon.CreatedDate = DateTime.Now.ToLocalTime();
                ibon.CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID);
                ibon.CreatedLocation = "0.0.0.0";
                ib.Post(ibon);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.BONTAHANAN.ToString(), Activities = "Add New Inmate BonTahanan, Data=" + UserFunction.JsonString(ibon) });
            }            
        }            

        private void UpdateData()
        {
            BonTahananService serv = new BonTahananService();
            var bon = new BonTahanan();
            bon.Id = Convert.ToInt32(this.Tag);
            bon.NoBonTahanan = txtNoBonTahanan.Text;
            //bon.RegID = tahananId.Text;

            bon.Keperluan = ddlKeperluanBon.SelectedValue.ToString();
            bon.TanggalMulai = dpTanggalMulai.Value;
            bon.TanggalSelesai = dpTanggalSelesai.Value;
            bon.Lokasi = txtLokasi.Text;
            bon.Keterangan = txtKeterangan.Text;

            if (rbPersonel.CheckState == CheckState.Checked)
            {
                bon.TipePemohon = "Personel";
                bon.PangkatPemohon = ddlPangkatPemohon.SelectedValue.ToString();
                bon.SatkerPemohon = txtSatkerPemohon.Text;
            }
            else
            {
                bon.TipePemohon = "Jaksa";
                bon.PangkatPemohon = null;
                bon.SatkerPemohon = null;
            }

            bon.NamaPemohon = txtNamaPemohon.Text;
            bon.NrpPemohon = txtNrpPemohon.Text;
            bon.NoHpPemohon = txtNohpPemohon.Text;

            bon.NamaPenanggungJawab = txtNamaPJ.Text;
            bon.NrpPenanggungJawab = txtNrpPJ.Text;
            if (txtNrpPJ.Text != String.Empty)
                bon.PangkatPenanggungJawab = ddlPangkatPJ.SelectedValue.ToString();
            bon.SatkerPenanggungJawab = txtSatkerPJ.Text;
            bon.NoHpPenanggungJawab = txtNohpPJ.Text;

            bon.NamaPenyetuju = txtNamaDisetujui.Text;
            bon.NrpPenyetuju = txtNrpDisetujui.Text;
            bon.PangkatPenyetuju = ddlPangkatDisetujui.SelectedValue.ToString();
            bon.SatkerPenyetuju = txtSatkerDisetujui.Text;

            bon.UpdatedDate = DateTime.Now.ToLocalTime();
            bon.UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID);
            bon.UpdatedLocation = "0.0.0.0";
            serv.Update(bon);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Update BonTahanan, Old Data=" + UserFunction.JsonString(OldData) + ",  New Data=" + UserFunction.JsonString(bon) });


            InmateBonTahananService ib = new InmateBonTahananService();
            var ibon = new InmateBonTahanan();
            var bont = serv.GetIdByNo(txtNoBonTahanan.Text);
            ib.DeleteByBonId(bon.Id);

            var str = txtListTahanan.Text;
            char[] separator = { ';' };
            string[] strarr = null;
            strarr = str.Split(separator);

            //foreach (var item in ddlNama.CheckedItems)
            foreach (var item in strarr)
            {
                //ibon.RegID = item.Value.ToString();
                ibon.RegID = item;
                ibon.BonTahananId = bont.Id;
                ibon.CreatedDate = DateTime.Now.ToLocalTime();
                ibon.CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID);
                ibon.CreatedLocation = "0.0.0.0";
                ib.Post(ibon);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.BONTAHANAN.ToString(), Activities = "Add New Inmate BonTahanan, Data=" + UserFunction.JsonString(ibon) });
            }
        }

        private bool DataValid()
        {
            if (this.txtNoBonTahanan.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi Nomor Bon Tahanan!";
                return false;
            }
            //if (this.ddlNama.Items.Count == 0)
            //{
            //    this.lblError.Visible = true;
            //    this.lblError.Text = "Mohon Mengisi Nama Tahanan!";
            //    return false;
            //}
            if (this.txtListTahanan.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Memilih Tahanan!";
                return false;
            }
            if (this.ddlKeperluanBon.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Memilih Keperluan Bon Tahanan!";
                return false;
            }
            if (this.dpTanggalMulai.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi Tanggal Mulai!";
                return false;
            }
            if (this.dpTanggalSelesai.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi Tanggal Selesai!";
                return false;
            }
            if (this.txtLokasi.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi Lokasi!";
                return false;
            }
            if (this.txtKeterangan.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi Keterangan!";
                return false;
            }
            if (rbPersonel.CheckState == CheckState.Checked)
            {
                if (this.txtNamaPemohon.Text == string.Empty)
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = "Mohon Mengisi Nama Yang Memohon!";
                    return false;
                }
                if (this.txtNrpPemohon.Text == string.Empty)
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = "Mohon Mengisi NRP Yang Memohon!";
                    return false;
                }
                if (this.ddlPangkatPemohon.Text == string.Empty)
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = "Mohon Memilih Pangkat Yang Memohon!";
                    return false;
                }
                if (this.txtSatkerPemohon.Text == string.Empty)
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = "Mohon Mengisi Satuan Kerja Yang Memohon!";
                    return false;
                }
                if (this.txtNohpPemohon.Text == string.Empty)
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = "Mohon Mengisi Nomor Handphone Yang Memohon!";
                    return false;
                }
            }
            else
            {
                if (this.txtNamaPemohon.Text == string.Empty)
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = "Mohon Mengisi Nama Yang Memohon!";
                    return false;
                }
                if (this.txtNrpPemohon.Text == string.Empty)
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = "Mohon Mengisi NIP Jaksa Yang Memohon!";
                    return false;
                }
                if (this.txtNohpPemohon.Text == string.Empty)
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = "Mohon Mengisi Nomor Handphone Yang Memohon!";
                    return false;
                }
            }
            //if (this.txtNamaPJ.Text == string.Empty)
            //{
            //    this.lblError.Visible = true;
            //    this.lblError.Text = "Mohon Mengisi Nama Yang Menjemput!";
            //    return false;
            //}
            //if (this.txtNrpPJ.Text == string.Empty)
            //{
            //    this.lblError.Visible = true;
            //    this.lblError.Text = "Mohon Mengisi NRP Yang Menjemput!";
            //    return false;
            //}
            //if (this.ddlPangkatPJ.Text == string.Empty)
            //{
            //    this.lblError.Visible = true;
            //    this.lblError.Text = "Mohon Memilih Pangkat Yang Menjemput!";
            //    return false;
            //}
            //if (this.txtSatkerPJ.Text == string.Empty)
            //{
            //    this.lblError.Visible = true;
            //    this.lblError.Text = "Mohon Mengisi Satuan Kerja Yang Menjemput!";
            //    return false;
            //}
            //if (this.txtNohpPJ.Text == string.Empty)
            //{
            //    this.lblError.Visible = true;
            //    this.lblError.Text = "Mohon Mengisi Nomor Handphone Yang Menjemput!";
            //    return false;
            //}
            if (this.txtNamaDisetujui.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi Nama Yang Menyetujui!";
                return false;
            }
            if (this.txtNrpDisetujui.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi NRP Yang Menyetujui!";
                return false;
            }
            if (this.ddlPangkatDisetujui.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Memilih Pangkat Yang Menyetujui!";
                return false;
            }
            if (this.txtSatkerDisetujui.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Mengisi Satuan Kerja Yang Menyetujui!";
                return false;
            }
            return true;
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            UserFunction.ClearControls(tableLayoutPanel1);

            if (this.Visible)
            {
                LoadData();
            }
            else
            {

            }
        }
    }
}
