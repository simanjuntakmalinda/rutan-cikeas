﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using System.Threading;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class VisitDetail : Base
    {
        private Point MouseDownLocation;
        private Visit data;
        public VisitDetail()
        {
            InitializeComponent();
            InitForm();
            InitEvents();

        }

        private void LoadData(string id)
        {
            VisitService kunjungan = new VisitService();
            data = kunjungan.GetDetailById(id);
            if (data != null)
            {
                namapegunjung.Text = data.VisitorName;
                namatahanan.Text = data.InmatesName;
                waktumulai.Text = data.StartVisit == null ? string.Empty : data.StartVisit.Value.ToString("dd MMMM yyyy HH:mm");
                waktuselesai.Text = data.EndVisit==null?string.Empty: data.EndVisit.Value.ToString("dd MMMM yyyy HH:mm");

                if (data.IsCheckedOut == false)
                {
                    this.btnCheckOut.Visible = true;
                }
                else
                {
                    this.btnCheckOut.Visible = false;
                }

                keperluan.Text = data.BringingItems;
                relation.Text = data.Relation;
                visitortype.Text = data.Type;
                inputter.Text = data.CreatedBy;
                inputdate.Text = data.CreatedDate == null ? string.Empty : data.CreatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss");
                updater.Text = data.UpdatedBy;
                updatedate.Text = data.UpdatedDate==null?string.Empty:data.UpdatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss");
            }
        }

        private void InitForm()
        {
            ToolTip toolTip = new ToolTip();
            toolTip.ToolTipIcon = ToolTipIcon.Info;
            toolTip.ToolTipTitle = "Info";
            toolTip.SetToolTip(exit, "Tutup Informasi Kunjungan");
            lblButton.BackColor = Global.MainColor;
            lblTitle.BackColor = Global.MainColor;
            lblTitle.Text = "      INFORMASI KUNJUNGAN";
            lblTitle.ForeColor = Color.White;
            lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;
            lblTitle.LabelElement.CustomFont = Global.MainFont;
            lblTitle.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            btnClose.ButtonElement.CustomFontStyle = FontStyle.Bold;
            btnClose.ButtonElement.CustomFont = Global.MainFont;
            btnClose.ButtonElement.CustomFontSize = Global.CustomFontSizeMain;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
        }

        private void InitEvents()
        {
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            exit.Click += exit_Click;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            this.VisibleChanged += Form_VisibleChanged;
            btnClose.Click += btnClose_Click;
            btnCheckOut.Click += btnCheckOut_Click;
        }

        private void btnCheckOut_Click(object sender, EventArgs e)
        {
            VisitorService visitor = new VisitorService();
            var visitordata = visitor.GetDetailById(data.VisitorId.ToString());

            if (visitordata != null)
            {
                if (RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "<html>Check Out Visitor :<br>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;" + visitordata.FullName + "<br>ID Type/No&nbsp;&nbsp;:&nbsp;&nbsp;" + visitordata.IDType + "/" + visitordata.IDNo, "Visitor Checkout", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes)
                {
                    if (RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "Check Out Visitor (" + data.VisitorName + ") ?", "Visitor Checkout", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes)
                    {
                        VisitService visitserv = new VisitService();
                        visitserv.CheckOutVisitor(data.Id, GlobalVariables.UserID);
                        UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERVISITOR.ToString(), Activities = "CheckOut Visitor, Data=" + UserFunction.JsonString(data) });
                        UserFunction.MsgBox(TipeMsg.Info, "Berhasil Keluar !");
                        UserFunction.LoadDataKunjunganToGrid(MainForm.formMain.ListKunjungan.GvVisitor);
                        UserFunction.SetInitGridView(MainForm.formMain.ListKunjungan.GvVisitor);
                        LoadData(this.Tag.ToString());
                    }
                }
            }
            else
            {
                RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "Data Pengunjung Tidak Ditemukan", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (this.Tag != null)
                    LoadData(this.Tag.ToString());
                else
                    this.Hide();
            }
        }
    }
}
