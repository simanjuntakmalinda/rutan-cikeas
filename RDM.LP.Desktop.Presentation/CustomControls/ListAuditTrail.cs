﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListAuditTrail : Base
    {
        public RadGridView GvUser { get { return this.gvAuditTrail; } }
        public ListAuditTrail()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
            LoadDropDownList();
        }

        private void LoadDropDownList()
        {
            
            AppUserService userserv = new AppUserService();
            ddlUser.Items.Clear();
            ddlUser.Items.Insert(0, new RadListDataItem { Text = "ALL", Value = "ALL" });
            ddlUser.DisplayMember = "Username";
            ddlUser.ValueMember = "Id";

            var data = userserv.Get().ToList();
            var newoption = new AspnetUsers
            {
                Id = 0,
                UserName = "ALL USER"
            };

            data.Add(newoption);
            ddlUser.DataSource = data.OrderBy(x=>x.Id);
        }

        private void InitEvents()
        {
            gvAuditTrail.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvAuditTrail.RowFormatting += radGridView_RowFormatting;
            gvAuditTrail.ContextMenuOpening += radGridView_ContextMenuOpening;
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            btnSubmit.Click += btnSubmit_Click;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;

        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            LoadDataGrid();
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarAuditTrail.pdf", "AUDIT TRAIL LIST DATA", gvAuditTrail);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarAuditTrail.csv", "AUDIT TRAIL LIST DATA", gvAuditTrail);
        }

        private void LoadDataGrid()
        {
            gvAuditTrail.DataSource = null;
            AuditTrailService audittrailserv = new AuditTrailService();
            gvAuditTrail.AutoGenerateColumns = true;
            gvAuditTrail.DataSource = audittrailserv.Get(ddlUser.Text,ddlStart.Value.ToString("yyyy-MM-dd"), ddlEnd.Value.ToString("yyyy-MM-dd"));

            gvAuditTrail.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvAuditTrail.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvAuditTrail.Columns["UserName"].IsVisible = true;
            gvAuditTrail.Columns["UserName"].HeaderText = "NAMA PENGGUNA";
            gvAuditTrail.Columns["UserName"].Width = 100;

            gvAuditTrail.Columns["Modul"].IsVisible = true;
            gvAuditTrail.Columns["Modul"].HeaderText = "MODUL";
            gvAuditTrail.Columns["Modul"].Width = 75;

            gvAuditTrail.Columns["LogDate"].IsVisible = true;
            gvAuditTrail.Columns["LogDate"].HeaderText = "TANGGAL LOG";
            gvAuditTrail.Columns["LogDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvAuditTrail.Columns["LogDate"].FormatString = "{0:dd MMM yyyy HH:mm:ss}";
            gvAuditTrail.Columns["LogDate"].Width = 100;

            gvAuditTrail.Columns["Activities"].IsVisible = true;
            gvAuditTrail.Columns["Activities"].HeaderText = "ACTIVITIES";
            gvAuditTrail.Columns["Activities"].Width = 300;
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     AUDIT TRAIL";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;
            this.lblDetail.LabelElement.CustomFontStyle = FontStyle.Bold;
            this.gvAuditTrail.GridViewElement.DrawBorder = true;
            this.gvAuditTrail.GridViewElement.BorderWidth = 3f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
            UserFunction.SetInitGridView(gvAuditTrail);

        }

    }
}
