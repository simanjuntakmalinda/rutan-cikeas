﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.DataAccess.Helper;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormPswChange : Base
    {
        private Point MouseDownLocation;
        public FormPswChange()
        {
            InitializeComponent();
            InitForm();
            LoadData();
            InitEvents();    
        }

        public void LoadData()
        {
            UsertxtUserName.Text = GlobalVariables.UserID;

        }

        private void InitEvents()
        {
            //Events
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;

            UsertxtUserName.KeyDown += Control_KeyDown;
            UsertxtPasswordOld.KeyDown += Control_KeyDown;
            UsertxtPassword1.KeyDown += Control_KeyDown;
            UsertxtPassword2.KeyDown += Control_KeyDown;
            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;

            exit.Click += exit_Click;
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SendKeys.Send("{TAB}");
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                UpdatePassword();
                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "<html>Kata Sandi Anda telah Berhasil Diubah !<br>Silahkan Login Kembali dengan Kata Sandi Baru Anda !");
                this.Hide();
                MainForm.formMain.Hide();
                FrmLogin frmlogin = GlobalForm.formlogin;
                frmlogin.Show();
                ThemeResolutionService.ApplicationThemeName = null;
                frmlogin.Refresh();
                this.Tag = null;
            }
        }

        private void UpdatePassword()
        {
            var username = UsertxtUserName.Text;
            AppUserService userserv = new AppUserService();
            var user = new AspnetUsers
            {
                UserName = UsertxtUserName.Text.ToUpper(),
                Password = SimpleRSA.Encrypt(UsertxtPassword1.Text)
            };
            userserv.ChangePassword(user);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERUSER.ToString(), Activities = "Change Password, UserName=" + user.UserName });
        }

        private void InitForm()
        {
            this.lblError.Visible = false;
            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     UBAH KATA SANDI";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 15.5f;

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private bool DataValid()
        {
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;
            if (this.UsertxtPasswordOld.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Kata Sandi Lama!";
                return false;
            }
            if (this.UsertxtPassword1.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Kata Sandi Baru!";
                return false;
            }

            if (this.UsertxtPassword1.Text.Length < 5)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Kata Sandi Baru minimal terdiri dari 5 karakter!";
                return false;
            }

            if (this.UsertxtPassword2.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Konfirmasi Kata Sandi Baru!";
                return false;
            }

            if (this.UsertxtPassword2.Text.Length < 5)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Konfirmasi Kata Sandi Baru minimal terdiri dari 5 karakter!";
                return false;
            }

            if (this.UsertxtPassword1.Text != this.UsertxtPassword2.Text)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Kata Sandi Baru dan Konfirmasi Kata Sandi Baru tidak sama!";
                return false;
            }


            AppUserService userserv = new AppUserService();
            if (SimpleRSA.Decrypt(userserv.GetByUserName(UsertxtUserName.Text).Password) != UsertxtPasswordOld.Text)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Kata Sandi Lama tidak Valid!";
                return false;
            }

            return true;
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }
    }
}
