﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class FormTerimaTahanan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTerimaTahanan));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnBack = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            this.radPanelError = new Telerik.WinControls.UI.RadPanel();
            this.lblError = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.tpDiterima = new Telerik.WinControls.UI.RadTimePicker();
            this.radPanel22 = new Telerik.WinControls.UI.RadPanel();
            this.txtSatkerYangMenyerahkan = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel21 = new Telerik.WinControls.UI.RadPanel();
            this.txtNrpYangMenyerahkan = new Telerik.WinControls.UI.RadTextBox();
            this.lblWaktuTerima = new Telerik.WinControls.UI.RadLabel();
            this.lblNoCell = new Telerik.WinControls.UI.RadLabel();
            this.radPanel20 = new Telerik.WinControls.UI.RadPanel();
            this.dpTanggalTerima = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radPanel17 = new Telerik.WinControls.UI.RadPanel();
            this.txtNoCell = new Telerik.WinControls.UI.RadListView();
            this.radPanel19 = new Telerik.WinControls.UI.RadPanel();
            this.ddlBonTahanan = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel18 = new Telerik.WinControls.UI.RadPanel();
            this.txtNamaTahanan = new Telerik.WinControls.UI.RadListView();
            this.lblNamaTahanan = new Telerik.WinControls.UI.RadLabel();
            this.lblNoBonTahanan = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailVisitor = new Telerik.WinControls.UI.RadLabel();
            this.lblNamaYangMenyetujui = new Telerik.WinControls.UI.RadLabel();
            this.radPanel9 = new Telerik.WinControls.UI.RadPanel();
            this.txtNamaYangMenyetujui = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel15 = new Telerik.WinControls.UI.RadPanel();
            this.txtNamaYangMenyerahkan = new Telerik.WinControls.UI.RadTextBox();
            this.lblNamaYangMenyerahkan = new Telerik.WinControls.UI.RadLabel();
            this.lblYangMenyerahkan = new Telerik.WinControls.UI.RadLabel();
            this.lblYangMenerima = new Telerik.WinControls.UI.RadLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblNamaYangMenerima = new Telerik.WinControls.UI.RadLabel();
            this.radPanel16 = new Telerik.WinControls.UI.RadPanel();
            this.ddlPangkatYangMenyerahkan = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel11 = new Telerik.WinControls.UI.RadPanel();
            this.txtNamaYangMenerima = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel10 = new Telerik.WinControls.UI.RadPanel();
            this.txtNrpYangMenerima = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel12 = new Telerik.WinControls.UI.RadPanel();
            this.ddlPangkatYangMenerima = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.txtSatkerYangMenerima = new Telerik.WinControls.UI.RadTextBox();
            this.lblYangMenyetujui = new Telerik.WinControls.UI.RadLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.txtNrpYangMenyetujui = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel6 = new Telerik.WinControls.UI.RadPanel();
            this.ddlPangkatYangMenyetujui = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel7 = new Telerik.WinControls.UI.RadPanel();
            this.txtSatkerYangMenyetujui = new Telerik.WinControls.UI.RadTextBox();
            this.lblDetailSurat = new Telerik.WinControls.UI.RadLabel();
            this.headerPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.cameraSource = new RDM.LP.Desktop.Presentation.CustomControls.CameraSource();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).BeginInit();
            this.radPanelError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tpDiterima)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel22)).BeginInit();
            this.radPanel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSatkerYangMenyerahkan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel21)).BeginInit();
            this.radPanel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNrpYangMenyerahkan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWaktuTerima)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoCell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel20)).BeginInit();
            this.radPanel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dpTanggalTerima)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel17)).BeginInit();
            this.radPanel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoCell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel19)).BeginInit();
            this.radPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlBonTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel18)).BeginInit();
            this.radPanel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoBonTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaYangMenyetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel9)).BeginInit();
            this.radPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaYangMenyetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel15)).BeginInit();
            this.radPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaYangMenyerahkan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaYangMenyerahkan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYangMenyerahkan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYangMenerima)).BeginInit();
            this.lblYangMenerima.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaYangMenerima)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel16)).BeginInit();
            this.radPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPangkatYangMenyerahkan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel11)).BeginInit();
            this.radPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaYangMenerima)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel10)).BeginInit();
            this.radPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNrpYangMenerima)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel12)).BeginInit();
            this.radPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPangkatYangMenerima)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSatkerYangMenerima)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYangMenyetujui)).BeginInit();
            this.lblYangMenyetujui.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNrpYangMenyetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).BeginInit();
            this.radPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlPangkatYangMenyetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).BeginInit();
            this.radPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSatkerYangMenyetujui)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).BeginInit();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnBack);
            this.lblButton.Controls.Add(this.btnSave);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 642);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(20);
            this.lblButton.Size = new System.Drawing.Size(1183, 94);
            this.lblButton.TabIndex = 81;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.btnBack_Image;
            this.btnBack.Location = new System.Drawing.Point(20, 20);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(244, 54);
            this.btnBack.TabIndex = 20;
            this.btnBack.Tag = "";
            this.btnBack.Text = "&BATAL";
            this.btnBack.ThemeName = "MaterialBlueGrey";
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.if_checked_2_309094;
            this.btnSave.Location = new System.Drawing.Point(858, 20);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(305, 54);
            this.btnSave.TabIndex = 19;
            this.btnSave.Text = "&SIMPAN";
            this.btnSave.ThemeName = "MaterialBlueGrey";
            // 
            // radPanelError
            // 
            this.radPanelError.Controls.Add(this.lblError);
            this.radPanelError.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanelError.Location = new System.Drawing.Point(0, 598);
            this.radPanelError.Margin = new System.Windows.Forms.Padding(0);
            this.radPanelError.Name = "radPanelError";
            this.radPanelError.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radPanelError.Size = new System.Drawing.Size(1183, 43);
            this.radPanelError.TabIndex = 80;
            // 
            // lblError
            // 
            this.lblError.AutoSize = false;
            this.lblError.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.warning;
            this.lblError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblError.Location = new System.Drawing.Point(15, 0);
            this.lblError.Name = "lblError";
            this.lblError.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.lblError.Size = new System.Drawing.Size(1168, 39);
            this.lblError.TabIndex = 24;
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("radScrollablePanel1.BackgroundImage")));
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 108);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(1164, 488);
            this.radScrollablePanel1.Size = new System.Drawing.Size(1183, 490);
            this.radScrollablePanel1.TabIndex = 78;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 380F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel3, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.radLabel2, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.radPanel22, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.radPanel21, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblWaktuTerima, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblNoCell, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel20, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.radPanel17, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel19, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel18, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblNamaTahanan, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblNoBonTahanan, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailVisitor, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblNamaYangMenyetujui, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.radPanel9, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.radPanel15, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblNamaYangMenyerahkan, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblYangMenyerahkan, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblYangMenerima, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblNamaYangMenerima, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.radPanel16, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.radPanel11, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.radPanel10, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.radPanel12, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 3, 10);
            this.tableLayoutPanel1.Controls.Add(this.lblYangMenyetujui, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.radPanel6, 2, 13);
            this.tableLayoutPanel1.Controls.Add(this.radPanel7, 3, 13);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 15;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1164, 850);
            this.tableLayoutPanel1.TabIndex = 161;
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel3.ForeColor = System.Drawing.Color.Black;
            this.radLabel3.Location = new System.Drawing.Point(14, 764);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(372, 42);
            this.radLabel3.TabIndex = 34;
            this.radLabel3.Text = "<html>NRP / Pangkat / Satuan Kerja<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel2.ForeColor = System.Drawing.Color.Black;
            this.radLabel2.Location = new System.Drawing.Point(14, 614);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(372, 42);
            this.radLabel2.TabIndex = 34;
            this.radLabel2.Text = "<html>NRP / Pangkat / Satuan Kerja</html>";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.ForeColor = System.Drawing.Color.Black;
            this.radLabel1.Location = new System.Drawing.Point(14, 464);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(372, 42);
            this.radLabel1.TabIndex = 56;
            this.radLabel1.Text = "<html>NRP / Pangkat / Satuan Kerja<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel3
            // 
            this.radPanel3.Controls.Add(this.tpDiterima);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Location = new System.Drawing.Point(629, 313);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(230, 44);
            this.radPanel3.TabIndex = 5;
            // 
            // tpDiterima
            // 
            this.tpDiterima.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tpDiterima.Location = new System.Drawing.Point(0, 0);
            this.tpDiterima.MaxValue = new System.DateTime(9999, 12, 31, 23, 59, 59, 0);
            this.tpDiterima.MinValue = new System.DateTime(((long)(0)));
            this.tpDiterima.Name = "tpDiterima";
            this.tpDiterima.Size = new System.Drawing.Size(230, 24);
            this.tpDiterima.TabIndex = 1;
            this.tpDiterima.TabStop = false;
            this.tpDiterima.Value = new System.DateTime(2019, 2, 1, 9, 47, 0, 0);
            // 
            // radPanel22
            // 
            this.radPanel22.Controls.Add(this.txtSatkerYangMenyerahkan);
            this.radPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel22.Location = new System.Drawing.Point(865, 463);
            this.radPanel22.Name = "radPanel22";
            this.radPanel22.Size = new System.Drawing.Size(230, 44);
            this.radPanel22.TabIndex = 10;
            // 
            // txtSatkerYangMenyerahkan
            // 
            this.txtSatkerYangMenyerahkan.AutoSize = false;
            this.txtSatkerYangMenyerahkan.BackColor = System.Drawing.Color.White;
            this.txtSatkerYangMenyerahkan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSatkerYangMenyerahkan.Location = new System.Drawing.Point(0, 0);
            this.txtSatkerYangMenyerahkan.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtSatkerYangMenyerahkan.Multiline = true;
            this.txtSatkerYangMenyerahkan.Name = "txtSatkerYangMenyerahkan";
            this.txtSatkerYangMenyerahkan.Size = new System.Drawing.Size(230, 44);
            this.txtSatkerYangMenyerahkan.TabIndex = 7;
            // 
            // radPanel21
            // 
            this.radPanel21.Controls.Add(this.txtNrpYangMenyerahkan);
            this.radPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel21.Location = new System.Drawing.Point(393, 463);
            this.radPanel21.Name = "radPanel21";
            this.radPanel21.Size = new System.Drawing.Size(230, 44);
            this.radPanel21.TabIndex = 8;
            // 
            // txtNrpYangMenyerahkan
            // 
            this.txtNrpYangMenyerahkan.AutoSize = false;
            this.txtNrpYangMenyerahkan.BackColor = System.Drawing.Color.White;
            this.txtNrpYangMenyerahkan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNrpYangMenyerahkan.Location = new System.Drawing.Point(0, 0);
            this.txtNrpYangMenyerahkan.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNrpYangMenyerahkan.MaxLength = 8;
            this.txtNrpYangMenyerahkan.Multiline = true;
            this.txtNrpYangMenyerahkan.Name = "txtNrpYangMenyerahkan";
            this.txtNrpYangMenyerahkan.Size = new System.Drawing.Size(230, 44);
            this.txtNrpYangMenyerahkan.TabIndex = 6;
            // 
            // lblWaktuTerima
            // 
            this.lblWaktuTerima.AutoSize = false;
            this.lblWaktuTerima.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWaktuTerima.ForeColor = System.Drawing.Color.Black;
            this.lblWaktuTerima.Location = new System.Drawing.Point(14, 314);
            this.lblWaktuTerima.Margin = new System.Windows.Forms.Padding(4);
            this.lblWaktuTerima.Name = "lblWaktuTerima";
            this.lblWaktuTerima.Size = new System.Drawing.Size(372, 42);
            this.lblWaktuTerima.TabIndex = 53;
            this.lblWaktuTerima.Text = "<html>Hari / Tanggal / Jam Diserahkan<span style=\"color: #ff0000\"> *</span></html" +
    ">";
            // 
            // lblNoCell
            // 
            this.lblNoCell.AutoSize = false;
            this.lblNoCell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoCell.ForeColor = System.Drawing.Color.Black;
            this.lblNoCell.Location = new System.Drawing.Point(14, 264);
            this.lblNoCell.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoCell.Name = "lblNoCell";
            this.lblNoCell.Size = new System.Drawing.Size(372, 42);
            this.lblNoCell.TabIndex = 52;
            this.lblNoCell.Text = "<html>WARD / No Sel<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel20
            // 
            this.radPanel20.Controls.Add(this.dpTanggalTerima);
            this.radPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel20.Location = new System.Drawing.Point(393, 313);
            this.radPanel20.Name = "radPanel20";
            this.radPanel20.Size = new System.Drawing.Size(230, 44);
            this.radPanel20.TabIndex = 4;
            // 
            // dpTanggalTerima
            // 
            this.dpTanggalTerima.AutoSize = false;
            this.dpTanggalTerima.BackColor = System.Drawing.Color.Transparent;
            this.dpTanggalTerima.CalendarSize = new System.Drawing.Size(290, 320);
            this.dpTanggalTerima.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dpTanggalTerima.Location = new System.Drawing.Point(0, 0);
            this.dpTanggalTerima.Name = "dpTanggalTerima";
            this.dpTanggalTerima.Size = new System.Drawing.Size(230, 44);
            this.dpTanggalTerima.TabIndex = 7;
            this.dpTanggalTerima.TabStop = false;
            this.dpTanggalTerima.Text = "Friday, December 21, 2018";
            this.dpTanggalTerima.Value = new System.DateTime(2018, 12, 21, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.dpTanggalTerima.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dpTanggalTerima.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dpTanggalTerima.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpTanggalTerima.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpTanggalTerima.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpTanggalTerima.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpTanggalTerima.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dpTanggalTerima.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "Friday, December 21, 2018";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dpTanggalTerima.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // radPanel17
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel17, 2);
            this.radPanel17.Controls.Add(this.txtNoCell);
            this.radPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel17.Location = new System.Drawing.Point(393, 263);
            this.radPanel17.Name = "radPanel17";
            this.radPanel17.Size = new System.Drawing.Size(466, 44);
            this.radPanel17.TabIndex = 3;
            // 
            // txtNoCell
            // 
            this.txtNoCell.AllowColumnReorder = false;
            this.txtNoCell.AllowColumnResize = false;
            this.txtNoCell.AllowDragDrop = true;
            this.txtNoCell.AllowEdit = false;
            this.txtNoCell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoCell.EnableCodedUITests = true;
            this.txtNoCell.EnableColumnSort = true;
            this.txtNoCell.EnableCustomGrouping = true;
            this.txtNoCell.EnableFiltering = true;
            this.txtNoCell.EnableGrouping = true;
            this.txtNoCell.EnableKeyMap = true;
            this.txtNoCell.EnableLassoSelection = true;
            this.txtNoCell.EnableSorting = true;
            this.txtNoCell.Location = new System.Drawing.Point(0, 0);
            this.txtNoCell.MultiSelect = true;
            this.txtNoCell.Name = "txtNoCell";
            this.txtNoCell.Size = new System.Drawing.Size(466, 44);
            this.txtNoCell.TabIndex = 100;
            ((Telerik.WinControls.UI.RadListViewElement)(this.txtNoCell.GetChildAt(0))).HorizontalLineWidth = 0;
            ((Telerik.WinControls.UI.RadListViewElement)(this.txtNoCell.GetChildAt(0))).FocusBorderWidth = 0;
            ((Telerik.WinControls.UI.RadListViewElement)(this.txtNoCell.GetChildAt(0))).BorderHighlightThickness = 0;
            // 
            // radPanel19
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel19, 2);
            this.radPanel19.Controls.Add(this.ddlBonTahanan);
            this.radPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel19.Location = new System.Drawing.Point(393, 63);
            this.radPanel19.Name = "radPanel19";
            this.radPanel19.Size = new System.Drawing.Size(466, 44);
            this.radPanel19.TabIndex = 1;
            // 
            // ddlBonTahanan
            // 
            this.ddlBonTahanan.AutoSize = false;
            this.ddlBonTahanan.BackColor = System.Drawing.Color.White;
            this.ddlBonTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlBonTahanan.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlBonTahanan.DropDownHeight = 400;
            this.ddlBonTahanan.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlBonTahanan.Location = new System.Drawing.Point(0, 0);
            this.ddlBonTahanan.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlBonTahanan.Name = "ddlBonTahanan";
            // 
            // 
            // 
            this.ddlBonTahanan.RootElement.CustomFont = "Roboto";
            this.ddlBonTahanan.RootElement.CustomFontSize = 13F;
            this.ddlBonTahanan.Size = new System.Drawing.Size(466, 44);
            this.ddlBonTahanan.TabIndex = 16;
            this.ddlBonTahanan.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlBonTahanan.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlBonTahanan.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlBonTahanan.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlBonTahanan.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlBonTahanan.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlBonTahanan.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlBonTahanan.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlBonTahanan.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlBonTahanan.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlBonTahanan.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel18
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel18, 2);
            this.radPanel18.Controls.Add(this.txtNamaTahanan);
            this.radPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel18.Location = new System.Drawing.Point(393, 113);
            this.radPanel18.Name = "radPanel18";
            this.radPanel18.Size = new System.Drawing.Size(466, 144);
            this.radPanel18.TabIndex = 2;
            // 
            // txtNamaTahanan
            // 
            this.txtNamaTahanan.AllowColumnReorder = false;
            this.txtNamaTahanan.AllowColumnResize = false;
            this.txtNamaTahanan.AllowDragDrop = true;
            this.txtNamaTahanan.AllowEdit = false;
            this.txtNamaTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNamaTahanan.EnableCodedUITests = true;
            this.txtNamaTahanan.EnableColumnSort = true;
            this.txtNamaTahanan.EnableCustomGrouping = true;
            this.txtNamaTahanan.EnableFiltering = true;
            this.txtNamaTahanan.EnableGrouping = true;
            this.txtNamaTahanan.EnableKeyMap = true;
            this.txtNamaTahanan.EnableLassoSelection = true;
            this.txtNamaTahanan.EnableSorting = true;
            this.txtNamaTahanan.Location = new System.Drawing.Point(0, 0);
            this.txtNamaTahanan.MultiSelect = true;
            this.txtNamaTahanan.Name = "txtNamaTahanan";
            this.txtNamaTahanan.Size = new System.Drawing.Size(466, 144);
            this.txtNamaTahanan.TabIndex = 100;
            ((Telerik.WinControls.UI.RadListViewElement)(this.txtNamaTahanan.GetChildAt(0))).HorizontalLineWidth = 0;
            ((Telerik.WinControls.UI.RadListViewElement)(this.txtNamaTahanan.GetChildAt(0))).FocusBorderWidth = 0;
            ((Telerik.WinControls.UI.RadListViewElement)(this.txtNamaTahanan.GetChildAt(0))).BorderHighlightThickness = 0;
            // 
            // lblNamaTahanan
            // 
            this.lblNamaTahanan.AutoSize = false;
            this.lblNamaTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNamaTahanan.ForeColor = System.Drawing.Color.Black;
            this.lblNamaTahanan.Location = new System.Drawing.Point(14, 114);
            this.lblNamaTahanan.Margin = new System.Windows.Forms.Padding(4);
            this.lblNamaTahanan.Name = "lblNamaTahanan";
            this.lblNamaTahanan.Size = new System.Drawing.Size(372, 142);
            this.lblNamaTahanan.TabIndex = 51;
            this.lblNamaTahanan.Text = "<html>Nama Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblNoBonTahanan
            // 
            this.lblNoBonTahanan.AutoSize = false;
            this.lblNoBonTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoBonTahanan.ForeColor = System.Drawing.Color.Black;
            this.lblNoBonTahanan.Location = new System.Drawing.Point(14, 64);
            this.lblNoBonTahanan.Margin = new System.Windows.Forms.Padding(4);
            this.lblNoBonTahanan.Name = "lblNoBonTahanan";
            this.lblNoBonTahanan.Size = new System.Drawing.Size(372, 42);
            this.lblNoBonTahanan.TabIndex = 50;
            this.lblNoBonTahanan.Text = "<html>No Bon Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblDetailVisitor
            // 
            this.lblDetailVisitor.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailVisitor, 4);
            this.lblDetailVisitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailVisitor.Location = new System.Drawing.Point(13, 13);
            this.lblDetailVisitor.Name = "lblDetailVisitor";
            this.lblDetailVisitor.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDetailVisitor.Size = new System.Drawing.Size(1082, 44);
            this.lblDetailVisitor.TabIndex = 134;
            this.lblDetailVisitor.Text = "DETAIL TERIMA TAHANAN";
            // 
            // lblNamaYangMenyetujui
            // 
            this.lblNamaYangMenyetujui.AutoSize = false;
            this.lblNamaYangMenyetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNamaYangMenyetujui.ForeColor = System.Drawing.Color.Black;
            this.lblNamaYangMenyetujui.Location = new System.Drawing.Point(14, 714);
            this.lblNamaYangMenyetujui.Margin = new System.Windows.Forms.Padding(4);
            this.lblNamaYangMenyetujui.Name = "lblNamaYangMenyetujui";
            this.lblNamaYangMenyetujui.Size = new System.Drawing.Size(372, 42);
            this.lblNamaYangMenyetujui.TabIndex = 131;
            this.lblNamaYangMenyetujui.Text = "<html>Nama<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel9
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel9, 2);
            this.radPanel9.Controls.Add(this.txtNamaYangMenyetujui);
            this.radPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel9.Location = new System.Drawing.Point(393, 713);
            this.radPanel9.Name = "radPanel9";
            this.radPanel9.Size = new System.Drawing.Size(466, 44);
            this.radPanel9.TabIndex = 15;
            // 
            // txtNamaYangMenyetujui
            // 
            this.txtNamaYangMenyetujui.AutoSize = false;
            this.txtNamaYangMenyetujui.BackColor = System.Drawing.Color.White;
            this.txtNamaYangMenyetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNamaYangMenyetujui.Location = new System.Drawing.Point(0, 0);
            this.txtNamaYangMenyetujui.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNamaYangMenyetujui.Name = "txtNamaYangMenyetujui";
            this.txtNamaYangMenyetujui.Size = new System.Drawing.Size(466, 44);
            this.txtNamaYangMenyetujui.TabIndex = 100;
            // 
            // radPanel15
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel15, 2);
            this.radPanel15.Controls.Add(this.txtNamaYangMenyerahkan);
            this.radPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel15.Location = new System.Drawing.Point(393, 413);
            this.radPanel15.Name = "radPanel15";
            this.radPanel15.Size = new System.Drawing.Size(466, 44);
            this.radPanel15.TabIndex = 7;
            // 
            // txtNamaYangMenyerahkan
            // 
            this.txtNamaYangMenyerahkan.AutoSize = false;
            this.txtNamaYangMenyerahkan.BackColor = System.Drawing.Color.White;
            this.txtNamaYangMenyerahkan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNamaYangMenyerahkan.Location = new System.Drawing.Point(0, 0);
            this.txtNamaYangMenyerahkan.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNamaYangMenyerahkan.MaxLength = 16;
            this.txtNamaYangMenyerahkan.Name = "txtNamaYangMenyerahkan";
            this.txtNamaYangMenyerahkan.Size = new System.Drawing.Size(466, 44);
            this.txtNamaYangMenyerahkan.TabIndex = 4;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNamaYangMenyerahkan.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNamaYangMenyerahkan.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNamaYangMenyerahkan.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNamaYangMenyerahkan.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // lblNamaYangMenyerahkan
            // 
            this.lblNamaYangMenyerahkan.AutoSize = false;
            this.lblNamaYangMenyerahkan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNamaYangMenyerahkan.ForeColor = System.Drawing.Color.Black;
            this.lblNamaYangMenyerahkan.Location = new System.Drawing.Point(14, 414);
            this.lblNamaYangMenyerahkan.Margin = new System.Windows.Forms.Padding(4);
            this.lblNamaYangMenyerahkan.Name = "lblNamaYangMenyerahkan";
            this.lblNamaYangMenyerahkan.Size = new System.Drawing.Size(372, 42);
            this.lblNamaYangMenyerahkan.TabIndex = 55;
            this.lblNamaYangMenyerahkan.Text = "<html>Nama<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblYangMenyerahkan
            // 
            this.lblYangMenyerahkan.AutoSize = false;
            this.lblYangMenyerahkan.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tableLayoutPanel1.SetColumnSpan(this.lblYangMenyerahkan, 4);
            this.lblYangMenyerahkan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblYangMenyerahkan.ForeColor = System.Drawing.Color.Black;
            this.lblYangMenyerahkan.Location = new System.Drawing.Point(14, 364);
            this.lblYangMenyerahkan.Margin = new System.Windows.Forms.Padding(4);
            this.lblYangMenyerahkan.Name = "lblYangMenyerahkan";
            this.lblYangMenyerahkan.Size = new System.Drawing.Size(1080, 42);
            this.lblYangMenyerahkan.TabIndex = 54;
            this.lblYangMenyerahkan.Text = "YANG MENYERAHKAN";
            // 
            // lblYangMenerima
            // 
            this.lblYangMenerima.AutoSize = false;
            this.lblYangMenerima.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tableLayoutPanel1.SetColumnSpan(this.lblYangMenerima, 4);
            this.lblYangMenerima.Controls.Add(this.label1);
            this.lblYangMenerima.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblYangMenerima.Location = new System.Drawing.Point(13, 513);
            this.lblYangMenerima.Name = "lblYangMenerima";
            this.lblYangMenerima.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblYangMenerima.Size = new System.Drawing.Size(1082, 44);
            this.lblYangMenerima.TabIndex = 194;
            this.lblYangMenerima.Text = "YANG MENERIMA";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(1077, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 44);
            this.label1.TabIndex = 167;
            this.label1.Visible = false;
            // 
            // lblNamaYangMenerima
            // 
            this.lblNamaYangMenerima.AutoSize = false;
            this.lblNamaYangMenerima.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNamaYangMenerima.ForeColor = System.Drawing.Color.Black;
            this.lblNamaYangMenerima.Location = new System.Drawing.Point(14, 564);
            this.lblNamaYangMenerima.Margin = new System.Windows.Forms.Padding(4);
            this.lblNamaYangMenerima.Name = "lblNamaYangMenerima";
            this.lblNamaYangMenerima.Size = new System.Drawing.Size(372, 42);
            this.lblNamaYangMenerima.TabIndex = 130;
            this.lblNamaYangMenerima.Text = "<html>Nama</html>";
            // 
            // radPanel16
            // 
            this.radPanel16.Controls.Add(this.ddlPangkatYangMenyerahkan);
            this.radPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel16.Location = new System.Drawing.Point(629, 463);
            this.radPanel16.Name = "radPanel16";
            this.radPanel16.Size = new System.Drawing.Size(230, 44);
            this.radPanel16.TabIndex = 9;
            // 
            // ddlPangkatYangMenyerahkan
            // 
            this.ddlPangkatYangMenyerahkan.AutoSize = false;
            this.ddlPangkatYangMenyerahkan.BackColor = System.Drawing.Color.White;
            this.ddlPangkatYangMenyerahkan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlPangkatYangMenyerahkan.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlPangkatYangMenyerahkan.DropDownHeight = 400;
            this.ddlPangkatYangMenyerahkan.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlPangkatYangMenyerahkan.Location = new System.Drawing.Point(0, 0);
            this.ddlPangkatYangMenyerahkan.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlPangkatYangMenyerahkan.Name = "ddlPangkatYangMenyerahkan";
            // 
            // 
            // 
            this.ddlPangkatYangMenyerahkan.RootElement.CustomFont = "Roboto";
            this.ddlPangkatYangMenyerahkan.RootElement.CustomFontSize = 13F;
            this.ddlPangkatYangMenyerahkan.Size = new System.Drawing.Size(230, 44);
            this.ddlPangkatYangMenyerahkan.TabIndex = 17;
            this.ddlPangkatYangMenyerahkan.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatYangMenyerahkan.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatYangMenyerahkan.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatYangMenyerahkan.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatYangMenyerahkan.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatYangMenyerahkan.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatYangMenyerahkan.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatYangMenyerahkan.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatYangMenyerahkan.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPangkatYangMenyerahkan.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPangkatYangMenyerahkan.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel11
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel11, 2);
            this.radPanel11.Controls.Add(this.txtNamaYangMenerima);
            this.radPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel11.Location = new System.Drawing.Point(393, 563);
            this.radPanel11.Name = "radPanel11";
            this.radPanel11.Size = new System.Drawing.Size(466, 44);
            this.radPanel11.TabIndex = 11;
            // 
            // txtNamaYangMenerima
            // 
            this.txtNamaYangMenerima.AutoSize = false;
            this.txtNamaYangMenerima.BackColor = System.Drawing.Color.White;
            this.txtNamaYangMenerima.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNamaYangMenerima.Location = new System.Drawing.Point(0, 0);
            this.txtNamaYangMenerima.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNamaYangMenerima.Name = "txtNamaYangMenerima";
            this.txtNamaYangMenerima.Size = new System.Drawing.Size(466, 44);
            this.txtNamaYangMenerima.TabIndex = 5;
            // 
            // radPanel10
            // 
            this.radPanel10.Controls.Add(this.txtNrpYangMenerima);
            this.radPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel10.Location = new System.Drawing.Point(393, 613);
            this.radPanel10.Name = "radPanel10";
            this.radPanel10.Size = new System.Drawing.Size(230, 44);
            this.radPanel10.TabIndex = 12;
            // 
            // txtNrpYangMenerima
            // 
            this.txtNrpYangMenerima.AutoSize = false;
            this.txtNrpYangMenerima.BackColor = System.Drawing.Color.White;
            this.txtNrpYangMenerima.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNrpYangMenerima.Location = new System.Drawing.Point(0, 0);
            this.txtNrpYangMenerima.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNrpYangMenerima.MaxLength = 8;
            this.txtNrpYangMenerima.Name = "txtNrpYangMenerima";
            this.txtNrpYangMenerima.Size = new System.Drawing.Size(230, 44);
            this.txtNrpYangMenerima.TabIndex = 5;
            // 
            // radPanel12
            // 
            this.radPanel12.Controls.Add(this.ddlPangkatYangMenerima);
            this.radPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel12.Location = new System.Drawing.Point(629, 613);
            this.radPanel12.Name = "radPanel12";
            this.radPanel12.Size = new System.Drawing.Size(230, 44);
            this.radPanel12.TabIndex = 13;
            // 
            // ddlPangkatYangMenerima
            // 
            this.ddlPangkatYangMenerima.AutoSize = false;
            this.ddlPangkatYangMenerima.BackColor = System.Drawing.Color.White;
            this.ddlPangkatYangMenerima.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlPangkatYangMenerima.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlPangkatYangMenerima.DropDownHeight = 400;
            this.ddlPangkatYangMenerima.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlPangkatYangMenerima.Location = new System.Drawing.Point(0, 0);
            this.ddlPangkatYangMenerima.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlPangkatYangMenerima.Name = "ddlPangkatYangMenerima";
            // 
            // 
            // 
            this.ddlPangkatYangMenerima.RootElement.CustomFont = "Roboto";
            this.ddlPangkatYangMenerima.RootElement.CustomFontSize = 13F;
            this.ddlPangkatYangMenerima.Size = new System.Drawing.Size(230, 44);
            this.ddlPangkatYangMenerima.TabIndex = 16;
            this.ddlPangkatYangMenerima.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatYangMenerima.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatYangMenerima.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatYangMenerima.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatYangMenerima.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatYangMenerima.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatYangMenerima.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatYangMenerima.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatYangMenerima.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPangkatYangMenerima.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPangkatYangMenerima.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.txtSatkerYangMenerima);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(865, 613);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(230, 44);
            this.radPanel1.TabIndex = 14;
            // 
            // txtSatkerYangMenerima
            // 
            this.txtSatkerYangMenerima.AutoSize = false;
            this.txtSatkerYangMenerima.BackColor = System.Drawing.Color.White;
            this.txtSatkerYangMenerima.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSatkerYangMenerima.Location = new System.Drawing.Point(0, 0);
            this.txtSatkerYangMenerima.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtSatkerYangMenerima.Name = "txtSatkerYangMenerima";
            this.txtSatkerYangMenerima.Size = new System.Drawing.Size(230, 44);
            this.txtSatkerYangMenerima.TabIndex = 5;
            // 
            // lblYangMenyetujui
            // 
            this.lblYangMenyetujui.AutoSize = false;
            this.lblYangMenyetujui.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tableLayoutPanel1.SetColumnSpan(this.lblYangMenyetujui, 4);
            this.lblYangMenyetujui.Controls.Add(this.label2);
            this.lblYangMenyetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblYangMenyetujui.Location = new System.Drawing.Point(13, 663);
            this.lblYangMenyetujui.Name = "lblYangMenyetujui";
            this.lblYangMenyetujui.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblYangMenyetujui.Size = new System.Drawing.Size(1082, 44);
            this.lblYangMenyetujui.TabIndex = 195;
            this.lblYangMenyetujui.Text = "YANG MENYETUJUI";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(1077, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 44);
            this.label2.TabIndex = 167;
            this.label2.Visible = false;
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.txtNrpYangMenyetujui);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(393, 763);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(230, 44);
            this.radPanel2.TabIndex = 16;
            // 
            // txtNrpYangMenyetujui
            // 
            this.txtNrpYangMenyetujui.AutoSize = false;
            this.txtNrpYangMenyetujui.BackColor = System.Drawing.Color.White;
            this.txtNrpYangMenyetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNrpYangMenyetujui.Location = new System.Drawing.Point(0, 0);
            this.txtNrpYangMenyetujui.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNrpYangMenyetujui.MaxLength = 8;
            this.txtNrpYangMenyetujui.Name = "txtNrpYangMenyetujui";
            this.txtNrpYangMenyetujui.Size = new System.Drawing.Size(230, 44);
            this.txtNrpYangMenyetujui.TabIndex = 5;
            // 
            // radPanel6
            // 
            this.radPanel6.Controls.Add(this.ddlPangkatYangMenyetujui);
            this.radPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel6.Location = new System.Drawing.Point(629, 763);
            this.radPanel6.Name = "radPanel6";
            this.radPanel6.Size = new System.Drawing.Size(230, 44);
            this.radPanel6.TabIndex = 17;
            // 
            // ddlPangkatYangMenyetujui
            // 
            this.ddlPangkatYangMenyetujui.AutoSize = false;
            this.ddlPangkatYangMenyetujui.BackColor = System.Drawing.Color.White;
            this.ddlPangkatYangMenyetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlPangkatYangMenyetujui.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlPangkatYangMenyetujui.DropDownHeight = 400;
            this.ddlPangkatYangMenyetujui.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlPangkatYangMenyetujui.Location = new System.Drawing.Point(0, 0);
            this.ddlPangkatYangMenyetujui.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlPangkatYangMenyetujui.Name = "ddlPangkatYangMenyetujui";
            // 
            // 
            // 
            this.ddlPangkatYangMenyetujui.RootElement.CustomFont = "Roboto";
            this.ddlPangkatYangMenyetujui.RootElement.CustomFontSize = 13F;
            this.ddlPangkatYangMenyetujui.Size = new System.Drawing.Size(230, 44);
            this.ddlPangkatYangMenyetujui.TabIndex = 16;
            this.ddlPangkatYangMenyetujui.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatYangMenyetujui.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatYangMenyetujui.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatYangMenyetujui.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlPangkatYangMenyetujui.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatYangMenyetujui.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatYangMenyetujui.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatYangMenyetujui.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlPangkatYangMenyetujui.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPangkatYangMenyetujui.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlPangkatYangMenyetujui.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel7
            // 
            this.radPanel7.Controls.Add(this.txtSatkerYangMenyetujui);
            this.radPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel7.Location = new System.Drawing.Point(865, 763);
            this.radPanel7.Name = "radPanel7";
            this.radPanel7.Size = new System.Drawing.Size(230, 44);
            this.radPanel7.TabIndex = 18;
            // 
            // txtSatkerYangMenyetujui
            // 
            this.txtSatkerYangMenyetujui.AutoSize = false;
            this.txtSatkerYangMenyetujui.BackColor = System.Drawing.Color.White;
            this.txtSatkerYangMenyetujui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSatkerYangMenyetujui.Location = new System.Drawing.Point(0, 0);
            this.txtSatkerYangMenyetujui.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtSatkerYangMenyetujui.Name = "txtSatkerYangMenyetujui";
            this.txtSatkerYangMenyetujui.Size = new System.Drawing.Size(230, 44);
            this.txtSatkerYangMenyetujui.TabIndex = 5;
            // 
            // lblDetailSurat
            // 
            this.lblDetailSurat.AutoSize = false;
            this.lblDetailSurat.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailSurat.Location = new System.Drawing.Point(0, 56);
            this.lblDetailSurat.Name = "lblDetailSurat";
            this.lblDetailSurat.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetailSurat.Size = new System.Drawing.Size(1183, 52);
            this.lblDetailSurat.TabIndex = 75;
            this.lblDetailSurat.Text = "Please Fill All Required Fields.";
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.lblTitle);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Margin = new System.Windows.Forms.Padding(4);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1183, 56);
            this.headerPanel.TabIndex = 71;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(1183, 56);
            this.lblTitle.TabIndex = 71;
            // 
            // cameraSource
            // 
            this.cameraSource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cameraSource.Location = new System.Drawing.Point(32700, 320);
            this.cameraSource.Name = "cameraSource";
            this.cameraSource.Size = new System.Drawing.Size(581, 186);
            this.cameraSource.TabIndex = 82;
            this.cameraSource.Visible = false;
            // 
            // FormTerimaTahanan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.cameraSource);
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radPanelError);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblDetailSurat);
            this.Controls.Add(this.headerPanel);
            this.Name = "FormTerimaTahanan";
            this.Size = new System.Drawing.Size(1183, 736);
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).EndInit();
            this.radPanelError.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            this.radPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tpDiterima)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel22)).EndInit();
            this.radPanel22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSatkerYangMenyerahkan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel21)).EndInit();
            this.radPanel21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNrpYangMenyerahkan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWaktuTerima)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoCell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel20)).EndInit();
            this.radPanel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dpTanggalTerima)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel17)).EndInit();
            this.radPanel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNoCell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel19)).EndInit();
            this.radPanel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlBonTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel18)).EndInit();
            this.radPanel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoBonTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaYangMenyetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel9)).EndInit();
            this.radPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaYangMenyetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel15)).EndInit();
            this.radPanel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaYangMenyerahkan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaYangMenyerahkan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYangMenyerahkan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYangMenerima)).EndInit();
            this.lblYangMenerima.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblNamaYangMenerima)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel16)).EndInit();
            this.radPanel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlPangkatYangMenyerahkan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel11)).EndInit();
            this.radPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNamaYangMenerima)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel10)).EndInit();
            this.radPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNrpYangMenerima)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel12)).EndInit();
            this.radPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlPangkatYangMenerima)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSatkerYangMenerima)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYangMenyetujui)).EndInit();
            this.lblYangMenyetujui.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNrpYangMenyetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).EndInit();
            this.radPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlPangkatYangMenyetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).EndInit();
            this.radPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSatkerYangMenyetujui)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).EndInit();
            this.headerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList imageList;
        private Telerik.WinControls.UI.RadPanel headerPanel;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel lblDetailSurat;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel lblNamaYangMenyerahkan;
        private Telerik.WinControls.UI.RadLabel lblNamaTahanan;
        private Telerik.WinControls.UI.RadLabel lblNoBonTahanan;
        private Telerik.WinControls.UI.RadLabel lblNamaYangMenerima;
        private Telerik.WinControls.UI.RadLabel lblDetailVisitor;
        private Telerik.WinControls.UI.RadPanel radPanelError;
        private Telerik.WinControls.UI.RadLabel lblError;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnBack;
        private Telerik.WinControls.UI.RadButton btnSave;
        private CameraSource cameraSource;
        private Telerik.WinControls.UI.RadPanel radPanel6;
        private Telerik.WinControls.UI.RadLabel lblNamaYangMenyetujui;
        private Telerik.WinControls.UI.RadPanel radPanel15;
        private Telerik.WinControls.UI.RadLabel lblYangMenyerahkan;
        private Telerik.WinControls.UI.RadPanel radPanel16;
        private Telerik.WinControls.UI.RadTextBox txtNamaYangMenyerahkan;
        private Telerik.WinControls.UI.RadPanel radPanel18;
        private Telerik.WinControls.UI.RadPanel radPanel19;
        private Telerik.WinControls.UI.RadLabel lblYangMenerima;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadPanel radPanel12;
        private Telerik.WinControls.UI.RadPanel radPanel11;
        private Telerik.WinControls.UI.RadPanel radPanel10;
        private Telerik.WinControls.UI.RadPanel radPanel9;
        private Telerik.WinControls.UI.RadLabel lblYangMenyetujui;
        private System.Windows.Forms.Label label2;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel7;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadTextBox txtNamaYangMenerima;
        private Telerik.WinControls.UI.RadTextBox txtNrpYangMenerima;
        private Telerik.WinControls.UI.RadTextBox txtSatkerYangMenerima;
        private Telerik.WinControls.UI.RadTextBox txtNamaYangMenyetujui;
        private Telerik.WinControls.UI.RadTextBox txtNrpYangMenyetujui;
        private Telerik.WinControls.UI.RadTextBox txtSatkerYangMenyetujui;
        private Telerik.WinControls.UI.RadDropDownList ddlPangkatYangMenyetujui;
        private Telerik.WinControls.UI.RadDropDownList ddlPangkatYangMenerima;
        private Telerik.WinControls.UI.RadLabel lblWaktuTerima;
        private Telerik.WinControls.UI.RadLabel lblNoCell;
        private Telerik.WinControls.UI.RadPanel radPanel20;
        private Telerik.WinControls.UI.RadPanel radPanel17;
        private Telerik.WinControls.UI.RadDateTimePicker dpTanggalTerima;
        private Telerik.WinControls.UI.RadDropDownList ddlBonTahanan;
        private Telerik.WinControls.UI.RadTextBox txtNrpYangMenyerahkan;
        private Telerik.WinControls.UI.RadPanel radPanel21;
        private Telerik.WinControls.UI.RadPanel radPanel22;
        private Telerik.WinControls.UI.RadTextBox txtSatkerYangMenyerahkan;
        private Telerik.WinControls.UI.RadDropDownList ddlPangkatYangMenyerahkan;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadTimePicker tpDiterima;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadListView txtNamaTahanan;
        private Telerik.WinControls.UI.RadListView txtNoCell;
    }
}
