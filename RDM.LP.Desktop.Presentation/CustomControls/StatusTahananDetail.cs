﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using System.Threading;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class StatusTahananDetail : Base
    {
        private Point MouseDownLocation;
        public StatusTahananDetail()
        {
            InitializeComponent();
            InitForm();
            InitEvents();

        }

        private void LoadData(string id)
        {
            radPageView1.SelectedPage = radPageViewPage1;
            StatusTahananService stserv = new StatusTahananService();
            var data = stserv.GetById(Convert.ToInt32(id));
            if (data != null)
            {
                lblNama.Text = data.FullName;
                lblNoreg.Text = data.RegID;
                lblNosel.Text = data.NoSel;
                lblNoSkap.Text = data.NoSuratPenangkapan;
                lblTglKap.Text = data.TanggalPenangkapan.Value.ToString("dd MMMM yyyy");
                lblSpHan.Text = data.NoSuratPenahanan;
                lblTglSphan.Text = data.TanggalPenahanan.Value.ToString("dd MMMM yyyy");

                PerpanjanganTahananService prp = new PerpanjanganTahananService();
                var dt = prp.GetByStatusTahananId(Convert.ToInt32(this.Tag));
                if (dt != null)
                {
                    foreach (var item in dt)
                    {
                        if (item.PerpanjanganKe == "1")
                        {
                            var p1 = prp.GetByPerpanjanganKe("1");
                            if (p1 != null)
                            {
                                lblNoSuratP1.Text = p1.NoSurat;
                                lblTglSuratP1.Text = p1.TanggalPerpanjangan.Value.ToString("dd MMMM yyyy");
                                lblDurasiP1.Text = p1.Durasi.ToString();
                                lblFileP1.Text = p1.Dokumen;
                            }
                        }
                        else if (item.PerpanjanganKe == "2")
                        {
                            var p2 = prp.GetByPerpanjanganKe("2");
                            if (p2 != null)
                            {
                                lblNosuratP2.Text = p2.NoSurat;
                                lblTglSuratP2.Text = p2.TanggalPerpanjangan.Value.ToString("dd MMMM yyyy");
                                lblDurasiP2.Text = p2.Durasi.ToString();
                                lblFileP2.Text = p2.Dokumen;
                            }
                        }
                        else
                        {
                            var p3 = prp.GetByPerpanjanganKe("3");
                            if (p3 != null)
                            {
                                lblNoSuratP3.Text = p3.NoSurat;
                                lblTglSuratP3.Text = p3.TanggalPerpanjangan.Value.ToString("dd MMMM yyyy");
                                lblDurasiP3.Text = p3.Durasi.ToString();
                                lblFileP3.Text = p3.Dokumen;
                            }
                        }
                    }
                }
                inputter.Text = data.CreatedBy;
                inputdate.Text = data.CreatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss");
                updater.Text = data.UpdatedBy;
                updatedate.Text = data.UpdatedDate==null?string.Empty:data.UpdatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss");
            }
        }

        private void InitForm()
        {
            ToolTip toolTip = new ToolTip();
            toolTip.ToolTipIcon = ToolTipIcon.Info;
            toolTip.ToolTipTitle = "Info";
            toolTip.SetToolTip(exit, "Close Cell Details");

            btnClose.ButtonElement.CustomFontStyle = FontStyle.Bold;
            btnClose.ButtonElement.CustomFont = Global.MainFont;
            btnClose.ButtonElement.CustomFontSize = Global.CustomFontSizeMain;
            lblButton.BackColor = Global.MainColor;
            lblTitle.BackColor = Global.MainColor;
            lblTitle.Text = "      CELL DETAILS";
            lblTitle.ForeColor = Color.White;
            lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;
            lblTitle.LabelElement.CustomFont = Global.MainFont;
            lblTitle.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
            lblCellCode.ForeColor = Color.FromArgb(192, 0, 0);
            lblNama.ForeColor = Color.FromArgb(192, 0, 0);
        }

        private void InitEvents()
        {
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            exit.Click += exit_Click;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            this.VisibleChanged += Form_VisibleChanged;
            btnClose.Click += btnClose_Click;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (this.Tag != null)
                    LoadData(this.Tag.ToString());
                else
                    this.Hide();
            }
        }
    }
}
