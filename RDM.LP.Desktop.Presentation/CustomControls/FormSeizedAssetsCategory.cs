﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.DataAccess.Helper;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormSeizedAssetsCategory : Base
    {
        public TableLayoutPanel table1 { get { return this.tableLayoutPanel1; } }
        private Point MouseDownLocation;
        public FormSeizedAssetsCategory()
        {
            InitializeComponent();
            InitForm();
            LoadData();
            InitEvents();    
        }

        public void LoadData()
        {
            this.lblTitle.Text = "     TAMBAH KATEGORI BARANG YANG DISITA";

        }

        private void InitEvents()
        {
            //Events
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;

            txtNama.KeyDown += Control_KeyDown;
            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                InsertData();

                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Kategori Barang yang Disita Berhasil Disimpan !");
                UserFunction.LoadDDLCategory(MainForm.formMain.NewSeizedAssets.DDLCategory);
                MainForm.formMain.NewSeizedAssets.DDLCategory.Refresh();
                this.Hide();
                this.Tag = null;
            }
        }

        private void InsertData()
        {
            var name = txtNama.Text;
            SeizedassetsCategoryService categoryserv = new SeizedassetsCategoryService();
            var cat = new SeizedAssetsCategory
            {
                Nama = name,
                CreatedDate = DateTime.Now.ToLocalTime(),
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
            };
            categoryserv.Post(cat);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERNETWORK.ToString(), Activities = "Add New Seized Assets Category, Data=" + UserFunction.JsonString(cat) });
        }

        private void InitForm()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     TAMBAH KATEGORI BARANG YANG DISITA";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;
            this.lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 14f;
            this.lblDetailSurat.ForeColor = Color.White;
            this.lblDetailSurat.BackColor = Color.FromArgb(77,77,77);

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            UserFunction.ClearControls(tableLayoutPanel1);
            UserFunction.LoadDDLCategory(MainForm.formMain.NewSeizedAssets.DDLCategory);
            MainForm.formMain.NewSeizedAssets.DDLCategory.Refresh();
            this.Hide();
        }

        private bool DataValid()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            if (this.txtNama.Text == string.Empty)
            {
                radPanelError.Show();
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Nama Kategori!";
                return false;
            }

            return true;
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }
    }
}
