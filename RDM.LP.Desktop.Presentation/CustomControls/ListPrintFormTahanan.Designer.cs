﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class ListPrintFormTahanan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListPrintFormTahanan));
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.lblDetail = new Telerik.WinControls.UI.RadLabel();
            this.pdf = new System.Windows.Forms.PictureBox();
            this.excel = new System.Windows.Forms.PictureBox();
            this.gvPrintFormTahanan = new Telerik.WinControls.UI.RadGridView();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).BeginInit();
            this.lblDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPrintFormTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPrintFormTahanan.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDetail
            // 
            this.lblDetail.AutoSize = false;
            this.lblDetail.BackColor = System.Drawing.Color.Gainsboro;
            this.lblDetail.Controls.Add(this.pdf);
            this.lblDetail.Controls.Add(this.excel);
            this.lblDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetail.Location = new System.Drawing.Point(0, 56);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDetail.Size = new System.Drawing.Size(1349, 52);
            this.lblDetail.TabIndex = 16;
            this.lblDetail.Text = "DETAIL DATA";
            // 
            // pdf
            // 
            this.pdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pdf.Dock = System.Windows.Forms.DockStyle.Right;
            this.pdf.Image = ((System.Drawing.Image)(resources.GetObject("pdf.Image")));
            this.pdf.Location = new System.Drawing.Point(1263, 10);
            this.pdf.Name = "pdf";
            this.pdf.Size = new System.Drawing.Size(43, 32);
            this.pdf.TabIndex = 5;
            this.pdf.TabStop = false;
            // 
            // excel
            // 
            this.excel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.excel.Dock = System.Windows.Forms.DockStyle.Right;
            this.excel.Image = ((System.Drawing.Image)(resources.GetObject("excel.Image")));
            this.excel.Location = new System.Drawing.Point(1306, 10);
            this.excel.Name = "excel";
            this.excel.Size = new System.Drawing.Size(43, 32);
            this.excel.TabIndex = 4;
            this.excel.TabStop = false;
            // 
            // gvPrintFormTahanan
            // 
            this.gvPrintFormTahanan.AutoScroll = true;
            this.gvPrintFormTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvPrintFormTahanan.Location = new System.Drawing.Point(0, 108);
            // 
            // 
            // 
            this.gvPrintFormTahanan.MasterTemplate.AllowAddNewRow = false;
            this.gvPrintFormTahanan.MasterTemplate.AllowCellContextMenu = false;
            this.gvPrintFormTahanan.MasterTemplate.AllowColumnChooser = false;
            this.gvPrintFormTahanan.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.gvPrintFormTahanan.MasterTemplate.AllowColumnReorder = false;
            this.gvPrintFormTahanan.MasterTemplate.AllowDragToGroup = false;
            this.gvPrintFormTahanan.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gvPrintFormTahanan.Name = "gvPrintFormTahanan";
            this.gvPrintFormTahanan.Size = new System.Drawing.Size(1349, 413);
            this.gvPrintFormTahanan.TabIndex = 58;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(1349, 56);
            this.lblTitle.TabIndex = 1;
            // 
            // ListPrintFormTahanan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gvPrintFormTahanan);
            this.Controls.Add(this.lblDetail);
            this.Controls.Add(this.lblTitle);
            this.Name = "ListPrintFormTahanan";
            this.Size = new System.Drawing.Size(1349, 521);
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).EndInit();
            this.lblDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPrintFormTahanan.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPrintFormTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel lblDetail;
        private Telerik.WinControls.UI.RadGridView gvPrintFormTahanan;
        private System.Windows.Forms.PictureBox pdf;
        private System.Windows.Forms.PictureBox excel;
    }
}
