﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class FormStatusTahanan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormStatusTahanan));
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            this.lbl6 = new Telerik.WinControls.UI.RadLabel();
            this.lbl3 = new Telerik.WinControls.UI.RadLabel();
            this.lbl2 = new Telerik.WinControls.UI.RadLabel();
            this.lbl1 = new Telerik.WinControls.UI.RadLabel();
            this.lbl5 = new Telerik.WinControls.UI.RadLabel();
            this.lbl4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.tableFormCell = new System.Windows.Forms.TableLayoutPanel();
            this.lblDetail = new Telerik.WinControls.UI.RadLabel();
            this.PanelContainer = new Telerik.WinControls.UI.RadScrollablePanelContainer();
            this.radPanelError = new Telerik.WinControls.UI.RadPanel();
            this.lblError = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtNamatahanan = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel6 = new Telerik.WinControls.UI.RadPanel();
            this.tbNoSuratTahan = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel5 = new Telerik.WinControls.UI.RadPanel();
            this.dtTanggalPenangkapan = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.tbNoSuratTangkap = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.tbNoSel = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.tbNoReg = new Telerik.WinControls.UI.RadTextBox();
            this.lblDetailSurat = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.dtTanggalPenahanan = new Telerik.WinControls.UI.RadDateTimePicker();
            this.btnSearch = new Telerik.WinControls.UI.RadButton();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.tablePerpanjangan1 = new System.Windows.Forms.TableLayoutPanel();
            this.dtTanggalSuratP1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.tbFileP1 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.tbNoSuratP1 = new Telerik.WinControls.UI.RadTextBox();
            this.btnBrowseP1 = new Telerik.WinControls.UI.RadButton();
            this.ddlDurasi1 = new Telerik.WinControls.UI.RadDropDownList();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.tablePerpanjangan2 = new System.Windows.Forms.TableLayoutPanel();
            this.dtTanggalSuratP2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.tbFileP2 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.tbNoSuratP2 = new Telerik.WinControls.UI.RadTextBox();
            this.btnBrowseP2 = new Telerik.WinControls.UI.RadButton();
            this.ddlDurasi2 = new Telerik.WinControls.UI.RadDropDownList();
            this.radPageViewPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.tablePerpanjangan3 = new System.Windows.Forms.TableLayoutPanel();
            this.dtTanggalSuratP3 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.txtFileP3 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.tbNosuratP3 = new Telerik.WinControls.UI.RadTextBox();
            this.btnBrowseP3 = new Telerik.WinControls.UI.RadButton();
            this.ddlDurasi3 = new Telerik.WinControls.UI.RadDropDownList();
            this.lblRequired = new Telerik.WinControls.UI.RadLabel();
            this.headerPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnBack = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.lbl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            this.tableFormCell.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).BeginInit();
            this.radPanelError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNamatahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).BeginInit();
            this.radPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbNoSuratTahan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).BeginInit();
            this.radPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalPenangkapan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbNoSuratTangkap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbNoSel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbNoReg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            this.radPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalPenahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            this.tablePerpanjangan1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalSuratP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFileP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNoSuratP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBrowseP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlDurasi1)).BeginInit();
            this.radPageViewPage2.SuspendLayout();
            this.tablePerpanjangan2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalSuratP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFileP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNoSuratP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBrowseP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlDurasi2)).BeginInit();
            this.radPageViewPage3.SuspendLayout();
            this.tablePerpanjangan3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalSuratP3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFileP3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNosuratP3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBrowseP3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlDurasi3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).BeginInit();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = false;
            this.lbl6.Location = new System.Drawing.Point(14, 414);
            this.lbl6.Margin = new System.Windows.Forms.Padding(4);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(317, 42);
            this.lbl6.TabIndex = 120;
            this.lbl6.Text = "<html>Cell Status<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = false;
            this.lbl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl3.Location = new System.Drawing.Point(14, 164);
            this.lbl3.Margin = new System.Windows.Forms.Padding(4);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(317, 42);
            this.lbl3.TabIndex = 65;
            this.lbl3.Text = "<html>Cell Floor<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = false;
            this.lbl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl2.Location = new System.Drawing.Point(14, 114);
            this.lbl2.Margin = new System.Windows.Forms.Padding(4);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(317, 42);
            this.lbl2.TabIndex = 118;
            this.lbl2.Text = "<html>Building Name<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = false;
            this.lbl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl1.Location = new System.Drawing.Point(14, 64);
            this.lbl1.Margin = new System.Windows.Forms.Padding(4);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(317, 42);
            this.lbl1.TabIndex = 19;
            this.lbl1.Text = "<html>Cell Code<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = false;
            this.lbl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl5.Location = new System.Drawing.Point(14, 264);
            this.lbl5.Margin = new System.Windows.Forms.Padding(4);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(317, 42);
            this.lbl5.TabIndex = 66;
            this.lbl5.Text = "<html>Cell Description<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = false;
            this.lbl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl4.Location = new System.Drawing.Point(14, 214);
            this.lbl4.Margin = new System.Windows.Forms.Padding(4);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(317, 42);
            this.lbl4.TabIndex = 20;
            this.lbl4.Text = "<html>Cell Number<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel5.Location = new System.Drawing.Point(14, 364);
            this.radLabel5.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(317, 42);
            this.radLabel5.TabIndex = 121;
            this.radLabel5.Text = "<html>Cell Type<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // tableFormCell
            // 
            this.tableFormCell.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tableFormCell.ColumnCount = 3;
            this.tableFormCell.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tableFormCell.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableFormCell.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableFormCell.Controls.Add(this.lblDetail, 0, 0);
            this.tableFormCell.Controls.Add(this.radLabel5, 0, 7);
            this.tableFormCell.Controls.Add(this.lbl4, 0, 4);
            this.tableFormCell.Controls.Add(this.lbl5, 0, 5);
            this.tableFormCell.Controls.Add(this.lbl1, 0, 1);
            this.tableFormCell.Controls.Add(this.lbl2, 0, 2);
            this.tableFormCell.Controls.Add(this.lbl3, 0, 3);
            this.tableFormCell.Controls.Add(this.lbl6, 0, 8);
            this.tableFormCell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableFormCell.Location = new System.Drawing.Point(0, 0);
            this.tableFormCell.Margin = new System.Windows.Forms.Padding(10);
            this.tableFormCell.Name = "tableFormCell";
            this.tableFormCell.Padding = new System.Windows.Forms.Padding(10);
            this.tableFormCell.RowCount = 10;
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableFormCell.Size = new System.Drawing.Size(996, 530);
            this.tableFormCell.TabIndex = 17;
            // 
            // lblDetail
            // 
            this.lblDetail.AutoSize = false;
            this.tableFormCell.SetColumnSpan(this.lblDetail, 3);
            this.lblDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetail.Image = ((System.Drawing.Image)(resources.GetObject("lblDetail.Image")));
            this.lblDetail.Location = new System.Drawing.Point(13, 13);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetail.Size = new System.Drawing.Size(970, 44);
            this.lblDetail.TabIndex = 130;
            this.lblDetail.Text = "         DETAIL CELL DATA";
            // 
            // PanelContainer
            // 
            this.PanelContainer.AutoScroll = false;
            this.PanelContainer.Dock = System.Windows.Forms.DockStyle.None;
            this.PanelContainer.Location = new System.Drawing.Point(0, 0);
            this.PanelContainer.Size = new System.Drawing.Size(979, 548);
            // 
            // radPanelError
            // 
            this.radPanelError.Controls.Add(this.lblError);
            this.radPanelError.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanelError.Location = new System.Drawing.Point(0, 606);
            this.radPanelError.Margin = new System.Windows.Forms.Padding(0);
            this.radPanelError.Name = "radPanelError";
            this.radPanelError.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radPanelError.Size = new System.Drawing.Size(998, 43);
            this.radPanelError.TabIndex = 86;
            // 
            // lblError
            // 
            this.lblError.AutoSize = false;
            this.lblError.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.warning;
            this.lblError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblError.Location = new System.Drawing.Point(15, 0);
            this.lblError.Name = "lblError";
            this.lblError.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.lblError.Size = new System.Drawing.Size(983, 39);
            this.lblError.TabIndex = 24;
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.BackColor = System.Drawing.SystemColors.Control;
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 108);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(979, 496);
            this.radScrollablePanel1.Size = new System.Drawing.Size(998, 498);
            this.radScrollablePanel1.TabIndex = 85;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.01376F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.29956F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.686683F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 126F));
            this.tableLayoutPanel1.Controls.Add(this.txtNamatahanan, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel6, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.radPanel5, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailSurat, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radLabel3, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.radLabel4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.radLabel6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.radLabel7, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radLabel8, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.radLabel9, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.radLabel10, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.radPanel4, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.btnSearch, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPageView1, 0, 8);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(979, 617);
            this.tableLayoutPanel1.TabIndex = 70;
            // 
            // txtNamatahanan
            // 
            this.txtNamatahanan.AutoSize = false;
            this.txtNamatahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNamatahanan.Location = new System.Drawing.Point(288, 63);
            this.txtNamatahanan.MaxLength = 3;
            this.txtNamatahanan.Name = "txtNamatahanan";
            this.txtNamatahanan.ReadOnly = true;
            this.txtNamatahanan.Size = new System.Drawing.Size(496, 44);
            this.txtNamatahanan.TabIndex = 139;
            // 
            // radPanel6
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel6, 2);
            this.radPanel6.Controls.Add(this.tbNoSuratTahan);
            this.radPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel6.Location = new System.Drawing.Point(288, 313);
            this.radPanel6.Name = "radPanel6";
            this.radPanel6.Size = new System.Drawing.Size(551, 44);
            this.radPanel6.TabIndex = 5;
            // 
            // tbNoSuratTahan
            // 
            this.tbNoSuratTahan.AutoSize = false;
            this.tbNoSuratTahan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbNoSuratTahan.Location = new System.Drawing.Point(0, 0);
            this.tbNoSuratTahan.MaxLength = 100;
            this.tbNoSuratTahan.Name = "tbNoSuratTahan";
            this.tbNoSuratTahan.Size = new System.Drawing.Size(551, 44);
            this.tbNoSuratTahan.TabIndex = 140;
            // 
            // radPanel5
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel5, 2);
            this.radPanel5.Controls.Add(this.dtTanggalPenangkapan);
            this.radPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel5.Location = new System.Drawing.Point(288, 263);
            this.radPanel5.Name = "radPanel5";
            this.radPanel5.Size = new System.Drawing.Size(551, 44);
            this.radPanel5.TabIndex = 4;
            // 
            // dtTanggalPenangkapan
            // 
            this.dtTanggalPenangkapan.AutoSize = false;
            this.dtTanggalPenangkapan.Culture = new System.Globalization.CultureInfo("id-ID");
            this.dtTanggalPenangkapan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtTanggalPenangkapan.Location = new System.Drawing.Point(0, 0);
            this.dtTanggalPenangkapan.Name = "dtTanggalPenangkapan";
            this.dtTanggalPenangkapan.ReadOnly = true;
            this.dtTanggalPenangkapan.Size = new System.Drawing.Size(551, 44);
            this.dtTanggalPenangkapan.TabIndex = 1;
            this.dtTanggalPenangkapan.TabStop = false;
            this.dtTanggalPenangkapan.Text = "Selasa, 18 Juni 2019";
            this.dtTanggalPenangkapan.Value = new System.DateTime(2019, 6, 18, 18, 13, 5, 155);
            // 
            // radPanel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel3, 2);
            this.radPanel3.Controls.Add(this.tbNoSuratTangkap);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Location = new System.Drawing.Point(288, 213);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(551, 44);
            this.radPanel3.TabIndex = 3;
            // 
            // tbNoSuratTangkap
            // 
            this.tbNoSuratTangkap.AcceptsReturn = true;
            this.tbNoSuratTangkap.AutoSize = false;
            this.tbNoSuratTangkap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbNoSuratTangkap.Location = new System.Drawing.Point(0, 0);
            this.tbNoSuratTangkap.MaxLength = 100;
            this.tbNoSuratTangkap.Name = "tbNoSuratTangkap";
            this.tbNoSuratTangkap.Size = new System.Drawing.Size(551, 44);
            this.tbNoSuratTangkap.TabIndex = 3;
            // 
            // radPanel2
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel2, 2);
            this.radPanel2.Controls.Add(this.tbNoSel);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(288, 163);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(551, 44);
            this.radPanel2.TabIndex = 2;
            // 
            // tbNoSel
            // 
            this.tbNoSel.AutoSize = false;
            this.tbNoSel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbNoSel.Location = new System.Drawing.Point(0, 0);
            this.tbNoSel.MaxLength = 3;
            this.tbNoSel.Name = "tbNoSel";
            this.tbNoSel.ReadOnly = true;
            this.tbNoSel.Size = new System.Drawing.Size(551, 44);
            this.tbNoSel.TabIndex = 140;
            // 
            // radPanel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel1, 2);
            this.radPanel1.Controls.Add(this.tbNoReg);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(288, 113);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(551, 44);
            this.radPanel1.TabIndex = 1;
            // 
            // tbNoReg
            // 
            this.tbNoReg.AutoSize = false;
            this.tbNoReg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbNoReg.Location = new System.Drawing.Point(0, 0);
            this.tbNoReg.MaxLength = 3;
            this.tbNoReg.Name = "tbNoReg";
            this.tbNoReg.ReadOnly = true;
            this.tbNoReg.Size = new System.Drawing.Size(551, 44);
            this.tbNoReg.TabIndex = 140;
            // 
            // lblDetailSurat
            // 
            this.lblDetailSurat.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailSurat, 5);
            this.lblDetailSurat.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailSurat.Image = ((System.Drawing.Image)(resources.GetObject("lblDetailSurat.Image")));
            this.lblDetailSurat.Location = new System.Drawing.Point(13, 13);
            this.lblDetailSurat.Name = "lblDetailSurat";
            this.lblDetailSurat.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetailSurat.Size = new System.Drawing.Size(953, 44);
            this.lblDetailSurat.TabIndex = 138;
            this.lblDetailSurat.Text = "         DETAIL SEL";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel3.Location = new System.Drawing.Point(14, 314);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(267, 42);
            this.radLabel3.TabIndex = 121;
            this.radLabel3.Text = "<html>No Surat Penahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel4.Location = new System.Drawing.Point(14, 214);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(267, 42);
            this.radLabel4.TabIndex = 20;
            this.radLabel4.Text = "<html>No Surat Penagkapan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel6.Location = new System.Drawing.Point(14, 264);
            this.radLabel6.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(267, 42);
            this.radLabel6.TabIndex = 66;
            this.radLabel6.Text = "<html>Tanggal Pengangkapan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel7.Location = new System.Drawing.Point(14, 64);
            this.radLabel7.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(267, 42);
            this.radLabel7.TabIndex = 19;
            this.radLabel7.Text = "<html>Nama Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel8.Location = new System.Drawing.Point(14, 114);
            this.radLabel8.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(267, 42);
            this.radLabel8.TabIndex = 118;
            this.radLabel8.Text = "<html>No Registrasi  Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel9.Location = new System.Drawing.Point(14, 164);
            this.radLabel9.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(267, 42);
            this.radLabel9.TabIndex = 65;
            this.radLabel9.Text = "<html>No Sel Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel10
            // 
            this.radLabel10.AutoSize = false;
            this.radLabel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel10.Location = new System.Drawing.Point(14, 364);
            this.radLabel10.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(267, 42);
            this.radLabel10.TabIndex = 120;
            this.radLabel10.Text = "<html>Tanggal Penahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel4, 2);
            this.radPanel4.Controls.Add(this.dtTanggalPenahanan);
            this.radPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel4.Location = new System.Drawing.Point(288, 363);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(551, 44);
            this.radPanel4.TabIndex = 6;
            // 
            // dtTanggalPenahanan
            // 
            this.dtTanggalPenahanan.AutoSize = false;
            this.dtTanggalPenahanan.Culture = new System.Globalization.CultureInfo("id-ID");
            this.dtTanggalPenahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtTanggalPenahanan.Location = new System.Drawing.Point(0, 0);
            this.dtTanggalPenahanan.Name = "dtTanggalPenahanan";
            this.dtTanggalPenahanan.Size = new System.Drawing.Size(551, 44);
            this.dtTanggalPenahanan.TabIndex = 0;
            this.dtTanggalPenahanan.TabStop = false;
            this.dtTanggalPenahanan.Text = "Selasa, 18 Juni 2019";
            this.dtTanggalPenahanan.Value = new System.DateTime(2019, 6, 18, 18, 13, 5, 155);
            // 
            // btnSearch
            // 
            this.btnSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSearch.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.if_search_322497;
            this.btnSearch.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSearch.Location = new System.Drawing.Point(790, 63);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(49, 44);
            this.btnSearch.TabIndex = 140;
            // 
            // radPageView1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPageView1, 4);
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.Controls.Add(this.radPageViewPage3);
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(13, 413);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage3;
            this.radPageView1.Size = new System.Drawing.Size(953, 194);
            this.radPageView1.TabIndex = 141;
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.tablePerpanjangan1);
            this.radPageViewPage1.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(110F, 28F);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(932, 146);
            this.radPageViewPage1.Text = "Perpanjangan Ke 1";
            // 
            // tablePerpanjangan1
            // 
            this.tablePerpanjangan1.ColumnCount = 5;
            this.tablePerpanjangan1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.97526F));
            this.tablePerpanjangan1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.09546F));
            this.tablePerpanjangan1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.369099F));
            this.tablePerpanjangan1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.03863F));
            this.tablePerpanjangan1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.37339F));
            this.tablePerpanjangan1.Controls.Add(this.dtTanggalSuratP1, 1, 1);
            this.tablePerpanjangan1.Controls.Add(this.tbFileP1, 3, 1);
            this.tablePerpanjangan1.Controls.Add(this.radLabel17, 3, 0);
            this.tablePerpanjangan1.Controls.Add(this.radLabel18, 2, 0);
            this.tablePerpanjangan1.Controls.Add(this.radLabel19, 0, 0);
            this.tablePerpanjangan1.Controls.Add(this.radLabel20, 1, 0);
            this.tablePerpanjangan1.Controls.Add(this.tbNoSuratP1, 0, 1);
            this.tablePerpanjangan1.Controls.Add(this.btnBrowseP1, 4, 1);
            this.tablePerpanjangan1.Controls.Add(this.ddlDurasi1, 2, 1);
            this.tablePerpanjangan1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePerpanjangan1.Location = new System.Drawing.Point(0, 0);
            this.tablePerpanjangan1.Name = "tablePerpanjangan1";
            this.tablePerpanjangan1.RowCount = 2;
            this.tablePerpanjangan1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tablePerpanjangan1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tablePerpanjangan1.Size = new System.Drawing.Size(932, 146);
            this.tablePerpanjangan1.TabIndex = 3;
            this.tablePerpanjangan1.Tag = "1";
            // 
            // dtTanggalSuratP1
            // 
            this.dtTanggalSuratP1.AutoSize = false;
            this.dtTanggalSuratP1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dtTanggalSuratP1.Culture = new System.Globalization.CultureInfo("id-ID");
            this.dtTanggalSuratP1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dtTanggalSuratP1.Location = new System.Drawing.Point(264, 53);
            this.dtTanggalSuratP1.Name = "dtTanggalSuratP1";
            this.dtTanggalSuratP1.Size = new System.Drawing.Size(228, 44);
            this.dtTanggalSuratP1.TabIndex = 11;
            this.dtTanggalSuratP1.TabStop = false;
            this.dtTanggalSuratP1.Text = "Selasa, 18 Juni 2019";
            this.dtTanggalSuratP1.Value = new System.DateTime(2019, 6, 18, 18, 13, 5, 155);
            // 
            // tbFileP1
            // 
            this.tbFileP1.AutoSize = false;
            this.tbFileP1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tbFileP1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbFileP1.Location = new System.Drawing.Point(576, 53);
            this.tbFileP1.Name = "tbFileP1";
            this.tbFileP1.ReadOnly = true;
            this.tbFileP1.Size = new System.Drawing.Size(246, 94);
            this.tbFileP1.TabIndex = 10;
            // 
            // radLabel17
            // 
            this.radLabel17.AutoSize = false;
            this.radLabel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel17.Location = new System.Drawing.Point(576, 3);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(246, 44);
            this.radLabel17.TabIndex = 9;
            this.radLabel17.Text = "FILE";
            this.radLabel17.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel18
            // 
            this.radLabel18.AutoSize = false;
            this.radLabel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel18.Location = new System.Drawing.Point(498, 3);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(72, 44);
            this.radLabel18.TabIndex = 3;
            this.radLabel18.Text = "DURASI (HARI)";
            this.radLabel18.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel19
            // 
            this.radLabel19.AutoSize = false;
            this.radLabel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel19.Location = new System.Drawing.Point(3, 3);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(255, 44);
            this.radLabel19.TabIndex = 1;
            this.radLabel19.Text = "NO SURAT";
            this.radLabel19.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel20
            // 
            this.radLabel20.AutoSize = false;
            this.radLabel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel20.Location = new System.Drawing.Point(264, 3);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(228, 44);
            this.radLabel20.TabIndex = 2;
            this.radLabel20.Text = "TANGGAL SURAT";
            this.radLabel20.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbNoSuratP1
            // 
            this.tbNoSuratP1.AutoSize = false;
            this.tbNoSuratP1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tbNoSuratP1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbNoSuratP1.Location = new System.Drawing.Point(3, 53);
            this.tbNoSuratP1.Name = "tbNoSuratP1";
            this.tbNoSuratP1.Size = new System.Drawing.Size(255, 94);
            this.tbNoSuratP1.TabIndex = 4;
            // 
            // btnBrowseP1
            // 
            this.btnBrowseP1.Location = new System.Drawing.Point(828, 53);
            this.btnBrowseP1.Name = "btnBrowseP1";
            this.btnBrowseP1.Size = new System.Drawing.Size(101, 30);
            this.btnBrowseP1.TabIndex = 8;
            this.btnBrowseP1.Text = "Browse File";
            // 
            // ddlDurasi1
            // 
            this.ddlDurasi1.AutoSize = false;
            this.ddlDurasi1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ddlDurasi1.Location = new System.Drawing.Point(498, 53);
            this.ddlDurasi1.Name = "ddlDurasi1";
            this.ddlDurasi1.Size = new System.Drawing.Size(72, 44);
            this.ddlDurasi1.TabIndex = 12;
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.Controls.Add(this.tablePerpanjangan2);
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(110F, 28F);
            this.radPageViewPage2.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(932, 146);
            this.radPageViewPage2.Text = "Perpanjangan Ke 2";
            // 
            // tablePerpanjangan2
            // 
            this.tablePerpanjangan2.ColumnCount = 5;
            this.tablePerpanjangan2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.21888F));
            this.tablePerpanjangan2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.75107F));
            this.tablePerpanjangan2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.047211F));
            this.tablePerpanjangan2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.53648F));
            this.tablePerpanjangan2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.23176F));
            this.tablePerpanjangan2.Controls.Add(this.dtTanggalSuratP2, 1, 1);
            this.tablePerpanjangan2.Controls.Add(this.tbFileP2, 3, 1);
            this.tablePerpanjangan2.Controls.Add(this.radLabel12, 3, 0);
            this.tablePerpanjangan2.Controls.Add(this.radLabel13, 2, 0);
            this.tablePerpanjangan2.Controls.Add(this.radLabel14, 0, 0);
            this.tablePerpanjangan2.Controls.Add(this.radLabel15, 1, 0);
            this.tablePerpanjangan2.Controls.Add(this.tbNoSuratP2, 0, 1);
            this.tablePerpanjangan2.Controls.Add(this.btnBrowseP2, 4, 1);
            this.tablePerpanjangan2.Controls.Add(this.ddlDurasi2, 2, 1);
            this.tablePerpanjangan2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePerpanjangan2.Location = new System.Drawing.Point(0, 0);
            this.tablePerpanjangan2.Name = "tablePerpanjangan2";
            this.tablePerpanjangan2.RowCount = 3;
            this.tablePerpanjangan2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tablePerpanjangan2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 95F));
            this.tablePerpanjangan2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tablePerpanjangan2.Size = new System.Drawing.Size(932, 146);
            this.tablePerpanjangan2.TabIndex = 3;
            this.tablePerpanjangan2.Tag = "0";
            this.tablePerpanjangan2.Paint += new System.Windows.Forms.PaintEventHandler(this.tablePerpanjangan2_Paint);
            // 
            // dtTanggalSuratP2
            // 
            this.dtTanggalSuratP2.AutoSize = false;
            this.dtTanggalSuratP2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dtTanggalSuratP2.Culture = new System.Globalization.CultureInfo("id-ID");
            this.dtTanggalSuratP2.Dock = System.Windows.Forms.DockStyle.Top;
            this.dtTanggalSuratP2.Location = new System.Drawing.Point(266, 52);
            this.dtTanggalSuratP2.Name = "dtTanggalSuratP2";
            this.dtTanggalSuratP2.Size = new System.Drawing.Size(234, 44);
            this.dtTanggalSuratP2.TabIndex = 11;
            this.dtTanggalSuratP2.TabStop = false;
            this.dtTanggalSuratP2.Text = "Selasa, 18 Juni 2019";
            this.dtTanggalSuratP2.Value = new System.DateTime(2019, 6, 18, 18, 13, 5, 155);
            // 
            // tbFileP2
            // 
            this.tbFileP2.AutoSize = false;
            this.tbFileP2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tbFileP2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbFileP2.Location = new System.Drawing.Point(581, 52);
            this.tbFileP2.Name = "tbFileP2";
            this.tbFileP2.Size = new System.Drawing.Size(232, 89);
            this.tbFileP2.TabIndex = 10;
            // 
            // radLabel12
            // 
            this.radLabel12.AutoSize = false;
            this.radLabel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel12.Location = new System.Drawing.Point(581, 3);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(232, 43);
            this.radLabel12.TabIndex = 9;
            this.radLabel12.Text = "FILE";
            this.radLabel12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel13
            // 
            this.radLabel13.AutoSize = false;
            this.radLabel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel13.Location = new System.Drawing.Point(506, 3);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(69, 43);
            this.radLabel13.TabIndex = 3;
            this.radLabel13.Text = "DURASI (HARI)";
            this.radLabel13.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel14
            // 
            this.radLabel14.AutoSize = false;
            this.radLabel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel14.Location = new System.Drawing.Point(3, 3);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(257, 43);
            this.radLabel14.TabIndex = 1;
            this.radLabel14.Text = "NO SURAT";
            this.radLabel14.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel15
            // 
            this.radLabel15.AutoSize = false;
            this.radLabel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel15.Location = new System.Drawing.Point(266, 3);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(234, 43);
            this.radLabel15.TabIndex = 2;
            this.radLabel15.Text = "TANGGAL SURAT";
            this.radLabel15.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbNoSuratP2
            // 
            this.tbNoSuratP2.AutoSize = false;
            this.tbNoSuratP2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tbNoSuratP2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbNoSuratP2.Location = new System.Drawing.Point(3, 52);
            this.tbNoSuratP2.Name = "tbNoSuratP2";
            this.tbNoSuratP2.Size = new System.Drawing.Size(257, 89);
            this.tbNoSuratP2.TabIndex = 4;
            // 
            // btnBrowseP2
            // 
            this.btnBrowseP2.Location = new System.Drawing.Point(819, 52);
            this.btnBrowseP2.Name = "btnBrowseP2";
            this.btnBrowseP2.Size = new System.Drawing.Size(108, 30);
            this.btnBrowseP2.TabIndex = 8;
            this.btnBrowseP2.Text = "Browse File";
            // 
            // ddlDurasi2
            // 
            this.ddlDurasi2.AutoSize = false;
            this.ddlDurasi2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ddlDurasi2.Location = new System.Drawing.Point(506, 52);
            this.ddlDurasi2.Name = "ddlDurasi2";
            this.ddlDurasi2.Size = new System.Drawing.Size(69, 44);
            this.ddlDurasi2.TabIndex = 12;
            // 
            // radPageViewPage3
            // 
            this.radPageViewPage3.Controls.Add(this.tablePerpanjangan3);
            this.radPageViewPage3.ItemSize = new System.Drawing.SizeF(110F, 28F);
            this.radPageViewPage3.Location = new System.Drawing.Point(10, 37);
            this.radPageViewPage3.Name = "radPageViewPage3";
            this.radPageViewPage3.Size = new System.Drawing.Size(932, 146);
            this.radPageViewPage3.Text = "Perpanjangan Ke 3";
            // 
            // tablePerpanjangan3
            // 
            this.tablePerpanjangan3.ColumnCount = 5;
            this.tablePerpanjangan3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.21888F));
            this.tablePerpanjangan3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.75107F));
            this.tablePerpanjangan3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.047211F));
            this.tablePerpanjangan3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.96567F));
            this.tablePerpanjangan3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.80258F));
            this.tablePerpanjangan3.Controls.Add(this.dtTanggalSuratP3, 1, 1);
            this.tablePerpanjangan3.Controls.Add(this.txtFileP3, 3, 1);
            this.tablePerpanjangan3.Controls.Add(this.radLabel16, 3, 0);
            this.tablePerpanjangan3.Controls.Add(this.radLabel11, 2, 0);
            this.tablePerpanjangan3.Controls.Add(this.radLabel1, 0, 0);
            this.tablePerpanjangan3.Controls.Add(this.radLabel2, 1, 0);
            this.tablePerpanjangan3.Controls.Add(this.tbNosuratP3, 0, 1);
            this.tablePerpanjangan3.Controls.Add(this.btnBrowseP3, 4, 1);
            this.tablePerpanjangan3.Controls.Add(this.ddlDurasi3, 2, 1);
            this.tablePerpanjangan3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePerpanjangan3.Location = new System.Drawing.Point(0, 0);
            this.tablePerpanjangan3.Name = "tablePerpanjangan3";
            this.tablePerpanjangan3.RowCount = 3;
            this.tablePerpanjangan3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tablePerpanjangan3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tablePerpanjangan3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tablePerpanjangan3.Size = new System.Drawing.Size(932, 146);
            this.tablePerpanjangan3.TabIndex = 2;
            this.tablePerpanjangan3.Tag = "0";
            this.tablePerpanjangan3.Paint += new System.Windows.Forms.PaintEventHandler(this.tablePerpanjangan3_Paint);
            // 
            // dtTanggalSuratP3
            // 
            this.dtTanggalSuratP3.AutoSize = false;
            this.dtTanggalSuratP3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dtTanggalSuratP3.Culture = new System.Globalization.CultureInfo("id-ID");
            this.dtTanggalSuratP3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dtTanggalSuratP3.Location = new System.Drawing.Point(266, 53);
            this.dtTanggalSuratP3.Name = "dtTanggalSuratP3";
            this.dtTanggalSuratP3.Size = new System.Drawing.Size(234, 44);
            this.dtTanggalSuratP3.TabIndex = 11;
            this.dtTanggalSuratP3.TabStop = false;
            this.dtTanggalSuratP3.Text = "Selasa, 18 Juni 2019";
            this.dtTanggalSuratP3.Value = new System.DateTime(2019, 6, 18, 18, 13, 5, 155);
            // 
            // txtFileP3
            // 
            this.txtFileP3.AutoSize = false;
            this.txtFileP3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtFileP3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFileP3.Location = new System.Drawing.Point(581, 53);
            this.txtFileP3.Name = "txtFileP3";
            this.txtFileP3.Size = new System.Drawing.Size(236, 94);
            this.txtFileP3.TabIndex = 10;
            // 
            // radLabel16
            // 
            this.radLabel16.AutoSize = false;
            this.radLabel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel16.Location = new System.Drawing.Point(581, 3);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(236, 44);
            this.radLabel16.TabIndex = 9;
            this.radLabel16.Text = "FILE";
            this.radLabel16.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel11
            // 
            this.radLabel11.AutoSize = false;
            this.radLabel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel11.Location = new System.Drawing.Point(506, 3);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(69, 44);
            this.radLabel11.TabIndex = 3;
            this.radLabel11.Text = "DURASI (HARI)";
            this.radLabel11.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.Location = new System.Drawing.Point(3, 3);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(257, 44);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "NO SURAT";
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel2.Location = new System.Drawing.Point(266, 3);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(234, 44);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "TANGGAL SURAT";
            this.radLabel2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbNosuratP3
            // 
            this.tbNosuratP3.AutoSize = false;
            this.tbNosuratP3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tbNosuratP3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbNosuratP3.Location = new System.Drawing.Point(3, 53);
            this.tbNosuratP3.Name = "tbNosuratP3";
            this.tbNosuratP3.Size = new System.Drawing.Size(257, 94);
            this.tbNosuratP3.TabIndex = 4;
            // 
            // btnBrowseP3
            // 
            this.btnBrowseP3.Location = new System.Drawing.Point(823, 53);
            this.btnBrowseP3.Name = "btnBrowseP3";
            this.btnBrowseP3.Size = new System.Drawing.Size(105, 30);
            this.btnBrowseP3.TabIndex = 8;
            this.btnBrowseP3.Text = "Browse File";
            // 
            // ddlDurasi3
            // 
            this.ddlDurasi3.AutoSize = false;
            this.ddlDurasi3.Dock = System.Windows.Forms.DockStyle.Top;
            radListDataItem1.Text = "20";
            radListDataItem2.Text = "30";
            radListDataItem3.Text = "60";
            radListDataItem4.Text = "90";
            radListDataItem5.Text = "120";
            radListDataItem6.Text = "190";
            this.ddlDurasi3.Items.Add(radListDataItem1);
            this.ddlDurasi3.Items.Add(radListDataItem2);
            this.ddlDurasi3.Items.Add(radListDataItem3);
            this.ddlDurasi3.Items.Add(radListDataItem4);
            this.ddlDurasi3.Items.Add(radListDataItem5);
            this.ddlDurasi3.Items.Add(radListDataItem6);
            this.ddlDurasi3.Location = new System.Drawing.Point(506, 53);
            this.ddlDurasi3.Name = "ddlDurasi3";
            this.ddlDurasi3.Size = new System.Drawing.Size(69, 44);
            this.ddlDurasi3.TabIndex = 12;
            // 
            // lblRequired
            // 
            this.lblRequired.AutoSize = false;
            this.lblRequired.BackColor = System.Drawing.SystemColors.Control;
            this.lblRequired.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblRequired.Location = new System.Drawing.Point(0, 56);
            this.lblRequired.Name = "lblRequired";
            this.lblRequired.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblRequired.Size = new System.Drawing.Size(998, 52);
            this.lblRequired.TabIndex = 84;
            this.lblRequired.Text = "Mohon Isi seluruh Kolom yang Diperlukan";
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.lblTitle);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Margin = new System.Windows.Forms.Padding(4);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(998, 56);
            this.headerPanel.TabIndex = 81;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(998, 56);
            this.lblTitle.TabIndex = 8;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnBack);
            this.lblButton.Controls.Add(this.btnSave);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 649);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(998, 118);
            this.lblButton.TabIndex = 74;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.btnBack_Image;
            this.btnBack.Location = new System.Drawing.Point(30, 30);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(244, 58);
            this.btnBack.TabIndex = 8;
            this.btnBack.Tag = "";
            this.btnBack.Text = "&BATAL";
            this.btnBack.ThemeName = "MaterialBlueGrey";
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(663, 30);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(305, 58);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "&SIMPAN";
            this.btnSave.ThemeName = "MaterialBlueGrey";
            // 
            // FormStatusTahanan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.radPanelError);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblRequired);
            this.Controls.Add(this.headerPanel);
            this.Controls.Add(this.lblButton);
            this.Name = "FormStatusTahanan";
            this.Size = new System.Drawing.Size(998, 767);
            ((System.ComponentModel.ISupportInitialize)(this.lbl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            this.tableFormCell.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).EndInit();
            this.radPanelError.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNamatahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).EndInit();
            this.radPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbNoSuratTahan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).EndInit();
            this.radPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalPenangkapan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbNoSuratTangkap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbNoSel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbNoReg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            this.radPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalPenahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            this.tablePerpanjangan1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalSuratP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFileP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNoSuratP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBrowseP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlDurasi1)).EndInit();
            this.radPageViewPage2.ResumeLayout(false);
            this.tablePerpanjangan2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalSuratP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFileP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNoSuratP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBrowseP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlDurasi2)).EndInit();
            this.radPageViewPage3.ResumeLayout(false);
            this.tablePerpanjangan3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalSuratP3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFileP3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbNosuratP3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBrowseP3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlDurasi3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).EndInit();
            this.headerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnBack;
        private Telerik.WinControls.UI.RadButton btnSave;
        private Telerik.WinControls.UI.RadPanel headerPanel;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel lbl6;
        private System.Windows.Forms.TableLayoutPanel tableFormCell;
        private Telerik.WinControls.UI.RadLabel lblDetail;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel lbl4;
        private Telerik.WinControls.UI.RadLabel lbl5;
        private Telerik.WinControls.UI.RadLabel lbl1;
        private Telerik.WinControls.UI.RadLabel lbl2;
        private Telerik.WinControls.UI.RadLabel lbl3;
        private Telerik.WinControls.UI.RadLabel lblRequired;
        private Telerik.WinControls.UI.RadScrollablePanelContainer PanelContainer;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private Telerik.WinControls.UI.RadPanel radPanelError;
        private Telerik.WinControls.UI.RadLabel lblError;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel lblDetailSurat;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel6;
        private Telerik.WinControls.UI.RadPanel radPanel5;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadTextBox txtNamatahanan;
        private Telerik.WinControls.UI.RadButton btnSearch;
        private Telerik.WinControls.UI.RadTextBox tbNoSuratTangkap;
        private Telerik.WinControls.UI.RadTextBox tbNoSel;
        private Telerik.WinControls.UI.RadTextBox tbNoReg;
        private Telerik.WinControls.UI.RadTextBox tbNoSuratTahan;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage3;
        private System.Windows.Forms.TableLayoutPanel tablePerpanjangan3;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox tbNosuratP3;
        private Telerik.WinControls.UI.RadButton btnBrowseP3;
        private Telerik.WinControls.UI.RadDateTimePicker dtTanggalPenangkapan;
        private Telerik.WinControls.UI.RadDateTimePicker dtTanggalPenahanan;
        private Telerik.WinControls.UI.RadTextBox txtFileP3;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadDateTimePicker dtTanggalSuratP3;
        private System.Windows.Forms.TableLayoutPanel tablePerpanjangan1;
        private Telerik.WinControls.UI.RadDateTimePicker dtTanggalSuratP1;
        private Telerik.WinControls.UI.RadTextBox tbFileP1;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadTextBox tbNoSuratP1;
        private Telerik.WinControls.UI.RadButton btnBrowseP1;
        private System.Windows.Forms.TableLayoutPanel tablePerpanjangan2;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadDateTimePicker dtTanggalSuratP2;
        private Telerik.WinControls.UI.RadTextBox tbFileP2;
        private Telerik.WinControls.UI.RadTextBox tbNoSuratP2;
        private Telerik.WinControls.UI.RadButton btnBrowseP2;
        private Telerik.WinControls.UI.RadDropDownList ddlDurasi1;
        private Telerik.WinControls.UI.RadDropDownList ddlDurasi2;
        private Telerik.WinControls.UI.RadDropDownList ddlDurasi3;
    }
}
