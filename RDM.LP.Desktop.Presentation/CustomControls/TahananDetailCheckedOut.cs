﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using System.Threading;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class TahananDetailCheckedOut : Base
    {
        private Point MouseDownLocation;
        private GetInmatesCheckedOut data;
        public Button btnCheckOutx { get { return this.btnCheckOut; } }

        public TahananDetailCheckedOut()
        {
            InitializeComponent();
            InitForm();
            InitEvents();

        }

        private void LoadData(string id)
        {
            InmatesIsCheckedOutService co = new InmatesIsCheckedOutService();
            data = co.GetDetailById(Convert.ToInt32(id));
            if (data != null)
            {
                idregister.Text = data.RegID;
                jeniskelamin.Text = data.Gender;
                alamat.Text = data.Address;
                namatahanan.Text = data.InmatesName;
                btnCheckOut.Tag = data.RegID;
                waktumulai.Text = data.DateReg.Value.ToString("dd MMMM yyyy HH:mm");
                waktuselesai.Text = data.DateCheckedOut==null?string.Empty: data.DateCheckedOut.Value.ToString("dd MMMM yyyy HH:mm");
                notes.Text = data.CheckedOutNote;
                inputter.Text = data.CreatedBy;
                inputdate.Text = data.CreatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss");
                updater.Text = data.UpdatedBy;
                updatedate.Text = data.UpdatedDate==null?string.Empty:data.UpdatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss");
            }
        }

        private void InitForm()
        {
            ToolTip toolTip = new ToolTip();
            toolTip.ToolTipIcon = ToolTipIcon.Info;
            toolTip.ToolTipTitle = "Info";
            toolTip.SetToolTip(exit, "Tutup Informasi Tahanan Keluar");
            lblButton.BackColor = Global.MainColor;
            lblTitle.BackColor = Global.MainColor;
            lblTitle.Text = "      INFORMASI TAHANAN KELUAR";
            lblTitle.ForeColor = Color.White;
            lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;
            lblTitle.LabelElement.CustomFont = Global.MainFont;
            lblTitle.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            btnClose.ButtonElement.CustomFontStyle = FontStyle.Bold;
            btnClose.ButtonElement.CustomFont = Global.MainFont;
            btnClose.ButtonElement.CustomFontSize = Global.CustomFontSizeMain;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
        }

        private void InitEvents()
        {
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            exit.Click += exit_Click;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            this.VisibleChanged += Form_VisibleChanged;
            btnClose.Click += btnClose_Click;
            btnCheckOut.Click += btnCheckOut_Click;
        }

        private void btnCheckOut_Click(object sender, EventArgs e)
        {
            if (InmateCanCheckedOut())
            {
                MainForm.formMain.CheckedOutInmates.Tag = this.Tag;
                MainForm.formMain.CheckedOutInmates.Show();
            }
        }

        public bool InmateCanCheckedOut()
        {
            CellAllocationService alloc = new CellAllocationService();
            var allocationcell = alloc.GetByRegId(Convert.ToString(btnCheckOut.Tag));
            if (allocationcell != null)
            {
                var x = alloc.GetInmatesCheckoutCell(allocationcell.InmatesRegCode);
                if (x == null)
                {
                    return true;
                }
                if (x.InmatesRegCode.Equals(allocationcell.InmatesRegCode))
                {
                    RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "Tahanan Masih Terdaftar di ALOKASI SEL, Silakan Check Out Terlebih Dahulu", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);

                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "Tahanan belum dialokasikan ke dalam sel", "Error", MessageBoxButtons.OK, RadMessageIcon.Error);

                return false;
            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (this.Tag != null)
                    LoadData(this.Tag.ToString());
                else
                    this.Hide();
            }
        }
    }
}
