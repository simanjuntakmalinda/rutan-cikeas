﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListCellAllocation : Base
    {
        public RadGridView GvAllocation { get { return this.gvAllocation; } }
        public ListCellAllocation()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvAllocation.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvAllocation.RowFormatting += radGridView_RowFormatting;
            gvAllocation.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvAllocation.CellClick += gvAllocation_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
            picRefresh.Click += PicRefresh_Click;
        }

        private void PicRefresh_Click(object sender, EventArgs e)
        {
            UserFunction.LoadDataAllocationToGrid(gvAllocation);
            gvAllocation.Refresh();
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarAlokasiSel.pdf", "CELL ALLOCATION LIST DATA", gvAllocation);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarAlokasiSel.csv", "CELL ALLOCATION LIST DATA", gvAllocation);
        }

        private void gvAllocation_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == null)
                return;

            switch (e.Column.Index)
            {
                case 0:
                    MainForm.formMain.CellAllocationDetail.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.CellAllocationDetail.Show();
                    break;
                //case 1:
                //    MainForm.formMain.CellAllocation.Tag = e.Row.Cells["Id"].Value.ToString();
                //    MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.AddNew;
                //    MainForm.formMain.CellAllocation.LoadData();
                //    break;
                case 1:
                    MainForm.formMain.CheckedOut.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.CheckedOut.Show();
                    break;
            }
            
        }

        private void DeleteUser(string inmatesregcode)
        {
            using (CellAllocationService serv = new CellAllocationService())
            {
                serv.DeleteByInmatesRegCode(inmatesregcode);
            }
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DAFTAR DATA ALOKASI SEL";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            GridViewImageColumn btnView = new GridViewImageColumn();
            btnView.HeaderText = "";
            btnView.Name = "btnView";
            gvAllocation.AutoGenerateColumns = false;
            gvAllocation.Columns.Insert(0, btnView);
            gvAllocation.Refresh();
            gvAllocation.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            GridViewImageColumn btnUbah = new GridViewImageColumn();
            btnUbah.HeaderText = "";
            btnUbah.Name = "btnUbah";
            gvAllocation.AutoGenerateColumns = false;
            gvAllocation.Columns.Insert(1, btnUbah);
            gvAllocation.Refresh();
            gvAllocation.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnCheckedOut = new GridViewImageColumn();
            btnCheckedOut.HeaderText = "";
            btnCheckedOut.Name = "btnCheckedOut";
            gvAllocation.AutoGenerateColumns = false;
            gvAllocation.Columns.Insert(2, btnCheckedOut);
            gvAllocation.Refresh();
            gvAllocation.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            UserFunction.LoadDataAllocationToGrid(gvAllocation);
            UserFunction.SetInitGridView(gvAllocation);
            
        }
    }
}
