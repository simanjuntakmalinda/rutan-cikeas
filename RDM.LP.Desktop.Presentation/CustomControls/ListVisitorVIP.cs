﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListVisitorVIP : Base
    {
        public RadGridView GvVisitorVIP { get { return this.gvVisitorVIPSudahCheckedOut; } }
        private VisitorVIP data;

        public ListVisitorVIP()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvVisitorVIPSudahCheckedOut.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvVisitorVIPSudahCheckedOut.RowFormatting += radGridView_RowFormatting;
            gvVisitorVIPSudahCheckedOut.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvVisitorVIPSudahCheckedOut.CellClick += gvVisitor_CellClick;
            excelSudahCheckedOut.Click += excel_Click;
            pdfSudahCheckedOut.Click += pdf_Click;
            picRefresh.Click += PicRefresh_Click;
        }

        private void PicRefresh_Click(object sender, EventArgs e)
        {
            UserFunction.LoadDataPegunjungVIPToGrid(gvVisitorVIPSudahCheckedOut);
            gvVisitorVIPSudahCheckedOut.Refresh();
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarPengunjungVIP.pdf", "VIP VISITOR LIST DATA", gvVisitorVIPSudahCheckedOut);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarPengunjungVIP.csv", "VIP VISITOR LIST DATA", gvVisitorVIPSudahCheckedOut);
        }

        private void gvVisitor_CellClick(object sender, GridViewCellEventArgs e)
        {
            VisitorVIPService visitorserv = new VisitorVIPService();
            data = visitorserv.GetDetailById(Convert.ToString(e.Row.Cells["Id"].Value));

            if (e.Column == null)
                return;
            switch (e.Column.Index)
            {
                case 0:
                    MainForm.formMain.VisitorVIPDetail.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.VisitorVIPDetail.Show();
                    break;
                case 1:
                    MainForm.formMain.VisitorVIP.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.AddNew;
                    MainForm.formMain.VisitorVIP.LoadData();
                    break;
                case 2:

                    if (RadMessageBox.Show(new Form() { TopMost = true, StartPosition = FormStartPosition.CenterScreen }, "<html>Check Out Pengunjung :<br>Nama&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;" + data.FullName, "Checkout", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes)
                    {
                        VisitorVIPService visitor = new VisitorVIPService();
                        visitor.CheckOut((int)e.Row.Cells["Id"].Value, GlobalVariables.UserID);
                        UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERVISITOR.ToString(), Activities = "CheckOut Visitor VIP, Data=" + UserFunction.JsonString(data) });
                        UserFunction.MsgBox(TipeMsg.Info, "Berhasil Check Out!");

                        UserFunction.LoadDataPegunjungVIPToGrid(MainForm.formMain.ListVisitorVIP.GvVisitorVIP);
                        UserFunction.SetInitGridView(MainForm.formMain.ListVisitorVIP.GvVisitorVIP);

                        UserFunction.LoadDataKunjunganVIPToGrid(MainForm.formMain.ListKunjunganVIP.GvKunjungan);
                        UserFunction.SetInitGridView(MainForm.formMain.ListKunjunganVIP.GvKunjungan);
                        MainForm.formMain.VisitorVIP.LoadData();
                    }
                    break;
            }
            
        }

        private void DeleteVisitor(string Id)
        {
            using (VisitorVIPService pegunjungVIP = new VisitorVIPService())
            {
                var data = pegunjungVIP.GetDetailById(Id);
                pegunjungVIP.DeleteById(Id);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERVISITOR.ToString(), Activities = "Delete VisitorVIP, data=" + UserFunction.JsonString(data) });
            }
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DAFTAR PENGUNJUNG VIP";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;


            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Width = 15;
            btnDetail.Name = "btnDetail";
            gvVisitorVIPSudahCheckedOut.AutoGenerateColumns = false;
            gvVisitorVIPSudahCheckedOut.Columns.Insert(0, btnDetail);
            gvVisitorVIPSudahCheckedOut.Refresh();
            gvVisitorVIPSudahCheckedOut.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            GridViewImageColumn btnUbah = new GridViewImageColumn();
            btnUbah.HeaderText = "";
            btnUbah.Name = "btnUbah";
            gvVisitorVIPSudahCheckedOut.AutoGenerateColumns = false;
            gvVisitorVIPSudahCheckedOut.Columns.Insert(1, btnUbah);
            gvVisitorVIPSudahCheckedOut.Refresh();
            gvVisitorVIPSudahCheckedOut.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnHapus = new GridViewImageColumn();
            btnHapus.HeaderText = "";
            btnHapus.Name = "btnCheckout";
            gvVisitorVIPSudahCheckedOut.AutoGenerateColumns = false;
            gvVisitorVIPSudahCheckedOut.Columns.Insert(1, btnHapus);
            gvVisitorVIPSudahCheckedOut.Refresh();
            gvVisitorVIPSudahCheckedOut.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            UserFunction.LoadDataPegunjungVIPToGrid(gvVisitorVIPSudahCheckedOut);
            UserFunction.SetInitGridView(gvVisitorVIPSudahCheckedOut);

        }

    }
}
