﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using System.Threading;
using Telerik.WinControls.Primitives;

namespace RDM.LP.Desktop.Presentation.CustomControls
{

    public partial class Dashboard : UserControl
    {
        public PictureBox FullScreen { get { return this.full; }}
        public TableLayoutPanel NavigatorDashboard { get { return this.Navigator; } }
        public Dashboard()
        {
            InitializeComponent();
            InitEvents();
            InitForm();
            InitChart();
            LaodData();
        }

        private void InitEvents()
        {
            this.radChartInmates.ContextMenuOpening += Chart_ContextMenuOpening;
            this.radChartVisitor.ContextMenuOpening += Chart_ContextMenuOpening;
            next.Click += next_Click;
            prev.Click += prev_Click;
            timerDashboard.Tick += timerDashboard_Tick;
        }

        private void timerDashboard_Tick(object sender, EventArgs e)
        {
            Thread dasboardThread = new Thread(new ThreadStart(SwithPanelDashboard));
            dasboardThread.IsBackground = true;
            dasboardThread.Start();
        }

        private void prev_Click(object sender, EventArgs e)
        {
            SwithPanelDashboard();
        }

        private void SwithPanelDashboard()
        {
            this.Invoke((MethodInvoker)delegate
            {
                try
                {
                    Application.DoEvents();
                    panelDasboard.Visible = true;
                    Application.DoEvents();
                    PanelDashBoard.Visible = !PanelDashBoard.Visible;
                    Application.DoEvents();
                    PanelDashBoard2.Visible = !PanelDashBoard2.Visible;
                    Application.DoEvents();
                    if (!PanelDashBoard.Visible)
                    {
                        PanelDashBoard.Dock = DockStyle.Bottom;
                        PanelDashBoard2.Dock = DockStyle.Top;
                    }
                    else
                    {
                        PanelDashBoard.Dock = DockStyle.Top;
                        PanelDashBoard2.Dock = DockStyle.Bottom;
                    }
                    Application.DoEvents();
                    if (!PanelDashBoard2.Visible)
                    {
                        PanelDashBoard.Dock = DockStyle.Top;
                        PanelDashBoard2.Dock = DockStyle.Bottom;
                    }
                    else
                    {

                        PanelDashBoard.Dock = DockStyle.Bottom;
                        PanelDashBoard2.Dock = DockStyle.Top;
                    }
                    //Thread.Sleep(1500);
                    panelDasboard.Visible = false;
                }
                catch (Exception ex)
                {
                }
            });
        }

        private void next_Click(object sender, EventArgs e)
        {
            SwithPanelDashboard();
        }

        private void Chart_ContextMenuOpening(object sender, ChartViewContextMenuOpeningEventArgs e)
        {
            e.Cancel = true;
        }

        private void LaodData()
        {
            DataDasboard1();
            DataDasboard2();
        }

        private void DataDasboard2()
        {
            CellAllocationService cellallocationserv = new CellAllocationService();
            TotalWardA.Text = cellallocationserv.GetCountByWardType(1).ToString();
            TotalWardB.Text = cellallocationserv.GetCountByWardType(2).ToString();
            TotalWardC.Text = cellallocationserv.GetCountByWardType(3).ToString();

            var groupGenderA = cellallocationserv.GetCountByWardTypeGroupByGender(1);
            var groupGenderB = cellallocationserv.GetCountByWardTypeGroupByGender(2);
            var groupGenderC = cellallocationserv.GetCountByWardTypeGroupByGender(3);


            var grouReleigionA = cellallocationserv.GetCountByWardTypeGroupByReligion(1);
            var grouReleigionB = cellallocationserv.GetCountByWardTypeGroupByReligion(2);
            var grouReleigionC = cellallocationserv.GetCountByWardTypeGroupByReligion(3);



            WAA.Text = cellallocationserv.GetCountByWardTypeStatusAvailable(1).ToString();
            WAO.Text = cellallocationserv.GetCountByWardTypeStatusOccupied(1).ToString();
            WAM.Text = cellallocationserv.GetCountByWardTypeStatusMaintenance(1).ToString();


            WBA.Text = cellallocationserv.GetCountByWardTypeStatusAvailable(2).ToString();
            WBO.Text = cellallocationserv.GetCountByWardTypeStatusOccupied(2).ToString();
            WBM.Text = cellallocationserv.GetCountByWardTypeStatusMaintenance(2).ToString();


            WCA.Text = cellallocationserv.GetCountByWardTypeStatusAvailable(3).ToString();
            WCO.Text = cellallocationserv.GetCountByWardTypeStatusOccupied(3).ToString();
            WCM.Text = cellallocationserv.GetCountByWardTypeStatusMaintenance(3).ToString();



            lblDetailA.Text = "<html>";
            foreach (var item in groupGenderA)
            {
                lblDetailA.Text = lblDetailA.Text + item.Total.ToString().PadRight(4, ' ') +  item.Nama + "<br>";
            }
            lblDetailB.Text = "<html>";
            foreach (var item in groupGenderB)
            {
                lblDetailB.Text = lblDetailB.Text + item.Total.ToString().PadRight(4, ' ') +  item.Nama + "<br>";
            }
            lblDetailC.Text = "<html>";
            foreach (var item in groupGenderC)
            {
                lblDetailC.Text = lblDetailC.Text + item.Total.ToString().PadRight(4, ' ') +  item.Nama + "<br>";
            }

            lblDetailA.Text = lblDetailA.Text + "_________________________________________________________<br><br>";
            foreach (var item in grouReleigionA)
            {
                lblDetailA.Text = lblDetailA.Text + item.Total.ToString().PadRight(4, ' ') + item.Nama + "<br>";
            }

            lblDetailB.Text = lblDetailB.Text + "_________________________________________________________<br><br>";
            foreach (var item in grouReleigionB)
            {
                lblDetailB.Text = lblDetailB.Text + item.Total.ToString().PadRight(4, ' ') + item.Nama + "<br>";
            }

            lblDetailC.Text = lblDetailC.Text + "_________________________________________________________<br><br>";
            foreach (var item in grouReleigionC)
            {
                lblDetailC.Text = lblDetailC.Text + item.Total.ToString().PadRight(4, ' ') + item.Nama + "<br>";
            }

        }

        private void DataDasboard1()
        {
            InmatesService tahanserv = new InmatesService();
            lblTotInmates.Text = tahanserv.GetCountToday().ToString();

            VisitorService visitorserv = new VisitorService();
            lblTotVisitor.Text = visitorserv.GetCountToday().ToString();


            var dataInmatesLast30 = tahanserv.GetCountLast30days();
            CartesianArea area = this.radChartInmates.GetArea<CartesianArea>();
            CartesianGrid grid = area.GetGrid<CartesianGrid>();
            grid.ForeColor = Color.FromArgb(235, 235, 235);
            grid.DrawVerticalStripes = false;
            grid.DrawHorizontalStripes = true;
            grid.DrawHorizontalFills = false;
            grid.DrawVerticalFills = false;
            area.ShowGrid = true;

            LineSeries lineSeries = new LineSeries();
            lineSeries.PointSize = new SizeF(0, 0);
            lineSeries.CategoryMember = "Category";
            lineSeries.ValueMember = "Value";
            lineSeries.DataSource = dataInmatesLast30;
            lineSeries.BorderColor = Color.FromArgb(142, 196, 65);
            lineSeries.BorderWidth = 2;
            radChartInmates.Series.Add(lineSeries);

            LinearAxis axeY = radChartInmates.Axes.Get<LinearAxis>(1);
            axeY.Minimum = 0;
            axeY.Maximum = 10;
            axeY.MajorStep = 1;
            axeY.ForeColor = Color.Brown;
            axeY.Title = "Total";

            CategoricalAxis axeX = radChartInmates.Axes.Get<CategoricalAxis>(0);
            axeX.LabelInterval = 1;
            axeX.LabelFormat = "{0}";
            axeX.ForeColor = Color.Brown;


            var dataVisitorLast30 = visitorserv.GetCountLast30days();
            area = this.radChartVisitor.GetArea<CartesianArea>();
            grid = area.GetGrid<CartesianGrid>();
            grid.ForeColor = Color.FromArgb(235, 235, 235);
            grid.DrawVerticalStripes = false;
            grid.DrawHorizontalStripes = true;
            grid.DrawHorizontalFills = false;
            grid.DrawVerticalFills = false;
            area.ShowGrid = true;

            lineSeries = new LineSeries();
            lineSeries.PointSize = new SizeF(0, 0);
            lineSeries.CategoryMember = "Category";
            lineSeries.ValueMember = "Value";
            lineSeries.DataSource = dataVisitorLast30;
            lineSeries.BorderColor = Color.FromArgb(142, 196, 65);
            lineSeries.BorderWidth = 2;
            radChartVisitor.Series.Add(lineSeries);

            axeY = radChartVisitor.Axes.Get<LinearAxis>(1);
            axeY.Minimum = 0;
            axeY.Maximum = 150;
            axeY.MajorStep = 20;
            axeY.ForeColor = Color.Brown;
            axeY.Title = "Total";

            axeX = radChartVisitor.Axes.Get<CategoricalAxis>(0);
            axeX.LabelInterval = 1;
            axeX.LabelFormat = "{0}";
            axeX.ForeColor = Color.Brown;
        }

        private void InitChart()
        {
            this.radChartInmates.ChartElement.TitlePosition = TitlePosition.Top;
            this.radChartInmates.ChartElement.TitleElement.TextAlignment = ContentAlignment.MiddleLeft;
            this.radChartInmates.ChartElement.TitleElement.Margin = new Padding(10, 10, 0, 0);
            this.radChartInmates.ChartElement.TitleElement.Font = new Font(Global.MainFont, 8f, System.Drawing.FontStyle.Regular); ;
            this.radChartInmates.View.Margin = new Padding(15);
            this.radChartInmates.BackColor = Global.MainColorLight;
            this.radChartInmates.ChartElement.BorderWidth = 0;
            this.radChartInmates.ChartElement.CustomFontSize = 8f;
            this.radChartInmates.ChartElement.ForeColor = Color.Brown;

            this.radChartVisitor.ChartElement.TitlePosition = TitlePosition.Top;
            this.radChartVisitor.ChartElement.TitleElement.TextAlignment = ContentAlignment.MiddleLeft;
            this.radChartVisitor.ChartElement.TitleElement.Margin = new Padding(10, 10, 0, 0);
            this.radChartVisitor.ChartElement.TitleElement.Font = new Font(Global.MainFont, 8f, System.Drawing.FontStyle.Regular); ;
            this.radChartVisitor.View.Margin = new Padding(15);
            this.radChartVisitor.BackColor = Global.MainColorLight;
            this.radChartVisitor.ChartElement.BorderWidth = 0;
            this.radChartVisitor.ChartElement.CustomFontSize = 8f;
            this.radChartVisitor.ChartElement.ForeColor = Color.Brown;
        }

        private void InitForm()
        {
            timerDashboard.Interval = Properties.Settings.Default.DashboardRefresinterval==0?20: Properties.Settings.Default.DashboardRefresinterval * 1000;
            PanelDashBoard.Dock = DockStyle.Fill;
            lblTotInmates.RootElement.CustomFont = Global.MainFont;
            lblTotInmates.RootElement.CustomFontSize = 75f;
            lblTotInmates.RootElement.CustomFontStyle = FontStyle.Bold;
            lblInmates.RootElement.CustomFont = Global.MainFont;
            lblInmates.RootElement.CustomFontSize = 15f;
            lblInmatesToday.RootElement.CustomFont = Global.MainFont;
            lblInmatesToday.RootElement.CustomFontSize = 15f;

            lblTotVisitor.RootElement.CustomFont = Global.MainFont;
            lblTotVisitor.RootElement.CustomFontSize = 75f;
            lblTotVisitor.RootElement.CustomFontStyle = FontStyle.Bold;
            lblVisitor.RootElement.CustomFont = Global.MainFont;
            lblVisitor.RootElement.CustomFontSize = 15f;
            lblVisitorToday.RootElement.CustomFont = Global.MainFont;
            lblVisitorToday.RootElement.CustomFontSize = 15f;

            lblStat1.RootElement.CustomFont = Global.MainFont;
            lblStat1.ForeColor = Color.FromArgb(66,66,66);
            lblStat1.RootElement.CustomFontSize = 14f;
            lblStat1.RootElement.CustomFontStyle = FontStyle.Bold;

            lblStat2.RootElement.CustomFont = Global.MainFont;
            lblStat2.ForeColor = Color.FromArgb(66, 66, 66);
            lblStat2.RootElement.CustomFontSize = 14f;
            lblStat2.RootElement.CustomFontStyle = FontStyle.Bold;


            TotalWardA.RootElement.CustomFont = Global.MainFont;
            TotalWardA.RootElement.CustomFontSize = 75f;
            TotalWardB.RootElement.CustomFont = Global.MainFont;
            TotalWardB.RootElement.CustomFontSize = 75f;
            TotalWardC.RootElement.CustomFont = Global.MainFont;
            TotalWardC.RootElement.CustomFontSize = 75f;

            WardA.RootElement.CustomFont = Global.MainFont;
            WardA.RootElement.CustomFontSize = 17.5f;
            WardB.RootElement.CustomFontSize = 17.5f;
            WardB.RootElement.CustomFont = Global.MainFont;
            WardC.RootElement.CustomFontSize = 17.5f;
            WardC.RootElement.CustomFont = Global.MainFont;
            WardC.RootElement.CustomFontSize = 17.5f;

            lblDetailA.RootElement.CustomFont = Global.MainFont;
            lblDetailA.RootElement.CustomFontSize = 15f;
            lblDetailB.RootElement.CustomFont = Global.MainFont;
            lblDetailB.RootElement.CustomFontSize = 15f;
            lblDetailC.RootElement.CustomFont = Global.MainFont;
            lblDetailC.RootElement.CustomFontSize = 15f;

            TextPrimitive primitive = (TextPrimitive)TotalWardA.RootElement.Children[0].Children[2].Children[1];
            primitive = (TextPrimitive)TotalWardA.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 80, FontStyle.Bold);
            primitive.ForeColor = Color.BlanchedAlmond;
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 2), Color.Black);
            TotalWardA.Text = "0";

            primitive = (TextPrimitive)TotalWardB.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 80, FontStyle.Bold);
            primitive.ForeColor = Color.BlanchedAlmond;
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 2), Color.Black);
            TotalWardB.Text = "0";

            primitive = (TextPrimitive)TotalWardC.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 80, FontStyle.Bold);
            primitive.ForeColor = Color.BlanchedAlmond;
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 2), Color.Black);
            TotalWardC.Text = "0";


            primitive = (TextPrimitive)WardA.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 16, FontStyle.Bold);
            primitive.ForeColor = Color.LightYellow;
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 2), Color.Black);
            WardA.Text = "WARD A";

            primitive = (TextPrimitive)WardB.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 16, FontStyle.Bold);
            primitive.ForeColor = Color.LightYellow;
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 2), Color.Black);
            WardA.Text = "WARD B";

            primitive = (TextPrimitive)WardC.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 16, FontStyle.Bold);
            primitive.ForeColor = Color.LightYellow;
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 2), Color.Black);
            WardA.Text = "WARD A";


            primitive = (TextPrimitive)lblTotInmates.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 80, FontStyle.Bold);
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 2), Color.Black);

            primitive = (TextPrimitive)lblTotVisitor.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 80, FontStyle.Bold);
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 2), Color.Black);

            primitive = (TextPrimitive)WardC.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 16, FontStyle.Bold);
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 2), Color.Black);


            primitive = (TextPrimitive)lblInmates.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 16, FontStyle.Bold);
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 2), Color.Black);

            primitive = (TextPrimitive)lblInmatesToday.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 16, FontStyle.Bold);
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 2), Color.Black);

            primitive = (TextPrimitive)lblVisitor.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 16, FontStyle.Bold);
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 2), Color.Black);

            primitive = (TextPrimitive)lblVisitorToday.RootElement.Children[0].Children[2].Children[1];
            primitive.DisableHTMLRendering = true;
            primitive.Font = new Font(Global.MainFont, 16, FontStyle.Bold);
            primitive.Shadow = new Telerik.WinControls.Paint.ShadowSettings(new Point(2, 2), Color.Black);

            foreach (Control ctrl in tableLayoutPanel1.Controls)
            {
                if (ctrl.GetType() == typeof(RadLabel))
                {
                    RadLabel label = ((RadLabel)ctrl);
                    label.LabelElement.CustomFont = Global.MainFont;
                    label.LabelElement.CustomFontSize = Global.CustomFontSizeMain;
                    //label.ForeColor = Color.FromArgb(89, 89, 89);
                    label.ForeColor = Color.White;
                    label.BackColor = Color.FromArgb(0, 255, 255, 255);
                }
            }

            foreach (Control ctrl in tableLayoutPanel3.Controls)
            {
                if (ctrl.GetType() == typeof(RadLabel))
                {
                    RadLabel label = ((RadLabel)ctrl);
                    label.LabelElement.CustomFont = Global.MainFont;
                    label.LabelElement.CustomFontSize = Global.CustomFontSizeMain;
                    //label.ForeColor = Color.FromArgb(89, 89, 89);
                    label.ForeColor = Color.White;
                    label.BackColor = Color.FromArgb(0, 255, 255, 255);
                }
            }

            foreach (Control ctrl in tableLayoutPanel4.Controls)
            {
                if (ctrl.GetType() == typeof(RadLabel))
                {
                    RadLabel label = ((RadLabel)ctrl);
                    label.LabelElement.CustomFont = Global.MainFont;
                    label.LabelElement.CustomFontSize = Global.CustomFontSizeMain;
                    //label.ForeColor = Color.FromArgb(89, 89, 89);
                    label.ForeColor = Color.White;
                    label.BackColor = Color.FromArgb(0, 255, 255, 255);
                }
            }

        }
    }

    
}
