﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class JadwalKunjunganDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JadwalKunjunganDetail));
            this.universitas = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.regID = new Telerik.WinControls.UI.RadLabel();
            this.lblRegID = new Telerik.WinControls.UI.RadLabel();
            this.BolehDikunjungi = new Telerik.WinControls.UI.RadLabel();
            this.lblIzin = new Telerik.WinControls.UI.RadLabel();
            this.lblVisitorFinger = new Telerik.WinControls.UI.RadLabel();
            this.lblInmatePhoto = new Telerik.WinControls.UI.RadLabel();
            this.namaLengkap = new Telerik.WinControls.UI.RadLabel();
            this.lblNama = new Telerik.WinControls.UI.RadLabel();
            this.lblDataVisitor = new Telerik.WinControls.UI.RadLabel();
            this.panelPic = new Telerik.WinControls.UI.RadPanel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.picCanvas = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tot = new Telerik.WinControls.UI.RadLabel();
            this.next = new System.Windows.Forms.PictureBox();
            this.prev = new System.Windows.Forms.PictureBox();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.picFinger = new System.Windows.Forms.PictureBox();
            this.lblWaktu = new Telerik.WinControls.UI.RadLabel();
            this.waktuKunjungan = new Telerik.WinControls.UI.RadLabel();
            this.lblIntensitas = new Telerik.WinControls.UI.RadLabel();
            this.intensitasKunjungan = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailData = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.inputter = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.inputdate = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.updater = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.updatedate = new Telerik.WinControls.UI.RadLabel();
            this.fullpicPanel = new Telerik.WinControls.UI.RadPanel();
            this.closefull = new System.Windows.Forms.PictureBox();
            this.fullPic = new System.Windows.Forms.PictureBox();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnClose = new Telerik.WinControls.UI.RadButton();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.noSelTahanan = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.regID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BolehDikunjungi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIzin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVisitorFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInmatePhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.namaLengkap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataVisitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelPic)).BeginInit();
            this.panelPic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.next)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWaktu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waktuKunjungan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIntensitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intensitasKunjungan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullpicPanel)).BeginInit();
            this.fullpicPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closefull)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.noSelTahanan)).BeginInit();
            this.SuspendLayout();
            // 
            // universitas
            // 
            this.universitas.AutoSize = false;
            this.universitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.universitas.Location = new System.Drawing.Point(504, 414);
            this.universitas.Margin = new System.Windows.Forms.Padding(4);
            this.universitas.Name = "universitas";
            this.universitas.Size = new System.Drawing.Size(728, 42);
            this.universitas.TabIndex = 53;
            this.universitas.Text = "Tahun Lulus";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 56);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(1094, 503);
            this.radScrollablePanel1.Size = new System.Drawing.Size(1113, 505);
            this.radScrollablePanel1.TabIndex = 15;
            this.radScrollablePanel1.VerticalScrollBarState = Telerik.WinControls.UI.ScrollState.AlwaysShow;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel1.Controls.Add(this.noSelTahanan, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblVisitorFinger, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblInmatePhoto, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblDataVisitor, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panelPic, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.radLabel14, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.radLabel13, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.radLabel12, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.radLabel7, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.updatedate, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.updater, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.inputdate, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.inputter, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailData, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblWaktu, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.waktuKunjungan, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblIntensitas, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.intensitasKunjungan, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblIzin, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.BolehDikunjungi, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblRegID, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.regID, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblNama, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.namaLengkap, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 17;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1094, 850);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // regID
            // 
            this.regID.AutoSize = false;
            this.regID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.regID.Location = new System.Drawing.Point(314, 114);
            this.regID.Margin = new System.Windows.Forms.Padding(4);
            this.regID.Name = "regID";
            this.regID.Size = new System.Drawing.Size(416, 42);
            this.regID.TabIndex = 58;
            // 
            // lblRegID
            // 
            this.lblRegID.AutoSize = false;
            this.lblRegID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRegID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblRegID.Location = new System.Drawing.Point(14, 114);
            this.lblRegID.Margin = new System.Windows.Forms.Padding(4);
            this.lblRegID.Name = "lblRegID";
            this.lblRegID.Size = new System.Drawing.Size(292, 42);
            this.lblRegID.TabIndex = 11;
            this.lblRegID.Text = "No Register Tahanan";
            // 
            // BolehDikunjungi
            // 
            this.BolehDikunjungi.AutoSize = false;
            this.BolehDikunjungi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BolehDikunjungi.Location = new System.Drawing.Point(314, 214);
            this.BolehDikunjungi.Margin = new System.Windows.Forms.Padding(4);
            this.BolehDikunjungi.Name = "BolehDikunjungi";
            this.BolehDikunjungi.Size = new System.Drawing.Size(416, 42);
            this.BolehDikunjungi.TabIndex = 61;
            // 
            // lblIzin
            // 
            this.lblIzin.AutoSize = false;
            this.lblIzin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIzin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblIzin.Location = new System.Drawing.Point(14, 214);
            this.lblIzin.Margin = new System.Windows.Forms.Padding(4);
            this.lblIzin.Name = "lblIzin";
            this.lblIzin.Size = new System.Drawing.Size(292, 42);
            this.lblIzin.TabIndex = 36;
            this.lblIzin.Text = "Status Kunjungan";
            // 
            // lblVisitorFinger
            // 
            this.lblVisitorFinger.AutoSize = false;
            this.lblVisitorFinger.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.lblVisitorFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblVisitorFinger.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.lblVisitorFinger.ForeColor = System.Drawing.Color.White;
            this.lblVisitorFinger.Image = ((System.Drawing.Image)(resources.GetObject("lblVisitorFinger.Image")));
            this.lblVisitorFinger.Location = new System.Drawing.Point(734, 410);
            this.lblVisitorFinger.Margin = new System.Windows.Forms.Padding(0);
            this.lblVisitorFinger.Name = "lblVisitorFinger";
            this.lblVisitorFinger.Padding = new System.Windows.Forms.Padding(8);
            this.lblVisitorFinger.Size = new System.Drawing.Size(350, 50);
            this.lblVisitorFinger.TabIndex = 183;
            this.lblVisitorFinger.Text = "         SIDIK JARI TAHANAN";
            // 
            // lblInmatePhoto
            // 
            this.lblInmatePhoto.AutoSize = false;
            this.lblInmatePhoto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.lblInmatePhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInmatePhoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.lblInmatePhoto.ForeColor = System.Drawing.Color.White;
            this.lblInmatePhoto.Image = ((System.Drawing.Image)(resources.GetObject("lblInmatePhoto.Image")));
            this.lblInmatePhoto.Location = new System.Drawing.Point(739, 10);
            this.lblInmatePhoto.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblInmatePhoto.Name = "lblInmatePhoto";
            this.lblInmatePhoto.Padding = new System.Windows.Forms.Padding(8);
            this.lblInmatePhoto.Size = new System.Drawing.Size(340, 50);
            this.lblInmatePhoto.TabIndex = 180;
            this.lblInmatePhoto.Text = "         FOTO TAHANAN";
            // 
            // namaLengkap
            // 
            this.namaLengkap.AutoSize = false;
            this.namaLengkap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.namaLengkap.Location = new System.Drawing.Point(314, 64);
            this.namaLengkap.Margin = new System.Windows.Forms.Padding(4);
            this.namaLengkap.Name = "namaLengkap";
            this.namaLengkap.Size = new System.Drawing.Size(416, 42);
            this.namaLengkap.TabIndex = 57;
            // 
            // lblNama
            // 
            this.lblNama.AutoSize = false;
            this.lblNama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNama.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNama.Location = new System.Drawing.Point(14, 64);
            this.lblNama.Margin = new System.Windows.Forms.Padding(4);
            this.lblNama.Name = "lblNama";
            this.lblNama.Size = new System.Drawing.Size(292, 42);
            this.lblNama.TabIndex = 10;
            this.lblNama.Text = "Nama Lengkap Tahanan";
            // 
            // lblDataVisitor
            // 
            this.lblDataVisitor.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDataVisitor, 2);
            this.lblDataVisitor.Cursor = System.Windows.Forms.Cursors.No;
            this.lblDataVisitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDataVisitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDataVisitor.Image = ((System.Drawing.Image)(resources.GetObject("lblDataVisitor.Image")));
            this.lblDataVisitor.Location = new System.Drawing.Point(14, 14);
            this.lblDataVisitor.Margin = new System.Windows.Forms.Padding(4);
            this.lblDataVisitor.Name = "lblDataVisitor";
            this.lblDataVisitor.Size = new System.Drawing.Size(716, 42);
            this.lblDataVisitor.TabIndex = 62;
            this.lblDataVisitor.Text = "       INFORMASI JADWAL KUNJUNGAN TAHANAN";
            // 
            // panelPic
            // 
            this.panelPic.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelPic.Controls.Add(this.radPanel1);
            this.panelPic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPic.Location = new System.Drawing.Point(737, 63);
            this.panelPic.Name = "panelPic";
            this.tableLayoutPanel1.SetRowSpan(this.panelPic, 7);
            this.panelPic.Size = new System.Drawing.Size(344, 344);
            this.panelPic.TabIndex = 84;
            // 
            // radPanel1
            // 
            this.radPanel1.AutoScrollToCurrentControl = false;
            this.radPanel1.BackColor = System.Drawing.Color.Transparent;
            this.radPanel1.Controls.Add(this.picCanvas);
            this.radPanel1.Controls.Add(this.tableLayoutPanel2);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.radPanel1.Size = new System.Drawing.Size(344, 344);
            this.radPanel1.TabIndex = 85;
            // 
            // picCanvas
            // 
            this.picCanvas.BackColor = System.Drawing.Color.DimGray;
            this.picCanvas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picCanvas.Cursor = System.Windows.Forms.Cursors.NoMove2D;
            this.picCanvas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picCanvas.Location = new System.Drawing.Point(5, 5);
            this.picCanvas.Margin = new System.Windows.Forms.Padding(0);
            this.picCanvas.Name = "picCanvas";
            this.picCanvas.Size = new System.Drawing.Size(334, 286);
            this.picCanvas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCanvas.TabIndex = 87;
            this.picCanvas.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel2.Controls.Add(this.tot, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.next, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.prev, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(5, 291);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(334, 48);
            this.tableLayoutPanel2.TabIndex = 86;
            // 
            // tot
            // 
            this.tot.AutoSize = false;
            this.tot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tot.ForeColor = System.Drawing.Color.White;
            this.tot.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tot.Location = new System.Drawing.Point(68, 3);
            this.tot.Name = "tot";
            this.tot.Size = new System.Drawing.Size(198, 42);
            this.tot.TabIndex = 4;
            this.tot.Text = "0/0";
            this.tot.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // next
            // 
            this.next.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("next.BackgroundImage")));
            this.next.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.next.Dock = System.Windows.Forms.DockStyle.Fill;
            this.next.Location = new System.Drawing.Point(272, 3);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(59, 42);
            this.next.TabIndex = 1;
            this.next.TabStop = false;
            // 
            // prev
            // 
            this.prev.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("prev.BackgroundImage")));
            this.prev.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.prev.Cursor = System.Windows.Forms.Cursors.Hand;
            this.prev.Dock = System.Windows.Forms.DockStyle.Fill;
            this.prev.Location = new System.Drawing.Point(3, 3);
            this.prev.Name = "prev";
            this.prev.Size = new System.Drawing.Size(59, 42);
            this.prev.TabIndex = 0;
            this.prev.TabStop = false;
            // 
            // radPanel2
            // 
            this.radPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.radPanel2.Controls.Add(this.picFinger);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(734, 460);
            this.radPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Padding = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel1.SetRowSpan(this.radPanel2, 7);
            this.radPanel2.Size = new System.Drawing.Size(350, 350);
            this.radPanel2.TabIndex = 86;
            // 
            // picFinger
            // 
            this.picFinger.BackColor = System.Drawing.Color.DimGray;
            this.picFinger.Cursor = System.Windows.Forms.Cursors.NoMove2D;
            this.picFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picFinger.Location = new System.Drawing.Point(6, 6);
            this.picFinger.Margin = new System.Windows.Forms.Padding(0);
            this.picFinger.Name = "picFinger";
            this.picFinger.Size = new System.Drawing.Size(338, 338);
            this.picFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFinger.TabIndex = 87;
            this.picFinger.TabStop = false;
            // 
            // lblWaktu
            // 
            this.lblWaktu.AutoSize = false;
            this.lblWaktu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWaktu.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblWaktu.Location = new System.Drawing.Point(14, 314);
            this.lblWaktu.Margin = new System.Windows.Forms.Padding(4);
            this.lblWaktu.Name = "lblWaktu";
            this.lblWaktu.Size = new System.Drawing.Size(292, 42);
            this.lblWaktu.TabIndex = 34;
            this.lblWaktu.Text = "Waktu Kunjungan";
            // 
            // waktuKunjungan
            // 
            this.waktuKunjungan.AutoSize = false;
            this.waktuKunjungan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.waktuKunjungan.Location = new System.Drawing.Point(314, 314);
            this.waktuKunjungan.Margin = new System.Windows.Forms.Padding(4);
            this.waktuKunjungan.Name = "waktuKunjungan";
            this.waktuKunjungan.Size = new System.Drawing.Size(416, 42);
            this.waktuKunjungan.TabIndex = 56;
            // 
            // lblIntensitas
            // 
            this.lblIntensitas.AutoSize = false;
            this.lblIntensitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIntensitas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblIntensitas.Location = new System.Drawing.Point(14, 264);
            this.lblIntensitas.Margin = new System.Windows.Forms.Padding(4);
            this.lblIntensitas.Name = "lblIntensitas";
            this.lblIntensitas.Size = new System.Drawing.Size(292, 42);
            this.lblIntensitas.TabIndex = 35;
            this.lblIntensitas.Text = "Instensitas Kunjungan";
            // 
            // intensitasKunjungan
            // 
            this.intensitasKunjungan.AutoSize = false;
            this.intensitasKunjungan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.intensitasKunjungan.Location = new System.Drawing.Point(314, 264);
            this.intensitasKunjungan.Margin = new System.Windows.Forms.Padding(4);
            this.intensitasKunjungan.Name = "intensitasKunjungan";
            this.intensitasKunjungan.Size = new System.Drawing.Size(416, 42);
            this.intensitasKunjungan.TabIndex = 60;
            // 
            // lblDetailData
            // 
            this.lblDetailData.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailData, 2);
            this.lblDetailData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDetailData.Image = ((System.Drawing.Image)(resources.GetObject("lblDetailData.Image")));
            this.lblDetailData.Location = new System.Drawing.Point(14, 414);
            this.lblDetailData.Margin = new System.Windows.Forms.Padding(4);
            this.lblDetailData.Name = "lblDetailData";
            this.lblDetailData.Size = new System.Drawing.Size(716, 42);
            this.lblDetailData.TabIndex = 71;
            this.lblDetailData.Text = "      DATA LOG";
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel7.Location = new System.Drawing.Point(14, 464);
            this.radLabel7.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(292, 42);
            this.radLabel7.TabIndex = 68;
            this.radLabel7.Text = "Inputter";
            // 
            // inputter
            // 
            this.inputter.AutoSize = false;
            this.inputter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputter.Location = new System.Drawing.Point(314, 464);
            this.inputter.Margin = new System.Windows.Forms.Padding(4);
            this.inputter.Name = "inputter";
            this.inputter.Size = new System.Drawing.Size(416, 42);
            this.inputter.TabIndex = 77;
            // 
            // radLabel12
            // 
            this.radLabel12.AutoSize = false;
            this.radLabel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel12.Location = new System.Drawing.Point(14, 514);
            this.radLabel12.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(292, 42);
            this.radLabel12.TabIndex = 72;
            this.radLabel12.Text = "Input Date";
            // 
            // inputdate
            // 
            this.inputdate.AutoSize = false;
            this.inputdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputdate.Location = new System.Drawing.Point(314, 514);
            this.inputdate.Margin = new System.Windows.Forms.Padding(4);
            this.inputdate.Name = "inputdate";
            this.inputdate.Size = new System.Drawing.Size(416, 42);
            this.inputdate.TabIndex = 78;
            // 
            // radLabel13
            // 
            this.radLabel13.AutoSize = false;
            this.radLabel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel13.Location = new System.Drawing.Point(14, 564);
            this.radLabel13.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(292, 42);
            this.radLabel13.TabIndex = 73;
            this.radLabel13.Text = "Updater";
            // 
            // updater
            // 
            this.updater.AutoSize = false;
            this.updater.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updater.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updater.Location = new System.Drawing.Point(314, 564);
            this.updater.Margin = new System.Windows.Forms.Padding(4);
            this.updater.Name = "updater";
            this.updater.Size = new System.Drawing.Size(416, 42);
            this.updater.TabIndex = 80;
            // 
            // radLabel14
            // 
            this.radLabel14.AutoSize = false;
            this.radLabel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel14.Location = new System.Drawing.Point(14, 614);
            this.radLabel14.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(292, 42);
            this.radLabel14.TabIndex = 74;
            this.radLabel14.Text = "Update Date";
            // 
            // updatedate
            // 
            this.updatedate.AutoSize = false;
            this.updatedate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updatedate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updatedate.Location = new System.Drawing.Point(314, 614);
            this.updatedate.Margin = new System.Windows.Forms.Padding(4);
            this.updatedate.Name = "updatedate";
            this.updatedate.Size = new System.Drawing.Size(416, 42);
            this.updatedate.TabIndex = 79;
            // 
            // fullpicPanel
            // 
            this.fullpicPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(65)))));
            this.fullpicPanel.Controls.Add(this.closefull);
            this.fullpicPanel.Controls.Add(this.fullPic);
            this.fullpicPanel.Location = new System.Drawing.Point(7600, 22);
            this.fullpicPanel.Name = "fullpicPanel";
            this.fullpicPanel.Padding = new System.Windows.Forms.Padding(5);
            this.fullpicPanel.Size = new System.Drawing.Size(603, 641);
            this.fullpicPanel.TabIndex = 84;
            this.fullpicPanel.Visible = false;
            // 
            // closefull
            // 
            this.closefull.BackColor = System.Drawing.Color.Transparent;
            this.closefull.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.closefull.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closefull.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.delete1;
            this.closefull.Location = new System.Drawing.Point(553, 5);
            this.closefull.Name = "closefull";
            this.closefull.Size = new System.Drawing.Size(44, 45);
            this.closefull.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.closefull.TabIndex = 3;
            this.closefull.TabStop = false;
            // 
            // fullPic
            // 
            this.fullPic.BackColor = System.Drawing.Color.White;
            this.fullPic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fullPic.Location = new System.Drawing.Point(5, 5);
            this.fullPic.Margin = new System.Windows.Forms.Padding(10);
            this.fullPic.Name = "fullPic";
            this.fullPic.Size = new System.Drawing.Size(593, 631);
            this.fullPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fullPic.TabIndex = 0;
            this.fullPic.TabStop = false;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1113, 56);
            this.lblTitle.TabIndex = 8;
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(1048, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 56);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 1;
            this.exit.TabStop = false;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnClose);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 587);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(1113, 122);
            this.lblButton.TabIndex = 88;
            // 
            // btnClose
            // 
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(30, 30);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(244, 62);
            this.btnClose.TabIndex = 28;
            this.btnClose.Tag = "";
            this.btnClose.Text = "&TUTUP";
            this.btnClose.ThemeName = "MaterialBlueGrey";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel1.Location = new System.Drawing.Point(14, 164);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(292, 42);
            this.radLabel1.TabIndex = 11;
            this.radLabel1.Text = "No Sel Tahanan";
            // 
            // noSelTahanan
            // 
            this.noSelTahanan.AutoSize = false;
            this.noSelTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.noSelTahanan.Location = new System.Drawing.Point(314, 164);
            this.noSelTahanan.Margin = new System.Windows.Forms.Padding(4);
            this.noSelTahanan.Name = "noSelTahanan";
            this.noSelTahanan.Size = new System.Drawing.Size(416, 42);
            this.noSelTahanan.TabIndex = 59;
            // 
            // JadwalKunjunganDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.fullpicPanel);
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblTitle);
            this.Name = "JadwalKunjunganDetail";
            this.Size = new System.Drawing.Size(1113, 709);
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.regID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BolehDikunjungi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIzin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVisitorFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInmatePhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.namaLengkap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataVisitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelPic)).EndInit();
            this.panelPic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.next)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWaktu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waktuKunjungan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIntensitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intensitasKunjungan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullpicPanel)).EndInit();
            this.fullpicPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.closefull)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.noSelTahanan)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel universitas;
        private System.Windows.Forms.PictureBox exit;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel updater;
        private Telerik.WinControls.UI.RadLabel inputdate;
        private Telerik.WinControls.UI.RadLabel intensitasKunjungan;
        private Telerik.WinControls.UI.RadLabel namaLengkap;
        private Telerik.WinControls.UI.RadLabel waktuKunjungan;
        private Telerik.WinControls.UI.RadLabel lblIntensitas;
        private Telerik.WinControls.UI.RadLabel lblWaktu;
        private Telerik.WinControls.UI.RadLabel lblNama;
        private Telerik.WinControls.UI.RadLabel lblDataVisitor;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel inputter;
        private Telerik.WinControls.UI.RadLabel lblDetailData;
        private Telerik.WinControls.UI.RadLabel updatedate;
        private Telerik.WinControls.UI.RadPanel fullpicPanel;
        private System.Windows.Forms.PictureBox fullPic;
        private System.Windows.Forms.PictureBox closefull;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnClose;
        private Telerik.WinControls.UI.RadLabel BolehDikunjungi;
        private Telerik.WinControls.UI.RadLabel lblIzin;
        private Telerik.WinControls.UI.RadLabel lblVisitorFinger;
        private Telerik.WinControls.UI.RadLabel lblInmatePhoto;
        private Telerik.WinControls.UI.RadPanel panelPic;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private System.Windows.Forms.PictureBox picCanvas;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel tot;
        private System.Windows.Forms.PictureBox next;
        private System.Windows.Forms.PictureBox prev;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private System.Windows.Forms.PictureBox picFinger;
        private Telerik.WinControls.UI.RadLabel regID;
        private Telerik.WinControls.UI.RadLabel lblRegID;
        private Telerik.WinControls.UI.RadLabel noSelTahanan;
        private Telerik.WinControls.UI.RadLabel radLabel1;
    }
}
