﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class FingerPrint_Inmate
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FingerPrint_Inmate));
            this.pbLeftLittleFinger = new System.Windows.Forms.PictureBox();
            this.pbRightThumbFinger = new System.Windows.Forms.PictureBox();
            this.panel2 = new Telerik.WinControls.UI.RadPanel();
            this.pbRightIndexFinger = new System.Windows.Forms.PictureBox();
            this.panel3 = new Telerik.WinControls.UI.RadPanel();
            this.pbRightMiddleFinger = new System.Windows.Forms.PictureBox();
            this.panel4 = new Telerik.WinControls.UI.RadPanel();
            this.pbRightRingFinger = new System.Windows.Forms.PictureBox();
            this.lblmessage = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel10 = new Telerik.WinControls.UI.RadPanel();
            this.panel9 = new Telerik.WinControls.UI.RadPanel();
            this.pbLeftRingFinger = new System.Windows.Forms.PictureBox();
            this.panel8 = new Telerik.WinControls.UI.RadPanel();
            this.pbLeftMiddleFinger = new System.Windows.Forms.PictureBox();
            this.panel7 = new Telerik.WinControls.UI.RadPanel();
            this.pbLeftIndexFinger = new System.Windows.Forms.PictureBox();
            this.lblLRingFinger = new System.Windows.Forms.Label();
            this.lblLMiddleFinger = new System.Windows.Forms.Label();
            this.lblLThumb = new System.Windows.Forms.Label();
            this.panel6 = new Telerik.WinControls.UI.RadPanel();
            this.pbLeftThumbFinger = new System.Windows.Forms.PictureBox();
            this.lblRLittleFinger = new System.Windows.Forms.Label();
            this.lblLLittleFinger = new System.Windows.Forms.Label();
            this.lblRRingFinger = new System.Windows.Forms.Label();
            this.lblRMiddleFinger = new System.Windows.Forms.Label();
            this.lblLeft = new System.Windows.Forms.Label();
            this.lblRThumb = new System.Windows.Forms.Label();
            this.lblRight = new System.Windows.Forms.Label();
            this.panel1 = new Telerik.WinControls.UI.RadPanel();
            this.panel5 = new Telerik.WinControls.UI.RadPanel();
            this.pbRightLittleFinger = new System.Windows.Forms.PictureBox();
            this.panelbiru = new Telerik.WinControls.UI.RadPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tblButton = new System.Windows.Forms.TableLayoutPanel();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbLeftLittleFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRightThumbFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRightIndexFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel3)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRightMiddleFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel4)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRightRingFinger)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel10)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel9)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLeftRingFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel8)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLeftMiddleFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel7)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLeftIndexFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel6)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLeftThumbFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel5)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRightLittleFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelbiru)).BeginInit();
            this.panelbiru.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            this.SuspendLayout();
            // 
            // pbLeftLittleFinger
            // 
            this.pbLeftLittleFinger.BackColor = System.Drawing.Color.Silver;
            this.pbLeftLittleFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbLeftLittleFinger.Location = new System.Drawing.Point(4, 4);
            this.pbLeftLittleFinger.Name = "pbLeftLittleFinger";
            this.pbLeftLittleFinger.Size = new System.Drawing.Size(164, 206);
            this.pbLeftLittleFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLeftLittleFinger.TabIndex = 104;
            this.pbLeftLittleFinger.TabStop = false;
            // 
            // pbRightThumbFinger
            // 
            this.pbRightThumbFinger.BackColor = System.Drawing.Color.Silver;
            this.pbRightThumbFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbRightThumbFinger.InitialImage = null;
            this.pbRightThumbFinger.Location = new System.Drawing.Point(4, 4);
            this.pbRightThumbFinger.Margin = new System.Windows.Forms.Padding(0);
            this.pbRightThumbFinger.Name = "pbRightThumbFinger";
            this.pbRightThumbFinger.Size = new System.Drawing.Size(155, 206);
            this.pbRightThumbFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRightThumbFinger.TabIndex = 97;
            this.pbRightThumbFinger.TabStop = false;
            this.pbRightThumbFinger.Tag = "1";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Controls.Add(this.pbRightIndexFinger);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(290, 43);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(4);
            this.panel2.Size = new System.Drawing.Size(166, 214);
            this.panel2.TabIndex = 134;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panel2.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(4);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel2.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel2.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // pbRightIndexFinger
            // 
            this.pbRightIndexFinger.BackColor = System.Drawing.Color.Silver;
            this.pbRightIndexFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbRightIndexFinger.Location = new System.Drawing.Point(4, 4);
            this.pbRightIndexFinger.Margin = new System.Windows.Forms.Padding(0);
            this.pbRightIndexFinger.Name = "pbRightIndexFinger";
            this.pbRightIndexFinger.Size = new System.Drawing.Size(158, 206);
            this.pbRightIndexFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRightIndexFinger.TabIndex = 98;
            this.pbRightIndexFinger.TabStop = false;
            this.pbRightIndexFinger.Tag = "2";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.Controls.Add(this.pbRightMiddleFinger);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(488, 43);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(4);
            this.panel3.Size = new System.Drawing.Size(164, 214);
            this.panel3.TabIndex = 135;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panel3.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(4);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel3.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel3.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // pbRightMiddleFinger
            // 
            this.pbRightMiddleFinger.BackColor = System.Drawing.Color.Silver;
            this.pbRightMiddleFinger.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pbRightMiddleFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbRightMiddleFinger.Location = new System.Drawing.Point(4, 4);
            this.pbRightMiddleFinger.Name = "pbRightMiddleFinger";
            this.pbRightMiddleFinger.Size = new System.Drawing.Size(156, 206);
            this.pbRightMiddleFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRightMiddleFinger.TabIndex = 99;
            this.pbRightMiddleFinger.TabStop = false;
            this.pbRightMiddleFinger.Tag = "3";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Silver;
            this.panel4.Controls.Add(this.pbRightRingFinger);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(682, 43);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(4);
            this.panel4.Size = new System.Drawing.Size(166, 214);
            this.panel4.TabIndex = 136;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panel4.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(4);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel4.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel4.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // pbRightRingFinger
            // 
            this.pbRightRingFinger.BackColor = System.Drawing.Color.Silver;
            this.pbRightRingFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbRightRingFinger.Location = new System.Drawing.Point(4, 4);
            this.pbRightRingFinger.Name = "pbRightRingFinger";
            this.pbRightRingFinger.Size = new System.Drawing.Size(158, 206);
            this.pbRightRingFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRightRingFinger.TabIndex = 100;
            this.pbRightRingFinger.TabStop = false;
            this.pbRightRingFinger.Tag = "4";
            // 
            // lblmessage
            // 
            this.lblmessage.AutoSize = true;
            this.lblmessage.BackColor = System.Drawing.Color.Transparent;
            this.lblmessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblmessage.Location = new System.Drawing.Point(13, 575);
            this.lblmessage.Name = "lblmessage";
            this.lblmessage.Size = new System.Drawing.Size(1077, 38);
            this.lblmessage.TabIndex = 96;
            this.lblmessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.tableLayoutPanel2.ColumnCount = 10;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 169F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 172F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 172F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 178F));
            this.tableLayoutPanel2.Controls.Add(this.panel10, 9, 3);
            this.tableLayoutPanel2.Controls.Add(this.panel9, 7, 3);
            this.tableLayoutPanel2.Controls.Add(this.panel8, 5, 3);
            this.tableLayoutPanel2.Controls.Add(this.panel7, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblLRingFinger, 7, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblLMiddleFinger, 5, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblLThumb, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel6, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblRLittleFinger, 9, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblLLittleFinger, 9, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblRRingFinger, 7, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblRMiddleFinger, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblLeft, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblRThumb, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblRight, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel2, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel3, 5, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel5, 9, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel4, 7, 1);
            this.tableLayoutPanel2.Controls.Add(this.label1, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 3, 2);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(13, 13);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(0, 0, 20, 20);
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1074, 535);
            this.tableLayoutPanel2.TabIndex = 97;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Silver;
            this.panel10.Controls.Add(this.pbLeftLittleFinger);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(879, 303);
            this.panel10.Name = "panel10";
            this.panel10.Padding = new System.Windows.Forms.Padding(4);
            this.panel10.Size = new System.Drawing.Size(172, 214);
            this.panel10.TabIndex = 125;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panel10.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(4);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel10.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel10.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Silver;
            this.panel9.Controls.Add(this.pbLeftRingFinger);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(682, 303);
            this.panel9.Name = "panel9";
            this.panel9.Padding = new System.Windows.Forms.Padding(4);
            this.panel9.Size = new System.Drawing.Size(166, 214);
            this.panel9.TabIndex = 124;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panel9.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(4);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel9.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel9.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // pbLeftRingFinger
            // 
            this.pbLeftRingFinger.BackColor = System.Drawing.Color.Silver;
            this.pbLeftRingFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbLeftRingFinger.Location = new System.Drawing.Point(4, 4);
            this.pbLeftRingFinger.Name = "pbLeftRingFinger";
            this.pbLeftRingFinger.Size = new System.Drawing.Size(158, 206);
            this.pbLeftRingFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLeftRingFinger.TabIndex = 103;
            this.pbLeftRingFinger.TabStop = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Silver;
            this.panel8.Controls.Add(this.pbLeftMiddleFinger);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(488, 303);
            this.panel8.Name = "panel8";
            this.panel8.Padding = new System.Windows.Forms.Padding(4);
            this.panel8.Size = new System.Drawing.Size(164, 214);
            this.panel8.TabIndex = 123;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panel8.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(4);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel8.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel8.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // pbLeftMiddleFinger
            // 
            this.pbLeftMiddleFinger.BackColor = System.Drawing.Color.Silver;
            this.pbLeftMiddleFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbLeftMiddleFinger.Location = new System.Drawing.Point(4, 4);
            this.pbLeftMiddleFinger.Name = "pbLeftMiddleFinger";
            this.pbLeftMiddleFinger.Size = new System.Drawing.Size(156, 206);
            this.pbLeftMiddleFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLeftMiddleFinger.TabIndex = 102;
            this.pbLeftMiddleFinger.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Silver;
            this.panel7.Controls.Add(this.pbLeftIndexFinger);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(290, 303);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(4);
            this.panel7.Size = new System.Drawing.Size(166, 214);
            this.panel7.TabIndex = 122;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panel7.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(4);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel7.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel7.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // pbLeftIndexFinger
            // 
            this.pbLeftIndexFinger.BackColor = System.Drawing.Color.Silver;
            this.pbLeftIndexFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbLeftIndexFinger.Location = new System.Drawing.Point(4, 4);
            this.pbLeftIndexFinger.Name = "pbLeftIndexFinger";
            this.pbLeftIndexFinger.Size = new System.Drawing.Size(158, 206);
            this.pbLeftIndexFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLeftIndexFinger.TabIndex = 101;
            this.pbLeftIndexFinger.TabStop = false;
            // 
            // lblLRingFinger
            // 
            this.lblLRingFinger.BackColor = System.Drawing.Color.Transparent;
            this.lblLRingFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLRingFinger.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLRingFinger.ForeColor = System.Drawing.Color.White;
            this.lblLRingFinger.Location = new System.Drawing.Point(682, 260);
            this.lblLRingFinger.Name = "lblLRingFinger";
            this.lblLRingFinger.Size = new System.Drawing.Size(166, 40);
            this.lblLRingFinger.TabIndex = 112;
            this.lblLRingFinger.Text = "JARI MANIS";
            this.lblLRingFinger.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLMiddleFinger
            // 
            this.lblLMiddleFinger.BackColor = System.Drawing.Color.Transparent;
            this.lblLMiddleFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLMiddleFinger.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLMiddleFinger.ForeColor = System.Drawing.Color.White;
            this.lblLMiddleFinger.Location = new System.Drawing.Point(488, 260);
            this.lblLMiddleFinger.Name = "lblLMiddleFinger";
            this.lblLMiddleFinger.Size = new System.Drawing.Size(164, 40);
            this.lblLMiddleFinger.TabIndex = 111;
            this.lblLMiddleFinger.Text = "JARI TENGAH";
            this.lblLMiddleFinger.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLThumb
            // 
            this.lblLThumb.BackColor = System.Drawing.Color.Transparent;
            this.lblLThumb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLThumb.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLThumb.ForeColor = System.Drawing.Color.White;
            this.lblLThumb.Location = new System.Drawing.Point(97, 260);
            this.lblLThumb.Name = "lblLThumb";
            this.lblLThumb.Size = new System.Drawing.Size(163, 40);
            this.lblLThumb.TabIndex = 109;
            this.lblLThumb.Text = "IBU JARI";
            this.lblLThumb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Silver;
            this.panel6.Controls.Add(this.pbLeftThumbFinger);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(97, 303);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(4);
            this.panel6.Size = new System.Drawing.Size(163, 214);
            this.panel6.TabIndex = 138;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panel6.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(4);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel6.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel6.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // pbLeftThumbFinger
            // 
            this.pbLeftThumbFinger.BackColor = System.Drawing.Color.Silver;
            this.pbLeftThumbFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbLeftThumbFinger.Location = new System.Drawing.Point(4, 4);
            this.pbLeftThumbFinger.Margin = new System.Windows.Forms.Padding(0);
            this.pbLeftThumbFinger.Name = "pbLeftThumbFinger";
            this.pbLeftThumbFinger.Size = new System.Drawing.Size(155, 206);
            this.pbLeftThumbFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLeftThumbFinger.TabIndex = 102;
            this.pbLeftThumbFinger.TabStop = false;
            // 
            // lblRLittleFinger
            // 
            this.lblRLittleFinger.BackColor = System.Drawing.Color.Transparent;
            this.lblRLittleFinger.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRLittleFinger.ForeColor = System.Drawing.Color.White;
            this.lblRLittleFinger.Location = new System.Drawing.Point(879, 0);
            this.lblRLittleFinger.Name = "lblRLittleFinger";
            this.lblRLittleFinger.Size = new System.Drawing.Size(170, 40);
            this.lblRLittleFinger.TabIndex = 108;
            this.lblRLittleFinger.Text = "JARI\r\nKELINGKING";
            this.lblRLittleFinger.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLLittleFinger
            // 
            this.lblLLittleFinger.BackColor = System.Drawing.Color.Transparent;
            this.lblLLittleFinger.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLLittleFinger.ForeColor = System.Drawing.Color.White;
            this.lblLLittleFinger.Location = new System.Drawing.Point(879, 260);
            this.lblLLittleFinger.Name = "lblLLittleFinger";
            this.lblLLittleFinger.Size = new System.Drawing.Size(170, 40);
            this.lblLLittleFinger.TabIndex = 113;
            this.lblLLittleFinger.Text = "JARI\r\nKELINGKING";
            this.lblLLittleFinger.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRRingFinger
            // 
            this.lblRRingFinger.BackColor = System.Drawing.Color.Transparent;
            this.lblRRingFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRRingFinger.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRRingFinger.ForeColor = System.Drawing.Color.White;
            this.lblRRingFinger.Location = new System.Drawing.Point(682, 0);
            this.lblRRingFinger.Name = "lblRRingFinger";
            this.lblRRingFinger.Size = new System.Drawing.Size(166, 40);
            this.lblRRingFinger.TabIndex = 107;
            this.lblRRingFinger.Text = "JARI MANIS";
            this.lblRRingFinger.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRMiddleFinger
            // 
            this.lblRMiddleFinger.BackColor = System.Drawing.Color.Transparent;
            this.lblRMiddleFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRMiddleFinger.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRMiddleFinger.ForeColor = System.Drawing.Color.White;
            this.lblRMiddleFinger.Location = new System.Drawing.Point(488, 0);
            this.lblRMiddleFinger.Name = "lblRMiddleFinger";
            this.lblRMiddleFinger.Size = new System.Drawing.Size(164, 40);
            this.lblRMiddleFinger.TabIndex = 106;
            this.lblRMiddleFinger.Text = "JARI TENGAH";
            this.lblRMiddleFinger.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLeft
            // 
            this.lblLeft.AutoSize = true;
            this.lblLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLeft.ForeColor = System.Drawing.Color.White;
            this.lblLeft.Location = new System.Drawing.Point(3, 300);
            this.lblLeft.Name = "lblLeft";
            this.lblLeft.Size = new System.Drawing.Size(88, 220);
            this.lblLeft.TabIndex = 115;
            this.lblLeft.Text = "KIRI";
            this.lblLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRThumb
            // 
            this.lblRThumb.AutoSize = true;
            this.lblRThumb.BackColor = System.Drawing.Color.Transparent;
            this.lblRThumb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRThumb.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRThumb.ForeColor = System.Drawing.Color.White;
            this.lblRThumb.Location = new System.Drawing.Point(97, 0);
            this.lblRThumb.Name = "lblRThumb";
            this.lblRThumb.Size = new System.Drawing.Size(163, 40);
            this.lblRThumb.TabIndex = 104;
            this.lblRThumb.Text = "IBU JARI";
            this.lblRThumb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRight
            // 
            this.lblRight.AutoSize = true;
            this.lblRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRight.ForeColor = System.Drawing.Color.White;
            this.lblRight.Location = new System.Drawing.Point(3, 40);
            this.lblRight.Name = "lblRight";
            this.lblRight.Size = new System.Drawing.Size(88, 220);
            this.lblRight.TabIndex = 114;
            this.lblRight.Text = "KANAN";
            this.lblRight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.pbRightThumbFinger);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(97, 43);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(4);
            this.panel1.Size = new System.Drawing.Size(163, 214);
            this.panel1.TabIndex = 133;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panel1.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(4);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel1.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel1.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Silver;
            this.panel5.Controls.Add(this.pbRightLittleFinger);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(879, 43);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(4);
            this.panel5.Size = new System.Drawing.Size(172, 214);
            this.panel5.TabIndex = 137;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panel5.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(4);
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel5.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panel5.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // pbRightLittleFinger
            // 
            this.pbRightLittleFinger.BackColor = System.Drawing.Color.Silver;
            this.pbRightLittleFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbRightLittleFinger.Location = new System.Drawing.Point(4, 4);
            this.pbRightLittleFinger.Name = "pbRightLittleFinger";
            this.pbRightLittleFinger.Size = new System.Drawing.Size(164, 206);
            this.pbRightLittleFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRightLittleFinger.TabIndex = 102;
            this.pbRightLittleFinger.TabStop = false;
            // 
            // panelbiru
            // 
            this.panelbiru.Controls.Add(this.tableLayoutPanel1);
            this.panelbiru.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelbiru.Location = new System.Drawing.Point(0, 50);
            this.panelbiru.Name = "panelbiru";
            this.panelbiru.Padding = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.panelbiru.Size = new System.Drawing.Size(1123, 694);
            this.panelbiru.TabIndex = 89;
            ((Telerik.WinControls.UI.RadPanelElement)(this.panelbiru.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(10, 0, 10, 10);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.panelbiru.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.SystemColors.ActiveCaption;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.panelbiru.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panelbiru.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.panelbiru.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.DimGray;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tblButton, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblmessage, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 93.58778F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.412214F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1103, 684);
            this.tableLayoutPanel1.TabIndex = 100;
            // 
            // tblButton
            // 
            this.tblButton.ColumnCount = 2;
            this.tblButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblButton.Controls.Add(this.btnClear, 0, 0);
            this.tblButton.Controls.Add(this.btnSave, 1, 0);
            this.tblButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.tblButton.Location = new System.Drawing.Point(735, 616);
            this.tblButton.Name = "tblButton";
            this.tblButton.RowCount = 1;
            this.tblButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblButton.Size = new System.Drawing.Size(355, 55);
            this.tblButton.TabIndex = 95;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnClear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(3, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(171, 49);
            this.btnClear.TabIndex = 92;
            this.btnClear.Text = "HAPUS";
            this.btnClear.UseVisualStyleBackColor = false;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Enabled = false;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(180, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(172, 49);
            this.btnSave.TabIndex = 93;
            this.btnSave.Text = "LANJUTKAN";
            this.btnSave.UseVisualStyleBackColor = false;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1123, 50);
            this.lblTitle.TabIndex = 88;
            this.lblTitle.Text = "           DAFTARKAN SIDIK JARI BARU";
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(1058, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 50);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 2;
            this.exit.TabStop = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(290, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 40);
            this.label1.TabIndex = 139;
            this.label1.Text = "JARI\r\nTELUNJUK";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(290, 260);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 40);
            this.label2.TabIndex = 140;
            this.label2.Text = "JARI\r\nTELUNJUK";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FingerPrint_Inmate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panelbiru);
            this.Controls.Add(this.lblTitle);
            this.Name = "FingerPrint_Inmate";
            this.Size = new System.Drawing.Size(1123, 744);
            ((System.ComponentModel.ISupportInitialize)(this.pbLeftLittleFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRightThumbFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel2)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbRightIndexFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel3)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbRightMiddleFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel4)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbRightRingFinger)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel10)).EndInit();
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel9)).EndInit();
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLeftRingFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel8)).EndInit();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLeftMiddleFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel7)).EndInit();
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLeftIndexFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel6)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLeftThumbFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel5)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbRightLittleFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelbiru)).EndInit();
            this.panelbiru.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbLeftLittleFinger;
        private System.Windows.Forms.PictureBox pbRightThumbFinger;
        private Telerik.WinControls.UI.RadPanel panel2;
        private System.Windows.Forms.PictureBox pbRightIndexFinger;
        private Telerik.WinControls.UI.RadPanel panel3;
        private System.Windows.Forms.PictureBox pbRightMiddleFinger;
        private Telerik.WinControls.UI.RadPanel panel4;
        private System.Windows.Forms.PictureBox pbRightRingFinger;
        private System.Windows.Forms.Label lblmessage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadPanel panel10;
        private Telerik.WinControls.UI.RadPanel panel9;
        private System.Windows.Forms.PictureBox pbLeftRingFinger;
        private Telerik.WinControls.UI.RadPanel panel8;
        private System.Windows.Forms.PictureBox pbLeftMiddleFinger;
        private Telerik.WinControls.UI.RadPanel panel7;
        private System.Windows.Forms.PictureBox pbLeftIndexFinger;
        private System.Windows.Forms.Label lblLRingFinger;
        private System.Windows.Forms.Label lblLMiddleFinger;
        private System.Windows.Forms.Label lblLThumb;
        private Telerik.WinControls.UI.RadPanel panel6;
        private System.Windows.Forms.PictureBox pbLeftThumbFinger;
        private System.Windows.Forms.Label lblRLittleFinger;
        private System.Windows.Forms.Label lblLLittleFinger;
        private System.Windows.Forms.Label lblRRingFinger;
        private System.Windows.Forms.Label lblRMiddleFinger;
        private System.Windows.Forms.Label lblLeft;
        private System.Windows.Forms.Label lblRThumb;
        private System.Windows.Forms.Label lblRight;
        private Telerik.WinControls.UI.RadPanel panel1;
        private Telerik.WinControls.UI.RadPanel panel5;
        private System.Windows.Forms.PictureBox pbRightLittleFinger;
        private Telerik.WinControls.UI.RadPanel panelbiru;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tblButton;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSave;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private System.Windows.Forms.PictureBox exit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
