﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;
using Telerik.WinControls.Export;
using System.IO;
using System.Threading;
using Telerik.WinControls.UI.Export;
using Telerik.Windows.Documents.Media;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListKunjunganVIP : Base
    {
        public RadGridView GvKunjungan { get { return this.gvKunjunganVIP; } }
        private VisitorVIP data;
        public ListKunjunganVIP()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvKunjunganVIP.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvKunjunganVIP.RowFormatting += radGridView_RowFormatting;
            gvKunjunganVIP.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvKunjunganVIP.CellClick += gvCell_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarKunjunganVIP.pdf", "DAFTAR KUNJUNGAN VIP",gvKunjunganVIP);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarKunjunganVIP.csv", "DAFTAR KUNJUNGAN VIP", gvKunjunganVIP);
        }

        private void gvCell_CellClick(object sender, GridViewCellEventArgs e)
        {
            VisitorVIPService visitorserv = new VisitorVIPService();
            data = visitorserv.GetDetailById(Convert.ToString(e.Row.Cells["Id"].Value));
            if (e.Column == null)
            {
                return;
            }
            else {
                MainForm.formMain.VisitorVIPDetail.Tag = e.Row.Cells["Id"].Value.ToString();
                //MainForm.formMain.VisitorVIPDetail.btncheckout.Hide();
                MainForm.formMain.VisitorVIPDetail.Show();
            }
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DAFTAR KUNJUNGAN VIP";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;
            this.lblDetail.BackColor = Color.FromArgb(77, 77, 77);
            this.lblDetail.ForeColor = Color.White;

            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Name = "btnDetail";
            gvKunjunganVIP.AutoGenerateColumns = false;
            gvKunjunganVIP.Columns.Insert(0, btnDetail);
            gvKunjunganVIP.Refresh();
            gvKunjunganVIP.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            UserFunction.LoadDataKunjunganVIPToGrid(gvKunjunganVIP);
            UserFunction.SetInitGridView(gvKunjunganVIP);

        }
    }
}
