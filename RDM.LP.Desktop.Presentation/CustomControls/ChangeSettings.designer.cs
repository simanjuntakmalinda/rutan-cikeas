﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class ChangeSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangeSettings));
            this.headerPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            this.lblDetailSurat = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtUserName = new Telerik.WinControls.UI.RadTextBox();
            this.txtDBDataSource = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel25 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.pswPassword = new Telerik.WinControls.UI.RadTextBox();
            this.lblDetail2 = new Telerik.WinControls.UI.RadLabel();
            this.lblDetail1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.txtInterval = new Telerik.WinControls.UI.RadTextBox();
            this.lblSecond = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.txtSeperator1 = new Telerik.WinControls.UI.RadTextBox();
            this.txtSeperator2 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.txtPagingSize = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.pswRawPassword = new Telerik.WinControls.UI.RadTextBox();
            this.radPanelError = new Telerik.WinControls.UI.RadPanel();
            this.lblError = new Telerik.WinControls.UI.RadLabel();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnBack = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).BeginInit();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDBDataSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pswPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSecond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSeperator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSeperator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPagingSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pswRawPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).BeginInit();
            this.radPanelError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            this.SuspendLayout();
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.lblTitle);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Margin = new System.Windows.Forms.Padding(4);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(827, 56);
            this.headerPanel.TabIndex = 14;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblTitle.Size = new System.Drawing.Size(827, 56);
            this.lblTitle.TabIndex = 17;
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(767, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 56);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 4;
            this.exit.TabStop = false;
            // 
            // lblDetailSurat
            // 
            this.lblDetailSurat.AutoSize = false;
            this.lblDetailSurat.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailSurat.Location = new System.Drawing.Point(0, 56);
            this.lblDetailSurat.Name = "lblDetailSurat";
            this.lblDetailSurat.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetailSurat.Size = new System.Drawing.Size(827, 52);
            this.lblDetailSurat.TabIndex = 17;
            this.lblDetailSurat.Text = "<html>Pengaturan parameter digunakan oleh sistem (Harap login kembali setelah diu" +
    "bah !)</html>";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 108);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(808, 312);
            this.radScrollablePanel1.Size = new System.Drawing.Size(827, 314);
            this.radScrollablePanel1.TabIndex = 66;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Controls.Add(this.txtUserName, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtDBDataSource, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.radLabel25, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.radLabel2, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.pswPassword, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.lblDetail2, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblDetail1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radLabel5, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtInterval, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblSecond, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.radLabel3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.radLabel4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtSeperator1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtSeperator2, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.radLabel6, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtPagingSize, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.radLabel7, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.radLabel8, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.pswRawPassword, 1, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(808, 550);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // txtUserName
            // 
            this.txtUserName.AutoSize = false;
            this.txtUserName.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtUserName, 2);
            this.txtUserName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtUserName.Location = new System.Drawing.Point(404, 410);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(0);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(394, 50);
            this.txtUserName.TabIndex = 6;
            // 
            // txtDBDataSource
            // 
            this.txtDBDataSource.AutoSize = false;
            this.txtDBDataSource.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtDBDataSource, 2);
            this.txtDBDataSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDBDataSource.Location = new System.Drawing.Point(404, 360);
            this.txtDBDataSource.Margin = new System.Windows.Forms.Padding(0);
            this.txtDBDataSource.Name = "txtDBDataSource";
            this.txtDBDataSource.Size = new System.Drawing.Size(394, 50);
            this.txtDBDataSource.TabIndex = 5;
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.Location = new System.Drawing.Point(14, 414);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(386, 42);
            this.radLabel1.TabIndex = 32;
            this.radLabel1.Text = "Nama Pengguna";
            // 
            // radLabel25
            // 
            this.radLabel25.AutoSize = false;
            this.radLabel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel25.Location = new System.Drawing.Point(14, 364);
            this.radLabel25.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel25.Name = "radLabel25";
            this.radLabel25.Size = new System.Drawing.Size(386, 42);
            this.radLabel25.TabIndex = 18;
            this.radLabel25.Text = "Sumber Data";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel2.Location = new System.Drawing.Point(14, 464);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(386, 42);
            this.radLabel2.TabIndex = 99;
            this.radLabel2.Tag = "s";
            this.radLabel2.Text = "Kata Sandi";
            // 
            // pswPassword
            // 
            this.pswPassword.AutoSize = false;
            this.pswPassword.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.pswPassword, 2);
            this.pswPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pswPassword.Location = new System.Drawing.Point(404, 460);
            this.pswPassword.Margin = new System.Windows.Forms.Padding(0);
            this.pswPassword.Name = "pswPassword";
            this.pswPassword.PasswordChar = '*';
            this.pswPassword.Size = new System.Drawing.Size(394, 50);
            this.pswPassword.TabIndex = 7;
            // 
            // lblDetail2
            // 
            this.lblDetail2.AutoSize = false;
            this.lblDetail2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lblDetail2.BackgroundImage")));
            this.lblDetail2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetail2, 3);
            this.lblDetail2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetail2.Location = new System.Drawing.Point(13, 313);
            this.lblDetail2.Name = "lblDetail2";
            this.lblDetail2.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetail2.Size = new System.Drawing.Size(782, 44);
            this.lblDetail2.TabIndex = 105;
            this.lblDetail2.Text = "        PENGATURAN KONEKSI BASIS DATA";
            // 
            // lblDetail1
            // 
            this.lblDetail1.AutoSize = false;
            this.lblDetail1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lblDetail1.BackgroundImage")));
            this.lblDetail1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetail1, 3);
            this.lblDetail1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetail1.Location = new System.Drawing.Point(13, 13);
            this.lblDetail1.Name = "lblDetail1";
            this.lblDetail1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetail1.Size = new System.Drawing.Size(782, 44);
            this.lblDetail1.TabIndex = 106;
            this.lblDetail1.Text = "          UMUM";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel5.Location = new System.Drawing.Point(14, 64);
            this.radLabel5.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(386, 42);
            this.radLabel5.TabIndex = 107;
            this.radLabel5.Text = "<html>Interval Refresh Dashboard (detik)</html>";
            // 
            // txtInterval
            // 
            this.txtInterval.AutoSize = false;
            this.txtInterval.BackColor = System.Drawing.Color.Transparent;
            this.txtInterval.Location = new System.Drawing.Point(404, 60);
            this.txtInterval.Margin = new System.Windows.Forms.Padding(0);
            this.txtInterval.MaxLength = 3;
            this.txtInterval.Name = "txtInterval";
            this.txtInterval.Size = new System.Drawing.Size(78, 50);
            this.txtInterval.TabIndex = 1;
            this.txtInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblSecond
            // 
            this.lblSecond.AutoSize = false;
            this.lblSecond.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSecond.Location = new System.Drawing.Point(486, 64);
            this.lblSecond.Margin = new System.Windows.Forms.Padding(4);
            this.lblSecond.Name = "lblSecond";
            this.lblSecond.Size = new System.Drawing.Size(308, 42);
            this.lblSecond.TabIndex = 109;
            this.lblSecond.Text = "Detik";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel3.Location = new System.Drawing.Point(14, 114);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(386, 42);
            this.radLabel3.TabIndex = 110;
            this.radLabel3.Text = "Kode Sel (Dibuat otomatis) Pemisah 1 (1 karakter)";
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel4.Location = new System.Drawing.Point(14, 164);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(386, 42);
            this.radLabel4.TabIndex = 111;
            this.radLabel4.Text = "Kode Sel (Dibuat otomatis) Pemisah 2 (1 karakter)";
            // 
            // txtSeperator1
            // 
            this.txtSeperator1.AutoSize = false;
            this.txtSeperator1.BackColor = System.Drawing.Color.Transparent;
            this.txtSeperator1.Location = new System.Drawing.Point(404, 110);
            this.txtSeperator1.Margin = new System.Windows.Forms.Padding(0);
            this.txtSeperator1.MaxLength = 1;
            this.txtSeperator1.Name = "txtSeperator1";
            this.txtSeperator1.Size = new System.Drawing.Size(78, 50);
            this.txtSeperator1.TabIndex = 2;
            this.txtSeperator1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSeperator2
            // 
            this.txtSeperator2.AutoSize = false;
            this.txtSeperator2.BackColor = System.Drawing.Color.Transparent;
            this.txtSeperator2.Location = new System.Drawing.Point(404, 160);
            this.txtSeperator2.Margin = new System.Windows.Forms.Padding(0);
            this.txtSeperator2.MaxLength = 1;
            this.txtSeperator2.Name = "txtSeperator2";
            this.txtSeperator2.Size = new System.Drawing.Size(78, 50);
            this.txtSeperator2.TabIndex = 3;
            this.txtSeperator2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Location = new System.Drawing.Point(14, 214);
            this.radLabel6.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(386, 42);
            this.radLabel6.TabIndex = 116;
            this.radLabel6.Text = "Ukuran Halaman Grid";
            // 
            // txtPagingSize
            // 
            this.txtPagingSize.AutoSize = false;
            this.txtPagingSize.BackColor = System.Drawing.Color.Transparent;
            this.txtPagingSize.Location = new System.Drawing.Point(404, 210);
            this.txtPagingSize.Margin = new System.Windows.Forms.Padding(0);
            this.txtPagingSize.MaxLength = 3;
            this.txtPagingSize.Name = "txtPagingSize";
            this.txtPagingSize.Size = new System.Drawing.Size(78, 50);
            this.txtPagingSize.TabIndex = 4;
            this.txtPagingSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel7.Location = new System.Drawing.Point(486, 214);
            this.radLabel7.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(308, 42);
            this.radLabel7.TabIndex = 117;
            this.radLabel7.Text = "Data / Halaman";
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel8.Location = new System.Drawing.Point(14, 264);
            this.radLabel8.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(386, 42);
            this.radLabel8.TabIndex = 118;
            this.radLabel8.Text = "Password Unduh Data Mentah (default:123456)";
            // 
            // pswRawPassword
            // 
            this.pswRawPassword.AutoSize = false;
            this.pswRawPassword.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.pswRawPassword, 2);
            this.pswRawPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pswRawPassword.Location = new System.Drawing.Point(404, 260);
            this.pswRawPassword.Margin = new System.Windows.Forms.Padding(0);
            this.pswRawPassword.MaxLength = 20;
            this.pswRawPassword.Name = "pswRawPassword";
            this.pswRawPassword.PasswordChar = '*';
            this.pswRawPassword.Size = new System.Drawing.Size(394, 50);
            this.pswRawPassword.TabIndex = 119;
            // 
            // radPanelError
            // 
            this.radPanelError.Controls.Add(this.lblError);
            this.radPanelError.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanelError.Location = new System.Drawing.Point(0, 422);
            this.radPanelError.Margin = new System.Windows.Forms.Padding(0);
            this.radPanelError.Name = "radPanelError";
            this.radPanelError.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radPanelError.Size = new System.Drawing.Size(827, 43);
            this.radPanelError.TabIndex = 67;
            // 
            // lblError
            // 
            this.lblError.AutoSize = false;
            this.lblError.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.warning;
            this.lblError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblError.Location = new System.Drawing.Point(15, 0);
            this.lblError.Name = "lblError";
            this.lblError.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.lblError.Size = new System.Drawing.Size(812, 39);
            this.lblError.TabIndex = 24;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnBack);
            this.lblButton.Controls.Add(this.btnSave);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 467);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(827, 108);
            this.lblButton.TabIndex = 68;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.Location = new System.Drawing.Point(30, 30);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(244, 48);
            this.btnBack.TabIndex = 9;
            this.btnBack.Tag = "";
            this.btnBack.Text = "&BATAL";
            this.btnBack.ThemeName = "MaterialBlueGrey";
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.if_checked_2_309094;
            this.btnSave.Location = new System.Drawing.Point(492, 30);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(305, 48);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "&SIMPAN";
            this.btnSave.ThemeName = "MaterialBlueGrey";
            // 
            // ChangeSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radPanelError);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblDetailSurat);
            this.Controls.Add(this.headerPanel);
            this.Name = "ChangeSettings";
            this.Size = new System.Drawing.Size(827, 575);
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).EndInit();
            this.headerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDBDataSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pswPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSecond)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSeperator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSeperator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPagingSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pswRawPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).EndInit();
            this.radPanelError.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadPanel headerPanel;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel lblDetailSurat;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private Telerik.WinControls.UI.RadPanel radPanelError;
        private Telerik.WinControls.UI.RadLabel lblError;
        private System.Windows.Forms.PictureBox exit;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadTextBox txtUserName;
        private Telerik.WinControls.UI.RadTextBox txtDBDataSource;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel25;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox pswPassword;
        private Telerik.WinControls.UI.RadLabel lblDetail2;
        private Telerik.WinControls.UI.RadLabel lblDetail1;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox txtInterval;
        private Telerik.WinControls.UI.RadLabel lblSecond;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox txtSeperator1;
        private Telerik.WinControls.UI.RadTextBox txtSeperator2;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnBack;
        private Telerik.WinControls.UI.RadButton btnSave;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox txtPagingSize;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadTextBox pswRawPassword;
    }
}
