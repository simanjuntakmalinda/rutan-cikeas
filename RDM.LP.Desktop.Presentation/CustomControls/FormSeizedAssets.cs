﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.DataAccess.Helper;
using Microsoft.VisualBasic;
using System.Collections;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormSeizedAssets : Base
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSeizedAssets));
        private Point MouseDownLocation;
        public RadGridView GVSeizedAssets { get { return this.gvSeizedAssets; } set { } }

        public DataTable dtAssets = new DataTable();

        public FormSeizedAssets()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
            LoadData();
        }

        private void InitForm()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            lblError.Text = String.Empty;
            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     TAMBAH BARANG SITAAN BARU";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;
            this.lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
            ddlCase.SelectedIndex = 0;

            GridViewImageColumn btnDelete = new GridViewImageColumn();
            btnDelete.HeaderText = "";
            btnDelete.Name = "btnDelete";

            gvSeizedAssets.AutoGenerateColumns = false;
            gvSeizedAssets.Columns.Insert(0, btnDelete);
            gvSeizedAssets.Refresh();
            gvSeizedAssets.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            dtAssets.Clear();
            dtAssets.Columns.Add("Id");
            dtAssets.Columns.Add("CategoryId");
            dtAssets.Columns.Add("CategoryName");
            dtAssets.Columns.Add("Detail");
            dtAssets.Columns.Add("Quantity");
            dtAssets.Columns.Add("StorageType");
            dtAssets.Columns.Add("StorageLocation");
            dtAssets.Columns.Add("ConfiscatedDate");
            dtAssets.Columns.Add("Note");

            LoadDataSeizedAssetsCaseToGrid(gvSeizedAssets, Convert.ToInt32(ddlCase.SelectedValue));
            UserFunction.SetInitGridView(gvSeizedAssets);
        }

        private void LoadDataSeizedAssetsCaseToGrid(RadGridView gvAssets, int Id)
        {
            gvAssets.DataSource = null;
            SeizedAssetsService aset = new SeizedAssetsService();
            IEnumerable<SeizedAssets> data = aset.GetByCaseId(Id);

            dtAssets.Rows.Clear();

            foreach (var _data in data)
            {
                DataRow dr = dtAssets.NewRow();
                dr["Id"] = _data.Id;
                dr["CategoryId"] = _data.CategoryId;
                dr["CategoryName"] = _data.CategoryName;
                dr["Detail"] = _data.Detail;
                dr["Quantity"] = _data.Quantity;
                dr["StorageType"] = _data.StorageType;
                dr["StorageLocation"] = _data.StorageLocation;
                dr["ConfiscatedDate"] = _data.ConfiscatedDate != null ?_data.ConfiscatedDate.Value.ToString("dd MMM yyyy", new System.Globalization.CultureInfo("id-ID")) : null;
                dr["Note"] = _data.Note;
                dtAssets.Rows.Add(dr);
            }

            LoadDataToGridview(gvAssets, dtAssets);            
        }

        public void LoadDataToGridview(RadGridView gvAssets, DataTable dtAssets)
        {
            gvAssets.AutoGenerateColumns = true;
            gvAssets.DataSource = dtAssets;

            gvAssets.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            foreach (GridViewDataColumn col in gvAssets.Columns)
            {
                col.IsVisible = false;
                col.HeaderTextAlignment = ContentAlignment.MiddleLeft;
                col.ReadOnly = true;
            }

            gvAssets.Columns[0].Width = 50;
            gvAssets.Columns[0].IsVisible = true;
            gvAssets.Columns[0].ReadOnly = false;
            gvAssets.Columns[0].NullValue = Resources.delete;

            gvAssets.Columns["Id"].IsVisible = false;
            gvAssets.Columns["Id"].HeaderText = "Id";
            gvAssets.Columns["Id"].Width = 100;

            gvAssets.Columns["CategoryName"].IsVisible = true;
            gvAssets.Columns["CategoryName"].HeaderText = "NAMA KATEGORI";
            gvAssets.Columns["CategoryName"].Width = 200;

            gvAssets.Columns["Detail"].IsVisible = true;
            gvAssets.Columns["Detail"].HeaderText = "DETAIL";
            gvAssets.Columns["Detail"].Width = 200;

            gvAssets.Columns["Quantity"].IsVisible = true;
            gvAssets.Columns["Quantity"].HeaderText = "JUMLAH";
            gvAssets.Columns["Quantity"].Width = 200;

            gvAssets.Columns["StorageType"].IsVisible = true;
            gvAssets.Columns["StorageType"].HeaderText = "TIPE PENYIMPANAN";
            gvAssets.Columns["StorageType"].Width = 200;

            gvAssets.Columns["StorageLocation"].IsVisible = true;
            gvAssets.Columns["StorageLocation"].HeaderText = "LOKASI PENYIMPANAN";
            gvAssets.Columns["StorageLocation"].Width = 200;

            gvAssets.Columns["ConfiscatedDate"].IsVisible = true;
            gvAssets.Columns["ConfiscatedDate"].HeaderText = "TANGGAL SITA";
            gvAssets.Columns["ConfiscatedDate"].Width = 200;
            gvAssets.Columns["ConfiscatedDate"].FormatInfo = new System.Globalization.CultureInfo("id-ID");
            gvAssets.Columns["ConfiscatedDate"].FormatString = "{0:dd MMM yyyy}";


            gvAssets.Columns["Note"].IsVisible = true;
            gvAssets.Columns["Note"].HeaderText = "CATATAN";
            gvAssets.Columns["Note"].Width = 200;
        }

        private void InitEvents()
        {
            //Events
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            ddlCase.KeyDown += Control_KeyDown;
            ddlCase.SelectedIndexChanged += DdlCase_SelectedIndexChanged;
            gvSeizedAssets.CellClick += GvInmates_CellClick;
            btnAdd.Click += BtnAdd_Click;
            btnBack.Click += BtnBack_Click;
            btnSave.Click += BtnSave_Click;

            ddlCase.KeyDown += Control_KeyDown;
            btnAdd.KeyDown += Control_KeyDown;
            gvSeizedAssets.KeyDown += Control_KeyDown;
            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                if (this.Tag == null)
                    SaveData();
                else
                    UpdatedData();

                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Disimpan !");
                UserFunction.LoadDataSeizedAssetsToGrid(MainForm.formMain.ListSeizedAssets.GvSeizedAssets);
                MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
                MainForm.formMain.AddNew.Text = "TAMBAH DATA BARANG SITAAN BARU";
                this.lblTitle.Text = "     TAMBAH BARANG SITAAN BARU";
                this.Tag = null;
            }
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            this.Tag = null;
            this.lblTitle.Text = "     TAMBAH DATA BARANG SITAAN BARU";
            MainForm.formMain.AddNew.Text = "TAMBAH DATA BARANG SITAAN BARU";
            UserFunction.ClearControls(tableLayoutPanel1);
            MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            MainForm.formMain.NewSeizedAssets.ClearForm();
            MainForm.formMain.NewSeizedAssets.Show();
        }

        private void GvInmates_CellClick(object sender, GridViewCellEventArgs e)
        {
            int idx = e.Row.Index;
            if (e.Column == null)
                return;

            switch (e.Column.Index)
            {
                case 0:
                    var question = UserFunction.Confirm("Hapus Barang Sitaan?");
                    if (question == DialogResult.Yes)
                    {
                        gvSeizedAssets.DataSource = null;
                        dtAssets.Rows.RemoveAt(idx);
                        MainForm.formMain.SeizedAssets.LoadDataToGridview(gvSeizedAssets, dtAssets);
                    }
                    break;
            }
        }

        private void DeleteSeizedAssets(string id)
        {
            using (SeizedAssetsService assets = new SeizedAssetsService())
            {
                var data = assets.GetById(Convert.ToInt32(id));
                assets.DeleteById(Convert.ToInt32(id));
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Delete Seized Assets, Data=" + UserFunction.JsonString(data) });
            }
        }

        private void DdlCase_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            LoadDataSeizedAssetsCaseToGrid(gvSeizedAssets, Convert.ToInt32(ddlCase.SelectedValue));
        }

        class ComboItem
        {
            public int ID { get; set; }
            public string Text { get; set; }
        }

        public void LoadData()
        {
            this.lblTitle.Text = "     TAMBAH DATA BARANG SITAAN BARU";

            CasesService caseserv = new CasesService();
            ddlCase.DisplayMember = "Name";
            ddlCase.ValueMember = "Id";
            ddlCase.DataSource = caseserv.Get();

            ddlCase.SelectedIndex = 0;
            ddlCase.Enabled = true;

            if (this.Tag != null)
            {
                this.lblTitle.Text = "     UBAH BARANG YANG DISITA";
                MainForm.formMain.AddNew.Text = "UBAH BARANG YANG DISITA";
                ddlCase.SelectedValue = Convert.ToInt32(this.Tag);
                ddlCase.Enabled = false;
            }
        }

        private void ClearData()
        {

        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }

        private void SaveData()
        {
            if (this.Tag == null)
            {
                int CaseId = Convert.ToInt32(ddlCase.SelectedItem.Value);

                SeizedAssetsService assetsserv = new SeizedAssetsService();
                var assets = assetsserv.GetByCaseId(CaseId);
                foreach (var _data in assets)
                {
                    assetsserv.DeleteById(_data.Id);
                }
                assetsserv.DeleteSeizedAssetsCase(CaseId);

                InsertData();
            }
            else
                UpdatedData();
        }

        private void UpdatedData()
        {
            SeizedAssetsService assetsserv = new SeizedAssetsService();
            var assets = assetsserv.GetByCaseId(Convert.ToInt32(this.Tag));
            foreach(var _data in assets)
            {
                assetsserv.DeleteById(_data.Id);
            }
            assetsserv.DeleteSeizedAssetsCase(Convert.ToInt32(this.Tag));

            InsertData();
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERCELL.ToString(), Activities = "Update Seized Assets, New Data= " + UserFunction.JsonString(assets) });
        }

        private void InsertData()
        {
            int CategoryId = 0;
            string Detail = "";
            string Quantity = "";
            string StorageType = "";
            string StorageLocation = "";
            string ConfiscatedDate = "";
            string Note = "";

            SeizedAssetsService assetsserv = new SeizedAssetsService();
            for (int i = 0; i< dtAssets.Rows.Count; i++)
            {
                for (int j = 0; j < dtAssets.Columns.Count; j++)
                {
                    object _data = dtAssets.Rows[i].ItemArray[j];
                    if (j == 1)
                        CategoryId = Convert.ToInt32(_data);
                    else if (j == 3)
                        Detail = _data.ToString();
                    else if (j == 4)
                        Quantity = _data.ToString();
                    else if (j == 5)
                        StorageType = _data.ToString();
                    else if (j == 6)
                        StorageLocation = _data.ToString();
                    else if (j == 7)
                        ConfiscatedDate = _data.ToString();
                    else if (j == 8)
                        Note = _data.ToString();
                }

                var asset = new SeizedAssets
                {
                    CategoryId = CategoryId,
                    Detail = Detail.ToUpper(),
                    Quantity = Quantity.ToUpper(),
                    StorageType = StorageType.ToUpper(),
                    StorageLocation = StorageLocation.ToUpper(),
                    ConfiscatedDate = Convert.ToDateTime(ConfiscatedDate),
                    Note = Note.ToUpper(),
                    CreatedDate = DateTime.Now.ToLocalTime(),
                    CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
                };
                int AssetsId = assetsserv.Post(asset);

                var assetInmate = new SeizedAssets
                {
                    SeizedAssetsId = AssetsId,
                    CaseId = Convert.ToInt32(ddlCase.SelectedItem.Value),
                    CreatedDate = DateTime.Now.ToLocalTime(),
                    CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
                };
                assetsserv.PostAssetsCase(assetInmate);

                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERCELL.ToString(), Activities = "Add New Seized Assets, Data=" + UserFunction.JsonString(asset) });
            }
        }

        private bool DataValid()
        {
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;
            if (this.ddlCase.SelectedItem.Value == null)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Kategori Barang yang Disita!";
                return false;
            }
            return true;
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }
    }
}
