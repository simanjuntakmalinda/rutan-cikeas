﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class ListVisitorVIP
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListVisitorVIP));
            this.gvVisitorVIPSudahCheckedOut = new Telerik.WinControls.UI.RadGridView();
            this.lblDetail = new Telerik.WinControls.UI.RadLabel();
            this.pdfSudahCheckedOut = new System.Windows.Forms.PictureBox();
            this.excelSudahCheckedOut = new System.Windows.Forms.PictureBox();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.picRefresh = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisitorVIPSudahCheckedOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisitorVIPSudahCheckedOut.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).BeginInit();
            this.lblDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pdfSudahCheckedOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelSudahCheckedOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRefresh)).BeginInit();
            this.SuspendLayout();
            // 
            // gvVisitorVIPSudahCheckedOut
            // 
            this.gvVisitorVIPSudahCheckedOut.AutoScroll = true;
            this.gvVisitorVIPSudahCheckedOut.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvVisitorVIPSudahCheckedOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvVisitorVIPSudahCheckedOut.EnableCustomFiltering = true;
            this.gvVisitorVIPSudahCheckedOut.Location = new System.Drawing.Point(0, 108);
            // 
            // 
            // 
            this.gvVisitorVIPSudahCheckedOut.MasterTemplate.AllowAddNewRow = false;
            this.gvVisitorVIPSudahCheckedOut.MasterTemplate.AllowCellContextMenu = false;
            this.gvVisitorVIPSudahCheckedOut.MasterTemplate.AllowColumnChooser = false;
            this.gvVisitorVIPSudahCheckedOut.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.gvVisitorVIPSudahCheckedOut.MasterTemplate.AllowColumnReorder = false;
            this.gvVisitorVIPSudahCheckedOut.MasterTemplate.AllowDragToGroup = false;
            this.gvVisitorVIPSudahCheckedOut.MasterTemplate.EnableCustomFiltering = true;
            this.gvVisitorVIPSudahCheckedOut.MasterTemplate.EnableFiltering = true;
            this.gvVisitorVIPSudahCheckedOut.MasterTemplate.EnablePaging = true;
            this.gvVisitorVIPSudahCheckedOut.MasterTemplate.PageSize = 10;
            this.gvVisitorVIPSudahCheckedOut.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gvVisitorVIPSudahCheckedOut.Name = "gvVisitorVIPSudahCheckedOut";
            this.gvVisitorVIPSudahCheckedOut.Size = new System.Drawing.Size(1349, 651);
            this.gvVisitorVIPSudahCheckedOut.TabIndex = 58;
            // 
            // lblDetail
            // 
            this.lblDetail.AutoSize = false;
            this.lblDetail.BackColor = System.Drawing.Color.Gainsboro;
            this.lblDetail.Controls.Add(this.picRefresh);
            this.lblDetail.Controls.Add(this.pdfSudahCheckedOut);
            this.lblDetail.Controls.Add(this.excelSudahCheckedOut);
            this.lblDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetail.Location = new System.Drawing.Point(0, 56);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDetail.Size = new System.Drawing.Size(1349, 52);
            this.lblDetail.TabIndex = 16;
            this.lblDetail.Text = "DAFTAR PENGUNJUNG VIP";
            // 
            // pdfSudahCheckedOut
            // 
            this.pdfSudahCheckedOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pdfSudahCheckedOut.Dock = System.Windows.Forms.DockStyle.Right;
            this.pdfSudahCheckedOut.Image = ((System.Drawing.Image)(resources.GetObject("pdfSudahCheckedOut.Image")));
            this.pdfSudahCheckedOut.Location = new System.Drawing.Point(1263, 10);
            this.pdfSudahCheckedOut.Name = "pdfSudahCheckedOut";
            this.pdfSudahCheckedOut.Size = new System.Drawing.Size(43, 32);
            this.pdfSudahCheckedOut.TabIndex = 3;
            this.pdfSudahCheckedOut.TabStop = false;
            // 
            // excelSudahCheckedOut
            // 
            this.excelSudahCheckedOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.excelSudahCheckedOut.Dock = System.Windows.Forms.DockStyle.Right;
            this.excelSudahCheckedOut.Image = ((System.Drawing.Image)(resources.GetObject("excelSudahCheckedOut.Image")));
            this.excelSudahCheckedOut.Location = new System.Drawing.Point(1306, 10);
            this.excelSudahCheckedOut.Name = "excelSudahCheckedOut";
            this.excelSudahCheckedOut.Size = new System.Drawing.Size(43, 32);
            this.excelSudahCheckedOut.TabIndex = 2;
            this.excelSudahCheckedOut.TabStop = false;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(1349, 56);
            this.lblTitle.TabIndex = 1;
            // 
            // picRefresh
            // 
            this.picRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picRefresh.Dock = System.Windows.Forms.DockStyle.Right;
            this.picRefresh.Image = ((System.Drawing.Image)(resources.GetObject("picRefresh.Image")));
            this.picRefresh.Location = new System.Drawing.Point(1220, 10);
            this.picRefresh.Name = "picRefresh";
            this.picRefresh.Size = new System.Drawing.Size(43, 32);
            this.picRefresh.TabIndex = 11;
            this.picRefresh.TabStop = false;
            // 
            // ListVisitorVIP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gvVisitorVIPSudahCheckedOut);
            this.Controls.Add(this.lblDetail);
            this.Controls.Add(this.lblTitle);
            this.Name = "ListVisitorVIP";
            this.Size = new System.Drawing.Size(1349, 759);
            ((System.ComponentModel.ISupportInitialize)(this.gvVisitorVIPSudahCheckedOut.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVisitorVIPSudahCheckedOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).EndInit();
            this.lblDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pdfSudahCheckedOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelSudahCheckedOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRefresh)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel lblDetail;
        private Telerik.WinControls.UI.RadGridView gvVisitorVIPSudahCheckedOut;
        private System.Windows.Forms.PictureBox pdfSudahCheckedOut;
        private System.Windows.Forms.PictureBox excelSudahCheckedOut;
        private System.Windows.Forms.PictureBox picRefresh;
    }
}
