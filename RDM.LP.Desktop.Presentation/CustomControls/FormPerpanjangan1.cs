﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.DataAccess.Helper;
using Microsoft.VisualBasic;
using System.Diagnostics;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormPerpanjangan1 : Base
    {
        private Point MouseDownLocation;
        private PerpanjanganTahanan OldData;
        public FormPerpanjangan1()
        {
            InitializeComponent();
            InitForm();
            LoadData();
            InitEvents();
        }

        class ComboItem
        {
            public int ID { get; set; }
            public string Text { get; set; }
        }

        public void LoadData()
        {
            this.lblTitle.Text = "     TAMBAH DATA BARU";

            ClearForm();

            if (this.Tag != null && this.Tag != string.Empty)
            {
                PerpanjanganTahananService cellserv = new PerpanjanganTahananService();
                var data = cellserv.GetById(Convert.ToInt32(this.Tag));
                
                MainForm.formMain.AddNew.Text = "UBAH DATA";
                this.lblTitle.Text = "     UBAH DATA";
                OldData = data;
                txtNoSuratPerpanjangan.Text = data.PerpanjanganKe;
                dtPerpanjanganTahanan.Value = data.TanggalPerpanjangan != null ? (DateTime)data.TanggalPerpanjangan : DateTime.Now.ToLocalTime();
                ddlDurasi.SelectedValue = data.Durasi;
            }
        }

        private void ClearForm()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            txtNoSuratPerpanjangan.Text = string.Empty;
            dtPerpanjanganTahanan.Value = DateTime.Now;
            ddlDurasi.ResetText();
            ddlDurasi.Items.Add("20");
            ddlDurasi.Items.Add("30");
            ddlDurasi.Items.Add("60");
            ddlDurasi.Items.Add("90");
            ddlDurasi.Items.Add("120");
            ddlDurasi.Items.Add("180");
            ddlDurasi.SelectedIndex = -1;
        }

        private void InitEvents()
        {
            //Events
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            
            txtNoSuratPerpanjangan.KeyDown += Control_KeyDown;
            dtPerpanjanganTahanan.KeyDown += Control_KeyDown;
            ddlDurasi.KeyDown += Control_KeyDown;
            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;
            btnBrowse.Click += Browse_Click;
        }

        private void Browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            //To where your opendialog box get starting location. My initial directory location is desktop.
            openFileDialog1.InitialDirectory = "C://Desktop";
            //Your opendialog box title name.
            openFileDialog1.Title = "Select file to be upload.";
            //which type file format you want to upload in database. just add them.
            openFileDialog1.Filter = "Select Valid Document(*.pdf; *.doc; *.xlsx; *.html)|*.pdf; *.docx; *.xlsx; *.html";
            //FilterIndex property represents the index of the filter currently selected in the file dialog box.
            openFileDialog1.FilterIndex = 1;
            try
            {
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (openFileDialog1.CheckFileExists)
                    {
                        string path = System.IO.Path.GetFullPath(openFileDialog1.FileName);
                        txtPath.Text = path;
                    }
                }
                else
                {
                    MessageBox.Show("Please Upload document.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }      

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                if (this.Tag == null)
                    SaveCell();
                else
                    UpdatedData();

                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Disimpan !");
                UserFunction.LoadDataCellToGrid(MainForm.formMain.ListCell.GvCell);
                MainForm.formMain.CellAllocation.DDLCellCode.Refresh();
                MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
                MainForm.formMain.AddNew.Text = "TAMBAH DATA BARU";
                this.lblTitle.Text = "     TAMBAH DATA BARU";
                this.Tag = null;
            }
        }

        private void SaveCell()
        {
            if (this.Tag == null)
                InsertData();
            else
                UpdatedData();
        }

        private void UpdatedData()
        {
            
        }

        private void InsertData()
        {
            
            
        }

        private void InitForm()
        {
            this.lblError.Visible = false;
            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     TAMBAH SEL BARU";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;
            this.lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 15.5f;

            this.lblRequired.LabelElement.CustomFont = Global.MainFont;
            this.lblRequired.LabelElement.CustomFontSize = 14f;
            this.lblRequired.ForeColor = Color.White;
            this.lblRequired.BackColor = Color.FromArgb(77, 77, 77);

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ClearForm();
            this.Tag = null;
            this.Hide();
        }

        private bool DataValid()
        {
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;
            if ((string)this.txtNoSuratPerpanjangan.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Nomor Surat!";
                return false;
            }
            if (this.dtPerpanjanganTahanan.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi tanggal!";
                return false;
            }
            if ((string)this.ddlDurasi.SelectedItem.Value == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Durasi Penahanan!";
                return false;
            }
            if (this.txtPath.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Upload File!";
                return false;
            }
            return true;
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void txtNoSuratPerpanjangan_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblButton_Click(object sender, EventArgs e)
        {

        }

        private void btnBack_Click_1(object sender, EventArgs e)
        {

        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {

        }

        private void headerPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lbl6_Click(object sender, EventArgs e)
        {

        }

        private void lbl3_Click(object sender, EventArgs e)
        {

        }

        private void lbl2_Click(object sender, EventArgs e)
        {

        }

        private void lbl1_Click(object sender, EventArgs e)
        {

        }

        private void lbl5_Click(object sender, EventArgs e)
        {

        }

        private void lbl4_Click(object sender, EventArgs e)
        {

        }

        private void radLabel5_Click(object sender, EventArgs e)
        {

        }

        private void tableFormCell_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblDetail_Click(object sender, EventArgs e)
        {

        }

        private void PanelContainer_Paint(object sender, PaintEventArgs e)
        {

        }

        private void radScrollablePanel1_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void radPanel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void radPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtcellnumber_TextChanged(object sender, EventArgs e)
        {

        }

        private void radPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ddlDurasi_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {

        }

        private void radPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void radPanelError_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dtPerpanjanganTahanan_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtFile_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
