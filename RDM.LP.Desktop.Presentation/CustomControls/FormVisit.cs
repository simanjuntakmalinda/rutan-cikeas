﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.Desktop.Presentation;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormVisit : Base
    {
        public Label TahananId { get { return this.IdTahanan; } set { this.IdTahanan = value; } }
        public RadTextBox NamaTahanan { get { return this.txtNamaTahanan; } set { this.txtNamaTahanan = value; } }
        public Label PengunjungId { get { return this.IdPengunjung; } set { this.IdPengunjung = value; } }
        public RadTextBox NamaPegunjung { get { return this.txtNamaPegunjung; } set { this.txtNamaPegunjung = value; } }
        public FormVisit()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            //Events
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            btnSearchTahanan.Click += btnSearchTahanan_Click;
            btnSearchVisitor.Click += btnSearchVisitor_Click;

            txtNamaPegunjung.KeyDown += Control_KeyDown;
            txtNamaTahanan.KeyDown += Control_KeyDown;
            ddlMulai.KeyDown += Control_KeyDown;
            ddlMulaiJam.KeyDown += Control_KeyDown;
            ddlSelesai.KeyDown += Control_KeyDown;
            ddlSelesaiJam.KeyDown += Control_KeyDown;
            txtKeperluan.KeyDown += Control_KeyDown;
            btnSearchTahanan.KeyDown += Control_KeyDown;
            btnSearchVisitor.KeyDown += Control_KeyDown;
            btnAddTahanan.KeyDown += Control_KeyDown;
            btnAddVisitor.KeyDown += Control_KeyDown;
            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }

        private void btnSearchVisitor_Click(object sender, EventArgs e)
        {
            MainForm.formMain.SearchData.Tag = SearchType.Visitor.ToString();
            MainForm.formMain.SearchData.Show();
        }

        private void btnSearchTahanan_Click(object sender, EventArgs e)
        {
            MainForm.formMain.SearchData.Tag = SearchType.Tahanan.ToString();
            MainForm.formMain.SearchData.Show();
        }

        private void InitForm()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     PENDAFTARAN PENGUNJUNG";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 15.5f;

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
            this.ddlMulaiJam.Format = DateTimePickerFormat.Custom;
            this.ddlMulaiJam.CustomFormat = "hh:mm.fff";
            this.ddlMulaiJam.ShowUpDown = true;

            this.ddlSelesaiJam.Format = DateTimePickerFormat.Custom;
            this.ddlSelesaiJam.CustomFormat = "hh:mm.fff";
            this.ddlSelesaiJam.ShowUpDown = true;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            UserFunction.ClearControls(tableLayoutPanel1);
            MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                SavePengunjung();
                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Data Pengunjung Berhasil Ditambahkan !");
                UserFunction.LoadDataPegunjungToGrid(MainForm.formMain.ListVisitor.GvVisitor);
                MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
                this.Tag = null;
            }
        }

        private void SavePengunjung()
        {
            var start = new DateTime(ddlMulai.Value.Year, ddlMulai.Value.Month, ddlMulai.Value.Day, ddlMulaiJam.Value.Hour, ddlMulaiJam.Value.Minute, ddlMulaiJam.Value.Second);
            var end = new DateTime(ddlSelesai.Value.Year, ddlSelesai.Value.Month, ddlSelesai.Value.Day, ddlSelesaiJam.Value.Hour, ddlSelesaiJam.Value.Minute, ddlSelesaiJam.Value.Second);
            var data = new Visit
            {
                InmatesId = Convert.ToInt32(TahananId.Text),
                InmatesName = txtNamaPegunjung.Text,
                VisitorId = Convert.ToInt32(PengunjungId.Text),
                StartVisit = start,
                EndVisit = end,
                BringingItems = txtKeperluan.Text.Replace("'","`"),
                CreatedDate = DateTime.Now.ToLocalTime(),
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
            };
            VisitService pengunjung = new VisitService();
            pengunjung.Post(data);
        }


        private bool DataValid()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            if (this.txtNamaPegunjung.Text == string.Empty )
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Nama Pengunjung Harus Dipilih!";
                return false;
            }

            if (this.txtNamaTahanan.Text == string.Empty )
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Nama Tahanaan Harus Dipilih!";
                return false;
            }
            if (this.ddlMulai.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Waktu Mulai Kunjungan Harus Dipilih!";
                return false;
            }
            if (this.ddlMulai.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Waktu Mulai Kunjungan Harus Dipilih!";
                return false;
            }
            if (this.txtKeperluan.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Detail Keperluan Kunjungan Harus Diisi!";
                return false;
            }
            return true;
        }

    }
}
