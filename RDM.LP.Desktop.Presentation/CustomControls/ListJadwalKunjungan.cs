﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListJadwalKunjungan : Base
    {
        public RadGridView GvJadwalKunjungan { get { return this.gvJadwalKunjungan; } }
        public ListJadwalKunjungan()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvJadwalKunjungan.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvJadwalKunjungan.RowFormatting += radGridView_RowFormatting;
            gvJadwalKunjungan.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvJadwalKunjungan.CellClick += gvJadwalKunjungan_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
            picRefresh.Click += PicRefresh_Click;
        }

        private void PicRefresh_Click(object sender, EventArgs e)
        {
            UserFunction.LoadDataJadwalKunjunganToGrid(gvJadwalKunjungan);
            gvJadwalKunjungan.Refresh();
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "JadwalKunjungan.pdf", "JADWAL KUNJUNGAN LIST DATA", gvJadwalKunjungan);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "JadwalKunjungan.csv", "JADWAL KUNJUNGAN LIST DATA", gvJadwalKunjungan);
        }

        private void gvJadwalKunjungan_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == null)
                return;

            switch (e.Column.Index)
            {
                case 0:
                    MainForm.formMain.JadwalKunjunganDetail.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.JadwalKunjunganDetail.Show();
                    break;
                case 1:
                    MainForm.formMain.JadwalKunjungan.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.AddNew;
                    MainForm.formMain.JadwalKunjungan.LoadData();
                    break;
                case 2:
                    var question  = UserFunction.Confirm("Hapus pengguna ?");
                    if (question == DialogResult.Yes)
                    {
                        DeleteJadwalKunjungan(e.Row.Cells["Id"].Value.ToString());
                        UserFunction.MsgBox(TipeMsg.Info, "Jadwal Kunjungan telah dihapus !");
                        UserFunction.LoadDataJadwalKunjunganToGrid(gvJadwalKunjungan);
                        MainForm.formMain.JadwalKunjungan.LoadData();
                    }
                    break;
            }
            
        }

        private void DeleteJadwalKunjungan(string userName)
        {
            using (JadwalKunjunganService jadwalserv = new JadwalKunjunganService())
            {
                var data = jadwalserv.GetById(Convert.ToInt32(userName));
                jadwalserv.DeleteById(data.Id);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERUSER.ToString(), Activities = "Delete JadwalKunjungan, Data =" + UserFunction.JsonString(data) });
            }
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DAFTAR JADWAL KUNJUNGAN";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;
            this.lblDetail.BackColor = Color.FromArgb(77,77,77);
            this.lblDetail.ForeColor = Color.White;

            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Width = 15;
            btnDetail.Name = "btnDetail";
            gvJadwalKunjungan.AutoGenerateColumns = false;
            gvJadwalKunjungan.Columns.Insert(0, btnDetail);
            gvJadwalKunjungan.Refresh();
            gvJadwalKunjungan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            GridViewImageColumn btnUbah = new GridViewImageColumn();
            btnUbah.HeaderText = "";
            btnUbah.Name = "btnUbah";
            gvJadwalKunjungan.AutoGenerateColumns = false;
            gvJadwalKunjungan.Columns.Insert(1, btnUbah);
            gvJadwalKunjungan.Refresh();
            gvJadwalKunjungan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnHapus = new GridViewImageColumn();
            btnHapus.HeaderText = "";
            btnHapus.Name = "btnHapus";
            gvJadwalKunjungan.AutoGenerateColumns = false;
            gvJadwalKunjungan.Columns.Insert(2, btnHapus);
            gvJadwalKunjungan.Refresh();
            gvJadwalKunjungan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            UserFunction.LoadDataJadwalKunjunganToGrid(gvJadwalKunjungan);
            UserFunction.SetInitGridView(gvJadwalKunjungan);

        }
    }
}
