﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using System.Threading;
using System.IO;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class JadwalKunjunganDetail : Base
    {
        int countImages = 0;
        int totalImages = 0;
        List<PictureBox> ImagesList = new List<PictureBox>();
        private Point MouseDownLocation;

        public JadwalKunjunganDetail()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void LoadData(string id)
        {
            picCanvas.Image = null;
            picFinger.Image = null;
            totalImages = 0;
            countImages = 0;
            ImagesList.Clear();

            JadwalKunjunganService jadwalserv = new JadwalKunjunganService();
            var datajadwal = jadwalserv.GetDetailById(id);
            if (datajadwal != null)
            {
                BolehDikunjungi.Text = datajadwal.BolehDikunjungi == true ? "Diizinkan" : "Tidak Diizinkan";

                if (datajadwal.BolehDikunjungi == false)
                {
                    intensitasKunjungan.Text = "-";
                    waktuKunjungan.Text = "-";
                }
                else
                {
                    intensitasKunjungan.Text = datajadwal.PeriodeKunjungan;
                    waktuKunjungan.Text = datajadwal.WaktuKunjungan;

                    if (datajadwal.PeriodeKunjungan == "PER MINGGU")
                    {
                        lblWaktu.Text = "HARI KUNJUNGAN";
                    }
                    if (datajadwal.PeriodeKunjungan == "PER BULAN")
                    {
                        lblWaktu.Text = "TANGGAL KUNJUNGAN";
                    }
                }
                inputter.Text = datajadwal.CreatedBy;
                inputdate.Text = datajadwal.CreatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss");
                updater.Text = datajadwal.UpdatedBy;
                updatedate.Text = datajadwal.UpdatedDate == null ? string.Empty : datajadwal.UpdatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss");
                                
                InmatesService tahanan = new InmatesService();
                var data = tahanan.GetDetailByRegIdKunjungan(datajadwal.InmatesRegID);
                if (data != null)
                {
                    regID.Text = data.RegID;
                    namaLengkap.Text = data.FullName;
                    noSelTahanan.Text = data.CellCode;
                    LoadPhoto(data);
                    LoadDataFinger(data);
                }
            }
        }

        private void LoadDataFinger(Inmates data)
        {
            FingerPrintService fingerserv = new FingerPrintService();
            var finger = fingerserv.GetByRegID(data.RegID);
            if (finger != null)
            {
                var imageBytes = (byte[])Convert.FromBase64String(finger.ImagePrint);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);
                picFinger.Image = image;
            }
        }

        private void LoadPhoto(Inmates data)
        {
            PhotosService photoserv = new PhotosService();
            var foto = photoserv.GetByRegID(data.RegID);
            foreach (var item in foto)
            {
                var imageBytes = (byte[])Convert.FromBase64String(item.Data);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);

                ImagesList.Add(new PictureBox());
                ImagesList[ImagesList.Count - 1].Image = image;

                totalImages++;
                countImages++;
                picCanvas.Image = ImagesList[0].Image;
            }
            tot.Text = string.Format("{0}/{1}", countImages, totalImages);
        }

        private void InitForm()
        {
            ToolTip toolTip = new ToolTip();
            toolTip.ToolTipIcon = ToolTipIcon.Info;
            toolTip.ToolTipTitle = "Info";
            toolTip.SetToolTip(exit, "Tutup Jadwal Kunjungan Tahanan");
            lblButton.BackColor = Global.MainColor;
            lblTitle.BackColor = Global.MainColor;
            lblTitle.Text = "      JADWAL KUNJUNGAN TAHANAN";
            lblTitle.ForeColor = Color.White;
            lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;
            lblTitle.LabelElement.CustomFont = Global.MainFont;
            lblTitle.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            btnClose.ButtonElement.CustomFontStyle = FontStyle.Bold;
            btnClose.ButtonElement.CustomFont = Global.MainFont;
            btnClose.ButtonElement.CustomFontSize = Global.CustomFontSizeMain;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);

            lblInmatePhoto.BackColor = Global.MainColor;
            lblInmatePhoto.ForeColor = Color.White;
            lblVisitorFinger.BackColor = Global.MainColor;
            lblVisitorFinger.ForeColor = Color.White;
        }

        private void InitEvents()
        {
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            exit.Click += exit_Click;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            this.VisibleChanged += Form_VisibleChanged;
            btnClose.Click += btnClose_Click;
            next.Click += next_Click;
            prev.Click += prev_Click;
            picCanvas.Click += picCanvas_Click;
            picFinger.Click += picFinger_Click;
            closefull.Click += closefull_Click;
        }

        private void closefull_Click(object sender, EventArgs e)
        {
            fullPic.Image = null;
            fullpicPanel.Hide();
        }

        private void picCanvas_Click(object sender, EventArgs e)
        {
            fullpicPanel.Show();
            fullPic.Image = picCanvas.Image;
        }

        private void picFinger_Click(object sender, EventArgs e)
        {
            fullpicPanel.Show();
            fullPic.Image = picFinger.Image;
        }

        private void prev_Click(object sender, EventArgs e)
        {
            if (countImages == 0)
            {
                return;
            }
            else
            {
                LoadPhoto();
                countImages--;
            }
        }

        private void next_Click(object sender, EventArgs e)
        {
            if (countImages >= ImagesList.Count)
                return;
            else
                countImages++;
            LoadPhoto();
        }

        private void LoadPhoto()
        {
            if (countImages <= 0)
                picCanvas.Image = null;
            else
                picCanvas.Image = ImagesList[countImages - 1].Image;
            tot.Text = string.Format("{0}/{1}", countImages, totalImages);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (this.Tag != null)
                {
                    LoadData(this.Tag.ToString());
                    fullpicPanel.Location =
                    new Point(this.Width / 2 - fullpicPanel.Size.Width / 2,
                      this.Height / 2 - fullpicPanel.Size.Height / 2);
                }
                else
                    this.Hide();
            }
        }
    }
}
