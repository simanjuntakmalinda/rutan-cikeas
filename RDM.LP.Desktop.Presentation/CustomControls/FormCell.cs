﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.DataAccess.Helper;
using Microsoft.VisualBasic;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormCell : Base
    {
        private Point MouseDownLocation;
        private Cell OldData;
        public FormCell()
        {
            InitializeComponent();
            InitForm();
            LoadData();
            InitEvents();
        }

        class ComboItem
        {
            public int ID { get; set; }
            public string Text { get; set; }
        }

        public void LoadData()
        {
            this.lblTitle.Text = "     TAMBAH DATA SEL BARU";

            ClearForm();

            if (this.Tag != null && this.Tag != string.Empty)
            {
                CellService cellserv = new CellService();
                var data = cellserv.GetById(Convert.ToInt32(this.Tag));
                if (data == null)
                {
                    ddlBuildingName.SelectedIndex = 0;
                    ddlBuildingName.Enabled = true;

                    ddlCellStatus.SelectedIndex = 0;
                    ddlCellStatus.Enabled = true;

                    ddlCellFloor.SelectedIndex = 0;
                    ddlCellFloor.Enabled = true;

                    ddlCellType.SelectedIndex = 0;
                    ddlCellType.Enabled = true;

                    ddlCellStatus.SelectedValue = "TERSEDIA";
                    ddlCellStatus.Enabled = false;
                    return;
                }
                else
                {
                    MainForm.formMain.AddNew.Text = "UBAH DATA SEL";
                    this.lblTitle.Text = "     UBAH DATA SEL";
                    OldData = data;

                    //ddlBuildingName.SelectedIndex = 0;
                    //ddlBuildingName.Enabled = false;
                    ddlBuildingName.SelectedValue = data.BuildingId;

                    //ddlCellFloor.SelectedIndex = 0;
                    //ddlCellFloor.Enabled = false;
                    ddlCellFloor.SelectedValue = data.CellFloor;

                    //txtcellnumber.Enabled = false;
                    txtcellnumber.Text = data.CellNumber;
                    lblCellCode.Text = data.CellCode;

                    txtCellDescription.Text = data.CellDescription;

                    //ddlCellStatus.SelectedIndex = 0;
                    //ddlCellStatus.Enabled = false;
                    ddlCellStatus.SelectedValue = data.CellStatus;

                    //ddlCellType.SelectedIndex = 0;
                    //ddlCellType.Enabled = true;
                    ddlCellType.SelectedValue = data.CellType;
                }
            }
        }

        private void ClearForm()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            //ddl building
            BuildingService buildingserv = new BuildingService();
            ddlBuildingName.DisplayMember = "BuildingName";
            ddlBuildingName.ValueMember = "Id";
            ddlBuildingName.DataSource = buildingserv.Get();

            //ddl cell status
            CellStatusService cellstatusserv = new CellStatusService();
            ddlCellStatus.DisplayMember = "Status";
            ddlCellStatus.ValueMember = "Status";
            ddlCellStatus.DataSource = cellstatusserv.Get();

            //ddl cell type
            List<Cell> CellType = new List<Cell>();
            CellType.Add(new Cell() { CellTypeId = "1", CellType = "ONE MAN ONE CELL" });
            CellType.Add(new Cell() { CellTypeId = "2", CellType = "TWO MAN ONE CELL" });
            ddlCellType.DisplayMember = "CellType";
            ddlCellType.ValueMember = "CellTypeId";
            ddlCellType.DataSource = CellType;

            ddlBuildingName_SelectedIndexChanged(ddlBuildingName, null);

            lblCellCode.Text = " * AUTO GENERATED";
            txtcellnumber.Enabled = true;
            txtcellnumber.Text = null;

            ddlCellFloor.SelectedIndex = 0;
            ddlCellFloor.Enabled = true;
            ddlCellFloor.SelectedValue = null;

            txtCellDescription.Text = null;
            lblError.Text = string.Empty;
        }

        private void InitEvents()
        {
            //Events
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            ddlBuildingName.SelectedIndexChanged += ddlBuildingName_SelectedIndexChanged;
            ddlCellStatus.SelectedIndexChanged += ddlCellStatus_SelectedIndexChanged;
            ddlCellFloor.SelectedIndexChanged += ddlCellFloor_SelectedIndexChanged;
            txtcellnumber.KeyUp += txtcellnumber_KeyUp;
            
            ddlBuildingName.KeyDown += Control_KeyDown;
            ddlCellFloor.KeyDown += Control_KeyDown;
            txtcellnumber.KeyDown += Control_KeyDown;
            txtCellDescription.KeyDown += Control_KeyDown;
            ddlCellType.KeyDown += Control_KeyDown;
            ddlCellStatus.KeyDown += Control_KeyDown;
            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }

        private void ddlCellFloor_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            txtcellnumber.Text = String.Empty;
            //GenerateCellCode(); //sementara ditutup, karena cell code tidak jadi kode cell
        }

        private void txtcellnumber_KeyUp(object sender, KeyEventArgs e)
        {
            GenerateCellCode();
        }

        private void GenerateCellCode()
        {
            if (this.Visible == true)
            {
                if (ddlCellFloor.SelectedItem == null)
                    return;

                if (!UserFunction.IsNumeric(txtcellnumber.Text))
                {
                    txtcellnumber.Text = string.Empty;
                }
                else
                    lblCellCode.Text = string.Format("{0}{1}", ddlBuildingName.SelectedItem.Text, txtcellnumber.Text);

            }
        }

        private void ddlBuildingName_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            ddlCellFloor.Items.Clear();

            var building = ddlBuildingName.SelectedValue;
            if (building != null)
            {
                BuildingService buildingserv = new BuildingService();
                var dataItem = buildingserv.GetById((int)building);

                for (var i = 1; i <= dataItem.JumlahLantai; i++)
                {
                    ddlCellFloor.Items.Add(i.ToString());
                }

                this.lblCellCode.Text = " * AUTO GENERATED";
                ddlCellFloor.SelectedIndex = -1;
                txtcellnumber.Text = String.Empty;
            }
        }

        private void ddlCellStatus_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            var status = ddlCellStatus.SelectedValue;
            if (status != null)
            {
                if (status.ToString() == "TERSEDIA")
                    cellstatuscolor.BackColor = Color.Green;
                else if (status.ToString() == "DITEMPATI")
                    cellstatuscolor.BackColor = Color.Red;
                else if (status.ToString() == "DALAM PEMELIHARAAN")
                    cellstatuscolor.BackColor = Color.Yellow;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                if (this.Tag == null)
                {
                    if (ValidasiCellNumber())
                    {
                        SaveCell();
                        UserFunction.ClearControls(tableLayoutPanel1);
                        UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Disimpan !");
                        UserFunction.LoadDataCellToGrid(MainForm.formMain.ListCell.GvCell);
                        MainForm.formMain.CellAllocation.DDLCellCode.Refresh();
                        MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
                        MainForm.formMain.AddNew.Text = "TAMBAH DATA SEL BARU";
                        this.lblTitle.Text = "     TAMBAH DATA SEL BARU";
                        this.lblCellCode.Text = " * AUTO GENERATED";
                        this.Tag = null;
                    }
                        
                }
                else
                {
                    if (ValidasiCellNumberEdit())
                    {
                        UpdatedData();
                        UserFunction.ClearControls(tableLayoutPanel1);
                        UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Disimpan !");
                        UserFunction.LoadDataCellToGrid(MainForm.formMain.ListCell.GvCell);
                        MainForm.formMain.CellAllocation.DDLCellCode.Refresh();
                        MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
                        MainForm.formMain.AddNew.Text = "TAMBAH DATA SEL BARU";
                        this.lblTitle.Text = "     TAMBAH DATA SEL BARU";
                        this.lblCellCode.Text = " * AUTO GENERATED";
                        this.Tag = null;
                    }
                }
            }
        }

        private bool ValidasiCellNumberEdit()
        {
            CellService cell = new CellService();
            var data = cell.GetByCellCode(lblCellCode.Text);
            var id = int.Parse(this.Tag.ToString());
            if (data != null && data.Id != id)
            {
                UserFunction.MsgBox(TipeMsg.Error, "Sel Sudah Ada !");
                lblError.Visible = true;
                lblError.Text = "Sel Sudah Ada !";
                txtcellnumber.Text = string.Empty;
                return false;
            }
            else
            {
                lblError.Visible = false;
                lblError.Text = string.Empty;
                return true;
            }
        }

        private bool ValidasiCellNumber()
        {
            CellService cell = new CellService();
            var data = cell.GetByCellCode(lblCellCode.Text);
            if (data != null)
            {
                UserFunction.MsgBox(TipeMsg.Error, "Sel Sudah Ada !");
                lblError.Visible = true;
                lblError.Text = "Sel Sudah Ada !";
                txtcellnumber.Text = string.Empty;
                return false;
            }
            else
            {
                lblError.Visible = false;
                lblError.Text = string.Empty;
                return true;
            }
        }

        private void SaveCell()
        {
            if (this.Tag == null)
                InsertData();
            else
                UpdatedData();
        }

        private void UpdatedData()
        {
            var cellCode = lblCellCode.Text;
            CellService cellserv = new CellService();
            var cell = new Cell
            {
                Id = Convert.ToInt32(this.Tag),
                CellNumber = txtcellnumber.Text.ToUpper(),
                CellCode = lblCellCode.Text.ToUpper(),
                BuildingId = (int)ddlBuildingName.SelectedItem.Value,
                CellFloor = ddlCellFloor.SelectedItem.Text,
                CellDescription = txtCellDescription.Text.ToUpper(),
                CellStatus = ddlCellStatus.SelectedItem.Text,
                CellType = (string)ddlCellType.SelectedItem.Value,
                UpdatedDate = DateTime.Now.ToLocalTime(),
                UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
            };
            cellserv.Update(cell);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERCELL.ToString(), Activities = "Update Master Cell, Old Data=" + UserFunction.JsonString(OldData) + ",  New Data=" + UserFunction.JsonString(cell) });
        }

        private void InsertData()
        {
            CellService cellserv = new CellService();
            var cell = new Cell
            {
                CellNumber = txtcellnumber.Text.ToUpper(),
                CellCode = lblCellCode.Text.ToUpper(),
                BuildingId = (int)ddlBuildingName.SelectedValue,
                CellFloor = ddlCellFloor.SelectedItem.Text,
                CellDescription = txtCellDescription.Text.ToUpper(),
                CellStatus = ddlCellStatus.SelectedItem.Text,
                CellType = (string)ddlCellType.SelectedValue,
                CreatedDate = DateTime.Now.ToLocalTime(),
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
            };
            cellserv.Post(cell);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERCELL.ToString(), Activities = "Add New Master Cell, Data=" + UserFunction.JsonString(cell) });
        }

        private void InitForm()
        {
            this.lblError.Visible = false;
            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     TAMBAH SEL BARU";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;
            this.lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 15.5f;

            this.lblRequired.LabelElement.CustomFont = Global.MainFont;
            this.lblRequired.LabelElement.CustomFontSize = 14f;
            this.lblRequired.ForeColor = Color.White;
            this.lblRequired.BackColor = Color.FromArgb(77, 77, 77);

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
            lblCellCode.ForeColor = Color.Navy;
            ddlBuildingName.SelectedIndex = 0;
            ddlCellFloor.SelectedIndex = 0;
            ddlCellStatus.SelectedIndex = 0;
            ddlCellType.SelectedIndex = 0;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ClearForm();
            this.Tag = null;
            MainForm.formMain.AddNew.Text = "TAMBAH DATA SEL BARU";
            this.lblTitle.Text = "     TAMBAH DATA SEL BARU";
            UserFunction.ClearControls(tableLayoutPanel1);
            MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
        }

        private bool DataValid()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            if ((string)this.txtcellnumber.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Nomor Sel!";
                return false;
            }
            if (this.lblCellCode.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Kode Sel!";
                return false;
            }
            if (this.ddlBuildingName.SelectedItem.Value == null)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Nama Gedung!";
                return false;
            }
            if ((string)this.ddlCellFloor.SelectedItem.Value == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Lantai Sel!";
                return false;
            }
            if (this.txtCellDescription.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Deskripsi Sel!";
                return false;
            }
            if ((string)this.ddlCellStatus.SelectedItem.Value == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Status Sel!";
                return false;
            }
            if ((string)this.ddlCellType.SelectedItem.Value == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tipe Sel!";
                return false;
            }
            return true;
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }
    }
}
