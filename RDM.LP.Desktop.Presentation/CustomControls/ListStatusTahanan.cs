﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListStatusTahanan : Base
    {
        public RadGridView GvTahanan { get { return this.gvTahanan; } }
        public ListStatusTahanan()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }
         
        private void InitEvents()
        {
            gvTahanan.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvTahanan.RowFormatting += radGridView_RowFormatting;
            gvTahanan.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvTahanan.CellClick += gvTahanan_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
            picRefresh.Click += PicRefresh_Click;
        }

        private void PicRefresh_Click(object sender, EventArgs e)
        {
            UserFunction.LoadDataStatusTahananToGrid(gvTahanan);
            gvTahanan.Refresh();
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarTahanan.pdf", "INMATES LIST DATA", gvTahanan);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarTahanan.csv", "INMATES LIST DATA", gvTahanan);
        }

        private void gvTahanan_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == null)
                return;

            switch (e.Column.Index)
            {
                case 0:
                    MainForm.formMain.StatusTahananDetail.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.StatusTahananDetail.Show();
                    break;
                case 1:
                    MainForm.formMain.StatusTahanan.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.AddNew;
                    MainForm.formMain.StatusTahanan.LoadData();
                    break;
                case 2:
                    var question  = UserFunction.Confirm("Hapus Status Tahanan ?");
                    if (question == DialogResult.Yes)
                    { 
                        DeleteTahanan(e.Row.Cells["Id"].Value.ToString());
                    }
                    break;
            }
            
        }

        private void DeleteTahanan(string Id)
        {
            var id = Int32.Parse(Id);
            StatusTahananService tahanan = new StatusTahananService();
            var data = tahanan.GetById(id);

            if (data != null)
            {                
                tahanan.DeleteById(id);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.STATUSTAHANAN.ToString(), Activities = "Delete StatusTahanan, data=" + UserFunction.JsonString(data) });
                
                UserFunction.MsgBox(TipeMsg.Info, "Data berhasil dihapus !");
                UserFunction.LoadDataStatusTahananToGrid(gvTahanan);
            }
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DAFTAR STATUS TAHANAN";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;


            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Width = 15;
            btnDetail.Name = "btnDetail";
            gvTahanan.AutoGenerateColumns = false;
            gvTahanan.Columns.Insert(0, btnDetail);
            gvTahanan.Refresh();
            gvTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            GridViewImageColumn btnUbah = new GridViewImageColumn();
            btnUbah.HeaderText = "";
            btnUbah.Name = "btnUbah";
            gvTahanan.AutoGenerateColumns = false;
            gvTahanan.Columns.Insert(1, btnUbah);
            gvTahanan.Refresh();
            gvTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnHapus = new GridViewImageColumn();
            btnHapus.HeaderText = "";
            btnHapus.Name = "btnHapus";
            gvTahanan.AutoGenerateColumns = false;
            gvTahanan.Columns.Insert(2, btnHapus);
            gvTahanan.Refresh();
            gvTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            UserFunction.LoadDataStatusTahananToGrid(gvTahanan);
            UserFunction.SetInitGridView(gvTahanan);

        }

    }
}
