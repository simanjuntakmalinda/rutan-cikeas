﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.DataAccess.Helper;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class NewSeizedAssets : Base
    {
        public TableLayoutPanel table1 { get { return this.tableLayoutPanel1; } }
        public RadDropDownList DDLCategory { get { return this.ddlCategory; } }

        private Point MouseDownLocation;

        public NewSeizedAssets()
        {
            InitializeComponent();
            InitForm();
            LoadData();
            InitEvents();    
        }

        public void LoadData()
        {
            this.lblTitle.Text = "     TAMBAH DATA BENDA SITAAN";
            UserFunction.LoadDDLCategory(DDLCategory);
        }

        private void InitEvents()
        {
            //Events
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            btnAddCategory.Click += BtnAddCategory_Click;
            ddlCategory.KeyDown += Control_KeyDown;
            btnSave.Click += btnSave_Click;
            btnCancel.Click += btnCancel_Click;
            exit.Click += Exit_Click;
            
            radPanel1.KeyDown += Control_KeyDown;
            radPanel2.KeyDown += Control_KeyDown;
            radPanel3.KeyDown += Control_KeyDown;
            radPanel4.KeyDown += Control_KeyDown;
            radPanel5.KeyDown += Control_KeyDown;
            radPanel6.KeyDown += Control_KeyDown;
            radPanel7.KeyDown += Control_KeyDown;
            btnSave.KeyDown += Control_KeyDown;
            btnCancel.KeyDown += Control_KeyDown;
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            ClearForm();
            this.Hide();
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }

        private void BtnAddCategory_Click(object sender, EventArgs e)
        {
            formCategory.Show();
            formCategory.Location = new Point(ClientSize.Width / 2 - formCategory.Size.Width / 2,
              ClientSize.Height / 2 - formCategory.Size.Height / 2);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                RadGridView gvAssets = MainForm.formMain.SeizedAssets.GVSeizedAssets;
                gvAssets.DataSource = null;

                DataTable dtAssets = MainForm.formMain.SeizedAssets.dtAssets;
                DataRow dr = MainForm.formMain.SeizedAssets.dtAssets.NewRow();

                dr["Id"] = "0";
                dr["CategoryId"] = ddlCategory.SelectedItem.Value;
                dr["CategoryName"] = ddlCategory.SelectedItem;
                dr["Detail"] = txtDetail.Text;
                dr["Quantity"] = txtQuantity.Text;
                if (rbRupbasan.IsChecked == true)
                    dr["StorageType"] = "RUANG BARANG BUKTI";
                else
                    dr["StorageType"] = "LAINNYA";
                dr["StorageLocation"] = txtStorageLoc.Text;
                dr["ConfiscatedDate"] = dpConfiscatedDate.Value;
                dr["Note"] = txtNote.Text;
                dtAssets.Rows.Add(dr);

                MainForm.formMain.SeizedAssets.LoadDataToGridview(gvAssets, dtAssets);

                ClearForm();
                this.Hide();
                this.Tag = null;
            }
        }

        private bool DataValid()
        {
            if (ddlCategory.SelectedItem.Value == null)
            {
                lblError.Show();
                lblError.Text = "Mohon Pilih Kategori";
                return false;
            }
            else if (txtDetail.Text == string.Empty)
            {
                lblError.Show();
                lblError.Text = "Mohon Isi Detail";
                return false;
            }
            else if (txtQuantity.Text == string.Empty)
            {
                lblError.Show();
                lblError.Text = "Mohon Isi Satuan";
                return false;
            }
            else if (rbRupbasan.IsChecked == false && rbLainnya.IsChecked == false)
            {
                lblError.Show();
                lblError.Text = "Mohon Pilih Jenis Penyimpanan";
                return false;
            }
            else if (txtStorageLoc.Text == string.Empty)
            {
                lblError.Show();
                lblError.Text = "Mohon Isi Lokasi Penyimpanan";
                return false;
            }
            else if (dpConfiscatedDate.Value == null)
            {
                lblError.Show();
                lblError.Text = "Mohon Isi Tanggal Sita";
                return false;
            }
            else if (txtNote.Text == string.Empty)
            {
                lblError.Show();
                lblError.Text = "Mohon Isi Catatan";
                return false;
            }
            return true;
        }

        private void InitForm()
        {
            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;
            this.lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 14f;
            this.lblDetailSurat.ForeColor = Color.White;
            this.lblDetailSurat.BackColor = Color.FromArgb(77,77,77);

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnCancel.ButtonElement.CustomFont = Global.MainFont;
            this.btnCancel.ButtonElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
            ddlCategory.SelectedIndex = 0;

            GridViewImageColumn btnDelete = new GridViewImageColumn();
            btnDelete.HeaderText = "";
            btnDelete.Name = "btnAddAssets";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            ClearForm();
        }

        public void ClearForm()
        {
            ddlCategory.SelectedItem = null;
            ddlCategory.SelectedIndex = 0;
            txtDetail.Text = null;
            txtQuantity.Text = null;
            rbRupbasan.IsChecked = false;
            rbLainnya.IsChecked = false;
            txtStorageLoc.Text = null;
            txtNote.Text = null;
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }
    }
}
