﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class NewSeizedAssets
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewSeizedAssets));
            this.lblDetailSurat = new Telerik.WinControls.UI.RadLabel();
            this.headerPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnCancel = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            this.formCategory = new RDM.LP.Desktop.Presentation.CustomControls.FormSeizedAssetsCategory();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.txtDetail = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.txtQuantity = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.ddlCategory = new Telerik.WinControls.UI.RadDropDownList();
            this.lblDetailVisitor = new Telerik.WinControls.UI.RadLabel();
            this.lblCategory = new Telerik.WinControls.UI.RadLabel();
            this.lblQuantity = new Telerik.WinControls.UI.RadLabel();
            this.lblDetail = new Telerik.WinControls.UI.RadLabel();
            this.lblStorageLoc = new Telerik.WinControls.UI.RadLabel();
            this.btnAddCategory = new Telerik.WinControls.UI.RadButton();
            this.radPanel6 = new Telerik.WinControls.UI.RadPanel();
            this.rbRupbasan = new Telerik.WinControls.UI.RadRadioButton();
            this.rbLainnya = new Telerik.WinControls.UI.RadRadioButton();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.txtNote = new Telerik.WinControls.UI.RadTextBox();
            this.lblNote = new Telerik.WinControls.UI.RadLabel();
            this.radPanel7 = new Telerik.WinControls.UI.RadPanel();
            this.dpConfiscatedDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblDate = new Telerik.WinControls.UI.RadLabel();
            this.radPanel5 = new Telerik.WinControls.UI.RadPanel();
            this.txtStorageLoc = new Telerik.WinControls.UI.RadTextBox();
            this.radPanelError = new Telerik.WinControls.UI.RadPanel();
            this.lblError = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).BeginInit();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStorageLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).BeginInit();
            this.radPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbRupbasan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbLainnya)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            this.radPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).BeginInit();
            this.radPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dpConfiscatedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).BeginInit();
            this.radPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtStorageLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).BeginInit();
            this.radPanelError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDetailSurat
            // 
            this.lblDetailSurat.AutoSize = false;
            this.lblDetailSurat.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailSurat.Location = new System.Drawing.Point(0, 56);
            this.lblDetailSurat.Name = "lblDetailSurat";
            this.lblDetailSurat.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetailSurat.Size = new System.Drawing.Size(791, 52);
            this.lblDetailSurat.TabIndex = 67;
            this.lblDetailSurat.Text = "Mohon Isi seluruh Kolom yang Diperlukan";
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.lblTitle);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Margin = new System.Windows.Forms.Padding(4);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(791, 56);
            this.headerPanel.TabIndex = 66;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lblTitle.Size = new System.Drawing.Size(791, 56);
            this.lblTitle.TabIndex = 81;
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(732, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(49, 56);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 4;
            this.exit.TabStop = false;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnCancel);
            this.lblButton.Controls.Add(this.btnSave);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 557);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(791, 106);
            this.lblButton.TabIndex = 71;
            // 
            // btnCancel
            // 
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(30, 30);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(185, 46);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Tag = "";
            this.btnCancel.Text = "&BATAL";
            this.btnCancel.ThemeName = "MaterialBlueGrey";
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.if_checked_2_309094;
            this.btnSave.Location = new System.Drawing.Point(570, 30);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(191, 46);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "&TAMBAH";
            this.btnSave.ThemeName = "MaterialBlueGrey";
            // 
            // formCategory
            // 
            this.formCategory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.formCategory.Location = new System.Drawing.Point(1004, 175);
            this.formCategory.Name = "formCategory";
            this.formCategory.Size = new System.Drawing.Size(791, 384);
            this.formCategory.TabIndex = 84;
            this.formCategory.Visible = false;
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 108);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(772, 403);
            this.radScrollablePanel1.Size = new System.Drawing.Size(791, 405);
            this.radScrollablePanel1.TabIndex = 86;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailVisitor, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblCategory, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblQuantity, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblDetail, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblStorageLoc, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnAddCategory, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel6, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.radPanel4, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblNote, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.radPanel7, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblDate, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.radPanel5, 1, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(772, 650);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // radPanel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel3, 2);
            this.radPanel3.Controls.Add(this.txtDetail);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Location = new System.Drawing.Point(263, 163);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(496, 94);
            this.radPanel3.TabIndex = 3;
            // 
            // txtDetail
            // 
            this.txtDetail.AutoSize = false;
            this.txtDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDetail.Location = new System.Drawing.Point(0, 0);
            this.txtDetail.MaxLength = 150;
            this.txtDetail.Multiline = true;
            this.txtDetail.Name = "txtDetail";
            this.txtDetail.Size = new System.Drawing.Size(496, 94);
            this.txtDetail.TabIndex = 196;
            // 
            // radPanel2
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel2, 2);
            this.radPanel2.Controls.Add(this.txtQuantity);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(263, 113);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(496, 44);
            this.radPanel2.TabIndex = 2;
            // 
            // txtQuantity
            // 
            this.txtQuantity.AutoSize = false;
            this.txtQuantity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtQuantity.Location = new System.Drawing.Point(0, 0);
            this.txtQuantity.MaxLength = 150;
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(496, 44);
            this.txtQuantity.TabIndex = 194;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.ddlCategory);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(263, 63);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(450, 44);
            this.radPanel1.TabIndex = 1;
            // 
            // ddlCategory
            // 
            this.ddlCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlCategory.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlCategory.Location = new System.Drawing.Point(0, 0);
            this.ddlCategory.Name = "ddlCategory";
            this.ddlCategory.Size = new System.Drawing.Size(450, 24);
            this.ddlCategory.TabIndex = 136;
            // 
            // lblDetailVisitor
            // 
            this.lblDetailVisitor.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailVisitor, 3);
            this.lblDetailVisitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailVisitor.Location = new System.Drawing.Point(13, 13);
            this.lblDetailVisitor.Name = "lblDetailVisitor";
            this.lblDetailVisitor.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDetailVisitor.Size = new System.Drawing.Size(746, 44);
            this.lblDetailVisitor.TabIndex = 134;
            this.lblDetailVisitor.Text = "DETAIL BENDA SITAAN BARU";
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = false;
            this.lblCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCategory.Location = new System.Drawing.Point(14, 64);
            this.lblCategory.Margin = new System.Windows.Forms.Padding(4);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(242, 42);
            this.lblCategory.TabIndex = 135;
            this.lblCategory.Text = "<html>Kategori<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = false;
            this.lblQuantity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblQuantity.Location = new System.Drawing.Point(14, 114);
            this.lblQuantity.Margin = new System.Windows.Forms.Padding(4);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(242, 42);
            this.lblQuantity.TabIndex = 193;
            this.lblQuantity.Text = "<html>Jumlah<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblDetail
            // 
            this.lblDetail.AutoSize = false;
            this.lblDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetail.Location = new System.Drawing.Point(14, 164);
            this.lblDetail.Margin = new System.Windows.Forms.Padding(4);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Size = new System.Drawing.Size(242, 92);
            this.lblDetail.TabIndex = 195;
            this.lblDetail.Text = "<html>Detail<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblStorageLoc
            // 
            this.lblStorageLoc.AutoSize = false;
            this.lblStorageLoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStorageLoc.Location = new System.Drawing.Point(14, 264);
            this.lblStorageLoc.Margin = new System.Windows.Forms.Padding(4);
            this.lblStorageLoc.Name = "lblStorageLoc";
            this.lblStorageLoc.Size = new System.Drawing.Size(242, 92);
            this.lblStorageLoc.TabIndex = 197;
            this.lblStorageLoc.Text = "<html>Lokasi Penyimpanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // btnAddCategory
            // 
            this.btnAddCategory.AutoSize = true;
            this.btnAddCategory.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.add_doc;
            this.btnAddCategory.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAddCategory.Location = new System.Drawing.Point(719, 63);
            this.btnAddCategory.Name = "btnAddCategory";
            this.btnAddCategory.Size = new System.Drawing.Size(27, 27);
            this.btnAddCategory.TabIndex = 190;
            // 
            // radPanel6
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel6, 2);
            this.radPanel6.Controls.Add(this.rbRupbasan);
            this.radPanel6.Controls.Add(this.rbLainnya);
            this.radPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel6.Location = new System.Drawing.Point(263, 263);
            this.radPanel6.Name = "radPanel6";
            this.radPanel6.Size = new System.Drawing.Size(496, 94);
            this.radPanel6.TabIndex = 4;
            // 
            // rbRupbasan
            // 
            this.rbRupbasan.Location = new System.Drawing.Point(35, 12);
            this.rbRupbasan.Name = "rbRupbasan";
            this.rbRupbasan.Size = new System.Drawing.Size(138, 18);
            this.rbRupbasan.TabIndex = 199;
            this.rbRupbasan.Text = "RUANG BARANG BUKTI";
            // 
            // rbLainnya
            // 
            this.rbLainnya.Location = new System.Drawing.Point(35, 60);
            this.rbLainnya.Name = "rbLainnya";
            this.rbLainnya.Size = new System.Drawing.Size(66, 18);
            this.rbLainnya.TabIndex = 200;
            this.rbLainnya.Text = "LAINNYA";
            // 
            // radPanel4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel4, 2);
            this.radPanel4.Controls.Add(this.txtNote);
            this.radPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel4.Location = new System.Drawing.Point(263, 513);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(496, 94);
            this.radPanel4.TabIndex = 7;
            // 
            // txtNote
            // 
            this.txtNote.AutoSize = false;
            this.txtNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNote.Location = new System.Drawing.Point(0, 0);
            this.txtNote.MaxLength = 150;
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(496, 94);
            this.txtNote.TabIndex = 198;
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = false;
            this.lblNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNote.Location = new System.Drawing.Point(14, 514);
            this.lblNote.Margin = new System.Windows.Forms.Padding(4);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(242, 92);
            this.lblNote.TabIndex = 191;
            this.lblNote.Text = "<html>Catatan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel7
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel7, 2);
            this.radPanel7.Controls.Add(this.dpConfiscatedDate);
            this.radPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel7.Location = new System.Drawing.Point(263, 463);
            this.radPanel7.Name = "radPanel7";
            this.radPanel7.Size = new System.Drawing.Size(496, 44);
            this.radPanel7.TabIndex = 6;
            // 
            // dpConfiscatedDate
            // 
            this.dpConfiscatedDate.Culture = new System.Globalization.CultureInfo("id-ID");
            this.dpConfiscatedDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dpConfiscatedDate.Location = new System.Drawing.Point(0, 0);
            this.dpConfiscatedDate.Name = "dpConfiscatedDate";
            this.dpConfiscatedDate.Size = new System.Drawing.Size(496, 24);
            this.dpConfiscatedDate.TabIndex = 201;
            this.dpConfiscatedDate.TabStop = false;
            this.dpConfiscatedDate.Text = "Kamis, 20 Desember 2018";
            this.dpConfiscatedDate.Value = new System.DateTime(2018, 12, 20, 13, 52, 31, 584);
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = false;
            this.lblDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDate.Location = new System.Drawing.Point(14, 464);
            this.lblDate.Margin = new System.Windows.Forms.Padding(4);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(242, 42);
            this.lblDate.TabIndex = 198;
            this.lblDate.Text = "<html>Tanggal Sita<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel5
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel5, 2);
            this.radPanel5.Controls.Add(this.txtStorageLoc);
            this.radPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel5.Location = new System.Drawing.Point(263, 363);
            this.radPanel5.Name = "radPanel5";
            this.radPanel5.Size = new System.Drawing.Size(496, 94);
            this.radPanel5.TabIndex = 5;
            // 
            // txtStorageLoc
            // 
            this.txtStorageLoc.AutoSize = false;
            this.txtStorageLoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtStorageLoc.Location = new System.Drawing.Point(0, 0);
            this.txtStorageLoc.MaxLength = 150;
            this.txtStorageLoc.Multiline = true;
            this.txtStorageLoc.Name = "txtStorageLoc";
            this.txtStorageLoc.Size = new System.Drawing.Size(496, 94);
            this.txtStorageLoc.TabIndex = 197;
            // 
            // radPanelError
            // 
            this.radPanelError.Controls.Add(this.lblError);
            this.radPanelError.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanelError.Location = new System.Drawing.Point(0, 514);
            this.radPanelError.Margin = new System.Windows.Forms.Padding(0);
            this.radPanelError.Name = "radPanelError";
            this.radPanelError.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radPanelError.Size = new System.Drawing.Size(791, 43);
            this.radPanelError.TabIndex = 87;
            // 
            // lblError
            // 
            this.lblError.AutoSize = false;
            this.lblError.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.warning;
            this.lblError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblError.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblError.Location = new System.Drawing.Point(15, 4);
            this.lblError.Name = "lblError";
            this.lblError.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.lblError.Size = new System.Drawing.Size(776, 39);
            this.lblError.TabIndex = 24;
            this.lblError.Visible = false;
            // 
            // NewSeizedAssets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.formCategory);
            this.Controls.Add(this.radPanelError);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.lblDetailSurat);
            this.Controls.Add(this.headerPanel);
            this.Name = "NewSeizedAssets";
            this.Size = new System.Drawing.Size(791, 663);
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).EndInit();
            this.headerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStorageLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).EndInit();
            this.radPanel6.ResumeLayout(false);
            this.radPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbRupbasan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbLainnya)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            this.radPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).EndInit();
            this.radPanel7.ResumeLayout(false);
            this.radPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dpConfiscatedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).EndInit();
            this.radPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtStorageLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).EndInit();
            this.radPanelError.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private FormSeizedAssetsCategory formCategory;
        private Telerik.WinControls.UI.RadLabel lblDetailSurat;
        private Telerik.WinControls.UI.RadPanel headerPanel;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnCancel;
        private Telerik.WinControls.UI.RadButton btnSave;
        private System.Windows.Forms.PictureBox exit;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel lblDetailVisitor;
        private Telerik.WinControls.UI.RadLabel lblCategory;
        private Telerik.WinControls.UI.RadDropDownList ddlCategory;
        private Telerik.WinControls.UI.RadButton btnAddCategory;
        private Telerik.WinControls.UI.RadLabel lblQuantity;
        private Telerik.WinControls.UI.RadTextBox txtQuantity;
        private Telerik.WinControls.UI.RadLabel lblDetail;
        private Telerik.WinControls.UI.RadTextBox txtDetail;
        private Telerik.WinControls.UI.RadLabel lblStorageLoc;
        private Telerik.WinControls.UI.RadRadioButton rbRupbasan;
        private Telerik.WinControls.UI.RadRadioButton rbLainnya;
        private Telerik.WinControls.UI.RadTextBox txtStorageLoc;
        private Telerik.WinControls.UI.RadLabel lblNote;
        private Telerik.WinControls.UI.RadLabel lblDate;
        private Telerik.WinControls.UI.RadTextBox txtNote;
        private Telerik.WinControls.UI.RadPanel radPanelError;
        private Telerik.WinControls.UI.RadLabel lblError;
        private Telerik.WinControls.UI.RadDateTimePicker dpConfiscatedDate;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel6;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadPanel radPanel5;
        private Telerik.WinControls.UI.RadPanel radPanel7;
    }
}
