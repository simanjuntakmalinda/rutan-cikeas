﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class FormTahanan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTahanan));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.headerPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailSurat = new Telerik.WinControls.UI.RadLabel();
            this.radPanelError = new Telerik.WinControls.UI.RadPanel();
            this.lblError = new Telerik.WinControls.UI.RadLabel();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnBack = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtSuratPenahanan = new Telerik.WinControls.UI.RadTextBox();
            this.txtSuratPenangkapan = new Telerik.WinControls.UI.RadTextBox();
            this.dtTanggalPenahanan = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel27 = new Telerik.WinControls.UI.RadPanel();
            this.dtTanggalPenangkapan = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radPanel26 = new Telerik.WinControls.UI.RadPanel();
            this.dtTanggalKejadian = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radPanel25 = new Telerik.WinControls.UI.RadPanel();
            this.txtPeranTahanan = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel24 = new Telerik.WinControls.UI.RadPanel();
            this.txtTB = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel22 = new Telerik.WinControls.UI.RadPanel();
            this.txtLokasiPenangkapan = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel19 = new Telerik.WinControls.UI.RadPanel();
            this.txtSourceLocation = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel18 = new Telerik.WinControls.UI.RadPanel();
            this.txtTelp = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel16 = new Telerik.WinControls.UI.RadPanel();
            this.txtCity = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel17 = new Telerik.WinControls.UI.RadPanel();
            this.txtAddress = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel15 = new Telerik.WinControls.UI.RadPanel();
            this.txtNoIdentitas = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel11 = new Telerik.WinControls.UI.RadPanel();
            this.ddlNationality = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel10 = new Telerik.WinControls.UI.RadPanel();
            this.ddlVocation = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel9 = new Telerik.WinControls.UI.RadPanel();
            this.ddlMaritalStatus = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel6 = new Telerik.WinControls.UI.RadPanel();
            this.ddlType = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel5 = new Telerik.WinControls.UI.RadPanel();
            this.ddlCategory = new Telerik.WinControls.UI.RadDropDownList();
            this.lblNetwork = new Telerik.WinControls.UI.RadLabel();
            this.lblPDOB = new Telerik.WinControls.UI.RadLabel();
            this.lblMaritalStatus = new Telerik.WinControls.UI.RadLabel();
            this.lblVocation = new Telerik.WinControls.UI.RadLabel();
            this.lblNationality = new Telerik.WinControls.UI.RadLabel();
            this.lblFullName = new Telerik.WinControls.UI.RadLabel();
            this.lblIDType = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailVisitor = new Telerik.WinControls.UI.RadLabel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.picCanvas = new System.Windows.Forms.PictureBox();
            this.lblInmatePhoto = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.btnAddCases = new Telerik.WinControls.UI.RadButton();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.ddlNetwork = new Telerik.WinControls.UI.RadCheckedDropDownList();
            this.lblInmateCategory = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailVisit = new Telerik.WinControls.UI.RadLabel();
            this.tahananId = new System.Windows.Forms.Label();
            this.lblPhone = new Telerik.WinControls.UI.RadLabel();
            this.lblReligion = new Telerik.WinControls.UI.RadLabel();
            this.lblGender = new Telerik.WinControls.UI.RadLabel();
            this.lblCity = new Telerik.WinControls.UI.RadLabel();
            this.lblAddress = new Telerik.WinControls.UI.RadLabel();
            this.lblLocation = new Telerik.WinControls.UI.RadLabel();
            this.radPanel7 = new Telerik.WinControls.UI.RadPanel();
            this.ddlAgama = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel8 = new Telerik.WinControls.UI.RadPanel();
            this.ddlJnsIdentitas = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.txtNama = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel13 = new Telerik.WinControls.UI.RadPanel();
            this.txtPlaceofBirth = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel14 = new Telerik.WinControls.UI.RadPanel();
            this.txtNotes = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel20 = new Telerik.WinControls.UI.RadPanel();
            this.dpDateofBirth = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radPanel12 = new Telerik.WinControls.UI.RadPanel();
            this.ddlGender = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel21 = new Telerik.WinControls.UI.RadPanel();
            this.ddlCases = new Telerik.WinControls.UI.RadCheckedDropDownList();
            this.radPanel23 = new Telerik.WinControls.UI.RadPanel();
            this.txtBB = new Telerik.WinControls.UI.RadTextBox();
            this.btnAddNetwork = new Telerik.WinControls.UI.RadButton();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnTakePhoto = new System.Windows.Forms.Button();
            this.tblNavigation = new System.Windows.Forms.TableLayoutPanel();
            this.labelRight = new Telerik.WinControls.UI.RadLabel();
            this.lblFront = new Telerik.WinControls.UI.RadLabel();
            this.labelLeft = new Telerik.WinControls.UI.RadLabel();
            this.lblVisitorFinger = new Telerik.WinControls.UI.RadLabel();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.picFinger = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnUbahSidikJari = new System.Windows.Forms.Button();
            this.formCases = new RDM.LP.Desktop.Presentation.CustomControls.FormCases();
            this.formNetwork = new RDM.LP.Desktop.Presentation.CustomControls.FormNetwork();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).BeginInit();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).BeginInit();
            this.radPanelError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuratPenahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuratPenangkapan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalPenahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel27)).BeginInit();
            this.radPanel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalPenangkapan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel26)).BeginInit();
            this.radPanel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalKejadian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel25)).BeginInit();
            this.radPanel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeranTahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel24)).BeginInit();
            this.radPanel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel22)).BeginInit();
            this.radPanel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLokasiPenangkapan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel19)).BeginInit();
            this.radPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSourceLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel18)).BeginInit();
            this.radPanel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel16)).BeginInit();
            this.radPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel17)).BeginInit();
            this.radPanel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel15)).BeginInit();
            this.radPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoIdentitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel11)).BeginInit();
            this.radPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlNationality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel10)).BeginInit();
            this.radPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlVocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel9)).BeginInit();
            this.radPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlMaritalStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).BeginInit();
            this.radPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).BeginInit();
            this.radPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNetwork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPDOB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMaritalStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNationality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFullName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIDType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInmatePhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddCases)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlNetwork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInmateCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisit)).BeginInit();
            this.lblDetailVisit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReligion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).BeginInit();
            this.radPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlAgama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).BeginInit();
            this.radPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlJnsIdentitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            this.radPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel13)).BeginInit();
            this.radPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlaceofBirth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel14)).BeginInit();
            this.radPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel20)).BeginInit();
            this.radPanel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dpDateofBirth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel12)).BeginInit();
            this.radPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlGender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel21)).BeginInit();
            this.radPanel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlCases)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel23)).BeginInit();
            this.radPanel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddNetwork)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.tblNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFront)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVisitorFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFinger)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.lblTitle);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Margin = new System.Windows.Forms.Padding(4);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1516, 56);
            this.headerPanel.TabIndex = 71;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(1516, 56);
            this.lblTitle.TabIndex = 711;
            // 
            // lblDetailSurat
            // 
            this.lblDetailSurat.AutoSize = false;
            this.lblDetailSurat.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailSurat.Location = new System.Drawing.Point(0, 56);
            this.lblDetailSurat.Name = "lblDetailSurat";
            this.lblDetailSurat.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetailSurat.Size = new System.Drawing.Size(1516, 52);
            this.lblDetailSurat.TabIndex = 751;
            this.lblDetailSurat.Text = "Mohon Isi seluruh Kolom yang Diperlukan";
            // 
            // radPanelError
            // 
            this.radPanelError.Controls.Add(this.lblError);
            this.radPanelError.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanelError.Location = new System.Drawing.Point(0, 594);
            this.radPanelError.Margin = new System.Windows.Forms.Padding(0);
            this.radPanelError.Name = "radPanelError";
            this.radPanelError.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radPanelError.Size = new System.Drawing.Size(1516, 43);
            this.radPanelError.TabIndex = 80;
            // 
            // lblError
            // 
            this.lblError.AutoSize = false;
            this.lblError.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.warning;
            this.lblError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblError.Location = new System.Drawing.Point(15, 0);
            this.lblError.Name = "lblError";
            this.lblError.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.lblError.Size = new System.Drawing.Size(1501, 39);
            this.lblError.TabIndex = 24;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnBack);
            this.lblButton.Controls.Add(this.btnSave);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblButton.Location = new System.Drawing.Point(0, 637);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(1516, 118);
            this.lblButton.TabIndex = 81;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.btnBack_Image;
            this.btnBack.Location = new System.Drawing.Point(30, 30);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(244, 58);
            this.btnBack.TabIndex = 28;
            this.btnBack.Tag = "";
            this.btnBack.Text = "&BATAL";
            this.btnBack.ThemeName = "MaterialBlueGrey";
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.if_checked_2_309094;
            this.btnSave.Location = new System.Drawing.Point(1181, 30);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(305, 58);
            this.btnSave.TabIndex = 27;
            this.btnSave.Text = "&SIMPAN";
            this.btnSave.ThemeName = "MaterialBlueGrey";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("radScrollablePanel1.BackgroundImage")));
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 108);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(1497, 484);
            this.radScrollablePanel1.Size = new System.Drawing.Size(1516, 486);
            this.radScrollablePanel1.TabIndex = 78;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 152F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 246F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 165F));
            this.tableLayoutPanel1.Controls.Add(this.txtSuratPenahanan, 0, 22);
            this.tableLayoutPanel1.Controls.Add(this.txtSuratPenangkapan, 1, 20);
            this.tableLayoutPanel1.Controls.Add(this.dtTanggalPenahanan, 1, 23);
            this.tableLayoutPanel1.Controls.Add(this.radLabel14, 0, 23);
            this.tableLayoutPanel1.Controls.Add(this.radLabel13, 0, 22);
            this.tableLayoutPanel1.Controls.Add(this.radLabel12, 0, 20);
            this.tableLayoutPanel1.Controls.Add(this.radPanel27, 1, 21);
            this.tableLayoutPanel1.Controls.Add(this.radPanel26, 1, 19);
            this.tableLayoutPanel1.Controls.Add(this.radPanel25, 1, 18);
            this.tableLayoutPanel1.Controls.Add(this.radPanel24, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.radLabel11, 4, 8);
            this.tableLayoutPanel1.Controls.Add(this.radLabel10, 4, 7);
            this.tableLayoutPanel1.Controls.Add(this.radLabel9, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.radLabel8, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.radLabel5, 0, 21);
            this.tableLayoutPanel1.Controls.Add(this.radLabel4, 0, 19);
            this.tableLayoutPanel1.Controls.Add(this.radPanel22, 1, 15);
            this.tableLayoutPanel1.Controls.Add(this.radLabel2, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 18);
            this.tableLayoutPanel1.Controls.Add(this.radPanel19, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.radPanel18, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.radPanel16, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.radPanel17, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.radPanel15, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel11, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.radPanel10, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.radPanel9, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.radPanel6, 3, 13);
            this.tableLayoutPanel1.Controls.Add(this.radPanel5, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.lblNetwork, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.lblPDOB, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblMaritalStatus, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblVocation, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblNationality, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.lblFullName, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblIDType, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailVisitor, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblInmatePhoto, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.radLabel3, 0, 17);
            this.tableLayoutPanel1.Controls.Add(this.btnAddCases, 3, 17);
            this.tableLayoutPanel1.Controls.Add(this.radLabel7, 0, 24);
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 1, 16);
            this.tableLayoutPanel1.Controls.Add(this.lblInmateCategory, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.radLabel6, 2, 13);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailVisit, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.lblPhone, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.lblReligion, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblGender, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblCity, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblAddress, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblLocation, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.radPanel7, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.radPanel8, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel4, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.radPanel13, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel14, 1, 24);
            this.tableLayoutPanel1.Controls.Add(this.radPanel20, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel12, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.radPanel21, 1, 17);
            this.tableLayoutPanel1.Controls.Add(this.radPanel23, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.btnAddNetwork, 3, 16);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 5, 6);
            this.tableLayoutPanel1.Controls.Add(this.tblNavigation, 6, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblVisitorFinger, 5, 8);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 5, 9);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 5, 16);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 26;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1497, 1465);
            this.tableLayoutPanel1.TabIndex = 161;
            // 
            // txtSuratPenahanan
            // 
            this.txtSuratPenahanan.AutoSize = false;
            this.txtSuratPenahanan.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.SetColumnSpan(this.txtSuratPenahanan, 2);
            this.txtSuratPenahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSuratPenahanan.Location = new System.Drawing.Point(265, 1210);
            this.txtSuratPenahanan.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtSuratPenahanan.Name = "txtSuratPenahanan";
            this.txtSuratPenahanan.Size = new System.Drawing.Size(556, 50);
            this.txtSuratPenahanan.TabIndex = 338;
            // 
            // txtSuratPenangkapan
            // 
            this.txtSuratPenangkapan.AutoSize = false;
            this.txtSuratPenangkapan.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.SetColumnSpan(this.txtSuratPenangkapan, 2);
            this.txtSuratPenangkapan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSuratPenangkapan.Location = new System.Drawing.Point(265, 1110);
            this.txtSuratPenangkapan.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtSuratPenangkapan.Name = "txtSuratPenangkapan";
            this.txtSuratPenangkapan.Size = new System.Drawing.Size(556, 50);
            this.txtSuratPenangkapan.TabIndex = 337;
            // 
            // dtTanggalPenahanan
            // 
            this.dtTanggalPenahanan.AutoSize = false;
            this.dtTanggalPenahanan.BackColor = System.Drawing.Color.Transparent;
            this.dtTanggalPenahanan.CalendarSize = new System.Drawing.Size(290, 320);
            this.tableLayoutPanel1.SetColumnSpan(this.dtTanggalPenahanan, 2);
            this.dtTanggalPenahanan.Culture = new System.Globalization.CultureInfo("id-ID");
            this.dtTanggalPenahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtTanggalPenahanan.Location = new System.Drawing.Point(263, 1263);
            this.dtTanggalPenahanan.Name = "dtTanggalPenahanan";
            this.dtTanggalPenahanan.Size = new System.Drawing.Size(560, 44);
            this.dtTanggalPenahanan.TabIndex = 336;
            this.dtTanggalPenahanan.TabStop = false;
            this.dtTanggalPenahanan.Text = "Jumat, 21 Desember 2018";
            this.dtTanggalPenahanan.Value = new System.DateTime(2018, 12, 21, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.dtTanggalPenahanan.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dtTanggalPenahanan.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dtTanggalPenahanan.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dtTanggalPenahanan.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dtTanggalPenahanan.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dtTanggalPenahanan.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dtTanggalPenahanan.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dtTanggalPenahanan.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "Jumat, 21 Desember 2018";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dtTanggalPenahanan.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // radLabel14
            // 
            this.radLabel14.AutoSize = false;
            this.radLabel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel14.ForeColor = System.Drawing.Color.Black;
            this.radLabel14.Location = new System.Drawing.Point(14, 1264);
            this.radLabel14.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(242, 42);
            this.radLabel14.TabIndex = 335;
            this.radLabel14.Text = "<html>Tanggal Penahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel13
            // 
            this.radLabel13.AutoSize = false;
            this.radLabel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel13.ForeColor = System.Drawing.Color.Black;
            this.radLabel13.Location = new System.Drawing.Point(14, 1214);
            this.radLabel13.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(242, 42);
            this.radLabel13.TabIndex = 334;
            this.radLabel13.Text = "<html>Surat Penahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel12
            // 
            this.radLabel12.AutoSize = false;
            this.radLabel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel12.ForeColor = System.Drawing.Color.Black;
            this.radLabel12.Location = new System.Drawing.Point(14, 1114);
            this.radLabel12.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(242, 42);
            this.radLabel12.TabIndex = 333;
            this.radLabel12.Text = "<html>Surat Penangkapan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel27
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel27, 2);
            this.radPanel27.Controls.Add(this.dtTanggalPenangkapan);
            this.radPanel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel27.Location = new System.Drawing.Point(263, 1163);
            this.radPanel27.Name = "radPanel27";
            this.radPanel27.Size = new System.Drawing.Size(560, 44);
            this.radPanel27.TabIndex = 24;
            // 
            // dtTanggalPenangkapan
            // 
            this.dtTanggalPenangkapan.AutoSize = false;
            this.dtTanggalPenangkapan.BackColor = System.Drawing.Color.Transparent;
            this.dtTanggalPenangkapan.CalendarSize = new System.Drawing.Size(290, 320);
            this.dtTanggalPenangkapan.Culture = new System.Globalization.CultureInfo("id-ID");
            this.dtTanggalPenangkapan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtTanggalPenangkapan.Location = new System.Drawing.Point(0, 0);
            this.dtTanggalPenangkapan.Name = "dtTanggalPenangkapan";
            this.dtTanggalPenangkapan.Size = new System.Drawing.Size(560, 44);
            this.dtTanggalPenangkapan.TabIndex = 24;
            this.dtTanggalPenangkapan.TabStop = false;
            this.dtTanggalPenangkapan.Text = "Jumat, 21 Desember 2018";
            this.dtTanggalPenangkapan.Value = new System.DateTime(2018, 12, 21, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.dtTanggalPenangkapan.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dtTanggalPenangkapan.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dtTanggalPenangkapan.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dtTanggalPenangkapan.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dtTanggalPenangkapan.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dtTanggalPenangkapan.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dtTanggalPenangkapan.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dtTanggalPenangkapan.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "Jumat, 21 Desember 2018";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dtTanggalPenangkapan.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // radPanel26
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel26, 2);
            this.radPanel26.Controls.Add(this.dtTanggalKejadian);
            this.radPanel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel26.Location = new System.Drawing.Point(263, 1063);
            this.radPanel26.Name = "radPanel26";
            this.radPanel26.Size = new System.Drawing.Size(560, 44);
            this.radPanel26.TabIndex = 23;
            // 
            // dtTanggalKejadian
            // 
            this.dtTanggalKejadian.AutoSize = false;
            this.dtTanggalKejadian.BackColor = System.Drawing.Color.Transparent;
            this.dtTanggalKejadian.CalendarSize = new System.Drawing.Size(290, 320);
            this.dtTanggalKejadian.Culture = new System.Globalization.CultureInfo("id-ID");
            this.dtTanggalKejadian.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtTanggalKejadian.Location = new System.Drawing.Point(0, 0);
            this.dtTanggalKejadian.Name = "dtTanggalKejadian";
            this.dtTanggalKejadian.Size = new System.Drawing.Size(560, 44);
            this.dtTanggalKejadian.TabIndex = 23;
            this.dtTanggalKejadian.TabStop = false;
            this.dtTanggalKejadian.Text = "Jumat, 21 Desember 2018";
            this.dtTanggalKejadian.Value = new System.DateTime(2018, 12, 21, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.dtTanggalKejadian.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dtTanggalKejadian.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dtTanggalKejadian.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dtTanggalKejadian.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dtTanggalKejadian.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dtTanggalKejadian.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dtTanggalKejadian.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dtTanggalKejadian.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "Jumat, 21 Desember 2018";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dtTanggalKejadian.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // radPanel25
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel25, 2);
            this.radPanel25.Controls.Add(this.txtPeranTahanan);
            this.radPanel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel25.Location = new System.Drawing.Point(263, 1012);
            this.radPanel25.Name = "radPanel25";
            this.radPanel25.Size = new System.Drawing.Size(560, 45);
            this.radPanel25.TabIndex = 22;
            // 
            // txtPeranTahanan
            // 
            this.txtPeranTahanan.AutoSize = false;
            this.txtPeranTahanan.BackColor = System.Drawing.Color.White;
            this.txtPeranTahanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPeranTahanan.Location = new System.Drawing.Point(0, 0);
            this.txtPeranTahanan.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtPeranTahanan.Name = "txtPeranTahanan";
            this.txtPeranTahanan.Size = new System.Drawing.Size(560, 45);
            this.txtPeranTahanan.TabIndex = 194;
            // 
            // radPanel24
            // 
            this.radPanel24.Controls.Add(this.txtTB);
            this.radPanel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel24.Location = new System.Drawing.Point(829, 513);
            this.radPanel24.Name = "radPanel24";
            this.radPanel24.Size = new System.Drawing.Size(240, 44);
            this.radPanel24.TabIndex = 13;
            // 
            // txtTB
            // 
            this.txtTB.AutoSize = false;
            this.txtTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTB.Location = new System.Drawing.Point(0, 0);
            this.txtTB.Name = "txtTB";
            this.txtTB.Size = new System.Drawing.Size(240, 44);
            this.txtTB.TabIndex = 1;
            // 
            // radLabel11
            // 
            this.radLabel11.AutoSize = false;
            this.radLabel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel11.ForeColor = System.Drawing.Color.Black;
            this.radLabel11.Location = new System.Drawing.Point(1076, 514);
            this.radLabel11.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radLabel11.Size = new System.Drawing.Size(62, 42);
            this.radLabel11.TabIndex = 205;
            this.radLabel11.Text = "CM";
            // 
            // radLabel10
            // 
            this.radLabel10.AutoSize = false;
            this.radLabel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel10.ForeColor = System.Drawing.Color.Black;
            this.radLabel10.Location = new System.Drawing.Point(1076, 464);
            this.radLabel10.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radLabel10.Size = new System.Drawing.Size(62, 42);
            this.radLabel10.TabIndex = 204;
            this.radLabel10.Text = "KG";
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel9.ForeColor = System.Drawing.Color.Black;
            this.radLabel9.Location = new System.Drawing.Point(678, 514);
            this.radLabel9.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radLabel9.Size = new System.Drawing.Size(144, 42);
            this.radLabel9.TabIndex = 203;
            this.radLabel9.Text = "<html>Tinggi Badan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel8.ForeColor = System.Drawing.Color.Black;
            this.radLabel8.Location = new System.Drawing.Point(678, 464);
            this.radLabel8.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radLabel8.Size = new System.Drawing.Size(144, 42);
            this.radLabel8.TabIndex = 202;
            this.radLabel8.Text = "<html>Berat Badan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel5.ForeColor = System.Drawing.Color.Black;
            this.radLabel5.Location = new System.Drawing.Point(14, 1164);
            this.radLabel5.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(242, 42);
            this.radLabel5.TabIndex = 199;
            this.radLabel5.Text = "<html>Tanggal Penangkapan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel4.ForeColor = System.Drawing.Color.Black;
            this.radLabel4.Location = new System.Drawing.Point(14, 1064);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(242, 42);
            this.radLabel4.TabIndex = 198;
            this.radLabel4.Text = "<html>Tanggal Kejadian<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel22
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel22, 3);
            this.radPanel22.Controls.Add(this.txtLokasiPenangkapan);
            this.radPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel22.Location = new System.Drawing.Point(263, 863);
            this.radPanel22.Name = "radPanel22";
            this.radPanel22.Size = new System.Drawing.Size(806, 44);
            this.radPanel22.TabIndex = 19;
            // 
            // txtLokasiPenangkapan
            // 
            this.txtLokasiPenangkapan.AutoSize = false;
            this.txtLokasiPenangkapan.BackColor = System.Drawing.Color.White;
            this.txtLokasiPenangkapan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLokasiPenangkapan.Location = new System.Drawing.Point(0, 0);
            this.txtLokasiPenangkapan.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtLokasiPenangkapan.Name = "txtLokasiPenangkapan";
            this.txtLokasiPenangkapan.Size = new System.Drawing.Size(806, 44);
            this.txtLokasiPenangkapan.TabIndex = 16;
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel2.ForeColor = System.Drawing.Color.Black;
            this.radLabel2.Location = new System.Drawing.Point(14, 864);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(242, 42);
            this.radLabel2.TabIndex = 196;
            this.radLabel2.Text = "<html>Lokasi Penangkapan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.ForeColor = System.Drawing.Color.Black;
            this.radLabel1.Location = new System.Drawing.Point(14, 1013);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(242, 43);
            this.radLabel1.TabIndex = 193;
            this.radLabel1.Text = "<html>Peran<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel19
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel19, 3);
            this.radPanel19.Controls.Add(this.txtSourceLocation);
            this.radPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel19.Location = new System.Drawing.Point(263, 813);
            this.radPanel19.Name = "radPanel19";
            this.radPanel19.Size = new System.Drawing.Size(806, 44);
            this.radPanel19.TabIndex = 18;
            // 
            // txtSourceLocation
            // 
            this.txtSourceLocation.AutoSize = false;
            this.txtSourceLocation.BackColor = System.Drawing.Color.White;
            this.txtSourceLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSourceLocation.Location = new System.Drawing.Point(0, 0);
            this.txtSourceLocation.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtSourceLocation.Name = "txtSourceLocation";
            this.txtSourceLocation.Size = new System.Drawing.Size(806, 44);
            this.txtSourceLocation.TabIndex = 16;
            // 
            // radPanel18
            // 
            this.radPanel18.Controls.Add(this.txtTelp);
            this.radPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel18.Location = new System.Drawing.Point(263, 613);
            this.radPanel18.Name = "radPanel18";
            this.radPanel18.Size = new System.Drawing.Size(408, 44);
            this.radPanel18.TabIndex = 15;
            // 
            // txtTelp
            // 
            this.txtTelp.AutoSize = false;
            this.txtTelp.BackColor = System.Drawing.Color.White;
            this.txtTelp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTelp.Location = new System.Drawing.Point(0, 0);
            this.txtTelp.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtTelp.MaxLength = 12;
            this.txtTelp.Name = "txtTelp";
            this.txtTelp.Padding = new System.Windows.Forms.Padding(5);
            this.txtTelp.Size = new System.Drawing.Size(408, 44);
            this.txtTelp.TabIndex = 13;
            // 
            // radPanel16
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel16, 3);
            this.radPanel16.Controls.Add(this.txtCity);
            this.radPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel16.Location = new System.Drawing.Point(263, 363);
            this.radPanel16.Name = "radPanel16";
            this.radPanel16.Size = new System.Drawing.Size(806, 44);
            this.radPanel16.TabIndex = 7;
            // 
            // txtCity
            // 
            this.txtCity.AutoSize = false;
            this.txtCity.BackColor = System.Drawing.Color.White;
            this.txtCity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCity.Location = new System.Drawing.Point(0, 0);
            this.txtCity.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtCity.Name = "txtCity";
            this.txtCity.Padding = new System.Windows.Forms.Padding(5);
            this.txtCity.Size = new System.Drawing.Size(806, 44);
            this.txtCity.TabIndex = 7;
            // 
            // radPanel17
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel17, 3);
            this.radPanel17.Controls.Add(this.txtAddress);
            this.radPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel17.Location = new System.Drawing.Point(263, 213);
            this.radPanel17.Name = "radPanel17";
            this.radPanel17.Size = new System.Drawing.Size(806, 144);
            this.radPanel17.TabIndex = 6;
            // 
            // txtAddress
            // 
            this.txtAddress.AutoSize = false;
            this.txtAddress.BackColor = System.Drawing.Color.White;
            this.txtAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAddress.Location = new System.Drawing.Point(0, 0);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(806, 144);
            this.txtAddress.TabIndex = 6;
            // 
            // radPanel15
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel15, 2);
            this.radPanel15.Controls.Add(this.txtNoIdentitas);
            this.radPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel15.Location = new System.Drawing.Point(677, 63);
            this.radPanel15.Name = "radPanel15";
            this.radPanel15.Size = new System.Drawing.Size(392, 44);
            this.radPanel15.TabIndex = 2;
            // 
            // txtNoIdentitas
            // 
            this.txtNoIdentitas.AutoSize = false;
            this.txtNoIdentitas.BackColor = System.Drawing.Color.White;
            this.txtNoIdentitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoIdentitas.Location = new System.Drawing.Point(0, 0);
            this.txtNoIdentitas.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNoIdentitas.MaxLength = 16;
            this.txtNoIdentitas.Name = "txtNoIdentitas";
            this.txtNoIdentitas.Size = new System.Drawing.Size(392, 44);
            this.txtNoIdentitas.TabIndex = 2;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radPanel11
            // 
            this.radPanel11.Controls.Add(this.ddlNationality);
            this.radPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel11.Location = new System.Drawing.Point(263, 563);
            this.radPanel11.Name = "radPanel11";
            this.radPanel11.Size = new System.Drawing.Size(408, 44);
            this.radPanel11.TabIndex = 14;
            // 
            // ddlNationality
            // 
            this.ddlNationality.AutoSize = false;
            this.ddlNationality.BackColor = System.Drawing.Color.White;
            this.ddlNationality.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlNationality.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlNationality.DropDownHeight = 400;
            this.ddlNationality.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlNationality.Location = new System.Drawing.Point(0, 0);
            this.ddlNationality.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlNationality.Name = "ddlNationality";
            // 
            // 
            // 
            this.ddlNationality.RootElement.CustomFont = "Roboto";
            this.ddlNationality.RootElement.CustomFontSize = 13F;
            this.ddlNationality.Size = new System.Drawing.Size(408, 44);
            this.ddlNationality.TabIndex = 12;
            this.ddlNationality.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlNationality.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlNationality.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlNationality.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlNationality.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlNationality.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlNationality.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlNationality.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlNationality.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlNationality.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlNationality.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel10
            // 
            this.radPanel10.Controls.Add(this.ddlVocation);
            this.radPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel10.Location = new System.Drawing.Point(263, 513);
            this.radPanel10.Name = "radPanel10";
            this.radPanel10.Size = new System.Drawing.Size(408, 44);
            this.radPanel10.TabIndex = 12;
            // 
            // ddlVocation
            // 
            this.ddlVocation.AutoSize = false;
            this.ddlVocation.BackColor = System.Drawing.Color.White;
            this.ddlVocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlVocation.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlVocation.DropDownHeight = 400;
            this.ddlVocation.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlVocation.Location = new System.Drawing.Point(0, 0);
            this.ddlVocation.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlVocation.Name = "ddlVocation";
            // 
            // 
            // 
            this.ddlVocation.RootElement.CustomFont = "Roboto";
            this.ddlVocation.RootElement.CustomFontSize = 13F;
            this.ddlVocation.Size = new System.Drawing.Size(408, 44);
            this.ddlVocation.TabIndex = 11;
            this.ddlVocation.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlVocation.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlVocation.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlVocation.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlVocation.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlVocation.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlVocation.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlVocation.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlVocation.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlVocation.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlVocation.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel9
            // 
            this.radPanel9.Controls.Add(this.ddlMaritalStatus);
            this.radPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel9.Location = new System.Drawing.Point(263, 463);
            this.radPanel9.Name = "radPanel9";
            this.radPanel9.Size = new System.Drawing.Size(408, 44);
            this.radPanel9.TabIndex = 10;
            // 
            // ddlMaritalStatus
            // 
            this.ddlMaritalStatus.AutoSize = false;
            this.ddlMaritalStatus.BackColor = System.Drawing.Color.White;
            this.ddlMaritalStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlMaritalStatus.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlMaritalStatus.DropDownHeight = 400;
            this.ddlMaritalStatus.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlMaritalStatus.Location = new System.Drawing.Point(0, 0);
            this.ddlMaritalStatus.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlMaritalStatus.Name = "ddlMaritalStatus";
            // 
            // 
            // 
            this.ddlMaritalStatus.RootElement.CustomFont = "Roboto";
            this.ddlMaritalStatus.RootElement.CustomFontSize = 13F;
            this.ddlMaritalStatus.Size = new System.Drawing.Size(408, 44);
            this.ddlMaritalStatus.TabIndex = 10;
            this.ddlMaritalStatus.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlMaritalStatus.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlMaritalStatus.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlMaritalStatus.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlMaritalStatus.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlMaritalStatus.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlMaritalStatus.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlMaritalStatus.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlMaritalStatus.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlMaritalStatus.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlMaritalStatus.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel6
            // 
            this.radPanel6.Controls.Add(this.ddlType);
            this.radPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel6.Location = new System.Drawing.Point(829, 763);
            this.radPanel6.Name = "radPanel6";
            this.radPanel6.Size = new System.Drawing.Size(240, 44);
            this.radPanel6.TabIndex = 17;
            // 
            // ddlType
            // 
            this.ddlType.AutoSize = false;
            this.ddlType.BackColor = System.Drawing.Color.White;
            this.ddlType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlType.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlType.DropDownHeight = 400;
            this.ddlType.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlType.Location = new System.Drawing.Point(0, 0);
            this.ddlType.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlType.Name = "ddlType";
            // 
            // 
            // 
            this.ddlType.RootElement.CustomFont = "Roboto";
            this.ddlType.RootElement.CustomFontSize = 13F;
            this.ddlType.Size = new System.Drawing.Size(240, 44);
            this.ddlType.TabIndex = 15;
            this.ddlType.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlType.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlType.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlType.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlType.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlType.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlType.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlType.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlType.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel5
            // 
            this.radPanel5.Controls.Add(this.ddlCategory);
            this.radPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel5.Location = new System.Drawing.Point(263, 763);
            this.radPanel5.Name = "radPanel5";
            this.radPanel5.Size = new System.Drawing.Size(408, 44);
            this.radPanel5.TabIndex = 16;
            // 
            // ddlCategory
            // 
            this.ddlCategory.AutoSize = false;
            this.ddlCategory.BackColor = System.Drawing.Color.White;
            this.ddlCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlCategory.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlCategory.DropDownHeight = 400;
            this.ddlCategory.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlCategory.Location = new System.Drawing.Point(0, 0);
            this.ddlCategory.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlCategory.Name = "ddlCategory";
            // 
            // 
            // 
            this.ddlCategory.RootElement.CustomFont = "Roboto";
            this.ddlCategory.RootElement.CustomFontSize = 13F;
            this.ddlCategory.Size = new System.Drawing.Size(408, 44);
            this.ddlCategory.TabIndex = 14;
            this.ddlCategory.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlCategory.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlCategory.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlCategory.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlCategory.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlCategory.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlCategory.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlCategory.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlCategory.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlCategory.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlCategory.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // lblNetwork
            // 
            this.lblNetwork.AutoSize = false;
            this.lblNetwork.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNetwork.ForeColor = System.Drawing.Color.Black;
            this.lblNetwork.Location = new System.Drawing.Point(14, 914);
            this.lblNetwork.Margin = new System.Windows.Forms.Padding(4);
            this.lblNetwork.Name = "lblNetwork";
            this.lblNetwork.Size = new System.Drawing.Size(242, 42);
            this.lblNetwork.TabIndex = 137;
            this.lblNetwork.Text = "<html>Jaringan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblPDOB
            // 
            this.lblPDOB.AutoSize = false;
            this.lblPDOB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPDOB.ForeColor = System.Drawing.Color.Black;
            this.lblPDOB.Location = new System.Drawing.Point(14, 164);
            this.lblPDOB.Margin = new System.Windows.Forms.Padding(4);
            this.lblPDOB.Name = "lblPDOB";
            this.lblPDOB.Size = new System.Drawing.Size(242, 42);
            this.lblPDOB.TabIndex = 331;
            this.lblPDOB.Text = "<html>Tempat/Tanggal Lahir<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblMaritalStatus
            // 
            this.lblMaritalStatus.AutoSize = false;
            this.lblMaritalStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMaritalStatus.ForeColor = System.Drawing.Color.Black;
            this.lblMaritalStatus.Location = new System.Drawing.Point(14, 464);
            this.lblMaritalStatus.Margin = new System.Windows.Forms.Padding(4);
            this.lblMaritalStatus.Name = "lblMaritalStatus";
            this.lblMaritalStatus.Size = new System.Drawing.Size(242, 42);
            this.lblMaritalStatus.TabIndex = 112;
            this.lblMaritalStatus.Text = "<html>Status Pernikahan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblVocation
            // 
            this.lblVocation.AutoSize = false;
            this.lblVocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblVocation.ForeColor = System.Drawing.Color.Black;
            this.lblVocation.Location = new System.Drawing.Point(14, 514);
            this.lblVocation.Margin = new System.Windows.Forms.Padding(4);
            this.lblVocation.Name = "lblVocation";
            this.lblVocation.Size = new System.Drawing.Size(242, 42);
            this.lblVocation.TabIndex = 112;
            this.lblVocation.Text = "<html>Pekerjaan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblNationality
            // 
            this.lblNationality.AutoSize = false;
            this.lblNationality.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNationality.ForeColor = System.Drawing.Color.Black;
            this.lblNationality.Location = new System.Drawing.Point(14, 564);
            this.lblNationality.Margin = new System.Windows.Forms.Padding(4);
            this.lblNationality.Name = "lblNationality";
            this.lblNationality.Size = new System.Drawing.Size(242, 42);
            this.lblNationality.TabIndex = 112;
            this.lblNationality.Text = "<html>Kewarganegaraan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblFullName
            // 
            this.lblFullName.AutoSize = false;
            this.lblFullName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFullName.ForeColor = System.Drawing.Color.Black;
            this.lblFullName.Location = new System.Drawing.Point(14, 114);
            this.lblFullName.Margin = new System.Windows.Forms.Padding(4);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.Size = new System.Drawing.Size(242, 42);
            this.lblFullName.TabIndex = 181;
            this.lblFullName.Text = "<html>NAMA LENGKAP<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblIDType
            // 
            this.lblIDType.AutoSize = false;
            this.lblIDType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIDType.ForeColor = System.Drawing.Color.Black;
            this.lblIDType.Location = new System.Drawing.Point(14, 64);
            this.lblIDType.Margin = new System.Windows.Forms.Padding(4);
            this.lblIDType.Name = "lblIDType";
            this.lblIDType.Size = new System.Drawing.Size(242, 42);
            this.lblIDType.TabIndex = 191;
            this.lblIDType.Text = "<html>JENIS IDENTITAS/NO<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblDetailVisitor
            // 
            this.lblDetailVisitor.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailVisitor, 4);
            this.lblDetailVisitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailVisitor.Location = new System.Drawing.Point(13, 13);
            this.lblDetailVisitor.Name = "lblDetailVisitor";
            this.lblDetailVisitor.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDetailVisitor.Size = new System.Drawing.Size(1056, 44);
            this.lblDetailVisitor.TabIndex = 134;
            this.lblDetailVisitor.Text = "INMATE PROFILE";
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel1, 2);
            this.radPanel1.Controls.Add(this.picCanvas);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(1147, 60);
            this.radPanel1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.tableLayoutPanel1.SetRowSpan(this.radPanel1, 5);
            this.radPanel1.Size = new System.Drawing.Size(335, 345);
            this.radPanel1.TabIndex = 144;
            // 
            // picCanvas
            // 
            this.picCanvas.BackColor = System.Drawing.Color.Silver;
            this.picCanvas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picCanvas.Location = new System.Drawing.Point(5, 0);
            this.picCanvas.Margin = new System.Windows.Forms.Padding(8);
            this.picCanvas.Name = "picCanvas";
            this.picCanvas.Size = new System.Drawing.Size(325, 340);
            this.picCanvas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCanvas.TabIndex = 129;
            this.picCanvas.TabStop = false;
            // 
            // lblInmatePhoto
            // 
            this.lblInmatePhoto.AutoSize = false;
            this.lblInmatePhoto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.tableLayoutPanel1.SetColumnSpan(this.lblInmatePhoto, 2);
            this.lblInmatePhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInmatePhoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.lblInmatePhoto.ForeColor = System.Drawing.Color.White;
            this.lblInmatePhoto.Image = ((System.Drawing.Image)(resources.GetObject("lblInmatePhoto.Image")));
            this.lblInmatePhoto.Location = new System.Drawing.Point(1147, 10);
            this.lblInmatePhoto.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblInmatePhoto.Name = "lblInmatePhoto";
            this.lblInmatePhoto.Padding = new System.Windows.Forms.Padding(8);
            this.lblInmatePhoto.Size = new System.Drawing.Size(335, 50);
            this.lblInmatePhoto.TabIndex = 179;
            this.lblInmatePhoto.Text = "         FOTO TAHANAN";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel3.ForeColor = System.Drawing.Color.Black;
            this.radLabel3.Location = new System.Drawing.Point(14, 964);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(242, 41);
            this.radLabel3.TabIndex = 185;
            this.radLabel3.Text = "<html>Kasus<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // btnAddCases
            // 
            this.btnAddCases.AutoSize = true;
            this.btnAddCases.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAddCases.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.add_doc;
            this.btnAddCases.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAddCases.Location = new System.Drawing.Point(829, 963);
            this.btnAddCases.Name = "btnAddCases";
            this.btnAddCases.Size = new System.Drawing.Size(27, 27);
            this.btnAddCases.TabIndex = 189;
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel7.ForeColor = System.Drawing.Color.Black;
            this.radLabel7.Location = new System.Drawing.Point(14, 1314);
            this.radLabel7.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(242, 142);
            this.radLabel7.TabIndex = 139;
            this.radLabel7.Text = "<html>Catatan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel3, 2);
            this.radPanel3.Controls.Add(this.ddlNetwork);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Location = new System.Drawing.Point(263, 913);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(560, 44);
            this.radPanel3.TabIndex = 20;
            // 
            // ddlNetwork
            // 
            this.ddlNetwork.AutoSize = false;
            this.ddlNetwork.BackColor = System.Drawing.Color.White;
            this.ddlNetwork.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlNetwork.DropDownHeight = 400;
            this.ddlNetwork.Location = new System.Drawing.Point(0, 0);
            this.ddlNetwork.Name = "ddlNetwork";
            this.ddlNetwork.Size = new System.Drawing.Size(560, 44);
            this.ddlNetwork.TabIndex = 19;
            ((Telerik.WinControls.UI.RadCheckedDropDownListElement)(this.ddlNetwork.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlNetwork.GetChildAt(0).GetChildAt(2))).MinSize = new System.Drawing.Size(0, 50);
            // 
            // lblInmateCategory
            // 
            this.lblInmateCategory.AutoSize = false;
            this.lblInmateCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInmateCategory.ForeColor = System.Drawing.Color.Black;
            this.lblInmateCategory.Location = new System.Drawing.Point(14, 764);
            this.lblInmateCategory.Margin = new System.Windows.Forms.Padding(4);
            this.lblInmateCategory.Name = "lblInmateCategory";
            this.lblInmateCategory.Size = new System.Drawing.Size(242, 42);
            this.lblInmateCategory.TabIndex = 137;
            this.lblInmateCategory.Text = "<html>Kategori Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel6.ForeColor = System.Drawing.Color.Black;
            this.radLabel6.Location = new System.Drawing.Point(678, 764);
            this.radLabel6.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radLabel6.Size = new System.Drawing.Size(144, 42);
            this.radLabel6.TabIndex = 138;
            this.radLabel6.Text = "<html>Tipe Tahanan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblDetailVisit
            // 
            this.lblDetailVisit.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailVisit, 4);
            this.lblDetailVisit.Controls.Add(this.tahananId);
            this.lblDetailVisit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailVisit.Location = new System.Drawing.Point(13, 713);
            this.lblDetailVisit.Name = "lblDetailVisit";
            this.lblDetailVisit.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDetailVisit.Size = new System.Drawing.Size(1056, 44);
            this.lblDetailVisit.TabIndex = 135;
            this.lblDetailVisit.Text = "DETAIL TAHANAN";
            // 
            // tahananId
            // 
            this.tahananId.Location = new System.Drawing.Point(1077, 0);
            this.tahananId.Name = "tahananId";
            this.tahananId.Size = new System.Drawing.Size(52, 44);
            this.tahananId.TabIndex = 167;
            this.tahananId.Visible = false;
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = false;
            this.lblPhone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPhone.ForeColor = System.Drawing.Color.Black;
            this.lblPhone.Location = new System.Drawing.Point(14, 614);
            this.lblPhone.Margin = new System.Windows.Forms.Padding(4);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(242, 42);
            this.lblPhone.TabIndex = 111;
            this.lblPhone.Text = "<html>Nomor Telepon<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblReligion
            // 
            this.lblReligion.AutoSize = false;
            this.lblReligion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblReligion.ForeColor = System.Drawing.Color.Black;
            this.lblReligion.Location = new System.Drawing.Point(14, 414);
            this.lblReligion.Margin = new System.Windows.Forms.Padding(4);
            this.lblReligion.Name = "lblReligion";
            this.lblReligion.Size = new System.Drawing.Size(242, 42);
            this.lblReligion.TabIndex = 130;
            this.lblReligion.Text = "<html>Agama<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = false;
            this.lblGender.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGender.ForeColor = System.Drawing.Color.Black;
            this.lblGender.Location = new System.Drawing.Point(678, 414);
            this.lblGender.Margin = new System.Windows.Forms.Padding(4);
            this.lblGender.Name = "lblGender";
            this.lblGender.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.lblGender.Size = new System.Drawing.Size(144, 42);
            this.lblGender.TabIndex = 132;
            this.lblGender.Text = "<html>Jenis Kelamin<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = false;
            this.lblCity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCity.ForeColor = System.Drawing.Color.Black;
            this.lblCity.Location = new System.Drawing.Point(14, 364);
            this.lblCity.Margin = new System.Windows.Forms.Padding(4);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(242, 42);
            this.lblCity.TabIndex = 110;
            this.lblCity.Text = "<html>Kota<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = false;
            this.lblAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAddress.ForeColor = System.Drawing.Color.Black;
            this.lblAddress.Location = new System.Drawing.Point(14, 214);
            this.lblAddress.Margin = new System.Windows.Forms.Padding(4);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(242, 142);
            this.lblAddress.TabIndex = 321;
            this.lblAddress.Text = "<html>Alamat<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = false;
            this.lblLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLocation.ForeColor = System.Drawing.Color.Black;
            this.lblLocation.Location = new System.Drawing.Point(14, 814);
            this.lblLocation.Margin = new System.Windows.Forms.Padding(4);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(242, 42);
            this.lblLocation.TabIndex = 136;
            this.lblLocation.Text = "<html>Lokasi Kejadian<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel7
            // 
            this.radPanel7.Controls.Add(this.ddlAgama);
            this.radPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel7.Location = new System.Drawing.Point(263, 413);
            this.radPanel7.Name = "radPanel7";
            this.radPanel7.Size = new System.Drawing.Size(408, 44);
            this.radPanel7.TabIndex = 8;
            // 
            // ddlAgama
            // 
            this.ddlAgama.AutoSize = false;
            this.ddlAgama.BackColor = System.Drawing.Color.White;
            this.ddlAgama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlAgama.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlAgama.DropDownHeight = 400;
            this.ddlAgama.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlAgama.Location = new System.Drawing.Point(0, 0);
            this.ddlAgama.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlAgama.Name = "ddlAgama";
            // 
            // 
            // 
            this.ddlAgama.RootElement.CustomFont = "Roboto";
            this.ddlAgama.RootElement.CustomFontSize = 13F;
            this.ddlAgama.Size = new System.Drawing.Size(408, 44);
            this.ddlAgama.TabIndex = 8;
            this.ddlAgama.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlAgama.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlAgama.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlAgama.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlAgama.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlAgama.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlAgama.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlAgama.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlAgama.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlAgama.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlAgama.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel8
            // 
            this.radPanel8.Controls.Add(this.ddlJnsIdentitas);
            this.radPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel8.Location = new System.Drawing.Point(263, 63);
            this.radPanel8.Name = "radPanel8";
            this.radPanel8.Size = new System.Drawing.Size(408, 44);
            this.radPanel8.TabIndex = 1;
            // 
            // ddlJnsIdentitas
            // 
            this.ddlJnsIdentitas.AutoSize = false;
            this.ddlJnsIdentitas.BackColor = System.Drawing.Color.White;
            this.ddlJnsIdentitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlJnsIdentitas.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlJnsIdentitas.DropDownHeight = 400;
            this.ddlJnsIdentitas.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlJnsIdentitas.Location = new System.Drawing.Point(0, 0);
            this.ddlJnsIdentitas.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlJnsIdentitas.Name = "ddlJnsIdentitas";
            // 
            // 
            // 
            this.ddlJnsIdentitas.RootElement.CustomFont = "Roboto";
            this.ddlJnsIdentitas.RootElement.CustomFontSize = 13F;
            this.ddlJnsIdentitas.Size = new System.Drawing.Size(408, 44);
            this.ddlJnsIdentitas.TabIndex = 1;
            this.ddlJnsIdentitas.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlJnsIdentitas.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlJnsIdentitas.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlJnsIdentitas.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlJnsIdentitas.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel4, 3);
            this.radPanel4.Controls.Add(this.txtNama);
            this.radPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel4.Location = new System.Drawing.Point(263, 113);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(806, 44);
            this.radPanel4.TabIndex = 3;
            // 
            // txtNama
            // 
            this.txtNama.AutoSize = false;
            this.txtNama.BackColor = System.Drawing.Color.White;
            this.txtNama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNama.Location = new System.Drawing.Point(0, 0);
            this.txtNama.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNama.Name = "txtNama";
            this.txtNama.Size = new System.Drawing.Size(806, 44);
            this.txtNama.TabIndex = 3;
            // 
            // radPanel13
            // 
            this.radPanel13.Controls.Add(this.txtPlaceofBirth);
            this.radPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel13.Location = new System.Drawing.Point(263, 163);
            this.radPanel13.Name = "radPanel13";
            this.radPanel13.Size = new System.Drawing.Size(408, 44);
            this.radPanel13.TabIndex = 4;
            // 
            // txtPlaceofBirth
            // 
            this.txtPlaceofBirth.AutoSize = false;
            this.txtPlaceofBirth.BackColor = System.Drawing.Color.White;
            this.txtPlaceofBirth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPlaceofBirth.Location = new System.Drawing.Point(0, 0);
            this.txtPlaceofBirth.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtPlaceofBirth.MaxLength = 16;
            this.txtPlaceofBirth.Name = "txtPlaceofBirth";
            this.txtPlaceofBirth.Size = new System.Drawing.Size(408, 44);
            this.txtPlaceofBirth.TabIndex = 4;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtPlaceofBirth.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtPlaceofBirth.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtPlaceofBirth.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtPlaceofBirth.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radPanel14
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel14, 3);
            this.radPanel14.Controls.Add(this.txtNotes);
            this.radPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel14.Location = new System.Drawing.Point(263, 1313);
            this.radPanel14.Name = "radPanel14";
            this.radPanel14.Size = new System.Drawing.Size(806, 144);
            this.radPanel14.TabIndex = 25;
            // 
            // txtNotes
            // 
            this.txtNotes.AutoSize = false;
            this.txtNotes.BackColor = System.Drawing.Color.White;
            this.txtNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNotes.Location = new System.Drawing.Point(0, 0);
            this.txtNotes.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNotes.Multiline = true;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.Padding = new System.Windows.Forms.Padding(5);
            this.txtNotes.Size = new System.Drawing.Size(806, 144);
            this.txtNotes.TabIndex = 19;
            // 
            // radPanel20
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel20, 2);
            this.radPanel20.Controls.Add(this.dpDateofBirth);
            this.radPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel20.Location = new System.Drawing.Point(677, 163);
            this.radPanel20.Name = "radPanel20";
            this.radPanel20.Size = new System.Drawing.Size(392, 44);
            this.radPanel20.TabIndex = 5;
            // 
            // dpDateofBirth
            // 
            this.dpDateofBirth.AutoSize = false;
            this.dpDateofBirth.BackColor = System.Drawing.Color.Transparent;
            this.dpDateofBirth.CalendarSize = new System.Drawing.Size(290, 320);
            this.dpDateofBirth.Culture = new System.Globalization.CultureInfo("id-ID");
            this.dpDateofBirth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dpDateofBirth.Location = new System.Drawing.Point(0, 0);
            this.dpDateofBirth.Name = "dpDateofBirth";
            this.dpDateofBirth.Size = new System.Drawing.Size(392, 44);
            this.dpDateofBirth.TabIndex = 5;
            this.dpDateofBirth.TabStop = false;
            this.dpDateofBirth.Text = "Jumat, 21 Desember 2018";
            this.dpDateofBirth.Value = new System.DateTime(2018, 12, 21, 0, 0, 0, 0);
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.dpDateofBirth.GetChildAt(0))).CalendarSize = new System.Drawing.Size(290, 320);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dpDateofBirth.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dpDateofBirth.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpDateofBirth.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpDateofBirth.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpDateofBirth.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.White;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.dpDateofBirth.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dpDateofBirth.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Text = "Jumat, 21 Desember 2018";
            ((Telerik.WinControls.UI.RadMaskedEditBoxElement)(this.dpDateofBirth.GetChildAt(0).GetChildAt(2).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            // 
            // radPanel12
            // 
            this.radPanel12.Controls.Add(this.ddlGender);
            this.radPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel12.Location = new System.Drawing.Point(829, 413);
            this.radPanel12.Name = "radPanel12";
            this.radPanel12.Size = new System.Drawing.Size(240, 44);
            this.radPanel12.TabIndex = 9;
            // 
            // ddlGender
            // 
            this.ddlGender.AutoSize = false;
            this.ddlGender.BackColor = System.Drawing.Color.White;
            this.ddlGender.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlGender.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlGender.DropDownHeight = 200;
            this.ddlGender.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlGender.Location = new System.Drawing.Point(0, 0);
            this.ddlGender.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlGender.Name = "ddlGender";
            // 
            // 
            // 
            this.ddlGender.RootElement.CustomFont = "Roboto";
            this.ddlGender.RootElement.CustomFontSize = 13F;
            this.ddlGender.Size = new System.Drawing.Size(240, 44);
            this.ddlGender.TabIndex = 9;
            this.ddlGender.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlGender.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlGender.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlGender.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlGender.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlGender.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlGender.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlGender.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlGender.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlGender.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlGender.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel21
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel21, 2);
            this.radPanel21.Controls.Add(this.ddlCases);
            this.radPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel21.Location = new System.Drawing.Point(263, 963);
            this.radPanel21.Name = "radPanel21";
            this.radPanel21.Size = new System.Drawing.Size(560, 43);
            this.radPanel21.TabIndex = 21;
            // 
            // ddlCases
            // 
            this.ddlCases.AutoSize = false;
            this.ddlCases.BackColor = System.Drawing.Color.White;
            this.ddlCases.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlCases.DropDownHeight = 400;
            this.ddlCases.Location = new System.Drawing.Point(0, 0);
            this.ddlCases.Name = "ddlCases";
            this.ddlCases.Size = new System.Drawing.Size(560, 43);
            this.ddlCases.TabIndex = 18;
            ((Telerik.WinControls.UI.RadCheckedDropDownListElement)(this.ddlCases.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlCases.GetChildAt(0).GetChildAt(2))).MinSize = new System.Drawing.Size(0, 50);
            // 
            // radPanel23
            // 
            this.radPanel23.Controls.Add(this.txtBB);
            this.radPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel23.Location = new System.Drawing.Point(829, 463);
            this.radPanel23.Name = "radPanel23";
            this.radPanel23.Size = new System.Drawing.Size(240, 44);
            this.radPanel23.TabIndex = 11;
            // 
            // txtBB
            // 
            this.txtBB.AutoSize = false;
            this.txtBB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBB.Location = new System.Drawing.Point(0, 0);
            this.txtBB.Name = "txtBB";
            this.txtBB.Size = new System.Drawing.Size(240, 44);
            this.txtBB.TabIndex = 0;
            // 
            // btnAddNetwork
            // 
            this.btnAddNetwork.AutoSize = true;
            this.btnAddNetwork.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAddNetwork.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.add_doc;
            this.btnAddNetwork.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAddNetwork.Location = new System.Drawing.Point(829, 913);
            this.btnAddNetwork.Name = "btnAddNetwork";
            this.btnAddNetwork.Size = new System.Drawing.Size(27, 27);
            this.btnAddNetwork.TabIndex = 188;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.btnTakePhoto, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(1145, 413);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(174, 44);
            this.tableLayoutPanel3.TabIndex = 173;
            // 
            // btnTakePhoto
            // 
            this.btnTakePhoto.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnTakePhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTakePhoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTakePhoto.Location = new System.Drawing.Point(3, 3);
            this.btnTakePhoto.Name = "btnTakePhoto";
            this.btnTakePhoto.Size = new System.Drawing.Size(168, 38);
            this.btnTakePhoto.TabIndex = 26;
            this.btnTakePhoto.Text = "AMBIL FOTO";
            this.btnTakePhoto.UseVisualStyleBackColor = false;
            // 
            // tblNavigation
            // 
            this.tblNavigation.ColumnCount = 3;
            this.tblNavigation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblNavigation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblNavigation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblNavigation.Controls.Add(this.labelRight, 0, 0);
            this.tblNavigation.Controls.Add(this.lblFront, 0, 0);
            this.tblNavigation.Controls.Add(this.labelLeft, 1, 0);
            this.tblNavigation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblNavigation.Location = new System.Drawing.Point(1325, 413);
            this.tblNavigation.Name = "tblNavigation";
            this.tblNavigation.RowCount = 1;
            this.tblNavigation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblNavigation.Size = new System.Drawing.Size(159, 44);
            this.tblNavigation.TabIndex = 166;
            // 
            // labelRight
            // 
            this.labelRight.AutoSize = false;
            this.labelRight.BackColor = System.Drawing.SystemColors.ControlDark;
            this.labelRight.BorderVisible = true;
            this.labelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelRight.Font = new System.Drawing.Font("Verdana", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRight.ForeColor = System.Drawing.Color.White;
            this.labelRight.Location = new System.Drawing.Point(56, 3);
            this.labelRight.Name = "labelRight";
            this.labelRight.Size = new System.Drawing.Size(47, 38);
            this.labelRight.TabIndex = 1001;
            this.labelRight.Text = "<html>Ka<br><size=5>Kanan";
            this.labelRight.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFront
            // 
            this.lblFront.AutoSize = false;
            this.lblFront.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblFront.BorderVisible = true;
            this.lblFront.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFront.Font = new System.Drawing.Font("Verdana", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFront.ForeColor = System.Drawing.Color.White;
            this.lblFront.Location = new System.Drawing.Point(3, 3);
            this.lblFront.Name = "lblFront";
            this.lblFront.Size = new System.Drawing.Size(47, 38);
            this.lblFront.TabIndex = 1000;
            this.lblFront.Text = "<html>D<br><size=5>Depan";
            this.lblFront.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLeft
            // 
            this.labelLeft.AutoSize = false;
            this.labelLeft.BackColor = System.Drawing.SystemColors.ControlDark;
            this.labelLeft.BorderVisible = true;
            this.labelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelLeft.Font = new System.Drawing.Font("Verdana", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLeft.ForeColor = System.Drawing.Color.White;
            this.labelLeft.Location = new System.Drawing.Point(109, 3);
            this.labelLeft.Name = "labelLeft";
            this.labelLeft.Size = new System.Drawing.Size(47, 38);
            this.labelLeft.TabIndex = 1002;
            this.labelLeft.Text = "<html>Ki<br><size=5>Kiri";
            this.labelLeft.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblVisitorFinger
            // 
            this.lblVisitorFinger.AutoSize = false;
            this.lblVisitorFinger.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.tableLayoutPanel1.SetColumnSpan(this.lblVisitorFinger, 2);
            this.lblVisitorFinger.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.lblVisitorFinger.ForeColor = System.Drawing.Color.White;
            this.lblVisitorFinger.Image = ((System.Drawing.Image)(resources.GetObject("lblVisitorFinger.Image")));
            this.lblVisitorFinger.Location = new System.Drawing.Point(1142, 510);
            this.lblVisitorFinger.Margin = new System.Windows.Forms.Padding(0);
            this.lblVisitorFinger.Name = "lblVisitorFinger";
            this.lblVisitorFinger.Padding = new System.Windows.Forms.Padding(8);
            this.lblVisitorFinger.Size = new System.Drawing.Size(345, 50);
            this.lblVisitorFinger.TabIndex = 182;
            this.lblVisitorFinger.Text = "         SIDIK JARI TAHANAN (IBU JARI)";
            // 
            // radPanel2
            // 
            this.radPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel2, 2);
            this.radPanel2.Controls.Add(this.picFinger);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(1142, 560);
            this.radPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.tableLayoutPanel1.SetRowSpan(this.radPanel2, 7);
            this.radPanel2.Size = new System.Drawing.Size(345, 350);
            this.radPanel2.TabIndex = 183;
            // 
            // picFinger
            // 
            this.picFinger.BackColor = System.Drawing.Color.Silver;
            this.picFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picFinger.Location = new System.Drawing.Point(5, 0);
            this.picFinger.Margin = new System.Windows.Forms.Padding(8, 0, 8, 8);
            this.picFinger.Name = "picFinger";
            this.picFinger.Size = new System.Drawing.Size(335, 345);
            this.picFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFinger.TabIndex = 176;
            this.picFinger.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.btnUbahSidikJari, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(1145, 913);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(339, 44);
            this.tableLayoutPanel2.TabIndex = 332;
            // 
            // btnUbahSidikJari
            // 
            this.btnUbahSidikJari.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnUbahSidikJari.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUbahSidikJari.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUbahSidikJari.Location = new System.Drawing.Point(3, 3);
            this.btnUbahSidikJari.Name = "btnUbahSidikJari";
            this.btnUbahSidikJari.Size = new System.Drawing.Size(333, 38);
            this.btnUbahSidikJari.TabIndex = 26;
            this.btnUbahSidikJari.Text = "UBAH SIDIK JARI";
            this.btnUbahSidikJari.UseVisualStyleBackColor = false;
            // 
            // formCases
            // 
            this.formCases.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.formCases.Location = new System.Drawing.Point(1000, 1000);
            this.formCases.Name = "formCases";
            this.formCases.Padding = new System.Windows.Forms.Padding(5);
            this.formCases.Size = new System.Drawing.Size(791, 409);
            this.formCases.TabIndex = 83;
            // 
            // formNetwork
            // 
            this.formNetwork.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.formNetwork.Location = new System.Drawing.Point(1000, 1000);
            this.formNetwork.Name = "formNetwork";
            this.formNetwork.Padding = new System.Windows.Forms.Padding(5);
            this.formNetwork.Size = new System.Drawing.Size(791, 409);
            this.formNetwork.TabIndex = 83;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // FormTahanan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.formCases);
            this.Controls.Add(this.formNetwork);
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radPanelError);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblDetailSurat);
            this.Controls.Add(this.headerPanel);
            this.Name = "FormTahanan";
            this.Size = new System.Drawing.Size(1516, 750);
            this.Load += new System.EventHandler(this.TambahTersangka_Load);
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).EndInit();
            this.headerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).EndInit();
            this.radPanelError.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuratPenahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuratPenangkapan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalPenahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel27)).EndInit();
            this.radPanel27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalPenangkapan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel26)).EndInit();
            this.radPanel26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtTanggalKejadian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel25)).EndInit();
            this.radPanel25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPeranTahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel24)).EndInit();
            this.radPanel24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel22)).EndInit();
            this.radPanel22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtLokasiPenangkapan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel19)).EndInit();
            this.radPanel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSourceLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel18)).EndInit();
            this.radPanel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel16)).EndInit();
            this.radPanel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel17)).EndInit();
            this.radPanel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel15)).EndInit();
            this.radPanel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNoIdentitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel11)).EndInit();
            this.radPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlNationality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel10)).EndInit();
            this.radPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlVocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel9)).EndInit();
            this.radPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlMaritalStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).EndInit();
            this.radPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).EndInit();
            this.radPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNetwork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPDOB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMaritalStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNationality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFullName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIDType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInmatePhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddCases)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlNetwork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInmateCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisit)).EndInit();
            this.lblDetailVisit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReligion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).EndInit();
            this.radPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlAgama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).EndInit();
            this.radPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlJnsIdentitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            this.radPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel13)).EndInit();
            this.radPanel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPlaceofBirth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel14)).EndInit();
            this.radPanel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel20)).EndInit();
            this.radPanel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dpDateofBirth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel12)).EndInit();
            this.radPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlGender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel21)).EndInit();
            this.radPanel21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlCases)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel23)).EndInit();
            this.radPanel23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtBB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddNetwork)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tblNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFront)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVisitorFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picFinger)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList imageList;
        private Telerik.WinControls.UI.RadPanel headerPanel;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel lblDetailSurat;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadTextBox txtCity;
        private Telerik.WinControls.UI.RadTextBox txtTelp;
        private Telerik.WinControls.UI.RadLabel lblPhone;
        private Telerik.WinControls.UI.RadLabel lblCity;
        private Telerik.WinControls.UI.RadLabel lblAddress;
        private Telerik.WinControls.UI.RadLabel lblFullName;
        private Telerik.WinControls.UI.RadLabel lblIDType;
        private Telerik.WinControls.UI.RadTextBox txtNoIdentitas;
        private Telerik.WinControls.UI.RadTextBox txtNama;
        private Telerik.WinControls.UI.RadTextBox txtAddress;
        private Telerik.WinControls.UI.RadLabel lblReligion;
        private Telerik.WinControls.UI.RadLabel lblGender;
        private Telerik.WinControls.UI.RadLabel lblDetailVisitor;
        private Telerik.WinControls.UI.RadLabel lblInmateCategory;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel lblLocation;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox txtNotes;
        private Telerik.WinControls.UI.RadLabel lblDetailVisit;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private System.Windows.Forms.PictureBox picCanvas;
        private Telerik.WinControls.UI.RadTextBox txtSourceLocation;
        private System.Windows.Forms.TableLayoutPanel tblNavigation;
        private System.Windows.Forms.Label tahananId;
        private Telerik.WinControls.UI.RadPanel radPanelError;
        private Telerik.WinControls.UI.RadLabel lblError;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnBack;
        private Telerik.WinControls.UI.RadButton btnSave;
        private Telerik.WinControls.UI.RadLabel lblInmatePhoto;
        private Telerik.WinControls.UI.RadLabel lblVisitorFinger;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private System.Windows.Forms.PictureBox picFinger;
        private FormNetwork formNetwork;
        private FormCases formCases;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Telerik.WinControls.UI.RadLabel labelRight;
        private Telerik.WinControls.UI.RadLabel lblFront;
        private Telerik.WinControls.UI.RadLabel labelLeft;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadButton btnAddNetwork;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadButton btnAddCases;
        private Telerik.WinControls.UI.RadCheckedDropDownList ddlCases;
        private Telerik.WinControls.UI.RadLabel lblMaritalStatus;
        private Telerik.WinControls.UI.RadLabel lblVocation;
        private Telerik.WinControls.UI.RadLabel lblNationality;
        private Telerik.WinControls.UI.RadLabel lblPDOB;
        private Telerik.WinControls.UI.RadTextBox txtPlaceofBirth;
        private Telerik.WinControls.UI.RadLabel lblNetwork;
        private Telerik.WinControls.UI.RadDropDownList ddlCategory;
        private Telerik.WinControls.UI.RadDropDownList ddlType;
        private Telerik.WinControls.UI.RadPanel radPanel5;
        private Telerik.WinControls.UI.RadPanel radPanel6;
        private Telerik.WinControls.UI.RadPanel radPanel8;
        private Telerik.WinControls.UI.RadPanel radPanel7;
        private Telerik.WinControls.UI.RadDropDownList ddlMaritalStatus;
        private Telerik.WinControls.UI.RadPanel radPanel10;
        private Telerik.WinControls.UI.RadDropDownList ddlVocation;
        private Telerik.WinControls.UI.RadPanel radPanel9;
        private Telerik.WinControls.UI.RadDropDownList ddlAgama;
        private Telerik.WinControls.UI.RadDropDownList ddlNationality;
        private Telerik.WinControls.UI.RadPanel radPanel12;
        private Telerik.WinControls.UI.RadDropDownList ddlGender;
        private Telerik.WinControls.UI.RadPanel radPanel11;
        private Telerik.WinControls.UI.RadDropDownList ddlJnsIdentitas;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadPanel radPanel14;
        private Telerik.WinControls.UI.RadPanel radPanel13;
        private Telerik.WinControls.UI.RadPanel radPanel15;
        private Telerik.WinControls.UI.RadPanel radPanel16;
        private Telerik.WinControls.UI.RadPanel radPanel17;
        private Telerik.WinControls.UI.RadPanel radPanel18;
        private Telerik.WinControls.UI.RadPanel radPanel19;
        private Telerik.WinControls.UI.RadDateTimePicker dpDateofBirth;
        private Telerik.WinControls.UI.RadPanel radPanel20;
        private Telerik.WinControls.UI.RadTextBox txtPeranTahanan;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadPanel radPanel22;
        private Telerik.WinControls.UI.RadTextBox txtLokasiPenangkapan;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadPanel radPanel21;
        private Telerik.WinControls.UI.RadDateTimePicker dtTanggalPenangkapan;
        private Telerik.WinControls.UI.RadDateTimePicker dtTanggalKejadian;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadPanel radPanel24;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadPanel radPanel23;
        private Telerik.WinControls.UI.RadTextBox txtBB;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private Telerik.WinControls.UI.RadTextBox txtTB;
        private Telerik.WinControls.UI.RadPanel radPanel27;
        private Telerik.WinControls.UI.RadPanel radPanel26;
        private Telerik.WinControls.UI.RadPanel radPanel25;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnUbahSidikJari;
        private System.Windows.Forms.Button btnTakePhoto;
        private Telerik.WinControls.UI.RadCheckedDropDownList ddlNetwork;
        private Telerik.WinControls.UI.RadTextBox txtSuratPenahanan;
        private Telerik.WinControls.UI.RadTextBox txtSuratPenangkapan;
        private Telerik.WinControls.UI.RadDateTimePicker dtTanggalPenahanan;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel12;
    }
}
