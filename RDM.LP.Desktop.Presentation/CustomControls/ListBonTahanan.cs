﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListBonTahanan : Base
    {
        public RadGridView GvBonTahanan { get { return this.gvBonTahanan; } }
        public ListBonTahanan()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvBonTahanan.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvBonTahanan.RowFormatting += radGridView_RowFormatting;
            gvBonTahanan.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvBonTahanan.CellClick += gvBonTahanan_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarBonTahanan.pdf", "DAFTAR DATA BON TAHANAN", gvBonTahanan);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarBonTahanan.csv", "DAFTAR DATA BON TAHANAN", gvBonTahanan);
        }

        private void gvBonTahanan_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == null)
                return;
            switch (e.Column.Index)
            {
                case 0:
                    MainForm.formMain.BonTahananDetail.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.BonTahananDetail.Show();
                    break;
                case 1:
                    MainForm.formMain.BonTahanan.Tag = e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.AddNew;
                    MainForm.formMain.BonTahanan.LoadData();
                    break;
                case 2:
                    var question  = UserFunction.Confirm("Hapus Bon Tahanan Ini?");
                    if (question == DialogResult.Yes)
                    {
                        DeleteBonTahanan(e.Row.Cells["Id"].Value.ToString());
                        UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Dihapus!");
                        UserFunction.LoadDataBonTahananToGrid(gvBonTahanan);
                    }
                    break;
                case 3:
                    MainForm.formMain.PreviewDoc.Tag = "BonTahanan-" + e.Row.Cells["Id"].Value.ToString();
                    MainForm.formMain.PreviewDoc.Show();
                    this.Hide();
                    break;
            }
            
        }

        private void DeleteBonTahanan(string Id)
        {
            using (BonTahananService bonserv = new BonTahananService())
            {
                var data = bonserv.GetById(Convert.ToInt32(Id));
                bonserv.DeleteById(Convert.ToInt32(Id));
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERVISITOR.ToString(), Activities = "Delete Visitor, data=" + UserFunction.JsonString(data) });
            }
        }

        private void InitForm()
        {
            this.lblTitle.Text = "     DAFTAR BON TAHANAN";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;


            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Width = 15;
            btnDetail.Name = "btnDetail";
            gvBonTahanan.AutoGenerateColumns = false;
            gvBonTahanan.Columns.Insert(0, btnDetail);
            gvBonTahanan.Refresh();
            gvBonTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            GridViewImageColumn btnUbah = new GridViewImageColumn();
            btnUbah.HeaderText = "";
            btnUbah.Name = "btnUbah";
            gvBonTahanan.AutoGenerateColumns = false;
            gvBonTahanan.Columns.Insert(1, btnUbah);
            gvBonTahanan.Refresh();
            gvBonTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnHapus = new GridViewImageColumn();
            btnHapus.HeaderText = "";
            btnHapus.Name = "btnHapus";
            gvBonTahanan.AutoGenerateColumns = false;
            gvBonTahanan.Columns.Insert(1, btnHapus);
            gvBonTahanan.Refresh();
            gvBonTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            GridViewImageColumn btnPrint = new GridViewImageColumn();
            btnPrint.HeaderText = "";
            btnPrint.Name = "btnPrint";
            gvBonTahanan.AutoGenerateColumns = false;
            gvBonTahanan.Columns.Insert(1, btnPrint);
            gvBonTahanan.Refresh();
            gvBonTahanan.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            UserFunction.LoadDataBonTahananToGrid(gvBonTahanan);
            UserFunction.SetInitGridView(gvBonTahanan);

        }

    }
}
