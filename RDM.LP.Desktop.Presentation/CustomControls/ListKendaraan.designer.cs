﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class ListKendaraan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListKendaraan));
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.gvKendaraan = new Telerik.WinControls.UI.RadGridView();
            this.lblDetail = new Telerik.WinControls.UI.RadLabel();
            this.pdf = new System.Windows.Forms.PictureBox();
            this.excel = new System.Windows.Forms.PictureBox();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.picRefresh = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.gvKendaraan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvKendaraan.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).BeginInit();
            this.lblDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRefresh)).BeginInit();
            this.SuspendLayout();
            // 
            // gvKendaraan
            // 
            this.gvKendaraan.AutoScroll = true;
            this.gvKendaraan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvKendaraan.Location = new System.Drawing.Point(0, 108);
            // 
            // 
            // 
            this.gvKendaraan.MasterTemplate.AllowAddNewRow = false;
            this.gvKendaraan.MasterTemplate.AllowCellContextMenu = false;
            this.gvKendaraan.MasterTemplate.AllowColumnChooser = false;
            this.gvKendaraan.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.gvKendaraan.MasterTemplate.AllowColumnReorder = false;
            this.gvKendaraan.MasterTemplate.AllowDragToGroup = false;
            this.gvKendaraan.MasterTemplate.EnableFiltering = true;
            this.gvKendaraan.MasterTemplate.EnablePaging = true;
            this.gvKendaraan.MasterTemplate.PageSize = 10;
            this.gvKendaraan.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gvKendaraan.Name = "gvKendaraan";
            this.gvKendaraan.Size = new System.Drawing.Size(1349, 413);
            this.gvKendaraan.TabIndex = 58;
            // 
            // lblDetail
            // 
            this.lblDetail.AutoSize = false;
            this.lblDetail.BackColor = System.Drawing.Color.Gainsboro;
            this.lblDetail.Controls.Add(this.picRefresh);
            this.lblDetail.Controls.Add(this.pdf);
            this.lblDetail.Controls.Add(this.excel);
            this.lblDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetail.Location = new System.Drawing.Point(0, 56);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDetail.Size = new System.Drawing.Size(1349, 52);
            this.lblDetail.TabIndex = 16;
            // 
            // pdf
            // 
            this.pdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pdf.Dock = System.Windows.Forms.DockStyle.Right;
            this.pdf.Image = ((System.Drawing.Image)(resources.GetObject("pdf.Image")));
            this.pdf.Location = new System.Drawing.Point(1263, 10);
            this.pdf.Name = "pdf";
            this.pdf.Size = new System.Drawing.Size(43, 32);
            this.pdf.TabIndex = 1;
            this.pdf.TabStop = false;
            // 
            // excel
            // 
            this.excel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.excel.Dock = System.Windows.Forms.DockStyle.Right;
            this.excel.Image = ((System.Drawing.Image)(resources.GetObject("excel.Image")));
            this.excel.Location = new System.Drawing.Point(1306, 10);
            this.excel.Name = "excel";
            this.excel.Size = new System.Drawing.Size(43, 32);
            this.excel.TabIndex = 0;
            this.excel.TabStop = false;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(1349, 56);
            this.lblTitle.TabIndex = 1;
            // 
            // picRefresh
            // 
            this.picRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picRefresh.Dock = System.Windows.Forms.DockStyle.Right;
            this.picRefresh.Image = ((System.Drawing.Image)(resources.GetObject("picRefresh.Image")));
            this.picRefresh.Location = new System.Drawing.Point(1220, 10);
            this.picRefresh.Name = "picRefresh";
            this.picRefresh.Size = new System.Drawing.Size(43, 32);
            this.picRefresh.TabIndex = 4;
            this.picRefresh.TabStop = false;
            // 
            // ListKendaraan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gvKendaraan);
            this.Controls.Add(this.lblDetail);
            this.Controls.Add(this.lblTitle);
            this.Name = "ListKendaraan";
            this.Size = new System.Drawing.Size(1349, 521);
            ((System.ComponentModel.ISupportInitialize)(this.gvKendaraan.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvKendaraan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetail)).EndInit();
            this.lblDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRefresh)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel lblDetail;
        private Telerik.WinControls.UI.RadGridView gvKendaraan;
        private System.Windows.Forms.PictureBox excel;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.PictureBox pdf;
        private System.Windows.Forms.PictureBox picRefresh;
    }
}
