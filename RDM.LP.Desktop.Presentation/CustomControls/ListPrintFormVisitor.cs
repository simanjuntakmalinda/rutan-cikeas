﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class ListPrintFormVisitor : Base
    {
        public RadGridView GvVisitor { get { return this.gvPrintForm; } }
        public ListPrintFormVisitor()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitEvents()
        {
            gvPrintForm.ViewCellFormatting += radGridView_ViewCellFormatting;
            gvPrintForm.RowFormatting += radGridView_RowFormatting;
            gvPrintForm.ContextMenuOpening += radGridView_ContextMenuOpening;
            gvPrintForm.CellClick += gvVisitor_CellClick;
            excel.Click += excel_Click;
            pdf.Click += pdf_Click;
        }

        private void pdf_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.PDF, DateTime.Now.ToString("yyyyMMdd") + "DaftarPengunjung.pdf", "VISITOR LIST DATA", gvPrintForm);
        }

        private void excel_Click(object sender, EventArgs e)
        {
            UserFunction.ExportGrid(ExportGridType.CSV, DateTime.Now.ToString("yyyyMMdd") + "DaftarPengunjung.csv", "VISITOR LIST DATA", gvPrintForm);
        }

        private void gvVisitor_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Column == null)
                return;
            switch (e.Column.Index)
            {
                case 0:
                    MainForm.formMain.PreviewDoc.Tag = "Visitor-" + e.Row.Cells["RegID"].Value.ToString();
                    MainForm.formMain.PreviewDoc.Show();
                    break;
            }
            
        }


        private void InitForm()
        {
            this.lblTitle.Text = "     VISITOR FORM PRINT";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.BackColor = Global.MainColor;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblDetail.LabelElement.CustomFont = Global.MainFont;
            this.lblDetail.LabelElement.CustomFontSize = Global.CustomFontSizeMain;


            GridViewImageColumn btnDetail = new GridViewImageColumn();
            btnDetail.HeaderText = "";
            btnDetail.Width = 15;
            btnDetail.Name = "btnDetail";
            gvPrintForm.AutoGenerateColumns = false;
            gvPrintForm.Columns.Insert(0, btnDetail);
            gvPrintForm.Refresh();
            gvPrintForm.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            UserFunction.LoadDataPrintVisitorToGrid(gvPrintForm);
            UserFunction.SetInitGridView(gvPrintForm);

        }

    }
}
