﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using System.Threading;
using System.IO;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class VisitorVIPDetail : Base
    {
        private Point MouseDownLocation;
        public VisitorVIPDetail()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void LoadData(string id)
        {            
            VisitorVIPService pegunjung = new VisitorVIPService();
            var data = pegunjung.GetDetailById(id);
            if (data != null)
            {
                identitas.Text = data.IDType + " / " + data.IDNo;
                namalengkap.Text = data.FullName;
                alamat.Text = data.Address;
                tujuan.Text = data.Purpose;
                catatan.Text = data.Notes;
                checkin.Text = data.StartVisit.Value.ToString("dd MMMM yyyy HH:mm:ss", new System.Globalization.CultureInfo("id-ID"));
                checkout.Text = data.EndVisit == null ? "-" : data.EndVisit.Value.ToString("dd MMMM yyyy HH:mm:ss", new System.Globalization.CultureInfo("id-ID"));
                inputter.Text = data.CreatedBy;
                inputdate.Text = data.CreatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss", new System.Globalization.CultureInfo("id-ID"));
                updater.Text = data.UpdatedBy;
                updatedate.Text = data.UpdatedDate == null ? string.Empty : data.UpdatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss", new System.Globalization.CultureInfo("id-ID"));
                
            }
        }

        private void InitForm()
        {
            ToolTip toolTip = new ToolTip();
            toolTip.ToolTipIcon = ToolTipIcon.Info;
            toolTip.ToolTipTitle = "Info";
            toolTip.SetToolTip(exit, "Tutup Profil Pengunjung VIP");
            lblButton.BackColor = Global.MainColor;
            lblTitle.BackColor = Global.MainColor;
            lblTitle.Text = "      PROFIL PENGUNJUNG VIP";
            lblTitle.ForeColor = Color.White;
            lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;
            lblTitle.LabelElement.CustomFont = Global.MainFont;
            lblTitle.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            btnClose.ButtonElement.CustomFontStyle = FontStyle.Bold;
            btnClose.ButtonElement.CustomFont = Global.MainFont;
            btnClose.ButtonElement.CustomFontSize = Global.CustomFontSizeMain;
            
            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
        }

        private void InitEvents()
        {
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            exit.Click += exit_Click;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            this.VisibleChanged += Form_VisibleChanged;
            btnClose.Click += btnClose_Click;
            closefull.Click += closefull_Click;
        }

        private void closefull_Click(object sender, EventArgs e)
        {
            fullPic.Image = null;
            fullpicPanel.Hide();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (this.Tag != null)
                {
                    LoadData(this.Tag.ToString());
                    fullpicPanel.Location =
                    new Point(this.Width / 2 - fullpicPanel.Size.Width / 2,
                      this.Height / 2 - fullpicPanel.Size.Height / 2);
                }
                else
                    this.Hide();
            }
        }
    }
}
