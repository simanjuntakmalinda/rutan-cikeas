﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.DataAccess.Helper;
using Microsoft.VisualBasic;
using System.IO;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormJadwalKunjungan : Base
    {
        public RadLabel TahananId { get { return this.lblTahananId; } set { this.lblTahananId = value; } }
        public RadTextBox NamaTahanan { get { return this.txtNamaTahanan; } set { this.txtNamaTahanan = value; } }
        public RadLabel NoIdentitas { get { return this.lblNoID; } set { this.lblNoID = value; } }
        public RadLabel NoSelTahanan { get { return this.lblNoSell; } set { this.lblNoSell = value; } }
        public PictureBox PicInmates1 { get { return this.picCanvas1; } set { } }
        public PictureBox PicInmates2 { get { return this.picCanvas2; } set { } }
        public PictureBox PicInmates3 { get { return this.picCanvas3; } set { } }
        public PictureBox PicFingerPrint { get { return this.picCanvas4; } set { } }

        private Point MouseDownLocation;
        private JadwalKunjungan OldData;
        public FormJadwalKunjungan()
        {
            InitializeComponent();
            InitForm();
            LoadData();
            InitEvents();
        }

        class ComboItem
        {
            public int ID { get; set; }
            public string Text { get; set; }
        }

        public void LoadData()
        {
            this.lblTitle.Text = "     TAMBAH DATA JADWAL KUNJUNGAN BARU";

            ClearForm();

            InmatesService tahanan = new InmatesService();
            
            rbYa.CheckState = CheckState.Unchecked;
            rbTidak.CheckState = CheckState.Unchecked;

            ddlJumlahKunjungan.Enabled = false;
            ddlWaktuKunjungan.Enabled = false;

            PicInmates1.Image = null;
            PicInmates2.Image = null;
            PicInmates3.Image = null;
            PicFingerPrint.Image = null;

            if (this.Tag != null)
            {
                JadwalKunjunganService jkserv = new JadwalKunjunganService();
                var data = jkserv.GetById(Convert.ToInt32(this.Tag));
                if (data == null)
                {
                    ddlJumlahKunjungan.SelectedIndex = 0;
                    ddlJumlahKunjungan.Enabled = true;
                    return;
                }
                else
                {
                    MainForm.formMain.AddNew.Text = "UBAH DATA JADWAL KUNJUNGAN";
                    this.lblTitle.Text = "     UBAH DATA JADWAL KUNJUNGAN";
                    
                    var imgList = tahanan.GetImage(data.InmatesRegID);
                    var fingerSource = tahanan.GetFingerPrint(data.InmatesRegID);

                    OldData = data;
                    TahananId.Text = data.InmatesRegID.ToUpper();
                    NamaTahanan.Text = data.InmatesName.ToUpper();
                    NoIdentitas.Text = data.NoIdentitas.ToUpper();
                    NoSelTahanan.Text = data.NoSelTahanan.ToUpper();

                    if (data.BolehDikunjungi == true)
                    {
                        rbYa.CheckState = CheckState.Checked;

                    }
                    else
                    {
                        rbTidak.CheckState = CheckState.Checked;
                    }

                    if (data.PeriodeKunjungan != String.Empty)
                    {
                        ddlJumlahKunjungan.SelectedIndex = ddlJumlahKunjungan.Items.FirstOrDefault(x => x.Text == data.PeriodeKunjungan).Index;
                        ddlJumlahKunjungan_SelectedIndexChanged(ddlJumlahKunjungan, EventArgs.Empty);
                        LoadWaktuKunjungan(data);
                    }

                    DisplayInmatesPhoto(imgList);
                    DisplayFingerPrint(fingerSource);

                }
            }
        }

        private void LoadWaktuKunjungan(JadwalKunjungan data)
        {
            var waktuKunjungan = data.WaktuKunjungan.Split(';');

            foreach (var _data in waktuKunjungan)
            {
                foreach (var _item in ddlWaktuKunjungan.Items)
                {
                    if (((RadCheckedListDataItem)_item).Text == _data)
                    {
                        ((RadCheckedListDataItem)_item).Checked = true;
                    }
                }
            }
        }

        private void ClearForm()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            TahananId.Text = null;
            NamaTahanan.Text = null;
            NoSelTahanan.Text = null;
            NoIdentitas.Text = null;
            NamaTahanan.Enabled = false;

            //ddl Zona Waktu
            ddlJumlahKunjungan.Items.Clear();
            ddlJumlahKunjungan.Items.Add("PER MINGGU");
            ddlJumlahKunjungan.Items.Add("PER BULAN");

            //ddl Zona Waktu
            ddlWaktuKunjungan.Items.Clear();

            rbYa.CheckState = CheckState.Unchecked;
            rbTidak.CheckState = CheckState.Unchecked;

        }

        private void InitEvents()
        {
            //Events
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            ddlJumlahKunjungan.SelectedIndexChanged += ddlJumlahKunjungan_SelectedIndexChanged;
            rbYa.CheckStateChanged += CheckStateChanged_click;
            rbTidak.CheckStateChanged += CheckStateChanged_click;
            ddlWaktuKunjungan.KeyDown += Control_KeyDown;
            ddlJumlahKunjungan.KeyDown += Control_KeyDown;
            btnSearchTahanan.Click += btnSearchTahanan_Click;
            btnSearchTahanan.KeyDown += Control_KeyDown;
            txtNamaTahanan.KeyDown += Control_KeyDown;
            
            radPanel1.KeyDown += Control_KeyDown;
            radPanel2.KeyDown += Control_KeyDown;
            radPanel3.KeyDown += Control_KeyDown;
            radPanel4.KeyDown += Control_KeyDown;
            radPanel5.KeyDown += Control_KeyDown;
            radPanel7.KeyDown += Control_KeyDown;
            radLabel3.KeyDown += Control_KeyDown;
        }

        private void btnSearchTahanan_Click(object sender, EventArgs e)
        {
            MainForm.formMain.SearchData.NamaForm = "FormJadwalKunjungan";
            MainForm.formMain.SearchData.Tag = SearchType.Tahanan.ToString();
            MainForm.formMain.SearchData.Show();
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }

        public void Tersangka_Changed()
        {
            InmatesService tahanan = new InmatesService();
            var regid = TahananId.Text;
            var dataimage = tahanan.GetImage(regid);
            var fingerSource = tahanan.GetFingerPrint(regid);
            lblRegID.Text = regid;

            if (dataimage.Count() != 0)
                DisplayInmatesPhoto(dataimage);
            else
            {
                MainForm.formMain.JadwalKunjungan.PicInmates1.Image = null;
                MainForm.formMain.JadwalKunjungan.PicInmates2.Image = null;
                MainForm.formMain.JadwalKunjungan.PicInmates3.Image = null;
            }

            if (fingerSource.Count() != 0)
                DisplayFingerPrint(fingerSource);
            else
                picCanvas4.Image = null;
        }

        private void ddlJumlahKunjungan_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ddl Waktu Kunjungan
            ddlWaktuKunjungan.Items.Clear();
            var intensitas = ddlJumlahKunjungan.SelectedItem;//.ToString();
            if (intensitas == null)
            {
                txtWaktuKunjungan.Text = "WAKTU KUNJUNGAN";
                ddlWaktuKunjungan.Items.Clear();
                ddlWaktuKunjungan.Enabled = false;
            }
            else
            {
                ddlWaktuKunjungan.Enabled = true;

                if (intensitas.ToString() == "PER MINGGU")
                {
                    txtWaktuKunjungan.Text = "HARI KUNJUNGAN";

                    ddlWaktuKunjungan.Items.Add("SENIN");
                    ddlWaktuKunjungan.Items.Add("SELASA");
                    ddlWaktuKunjungan.Items.Add("RABU");
                    ddlWaktuKunjungan.Items.Add("KAMIS");
                    ddlWaktuKunjungan.Items.Add("JUMAT");
                    ddlWaktuKunjungan.Items.Add("SABTU");
                    ddlWaktuKunjungan.Items.Add("MINGGU");
                }
                else if (intensitas.ToString() == "PER BULAN")
                {
                    txtWaktuKunjungan.Text = "TANGGAL KUNJUNGAN";
                    for (int i = 1; i <= 31; i++)
                    {
                        ddlWaktuKunjungan.Items.Add(i.ToString());
                    }
                }
            }
        }

        private void CheckStateChanged_click(object sender, EventArgs e)
        {
            if (((RadRadioButton)sender).CheckState == CheckState.Checked)
            {
                switch (((RadRadioButton)sender).Name)
                {
                    case "rbYa":
                        ddlJumlahKunjungan.Items.Clear();
                        ddlJumlahKunjungan.Items.Add("PER MINGGU");
                        ddlJumlahKunjungan.Items.Add("PER BULAN");
                        ddlJumlahKunjungan.Enabled = true;

                        ddlWaktuKunjungan.Items.Clear();
                        ddlWaktuKunjungan.Enabled = false;
                        break;
                    case "rbTidak":
                        ddlJumlahKunjungan.Items.Clear();
                        ddlJumlahKunjungan.Enabled = false;
                        ddlWaktuKunjungan.Items.Clear();
                        ddlWaktuKunjungan.Enabled = false;
                        break;
                }
            }
        }

        private void DisplayFingerPrint(IEnumerable<FingerPrint> fingerSource)
        {
            foreach (var item in fingerSource)
            {
                var imageBytes = (byte[])Convert.FromBase64String(item.ImagePrint);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);

                if (item.FingerId == "R1")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, picCanvas4.Width, picCanvas4.Height);
                    /* Clear any existing image in the PictureBox. */
                    picCanvas4.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    picCanvas4.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    picCanvas4.Image = imgOutput;
                }
            }
        }

        private void DisplayInmatesPhoto(IEnumerable<Photos> datatemp)
        {
            foreach (var item in datatemp)
            {
                var imageBytes = (byte[])Convert.FromBase64String(item.Data);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);
                if (item.Note.ToUpper() == "FRONT")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, MainForm.formMain.CellAllocation.PicInmates1.Width, MainForm.formMain.CellAllocation.PicInmates1.Height);
                    /* Clear any existing image in the PictureBox. */
                    MainForm.formMain.JadwalKunjungan.PicInmates1.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    MainForm.formMain.JadwalKunjungan.PicInmates1.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    MainForm.formMain.JadwalKunjungan.PicInmates1.Image = imgOutput;

                }
                else if (item.Note.ToUpper() == "LEFT")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, MainForm.formMain.CellAllocation.PicInmates2.Width, MainForm.formMain.CellAllocation.PicInmates2.Height);
                    /* Clear any existing image in the PictureBox. */
                    MainForm.formMain.JadwalKunjungan.PicInmates2.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    MainForm.formMain.JadwalKunjungan.PicInmates2.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    MainForm.formMain.JadwalKunjungan.PicInmates2.Image = imgOutput;
                }
                else if (item.Note.ToUpper() == "RIGHT")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, MainForm.formMain.CellAllocation.PicInmates3.Width, MainForm.formMain.CellAllocation.PicInmates3.Height);
                    /* Clear any existing image in the PictureBox. */
                    MainForm.formMain.JadwalKunjungan.PicInmates3.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    MainForm.formMain.JadwalKunjungan.PicInmates3.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    MainForm.formMain.JadwalKunjungan.PicInmates3.Image = imgOutput;
                }

            }
        }

        private Size getScaledImageDimensions(int width1, int height1, int width2, int height2)
        {
            /*Determine if Image is Portrait or Landscape. */
            double scaleImageMultiplier = 0;
            if (width1 > height1)    /* Image is Portrait */
            {
                /* Calculate the multiplier based on the heights. */
                if (height2 > width2)
                {
                    scaleImageMultiplier = (double)width2 / (double)width1;
                }

                else
                {
                    scaleImageMultiplier = (double)height2 / (double)height1;
                }
            }

            else /* Image is Landscape */
            {
                /* Calculate the multiplier based on the widths. */
                if (height2 >= width2)
                {
                    scaleImageMultiplier = (double)width2 / (double)width1;
                }

                else
                {
                    scaleImageMultiplier = (double)height2 / (double)height1;
                }
            }

            /* Generate and return the new scaled dimensions.

            * Essentially, we multiply each dimension of the original image
            * by the multiplier calculated above to yield the dimensions
            * of the scaled image. The scaled image can be larger or smaller
            * than the original.
            */
            return new Size((int)(width1 * scaleImageMultiplier), (int)(height1 * scaleImageMultiplier));
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                if (this.Tag == null)
                    InsertData();
                else
                    UpdatedData();

                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Disimpan !");
                UserFunction.LoadDataJadwalKunjunganToGrid(MainForm.formMain.ListJadwalKunjungan.GvJadwalKunjungan);
                MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
                MainForm.formMain.AddNew.Text = "TAMBAH DATA JADWAL KUNJUNGAN BARU";
                this.lblTitle.Text = "     TAMBAH DATA JADWAL KUNJUNGAN BARU";

                this.Tag = null;
                picCanvas1.Image = null;
                picCanvas2.Image = null;
                picCanvas3.Image = null;
                picCanvas4.Image = null;
                this.LoadData();
            }
        }

        private void UpdatedData()
        {
            JadwalKunjunganService jadwalserv = new JadwalKunjunganService();
            var jadwal = new JadwalKunjungan
            {
                Id = Convert.ToInt32(this.Tag),
                InmatesRegID = TahananId.Text,
                InmatesName = NamaTahanan.Text,
                NoIdentitas = NoIdentitas.Text,
                NoSelTahanan = NoSelTahanan.Text,
                BolehDikunjungi = rbYa.IsChecked == true ? true : false,
                PeriodeKunjungan = ddlJumlahKunjungan.Text,
                WaktuKunjungan = ddlWaktuKunjungan.Text,
                UpdatedDate = DateTime.Now.ToLocalTime(),
                UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
            };
            jadwalserv.Update(jadwal);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERCELL.ToString(), Activities = "Update JadwalKunjungan, Old Data=" + UserFunction.JsonString(OldData) + ",  New Data=" + UserFunction.JsonString(jadwal) });
        }

        private void InsertData()
        {
            JadwalKunjunganService jadwalserv = new JadwalKunjunganService();
            var jadwal = new JadwalKunjungan
            {
                InmatesRegID = TahananId.Text,
                InmatesName = NamaTahanan.Text,
                NoIdentitas = NoIdentitas.Text,
                NoSelTahanan = NoSelTahanan.Text,
                BolehDikunjungi = rbYa.IsChecked == true ? true : false,
                PeriodeKunjungan = ddlJumlahKunjungan.Text,
                WaktuKunjungan = ddlWaktuKunjungan.Text,
                CreatedDate = DateTime.Now.ToLocalTime(),
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
            };
            jadwalserv.Post(jadwal);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERCELL.ToString(), Activities = "Add New JadwalKunjungan, Data=" + UserFunction.JsonString(jadwal) });
        }

        private void InitForm()
        {
            this.btnSearchTahanan.RootElement.EnableElementShadow = false;
            this.lblError.Visible = false;
            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     TAMBAH JADWAL KUNJUNGAN BARU";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;
            this.lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 15.5f;

            this.lblRequired.LabelElement.CustomFont = Global.MainFont;
            this.lblRequired.LabelElement.CustomFontSize = 14f;
            this.lblRequired.ForeColor = Color.White;
            this.lblRequired.BackColor = Color.FromArgb(77, 77, 77);

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);

            ddlJumlahKunjungan.BackColor = Color.Gainsboro;
            ddlWaktuKunjungan.BackColor = Color.Gainsboro;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ClearForm();
            picCanvas1.Image = null;
            picCanvas2.Image = null;
            picCanvas3.Image = null;
            picCanvas4.Image = null;
            this.Tag = null;
            MainForm.formMain.AddNew.Text = "TAMBAH DATA JADWAL KUNJUNGAN BARU";
            this.lblTitle.Text = "     TAMBAH DATA JADWAL KUNJUNGAN BARU";
            UserFunction.ClearControls(tableLayoutPanel1);
            MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
        }

        private bool DataValid()
        {
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;
            if (this.txtNamaTahanan.Text == null)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Nama Tahanan!";
                return false;
            }
            if (this.rbYa.IsChecked == false && this.rbTidak.IsChecked == false)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Memilih Apakah Tahanan Boleh Dikunjungi Atau Tidak!";
                return false;
            }
            if (this.rbYa.IsChecked == true && (string)this.ddlJumlahKunjungan.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Intensitas Kunjungan!";
                return false;
            }
            if (this.rbYa.IsChecked == true && (string)this.ddlWaktuKunjungan.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Waktu Kunjungan!";
                return false;
            }
            return true;
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }
    }
}
