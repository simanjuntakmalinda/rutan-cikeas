﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.DataAccess.Helper;
using Microsoft.VisualBasic;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormKendaraan : Base
    {
        public RadDropDownList DDLKategori { get { return this.ddlKategoriKendaraan; } }
        private KendaraanTahanan OldData;
        public FormKendaraan()
        {
            InitializeComponent();
            InitForm();
            LoadData();
            InitEvents();
        }

        class ComboItem
        {
            public int ID { get; set; }
            public string Text { get; set; }
        }

        public void LoadData()
        {
            this.lblTitle.Text = "     TAMBAH DATA KENDARAAN BARU";

            ClearForm();

            if (this.Tag != null && this.Tag != string.Empty)
            {
                KendaraanTahananService kenserv = new KendaraanTahananService();
                var data = kenserv.GetById(Convert.ToInt32(this.Tag));
                if (data == null)
                {
                    ddlKategoriKendaraan.SelectedIndex = 0;
                    ddlKategoriKendaraan.Enabled = true;
                    
                    return;
                }
                else
                {
                    MainForm.formMain.AddNew.Text = "UBAH DATA KENDARAAN";
                    this.lblTitle.Text = "     UBAH DATA KENDARAAN";
                    OldData = data;
                    txtNoPlat.Text = data.NoPlat;
                    txtPengemudi.Text = data.Pengemudi;

                    ddlKategoriKendaraan.SelectedIndex = 0;
                    ddlKategoriKendaraan.Enabled = true;
                    ddlKategoriKendaraan.SelectedValue = data.KategoriKendaraan;

                    dtMasuk.Value = data.TanggalMasuk;
                    tpMasuk.Value = data.WaktuMasuk;
                    txtKeperluan.Text = data.Keperluan;
                    txtCatatan.Text = data.Catatan;
                }
            }
        }

        private void ClearForm()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            //ddl Zona Waktu
            ddlZonaWaktu.Items.Clear();
            ddlZonaWaktu.Items.Add("WIB");
            ddlZonaWaktu.Items.Add("WIT");
            ddlZonaWaktu.Items.Add("WITA");
            ddlZonaWaktu.SelectedIndex = 0;

            //ddl kendaraan
            KendaraanService buildingserv = new KendaraanService();
            ddlKategoriKendaraan.DisplayMember = "Nama";
            ddlKategoriKendaraan.ValueMember = "Id";
            ddlKategoriKendaraan.DataSource = buildingserv.Get();

            txtNoPlat.Text = string.Empty;
            txtPengemudi.Text = string.Empty;

            dtMasuk.Value = DateTime.Now;
            tpMasuk.Value = DateTime.Now;
            txtCatatan.Text = null;

            txtKeperluan.Text = null;
        }

        private void InitEvents()
        {
            //Events
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            btnAddKategori.Click += BtnAddKategori_Click;

            ddlKategoriKendaraan.KeyDown += Control_KeyDown;
            btnAddKategori.KeyDown += Control_KeyDown;
            txtNoPlat.KeyDown += Control_KeyDown;
            txtPengemudi.KeyDown += Control_KeyDown;
            dtMasuk.KeyDown += Control_KeyDown;
            tpMasuk.KeyDown += Control_KeyDown;
            ddlZonaWaktu.KeyDown += Control_KeyDown;
            txtKeperluan.KeyDown += Control_KeyDown;
            txtCatatan.KeyDown += Control_KeyDown;
            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }

        private void BtnAddKategori_Click(object sender, EventArgs e)
        {
            kategoriKendaraan.Show();
            kategoriKendaraan.Location = new Point(ClientSize.Width / 2 - kategoriKendaraan.Size.Width / 2,
              ClientSize.Height / 2 - kategoriKendaraan.Size.Height / 2);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                if (this.Tag == null)
                    SaveCell();
                else
                    UpdatedData();

                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Disimpan !");
                UserFunction.LoadDataKendaraanToGrid(MainForm.formMain.ListKendaraan.GvCell);
                MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
                MainForm.formMain.AddNew.Text = "TAMBAH DATA KENDARAAN";
                this.lblTitle.Text = "     TAMBAH DATA KENDARAAN BARU";
                this.Tag = null;
            }
        }

        private void SaveCell()
        {
            if (this.Tag == null)
                InsertData();
            else
                UpdatedData();
        }

        private void UpdatedData()
        {
            KendaraanTahananService cellserv = new KendaraanTahananService();
            var kend = new KendaraanTahanan
            {
                Id = Convert.ToInt32(this.Tag),
                Pengemudi = txtPengemudi.Text.ToUpper(),
                NoPlat = txtNoPlat.Text.ToUpper(),
                KategoriKendaraan = (int)ddlKategoriKendaraan.SelectedItem.Value,
                Keperluan = txtKeperluan.Text,
                Catatan = txtCatatan.Text.ToUpper(),
                WaktuMasuk = tpMasuk.Value,
                TanggalMasuk = dtMasuk.Value,
                UpdatedDate = DateTime.Now.ToLocalTime(),
                UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
            };
            cellserv.Update(kend);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.KENDARAANTAHANAN.ToString(), Activities = "Update Kendaraan Tahanan, Old Data=" + UserFunction.JsonString(OldData) + ",  New Data=" + UserFunction.JsonString(kend) });
        }

        private void InsertData()
        {
            KendaraanTahananService cellserv = new KendaraanTahananService();
            var kend = new KendaraanTahanan
            {
                NoPlat = txtNoPlat.Text.ToUpper(),
                Pengemudi = txtPengemudi.Text.ToUpper(),
                KategoriKendaraan = (int)ddlKategoriKendaraan.SelectedValue,
                Keperluan = txtKeperluan.Text.ToUpper(),
                Catatan = txtCatatan.Text.ToUpper(),
                TanggalMasuk = dtMasuk.Value,
                WaktuMasuk = tpMasuk.Value,
                ZonaWaktuMasuk = ddlZonaWaktu.Text,
                WaktuKeluar = null,
                CreatedDate = DateTime.Now.ToLocalTime(),
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
            };
            cellserv.Post(kend);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.KENDARAANTAHANAN.ToString(), Activities = "Add New Kendaraan Tahanan, Data=" + UserFunction.JsonString(kend) });
        }

        private void InitForm()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     TAMBAH KENDARAAN";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;
            this.lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 15.5f;

            this.lblRequired.LabelElement.CustomFont = Global.MainFont;
            this.lblRequired.LabelElement.CustomFontSize = 14f;
            this.lblRequired.ForeColor = Color.White;
            this.lblRequired.BackColor = Color.FromArgb(77, 77, 77);

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            ddlZonaWaktu.Hide();

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
            ddlKategoriKendaraan.SelectedIndex = 0;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ClearForm();
            this.Tag = null;
            MainForm.formMain.AddNew.Text = "TAMBAH DATA KENDARAAN";
            this.lblTitle.Text = "     TAMBAH DATA KENDARAAN";
            UserFunction.ClearControls(tableLayoutPanel1);
            MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
        }

        private bool DataValid()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            if ((string)this.txtNoPlat.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Nomor Plat!";
                return false;
            }
            if (this.txtPengemudi.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Nama Pengemudi!";
                return false;
            }
            if (this.ddlKategoriKendaraan.SelectedItem.Value == null)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Kategori Kendaraan!";
                return false;
            }
            if (this.dtMasuk.Value == null)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi tanggal masuk!";
                return false;
            }
            if (this.tpMasuk.Value == null)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Waktu Masuk!";
                return false;
            }
            if (this.txtKeperluan.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Keperluan!";
                return false;
            }
            return true;
        }
    }
}
