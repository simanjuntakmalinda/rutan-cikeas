﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class FormCellAllocation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCellAllocation));
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnBack = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            this.headerPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel8 = new Telerik.WinControls.UI.RadPanel();
            this.DateEnter = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radPanel7 = new Telerik.WinControls.UI.RadPanel();
            this.txtRoommate = new Telerik.WinControls.UI.RadLabel();
            this.radPanel5 = new Telerik.WinControls.UI.RadPanel();
            this.txtBuilFloor = new Telerik.WinControls.UI.RadLabel();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.ddlCellCode = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.txtRegCode = new Telerik.WinControls.UI.RadDropDownList();
            this.lblLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.lblLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.lblLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.lblradLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.lblradLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailSurat = new Telerik.WinControls.UI.RadLabel();
            this.txtDate = new Telerik.WinControls.UI.RadLabel();
            this.lblLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.lblLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.lblLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.lblLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.lblLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.lblLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.lblLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.lblradLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.lblradLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.lblradLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.txtNama = new Telerik.WinControls.UI.RadLabel();
            this.txtReligion = new Telerik.WinControls.UI.RadLabel();
            this.txtGender = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailInmate = new Telerik.WinControls.UI.RadLabel();
            this.txtIdentity = new Telerik.WinControls.UI.RadLabel();
            this.txtCategory = new Telerik.WinControls.UI.RadLabel();
            this.txtType = new Telerik.WinControls.UI.RadLabel();
            this.txtNetwork = new Telerik.WinControls.UI.RadLabel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.lblphoto = new Telerik.WinControls.UI.RadLabel();
            this.lblrightthumb = new Telerik.WinControls.UI.RadLabel();
            this.lblleft = new Telerik.WinControls.UI.RadLabel();
            this.lblright = new Telerik.WinControls.UI.RadLabel();
            this.lblfront = new Telerik.WinControls.UI.RadLabel();
            this.lblRegID = new Telerik.WinControls.UI.RadLabel();
            this.picCanvas4 = new System.Windows.Forms.PictureBox();
            this.picCanvas3 = new System.Windows.Forms.PictureBox();
            this.picCanvas2 = new System.Windows.Forms.PictureBox();
            this.picCanvas1 = new System.Windows.Forms.PictureBox();
            this.PanelRb = new Telerik.WinControls.UI.RadPanel();
            this.txtSingleCell = new Telerik.WinControls.UI.RadRadioButton();
            this.txtDoubleCell = new Telerik.WinControls.UI.RadRadioButton();
            this.radPanel12 = new Telerik.WinControls.UI.RadPanel();
            this.txtTotalInused = new Telerik.WinControls.UI.RadLabel();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.ddlCellAllocationStatus = new Telerik.WinControls.UI.RadDropDownList();
            this.searchData1 = new RDM.LP.Desktop.Presentation.CustomControls.InmatesSearchData();
            this.PanelEror = new Telerik.WinControls.UI.RadPanel();
            this.radPanelError = new Telerik.WinControls.UI.RadPanel();
            this.lblError = new Telerik.WinControls.UI.RadLabel();
            this.fullpicPanel = new Telerik.WinControls.UI.RadPanel();
            this.closefull = new System.Windows.Forms.PictureBox();
            this.fullPic = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).BeginInit();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).BeginInit();
            this.radPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DateEnter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).BeginInit();
            this.radPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRoommate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).BeginInit();
            this.radPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBuilFloor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            this.radPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlCellCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblradLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblradLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblradLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblradLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblradLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReligion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailInmate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNetwork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblphoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblrightthumb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblleft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblright)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblfront)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelRb)).BeginInit();
            this.PanelRb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSingleCell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDoubleCell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel12)).BeginInit();
            this.radPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalInused)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlCellAllocationStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelEror)).BeginInit();
            this.PanelEror.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).BeginInit();
            this.radPanelError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullpicPanel)).BeginInit();
            this.fullpicPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closefull)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullPic)).BeginInit();
            this.SuspendLayout();
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnBack);
            this.lblButton.Controls.Add(this.btnSave);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 707);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(1361, 122);
            this.lblButton.TabIndex = 9;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.Location = new System.Drawing.Point(30, 30);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(244, 62);
            this.btnBack.TabIndex = 10;
            this.btnBack.Tag = "";
            this.btnBack.Text = "&BATAL";
            this.btnBack.ThemeName = "MaterialBlueGrey";
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.if_checked_2_309094;
            this.btnSave.Location = new System.Drawing.Point(1026, 30);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(305, 62);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "&SIMPAN";
            this.btnSave.ThemeName = "MaterialBlueGrey";
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.lblTitle);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Margin = new System.Windows.Forms.Padding(4);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1361, 52);
            this.headerPanel.TabIndex = 508;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(1093, 56);
            this.lblTitle.TabIndex = 8;
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.HorizontalScrollBarState = Telerik.WinControls.UI.ScrollState.AlwaysHide;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 52);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(1342, 542);
            this.radScrollablePanel1.Size = new System.Drawing.Size(1361, 544);
            this.radScrollablePanel1.TabIndex = 510;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.0903F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.243032F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.42809F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.94737F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.29825F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 470F));
            this.tableLayoutPanel1.Controls.Add(this.radPanel8, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.radPanel7, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.radPanel5, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.radPanel4, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblLabel3, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblLabel1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblLabel14, 3, 14);
            this.tableLayoutPanel1.Controls.Add(this.lblradLabel13, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblradLabel11, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailSurat, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtDate, 1, 17);
            this.tableLayoutPanel1.Controls.Add(this.lblLabel9, 3, 17);
            this.tableLayoutPanel1.Controls.Add(this.lblLabel8, 3, 16);
            this.tableLayoutPanel1.Controls.Add(this.lblLabel7, 3, 15);
            this.tableLayoutPanel1.Controls.Add(this.lblLabel6, 0, 17);
            this.tableLayoutPanel1.Controls.Add(this.lblLabel5, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.lblLabel2, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.lblLabel4, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.lblradLabel24, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblradLabel1, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblradLabel3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtNama, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.txtReligion, 1, 15);
            this.tableLayoutPanel1.Controls.Add(this.txtGender, 1, 16);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailInmate, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.txtIdentity, 4, 14);
            this.tableLayoutPanel1.Controls.Add(this.txtCategory, 4, 15);
            this.tableLayoutPanel1.Controls.Add(this.txtType, 4, 16);
            this.tableLayoutPanel1.Controls.Add(this.txtNetwork, 4, 17);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.PanelRb, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.radPanel12, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 1, 7);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 19;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1342, 865);
            this.tableLayoutPanel1.TabIndex = 67;
            // 
            // radPanel8
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel8, 3);
            this.radPanel8.Controls.Add(this.DateEnter);
            this.radPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel8.Location = new System.Drawing.Point(243, 321);
            this.radPanel8.Name = "radPanel8";
            this.radPanel8.Size = new System.Drawing.Size(450, 47);
            this.radPanel8.TabIndex = 7;
            // 
            // DateEnter
            // 
            this.DateEnter.AutoSize = false;
            this.DateEnter.BackColor = System.Drawing.Color.Transparent;
            this.DateEnter.CalendarSize = new System.Drawing.Size(290, 320);
            this.DateEnter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DateEnter.Location = new System.Drawing.Point(0, 0);
            this.DateEnter.Name = "DateEnter";
            this.DateEnter.Size = new System.Drawing.Size(450, 47);
            this.DateEnter.TabIndex = 7;
            this.DateEnter.TabStop = false;
            this.DateEnter.Value = new System.DateTime(((long)(0)));
            // 
            // radPanel7
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel7, 2);
            this.radPanel7.Controls.Add(this.txtRoommate);
            this.radPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel7.Location = new System.Drawing.Point(296, 271);
            this.radPanel7.Name = "radPanel7";
            this.radPanel7.Size = new System.Drawing.Size(397, 44);
            this.radPanel7.TabIndex = 6;
            // 
            // txtRoommate
            // 
            this.txtRoommate.AutoSize = false;
            this.txtRoommate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtRoommate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRoommate.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtRoommate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtRoommate.Location = new System.Drawing.Point(0, 0);
            this.txtRoommate.Name = "txtRoommate";
            this.txtRoommate.Size = new System.Drawing.Size(397, 44);
            this.txtRoommate.TabIndex = 6;
            this.txtRoommate.MouseLeave += new System.EventHandler(this.txtRoommate_MouseLeave);
            this.txtRoommate.MouseHover += new System.EventHandler(this.txtRoommate_MouseHover);
            // 
            // radPanel5
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel5, 3);
            this.radPanel5.Controls.Add(this.txtBuilFloor);
            this.radPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel5.Location = new System.Drawing.Point(243, 221);
            this.radPanel5.Name = "radPanel5";
            this.radPanel5.Size = new System.Drawing.Size(450, 44);
            this.radPanel5.TabIndex = 4;
            // 
            // txtBuilFloor
            // 
            this.txtBuilFloor.AutoSize = false;
            this.txtBuilFloor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBuilFloor.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtBuilFloor.Location = new System.Drawing.Point(0, 0);
            this.txtBuilFloor.Name = "txtBuilFloor";
            this.txtBuilFloor.Size = new System.Drawing.Size(450, 44);
            this.txtBuilFloor.TabIndex = 4;
            // 
            // radPanel4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel4, 3);
            this.radPanel4.Controls.Add(this.ddlCellCode);
            this.radPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel4.Location = new System.Drawing.Point(243, 171);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(450, 44);
            this.radPanel4.TabIndex = 3;
            // 
            // ddlCellCode
            // 
            this.ddlCellCode.AutoSize = false;
            this.ddlCellCode.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ddlCellCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlCellCode.DropDownHeight = 300;
            this.ddlCellCode.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem3.Tag = "1";
            radListDataItem3.Text = "User";
            radListDataItem4.Tag = "2";
            radListDataItem4.Text = "Superuser";
            this.ddlCellCode.Items.Add(radListDataItem3);
            this.ddlCellCode.Items.Add(radListDataItem4);
            this.ddlCellCode.Location = new System.Drawing.Point(0, 0);
            this.ddlCellCode.Name = "ddlCellCode";
            this.ddlCellCode.Size = new System.Drawing.Size(450, 44);
            this.ddlCellCode.TabIndex = 3;
            // 
            // radPanel2
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel2, 3);
            this.radPanel2.Controls.Add(this.txtRegCode);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(243, 71);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(450, 44);
            this.radPanel2.TabIndex = 1;
            // 
            // txtRegCode
            // 
            this.txtRegCode.AutoSize = false;
            this.txtRegCode.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtRegCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRegCode.DropDownHeight = 300;
            this.txtRegCode.Location = new System.Drawing.Point(0, 0);
            this.txtRegCode.Name = "txtRegCode";
            this.txtRegCode.Size = new System.Drawing.Size(450, 44);
            this.txtRegCode.TabIndex = 1;
            // 
            // lblLabel3
            // 
            this.lblLabel3.AutoSize = false;
            this.lblLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLabel3.Location = new System.Drawing.Point(14, 375);
            this.lblLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.lblLabel3.Name = "lblLabel3";
            this.lblLabel3.Size = new System.Drawing.Size(222, 45);
            this.lblLabel3.TabIndex = 527;
            this.lblLabel3.Text = "<html><span style=\"color: #ff0000\"><span style=\"color: #000000\">STATUS</span>*</s" +
    "pan></html>";
            // 
            // lblLabel1
            // 
            this.lblLabel1.AutoSize = false;
            this.lblLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLabel1.Location = new System.Drawing.Point(14, 122);
            this.lblLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.lblLabel1.Name = "lblLabel1";
            this.lblLabel1.Size = new System.Drawing.Size(222, 42);
            this.lblLabel1.TabIndex = 526;
            this.lblLabel1.Text = "<html>TIPE SEL<span style=\"color: #ff0000\">*</span></html>";
            // 
            // lblLabel14
            // 
            this.lblLabel14.AutoSize = false;
            this.lblLabel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblLabel14.Location = new System.Drawing.Point(539, 660);
            this.lblLabel14.Margin = new System.Windows.Forms.Padding(4);
            this.lblLabel14.Name = "lblLabel14";
            this.lblLabel14.Size = new System.Drawing.Size(153, 42);
            this.lblLabel14.TabIndex = 511;
            this.lblLabel14.Text = "<html>JENIS IDENTITAS/NO</html>";
            // 
            // lblradLabel13
            // 
            this.lblradLabel13.AutoSize = false;
            this.lblradLabel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblradLabel13.Location = new System.Drawing.Point(14, 272);
            this.lblradLabel13.Margin = new System.Windows.Forms.Padding(4);
            this.lblradLabel13.Name = "lblradLabel13";
            this.lblradLabel13.Size = new System.Drawing.Size(222, 42);
            this.lblradLabel13.TabIndex = 504;
            this.lblradLabel13.Text = "<html>JUMLAH ORANG</html>";
            // 
            // lblradLabel11
            // 
            this.lblradLabel11.AutoSize = false;
            this.lblradLabel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblradLabel11.Location = new System.Drawing.Point(14, 222);
            this.lblradLabel11.Margin = new System.Windows.Forms.Padding(4);
            this.lblradLabel11.Name = "lblradLabel11";
            this.lblradLabel11.Size = new System.Drawing.Size(222, 42);
            this.lblradLabel11.TabIndex = 502;
            this.lblradLabel11.Text = "<html>NAMA GEDUNG/LANTAI</html>";
            // 
            // lblDetailSurat
            // 
            this.lblDetailSurat.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailSurat, 6);
            this.lblDetailSurat.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailSurat.Image = ((System.Drawing.Image)(resources.GetObject("lblDetailSurat.Image")));
            this.lblDetailSurat.Location = new System.Drawing.Point(13, 13);
            this.lblDetailSurat.Name = "lblDetailSurat";
            this.lblDetailSurat.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetailSurat.Size = new System.Drawing.Size(1316, 52);
            this.lblDetailSurat.TabIndex = 138;
            this.lblDetailSurat.Text = "           DETAIL ALOKASI SEL";
            // 
            // txtDate
            // 
            this.txtDate.AutoSize = false;
            this.txtDate.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtDate, 2);
            this.txtDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDate.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtDate.ForeColor = System.Drawing.Color.Navy;
            this.txtDate.Location = new System.Drawing.Point(243, 809);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(289, 44);
            this.txtDate.TabIndex = 132;
            // 
            // lblLabel9
            // 
            this.lblLabel9.AutoSize = false;
            this.lblLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLabel9.Location = new System.Drawing.Point(539, 810);
            this.lblLabel9.Margin = new System.Windows.Forms.Padding(4);
            this.lblLabel9.Name = "lblLabel9";
            this.lblLabel9.Size = new System.Drawing.Size(153, 42);
            this.lblLabel9.TabIndex = 127;
            this.lblLabel9.Text = "<html>JARINGAN</html>";
            // 
            // lblLabel8
            // 
            this.lblLabel8.AutoSize = false;
            this.lblLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLabel8.Location = new System.Drawing.Point(539, 760);
            this.lblLabel8.Margin = new System.Windows.Forms.Padding(4);
            this.lblLabel8.Name = "lblLabel8";
            this.lblLabel8.Size = new System.Drawing.Size(153, 42);
            this.lblLabel8.TabIndex = 126;
            this.lblLabel8.Text = "<html>TIPE</html>";
            // 
            // lblLabel7
            // 
            this.lblLabel7.AutoSize = false;
            this.lblLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLabel7.Location = new System.Drawing.Point(539, 710);
            this.lblLabel7.Margin = new System.Windows.Forms.Padding(4);
            this.lblLabel7.Name = "lblLabel7";
            this.lblLabel7.Size = new System.Drawing.Size(153, 42);
            this.lblLabel7.TabIndex = 125;
            this.lblLabel7.Text = "<html>KATEGORI</html>";
            // 
            // lblLabel6
            // 
            this.lblLabel6.AutoSize = false;
            this.lblLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLabel6.Location = new System.Drawing.Point(14, 810);
            this.lblLabel6.Margin = new System.Windows.Forms.Padding(4);
            this.lblLabel6.Name = "lblLabel6";
            this.lblLabel6.Size = new System.Drawing.Size(222, 42);
            this.lblLabel6.TabIndex = 124;
            this.lblLabel6.Text = "<html>TANGGAL TERDAFTAR</html>";
            // 
            // lblLabel5
            // 
            this.lblLabel5.AutoSize = false;
            this.lblLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLabel5.Location = new System.Drawing.Point(14, 760);
            this.lblLabel5.Margin = new System.Windows.Forms.Padding(4);
            this.lblLabel5.Name = "lblLabel5";
            this.lblLabel5.Size = new System.Drawing.Size(222, 42);
            this.lblLabel5.TabIndex = 123;
            this.lblLabel5.Text = "<html>JENIS KELAMIN</html>";
            // 
            // lblLabel2
            // 
            this.lblLabel2.AutoSize = false;
            this.lblLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLabel2.Location = new System.Drawing.Point(14, 710);
            this.lblLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.lblLabel2.Name = "lblLabel2";
            this.lblLabel2.Size = new System.Drawing.Size(222, 42);
            this.lblLabel2.TabIndex = 122;
            this.lblLabel2.Text = "<html>AGAMA</html>";
            // 
            // lblLabel4
            // 
            this.lblLabel4.AutoSize = false;
            this.lblLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLabel4.Location = new System.Drawing.Point(14, 660);
            this.lblLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.lblLabel4.Name = "lblLabel4";
            this.lblLabel4.Size = new System.Drawing.Size(222, 42);
            this.lblLabel4.TabIndex = 120;
            this.lblLabel4.Text = "<html>NAMA</html>";
            // 
            // lblradLabel24
            // 
            this.lblradLabel24.AutoSize = false;
            this.lblradLabel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblradLabel24.Location = new System.Drawing.Point(14, 72);
            this.lblradLabel24.Margin = new System.Windows.Forms.Padding(4);
            this.lblradLabel24.Name = "lblradLabel24";
            this.lblradLabel24.Size = new System.Drawing.Size(222, 42);
            this.lblradLabel24.TabIndex = 19;
            this.lblradLabel24.Text = "<html>NAMA TAHANAN <span style=\"color: #ff0000\">*</span></html>";
            // 
            // lblradLabel1
            // 
            this.lblradLabel1.AutoSize = false;
            this.lblradLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblradLabel1.Location = new System.Drawing.Point(14, 322);
            this.lblradLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.lblradLabel1.Name = "lblradLabel1";
            this.lblradLabel1.Size = new System.Drawing.Size(222, 45);
            this.lblradLabel1.TabIndex = 65;
            this.lblradLabel1.Text = "<html><span style=\"color: #ff0000\"><span style=\"color: #000000\">ALOKASI SEL</span" +
    ">*</span></html>";
            // 
            // lblradLabel3
            // 
            this.lblradLabel3.AutoSize = false;
            this.lblradLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblradLabel3.Location = new System.Drawing.Point(14, 172);
            this.lblradLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.lblradLabel3.Name = "lblradLabel3";
            this.lblradLabel3.Size = new System.Drawing.Size(222, 42);
            this.lblradLabel3.TabIndex = 118;
            this.lblradLabel3.Text = "<html>KODE SEL <span style=\"color: #ff0000\">*</span></html>";
            // 
            // txtNama
            // 
            this.txtNama.AutoSize = false;
            this.txtNama.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtNama, 2);
            this.txtNama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNama.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtNama.ForeColor = System.Drawing.Color.Navy;
            this.txtNama.Location = new System.Drawing.Point(243, 659);
            this.txtNama.Name = "txtNama";
            this.txtNama.Size = new System.Drawing.Size(289, 44);
            this.txtNama.TabIndex = 130;
            // 
            // txtReligion
            // 
            this.txtReligion.AutoSize = false;
            this.txtReligion.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtReligion, 2);
            this.txtReligion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtReligion.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtReligion.ForeColor = System.Drawing.Color.Navy;
            this.txtReligion.Location = new System.Drawing.Point(243, 709);
            this.txtReligion.Name = "txtReligion";
            this.txtReligion.Size = new System.Drawing.Size(289, 44);
            this.txtReligion.TabIndex = 131;
            // 
            // txtGender
            // 
            this.txtGender.AutoSize = false;
            this.txtGender.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtGender, 2);
            this.txtGender.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGender.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtGender.ForeColor = System.Drawing.Color.Navy;
            this.txtGender.Location = new System.Drawing.Point(243, 759);
            this.txtGender.Name = "txtGender";
            this.txtGender.Size = new System.Drawing.Size(289, 44);
            this.txtGender.TabIndex = 132;
            // 
            // lblDetailInmate
            // 
            this.lblDetailInmate.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailInmate, 5);
            this.lblDetailInmate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailInmate.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.tahanan_color;
            this.lblDetailInmate.Location = new System.Drawing.Point(13, 609);
            this.lblDetailInmate.Name = "lblDetailInmate";
            this.lblDetailInmate.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetailInmate.Size = new System.Drawing.Size(844, 44);
            this.lblDetailInmate.TabIndex = 519;
            this.lblDetailInmate.Text = "        INFORMASI TAHANAN";
            // 
            // txtIdentity
            // 
            this.txtIdentity.AutoSize = false;
            this.txtIdentity.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtIdentity, 2);
            this.txtIdentity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIdentity.ForeColor = System.Drawing.Color.Navy;
            this.txtIdentity.Location = new System.Drawing.Point(700, 660);
            this.txtIdentity.Margin = new System.Windows.Forms.Padding(4);
            this.txtIdentity.Name = "txtIdentity";
            this.txtIdentity.Size = new System.Drawing.Size(628, 42);
            this.txtIdentity.TabIndex = 518;
            // 
            // txtCategory
            // 
            this.txtCategory.AutoSize = false;
            this.txtCategory.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtCategory, 2);
            this.txtCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCategory.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtCategory.ForeColor = System.Drawing.Color.Navy;
            this.txtCategory.Location = new System.Drawing.Point(699, 709);
            this.txtCategory.Name = "txtCategory";
            this.txtCategory.Size = new System.Drawing.Size(630, 44);
            this.txtCategory.TabIndex = 132;
            // 
            // txtType
            // 
            this.txtType.AutoSize = false;
            this.txtType.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtType, 2);
            this.txtType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtType.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtType.ForeColor = System.Drawing.Color.Navy;
            this.txtType.Location = new System.Drawing.Point(699, 759);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(630, 44);
            this.txtType.TabIndex = 132;
            // 
            // txtNetwork
            // 
            this.txtNetwork.AutoSize = false;
            this.txtNetwork.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtNetwork, 2);
            this.txtNetwork.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNetwork.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtNetwork.ForeColor = System.Drawing.Color.Navy;
            this.txtNetwork.Location = new System.Drawing.Point(699, 809);
            this.txtNetwork.Name = "txtNetwork";
            this.txtNetwork.Size = new System.Drawing.Size(630, 44);
            this.txtNetwork.TabIndex = 132;
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.Black;
            this.radPanel1.Controls.Add(this.lblphoto);
            this.radPanel1.Controls.Add(this.lblrightthumb);
            this.radPanel1.Controls.Add(this.lblleft);
            this.radPanel1.Controls.Add(this.lblright);
            this.radPanel1.Controls.Add(this.lblfront);
            this.radPanel1.Controls.Add(this.lblRegID);
            this.radPanel1.Controls.Add(this.picCanvas4);
            this.radPanel1.Controls.Add(this.picCanvas3);
            this.radPanel1.Controls.Add(this.picCanvas2);
            this.radPanel1.Controls.Add(this.picCanvas1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.radPanel1.Location = new System.Drawing.Point(918, 68);
            this.radPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.radPanel1.Name = "radPanel1";
            this.tableLayoutPanel1.SetRowSpan(this.radPanel1, 10);
            this.radPanel1.Size = new System.Drawing.Size(414, 538);
            this.radPanel1.TabIndex = 501;
            // 
            // lblphoto
            // 
            this.lblphoto.AutoSize = false;
            this.lblphoto.BackColor = System.Drawing.Color.Transparent;
            this.lblphoto.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblphoto.ForeColor = System.Drawing.Color.White;
            this.lblphoto.Image = ((System.Drawing.Image)(resources.GetObject("lblphoto.Image")));
            this.lblphoto.Location = new System.Drawing.Point(10, 7);
            this.lblphoto.Name = "lblphoto";
            this.lblphoto.Size = new System.Drawing.Size(398, 36);
            this.lblphoto.TabIndex = 144;
            this.lblphoto.Text = "      FOTO DAN SIDIK JARI";
            // 
            // lblrightthumb
            // 
            this.lblrightthumb.AutoSize = false;
            this.lblrightthumb.BackColor = System.Drawing.Color.Transparent;
            this.lblrightthumb.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrightthumb.ForeColor = System.Drawing.Color.White;
            this.lblrightthumb.Location = new System.Drawing.Point(215, 465);
            this.lblrightthumb.Name = "lblrightthumb";
            this.lblrightthumb.Size = new System.Drawing.Size(191, 22);
            this.lblrightthumb.TabIndex = 143;
            this.lblrightthumb.Text = "IBU JARI KANAN";
            this.lblrightthumb.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblleft
            // 
            this.lblleft.AutoSize = false;
            this.lblleft.BackColor = System.Drawing.Color.Transparent;
            this.lblleft.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblleft.ForeColor = System.Drawing.Color.White;
            this.lblleft.Location = new System.Drawing.Point(16, 465);
            this.lblleft.Name = "lblleft";
            this.lblleft.Size = new System.Drawing.Size(191, 22);
            this.lblleft.TabIndex = 142;
            this.lblleft.Text = "KIRI";
            this.lblleft.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblright
            // 
            this.lblright.AutoSize = false;
            this.lblright.BackColor = System.Drawing.Color.Transparent;
            this.lblright.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblright.ForeColor = System.Drawing.Color.White;
            this.lblright.Location = new System.Drawing.Point(218, 248);
            this.lblright.Name = "lblright";
            this.lblright.Size = new System.Drawing.Size(191, 22);
            this.lblright.TabIndex = 141;
            this.lblright.Text = "KANAN";
            this.lblright.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblfront
            // 
            this.lblfront.AutoSize = false;
            this.lblfront.BackColor = System.Drawing.Color.Transparent;
            this.lblfront.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfront.ForeColor = System.Drawing.Color.White;
            this.lblfront.Location = new System.Drawing.Point(16, 248);
            this.lblfront.Name = "lblfront";
            this.lblfront.Size = new System.Drawing.Size(191, 22);
            this.lblfront.TabIndex = 140;
            this.lblfront.Text = "DEPAN";
            this.lblfront.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRegID
            // 
            this.lblRegID.AutoSize = false;
            this.lblRegID.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblRegID.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegID.ForeColor = System.Drawing.Color.White;
            this.lblRegID.Location = new System.Drawing.Point(0, 492);
            this.lblRegID.Name = "lblRegID";
            this.lblRegID.Size = new System.Drawing.Size(414, 46);
            this.lblRegID.TabIndex = 139;
            this.lblRegID.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picCanvas4
            // 
            this.picCanvas4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.picCanvas4.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picCanvas4.Location = new System.Drawing.Point(210, 274);
            this.picCanvas4.Margin = new System.Windows.Forms.Padding(0);
            this.picCanvas4.Name = "picCanvas4";
            this.picCanvas4.Size = new System.Drawing.Size(194, 189);
            this.picCanvas4.TabIndex = 137;
            this.picCanvas4.TabStop = false;
            // 
            // picCanvas3
            // 
            this.picCanvas3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.picCanvas3.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picCanvas3.Location = new System.Drawing.Point(7, 274);
            this.picCanvas3.Margin = new System.Windows.Forms.Padding(0);
            this.picCanvas3.Name = "picCanvas3";
            this.picCanvas3.Size = new System.Drawing.Size(197, 188);
            this.picCanvas3.TabIndex = 136;
            this.picCanvas3.TabStop = false;
            // 
            // picCanvas2
            // 
            this.picCanvas2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.picCanvas2.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picCanvas2.Location = new System.Drawing.Point(210, 50);
            this.picCanvas2.Margin = new System.Windows.Forms.Padding(0);
            this.picCanvas2.Name = "picCanvas2";
            this.picCanvas2.Size = new System.Drawing.Size(196, 186);
            this.picCanvas2.TabIndex = 135;
            this.picCanvas2.TabStop = false;
            // 
            // picCanvas1
            // 
            this.picCanvas1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.picCanvas1.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picCanvas1.Location = new System.Drawing.Point(8, 50);
            this.picCanvas1.Margin = new System.Windows.Forms.Padding(0);
            this.picCanvas1.Name = "picCanvas1";
            this.picCanvas1.Size = new System.Drawing.Size(197, 186);
            this.picCanvas1.TabIndex = 134;
            this.picCanvas1.TabStop = false;
            // 
            // PanelRb
            // 
            this.PanelRb.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.SetColumnSpan(this.PanelRb, 3);
            this.PanelRb.Controls.Add(this.txtSingleCell);
            this.PanelRb.Controls.Add(this.txtDoubleCell);
            this.PanelRb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelRb.Location = new System.Drawing.Point(243, 121);
            this.PanelRb.Name = "PanelRb";
            this.PanelRb.Size = new System.Drawing.Size(450, 44);
            this.PanelRb.TabIndex = 2;
            this.PanelRb.Paint += new System.Windows.Forms.PaintEventHandler(this.PanelRb_Paint);
            // 
            // txtSingleCell
            // 
            this.txtSingleCell.AutoSize = false;
            this.txtSingleCell.Dock = System.Windows.Forms.DockStyle.Right;
            this.txtSingleCell.Location = new System.Drawing.Point(245, 0);
            this.txtSingleCell.Name = "txtSingleCell";
            this.txtSingleCell.Size = new System.Drawing.Size(205, 44);
            this.txtSingleCell.TabIndex = 2;
            this.txtSingleCell.TabStop = false;
            this.txtSingleCell.Text = "SEL TUNGGAL";
            // 
            // txtDoubleCell
            // 
            this.txtDoubleCell.AutoSize = false;
            this.txtDoubleCell.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtDoubleCell.Location = new System.Drawing.Point(0, 0);
            this.txtDoubleCell.Name = "txtDoubleCell";
            this.txtDoubleCell.Size = new System.Drawing.Size(236, 44);
            this.txtDoubleCell.TabIndex = 2;
            this.txtDoubleCell.TabStop = false;
            this.txtDoubleCell.Text = "SEL GANDA";
            // 
            // radPanel12
            // 
            this.radPanel12.Controls.Add(this.txtTotalInused);
            this.radPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel12.Location = new System.Drawing.Point(243, 271);
            this.radPanel12.Name = "radPanel12";
            this.radPanel12.Size = new System.Drawing.Size(47, 44);
            this.radPanel12.TabIndex = 5;
            // 
            // txtTotalInused
            // 
            this.txtTotalInused.AutoSize = false;
            this.txtTotalInused.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTotalInused.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtTotalInused.Location = new System.Drawing.Point(0, 0);
            this.txtTotalInused.Name = "txtTotalInused";
            this.txtTotalInused.Size = new System.Drawing.Size(47, 44);
            this.txtTotalInused.TabIndex = 5;
            // 
            // radPanel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel3, 3);
            this.radPanel3.Controls.Add(this.ddlCellAllocationStatus);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Location = new System.Drawing.Point(243, 374);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(450, 47);
            this.radPanel3.TabIndex = 8;
            // 
            // ddlCellAllocationStatus
            // 
            this.ddlCellAllocationStatus.AutoSize = false;
            this.ddlCellAllocationStatus.BackColor = System.Drawing.Color.Transparent;
            this.ddlCellAllocationStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlCellAllocationStatus.DropDownHeight = 200;
            this.ddlCellAllocationStatus.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlCellAllocationStatus.Location = new System.Drawing.Point(0, 0);
            this.ddlCellAllocationStatus.Name = "ddlCellAllocationStatus";
            this.ddlCellAllocationStatus.Size = new System.Drawing.Size(450, 47);
            this.ddlCellAllocationStatus.TabIndex = 8;
            // 
            // searchData1
            // 
            this.searchData1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.searchData1.Location = new System.Drawing.Point(4900, 97);
            this.searchData1.Name = "searchData1";
            this.searchData1.Padding = new System.Windows.Forms.Padding(5);
            this.searchData1.Size = new System.Drawing.Size(1069, 536);
            this.searchData1.TabIndex = 511;
            // 
            // PanelEror
            // 
            this.PanelEror.Controls.Add(this.radPanelError);
            this.PanelEror.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelEror.Location = new System.Drawing.Point(0, 596);
            this.PanelEror.Name = "PanelEror";
            this.PanelEror.Size = new System.Drawing.Size(1361, 46);
            this.PanelEror.TabIndex = 512;
            // 
            // radPanelError
            // 
            this.radPanelError.Controls.Add(this.lblError);
            this.radPanelError.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanelError.Location = new System.Drawing.Point(0, 0);
            this.radPanelError.Margin = new System.Windows.Forms.Padding(0);
            this.radPanelError.Name = "radPanelError";
            this.radPanelError.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radPanelError.Size = new System.Drawing.Size(1361, 43);
            this.radPanelError.TabIndex = 70;
            // 
            // lblError
            // 
            this.lblError.AutoSize = false;
            this.lblError.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.warning1;
            this.lblError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblError.Location = new System.Drawing.Point(15, 0);
            this.lblError.Name = "lblError";
            this.lblError.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.lblError.Size = new System.Drawing.Size(1346, 39);
            this.lblError.TabIndex = 24;
            // 
            // fullpicPanel
            // 
            this.fullpicPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fullpicPanel.Controls.Add(this.closefull);
            this.fullpicPanel.Controls.Add(this.fullPic);
            this.fullpicPanel.Location = new System.Drawing.Point(28200, 94);
            this.fullpicPanel.Name = "fullpicPanel";
            this.fullpicPanel.Padding = new System.Windows.Forms.Padding(5);
            this.fullpicPanel.Size = new System.Drawing.Size(603, 641);
            this.fullpicPanel.TabIndex = 513;
            this.fullpicPanel.Visible = false;
            // 
            // closefull
            // 
            this.closefull.BackColor = System.Drawing.Color.Transparent;
            this.closefull.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.closefull.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closefull.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.delete1;
            this.closefull.Location = new System.Drawing.Point(555, 4);
            this.closefull.Name = "closefull";
            this.closefull.Size = new System.Drawing.Size(45, 45);
            this.closefull.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.closefull.TabIndex = 5;
            this.closefull.TabStop = false;
            // 
            // fullPic
            // 
            this.fullPic.BackColor = System.Drawing.Color.White;
            this.fullPic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fullPic.Location = new System.Drawing.Point(5, 5);
            this.fullPic.Name = "fullPic";
            this.fullPic.Size = new System.Drawing.Size(593, 631);
            this.fullPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fullPic.TabIndex = 0;
            this.fullPic.TabStop = false;
            // 
            // FormCellAllocation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.fullpicPanel);
            this.Controls.Add(this.PanelEror);
            this.Controls.Add(this.searchData1);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.headerPanel);
            this.Controls.Add(this.lblButton);
            this.Name = "FormCellAllocation";
            this.Size = new System.Drawing.Size(1361, 829);
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).EndInit();
            this.headerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).EndInit();
            this.radPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DateEnter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).EndInit();
            this.radPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtRoommate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).EndInit();
            this.radPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtBuilFloor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            this.radPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlCellCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtRegCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblradLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblradLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblradLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblradLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblradLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReligion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailInmate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNetwork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblphoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblrightthumb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblleft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblright)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblfront)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelRb)).EndInit();
            this.PanelRb.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSingleCell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDoubleCell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel12)).EndInit();
            this.radPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalInused)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlCellAllocationStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelEror)).EndInit();
            this.PanelEror.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).EndInit();
            this.radPanelError.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullpicPanel)).EndInit();
            this.fullpicPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.closefull)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullPic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnBack;
        private Telerik.WinControls.UI.RadButton btnSave;
        private Telerik.WinControls.UI.RadPanel headerPanel;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private InmatesSearchData searchData1;
        private Telerik.WinControls.UI.RadPanel PanelEror;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel txtIdentity;
        private Telerik.WinControls.UI.RadLabel lblLabel14;
        private Telerik.WinControls.UI.RadLabel txtTotalInused;
        private Telerik.WinControls.UI.RadLabel lblradLabel13;
        private Telerik.WinControls.UI.RadLabel lblradLabel11;
        private Telerik.WinControls.UI.RadDropDownList ddlCellAllocationStatus;
        private Telerik.WinControls.UI.RadDropDownList ddlCellCode;
        private Telerik.WinControls.UI.RadLabel lblDetailSurat;
        private Telerik.WinControls.UI.RadLabel txtNetwork;
        private Telerik.WinControls.UI.RadLabel txtType;
        private Telerik.WinControls.UI.RadLabel txtCategory;
        private Telerik.WinControls.UI.RadLabel txtDate;
        private Telerik.WinControls.UI.RadLabel lblLabel9;
        private Telerik.WinControls.UI.RadLabel lblLabel8;
        private Telerik.WinControls.UI.RadLabel lblLabel7;
        private Telerik.WinControls.UI.RadLabel lblLabel6;
        private Telerik.WinControls.UI.RadLabel lblLabel5;
        private Telerik.WinControls.UI.RadLabel lblLabel2;
        private Telerik.WinControls.UI.RadLabel lblLabel4;
        private Telerik.WinControls.UI.RadLabel lblradLabel24;
        private Telerik.WinControls.UI.RadLabel lblradLabel1;
        private Telerik.WinControls.UI.RadLabel lblradLabel3;
        private Telerik.WinControls.UI.RadDateTimePicker DateEnter;
        private Telerik.WinControls.UI.RadLabel txtNama;
        private Telerik.WinControls.UI.RadLabel txtReligion;
        private Telerik.WinControls.UI.RadLabel txtGender;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadLabel lblphoto;
        private Telerik.WinControls.UI.RadLabel lblrightthumb;
        private Telerik.WinControls.UI.RadLabel lblleft;
        private Telerik.WinControls.UI.RadLabel lblright;
        private Telerik.WinControls.UI.RadLabel lblfront;
        private Telerik.WinControls.UI.RadLabel lblRegID;
        private System.Windows.Forms.PictureBox picCanvas4;
        private System.Windows.Forms.PictureBox picCanvas3;
        private System.Windows.Forms.PictureBox picCanvas2;
        private System.Windows.Forms.PictureBox picCanvas1;
        private Telerik.WinControls.UI.RadLabel lblDetailInmate;
        private Telerik.WinControls.UI.RadPanel radPanelError;
        private Telerik.WinControls.UI.RadLabel lblError;
        private Telerik.WinControls.UI.RadPanel fullpicPanel;
        private System.Windows.Forms.PictureBox fullPic;
        private Telerik.WinControls.UI.RadPanel PanelRb;
        private Telerik.WinControls.UI.RadRadioButton txtSingleCell;
        private Telerik.WinControls.UI.RadRadioButton txtDoubleCell;
        private Telerik.WinControls.UI.RadLabel txtRoommate;
        private Telerik.WinControls.UI.RadDropDownList txtRegCode;
        private Telerik.WinControls.UI.RadLabel lblLabel1;
        private Telerik.WinControls.UI.RadLabel lblLabel3;
        private System.Windows.Forms.PictureBox closefull;
        private Telerik.WinControls.UI.RadPanel radPanel12;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadPanel radPanel5;
        private Telerik.WinControls.UI.RadPanel radPanel7;
        private Telerik.WinControls.UI.RadLabel txtBuilFloor;
        private Telerik.WinControls.UI.RadPanel radPanel8;
    }
}
