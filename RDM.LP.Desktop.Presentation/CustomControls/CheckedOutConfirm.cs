﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using System.Threading;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class CheckedOutConfirm : Base
    {
        private Point MouseDownLocation;
        public CheckedOutConfirm()
        {
            InitializeComponent();
            InitForm();
            InitEvents();

        }

        private void InitForm()
        {
            txtNote.Text = string.Empty;

            ToolTip toolTip = new ToolTip();
            toolTip.ToolTipIcon = ToolTipIcon.Info;
            toolTip.ToolTipTitle = "Info";
            toolTip.SetToolTip(exit, "Tutup Konfirmasi");
            txtNote.TextBoxElement.CustomFont = Global.MainFont;
            txtNote.TextBoxElement.CustomFontSize = Global.CustomFontSizeMain;

            this.btnConfirm.ButtonElement.CustomFont = Global.MainFont;
            this.btnConfirm.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            lblTitle.BackColor = Global.MainColor;
            lblTitle.Text = "      KONFIRMASI KELUAR SEL";
            lblTitle.ForeColor = Color.White;
            lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;
            lblTitle.LabelElement.CustomFont = Global.MainFont;
            lblTitle.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            ddlCheckedOutDate.Value = DateTime.Today.ToLocalTime();

            //GlobalFunction.InvisibleControlDetail(tableLayoutPanel1, 4);
            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;
            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
        }

        private void InitEvents()
        {
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            exit.Click += exit_Click;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            this.VisibleChanged += Form_VisibleChanged;
            btnConfirm.Click += BtnConfirm_Click;
            btnBack.Click += BtnBack_Click;
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            ClearData();
            this.Hide();
        }

        private void BtnConfirm_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                SaveData();
                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Berhasil Keluar !");
                this.Hide();
                UserFunction.LoadDataAllocationToGrid(MainForm.formMain.ListCellAllocation.GvAllocation);
                MainForm.formMain.ListCellAllocation.GvAllocation.Refresh();
                UserFunction.LoadDataCellToGrid(MainForm.formMain.ListCell.GvCell);
                MainForm.formMain.ListCell.GvCell.Refresh();

                InmatesService tahanan = new InmatesService();

                MainForm.formMain.CellAllocation.RegID.DataSource = tahanan.GetByName();
                MainForm.formMain.CellAllocation.RegID.DisplayMember = "FullName";
                MainForm.formMain.CellAllocation.RegID.ValueMember = "RegID";
                MainForm.formMain.CellAllocation.RegID.Text = string.Empty;
                MainForm.formMain.CellAllocation.RegID.SelectedValue = 0;
                MainForm.formMain.CellAllocation.RegID.Refresh();

                MainForm.formMain.CellAllocation.RegID.Refresh();
                MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
            }

            ClearData();
        }

        private void ClearData()
        {
            this.txtNote.Text = string.Empty;
            this.ddlCheckedOutDate.Value = DateTime.Today.ToLocalTime();
        }

        private bool DataValid()
        {
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;
            if (this.txtNote.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Catatan!";
                return false;
            }

            if (this.ddlCheckedOutDate.Text == string.Empty)
            {
                GlobalFunction.VisibleControlDetail(tableLayoutPanel1, 4);
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tanggal Keluar!";
                return false;
            }

            return true;
        }

        private void SaveData()
        {
            CellAllocationService alloc = new CellAllocationService();
            var allocationcell = alloc.GetById(Convert.ToInt32(this.Tag));
            CellService serv = new CellService();

            var cell = serv.GetByCode(allocationcell.CellCode);
            var model = new Cell
            {
                Id = cell.Id,
                CellStatus = "TERSEDIA",
                InUseCounter = (cell.InUseCounter) - 1,
                UpdatedDate = DateTime.Now.ToLocalTime(),
                UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
            };
            serv.Update(model);

            var data = new CellAllocation
            {
                Id = Convert.ToInt32(this.Tag),
                IsCheckedOut = true,
                CellCodeId = allocationcell.CellCodeId,
                CheckedOutDate = ddlCheckedOutDate.Value,
                CheckedOutNote = txtNote.Text == null ? "-" : txtNote.Text,
                DateEnter = allocationcell.DateEnter,
                UpdatedDate = DateTime.Now.ToLocalTime(),
                UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
            };
            alloc.Update(data);
        }

        private void exit_Click(object sender, EventArgs e)
        {
            ClearData();
            this.Hide();
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (this.Tag != null)
                    LoadData(this.Tag.ToString());
                else
                    this.Hide();
            }
        }

        private void LoadData(string id)
        {
            CellAllocationService serv = new CellAllocationService();
            var data = serv.GetById(Convert.ToInt32(id));

            ddlCheckedOutDate.Value = DateTime.Now;
            ddlCheckedOutDate.MaxDate = DateTime.Now;
            ddlCheckedOutDate.MinDate = data.DateReg == null ? DateTime.Now : data.DateReg;
        }
    }
}
