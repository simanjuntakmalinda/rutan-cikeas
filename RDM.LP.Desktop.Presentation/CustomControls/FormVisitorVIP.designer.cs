﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class FormVisitorVIP
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormVisitorVIP));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnBack = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            this.radPanelError = new Telerik.WinControls.UI.RadPanel();
            this.lblError = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.txtAddress = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel19 = new Telerik.WinControls.UI.RadPanel();
            this.ddlJnsIdentitas = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel18 = new Telerik.WinControls.UI.RadPanel();
            this.txtNama = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel15 = new Telerik.WinControls.UI.RadPanel();
            this.txtPurpose = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel14 = new Telerik.WinControls.UI.RadPanel();
            this.txtNotes = new Telerik.WinControls.UI.RadTextBox();
            this.lblFullName = new Telerik.WinControls.UI.RadLabel();
            this.lblIDType = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailVisitor = new Telerik.WinControls.UI.RadLabel();
            this.lblCity = new Telerik.WinControls.UI.RadLabel();
            this.radPanel17 = new Telerik.WinControls.UI.RadPanel();
            this.txtNoIdentitas = new Telerik.WinControls.UI.RadTextBox();
            this.lblAddress = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailSurat = new Telerik.WinControls.UI.RadLabel();
            this.headerPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.cameraSource = new RDM.LP.Desktop.Presentation.CustomControls.CameraSource();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).BeginInit();
            this.radPanelError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel19)).BeginInit();
            this.radPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlJnsIdentitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel18)).BeginInit();
            this.radPanel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel15)).BeginInit();
            this.radPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPurpose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel14)).BeginInit();
            this.radPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFullName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIDType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel17)).BeginInit();
            this.radPanel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoIdentitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).BeginInit();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnBack);
            this.lblButton.Controls.Add(this.btnSave);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 642);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(20);
            this.lblButton.Size = new System.Drawing.Size(1254, 94);
            this.lblButton.TabIndex = 81;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.btnBack_Image;
            this.btnBack.Location = new System.Drawing.Point(20, 20);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(244, 54);
            this.btnBack.TabIndex = 8;
            this.btnBack.Tag = "";
            this.btnBack.Text = "&BATAL";
            this.btnBack.ThemeName = "MaterialBlueGrey";
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.if_checked_2_309094;
            this.btnSave.Location = new System.Drawing.Point(929, 20);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(305, 54);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "&SIMPAN";
            this.btnSave.ThemeName = "MaterialBlueGrey";
            // 
            // radPanelError
            // 
            this.radPanelError.Controls.Add(this.lblError);
            this.radPanelError.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanelError.Location = new System.Drawing.Point(0, 598);
            this.radPanelError.Margin = new System.Windows.Forms.Padding(0);
            this.radPanelError.Name = "radPanelError";
            this.radPanelError.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radPanelError.Size = new System.Drawing.Size(1254, 43);
            this.radPanelError.TabIndex = 80;
            // 
            // lblError
            // 
            this.lblError.AutoSize = false;
            this.lblError.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.warning;
            this.lblError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblError.Location = new System.Drawing.Point(15, 0);
            this.lblError.Name = "lblError";
            this.lblError.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.lblError.Size = new System.Drawing.Size(1239, 39);
            this.lblError.TabIndex = 24;
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("radScrollablePanel1.BackgroundImage")));
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 108);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(1252, 488);
            this.radScrollablePanel1.Size = new System.Drawing.Size(1254, 490);
            this.radScrollablePanel1.TabIndex = 78;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 664F));
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.radLabel4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.radPanel19, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel18, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.radPanel15, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.radPanel14, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblFullName, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblIDType, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailVisitor, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblCity, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.radPanel17, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblAddress, 0, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1252, 487);
            this.tableLayoutPanel1.TabIndex = 161;
            // 
            // radPanel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel1, 2);
            this.radPanel1.Controls.Add(this.txtAddress);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(263, 163);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(976, 144);
            this.radPanel1.TabIndex = 4;
            // 
            // txtAddress
            // 
            this.txtAddress.AutoSize = false;
            this.txtAddress.BackColor = System.Drawing.Color.White;
            this.txtAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAddress.Location = new System.Drawing.Point(0, 0);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(976, 144);
            this.txtAddress.TabIndex = 105;
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel4.ForeColor = System.Drawing.Color.Black;
            this.radLabel4.Location = new System.Drawing.Point(14, 164);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(242, 142);
            this.radLabel4.TabIndex = 322;
            this.radLabel4.Text = "<html>Alamat<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel19
            // 
            this.radPanel19.Controls.Add(this.ddlJnsIdentitas);
            this.radPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel19.Location = new System.Drawing.Point(263, 63);
            this.radPanel19.Name = "radPanel19";
            this.radPanel19.Size = new System.Drawing.Size(312, 44);
            this.radPanel19.TabIndex = 1;
            // 
            // ddlJnsIdentitas
            // 
            this.ddlJnsIdentitas.AutoSize = false;
            this.ddlJnsIdentitas.BackColor = System.Drawing.Color.White;
            this.ddlJnsIdentitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlJnsIdentitas.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlJnsIdentitas.DropDownHeight = 400;
            this.ddlJnsIdentitas.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlJnsIdentitas.Location = new System.Drawing.Point(0, 0);
            this.ddlJnsIdentitas.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlJnsIdentitas.Name = "ddlJnsIdentitas";
            // 
            // 
            // 
            this.ddlJnsIdentitas.RootElement.CustomFont = "Roboto";
            this.ddlJnsIdentitas.RootElement.CustomFontSize = 13F;
            this.ddlJnsIdentitas.Size = new System.Drawing.Size(312, 44);
            this.ddlJnsIdentitas.TabIndex = 100;
            this.ddlJnsIdentitas.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlJnsIdentitas.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlJnsIdentitas.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlJnsIdentitas.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlJnsIdentitas.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlJnsIdentitas.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel18
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel18, 2);
            this.radPanel18.Controls.Add(this.txtNama);
            this.radPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel18.Location = new System.Drawing.Point(263, 113);
            this.radPanel18.Name = "radPanel18";
            this.radPanel18.Size = new System.Drawing.Size(976, 44);
            this.radPanel18.TabIndex = 3;
            // 
            // txtNama
            // 
            this.txtNama.AutoSize = false;
            this.txtNama.BackColor = System.Drawing.Color.White;
            this.txtNama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNama.Location = new System.Drawing.Point(0, 0);
            this.txtNama.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNama.Name = "txtNama";
            this.txtNama.Size = new System.Drawing.Size(976, 44);
            this.txtNama.TabIndex = 102;
            // 
            // radPanel15
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel15, 2);
            this.radPanel15.Controls.Add(this.txtPurpose);
            this.radPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel15.Location = new System.Drawing.Point(263, 313);
            this.radPanel15.Name = "radPanel15";
            this.radPanel15.Size = new System.Drawing.Size(976, 44);
            this.radPanel15.TabIndex = 5;
            // 
            // txtPurpose
            // 
            this.txtPurpose.AutoSize = false;
            this.txtPurpose.BackColor = System.Drawing.Color.White;
            this.txtPurpose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPurpose.Location = new System.Drawing.Point(0, 0);
            this.txtPurpose.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtPurpose.Name = "txtPurpose";
            this.txtPurpose.Size = new System.Drawing.Size(976, 44);
            this.txtPurpose.TabIndex = 105;
            // 
            // radPanel14
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel14, 2);
            this.radPanel14.Controls.Add(this.txtNotes);
            this.radPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel14.Location = new System.Drawing.Point(263, 363);
            this.radPanel14.Name = "radPanel14";
            this.radPanel14.Size = new System.Drawing.Size(976, 94);
            this.radPanel14.TabIndex = 6;
            // 
            // txtNotes
            // 
            this.txtNotes.AutoSize = false;
            this.txtNotes.BackColor = System.Drawing.Color.White;
            this.txtNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNotes.Location = new System.Drawing.Point(0, 0);
            this.txtNotes.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNotes.Multiline = true;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.Padding = new System.Windows.Forms.Padding(5);
            this.txtNotes.Size = new System.Drawing.Size(976, 94);
            this.txtNotes.TabIndex = 106;
            // 
            // lblFullName
            // 
            this.lblFullName.AutoSize = false;
            this.lblFullName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFullName.ForeColor = System.Drawing.Color.Black;
            this.lblFullName.Location = new System.Drawing.Point(14, 114);
            this.lblFullName.Margin = new System.Windows.Forms.Padding(4);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.Size = new System.Drawing.Size(242, 42);
            this.lblFullName.TabIndex = 181;
            this.lblFullName.Text = "<html>Nama Lengkap<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblIDType
            // 
            this.lblIDType.AutoSize = false;
            this.lblIDType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIDType.ForeColor = System.Drawing.Color.Black;
            this.lblIDType.Location = new System.Drawing.Point(14, 64);
            this.lblIDType.Margin = new System.Windows.Forms.Padding(4);
            this.lblIDType.Name = "lblIDType";
            this.lblIDType.Size = new System.Drawing.Size(242, 42);
            this.lblIDType.TabIndex = 191;
            this.lblIDType.Text = "<html>Jenis Identitas / No<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblDetailVisitor
            // 
            this.lblDetailVisitor.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailVisitor, 3);
            this.lblDetailVisitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailVisitor.Location = new System.Drawing.Point(13, 13);
            this.lblDetailVisitor.Name = "lblDetailVisitor";
            this.lblDetailVisitor.Padding = new System.Windows.Forms.Padding(10, 10, 0, 10);
            this.lblDetailVisitor.Size = new System.Drawing.Size(1226, 44);
            this.lblDetailVisitor.TabIndex = 134;
            this.lblDetailVisitor.Text = "DETAIL PEGUNJUNG VIP";
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = false;
            this.lblCity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCity.ForeColor = System.Drawing.Color.Black;
            this.lblCity.Location = new System.Drawing.Point(14, 364);
            this.lblCity.Margin = new System.Windows.Forms.Padding(4);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(242, 92);
            this.lblCity.TabIndex = 110;
            this.lblCity.Text = "<html>Catatan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radPanel17
            // 
            this.radPanel17.Controls.Add(this.txtNoIdentitas);
            this.radPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel17.Location = new System.Drawing.Point(581, 63);
            this.radPanel17.Name = "radPanel17";
            this.radPanel17.Size = new System.Drawing.Size(658, 44);
            this.radPanel17.TabIndex = 2;
            // 
            // txtNoIdentitas
            // 
            this.txtNoIdentitas.AutoSize = false;
            this.txtNoIdentitas.BackColor = System.Drawing.Color.White;
            this.txtNoIdentitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoIdentitas.Location = new System.Drawing.Point(0, 0);
            this.txtNoIdentitas.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtNoIdentitas.MaxLength = 16;
            this.txtNoIdentitas.Name = "txtNoIdentitas";
            this.txtNoIdentitas.Size = new System.Drawing.Size(658, 44);
            this.txtNoIdentitas.TabIndex = 101;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.txtNoIdentitas.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = false;
            this.lblAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAddress.ForeColor = System.Drawing.Color.Black;
            this.lblAddress.Location = new System.Drawing.Point(14, 314);
            this.lblAddress.Margin = new System.Windows.Forms.Padding(4);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(242, 42);
            this.lblAddress.TabIndex = 321;
            this.lblAddress.Text = "<html>Tujuan<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblDetailSurat
            // 
            this.lblDetailSurat.AutoSize = false;
            this.lblDetailSurat.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailSurat.Location = new System.Drawing.Point(0, 56);
            this.lblDetailSurat.Name = "lblDetailSurat";
            this.lblDetailSurat.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetailSurat.Size = new System.Drawing.Size(1254, 52);
            this.lblDetailSurat.TabIndex = 751;
            this.lblDetailSurat.Text = "Mohon Isi seluruh Kolom yang Diperlukan";
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.lblTitle);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Margin = new System.Windows.Forms.Padding(4);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(1254, 56);
            this.headerPanel.TabIndex = 71;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(1254, 56);
            this.lblTitle.TabIndex = 711;
            // 
            // cameraSource
            // 
            this.cameraSource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cameraSource.Location = new System.Drawing.Point(32700, 320);
            this.cameraSource.Name = "cameraSource";
            this.cameraSource.Size = new System.Drawing.Size(581, 186);
            this.cameraSource.TabIndex = 82;
            this.cameraSource.Visible = false;
            // 
            // FormVisitorVIP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.cameraSource);
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radPanelError);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblDetailSurat);
            this.Controls.Add(this.headerPanel);
            this.Name = "FormVisitorVIP";
            this.Size = new System.Drawing.Size(1254, 736);
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).EndInit();
            this.radPanelError.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel19)).EndInit();
            this.radPanel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlJnsIdentitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel18)).EndInit();
            this.radPanel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel15)).EndInit();
            this.radPanel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPurpose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel14)).EndInit();
            this.radPanel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFullName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIDType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel17)).EndInit();
            this.radPanel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNoIdentitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).EndInit();
            this.headerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList imageList;
        private Telerik.WinControls.UI.RadPanel headerPanel;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel lblDetailSurat;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadTextBox txtNotes;
        private Telerik.WinControls.UI.RadLabel lblCity;
        private Telerik.WinControls.UI.RadLabel lblAddress;
        private Telerik.WinControls.UI.RadLabel lblFullName;
        private Telerik.WinControls.UI.RadLabel lblIDType;
        private Telerik.WinControls.UI.RadTextBox txtNoIdentitas;
        private Telerik.WinControls.UI.RadTextBox txtNama;
        private Telerik.WinControls.UI.RadTextBox txtPurpose;
        private Telerik.WinControls.UI.RadLabel lblDetailVisitor;
        private Telerik.WinControls.UI.RadPanel radPanelError;
        private Telerik.WinControls.UI.RadLabel lblError;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnBack;
        private Telerik.WinControls.UI.RadButton btnSave;
        private CameraSource cameraSource;
        private Telerik.WinControls.UI.RadPanel radPanel14;
        private Telerik.WinControls.UI.RadPanel radPanel15;
        private Telerik.WinControls.UI.RadPanel radPanel18;
        private Telerik.WinControls.UI.RadPanel radPanel17;
        private Telerik.WinControls.UI.RadPanel radPanel19;
        private Telerik.WinControls.UI.RadDropDownList ddlJnsIdentitas;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadTextBox txtAddress;
        private Telerik.WinControls.UI.RadLabel radLabel4;
    }
}
