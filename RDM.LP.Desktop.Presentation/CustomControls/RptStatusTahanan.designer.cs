﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class RptStatusTahanan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RptStatusTahanan));
            this.headerPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.print = new System.Windows.Forms.Panel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableFormCell = new System.Windows.Forms.TableLayoutPanel();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).BeginInit();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableFormCell.SuspendLayout();
            this.SuspendLayout();
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.lblTitle);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Margin = new System.Windows.Forms.Padding(4);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(998, 56);
            this.headerPanel.TabIndex = 81;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.print);
            this.lblTitle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lblTitle.Size = new System.Drawing.Size(998, 56);
            this.lblTitle.TabIndex = 11;
            // 
            // print
            // 
            this.print.BackColor = System.Drawing.Color.Transparent;
            this.print.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.print;
            this.print.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.print.CausesValidation = false;
            this.print.Cursor = System.Windows.Forms.Cursors.Hand;
            this.print.Dock = System.Windows.Forms.DockStyle.Right;
            this.print.Location = new System.Drawing.Point(934, 0);
            this.print.Name = "print";
            this.print.Size = new System.Drawing.Size(54, 56);
            this.print.TabIndex = 1;
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.BackColor = System.Drawing.Color.White;
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 56);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableFormCell);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(996, 678);
            this.radScrollablePanel1.Size = new System.Drawing.Size(998, 680);
            this.radScrollablePanel1.TabIndex = 82;
            // 
            // tableFormCell
            // 
            this.tableFormCell.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tableFormCell.ColumnCount = 1;
            this.tableFormCell.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableFormCell.Controls.Add(this.webBrowser, 0, 0);
            this.tableFormCell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableFormCell.Location = new System.Drawing.Point(0, 0);
            this.tableFormCell.Margin = new System.Windows.Forms.Padding(10);
            this.tableFormCell.Name = "tableFormCell";
            this.tableFormCell.Padding = new System.Windows.Forms.Padding(10);
            this.tableFormCell.RowCount = 1;
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableFormCell.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableFormCell.Size = new System.Drawing.Size(996, 678);
            this.tableFormCell.TabIndex = 17;
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(13, 13);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(970, 652);
            this.webBrowser.TabIndex = 131;
            // 
            // RptStatusTahanan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.headerPanel);
            this.Name = "RptStatusTahanan";
            this.Size = new System.Drawing.Size(998, 735);
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).EndInit();
            this.headerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableFormCell.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadPanel headerPanel;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableFormCell;
        private System.Windows.Forms.WebBrowser webBrowser;
        private System.Windows.Forms.Panel print;
    }
}
