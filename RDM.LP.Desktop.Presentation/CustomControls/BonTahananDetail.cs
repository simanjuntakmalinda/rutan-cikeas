﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using System.Threading;
using Telerik.WinControls;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class BonTahananDetail : Base
    {
        private Point MouseDownLocation;
        private BonTahanan data;
        public BonTahananDetail()
        {
            InitializeComponent();
            InitForm();
            InitEvents();

        }

        private void LoadData(string id)
        {
            BonTahananService bonserv = new BonTahananService();
            InmateBonTahananService iboserv = new InmateBonTahananService();
            var data = bonserv.GetById(Convert.ToInt32(this.Tag));
            var datainmate = iboserv.GetByBonTahananId(Convert.ToInt32(this.Tag));
            data = bonserv.GetById(Convert.ToInt32(id));

            NamaTahanan.Items.Clear();

            if (data != null)
            {
                NoBonTahanan.Text = data.NoBonTahanan.ToUpper();
                //NamaTahanan.Text = data.NamaTahanan.ToUpper();
                ListViewItem lv = new ListViewItem();
                foreach (var item in datainmate)
                {
                    lv.Text = item.FullName;
                    lv.Tag = item.RegID;
                    NamaTahanan.Items.Add(lv.Tag,lv.Text);
                }

                Keperluan.Text = data.NamaKeperluan;
                Durasi.Text = data.TanggalMulai.ToString("dd MMMM yyyy").ToUpper() + " S.D. " + data.TanggalSelesai.ToString("dd MMMM yyyy").ToUpper() ;
                Lokasi.Text = data.Lokasi;
                Keterangan.Text = data.Keterangan;

                if(data.TipePemohon == "Jaksa")
                {
                    radLabelNRPPemohon.Text = "NIP";
                    DetailPemohon.Text = data.NrpPemohon;
                }
                else
                {
                    radLabelNRPPemohon.Text = "NRP / PANGKAT / SATKER";
                    DetailPemohon.Text = data.NrpPemohon + " / " + data.NamaPangkatPemohon.ToUpper() + " / " + data.SatkerPemohon;
                }

                NamaPemohon.Text = data.NamaPemohon;
                NoHpPemohon.Text = data.NoHpPemohon;

                NamaPJ.Text = data.NamaPenanggungJawab;
                DetailPJ.Text = data.NrpPenanggungJawab + " / " + data.NamaPangkatPenanggungJawab.ToUpper() + " / " + data.SatkerPenanggungJawab;
                NoHpPJ.Text = data.NoHpPenanggungJawab;

                NamaDisetujui.Text = data.NamaPenyetuju;
                DetailDisetujui.Text = data.NrpPenyetuju + " / " + data.NamaPangkatPenyetuju.ToUpper() + " / " + data.SatkerPenyetuju;

                inputter.Text = data.CreatedBy;
                inputdate.Text = data.CreatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss").ToUpper();
                updater.Text = data.UpdatedBy;
                updatedate.Text = data.UpdatedDate==null?string.Empty:data.UpdatedDate.Value.ToString("dd MMMM yyyy HH:mm:ss").ToUpper();
            }
        }

        private void InitForm()
        {
            ToolTip toolTip = new ToolTip();
            toolTip.ToolTipIcon = ToolTipIcon.Info;
            toolTip.ToolTipTitle = "Info";
            toolTip.SetToolTip(exit, "Close Visit Information");
            lblButton.BackColor = Global.MainColor;
            lblTitle.BackColor = Global.MainColor;
            lblTitle.Text = "      VISIT INFORMATION";
            lblTitle.ForeColor = Color.White;
            lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;
            lblTitle.LabelElement.CustomFont = Global.MainFont;
            lblTitle.LabelElement.CustomFontSize = Global.CustomFontSizeMain;

            btnClose.ButtonElement.CustomFontStyle = FontStyle.Bold;
            btnClose.ButtonElement.CustomFont = Global.MainFont;
            btnClose.ButtonElement.CustomFontSize = Global.CustomFontSizeMain;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
        }

        private void InitEvents()
        {
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            exit.Click += exit_Click;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            this.VisibleChanged += Form_VisibleChanged;
            btnClose.Click += btnClose_Click;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (this.Tag != null)
                    LoadData(this.Tag.ToString());
                else
                    this.Hide();
            }
        }
    }
}
