﻿using System;
using System.Drawing;
using System.Windows.Forms;
using libzkfpcsharp;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO;
using RDM.LP.DataAccess.Service;
using RDM.LP.DataAccess.ViewModel;
using System.Collections.Generic;
using System.Text;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FingerPrint_Inmate : UserControl
    {
        public PictureBox RightThumbFinger { get { return this.pbRightThumbFinger; } set { } }
        public PictureBox RightIndexFinger { get { return this.pbRightIndexFinger; } set { } }
        public PictureBox RightMiddleFinger { get { return this.pbRightMiddleFinger; } set { } }
        public PictureBox RightRingFinger { get { return this.pbRightRingFinger; } set { } }
        public PictureBox RightLittleFinger { get { return this.pbRightLittleFinger; } set { } }
        public PictureBox LeftThumbFinger { get { return this.pbLeftThumbFinger; } set { } }
        public PictureBox LeftIndexFinger { get { return this.pbLeftIndexFinger; } set { } }
        public PictureBox LeftMiddleFinger { get { return this.pbLeftMiddleFinger; } set { } }
        public PictureBox LeftRingFinger { get { return this.pbLeftRingFinger; } set { } }
        public PictureBox LeftLittleFinger { get { return this.pbLeftLittleFinger; } set { } }
        public Label Message { get { return this.lblmessage; } set { } }

        private Point MouseDownLocation;
        public FingerPrint_Inmate()
        {
            InitializeComponent();
            InitForm();
            InitEvents();
        }

        private void InitForm() {

        }

        private void InitEvents()
        {
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            exit.Click += exit_Click;
            btnClear.Click += btnClear_Click;
            btnSave.Click += btnSave_Click;

            pbRightThumbFinger.Click += pbRightThumb_Click;
            pbRightIndexFinger.Click += pbRightIndexFinger_Click;
            pbRightMiddleFinger.Click += pbRightMiddleFinger_Click;
            pbRightRingFinger.Click += pbRightRingFinger_Click;
            pbRightLittleFinger.Click += pbRightLittleFinger_Click;

            pbLeftThumbFinger.Click += pbLeftThumbFinger_Click;
            pbLeftIndexFinger.Click += pbLeftIndexFinger_Click;
            pbLeftMiddleFinger.Click += pbLeftMiddleFinger_Click;
            pbLeftRingFinger.Click += pbLeftRingFinger_Click;
            pbLeftLittleFinger.Click += pbLeftLittleFinger_Click;

            this.VisibleChanged += Form_VisibleChanged;
        }



        private void pbRightThumb_Click(object sender, EventArgs e)
        {
            RightThumb_Click();
        }

        public void RightThumb_Click()
        {
            panel1.BackColor = Color.Chartreuse;
            panel2.BackColor = Color.Silver;
            panel3.BackColor = Color.Silver;
            panel4.BackColor = Color.Silver;
            panel5.BackColor = Color.Silver;
            panel6.BackColor = Color.Silver;
            panel7.BackColor = Color.Silver;
            panel8.BackColor = Color.Silver;
            panel9.BackColor = Color.Silver;
            panel10.BackColor = Color.Silver;

            GlobalVariables.PanelAktif = 1;
            lblmessage.Text = "SILAHKAN TEKAN IBU JARI SEBELAH KANAN";
        }

        private void pbRightIndexFinger_Click(object sender, EventArgs e)
        {
            RightIndexFinger_Click();
        }

        public void RightIndexFinger_Click()
        {
            panel1.BackColor = Color.Silver;
            panel2.BackColor = Color.Chartreuse;
            panel3.BackColor = Color.Silver;
            panel4.BackColor = Color.Silver;
            panel5.BackColor = Color.Silver;
            panel6.BackColor = Color.Silver;
            panel7.BackColor = Color.Silver;
            panel8.BackColor = Color.Silver;
            panel9.BackColor = Color.Silver;
            panel10.BackColor = Color.Silver;

            GlobalVariables.PanelAktif = 2;
            lblmessage.Text = "SILAHKAN TEKAN JARI TELUNJUK SEBELAH KANAN";
        }

        private void pbRightMiddleFinger_Click(object sender, EventArgs e)
        {
            RightMiddleFinger_Click();
        }

        public void RightMiddleFinger_Click()
        {
            panel1.BackColor = Color.Silver;
            panel2.BackColor = Color.Silver;
            panel3.BackColor = Color.Chartreuse;
            panel4.BackColor = Color.Silver;
            panel5.BackColor = Color.Silver;
            panel6.BackColor = Color.Silver;
            panel7.BackColor = Color.Silver;
            panel8.BackColor = Color.Silver;
            panel9.BackColor = Color.Silver;
            panel10.BackColor = Color.Silver;

            GlobalVariables.PanelAktif = 3;
            lblmessage.Text = "SILAHKAN TEKAN JARI TENGAH SEBELAH KANAN";
        }

        private void pbRightRingFinger_Click(object sender, EventArgs e)
        {
            RightRingFinger_Click();
        }

        public void RightRingFinger_Click()
        {
            panel1.BackColor = Color.Silver;
            panel2.BackColor = Color.Silver;
            panel3.BackColor = Color.Silver;
            panel4.BackColor = Color.Chartreuse;
            panel5.BackColor = Color.Silver;
            panel6.BackColor = Color.Silver;
            panel7.BackColor = Color.Silver;
            panel8.BackColor = Color.Silver;
            panel9.BackColor = Color.Silver;
            panel10.BackColor = Color.Silver;

            GlobalVariables.PanelAktif = 4;
            lblmessage.Text = "SILAHKAN TEKAN JARI MANIS SEBELAH KANAN";
        }

        private void pbRightLittleFinger_Click(object sender, EventArgs e)
        {
            RightLittleFinger_Click();
        }

        public void RightLittleFinger_Click()
        {
            panel1.BackColor = Color.Silver;
            panel2.BackColor = Color.Silver;
            panel3.BackColor = Color.Silver;
            panel4.BackColor = Color.Silver;
            panel5.BackColor = Color.Chartreuse;
            panel6.BackColor = Color.Silver;
            panel7.BackColor = Color.Silver;
            panel8.BackColor = Color.Silver;
            panel9.BackColor = Color.Silver;
            panel10.BackColor = Color.Silver;

            GlobalVariables.PanelAktif = 5;
            lblmessage.Text = "SILAHKAN TEKAN JARI KELINGKING SEBELAH KANAN";
        }

        private void pbLeftThumbFinger_Click(object sender, EventArgs e)
        {
            LeftThumbFinger_Click();
        }

        public void LeftThumbFinger_Click()
        {
            panel1.BackColor = Color.Silver;
            panel2.BackColor = Color.Silver;
            panel3.BackColor = Color.Silver;
            panel4.BackColor = Color.Silver;
            panel5.BackColor = Color.Silver;
            panel6.BackColor = Color.Chartreuse;
            panel7.BackColor = Color.Silver;
            panel8.BackColor = Color.Silver;
            panel9.BackColor = Color.Silver;
            panel10.BackColor = Color.Silver;

            GlobalVariables.PanelAktif = 6;
            lblmessage.Text = "SILAHKAN TEKAN IBU JARI SEBELAH KIRI";
        }

        private void pbLeftIndexFinger_Click(object sender, EventArgs e)
        {
            LeftIndexFinger_Click();
        }

        public void LeftIndexFinger_Click()
        {
            panel1.BackColor = Color.Silver;
            panel2.BackColor = Color.Silver;
            panel3.BackColor = Color.Silver;
            panel4.BackColor = Color.Silver;
            panel5.BackColor = Color.Silver;
            panel6.BackColor = Color.Silver;
            panel7.BackColor = Color.Chartreuse;
            panel8.BackColor = Color.Silver;
            panel9.BackColor = Color.Silver;
            panel10.BackColor = Color.Silver;

            GlobalVariables.PanelAktif = 7;
            lblmessage.Text = "SILAHKAN TEKAN JARI TELUNJUK SEBELAH KIRI";
        }

        private void pbLeftMiddleFinger_Click(object sender, EventArgs e)
        {
            LeftMiddleFinger_Click();
        }

        public void LeftMiddleFinger_Click()
        {
            panel1.BackColor = Color.Silver;
            panel2.BackColor = Color.Silver;
            panel3.BackColor = Color.Silver;
            panel4.BackColor = Color.Silver;
            panel5.BackColor = Color.Silver;
            panel6.BackColor = Color.Silver;
            panel7.BackColor = Color.Silver;
            panel8.BackColor = Color.Chartreuse;
            panel9.BackColor = Color.Silver;
            panel10.BackColor = Color.Silver;

            GlobalVariables.PanelAktif = 8;
            lblmessage.Text = "SILAHKAN TEKAN JARI TENGAH SEBELAH KIRI";
        }

        private void pbLeftRingFinger_Click(object sender, EventArgs e)
        {
            LeftRingFinger_Click();
        }

        public void LeftRingFinger_Click()
        {
            panel1.BackColor = Color.Silver;
            panel2.BackColor = Color.Silver;
            panel3.BackColor = Color.Silver;
            panel4.BackColor = Color.Silver;
            panel5.BackColor = Color.Silver;
            panel6.BackColor = Color.Silver;
            panel7.BackColor = Color.Silver;
            panel8.BackColor = Color.Silver;
            panel9.BackColor = Color.Chartreuse;
            panel10.BackColor = Color.Silver;

            GlobalVariables.PanelAktif = 9;
            lblmessage.Text = "SILAHKAN TEKAN JARI MANIS SEBELAH KIRI";
        }

        private void pbLeftLittleFinger_Click(object sender, EventArgs e)
        {
            LeftLittleFinger_Click();
        }

        public void LeftLittleFinger_Click()
        {
            panel1.BackColor = Color.Silver;
            panel2.BackColor = Color.Silver;
            panel3.BackColor = Color.Silver;
            panel4.BackColor = Color.Silver;
            panel5.BackColor = Color.Silver;
            panel6.BackColor = Color.Silver;
            panel7.BackColor = Color.Silver;
            panel8.BackColor = Color.Silver;
            panel9.BackColor = Color.Silver;
            panel10.BackColor = Color.Chartreuse;

            GlobalVariables.PanelAktif = 10;
            lblmessage.Text = "SILAHKAN TEKAN JARI KELINGKING SEBELAH KIRI";
        }

        private void exit_Click(object sender, EventArgs e)
        {

            this.Hide();
            CleanFingerPrint_Inmate();
            MainForm.formMain.CaptureFingerPrint.ClearFingerPrint();

            if (this.Tag == null)
            {
                MainForm.formMain.CaptureFingerPrint.Show();
            }

            this.Tag = null;

        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            CleanFingerPrint_Inmate();
            MainForm.formMain.FingerPrint_Inmate.Tag = null;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var question = UserFunction.Confirm("Lanjutkan?");
            if (question == DialogResult.Yes)
            {
                MainForm.formMain.Tahanan.FingerPicture.Image = RightThumbFinger.Image;

                GlobalVariables.RegisterCount = -1;
                this.Hide();
                MainForm.formMain.Tahanan.IdNo.Focus();
                if (MainForm.formMain.FingerPrint_Inmate.Tag == null)
                {
                    MainForm.formMain.Tahanan.ClearDataInmates();
                }
            }
            else
            {
                this.Hide();
                //CleanFingerPrint_Inmate();
                MainForm.formMain.CaptureFingerPrint.ClearFingerPrint();
                MainForm.formMain.CaptureFingerPrint.Show();
            }

            MainForm.formMain.FingerPrint_Inmate.Tag = null;
        }

        public void CleanFingerPrint_Inmate()
        {
            RightThumbFinger.Image = null;
            RightIndexFinger.Image = null;
            RightMiddleFinger.Image = null;
            RightRingFinger.Image = null;
            RightLittleFinger.Image = null;
            LeftThumbFinger.Image = null;
            LeftIndexFinger.Image = null;
            LeftMiddleFinger.Image = null;
            LeftRingFinger.Image = null;
            LeftLittleFinger.Image = null;

            DisableSaveButton();
            RightThumb_Click();

        }

        public void EnableSaveButton()
        {
            this.btnSave.Enabled = true;
            this.btnSave.BackColor = System.Drawing.SystemColors.ControlDark;
        }

        public void DisableSaveButton()
        {
            this.btnSave.Enabled = false;
            this.btnSave.BackColor = System.Drawing.SystemColors.ControlLight;
        }

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                MainForm.formMain.CaptureFingerPrint.ResetDevice();
                MainForm.formMain.Opacity.Show();
                this.BringToFront();

                if (this.Tag != null)
                {
                    if (Convert.ToInt32(this.Tag) == GlobalVariables.REGISTER_FINGER_COUNT)
                    {
                        MainForm.formMain.FingerPrint_Inmate.EnableSaveButton();
                        MainForm.formMain.FingerPrint_Inmate.Message.Text = "KLIK TOMBOL LANJUTKAN UNTUK MELANJUTKAN";
                    }
                }
            }
            else
            {
                if(MainForm.formMain!=null)
                    MainForm.formMain.Opacity.Hide();
            }
        }
    }
}
