﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.DataAccess.Helper;
using System.IO;
using System.Collections;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormCellAllocation : Base
    {
        public InmatesSearchData search { get { return this.searchData1; } }
        //public TextBox RegID { get { return this.txtRegCode; } }
        public RadDropDownList RegID { get { return this.txtRegCode; } }
        public RadLabel FullName { get { return this.txtNama; } }
        public RadLabel Religion { get { return this.txtReligion; } }
        public RadLabel Gender { get { return this.txtGender; } }
        public RadLabel DateRegistered { get { return this.txtDate; } }
        public RadLabel InmatesCategory { get { return this.txtCategory; } }
        public RadLabel InmatesType { get { return this.txtType; } }
        public RadLabel Network { get { return this.txtNetwork; } }
        public RadLabel Identity { get { return this.txtIdentity; } }
        public RadLabel LblRegID { get { return this.lblRegID; } }
        public PictureBox PicInmates1 { get { return this.picCanvas1; } set { } }
        public PictureBox PicInmates2 { get { return this.picCanvas2; } set { } }
        public PictureBox PicInmates3 { get { return this.picCanvas3; } set { } }
        public PictureBox PicFingerPrint { get { return this.picCanvas4; } set { } }
        public RadDropDownList DDLCellCode { get { return this.ddlCellCode; } set { } }

        private Point MouseDownLocation;
        private CellAllocate OldData;
        public FormCellAllocation()
        {
            InitializeComponent();
            InitForm();
            LoadData();
            InitEvents();
        }

        public void LoadData()
        {
            //ClearData();
            this.lblTitle.Text = "       TAMBAH ALOKASI SEL BARU";
            //ddl CellCode
            CellService cellserv = new CellService();
            ddlCellCode.DisplayMember = "CellCode";
            ddlCellCode.ValueMember = "Id";
            ddlCellCode.DataSource = cellserv.GetAvailableCell();
            ddlCellCode.SelectedIndex = -1;

            //cell allocation status
            CellAllocationStatusService status = new CellAllocationStatusService();
            ddlCellAllocationStatus.DisplayMember = "Status";
            ddlCellAllocationStatus.ValueMember = "Id";
            ddlCellAllocationStatus.DataSource = status.Get();
            ddlCellAllocationStatus.SelectedIndex = 0;

            InmatesService tahanan = new InmatesService();
            
            //autocomplete
            this.txtRegCode.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.txtRegCode.DropDownListElement.AutoCompleteSuggest.SuggestMode = SuggestMode.Contains;
            this.txtRegCode.DataSource = tahanan.GetByNameNotOut();
            this.txtRegCode.DisplayMember = "FullName";
            this.txtRegCode.ValueMember = "Id";
            this.txtRegCode.Text = string.Empty;
            this.txtRegCode.SelectedValue = 0;
            txtDoubleCell.CheckState = CheckState.Unchecked;
            txtSingleCell.CheckState = CheckState.Unchecked;


            if (this.Tag != null)
            {
                MainForm.formMain.AddNew.Text = "EDIT ALOKASI SEL";
                this.lblTitle.Text = "     EDIT ALOKASI SEL";
                CellAllocationService serv = new CellAllocationService();
                var data = serv.GetById(Convert.ToInt32(this.Tag));
                if (data == null)
                {
                    ddlCellCode.SelectedIndex = -1;
                    ddlCellAllocationStatus.SelectedIndex = 0;
                    DateEnter.Value = DateTime.Today.ToLocalTime();
                    DdlCellCode_SelectedValueChanged(ddlCellCode, EventArgs.Empty);
                    return;
                }
                else
                {
                    var imgList = tahanan.GetImage(data.InmatesRegCode);
                    var fingerSource = tahanan.GetFingerPrint(data.InmatesRegCode);

                    OldData = data;
                    txtRegCode.Text = data.FullName?.ToUpper();
                    lblRegID.Text = data.InmatesRegCode?.ToUpper();
                    ddlCellCode.SelectedValue = data.CellCodeId;
                    ddlCellCode.Enabled = false;
                    txtIdentity.Text = data.IDInmates?.ToUpper();
                    txtBuilFloor.Text = data.BuildFloor?.ToUpper();
                    txtTotalInused.Text = data.InUseCounter?.ToUpper();
                    DateEnter.Value = data.DateEnter;
                    ddlCellAllocationStatus.SelectedValue = data.CellAllocationStatusId;
                    txtNama.Text = data.FullName?.ToUpper();
                    txtReligion.Text = data.Religion?.ToUpper();
                    txtGender.Text = data.Gender?.ToUpper();
                    txtDate.Text = data.DateReg.ToString("dd MMMM yyyy");
                    txtCategory.Text = data.Category?.ToUpper();
                    txtType.Text = data.Type?.ToUpper();
                    txtNetwork.Text = data.Network?.ToUpper();
                    DisplayInmatesPhoto(imgList);
                    DisplayFingerPrint(fingerSource);
                    if (data.CellType == "1")
                    {
                        txtSingleCell.CheckState = CheckState.Checked;
                    }
                    else
                    {
                        txtDoubleCell.CheckState = CheckState.Checked;
                    }
                }
            }
            CheckStateChanged_click(txtSingleCell, EventArgs.Empty);
        }

        private void DisplayFingerPrint(IEnumerable<FingerPrint> fingerSource)
        {
            foreach (var item in fingerSource)
            {
                var imageBytes = (byte[])Convert.FromBase64String(item.ImagePrint);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);

                if (item.FingerId == "R1")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, picCanvas4.Width, picCanvas4.Height);
                    /* Clear any existing image in the PictureBox. */
                    picCanvas4.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    picCanvas4.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    picCanvas4.Image = imgOutput;
                }
            }
        }

        private void DisplayInmatesPhoto(IEnumerable<Photos> datatemp)
        {
            foreach (var item in datatemp)
            {
                var imageBytes = (byte[])Convert.FromBase64String(item.Data);
                MemoryStream memStm = new MemoryStream(imageBytes);
                memStm.Seek(0, SeekOrigin.Begin);
                Image image = Image.FromStream(memStm);
                if (item.Note.ToUpper() == "FRONT")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, MainForm.formMain.CellAllocation.PicInmates1.Width, MainForm.formMain.CellAllocation.PicInmates1.Height);
                    /* Clear any existing image in the PictureBox. */
                    MainForm.formMain.CellAllocation.PicInmates1.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    MainForm.formMain.CellAllocation.PicInmates1.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    MainForm.formMain.CellAllocation.PicInmates1.Image = imgOutput;

                }
                else if (item.Note.ToUpper() == "LEFT")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, MainForm.formMain.CellAllocation.PicInmates2.Width, MainForm.formMain.CellAllocation.PicInmates2.Height);
                    /* Clear any existing image in the PictureBox. */
                    MainForm.formMain.CellAllocation.PicInmates2.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    MainForm.formMain.CellAllocation.PicInmates2.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    MainForm.formMain.CellAllocation.PicInmates2.Image = imgOutput;
                }
                else if (item.Note.ToUpper() == "RIGHT")
                {
                    Size fitImageSize = this.getScaledImageDimensions(image.Width, image.Height, MainForm.formMain.CellAllocation.PicInmates3.Width, MainForm.formMain.CellAllocation.PicInmates3.Height);
                    /* Clear any existing image in the PictureBox. */
                    MainForm.formMain.CellAllocation.PicInmates3.Image = null;
                    /* When fitting the image to the window, we want to keep it centered. */
                    MainForm.formMain.CellAllocation.PicInmates3.SizeMode = PictureBoxSizeMode.CenterImage;

                    Bitmap imgOutput = new Bitmap(image, fitImageSize.Width, fitImageSize.Height);
                    MainForm.formMain.CellAllocation.PicInmates3.Image = imgOutput;
                }

            }
        }

        private Size getScaledImageDimensions(int width1, int height1, int width2, int height2)
        {
            /*Determine if Image is Portrait or Landscape. */
            double scaleImageMultiplier = 0;
            if (width1 > height1)    /* Image is Portrait */
            {
                /* Calculate the multiplier based on the heights. */
                if (height2 > width2)
                {
                    scaleImageMultiplier = (double)width2 / (double)width1;
                }

                else
                {
                    scaleImageMultiplier = (double)height2 / (double)height1;
                }
            }

            else /* Image is Landscape */
            {
                /* Calculate the multiplier based on the widths. */
                if (height2 >= width2)
                {
                    scaleImageMultiplier = (double)width2 / (double)width1;
                }

                else
                {
                    scaleImageMultiplier = (double)height2 / (double)height1;
                }
            }

            /* Generate and return the new scaled dimensions.

            * Essentially, we multiply each dimension of the original image
            * by the multiplier calculated above to yield the dimensions
            * of the scaled image. The scaled image can be larger or smaller
            * than the original.
            */
            return new Size((int)(width1 * scaleImageMultiplier), (int)(height1 * scaleImageMultiplier));
        }

        private void ClearData()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            txtRegCode.Text = string.Empty;
            DateEnter.Text = string.Empty;
            txtNama.Text = string.Empty;
            txtReligion.Text = string.Empty;
            txtGender.Text = string.Empty;
            txtDate.Text = string.Empty;
            txtCategory.Text = string.Empty;
            txtType.Text = string.Empty;
            txtNetwork.Text = string.Empty;
            ddlCellCode.SelectedIndex = -1;
            ddlCellCode.Enabled = true;
            ddlCellAllocationStatus.SelectedIndex = 0;
            txtBuilFloor.Text = string.Empty;
            txtTotalInused.Text = string.Empty;
            txtIdentity.Text = string.Empty;
            lblRegID.Text = string.Empty;
            picCanvas1.Image = null;
            picCanvas2.Image = null;
            picCanvas3.Image = null;
            picCanvas4.Image = null;
            txtRoommate.Text = string.Empty;
        }

        private void InitEvents()
        {
            //Events
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;
            ddlCellCode.SelectedIndexChanged += DdlCellCode_SelectedValueChanged;
            picCanvas1.Click += picCanvas1_Click;
            picCanvas2.Click += picCanvas2_Click;
            picCanvas3.Click += picCanvas3_Click;
            picCanvas4.Click += picCanvas4_Click;
            closefull.Click += closefull_Click;
            txtSingleCell.CheckStateChanged += CheckStateChanged_click;
            txtDoubleCell.CheckStateChanged += CheckStateChanged_click;
            txtRoommate.Click += LblRoommate_Click;
            txtRegCode.SelectedValueChanged += txtRegCode_SelectedValueChanged;
            this.VisibleChanged += Form_VisibleChanged;

            PanelRb.KeyDown += Control_KeyDown;
            radPanel1.KeyDown += Control_KeyDown;
            radPanel2.KeyDown += Control_KeyDown;
            radPanel3.KeyDown += Control_KeyDown;
            radPanel4.KeyDown += Control_KeyDown;
            radPanel5.KeyDown += Control_KeyDown;
            radPanel7.KeyDown += Control_KeyDown;
            radPanel8.KeyDown += Control_KeyDown;
            txtRegCode.KeyDown += Control_KeyDown;
            ddlCellAllocationStatus.KeyDown += Control_KeyDown;
            txtDoubleCell.KeyDown += Control_KeyDown;
            txtSingleCell.KeyDown += Control_KeyDown;
            txtBuilFloor.KeyDown += Control_KeyDown;
            txtTotalInused.KeyDown += Control_KeyDown;
            txtRoommate.KeyDown += Control_KeyDown;
            DateEnter.KeyDown += Control_KeyDown;
            ddlCellAllocationStatus.KeyDown += Control_KeyDown;
            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }

        private void txtRegCode_SelectedValueChanged(object sender, EventArgs e)
        {
            InmatesService tahanan = new InmatesService();
            var regid = txtRegCode.SelectedValue == null ? "0" : txtRegCode.SelectedValue.ToString();
            var dataimage = tahanan.GetImage(regid);
            var fingerSource = tahanan.GetFingerPrint(regid);
            var data = tahanan.GetByRegId(regid);
            if (data.Count() != 0)
            {
                lblRegID.Text = data.FirstOrDefault().RegID.ToUpper();
                txtNama.Text = data.FirstOrDefault().FullName.ToUpper();
                txtReligion.Text = data.FirstOrDefault().Religion.ToUpper();
                txtGender.Text = data.FirstOrDefault().Gender.ToUpper();
                txtIdentity.Text = data.FirstOrDefault().IDType.ToUpper() + " / " + data.FirstOrDefault().IDNo;
                txtCategory.Text = data.FirstOrDefault().Category.ToUpper();
                txtType.Text = data.FirstOrDefault().Type.ToUpper();
                txtCategory.Text = data.FirstOrDefault().Category.ToUpper();
                txtNetwork.Text = data.FirstOrDefault().Network.ToUpper();
                txtDate.Text = data.FirstOrDefault().DateReg.Value == null ? string.Empty : data.FirstOrDefault().DateReg.Value.ToString("dd MMMM yyyy");
            }

            if (dataimage.Count() != 0)
                DisplayInmatesPhoto(dataimage);
            else
            {
                MainForm.formMain.JadwalKunjungan.PicInmates1.Image = null;
                MainForm.formMain.JadwalKunjungan.PicInmates2.Image = null;
                MainForm.formMain.JadwalKunjungan.PicInmates3.Image = null;
            }

            if (fingerSource.Count() != 0)
                DisplayFingerPrint(fingerSource);
            else
                picCanvas4.Image = null;
        }

        private void LblRoommate_Click(object sender, EventArgs e)
        {
            if (txtRoommate.Text != "(-)")
            {
                MainForm.formMain.CellAllocationDetail.Tag = txtRoommate.Tag;
                MainForm.formMain.CellAllocationDetail.Show();
            }
        }

        private void CheckStateChanged_click(object sender, EventArgs e)
        {
            txtBuilFloor.Text = string.Empty;
            txtTotalInused.Text = string.Empty;
            txtRoommate.Text = string.Empty;
            if (((RadRadioButton)sender).CheckState == CheckState.Checked)
            {
                CellService cellserv = new CellService();
                switch (((RadRadioButton)sender).Name)
                {
                    case "txtSingleCell":
                        ddlCellCode.DataSource = cellserv.GetByType("1");
                        //ddlCellCode.SelectedIndex = 0;
                        txtRoommate.Enabled = false;
                        break;
                    case "txtDoubleCell":
                        ddlCellCode.DataSource = cellserv.GetByType("2");
                        //ddlCellCode.SelectedIndex = 0;
                        txtRoommate.Enabled = true;
                        break;
                }
                //DdlCellCode_SelectedValueChanged(ddlCellCode, EventArgs.Empty);
            }
        }

        private void closefull_Click(object sender, EventArgs e)
        {
            fullPic.Image = null;
            fullpicPanel.Hide();
        }

        private void picCanvas4_Click(object sender, EventArgs e)
        {
            fullpicPanel.Show();
            fullPic.Image = picCanvas4.Image;
        }

        private void picCanvas3_Click(object sender, EventArgs e)
        {
            fullpicPanel.Show();
            fullPic.Image = picCanvas3.Image;
        }

        private void picCanvas2_Click(object sender, EventArgs e)
        {
            fullpicPanel.Show();
            fullPic.Image = picCanvas2.Image;
        }

        private void picCanvas1_Click(object sender, EventArgs e)
        {
            fullpicPanel.Show();
            fullPic.Image = picCanvas1.Image;
        }

        private void BtnSearchInmates_Click(object sender, EventArgs e)
        {

            searchData1.TxtSearch.Text = string.Empty;
            searchData1.Show();
            searchData1.Location = new Point(ClientSize.Width / 2 - searchData1.Size.Width / 2,
              ClientSize.Height / 2 - searchData1.Size.Height / 2);
        }

        private void DdlCellCode_SelectedValueChanged(object sender, EventArgs e)
        {
            CellService cellserv = new CellService();
            var data = cellserv.GetByCode(ddlCellCode.Text);
            var dataroommate = cellserv.GetRoommate(ddlCellCode.Text);
            if (data != null)
            {

                txtBuilFloor.Text = data.BuildingName + '/' + data.CellFloor;
                txtTotalInused.Text = data.InUseCounter.ToString();
            }
            if (dataroommate != null)
            {
                txtRoommate.Text = dataroommate.Roommate = "( " + dataroommate.Roommate + " )";
                txtRoommate.Tag = dataroommate.IdCellAllocation;
                txtRoommate.ForeColor = Color.Navy;
                txtRoommate.Enabled = true;
            }
            else
                txtRoommate.Text = string.Empty;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                if (this.Tag == null)
                    SaveData();
                else
                    UpdatetData();

                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Disimpan !");

                InmatesService tahanan = new InmatesService();

                this.txtRegCode.DataSource = tahanan.GetByName();
                txtRegCode.DisplayMember = "FullName";
                txtRegCode.ValueMember = "Id";
                txtRegCode.Text = string.Empty;
                txtRegCode.SelectedValue = 0;
                txtRegCode.Refresh();

                CellService cellserv = new CellService();
                ddlCellCode.DisplayMember = "CellCode";
                ddlCellCode.ValueMember = "Id";
                ddlCellCode.DataSource = cellserv.GetAvailableCell();
                ddlCellCode.SelectedIndex = -1;

                UserFunction.LoadDataAllocationToGrid(MainForm.formMain.ListCellAllocation.GvAllocation);
                UserFunction.LoadDataTahananCheckedOutToGrid(MainForm.formMain.DaftarTahanan.GvVisitor);
                MainForm.formMain.ListCellAllocation.GvAllocation.Refresh();
                MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
                MainForm.formMain.AddNew.Text = "TAMBAH ALOKASI SEL BARU";
                this.lblTitle.Text = "       TAMBAH ALOKASI SEL BARU";
                this.Tag = null;
                ClearData();
            }
        }

        private void SaveData()
        {
            if (this.Tag == null)
                InsertData();
            else
                UpdatetData();
        }

        private void UpdatetData()
        {
            CellAllocationService serv = new CellAllocationService();
            var alloc = new CellAllocation();
            alloc.Id = Convert.ToInt32(this.Tag);
            alloc.InmatesRegCode = txtRegCode.Text;
            alloc.CellCode = ddlCellCode.Text;
            alloc.CellCodeId = (int)ddlCellCode.SelectedValue;
            alloc.DateEnter = DateEnter.Value;
            alloc.CellAllocationStatusId = (int)ddlCellAllocationStatus.SelectedValue;
            alloc.CheckedOutDate = null;
            alloc.UpdatedDate = DateTime.Now.ToLocalTime();
            alloc.UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID);
            serv.Update(alloc);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.CELLALLOCATION.ToString(), Activities = "Update CellAllocation, Old Data=" + UserFunction.JsonString(OldData) + ",  New Data=" + UserFunction.JsonString(alloc) });

        }

        private void InsertData()
        {
            InmatesService inmates = new InmatesService();

            CellAllocationService serv = new CellAllocationService();
            var alloc = new CellAllocation();
            alloc.InmatesRegCode = inmates.GetDetailById(txtRegCode.SelectedValue.ToString()).RegID.ToString();
            alloc.CellCode = ddlCellCode.Text;
            alloc.CellCodeId = (int)ddlCellCode.SelectedValue;
            alloc.DateEnter = DateEnter.Value;
            alloc.CellAllocationStatusId = (int)ddlCellAllocationStatus.SelectedValue;
            alloc.CreatedDate = DateTime.Now.ToLocalTime();
            alloc.CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID);
            serv.Post(alloc);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.CELLALLOCATION.ToString(), Activities = "Add New Allocation, Data=" + UserFunction.JsonString(alloc) });

            var InmatesData = inmates.GetDetailByRegID(alloc.InmatesRegCode);

            InmatesIsCheckedOutService outserv = new InmatesIsCheckedOutService();
            var checkAlokasi = outserv.GetInmatesCheckoutCell(alloc.InmatesRegCode);
            if (checkAlokasi != null)
            {
                var dataout = new InmatesIsCheckedOut
                {
                    Id = checkAlokasi.Id,
                    InmatesId = InmatesData.Id,
                    InmatesName = InmatesData.FullName,
                    RegID = InmatesData.RegID,
                    DateReg = InmatesData.DateReg,
                    IsCheckedOut = false,

                    UpdatedDate = DateTime.Now.ToLocalTime(),
                    UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
                };
                outserv.Update(dataout);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.CELLALLOCATION.ToString(), Activities = "Update InmatesIsCheckedOut, Old Data=" + UserFunction.JsonString(checkAlokasi) + ",  New Data=" + UserFunction.JsonString(dataout) });
            }
            else
            {
                var newdataout = new InmatesIsCheckedOut
                {
                    InmatesId = InmatesData.Id,
                    InmatesName = InmatesData.FullName,
                    RegID = InmatesData.RegID,
                    DateReg = InmatesData.DateReg,
                    IsCheckedOut = false,

                    CreatedDate = DateTime.Now.ToLocalTime(),
                    CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
                };
                outserv.Post(newdataout);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Add New InmatesIsCheckedOut, Data=" + UserFunction.JsonString(newdataout) });
            }

            txtRegCode.Refresh();

            CellService cellserv = new CellService();
            var data = cellserv.GetByCode(ddlCellCode.Text);
            data.InUseCounter = (data.InUseCounter) + 1;
            data.BuildingName = null;
            cellserv.Update(data);

            if (data.CellType == "2" && data.InUseCounter > 1)
            {
                data.CellStatus = "DITEMPATI";
            }
            else if (data.CellType == "1" && data.InUseCounter == 1)
            {
                data.CellStatus = "DITEMPATI";
            }
            else
            {
                data.CellStatus = "TERSEDIA";
            }

            cellserv.Update(data);
        }

        private void InitForm()
        {
            this.lblError.Visible = false;
            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;


            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 15.5f;

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
            txtRoommate.RootElement.ForeColor = Color.FromArgb(192, 0, 0);
            PanelRb.RootElement.EnableElementShadow = false;

            DateEnter.Value = DateTime.Today.ToLocalTime();
            DateEnter.MaxDate = DateTime.Today.ToLocalTime();
            txtRegCode.BackColor = Color.Gainsboro;


            fullpicPanel.Location =
            new Point(this.Width / 2 - fullpicPanel.Size.Width / 2,
              this.Height / 2 - fullpicPanel.Size.Height / 2);

            lblphoto.ForeColor = Color.White;
            lblfront.ForeColor = Color.White;
            lblright.ForeColor = Color.White;
            lblleft.ForeColor = Color.White;
            lblrightthumb.ForeColor = Color.White;
            lblRegID.ForeColor = Color.White;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ClearData();
            this.Tag = null;
            MainForm.formMain.AddNew.Text = "TAMBAH ALOKASI SEL BARU";
            this.lblTitle.Text = "     TAMBAH ALOKASI SEL BARU";
            UserFunction.ClearControls(tableLayoutPanel1);
            MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
        }

        private bool DataValid()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            if (this.txtRegCode.SelectedValue == null)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Nama Tahanan!";
                return false;
            }
            if (this.ddlCellCode.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Kode Sel!";
                return false;
            }

            if (this.DateEnter.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tanggal Masuk!";
                return false;
            }

            if (this.ddlCellAllocationStatus.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Status Alokasi Sel!";
                return false;
            }
            return true;
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void txtRoommate_MouseHover(object sender, EventArgs e)
        {
            txtRoommate.ForeColor = Color.DarkTurquoise;
        }

        private void txtRoommate_MouseLeave(object sender, EventArgs e)
        {
            txtRoommate.ForeColor = Color.Navy;
        }

        private void PanelRb_Paint(object sender, PaintEventArgs e)
        {

        }


        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                ClearData();
            }
            else
            {
            }
        }
    }
}
