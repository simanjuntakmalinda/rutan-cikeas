﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class CellDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CellDetail));
            this.universitas = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.picStatusCell = new Telerik.WinControls.UI.RadLabel();
            this.lblDataCell = new Telerik.WinControls.UI.RadLabel();
            this.cellnumber = new Telerik.WinControls.UI.RadLabel();
            this.lblCellNumber = new Telerik.WinControls.UI.RadLabel();
            this.celltype = new Telerik.WinControls.UI.RadLabel();
            this.lblCellType = new Telerik.WinControls.UI.RadLabel();
            this.lblTglUpdate = new Telerik.WinControls.UI.RadLabel();
            this.lblUpdater = new Telerik.WinControls.UI.RadLabel();
            this.lblTglInput = new Telerik.WinControls.UI.RadLabel();
            this.lblInputter = new Telerik.WinControls.UI.RadLabel();
            this.updatedate = new Telerik.WinControls.UI.RadLabel();
            this.updater = new Telerik.WinControls.UI.RadLabel();
            this.inputdate = new Telerik.WinControls.UI.RadLabel();
            this.inputter = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailData = new Telerik.WinControls.UI.RadLabel();
            this.lblCellStatus = new Telerik.WinControls.UI.RadLabel();
            this.cellstatus = new Telerik.WinControls.UI.RadLabel();
            this.lblCellDescription = new Telerik.WinControls.UI.RadLabel();
            this.celldescription = new Telerik.WinControls.UI.RadLabel();
            this.lblBuildingName = new Telerik.WinControls.UI.RadLabel();
            this.buildingname = new Telerik.WinControls.UI.RadLabel();
            this.lblCellFloor = new Telerik.WinControls.UI.RadLabel();
            this.cellfloor = new Telerik.WinControls.UI.RadLabel();
            this.lblCellCode = new Telerik.WinControls.UI.RadLabel();
            this.cellcode = new Telerik.WinControls.UI.RadLabel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnClose = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picStatusCell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataCell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellnumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.celltype)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUpdater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInputter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellstatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.celldescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuildingName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buildingname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellFloor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellfloor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            this.SuspendLayout();
            // 
            // universitas
            // 
            this.universitas.AutoSize = false;
            this.universitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.universitas.Location = new System.Drawing.Point(504, 414);
            this.universitas.Margin = new System.Windows.Forms.Padding(4);
            this.universitas.Name = "universitas";
            this.universitas.Size = new System.Drawing.Size(728, 42);
            this.universitas.TabIndex = 53;
            this.universitas.Text = "Tahun Lulus";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 50);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(913, 564);
            this.radScrollablePanel1.Size = new System.Drawing.Size(932, 566);
            this.radScrollablePanel1.TabIndex = 15;
            this.radScrollablePanel1.VerticalScrollBarState = Telerik.WinControls.UI.ScrollState.AlwaysShow;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.picStatusCell, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblDataCell, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.cellnumber, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblCellNumber, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.celltype, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblCellType, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblTglUpdate, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.lblUpdater, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.lblTglInput, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.lblInputter, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.updatedate, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.updater, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.inputdate, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.inputter, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailData, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.lblCellStatus, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.cellstatus, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblCellDescription, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.celldescription, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblBuildingName, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.buildingname, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblCellFloor, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.cellfloor, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblCellCode, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.cellcode, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 15;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(913, 750);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // picStatusCell
            // 
            this.picStatusCell.AutoSize = false;
            this.picStatusCell.BackColor = System.Drawing.Color.Black;
            this.picStatusCell.BorderVisible = true;
            this.picStatusCell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picStatusCell.Location = new System.Drawing.Point(364, 364);
            this.picStatusCell.Margin = new System.Windows.Forms.Padding(4);
            this.picStatusCell.Name = "picStatusCell";
            this.picStatusCell.Size = new System.Drawing.Size(42, 42);
            this.picStatusCell.TabIndex = 142;
            // 
            // lblDataCell
            // 
            this.lblDataCell.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDataCell, 2);
            this.lblDataCell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDataCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDataCell.Image = ((System.Drawing.Image)(resources.GetObject("lblDataCell.Image")));
            this.lblDataCell.Location = new System.Drawing.Point(14, 14);
            this.lblDataCell.Margin = new System.Windows.Forms.Padding(4);
            this.lblDataCell.Name = "lblDataCell";
            this.lblDataCell.Size = new System.Drawing.Size(392, 42);
            this.lblDataCell.TabIndex = 86;
            this.lblDataCell.Text = "      DETAIL SEL";
            // 
            // cellnumber
            // 
            this.cellnumber.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.cellnumber, 2);
            this.cellnumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cellnumber.Location = new System.Drawing.Point(364, 214);
            this.cellnumber.Margin = new System.Windows.Forms.Padding(4);
            this.cellnumber.Name = "cellnumber";
            this.cellnumber.Size = new System.Drawing.Size(535, 42);
            this.cellnumber.TabIndex = 60;
            // 
            // lblCellNumber
            // 
            this.lblCellNumber.AutoSize = false;
            this.lblCellNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCellNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCellNumber.Location = new System.Drawing.Point(14, 214);
            this.lblCellNumber.Margin = new System.Windows.Forms.Padding(4);
            this.lblCellNumber.Name = "lblCellNumber";
            this.lblCellNumber.Size = new System.Drawing.Size(342, 42);
            this.lblCellNumber.TabIndex = 11;
            this.lblCellNumber.Text = "NOMOR SEL";
            // 
            // celltype
            // 
            this.celltype.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.celltype, 2);
            this.celltype.Dock = System.Windows.Forms.DockStyle.Fill;
            this.celltype.Location = new System.Drawing.Point(364, 314);
            this.celltype.Margin = new System.Windows.Forms.Padding(4);
            this.celltype.Name = "celltype";
            this.celltype.Size = new System.Drawing.Size(535, 42);
            this.celltype.TabIndex = 85;
            // 
            // lblCellType
            // 
            this.lblCellType.AutoSize = false;
            this.lblCellType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCellType.Location = new System.Drawing.Point(14, 314);
            this.lblCellType.Margin = new System.Windows.Forms.Padding(4);
            this.lblCellType.Name = "lblCellType";
            this.lblCellType.Size = new System.Drawing.Size(342, 42);
            this.lblCellType.TabIndex = 83;
            this.lblCellType.Text = "TIPE SEL";
            // 
            // lblTglUpdate
            // 
            this.lblTglUpdate.AutoSize = false;
            this.lblTglUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTglUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTglUpdate.Location = new System.Drawing.Point(14, 664);
            this.lblTglUpdate.Margin = new System.Windows.Forms.Padding(4);
            this.lblTglUpdate.Name = "lblTglUpdate";
            this.lblTglUpdate.Size = new System.Drawing.Size(342, 42);
            this.lblTglUpdate.TabIndex = 74;
            this.lblTglUpdate.Text = "DIUBAH TANGGAL";
            // 
            // lblUpdater
            // 
            this.lblUpdater.AutoSize = false;
            this.lblUpdater.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUpdater.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblUpdater.Location = new System.Drawing.Point(14, 614);
            this.lblUpdater.Margin = new System.Windows.Forms.Padding(4);
            this.lblUpdater.Name = "lblUpdater";
            this.lblUpdater.Size = new System.Drawing.Size(342, 42);
            this.lblUpdater.TabIndex = 73;
            this.lblUpdater.Text = "DIUBAH OLEH";
            // 
            // lblTglInput
            // 
            this.lblTglInput.AutoSize = false;
            this.lblTglInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTglInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTglInput.Location = new System.Drawing.Point(14, 564);
            this.lblTglInput.Margin = new System.Windows.Forms.Padding(4);
            this.lblTglInput.Name = "lblTglInput";
            this.lblTglInput.Size = new System.Drawing.Size(342, 42);
            this.lblTglInput.TabIndex = 72;
            this.lblTglInput.Text = "DIBUAT TANGGAL";
            // 
            // lblInputter
            // 
            this.lblInputter.AutoSize = false;
            this.lblInputter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInputter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblInputter.Location = new System.Drawing.Point(14, 514);
            this.lblInputter.Margin = new System.Windows.Forms.Padding(4);
            this.lblInputter.Name = "lblInputter";
            this.lblInputter.Size = new System.Drawing.Size(342, 42);
            this.lblInputter.TabIndex = 68;
            this.lblInputter.Text = "DIBUAT OLEH";
            // 
            // updatedate
            // 
            this.updatedate.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.updatedate, 2);
            this.updatedate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updatedate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updatedate.Location = new System.Drawing.Point(364, 664);
            this.updatedate.Margin = new System.Windows.Forms.Padding(4);
            this.updatedate.Name = "updatedate";
            this.updatedate.Size = new System.Drawing.Size(535, 42);
            this.updatedate.TabIndex = 79;
            // 
            // updater
            // 
            this.updater.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.updater, 2);
            this.updater.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updater.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updater.Location = new System.Drawing.Point(364, 614);
            this.updater.Margin = new System.Windows.Forms.Padding(4);
            this.updater.Name = "updater";
            this.updater.Size = new System.Drawing.Size(535, 42);
            this.updater.TabIndex = 80;
            // 
            // inputdate
            // 
            this.inputdate.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.inputdate, 2);
            this.inputdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputdate.Location = new System.Drawing.Point(364, 564);
            this.inputdate.Margin = new System.Windows.Forms.Padding(4);
            this.inputdate.Name = "inputdate";
            this.inputdate.Size = new System.Drawing.Size(535, 42);
            this.inputdate.TabIndex = 78;
            // 
            // inputter
            // 
            this.inputter.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.inputter, 2);
            this.inputter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputter.Location = new System.Drawing.Point(364, 514);
            this.inputter.Margin = new System.Windows.Forms.Padding(4);
            this.inputter.Name = "inputter";
            this.inputter.Size = new System.Drawing.Size(535, 42);
            this.inputter.TabIndex = 77;
            // 
            // lblDetailData
            // 
            this.lblDetailData.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailData, 2);
            this.lblDetailData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDetailData.Image = ((System.Drawing.Image)(resources.GetObject("lblDetailData.Image")));
            this.lblDetailData.Location = new System.Drawing.Point(14, 464);
            this.lblDetailData.Margin = new System.Windows.Forms.Padding(4);
            this.lblDetailData.Name = "lblDetailData";
            this.lblDetailData.Size = new System.Drawing.Size(392, 42);
            this.lblDetailData.TabIndex = 71;
            this.lblDetailData.Text = "        DETAIL DATA";
            // 
            // lblCellStatus
            // 
            this.lblCellStatus.AutoSize = false;
            this.lblCellStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCellStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCellStatus.Location = new System.Drawing.Point(14, 364);
            this.lblCellStatus.Margin = new System.Windows.Forms.Padding(4);
            this.lblCellStatus.Name = "lblCellStatus";
            this.lblCellStatus.Size = new System.Drawing.Size(342, 42);
            this.lblCellStatus.TabIndex = 82;
            this.lblCellStatus.Text = "STATUS SEL";
            // 
            // cellstatus
            // 
            this.cellstatus.AutoSize = false;
            this.cellstatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cellstatus.Location = new System.Drawing.Point(414, 364);
            this.cellstatus.Margin = new System.Windows.Forms.Padding(4);
            this.cellstatus.Name = "cellstatus";
            this.cellstatus.Size = new System.Drawing.Size(485, 42);
            this.cellstatus.TabIndex = 84;
            // 
            // lblCellDescription
            // 
            this.lblCellDescription.AutoSize = false;
            this.lblCellDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCellDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCellDescription.Location = new System.Drawing.Point(14, 264);
            this.lblCellDescription.Margin = new System.Windows.Forms.Padding(4);
            this.lblCellDescription.Name = "lblCellDescription";
            this.lblCellDescription.Size = new System.Drawing.Size(342, 42);
            this.lblCellDescription.TabIndex = 81;
            this.lblCellDescription.Text = "DESKRIPSI SEL";
            // 
            // celldescription
            // 
            this.celldescription.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.celldescription, 2);
            this.celldescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.celldescription.Location = new System.Drawing.Point(364, 264);
            this.celldescription.Margin = new System.Windows.Forms.Padding(4);
            this.celldescription.Name = "celldescription";
            this.celldescription.Size = new System.Drawing.Size(535, 42);
            this.celldescription.TabIndex = 83;
            // 
            // lblBuildingName
            // 
            this.lblBuildingName.AutoSize = false;
            this.lblBuildingName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblBuildingName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblBuildingName.Location = new System.Drawing.Point(14, 114);
            this.lblBuildingName.Margin = new System.Windows.Forms.Padding(4);
            this.lblBuildingName.Name = "lblBuildingName";
            this.lblBuildingName.Size = new System.Drawing.Size(342, 42);
            this.lblBuildingName.TabIndex = 19;
            this.lblBuildingName.Text = "NAMA GEDUNG";
            // 
            // buildingname
            // 
            this.buildingname.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.buildingname, 2);
            this.buildingname.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buildingname.Location = new System.Drawing.Point(364, 114);
            this.buildingname.Margin = new System.Windows.Forms.Padding(4);
            this.buildingname.Name = "buildingname";
            this.buildingname.Size = new System.Drawing.Size(535, 42);
            this.buildingname.TabIndex = 58;
            // 
            // lblCellFloor
            // 
            this.lblCellFloor.AutoSize = false;
            this.lblCellFloor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCellFloor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCellFloor.Location = new System.Drawing.Point(14, 164);
            this.lblCellFloor.Margin = new System.Windows.Forms.Padding(4);
            this.lblCellFloor.Name = "lblCellFloor";
            this.lblCellFloor.Size = new System.Drawing.Size(342, 42);
            this.lblCellFloor.TabIndex = 18;
            this.lblCellFloor.Text = "LANTAI";
            // 
            // cellfloor
            // 
            this.cellfloor.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.cellfloor, 2);
            this.cellfloor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cellfloor.Location = new System.Drawing.Point(364, 164);
            this.cellfloor.Margin = new System.Windows.Forms.Padding(4);
            this.cellfloor.Name = "cellfloor";
            this.cellfloor.Size = new System.Drawing.Size(535, 42);
            this.cellfloor.TabIndex = 59;
            // 
            // lblCellCode
            // 
            this.lblCellCode.AutoSize = false;
            this.lblCellCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCellCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCellCode.Location = new System.Drawing.Point(14, 64);
            this.lblCellCode.Margin = new System.Windows.Forms.Padding(4);
            this.lblCellCode.Name = "lblCellCode";
            this.lblCellCode.Size = new System.Drawing.Size(342, 42);
            this.lblCellCode.TabIndex = 10;
            this.lblCellCode.Text = "KODE SEL";
            // 
            // cellcode
            // 
            this.cellcode.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.cellcode, 2);
            this.cellcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cellcode.Location = new System.Drawing.Point(364, 64);
            this.cellcode.Margin = new System.Windows.Forms.Padding(4);
            this.cellcode.Name = "cellcode";
            this.cellcode.Size = new System.Drawing.Size(535, 42);
            this.cellcode.TabIndex = 57;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(932, 50);
            this.lblTitle.TabIndex = 85;
            this.lblTitle.Text = "           DETAIL RUANG SEL";
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(867, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 50);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 2;
            this.exit.TabStop = false;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnClose);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 616);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(932, 122);
            this.lblButton.TabIndex = 86;
            // 
            // btnClose
            // 
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(30, 30);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(244, 62);
            this.btnClose.TabIndex = 28;
            this.btnClose.Tag = "";
            this.btnClose.Text = "&TUTUP";
            this.btnClose.ThemeName = "MaterialBlueGrey";
            // 
            // CellDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblTitle);
            this.Name = "CellDetail";
            this.Size = new System.Drawing.Size(932, 738);
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picStatusCell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataCell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellnumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.celltype)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUpdater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInputter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellstatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.celldescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuildingName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buildingname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellFloor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellfloor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCellCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel universitas;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel updater;
        private Telerik.WinControls.UI.RadLabel cellfloor;
        private Telerik.WinControls.UI.RadLabel buildingname;
        private Telerik.WinControls.UI.RadLabel cellcode;
        private Telerik.WinControls.UI.RadLabel lblCellFloor;
        private Telerik.WinControls.UI.RadLabel lblCellCode;
        private Telerik.WinControls.UI.RadLabel lblBuildingName;
        private Telerik.WinControls.UI.RadLabel lblInputter;
        private Telerik.WinControls.UI.RadLabel lblTglInput;
        private Telerik.WinControls.UI.RadLabel lblUpdater;
        private Telerik.WinControls.UI.RadLabel updatedate;
        private Telerik.WinControls.UI.RadLabel inputdate;
        private Telerik.WinControls.UI.RadLabel inputter;
        private Telerik.WinControls.UI.RadLabel lblDetailData;
        private Telerik.WinControls.UI.RadLabel lblTglUpdate;
        private Telerik.WinControls.UI.RadLabel lblCellDescription;
        private Telerik.WinControls.UI.RadLabel lblCellStatus;
        private Telerik.WinControls.UI.RadLabel celldescription;
        private Telerik.WinControls.UI.RadLabel cellstatus;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private System.Windows.Forms.PictureBox exit;
        private Telerik.WinControls.UI.RadLabel cellnumber;
        private Telerik.WinControls.UI.RadLabel lblCellNumber;
        private Telerik.WinControls.UI.RadLabel celltype;
        private Telerik.WinControls.UI.RadLabel lblCellType;
        private Telerik.WinControls.UI.RadLabel lblDataCell;
        private Telerik.WinControls.UI.RadLabel picStatusCell;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnClose;
    }
}
