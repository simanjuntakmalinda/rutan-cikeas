﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class SeizedAssetsDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SeizedAssetsDetail));
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.universitas = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblAssets = new Telerik.WinControls.UI.RadLabel();
            this.lblDataCell = new Telerik.WinControls.UI.RadLabel();
            this.lblTglUpdate = new Telerik.WinControls.UI.RadLabel();
            this.lblUpdater = new Telerik.WinControls.UI.RadLabel();
            this.lblTglInput = new Telerik.WinControls.UI.RadLabel();
            this.lblInputter = new Telerik.WinControls.UI.RadLabel();
            this.updatedate = new Telerik.WinControls.UI.RadLabel();
            this.updater = new Telerik.WinControls.UI.RadLabel();
            this.inputdate = new Telerik.WinControls.UI.RadLabel();
            this.inputter = new Telerik.WinControls.UI.RadLabel();
            this.lblCaseName = new Telerik.WinControls.UI.RadLabel();
            this.caseName = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailData = new Telerik.WinControls.UI.RadLabel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnClose = new Telerik.WinControls.UI.RadButton();
            this.gvSeizedAssets = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblAssets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataCell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUpdater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInputter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCaseName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.caseName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSeizedAssets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSeizedAssets.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // universitas
            // 
            this.universitas.AutoSize = false;
            this.universitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.universitas.Location = new System.Drawing.Point(504, 414);
            this.universitas.Margin = new System.Windows.Forms.Padding(4);
            this.universitas.Name = "universitas";
            this.universitas.Size = new System.Drawing.Size(728, 42);
            this.universitas.TabIndex = 53;
            this.universitas.Text = "Tahun Lulus";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 56);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(913, 564);
            this.radScrollablePanel1.Size = new System.Drawing.Size(932, 566);
            this.radScrollablePanel1.TabIndex = 15;
            this.radScrollablePanel1.VerticalScrollBarState = Telerik.WinControls.UI.ScrollState.AlwaysShow;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 545F));
            this.tableLayoutPanel1.Controls.Add(this.lblAssets, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblDataCell, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblTglUpdate, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.lblUpdater, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblTglInput, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblInputter, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.updatedate, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.updater, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.inputdate, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.inputter, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblCaseName, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.caseName, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailData, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.gvSeizedAssets, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(913, 800);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // lblAssets
            // 
            this.lblAssets.AutoSize = false;
            this.lblAssets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAssets.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblAssets.Location = new System.Drawing.Point(14, 114);
            this.lblAssets.Margin = new System.Windows.Forms.Padding(4);
            this.lblAssets.Name = "lblAssets";
            this.lblAssets.Size = new System.Drawing.Size(342, 42);
            this.lblAssets.TabIndex = 88;
            this.lblAssets.Text = "DAFTAR BENDA SITAAN";
            // 
            // lblDataCell
            // 
            this.lblDataCell.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDataCell, 2);
            this.lblDataCell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDataCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDataCell.Image = ((System.Drawing.Image)(resources.GetObject("lblDataCell.Image")));
            this.lblDataCell.Location = new System.Drawing.Point(14, 14);
            this.lblDataCell.Margin = new System.Windows.Forms.Padding(4);
            this.lblDataCell.Name = "lblDataCell";
            this.lblDataCell.Size = new System.Drawing.Size(887, 42);
            this.lblDataCell.TabIndex = 86;
            this.lblDataCell.Text = "        DETAIL BENDA SITAAN";
            // 
            // lblTglUpdate
            // 
            this.lblTglUpdate.AutoSize = false;
            this.lblTglUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTglUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTglUpdate.Location = new System.Drawing.Point(14, 714);
            this.lblTglUpdate.Margin = new System.Windows.Forms.Padding(4);
            this.lblTglUpdate.Name = "lblTglUpdate";
            this.lblTglUpdate.Size = new System.Drawing.Size(342, 42);
            this.lblTglUpdate.TabIndex = 74;
            this.lblTglUpdate.Text = "DIUBAH TANGGAL";
            // 
            // lblUpdater
            // 
            this.lblUpdater.AutoSize = false;
            this.lblUpdater.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUpdater.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblUpdater.Location = new System.Drawing.Point(14, 664);
            this.lblUpdater.Margin = new System.Windows.Forms.Padding(4);
            this.lblUpdater.Name = "lblUpdater";
            this.lblUpdater.Size = new System.Drawing.Size(342, 42);
            this.lblUpdater.TabIndex = 73;
            this.lblUpdater.Text = "DIUBAH OLEH";
            // 
            // lblTglInput
            // 
            this.lblTglInput.AutoSize = false;
            this.lblTglInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTglInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTglInput.Location = new System.Drawing.Point(14, 614);
            this.lblTglInput.Margin = new System.Windows.Forms.Padding(4);
            this.lblTglInput.Name = "lblTglInput";
            this.lblTglInput.Size = new System.Drawing.Size(342, 42);
            this.lblTglInput.TabIndex = 72;
            this.lblTglInput.Text = "DIBUAT TANGGAL";
            // 
            // lblInputter
            // 
            this.lblInputter.AutoSize = false;
            this.lblInputter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInputter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblInputter.Location = new System.Drawing.Point(14, 564);
            this.lblInputter.Margin = new System.Windows.Forms.Padding(4);
            this.lblInputter.Name = "lblInputter";
            this.lblInputter.Size = new System.Drawing.Size(342, 42);
            this.lblInputter.TabIndex = 68;
            this.lblInputter.Text = "DIBUAT OLEH";
            // 
            // updatedate
            // 
            this.updatedate.AutoSize = false;
            this.updatedate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updatedate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updatedate.Location = new System.Drawing.Point(364, 714);
            this.updatedate.Margin = new System.Windows.Forms.Padding(4);
            this.updatedate.Name = "updatedate";
            this.updatedate.Size = new System.Drawing.Size(537, 42);
            this.updatedate.TabIndex = 79;
            // 
            // updater
            // 
            this.updater.AutoSize = false;
            this.updater.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updater.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updater.Location = new System.Drawing.Point(364, 664);
            this.updater.Margin = new System.Windows.Forms.Padding(4);
            this.updater.Name = "updater";
            this.updater.Size = new System.Drawing.Size(537, 42);
            this.updater.TabIndex = 80;
            // 
            // inputdate
            // 
            this.inputdate.AutoSize = false;
            this.inputdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputdate.Location = new System.Drawing.Point(364, 614);
            this.inputdate.Margin = new System.Windows.Forms.Padding(4);
            this.inputdate.Name = "inputdate";
            this.inputdate.Size = new System.Drawing.Size(537, 42);
            this.inputdate.TabIndex = 78;
            // 
            // inputter
            // 
            this.inputter.AutoSize = false;
            this.inputter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputter.Location = new System.Drawing.Point(364, 564);
            this.inputter.Margin = new System.Windows.Forms.Padding(4);
            this.inputter.Name = "inputter";
            this.inputter.Size = new System.Drawing.Size(537, 42);
            this.inputter.TabIndex = 77;
            // 
            // lblCaseName
            // 
            this.lblCaseName.AutoSize = false;
            this.lblCaseName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCaseName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCaseName.Location = new System.Drawing.Point(14, 64);
            this.lblCaseName.Margin = new System.Windows.Forms.Padding(4);
            this.lblCaseName.Name = "lblCaseName";
            this.lblCaseName.Size = new System.Drawing.Size(342, 42);
            this.lblCaseName.TabIndex = 10;
            this.lblCaseName.Text = "NAMA KASUS";
            // 
            // caseName
            // 
            this.caseName.AutoSize = false;
            this.caseName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.caseName.Location = new System.Drawing.Point(364, 64);
            this.caseName.Margin = new System.Windows.Forms.Padding(4);
            this.caseName.Name = "caseName";
            this.caseName.Size = new System.Drawing.Size(537, 42);
            this.caseName.TabIndex = 57;
            // 
            // lblDetailData
            // 
            this.lblDetailData.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailData, 2);
            this.lblDetailData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDetailData.Image = ((System.Drawing.Image)(resources.GetObject("lblDetailData.Image")));
            this.lblDetailData.Location = new System.Drawing.Point(14, 514);
            this.lblDetailData.Margin = new System.Windows.Forms.Padding(4);
            this.lblDetailData.Name = "lblDetailData";
            this.lblDetailData.Size = new System.Drawing.Size(887, 42);
            this.lblDetailData.TabIndex = 71;
            this.lblDetailData.Text = "      DETAIL DATA";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(932, 56);
            this.lblTitle.TabIndex = 85;
            this.lblTitle.Text = "           DETAIL BENDA SITAAN";
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(867, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 56);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 2;
            this.exit.TabStop = false;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnClose);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 666);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(932, 122);
            this.lblButton.TabIndex = 86;
            // 
            // btnClose
            // 
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(30, 30);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(244, 62);
            this.btnClose.TabIndex = 28;
            this.btnClose.Tag = "";
            this.btnClose.Text = "&TUTUP";
            this.btnClose.ThemeName = "MaterialBlueGrey";
            // 
            // gvSeizedAssets
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.gvSeizedAssets, 2);
            this.gvSeizedAssets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvSeizedAssets.Location = new System.Drawing.Point(13, 163);
            // 
            // 
            // 
            this.gvSeizedAssets.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.gvSeizedAssets.Name = "gvSeizedAssets";
            this.gvSeizedAssets.Size = new System.Drawing.Size(889, 294);
            this.gvSeizedAssets.TabIndex = 87;
            // 
            // SeizedAssetsDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblTitle);
            this.Name = "SeizedAssetsDetail";
            this.Size = new System.Drawing.Size(932, 788);
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblAssets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataCell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUpdater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInputter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCaseName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.caseName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSeizedAssets.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSeizedAssets)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel universitas;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel updater;
        private Telerik.WinControls.UI.RadLabel caseName;
        private Telerik.WinControls.UI.RadLabel lblCaseName;
        private Telerik.WinControls.UI.RadLabel lblInputter;
        private Telerik.WinControls.UI.RadLabel lblTglInput;
        private Telerik.WinControls.UI.RadLabel lblUpdater;
        private Telerik.WinControls.UI.RadLabel updatedate;
        private Telerik.WinControls.UI.RadLabel inputdate;
        private Telerik.WinControls.UI.RadLabel inputter;
        private Telerik.WinControls.UI.RadLabel lblDetailData;
        private Telerik.WinControls.UI.RadLabel lblTglUpdate;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private System.Windows.Forms.PictureBox exit;
        private Telerik.WinControls.UI.RadLabel lblDataCell;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnClose;
        private Telerik.WinControls.UI.RadLabel lblAssets;
        private Telerik.WinControls.UI.RadGridView gvSeizedAssets;
    }
}
