﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class VisitorVIPDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisitorVIPDetail));
            this.universitas = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblIdentitas = new Telerik.WinControls.UI.RadLabel();
            this.identitas = new Telerik.WinControls.UI.RadLabel();
            this.lblDataVisitor = new Telerik.WinControls.UI.RadLabel();
            this.checkout = new Telerik.WinControls.UI.RadLabel();
            this.lblCheckout = new Telerik.WinControls.UI.RadLabel();
            this.checkin = new Telerik.WinControls.UI.RadLabel();
            this.lblCheckin = new Telerik.WinControls.UI.RadLabel();
            this.catatan = new Telerik.WinControls.UI.RadLabel();
            this.lblCatatan = new Telerik.WinControls.UI.RadLabel();
            this.tujuan = new Telerik.WinControls.UI.RadLabel();
            this.lblTujuan = new Telerik.WinControls.UI.RadLabel();
            this.lblAlamat = new Telerik.WinControls.UI.RadLabel();
            this.alamat = new Telerik.WinControls.UI.RadLabel();
            this.lblNama = new Telerik.WinControls.UI.RadLabel();
            this.namalengkap = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailData = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.inputter = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.inputdate = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.updater = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.updatedate = new Telerik.WinControls.UI.RadLabel();
            this.fullpicPanel = new Telerik.WinControls.UI.RadPanel();
            this.closefull = new System.Windows.Forms.PictureBox();
            this.fullPic = new System.Windows.Forms.PictureBox();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnClose = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblIdentitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.identitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataVisitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCheckout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCheckin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.catatan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCatatan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tujuan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTujuan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAlamat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alamat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.namalengkap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullpicPanel)).BeginInit();
            this.fullpicPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closefull)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            this.SuspendLayout();
            // 
            // universitas
            // 
            this.universitas.AutoSize = false;
            this.universitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.universitas.Location = new System.Drawing.Point(504, 414);
            this.universitas.Margin = new System.Windows.Forms.Padding(4);
            this.universitas.Name = "universitas";
            this.universitas.Size = new System.Drawing.Size(728, 42);
            this.universitas.TabIndex = 53;
            this.universitas.Text = "Tahun Lulus";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 56);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(1086, 600);
            this.radScrollablePanel1.Size = new System.Drawing.Size(1105, 602);
            this.radScrollablePanel1.TabIndex = 15;
            this.radScrollablePanel1.VerticalScrollBarState = Telerik.WinControls.UI.ScrollState.AlwaysShow;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel1.Controls.Add(this.lblIdentitas, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.identitas, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDataVisitor, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkout, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblCheckout, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.checkin, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblCheckin, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.catatan, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblCatatan, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.tujuan, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblTujuan, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblAlamat, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.alamat, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblNama, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.namalengkap, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailData, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.radLabel7, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.inputter, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.radLabel12, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.inputdate, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.radLabel13, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.updater, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.radLabel14, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.updatedate, 1, 13);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 15;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1086, 750);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // lblIdentitas
            // 
            this.lblIdentitas.AutoSize = false;
            this.lblIdentitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIdentitas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblIdentitas.Location = new System.Drawing.Point(14, 64);
            this.lblIdentitas.Margin = new System.Windows.Forms.Padding(4);
            this.lblIdentitas.Name = "lblIdentitas";
            this.lblIdentitas.Size = new System.Drawing.Size(292, 42);
            this.lblIdentitas.TabIndex = 11;
            this.lblIdentitas.Text = "Jenis / Nomor Identitas";
            // 
            // identitas
            // 
            this.identitas.AutoSize = false;
            this.identitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.identitas.Location = new System.Drawing.Point(314, 64);
            this.identitas.Margin = new System.Windows.Forms.Padding(4);
            this.identitas.Name = "identitas";
            this.identitas.Size = new System.Drawing.Size(706, 42);
            this.identitas.TabIndex = 58;
            // 
            // lblDataVisitor
            // 
            this.lblDataVisitor.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDataVisitor, 2);
            this.lblDataVisitor.Cursor = System.Windows.Forms.Cursors.No;
            this.lblDataVisitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDataVisitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDataVisitor.Image = ((System.Drawing.Image)(resources.GetObject("lblDataVisitor.Image")));
            this.lblDataVisitor.Location = new System.Drawing.Point(14, 14);
            this.lblDataVisitor.Margin = new System.Windows.Forms.Padding(4);
            this.lblDataVisitor.Name = "lblDataVisitor";
            this.lblDataVisitor.Size = new System.Drawing.Size(1006, 42);
            this.lblDataVisitor.TabIndex = 62;
            this.lblDataVisitor.Text = "       DETAIL PENGUNJUNG VIP";
            // 
            // checkout
            // 
            this.checkout.AutoSize = false;
            this.checkout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkout.Location = new System.Drawing.Point(314, 364);
            this.checkout.Margin = new System.Windows.Forms.Padding(4);
            this.checkout.Name = "checkout";
            this.checkout.Size = new System.Drawing.Size(706, 42);
            this.checkout.TabIndex = 54;
            // 
            // lblCheckout
            // 
            this.lblCheckout.AutoSize = false;
            this.lblCheckout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCheckout.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCheckout.Location = new System.Drawing.Point(14, 364);
            this.lblCheckout.Margin = new System.Windows.Forms.Padding(4);
            this.lblCheckout.Name = "lblCheckout";
            this.lblCheckout.Size = new System.Drawing.Size(292, 42);
            this.lblCheckout.TabIndex = 33;
            this.lblCheckout.Text = "Check Out";
            // 
            // checkin
            // 
            this.checkin.AutoSize = false;
            this.checkin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkin.Location = new System.Drawing.Point(314, 314);
            this.checkin.Margin = new System.Windows.Forms.Padding(4);
            this.checkin.Name = "checkin";
            this.checkin.Size = new System.Drawing.Size(706, 42);
            this.checkin.TabIndex = 55;
            // 
            // lblCheckin
            // 
            this.lblCheckin.AutoSize = false;
            this.lblCheckin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCheckin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCheckin.Location = new System.Drawing.Point(14, 314);
            this.lblCheckin.Margin = new System.Windows.Forms.Padding(4);
            this.lblCheckin.Name = "lblCheckin";
            this.lblCheckin.Size = new System.Drawing.Size(292, 42);
            this.lblCheckin.TabIndex = 36;
            this.lblCheckin.Text = "Check In";
            // 
            // catatan
            // 
            this.catatan.AutoSize = false;
            this.catatan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.catatan.Location = new System.Drawing.Point(314, 264);
            this.catatan.Margin = new System.Windows.Forms.Padding(4);
            this.catatan.Name = "catatan";
            this.catatan.Size = new System.Drawing.Size(706, 42);
            this.catatan.TabIndex = 56;
            // 
            // lblCatatan
            // 
            this.lblCatatan.AutoSize = false;
            this.lblCatatan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCatatan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblCatatan.Location = new System.Drawing.Point(14, 264);
            this.lblCatatan.Margin = new System.Windows.Forms.Padding(4);
            this.lblCatatan.Name = "lblCatatan";
            this.lblCatatan.Size = new System.Drawing.Size(292, 42);
            this.lblCatatan.TabIndex = 34;
            this.lblCatatan.Text = "Catatan";
            // 
            // tujuan
            // 
            this.tujuan.AutoSize = false;
            this.tujuan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tujuan.Location = new System.Drawing.Point(314, 214);
            this.tujuan.Margin = new System.Windows.Forms.Padding(4);
            this.tujuan.Name = "tujuan";
            this.tujuan.Size = new System.Drawing.Size(706, 42);
            this.tujuan.TabIndex = 60;
            // 
            // lblTujuan
            // 
            this.lblTujuan.AutoSize = false;
            this.lblTujuan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTujuan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTujuan.Location = new System.Drawing.Point(14, 214);
            this.lblTujuan.Margin = new System.Windows.Forms.Padding(4);
            this.lblTujuan.Name = "lblTujuan";
            this.lblTujuan.Size = new System.Drawing.Size(292, 42);
            this.lblTujuan.TabIndex = 35;
            this.lblTujuan.Text = "Tujuan";
            // 
            // lblAlamat
            // 
            this.lblAlamat.AutoSize = false;
            this.lblAlamat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAlamat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblAlamat.Location = new System.Drawing.Point(14, 164);
            this.lblAlamat.Margin = new System.Windows.Forms.Padding(4);
            this.lblAlamat.Name = "lblAlamat";
            this.lblAlamat.Size = new System.Drawing.Size(292, 42);
            this.lblAlamat.TabIndex = 37;
            this.lblAlamat.Text = "Alamat";
            // 
            // alamat
            // 
            this.alamat.AutoSize = false;
            this.alamat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.alamat.Location = new System.Drawing.Point(314, 164);
            this.alamat.Margin = new System.Windows.Forms.Padding(4);
            this.alamat.Name = "alamat";
            this.alamat.Size = new System.Drawing.Size(706, 42);
            this.alamat.TabIndex = 62;
            // 
            // lblNama
            // 
            this.lblNama.AutoSize = false;
            this.lblNama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNama.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNama.Location = new System.Drawing.Point(14, 114);
            this.lblNama.Margin = new System.Windows.Forms.Padding(4);
            this.lblNama.Name = "lblNama";
            this.lblNama.Size = new System.Drawing.Size(292, 42);
            this.lblNama.TabIndex = 10;
            this.lblNama.Text = "Nama Lengkap";
            // 
            // namalengkap
            // 
            this.namalengkap.AutoSize = false;
            this.namalengkap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.namalengkap.Location = new System.Drawing.Point(314, 114);
            this.namalengkap.Margin = new System.Windows.Forms.Padding(4);
            this.namalengkap.Name = "namalengkap";
            this.namalengkap.Size = new System.Drawing.Size(706, 42);
            this.namalengkap.TabIndex = 57;
            // 
            // lblDetailData
            // 
            this.lblDetailData.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailData, 2);
            this.lblDetailData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDetailData.Image = ((System.Drawing.Image)(resources.GetObject("lblDetailData.Image")));
            this.lblDetailData.Location = new System.Drawing.Point(14, 464);
            this.lblDetailData.Margin = new System.Windows.Forms.Padding(4);
            this.lblDetailData.Name = "lblDetailData";
            this.lblDetailData.Size = new System.Drawing.Size(1006, 42);
            this.lblDetailData.TabIndex = 71;
            this.lblDetailData.Text = "        LOG DATA";
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel7.Location = new System.Drawing.Point(14, 514);
            this.radLabel7.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(292, 42);
            this.radLabel7.TabIndex = 68;
            this.radLabel7.Text = "DIBUAT OLEH";
            // 
            // inputter
            // 
            this.inputter.AutoSize = false;
            this.inputter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputter.Location = new System.Drawing.Point(314, 514);
            this.inputter.Margin = new System.Windows.Forms.Padding(4);
            this.inputter.Name = "inputter";
            this.inputter.Size = new System.Drawing.Size(706, 42);
            this.inputter.TabIndex = 77;
            // 
            // radLabel12
            // 
            this.radLabel12.AutoSize = false;
            this.radLabel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel12.Location = new System.Drawing.Point(14, 564);
            this.radLabel12.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(292, 42);
            this.radLabel12.TabIndex = 72;
            this.radLabel12.Text = "DIBUAT TANGGAL";
            // 
            // inputdate
            // 
            this.inputdate.AutoSize = false;
            this.inputdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputdate.Location = new System.Drawing.Point(314, 564);
            this.inputdate.Margin = new System.Windows.Forms.Padding(4);
            this.inputdate.Name = "inputdate";
            this.inputdate.Size = new System.Drawing.Size(706, 42);
            this.inputdate.TabIndex = 78;
            // 
            // radLabel13
            // 
            this.radLabel13.AutoSize = false;
            this.radLabel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel13.Location = new System.Drawing.Point(14, 614);
            this.radLabel13.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(292, 42);
            this.radLabel13.TabIndex = 73;
            this.radLabel13.Text = "DIUBAH OLEH";
            // 
            // updater
            // 
            this.updater.AutoSize = false;
            this.updater.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updater.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updater.Location = new System.Drawing.Point(314, 614);
            this.updater.Margin = new System.Windows.Forms.Padding(4);
            this.updater.Name = "updater";
            this.updater.Size = new System.Drawing.Size(706, 42);
            this.updater.TabIndex = 80;
            // 
            // radLabel14
            // 
            this.radLabel14.AutoSize = false;
            this.radLabel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel14.Location = new System.Drawing.Point(14, 664);
            this.radLabel14.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(292, 42);
            this.radLabel14.TabIndex = 74;
            this.radLabel14.Text = "DIUBAH TANGGAL";
            // 
            // updatedate
            // 
            this.updatedate.AutoSize = false;
            this.updatedate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updatedate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updatedate.Location = new System.Drawing.Point(314, 664);
            this.updatedate.Margin = new System.Windows.Forms.Padding(4);
            this.updatedate.Name = "updatedate";
            this.updatedate.Size = new System.Drawing.Size(706, 42);
            this.updatedate.TabIndex = 79;
            // 
            // fullpicPanel
            // 
            this.fullpicPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fullpicPanel.Controls.Add(this.closefull);
            this.fullpicPanel.Controls.Add(this.fullPic);
            this.fullpicPanel.Location = new System.Drawing.Point(7600, 22);
            this.fullpicPanel.Name = "fullpicPanel";
            this.fullpicPanel.Padding = new System.Windows.Forms.Padding(5);
            this.fullpicPanel.Size = new System.Drawing.Size(603, 641);
            this.fullpicPanel.TabIndex = 84;
            this.fullpicPanel.Visible = false;
            // 
            // closefull
            // 
            this.closefull.BackColor = System.Drawing.Color.Transparent;
            this.closefull.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.closefull.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closefull.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.delete1;
            this.closefull.Location = new System.Drawing.Point(553, 5);
            this.closefull.Name = "closefull";
            this.closefull.Size = new System.Drawing.Size(45, 45);
            this.closefull.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.closefull.TabIndex = 3;
            this.closefull.TabStop = false;
            // 
            // fullPic
            // 
            this.fullPic.BackColor = System.Drawing.Color.White;
            this.fullPic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fullPic.Location = new System.Drawing.Point(5, 5);
            this.fullPic.Name = "fullPic";
            this.fullPic.Size = new System.Drawing.Size(593, 631);
            this.fullPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fullPic.TabIndex = 0;
            this.fullPic.TabStop = false;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1105, 56);
            this.lblTitle.TabIndex = 8;
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(1040, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 56);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 1;
            this.exit.TabStop = false;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnClose);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 664);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(1105, 122);
            this.lblButton.TabIndex = 89;
            // 
            // btnClose
            // 
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(30, 30);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(244, 62);
            this.btnClose.TabIndex = 28;
            this.btnClose.Tag = "";
            this.btnClose.Text = "&TUTUP";
            this.btnClose.ThemeName = "MaterialBlueGrey";
            // 
            // VisitorVIPDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.fullpicPanel);
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblTitle);
            this.Name = "VisitorVIPDetail";
            this.Size = new System.Drawing.Size(1105, 786);
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblIdentitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.identitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataVisitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCheckout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCheckin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.catatan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCatatan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tujuan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTujuan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAlamat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alamat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.namalengkap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullpicPanel)).EndInit();
            this.fullpicPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.closefull)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel universitas;
        private System.Windows.Forms.PictureBox exit;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel updater;
        private Telerik.WinControls.UI.RadLabel inputdate;
        private Telerik.WinControls.UI.RadLabel tujuan;
        private Telerik.WinControls.UI.RadLabel namalengkap;
        private Telerik.WinControls.UI.RadLabel catatan;
        private Telerik.WinControls.UI.RadLabel lblTujuan;
        private Telerik.WinControls.UI.RadLabel lblCatatan;
        private Telerik.WinControls.UI.RadLabel lblCheckout;
        private Telerik.WinControls.UI.RadLabel lblNama;
        private Telerik.WinControls.UI.RadLabel checkout;
        private Telerik.WinControls.UI.RadLabel checkin;
        private Telerik.WinControls.UI.RadLabel lblCheckin;
        private Telerik.WinControls.UI.RadLabel lblDataVisitor;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel inputter;
        private Telerik.WinControls.UI.RadLabel lblDetailData;
        private Telerik.WinControls.UI.RadLabel updatedate;
        private Telerik.WinControls.UI.RadPanel fullpicPanel;
        private System.Windows.Forms.PictureBox fullPic;
        private System.Windows.Forms.PictureBox closefull;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnClose;
        private Telerik.WinControls.UI.RadLabel lblAlamat;
        private Telerik.WinControls.UI.RadLabel alamat;
        private Telerik.WinControls.UI.RadLabel lblIdentitas;
        private Telerik.WinControls.UI.RadLabel identitas;
    }
}
