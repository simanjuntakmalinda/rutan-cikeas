﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class TahananDetailCheckedOut
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TahananDetailCheckedOut));
            this.universitas = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.updater = new Telerik.WinControls.UI.RadLabel();
            this.inputdate = new Telerik.WinControls.UI.RadLabel();
            this.waktumulai = new Telerik.WinControls.UI.RadLabel();
            this.namatahanan = new Telerik.WinControls.UI.RadLabel();
            this.idregister = new Telerik.WinControls.UI.RadLabel();
            this.notes = new Telerik.WinControls.UI.RadLabel();
            this.lblAgama = new Telerik.WinControls.UI.RadLabel();
            this.lblTelp = new Telerik.WinControls.UI.RadLabel();
            this.lblJenisKelamin = new Telerik.WinControls.UI.RadLabel();
            this.lblNama = new Telerik.WinControls.UI.RadLabel();
            this.lblTglLahir = new Telerik.WinControls.UI.RadLabel();
            this.lblDataVisit = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.inputter = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailData = new Telerik.WinControls.UI.RadLabel();
            this.updatedate = new Telerik.WinControls.UI.RadLabel();
            this.waktuselesai = new Telerik.WinControls.UI.RadLabel();
            this.btnCheckOut = new System.Windows.Forms.Button();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.jeniskelamin = new Telerik.WinControls.UI.RadLabel();
            this.alamat = new Telerik.WinControls.UI.RadLabel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnClose = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waktumulai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.namatahanan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.idregister)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.notes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAgama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJenisKelamin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglLahir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waktuselesai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jeniskelamin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alamat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            this.SuspendLayout();
            // 
            // universitas
            // 
            this.universitas.AutoSize = false;
            this.universitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.universitas.Location = new System.Drawing.Point(504, 414);
            this.universitas.Margin = new System.Windows.Forms.Padding(4);
            this.universitas.Name = "universitas";
            this.universitas.Size = new System.Drawing.Size(728, 42);
            this.universitas.TabIndex = 53;
            this.universitas.Text = "Tahun Lulus";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 56);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(976, 512);
            this.radScrollablePanel1.Size = new System.Drawing.Size(995, 514);
            this.radScrollablePanel1.TabIndex = 15;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 408F));
            this.tableLayoutPanel1.Controls.Add(this.updater, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.inputdate, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.waktumulai, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.namatahanan, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.idregister, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.notes, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblAgama, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblTelp, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblJenisKelamin, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblNama, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblTglLahir, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblDataVisit, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radLabel7, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.radLabel12, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.radLabel13, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.radLabel14, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.inputter, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailData, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.updatedate, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.waktuselesai, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.btnCheckOut, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.radLabel2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.jeniskelamin, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.alamat, 1, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Symbol", 8.25F);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 15;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(976, 750);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // updater
            // 
            this.updater.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.updater, 2);
            this.updater.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updater.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updater.Location = new System.Drawing.Point(264, 614);
            this.updater.Margin = new System.Windows.Forms.Padding(4);
            this.updater.Name = "updater";
            this.updater.Size = new System.Drawing.Size(700, 42);
            this.updater.TabIndex = 80;
            // 
            // inputdate
            // 
            this.inputdate.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.inputdate, 2);
            this.inputdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputdate.Location = new System.Drawing.Point(264, 564);
            this.inputdate.Margin = new System.Windows.Forms.Padding(4);
            this.inputdate.Name = "inputdate";
            this.inputdate.Size = new System.Drawing.Size(700, 42);
            this.inputdate.TabIndex = 78;
            // 
            // waktumulai
            // 
            this.waktumulai.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.waktumulai, 2);
            this.waktumulai.Location = new System.Drawing.Point(264, 264);
            this.waktumulai.Margin = new System.Windows.Forms.Padding(4);
            this.waktumulai.Name = "waktumulai";
            this.waktumulai.Size = new System.Drawing.Size(600, 42);
            this.waktumulai.TabIndex = 59;
            // 
            // namatahanan
            // 
            this.namatahanan.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.namatahanan, 2);
            this.namatahanan.Location = new System.Drawing.Point(264, 114);
            this.namatahanan.Margin = new System.Windows.Forms.Padding(4);
            this.namatahanan.Name = "namatahanan";
            this.namatahanan.Size = new System.Drawing.Size(600, 42);
            this.namatahanan.TabIndex = 58;
            // 
            // idregister
            // 
            this.idregister.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.idregister, 2);
            this.idregister.Dock = System.Windows.Forms.DockStyle.Fill;
            this.idregister.Location = new System.Drawing.Point(264, 64);
            this.idregister.Margin = new System.Windows.Forms.Padding(4);
            this.idregister.Name = "idregister";
            this.idregister.Size = new System.Drawing.Size(700, 42);
            this.idregister.TabIndex = 57;
            // 
            // notes
            // 
            this.notes.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.notes, 2);
            this.notes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notes.Location = new System.Drawing.Point(264, 364);
            this.notes.Margin = new System.Windows.Forms.Padding(4);
            this.notes.Name = "notes";
            this.notes.Size = new System.Drawing.Size(700, 42);
            this.notes.TabIndex = 56;
            // 
            // lblAgama
            // 
            this.lblAgama.AutoSize = false;
            this.lblAgama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAgama.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblAgama.Location = new System.Drawing.Point(14, 314);
            this.lblAgama.Margin = new System.Windows.Forms.Padding(4);
            this.lblAgama.Name = "lblAgama";
            this.lblAgama.Size = new System.Drawing.Size(242, 42);
            this.lblAgama.TabIndex = 35;
            this.lblAgama.Text = "WAKTU KELUAR";
            // 
            // lblTelp
            // 
            this.lblTelp.AutoSize = false;
            this.lblTelp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTelp.Location = new System.Drawing.Point(14, 364);
            this.lblTelp.Margin = new System.Windows.Forms.Padding(4);
            this.lblTelp.Name = "lblTelp";
            this.lblTelp.Size = new System.Drawing.Size(242, 42);
            this.lblTelp.TabIndex = 34;
            this.lblTelp.Text = "CATATAN";
            // 
            // lblJenisKelamin
            // 
            this.lblJenisKelamin.AutoSize = false;
            this.lblJenisKelamin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblJenisKelamin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblJenisKelamin.Location = new System.Drawing.Point(14, 264);
            this.lblJenisKelamin.Margin = new System.Windows.Forms.Padding(4);
            this.lblJenisKelamin.Name = "lblJenisKelamin";
            this.lblJenisKelamin.Size = new System.Drawing.Size(242, 42);
            this.lblJenisKelamin.TabIndex = 18;
            this.lblJenisKelamin.Text = "WAKTU DAFTAR";
            // 
            // lblNama
            // 
            this.lblNama.AutoSize = false;
            this.lblNama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNama.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNama.Location = new System.Drawing.Point(14, 64);
            this.lblNama.Margin = new System.Windows.Forms.Padding(4);
            this.lblNama.Name = "lblNama";
            this.lblNama.Size = new System.Drawing.Size(242, 42);
            this.lblNama.TabIndex = 10;
            this.lblNama.Text = "ID REG";
            // 
            // lblTglLahir
            // 
            this.lblTglLahir.AutoSize = false;
            this.lblTglLahir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTglLahir.Location = new System.Drawing.Point(14, 114);
            this.lblTglLahir.Margin = new System.Windows.Forms.Padding(4);
            this.lblTglLahir.Name = "lblTglLahir";
            this.lblTglLahir.Size = new System.Drawing.Size(242, 42);
            this.lblTglLahir.TabIndex = 19;
            this.lblTglLahir.Text = "NAMA TAHANAN";
            // 
            // lblDataVisit
            // 
            this.lblDataVisit.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDataVisit, 3);
            this.lblDataVisit.Cursor = System.Windows.Forms.Cursors.No;
            this.lblDataVisit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDataVisit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDataVisit.Image = ((System.Drawing.Image)(resources.GetObject("lblDataVisit.Image")));
            this.lblDataVisit.Location = new System.Drawing.Point(14, 14);
            this.lblDataVisit.Margin = new System.Windows.Forms.Padding(4);
            this.lblDataVisit.Name = "lblDataVisit";
            this.lblDataVisit.Size = new System.Drawing.Size(950, 42);
            this.lblDataVisit.TabIndex = 62;
            this.lblDataVisit.Text = "       DETAIL TAHANAN";
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel7.Location = new System.Drawing.Point(14, 514);
            this.radLabel7.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(242, 40);
            this.radLabel7.TabIndex = 68;
            this.radLabel7.Text = "DIBUAT OLEH";
            // 
            // radLabel12
            // 
            this.radLabel12.AutoSize = false;
            this.radLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel12.Location = new System.Drawing.Point(14, 564);
            this.radLabel12.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(242, 42);
            this.radLabel12.TabIndex = 72;
            this.radLabel12.Text = "DIBUAT TANGGAL";
            // 
            // radLabel13
            // 
            this.radLabel13.AutoSize = false;
            this.radLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel13.Location = new System.Drawing.Point(14, 614);
            this.radLabel13.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(242, 42);
            this.radLabel13.TabIndex = 73;
            this.radLabel13.Text = "DIUBAH OLEH";
            // 
            // radLabel14
            // 
            this.radLabel14.AutoSize = false;
            this.radLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel14.Location = new System.Drawing.Point(14, 664);
            this.radLabel14.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(242, 42);
            this.radLabel14.TabIndex = 74;
            this.radLabel14.Text = "DIUBAH TANGGAL";
            // 
            // inputter
            // 
            this.inputter.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.inputter, 2);
            this.inputter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputter.Location = new System.Drawing.Point(264, 514);
            this.inputter.Margin = new System.Windows.Forms.Padding(4);
            this.inputter.Name = "inputter";
            this.inputter.Size = new System.Drawing.Size(700, 42);
            this.inputter.TabIndex = 77;
            // 
            // lblDetailData
            // 
            this.lblDetailData.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailData, 2);
            this.lblDetailData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDetailData.Image = ((System.Drawing.Image)(resources.GetObject("lblDetailData.Image")));
            this.lblDetailData.Location = new System.Drawing.Point(14, 464);
            this.lblDetailData.Margin = new System.Windows.Forms.Padding(4);
            this.lblDetailData.Name = "lblDetailData";
            this.lblDetailData.Size = new System.Drawing.Size(542, 42);
            this.lblDetailData.TabIndex = 71;
            this.lblDetailData.Text = "        LOG DATA ";
            // 
            // updatedate
            // 
            this.updatedate.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.updatedate, 2);
            this.updatedate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updatedate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updatedate.Location = new System.Drawing.Point(264, 664);
            this.updatedate.Margin = new System.Windows.Forms.Padding(4);
            this.updatedate.Name = "updatedate";
            this.updatedate.Size = new System.Drawing.Size(700, 42);
            this.updatedate.TabIndex = 79;
            // 
            // waktuselesai
            // 
            this.waktuselesai.AutoSize = false;
            this.waktuselesai.Location = new System.Drawing.Point(264, 314);
            this.waktuselesai.Margin = new System.Windows.Forms.Padding(4);
            this.waktuselesai.Name = "waktuselesai";
            this.waktuselesai.Size = new System.Drawing.Size(292, 42);
            this.waktuselesai.TabIndex = 60;
            // 
            // btnCheckOut
            // 
            this.btnCheckOut.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnCheckOut.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckOut.Image = ((System.Drawing.Image)(resources.GetObject("btnCheckOut.Image")));
            this.btnCheckOut.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCheckOut.Location = new System.Drawing.Point(563, 313);
            this.btnCheckOut.Name = "btnCheckOut";
            this.btnCheckOut.Padding = new System.Windows.Forms.Padding(5);
            this.btnCheckOut.Size = new System.Drawing.Size(207, 44);
            this.btnCheckOut.TabIndex = 93;
            this.btnCheckOut.Text = "    KELUAR";
            this.btnCheckOut.UseVisualStyleBackColor = false;
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel1.Location = new System.Drawing.Point(14, 164);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(242, 42);
            this.radLabel1.TabIndex = 94;
            this.radLabel1.Text = "JENIS KELAMIN";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel2.Location = new System.Drawing.Point(14, 214);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(242, 42);
            this.radLabel2.TabIndex = 95;
            this.radLabel2.Text = "ALAMAT";
            // 
            // jeniskelamin
            // 
            this.jeniskelamin.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.jeniskelamin, 2);
            this.jeniskelamin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.jeniskelamin.Location = new System.Drawing.Point(264, 164);
            this.jeniskelamin.Margin = new System.Windows.Forms.Padding(4);
            this.jeniskelamin.Name = "jeniskelamin";
            this.jeniskelamin.Size = new System.Drawing.Size(700, 42);
            this.jeniskelamin.TabIndex = 96;
            // 
            // alamat
            // 
            this.alamat.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.alamat, 2);
            this.alamat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.alamat.Location = new System.Drawing.Point(264, 214);
            this.alamat.Margin = new System.Windows.Forms.Padding(4);
            this.alamat.Name = "alamat";
            this.alamat.Size = new System.Drawing.Size(700, 42);
            this.alamat.TabIndex = 97;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(995, 56);
            this.lblTitle.TabIndex = 8;
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(930, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 56);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 1;
            this.exit.TabStop = false;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnClose);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 571);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(995, 122);
            this.lblButton.TabIndex = 89;
            // 
            // btnClose
            // 
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(30, 30);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(244, 62);
            this.btnClose.TabIndex = 28;
            this.btnClose.Tag = "";
            this.btnClose.Text = "&TUTUP";
            this.btnClose.ThemeName = "MaterialBlueGrey";
            // 
            // TahananDetailCheckedOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblTitle);
            this.Name = "TahananDetailCheckedOut";
            this.Size = new System.Drawing.Size(995, 693);
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.updater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waktumulai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.namatahanan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.idregister)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.notes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAgama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJenisKelamin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglLahir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waktuselesai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jeniskelamin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alamat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel universitas;
        private System.Windows.Forms.PictureBox exit;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel updater;
        private Telerik.WinControls.UI.RadLabel inputdate;
        private Telerik.WinControls.UI.RadLabel waktuselesai;
        private Telerik.WinControls.UI.RadLabel waktumulai;
        private Telerik.WinControls.UI.RadLabel namatahanan;
        private Telerik.WinControls.UI.RadLabel idregister;
        private Telerik.WinControls.UI.RadLabel notes;
        private Telerik.WinControls.UI.RadLabel lblAgama;
        private Telerik.WinControls.UI.RadLabel lblTelp;
        private Telerik.WinControls.UI.RadLabel lblJenisKelamin;
        private Telerik.WinControls.UI.RadLabel lblNama;
        private Telerik.WinControls.UI.RadLabel lblTglLahir;
        private Telerik.WinControls.UI.RadLabel lblDataVisit;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel inputter;
        private Telerik.WinControls.UI.RadLabel lblDetailData;
        private Telerik.WinControls.UI.RadLabel updatedate;
        private System.Windows.Forms.Button btnCheckOut;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel jeniskelamin;
        private Telerik.WinControls.UI.RadLabel alamat;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnClose;
    }
}
