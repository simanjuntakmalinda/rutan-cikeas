﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.Desktop.Presentation;
using AForge.Video;
using AForge.Video.DirectShow;
using Telerik.WinControls.Primitives;
using System.IO;
using RDM.LP.Desktop.Presentation.Properties;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormTahanan : Base
    {
        public PictureBox PicCancas { get { return this.picCanvas; } set { } }

        //private bool CamOn = false;
        private string RegId = string.Empty;
        private Int32 InmatesID = 0;
        private bool ExisitingInmates = false;
        public Label TahananId { get { return this.tahananId; } set { this.tahananId = value; } }
        public RadTextBox NamaTahanan { get { return this.txtSourceLocation; } set { this.txtSourceLocation = value; } }
        public PictureBox FingerPicture { get { return this.picFinger; } }
        public RadTextBox IdNo { get { return this.txtNoIdentitas; } }
        public RadDropDownList DDLNetwork { get { return this.ddlNetwork; } }
        public RadCheckedDropDownList DDLCases { get { return this.ddlCases; } }
        //private bool recapture;
        public List<PictureBox> PhotoList = new List<PictureBox>();
        public FormNetwork Network { get { return this.formNetwork; } }
        public RadTextBox TxtInmatesRole { get { return this.txtPeranTahanan; } }

        //int currentPhoto = 0;
        public int totalPhoto = 0;
        //private FilterInfoCollection CaptureDevice;
        //private VideoCaptureDevice FinalFrame;

        private bool fingerprint = false;
        public bool temp = false;

        public FormTahanan()
        {
            //recapture = false;
            InitializeComponent();
            InitForm();
            InitEvents();
            LoadData();
        }

        public void LoadData()
        {
            //ddl Agama
            AgamaService agamaserv = new AgamaService();
            ddlAgama.Items.Clear();
            ddlAgama.DisplayMember = "Nama";
            ddlAgama.ValueMember = "Id";
            ddlAgama.DataSource = agamaserv.Get();

            //ddl Inmate Category
            InmatesCategoryService inmatescatserv = new InmatesCategoryService();
            ddlCategory.Items.Clear();
            ddlCategory.DisplayMember = "Nama";
            ddlCategory.ValueMember = "Id";
            ddlCategory.DataSource = inmatescatserv.Get();

            //ddl Gender
            GenderService Genderserv = new GenderService();
            ddlGender.Items.Clear();
            ddlGender.DisplayMember = "Nama";
            ddlGender.ValueMember = "Id";
            ddlGender.DataSource = Genderserv.Get();

            //ddl Inmates Type
            InmatesTypeService inmatetypeserv = new InmatesTypeService();
            ddlType.Items.Clear();
            ddlType.DisplayMember = "Nama";
            ddlType.ValueMember = "Id";
            ddlType.DataSource = inmatetypeserv.Get();

            //ddl Cases
            CasesService caseserv = new CasesService();
            ddlCases.Items.Clear();
            ddlCases.DisplayMember = "Name";
            ddlCases.ValueMember = "Id";
            ddlCases.DataSource = caseserv.Get();

            //ddl Network
            NetworkService inmatenetworkserv = new NetworkService();
            ddlNetwork.Items.Clear();
            ddlNetwork.DisplayMember = "Name";
            ddlNetwork.ValueMember = "Id";
            ddlNetwork.DataSource = inmatenetworkserv.Get();

            //ddl Negara
            NegaraService negaraserv = new NegaraService();
            ddlNationality.Items.Clear();
            ddlNationality.DisplayMember = "Nama_Negara";
            ddlNationality.ValueMember = "Id_Negara";
            ddlNationality.DataSource = negaraserv.Get();

            //ddl Pekerjaan
            PekerjaanService pekerjaanserv = new PekerjaanService();
            ddlVocation.Items.Clear();
            ddlVocation.DisplayMember = "Nama";
            ddlVocation.ValueMember = "Id";
            ddlVocation.DataSource = pekerjaanserv.Get();

            //ddl Marital Status
            MaritalStatusService maritalserv = new MaritalStatusService();
            ddlMaritalStatus.Items.Clear();
            ddlMaritalStatus.DisplayMember = "Nama";
            ddlMaritalStatus.ValueMember = "Id";
            ddlMaritalStatus.DataSource = maritalserv.Get();

            //ddlDateReg.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            ddlJnsIdentitas_SelectedIndexChanged(ddlJnsIdentitas, null);

            PhotoList = new List<PictureBox>();
            picCanvas.Image = null;
            picFinger.Image = null;
            GlobalVariables.FPImageList.Clear();
            GlobalVariables.RegisterList.Clear();

            this.lblTitle.Text = "     TAMBAH DATA TAHANAN BARU";


            if (this.Tag != null)
            {
                MainForm.formMain.AddNew.Text = "UBAH DATA TAHANAN";
                this.lblTitle.Text = "     UBAH DATA TAHANAN";

                LoadDataById(Convert.ToString(this.Tag));
            }

            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;
        }

        private void InitEvents()
        {
            //Events
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            //this.Leave += Form_Leave;
            this.VisibleChanged += Form_VisibleChanged;
            //btnCapture.Click += btnCapture_Click;
            //btnSetting.Click += btnSetting_Click;
            //next.Click += next_Click;
            //prev.Click += prev_Click;
            //picDelete.Click += picDelete_Click;
            //CamOnOff.Click += CamOnOff_Click;
            //cameraSource.CameraSourceOK.Click += CameraSourceOK_Click;
            //MainForm.formMain.CameraSource.CameraSourceOK.Click += CameraSourceOK_Click;
            //txtNoIdentitas.LostFocus += txtNoIdentitas_LostFocus;
            ddlJnsIdentitas.KeyDown += Control_KeyDown;
            txtNoIdentitas.KeyDown += Control_KeyDown;
            txtNama.KeyDown += Control_KeyDown;
            txtAddress.KeyDown += Control_KeyDown;
            txtCity.KeyDown += Control_KeyDown;
            ddlAgama.KeyDown += Control_KeyDown;
            ddlGender.KeyDown += Control_KeyDown;
            txtTelp.KeyDown += Control_KeyDown;
            txtBB.KeyDown += Control_KeyDown;
            txtTB.KeyDown += Control_KeyDown;
            ddlCategory.KeyDown += Control_KeyDown;
            ddlType.KeyDown += Control_KeyDown;
            txtSourceLocation.KeyDown += Control_KeyDown;
            txtNotes.KeyDown += Control_KeyDown;
            txtTelp.KeyUp += txtTelp_KeyUp;
            txtBB.KeyUp += txtBB_KeyUp;
            txtTB.KeyUp += txtTB_keyUp;
            btnSave.KeyDown += Control_KeyDown;
            //ddlDateReg.KeyDown += Control_KeyDown;
            ddlCases.KeyDown += Control_KeyDown;
            ddlNetwork.KeyDown += Control_KeyDown;
            btnTakePhoto.Click += btnTakePhoto_Click;
            btnUbahSidikJari.Click += btnUbahSidikJari_Click;
            lblFront.Click += lblFront_Click;
            labelRight.Click += labelRight_Click;
            labelLeft.Click += labelLeft_Click;
            btnAddCases.Click += BtnAddCases_Click;
            btnAddNetwork.Click += BtnAddNetwork_Click;
            ddlJnsIdentitas.SelectedIndexChanged += ddlJnsIdentitas_SelectedIndexChanged;
            txtNoIdentitas.TextChanged += TxtNoIdentitas_TextChanged;

            dtTanggalKejadian.KeyDown += Control_KeyDown;
            dtTanggalPenangkapan.KeyDown += Control_KeyDown;
            radPanel3.KeyDown += Control_KeyDown;
            radPanel4.KeyDown += Control_KeyDown;
            radPanel5.KeyDown += Control_KeyDown;
            radPanel6.KeyDown += Control_KeyDown;
            radPanel7.KeyDown += Control_KeyDown;
            radPanel8.KeyDown += Control_KeyDown;
            radPanel9.KeyDown += Control_KeyDown;
            radPanel10.KeyDown += Control_KeyDown;
            radPanel11.KeyDown += Control_KeyDown;
            radPanel12.KeyDown += Control_KeyDown;
            radPanel13.KeyDown += Control_KeyDown;
            radPanel14.KeyDown += Control_KeyDown;
            radPanel15.KeyDown += Control_KeyDown;
            radPanel16.KeyDown += Control_KeyDown;
            radPanel17.KeyDown += Control_KeyDown;
            radPanel18.KeyDown += Control_KeyDown;
            radPanel19.KeyDown += Control_KeyDown;
            radPanel20.KeyDown += Control_KeyDown;
            radPanel21.KeyDown += Control_KeyDown;
            radPanel22.KeyDown += Control_KeyDown;
            radPanel23.KeyDown += Control_KeyDown;
            radPanel24.KeyDown += Control_KeyDown;
            btnTakePhoto.KeyDown += Control_KeyDown;
            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;
            btnAddCases.KeyDown += Control_KeyDown;
            btnAddNetwork.KeyDown += Control_KeyDown;
            labelLeft.KeyDown += Control_KeyDown;
            labelRight.KeyDown += Control_KeyDown;
            lblFront.KeyDown += Control_KeyDown;
            btnUbahSidikJari.KeyDown += Control_KeyDown;
        }
        
        private void txtTB_keyUp(object sender, KeyEventArgs e)
        {
            if (!UserFunction.IsNumeric(txtTB.Text))
            {
                txtTB.Text = string.Empty;
            }
        }

        private void txtBB_KeyUp(object sender, KeyEventArgs e)
        {
            if (!UserFunction.IsNumeric(txtBB.Text))
            {
                txtBB.Text = string.Empty;
            }
        }

        private void BtnAddCases_Click(object sender, EventArgs e)
        {
            formCases.Show();
            formCases.Location = new Point(ClientSize.Width / 2 - formCases.Size.Width / 2,
              ClientSize.Height / 2 - formCases.Size.Height / 2);
        }

        private void TxtNoIdentitas_TextChanged(object sender, EventArgs e)
        {
            if (ddlJnsIdentitas.SelectedValue != null)
            {
                if (ddlJnsIdentitas.SelectedValue.ToString() != "3")
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(txtNoIdentitas.Text, "[^0-9]"))
                    {
                        txtNoIdentitas.Text = txtNoIdentitas.Text.Remove(txtNoIdentitas.Text.Length - 1);
                    }
                }
            }
        }

        private void ddlJnsIdentitas_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (ddlJnsIdentitas.SelectedValue != null)
            {
                if (ddlJnsIdentitas.SelectedValue.ToString() == "1")
                {
                    txtNoIdentitas.Enabled = true;
                    txtNoIdentitas.MaxLength = 16;
                    while (txtNoIdentitas.TextLength > 16)
                    {
                        txtNoIdentitas.Text = txtNoIdentitas.Text.Remove(txtNoIdentitas.Text.Length - 1);
                    }
                }
                else if (ddlJnsIdentitas.SelectedValue.ToString() == "2")
                {
                    txtNoIdentitas.Enabled = true;
                    txtNoIdentitas.MaxLength = 12;
                    while (txtNoIdentitas.TextLength > 12)
                    {
                        txtNoIdentitas.Text = txtNoIdentitas.Text.Remove(txtNoIdentitas.Text.Length - 1);
                    }
                }
                else if (ddlJnsIdentitas.SelectedValue.ToString() == "3")
                {
                    txtNoIdentitas.Enabled = true;
                    txtNoIdentitas.MaxLength = 16;
                    while (txtNoIdentitas.TextLength > 16)
                    {
                        txtNoIdentitas.Text = txtNoIdentitas.Text.Remove(txtNoIdentitas.Text.Length - 1);
                    }
                }
                else if (ddlJnsIdentitas.SelectedValue.ToString() == "4")
                {
                    txtNoIdentitas.Text = "--";
                    txtNoIdentitas.Enabled = false;
                }
            }
        }

        private void BtnAddNetwork_Click(object sender, EventArgs e)
        {
            formNetwork.Show();
            formNetwork.Location = new Point(ClientSize.Width / 2 - formNetwork.Size.Width / 2,
              ClientSize.Height / 2 - formNetwork.Size.Height / 2);
        }

        public void lblFront_Click(object sender, EventArgs e) 
        {
            lblFront.ForeColor = Color.Navy;
            labelLeft.ForeColor = Color.White;
            labelRight.ForeColor = Color.White;
            totalPhoto = PhotoList.Count;

            if (totalPhoto >= 1)
            {
                picCanvas.Image = PhotoList[0].Image;                
            }
            else
            {
                picCanvas.Image = null;
            }
        }

        private void labelRight_Click(object sender, EventArgs e)
        {
            lblFront.ForeColor = Color.White;
            labelLeft.ForeColor = Color.White;
            labelRight.ForeColor = Color.Navy;
            totalPhoto = PhotoList.Count;

            if (totalPhoto >= 2)
            {
                picCanvas.Image = PhotoList[1].Image;
            }
            else
            {
                picCanvas.Image = null;
            }
        }

        private void labelLeft_Click(object sender, EventArgs e)
        {
            lblFront.ForeColor = Color.White;
            labelLeft.ForeColor = Color.Navy;
            labelRight.ForeColor = Color.White;
            totalPhoto = PhotoList.Count;

            if (totalPhoto >= 3)
            {
                picCanvas.Image = PhotoList[2].Image;
            }
            else
            {
                picCanvas.Image = null;
            }
        }

        private void btnTakePhoto_Click(object sender, EventArgs e)
        {
            MainForm.formMain.CapturePhoto.Show();
            MainForm.formMain.CapturePhoto.FrontPhoto_Click();

            totalPhoto = PhotoList.Count;
            
            if (totalPhoto >= 1)
            {
                MainForm.formMain.CapturePhoto.FrontPhoto.Image = PhotoList[0].Image;

                if (totalPhoto >= 2)
                {
                    MainForm.formMain.CapturePhoto.RightPhoto.Image = PhotoList[1].Image;

                    if (totalPhoto >= 3)
                    {
                        MainForm.formMain.CapturePhoto.LeftPhoto.Image = PhotoList[2].Image;
                    }
                }
            }
        }

        private void btnUbahSidikJari_Click(object sender, EventArgs e)
        {

            int totalFingerPrint = 0;

            if (GlobalVariables.FPImageList[0] != null)
            {
                MainForm.formMain.FingerPrint_Inmate.RightThumbFinger.Image = GenerateImage(GlobalVariables.FPImageList[0]);
                totalFingerPrint++;
            }

            if (GlobalVariables.FPImageList[1] != null)
            {
                MainForm.formMain.FingerPrint_Inmate.RightIndexFinger.Image = GenerateImage(GlobalVariables.FPImageList[1]);
                totalFingerPrint++;
            }
            
            if (GlobalVariables.FPImageList[2] != null)
            {
                MainForm.formMain.FingerPrint_Inmate.RightMiddleFinger.Image = GenerateImage(GlobalVariables.FPImageList[2]);
                totalFingerPrint++;
            }

            if (GlobalVariables.FPImageList[3] != null)
            {
                MainForm.formMain.FingerPrint_Inmate.RightRingFinger.Image = GenerateImage(GlobalVariables.FPImageList[1]);
                totalFingerPrint++;
            }

            if (GlobalVariables.FPImageList[4] != null)
            {
                MainForm.formMain.FingerPrint_Inmate.RightLittleFinger.Image = GenerateImage(GlobalVariables.FPImageList[4]);
                totalFingerPrint++;
            }

            if (GlobalVariables.FPImageList[5] != null)
            {
                MainForm.formMain.FingerPrint_Inmate.LeftThumbFinger.Image = GenerateImage(GlobalVariables.FPImageList[5]);
                totalFingerPrint++;
            }

            if (GlobalVariables.FPImageList[6] != null)
            {
                MainForm.formMain.FingerPrint_Inmate.LeftIndexFinger.Image = GenerateImage(GlobalVariables.FPImageList[6]);
                totalFingerPrint++;
            }

            if (GlobalVariables.FPImageList[7] != null)
            {
                MainForm.formMain.FingerPrint_Inmate.LeftMiddleFinger.Image = GenerateImage(GlobalVariables.FPImageList[7]);
                totalFingerPrint++;
            }

            if (GlobalVariables.FPImageList[8] != null)
            {
                MainForm.formMain.FingerPrint_Inmate.LeftRingFinger.Image = GenerateImage(GlobalVariables.FPImageList[8]);
                totalFingerPrint++;
            }

            if (GlobalVariables.FPImageList[9] != null)
            {
                MainForm.formMain.FingerPrint_Inmate.LeftLittleFinger.Image = GenerateImage(GlobalVariables.FPImageList[9]);
                totalFingerPrint++;
            }

            MainForm.formMain.FingerPrint_Inmate.Tag = totalFingerPrint;
            MainForm.formMain.FingerPrint_Inmate.Show();
            MainForm.formMain.FingerPrint_Inmate.RightThumb_Click();
            temp = true;

        }

        private void txtTelp_KeyUp(object sender, KeyEventArgs e)
        {
            if (!UserFunction.IsNumeric(txtTelp.Text))
            {
                txtTelp.Text = string.Empty;
            }
        }

        //private void txtNoIdentitas_LostFocus(object sender, EventArgs e)
        //{
        //    var idtype = ddlJnsIdentitas.SelectedItem.Text;
        //    var idno = txtNoIdentitas.Text;
        //    InmatesService inmatesserv = new InmatesService();

        //    var data = inmatesserv.GetDetailByIdentity(idtype, idno);
        //    if (data != null)
        //    {
        //        LoadDataInmates(data);
        //    }
        //}

        private void LoadDataInmates(Inmates data)
        {
            InmatesID = data.Id;
            //RegId = data.RegID;
            ddlJnsIdentitas.SelectedText = data.IDType;
            txtNoIdentitas.Text = data.IDNo;
            txtNama.Text = data.FullName;
            txtPlaceofBirth.Text = data.PlaceOfBirth;
            dpDateofBirth.Value = data.DateOfBirth != null ? (DateTime)data.DateOfBirth : DateTime.Now.ToLocalTime();
            txtAddress.Text = data.Address;
            txtCity.Text = data.City;
            ddlAgama.SelectedValue = ddlAgama.Items.FirstOrDefault(x=>x.Text == data.Religion).Value;
            ddlGender.SelectedValue = ddlGender.Items.FirstOrDefault(x => x.Text == data.Gender).Value;
            ddlMaritalStatus.SelectedValue = data.MaritalStatus != null ? ddlMaritalStatus.Items.FirstOrDefault(x => x.Text == data.MaritalStatus).Value : null;
            ddlNationality.SelectedValue = data.Nationality != null ? ddlNationality.Items.FirstOrDefault(x => x.Text == data.Nationality).Value : null;
            ddlVocation.SelectedValue = data.Vocation != null ? ddlVocation.Items.FirstOrDefault(x => x.Text == data.Vocation).Value : null;
            txtTelp.Text = data.TelpNo;
            ddlCategory.Text = data.Category;
            ddlType.Text = data.Type;
            txtSourceLocation.Text = data.SourceLocation;
            txtNotes.Text = data.Notes;
            ddlNetwork.SelectedValue = data.NetworkId;
            ddlCases.SelectedValue = data.CaseId;
            txtBB.Text = data.BeratBadan;
            txtTB.Text = data.TinggiBadan;
            txtTelp.Text = data.TelpNo;
            txtPeranTahanan.Text = data.Peran;
            txtLokasiPenangkapan.Text = data.LokasiPenangkapan;
            txtSuratPenangkapan.Text = data.NoSuratPenangkapan;
            txtSuratPenahanan.Text = data.NoSuratPenahanan;
            dtTanggalPenahanan.Value = data.TanggalPenahanan;
            dtTanggalPenangkapan.Value = data.TanggalPenangkapan;
            

            LoadDataFoto(data);
            //LoadDataFinger(data);
            LoadDataAllFinger(data);

            LoadCases(data);
            LoadNetwork(data);
            ExisitingInmates = true;
        }

        private void LoadCases(Inmates data)
        {
            CasesService caseserv = new CasesService();
            var inmatesCase = caseserv.GetInmatesCase(data.RegID);
            foreach (var _data in inmatesCase)
            {
                foreach (var _item in DDLCases.Items)
                {
                    if (Convert.ToInt32(((RadCheckedListDataItem)_item).Value) == _data.CaseId)
                    {
                        ((RadCheckedListDataItem)_item).Checked = true;
                    }
                }
            }
        }

        private void LoadNetwork(Inmates data)
        {
            NetworkService networkserv = new NetworkService();
            var inmatesNetwork = networkserv.GetInmatesNetwork(data.RegID);
            foreach (var _data in inmatesNetwork)
            {
                foreach (var _item in DDLNetwork.Items)
                {
                    if (Convert.ToInt32(((RadCheckedListDataItem)_item).Value) == _data.NetworkId)
                    {
                        ((RadCheckedListDataItem)_item).Checked = true;
                    }
                }
            }
        }

        //private void LoadDataFinger(Inmates data)
        //{
        //    FingerPrintService fingerserv = new FingerPrintService();
        //    var finger = fingerserv.GetByRegID(data.RegID);
        //    if (finger != null)
        //    {
        //        var imageBytes = (byte[])Convert.FromBase64String(finger.ImagePrint);
        //        MemoryStream memStm = new MemoryStream(imageBytes);
        //        memStm.Seek(0, SeekOrigin.Begin);
        //        Image image = Image.FromStream(memStm);
        //        picFinger.Image = image;
        //    }
        //}

        private void LoadDataAllFinger(Inmates data)
        {

            GlobalVariables.RegisterList.Clear();
            GlobalVariables.FPImageList.Clear();

            for (int i = 0; i < GlobalVariables.REGISTER_FINGER_COUNT; i++)
            {
                GlobalVariables.RegisterList.Insert(i, null);
                GlobalVariables.FPImageList.Insert(i, null);
            }

            FingerPrintService fingerserv = new FingerPrintService();
            var fingers = fingerserv.GetAllByRegID(data.RegID);
            if (fingers.Count() != 0)
            {
                foreach (var finger in fingers)
                {
                    if (finger.FingerId == "R1")
                    {
                        if (finger.ImagePrint != null)
                        {
                            MainForm.formMain.FingerPrint_Inmate.RightThumbFinger.Image = GenerateImage(finger.ImagePrint);
                            GlobalVariables.RegisterList[0] = finger.ByteImagePrint;
                            GlobalVariables.FPImageList[0] = finger.ImagePrint;

                            picFinger.Image = GenerateImage(finger.ImagePrint);
                        }
                    }
                    else if (finger.FingerId == "R2")
                    {
                        if (finger.ImagePrint != null)
                        {
                            MainForm.formMain.FingerPrint_Inmate.RightIndexFinger.Image = GenerateImage(finger.ImagePrint);
                            GlobalVariables.RegisterList[1] = finger.ByteImagePrint;
                            GlobalVariables.FPImageList[1] = finger.ImagePrint;
                        }
                    }
                    else if (finger.FingerId == "R3")
                    {
                        if (finger.ImagePrint != null)
                        {
                            MainForm.formMain.FingerPrint_Inmate.RightMiddleFinger.Image = GenerateImage(finger.ImagePrint);
                            GlobalVariables.RegisterList[2] = finger.ByteImagePrint;
                            GlobalVariables.FPImageList[2] = finger.ImagePrint;
                        }
                    }
                    else if (finger.FingerId == "R4")
                    {
                        if (finger.ImagePrint != null)
                        {
                            MainForm.formMain.FingerPrint_Inmate.RightRingFinger.Image = GenerateImage(finger.ImagePrint);
                            GlobalVariables.RegisterList[3] = finger.ByteImagePrint;
                            GlobalVariables.FPImageList[3] = finger.ImagePrint;
                        }
                    }
                    else if (finger.FingerId == "R5")
                    {
                        if (finger.ImagePrint != null)
                        {
                            MainForm.formMain.FingerPrint_Inmate.RightLittleFinger.Image = GenerateImage(finger.ImagePrint);
                            GlobalVariables.RegisterList[4] = finger.ByteImagePrint;
                            GlobalVariables.FPImageList[4] = finger.ImagePrint;
                        }
                    }
                    else if (finger.FingerId == "L1")
                    {
                        if (finger.ImagePrint != null)
                        {
                            MainForm.formMain.FingerPrint_Inmate.LeftThumbFinger.Image = GenerateImage(finger.ImagePrint);
                            GlobalVariables.RegisterList[5] = finger.ByteImagePrint;
                            GlobalVariables.FPImageList[5] = finger.ImagePrint;
                        }
                    }
                    else if (finger.FingerId == "L2")
                    {
                        if (finger.ImagePrint != null)
                        {
                            MainForm.formMain.FingerPrint_Inmate.LeftIndexFinger.Image = GenerateImage(finger.ImagePrint);
                            GlobalVariables.RegisterList[6] = finger.ByteImagePrint;
                            GlobalVariables.FPImageList[6] = finger.ImagePrint;
                        }
                    }
                    else if (finger.FingerId == "L3")
                    {
                        if (finger.ImagePrint != null)
                        {
                            MainForm.formMain.FingerPrint_Inmate.LeftMiddleFinger.Image = GenerateImage(finger.ImagePrint);
                            GlobalVariables.RegisterList[7] = finger.ByteImagePrint;
                            GlobalVariables.FPImageList[7] = finger.ImagePrint;
                        }
                    }
                    else if (finger.FingerId == "L4")
                    {
                        if (finger.ImagePrint != null)
                        {
                            MainForm.formMain.FingerPrint_Inmate.LeftRingFinger.Image = GenerateImage(finger.ImagePrint);
                            GlobalVariables.RegisterList[8] = finger.ByteImagePrint;
                            GlobalVariables.FPImageList[8] = finger.ImagePrint;
                        }
                    }
                    else if (finger.FingerId == "L5")
                    {
                        if (finger.ImagePrint != null)
                        {
                            MainForm.formMain.FingerPrint_Inmate.LeftLittleFinger.Image = GenerateImage(finger.ImagePrint);
                            GlobalVariables.RegisterList[9] = finger.ByteImagePrint;
                            GlobalVariables.FPImageList[9] = finger.ImagePrint;
                        }
                    }
                }
            }
        }

        public void ClearDataInmates()
        {
            UserFunction.ClearControls(tableLayoutPanel1);
            txtNama.Text = string.Empty;
            txtPlaceofBirth.Text = string.Empty;
            dpDateofBirth.Value = DateTime.Now.ToLocalTime();
            txtAddress.Text = string.Empty;
            txtCity.Text = string.Empty;
            ddlAgama.SelectedText = string.Empty;
            ddlMaritalStatus.SelectedText = string.Empty;
            ddlNationality.SelectedText = string.Empty;
            ddlVocation.SelectedText = string.Empty;
            ddlGender.SelectedText = string.Empty;
            txtTelp.Text = string.Empty;
            txtSourceLocation.Text = string.Empty;
            txtNotes.Text = string.Empty;
            //currentPhoto = 0;
            //totalPhoto = 0;
            PhotoList.Clear();
            picCanvas.Image = null;
            PhotoList = new List<PictureBox>();
            //picFinger.Image = null;
            //tot.Text = string.Format("<html>F<br><size=5>Front");
            //picDelete.Hide();
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }

        private Image GenerateImage(string stringImage)
        {
            var imageBytes = (byte[])Convert.FromBase64String(stringImage);
            MemoryStream memStm = new MemoryStream(imageBytes);
            memStm.Seek(0, SeekOrigin.Begin);
            Image image = Image.FromStream(memStm);
            return image;
        }

        private void LoadDataFoto(Inmates data)
        {
            //currentPhoto = 0;
            //totalPhoto = 0;
            PhotoList.Clear();
            PhotosService photoserv = new PhotosService();
            var foto = photoserv.GetByRegID(data.RegID);
            if(foto.Count() != 0) { 
                foreach (var item in foto)
                {
                    var imageBytes = (byte[])Convert.FromBase64String(item.Data);
                    MemoryStream memStm = new MemoryStream(imageBytes);
                    memStm.Seek(0, SeekOrigin.Begin);
                    Image image = Image.FromStream(memStm);

                    PhotoList.Add(new PictureBox());
                    PhotoList[PhotoList.Count - 1].Image = image;

                    //totalPhoto++;
                    //currentPhoto++;
                    //picDelete.Show();
                }
                picCanvas.Image = PhotoList[0].Image;
                //picCanvas.Image = PhotoList[currentPhoto - 1].Image;
                //tot.Text = string.Format("<html>R<br><size=5>Right");
            }
            else
            {
                picCanvas.Image = null;
                //tot.Text = string.Format("<html>F<br><size=5>Front");
                //picDelete.Hide();
            }
        }

        private void CameraSourceOK_Click(object sender, EventArgs e)
        {
            try
            {
                MainForm.formMain.CapturePhoto.StopFrame();
                MainForm.formMain.CapturePhoto.CaptureDevice = new FilterInfoCollection(FilterCategory.VideoInputDevice); ;
                MainForm.formMain.CapturePhoto.FinalFrame = new VideoCaptureDevice(MainForm.formMain.CapturePhoto.CaptureDevice[MainForm.formMain.CameraSource.ddlCameraSource.SelectedIndex].MonikerString);
                MainForm.formMain.CapturePhoto.FinalFrame.NewFrame += new NewFrameEventHandler(MainForm.formMain.CapturePhoto.FinalFrame_NewFrame);
                MainForm.formMain.CapturePhoto.FinalFrame.Start();
                MainForm.formMain.CameraSource.Hide();
            }
            catch (Exception)
            {
                picCanvas.Image = null;
                MainForm.formMain.CapturePhoto.StopFrame();
            }
        }

        //private void CamOnOff_Click(object sender, EventArgs e)
        //{
        //    if (totalPhoto >= 3)
        //    {
        //        CamOn = true;
        //        OnOffCamera();
        //        return;
        //    }
        //    OnOffCamera();
        //}

        //private void OnOffCamera()
        //{
        //    if (!CamOn)
        //    {
        //        StartFrame();
        //        CamOnOff.Image = Resources.CamOn;
        //        btnCapture.Image = Resources.camera_on;
        //        CamOn = true;
        //        btnCapture.Enabled = true;
        //        btnSetting.Enabled = true;
        //    }
        //    else
        //    {
        //        StopFrame();
        //        btnCapture.Enabled = false;
        //        btnSetting.Enabled = false;
        //        CamOnOff.Image = Resources.CamOff;
        //        btnCapture.Image = Resources.camera_off;
        //        picCanvas.Image = null;
        //        CamOn = false;
        //        if (PhotoList.Count != 0)
        //            picCanvas.Image = PhotoList[0].Image;
        //        else
        //            picCanvas.Image = null;
        //    }
        //}

        //private void picDelete_Click(object sender, EventArgs e)
        //{
        //    if (currentPhoto > 1)
        //    {
        //        PhotoList.Remove(PhotoList[currentPhoto - 1]);
        //        currentPhoto--;
        //    }
        //    else
        //    {
        //        PhotoList.Remove(PhotoList[0]);
        //    }
        //    totalPhoto--;
        //    LoadPhoto();
        //}

        //private void prev_Click(object sender, EventArgs e)
        //{
        //    if (currentPhoto <= 1) { }
        //    else
        //        currentPhoto--;
        //    LoadPhoto();
        //}

        //private void next_Click(object sender, EventArgs e)
        //{
        //    if (currentPhoto >= totalPhoto) { }
        //    else
        //        currentPhoto++;
        //    LoadPhoto();
        //}

        //private void LoadPhoto()
        //{
        //    if (totalPhoto <= 0)
        //    {
        //        picCanvas.Image = null;
        //        currentPhoto = 0;
        //        picDelete.Hide();
        //    }
        //    else
        //        picCanvas.Image = PhotoList[currentPhoto - 1].Image;

        //    PhotoPositition();
        //}

        //private void PhotoPositition()
        //{
        //    switch (currentPhoto)
        //    {
        //        case 1:
        //            tot.Text = string.Format("<html>F<br><size=5>Front");
        //            break;
        //        case 2:
        //            tot.Text = string.Format("<html>L<br><size=5>Left");
        //            break;
        //        case 3:
        //            tot.Text = string.Format("<html>R<br><size=5>Right");
        //            break;
        //        default:
        //            break;
        //    }
        //}

        //private void PhotoPosititionCapture()
        //{
        //    switch (currentPhoto)
        //    {
        //        case 1:
        //            tot.Text = string.Format("<html>L<br><size=5>Left");
        //            break;
        //        case 2:
        //            tot.Text = string.Format("<html>R<br><size=5>Right");
        //            break;
        //        case 3:
        //            tot.Text = string.Format("<html>R<br><size=5>Right");
        //            break;
        //        default:
        //            break;
        //    }
        //}

        private void btnSearchTahanan_Click(object sender, EventArgs e)
        {
            MainForm.formMain.SearchData.Tag = SearchType.Tahanan.ToString();
            MainForm.formMain.SearchData.Show();
        }

        //private void btnSetting_Click(object sender, EventArgs e)
        //{
        //    cameraSource.Show();
        //}

        //private void btnCapture_Click(object sender, EventArgs e)
        //{
        //    if (!CamOn)
        //        return;

        //    if (!recapture)
        //    {
        //        StopFrame();
        //        recapture = true;
        //        btnCapture.Image = Resources.camera_off;
        //        AddNewPhoto();
        //        if (totalPhoto >= 3)
        //        {
        //            OnOffCamera();
        //            picCanvas.Image = PhotoList[2].Image;
        //        }
        //    }
        //    else
        //    {
        //        recapture = false;
        //        btnCapture.Image = Resources.camera_on;
        //        StartFrame();
        //        PhotoPosititionCapture();
        //    }
        //}

        //private void AddNewPhoto()
        //{
        //    PhotoList.Add(new PictureBox());
        //    PhotoList[PhotoList.Count - 1].Image = picCanvas.Image;
        //    totalPhoto++;
        //    currentPhoto++;
        //    PhotoPositition();
        //    picDelete.Show();
        //}

        //private void Form_Leave(object sender, EventArgs e)
        //{
        //    StopFrame();
        //}

        private void InitForm()
        {

            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     FORM REGISTRASI TAHANAN";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;
            this.lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 14f;
            this.lblDetailSurat.ForeColor = Color.White;
            this.lblDetailSurat.BackColor = Color.FromArgb(77, 77, 77);

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailVisitor.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailVisitor.LabelElement.CustomFontSize = 15.5f;

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);

            //ddlDateReg.Format = DateTimePickerFormat.Custom;
            //ddlDateReg.CustomFormat = "dd MMM yyyy";

            lblInmatePhoto.BackColor = Color.FromArgb(45, 45, 48);
            lblInmatePhoto.ForeColor = Color.White;


            lblVisitorFinger.BackColor = Color.FromArgb(45, 45, 48);
            lblVisitorFinger.ForeColor = Color.White;

            //btnCapture.RootElement.EnableElementShadow = false;
            //btnCapture.ButtonElement.ShowBorder = true;
            //CamOnOff.RootElement.EnableElementShadow = false;
            //CamOnOff.ButtonElement.ShowBorder = true;
            //btnSetting.RootElement.EnableElementShadow = false;
            //btnSetting.ButtonElement.ShowBorder = true;

            ClearDataInmates();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            //StopFrame();
            this.Tag = null;
            MainForm.formMain.AddNew.Text = "TAMBAH DATA TAHANAN BARU";
            this.lblTitle.Text = "     TAMBAH DATA TAHANAN BARU";
            LoadData();
            UserFunction.ClearControls(tableLayoutPanel1);
            MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
            MainForm.formMain.FingerPrint_Inmate.CleanFingerPrint_Inmate();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {

                if (this.Tag == null)
                {
                    InsertInmates();
                    SavePhotos();
                    SaveFingerPrint();
                }
                else
                {
                    UpdateInmates();
                    UpdatePhotos();
                    UpdateFingerPrint();
                }

                LoadData();
                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Disimpan!");
                UserFunction.LoadDataTahananToGrid(MainForm.formMain.ListTahanan.GvTahanan);
                UserFunction.LoadDataPrintFormTahananToGrid(MainForm.formMain.ListPrintFormTahanan.GvTahanan);
                MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
                MainForm.formMain.FingerPrint_Inmate.CleanFingerPrint_Inmate();

                this.Tag = null;
                MainForm.formMain.AddNew.Text = "TAMBAH DATA TAHANAN BARU";
                this.lblTitle.Text = "     TAMBAH DATA TAHANAN BARU";
            }

            PhotoList = new List<PictureBox>();
            //picCanvas.Image = null;
            //picFinger.Image = null;
        }

        private void UpdateFingerPrint()
        {
            InmatesService inmateServ = new InmatesService();
            var inmates = inmateServ.GetDetailById(this.Tag.ToString());

            FingerPrintService fpserv = new FingerPrintService();
            fpserv.DeleteByRegId(inmates.RegID);

            SaveFingerPrint();
        }

        private void SaveFingerPrint()
        {
            if (GlobalVariables.RegisterList.Count == 0 && GlobalVariables.FPImageList.Count == 0)
                return;

            //var GeneratedId = DateTime.Now.ToString("yyyyMMddHHmmssFFF");
            for (var x = 0; x < 10; x++)
            {
                var FingerId = "";
                if (x == 0)
                {
                    FingerId = "R1";
                }
                else if (x == 1)
                {
                    FingerId = "R2";
                }
                else if (x == 2)
                {
                    FingerId = "R3";
                }
                else if (x == 3)
                {
                    FingerId = "R4";
                }
                else if (x == 4)
                {
                    FingerId = "R5";
                }
                else if (x == 5)
                {
                    FingerId = "L1";
                }
                else if (x == 6)
                {
                    FingerId = "L2";
                }
                else if (x == 7)
                {
                    FingerId = "L3";
                }
                else if (x == 8)
                {
                    FingerId = "L4";
                }
                else if (x == 9)
                {
                    FingerId = "L5";
                }

                FingerPrintService fingerprintserv = new FingerPrintService();
                if (GlobalVariables.FPImageList.Count >= x || GlobalVariables.RegisterList.Count >= x)
                {
                    if (GlobalVariables.FPImageList[x] != null || GlobalVariables.RegisterList[x] != null)
                    {

                        var fingerprint = new FingerPrint
                        {
                            RegID = RegId,
                            ImagePrint = GlobalVariables.FPImageList[x],
                            ByteImagePrint = GlobalVariables.RegisterList[x],
                            FingerId = FingerId,
                            IsVisitor = false,
                            CreatedDate = DateTime.Now.ToLocalTime(),
                            CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
                        };
                        fingerprintserv.Post(fingerprint);
                        UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.INMATESFINGERPRINT.ToString(), Activities = "Add New FingerPrint, Data=" + UserFunction.JsonString(fingerprint) });
                    }
                }

                //FingerPrintService fpserv = new FingerPrintService();
                //var data = fpserv.GetInmatesByDesc();
                //foreach (var d in data)
                //{
                //    if (GlobalVariables.FPImageList[x].Equals(d.ImagePrint))
                //    {
                //        GlobalVariables.FingerPrintID.Add(d.Id);
                //        break;
                //    }
                //}
            }
        }

        private void SavePhotos()
        {
            var count = 0;
            foreach (var item in PhotoList)
            {
                MemoryStream ms = new MemoryStream();
                item.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                byte[] foto = ms.ToArray();
                ms.Close();

                string base64Foto = Convert.ToBase64String(foto);
                string photoposition = string.Empty;
                switch (count)
                {
                    case 0:
                        photoposition = "FRONT";
                        break;
                    case 1:
                        photoposition = "LEFT";
                        break;

                    case 2:
                        photoposition = "RIGHT";
                        break;
                    default:
                        photoposition = string.Empty;
                        break;
                }

                PhotosService fotoserv = new PhotosService();
                var data = new Photos
                {
                    RegID = RegId,
                    Type = "I",
                    Data = base64Foto,
                    Note = photoposition,
                    CreatedDate = DateTime.Now.ToLocalTime(),
                    CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
                };
                count++;
                fotoserv.Post(data);
                UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.INMATESPHOTO.ToString(), Activities = "Add New Photo, Data=" + UserFunction.JsonString(data) });
            }
        }

        private void UpdatePhotos()
        {
            PhotosService fotoserv = new PhotosService();
            InmatesService inmateServ = new InmatesService();
            var inmates = inmateServ.GetDetailById(this.Tag.ToString());
            fotoserv.DeleteByRegId(inmates.RegID);

            SavePhotos();
        }

        private void InsertInmates()
        {
            InmatesLogService inmateslog = new InmatesLogService();
            RegId = inmateslog.GetRegID();

            InmatesService inmates = new InmatesService();
            var data = new Inmates
            {
                RegID = RegId,
                FullName = txtNama.Text,
                PlaceOfBirth = txtPlaceofBirth.Text,
                DateOfBirth = dpDateofBirth.Value,
                IDType = ddlJnsIdentitas.SelectedItem.Text,
                IDNo = txtNoIdentitas.Text,
                Address = txtAddress.Text,
                TelpNo = txtTelp.Text,
                Religion = ddlAgama.SelectedItem.Text,
                Gender = ddlGender.SelectedItem.Text,
                MaritalStatus = ddlMaritalStatus.SelectedItem.Text,
                Nationality = ddlNationality.SelectedItem.Text,
                Vocation = ddlVocation.SelectedItem.Text,
                City = txtCity.Text,
                Category = ddlCategory.SelectedItem.Text,
                Type = ddlType.SelectedItem.Text,
                Network = ddlNetwork.Text,
                NetworkId = Convert.ToInt32(ddlNetwork.SelectedValue),
                SourceLocation = txtSourceLocation.Text,
                Notes = txtNotes.Text,
                DateReg = DateTime.Now.ToLocalTime(),
                CaseId = Convert.ToInt32(ddlCases.SelectedValue),
                Peran = txtPeranTahanan.Text,
                TanggalKejadian = dtTanggalKejadian.Value,
                TanggalPenangkapan = dtTanggalPenangkapan.Value,
                LokasiPenangkapan = txtLokasiPenangkapan.Text,
                BeratBadan = txtBB.Text,
                TinggiBadan = txtTB.Text,
                NoSuratPenahanan = txtSuratPenahanan.Text,
                NoSuratPenangkapan = txtSuratPenangkapan.Text,
                TanggalPenahanan = dtTanggalPenahanan.Value,

                CreatedDate = DateTime.Now.ToLocalTime(),
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
            };
            InmatesID = inmates.Post(data);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Add New Inmates, Data=" + UserFunction.JsonString(data) });

            CasesService caseServ = new CasesService();
            //caseServ.DeleteInmatesCase(RegId);

            foreach (var _item in DDLCases.Items)
            {
                if (((RadCheckedListDataItem)_item).Checked)
                {
                    var dataCase = new InmatesCase
                    {
                        RegID = RegId,
                        CaseId = Convert.ToInt32(_item.Value),
                        CreatedDate = DateTime.Now.ToLocalTime(),
                        CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
                    };
                    caseServ.PostCases(dataCase);
                    UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Add New Cases, Data=" + UserFunction.JsonString(dataCase) });
                }
            }


            NetworkService networkServ = new NetworkService();
            foreach (var _item in DDLNetwork.Items)
            {
                if (((RadCheckedListDataItem)_item).Checked)
                {
                    var dataNetwork = new InmatesNetwork
                    {
                        RegID = RegId,
                        NetworkId = Convert.ToInt32(_item.Value),
                        CreatedDate = DateTime.Now.ToLocalTime(),
                        CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
                    };
                    networkServ.PostNetwork(dataNetwork);
                    UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Add New Network, Data=" + UserFunction.JsonString(dataNetwork) });
                }
            }

            var datalog = new InmatesLog
            {
                RegID = RegId,
                CreatedDate = DateTime.Now.ToLocalTime(),
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
            };
            inmateslog.Post(datalog);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Add New InmatesLog, Data=" + UserFunction.JsonString(datalog) });

        }

        private void UpdateInmates()
        {
            InmatesService inmatesserv = new InmatesService();
            var OldData = inmatesserv.GetDetailById(this.Tag.ToString());
            RegId = OldData.RegID;

            InmatesService inmates = new InmatesService();
            var data = new Inmates
            {
                Id = Convert.ToInt32(this.Tag),
                RegID = RegId,
                FullName = txtNama.Text,
                PlaceOfBirth = txtPlaceofBirth.Text,
                DateOfBirth = dpDateofBirth.Value,
                IDType = ddlJnsIdentitas.SelectedItem.Text,
                IDNo = txtNoIdentitas.Text,
                Address = txtAddress.Text,
                TelpNo = txtTelp.Text,
                Religion = ddlAgama.SelectedItem.Text,
                Gender = ddlGender.SelectedItem.Text,
                MaritalStatus = ddlMaritalStatus.SelectedItem.Text,
                Nationality = ddlNationality.SelectedItem.Text,
                Vocation = ddlVocation.SelectedItem.Text,
                City = txtCity.Text,
                Category = ddlCategory.SelectedItem.Text,
                Type = ddlType.SelectedItem.Text,
                Network = string.Empty,
                NetworkId = Convert.ToInt32(ddlNetwork.SelectedValue),
                SourceLocation = txtSourceLocation.Text,
                Notes = txtNotes.Text,
                DateReg = DateTime.Now.ToLocalTime(),
                CaseId = Convert.ToInt32(ddlCases.SelectedValue),
                Peran = txtPeranTahanan.Text,
                TanggalKejadian = dtTanggalKejadian.Value,
                TanggalPenangkapan = dtTanggalPenangkapan.Value,
                LokasiPenangkapan = txtLokasiPenangkapan.Text,
                BeratBadan = txtBB.Text,
                TinggiBadan = txtTB.Text,
                NoSuratPenangkapan = txtSuratPenangkapan.Text,
                NoSuratPenahanan = txtSuratPenahanan.Text,
                TanggalPenahanan = dtTanggalPenahanan.Value,

                UpdatedDate = DateTime.Now.ToLocalTime(),
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
            };
            inmates.Update(data);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Update Existing Inmates, Old Data =" + UserFunction.JsonString(OldData) + ", New Data=" + UserFunction.JsonString(data) });

            CasesService caseServ = new CasesService();
            caseServ.DeleteInmatesCase(RegId);

            foreach (var _item in DDLCases.Items)
            {
                if (((RadCheckedListDataItem)_item).Checked)
                {
                    var dataCase = new InmatesCase
                    {
                        RegID = RegId,
                        CaseId = Convert.ToInt32(_item.Value),
                        CreatedDate = DateTime.Now.ToLocalTime(),
                        CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
                    };
                    caseServ.PostCases(dataCase);
                    UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Add New Cases, Data=" + UserFunction.JsonString(dataCase) });
                }
            }

            NetworkService networkServ = new NetworkService();
            networkServ.DeleteNetworkByRegID(RegId);

            foreach (var _item in DDLNetwork.Items)
            {
                if (((RadCheckedListDataItem)_item).Checked)
                {
                    var dataNetwork = new InmatesNetwork
                    {
                        RegID = RegId,
                        NetworkId = Convert.ToInt32(_item.Value),
                        CreatedDate = DateTime.Now.ToLocalTime(),
                        CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID),
                    };
                    networkServ.PostNetwork(dataNetwork);
                    UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERINMATES.ToString(), Activities = "Add New Network, Data=" + UserFunction.JsonString(dataNetwork) });
                }
            }
        }

        private bool DataValid()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;

            if (this.ddlJnsIdentitas.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Jenis Identitas!";
                return false;
            }

            if (this.txtNoIdentitas.Text == string.Empty)
            {
                if(ddlJnsIdentitas.SelectedValue.ToString() == "4")
                {
                    return true;
                }
                else { 
                    this.lblError.Visible = true;
                    this.lblError.Text = "Mohon Isi Nomor Identitas!";
                    return false;
                }
            }
            if (this.txtNama.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Nama Lengkap!";
                return false;
            }
            if (this.txtPlaceofBirth.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tempat Lahir!";
                return false;
            }
            if (this.dpDateofBirth.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tanggal Lahir!";
                return false;
            }
            if (this.txtAddress.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Alamat!";
                return false;
            }
            if (this.txtTelp.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Nomor Telepon!";
                return false;
            }
            if (this.ddlGender.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Jenis Kelamin!";
                return false;
            }
            if (this.txtBB.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Berat Badan!";
                return false;
            }
            if (this.txtTB.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Tinggi Badan!";
                return false;
            }
            if (this.ddlCategory.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Silahkan Pilih Kategori Tahanan!";
                return false;
            }
            if (this.ddlType.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Silahkan Pilih Tipe Tahanan!";
                return false;
            }
            if (this.txtSourceLocation.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Lokasi!";
                return false;
            }
            if (this.ddlNetwork.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Silahkan Pilih Jaringan!";
                return false;
            }
            if (this.ddlCases.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Silahkan Pilih Kasus!";
                return false;
            }
            if(this.txtPeranTahanan.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Silahkan Isi Peran!";
                return false;
            }
            if (this.dtTanggalKejadian.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Silahkan Isi Tanggal Kejadian!";
            }
            if (this.dtTanggalPenangkapan.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Silahkan Isi Tanggal Penangkapan!";
            }
            if (this.txtNotes.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Catatan!";
                return false;
            }

            if ((this.PhotoList.Count == 0 || this.PhotoList.Count < 3) && fingerprint == true)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Ambil Gambar(Foto) Tahanan secara lengkap (Depan + Kiri + Kanan)!";
                return false;
            }

            return true;
        }

        private void TambahTersangka_Load(object sender, EventArgs e)
        {
            JenisIdentitasService jnsidentitas = new JenisIdentitasService();
            this.ddlJnsIdentitas.DisplayMember = "Nama";
            this.ddlJnsIdentitas.ValueMember = "Id";
            this.ddlJnsIdentitas.DataSource = jnsidentitas.Get();

        }

        //private void StartFrame()
        //{
        //    picDelete.Hide();
        //    CaptureDevice = new FilterInfoCollection(FilterCategory.VideoInputDevice);
        //    cameraSource.ddlCameraSource.Items.Clear();
        //    foreach (FilterInfo Device in CaptureDevice)
        //    {
        //        cameraSource.ddlCameraSource.Items.Add(Device.Name);
        //    }
        //    cameraSource.ddlCameraSource.SelectedIndex = 0;
        //    FinalFrame = new VideoCaptureDevice();
        //    picCanvas.Image = null;
        //    FinalFrame = new VideoCaptureDevice(CaptureDevice[cameraSource.ddlCameraSource.SelectedIndex].MonikerString);
        //    FinalFrame.NewFrame += new NewFrameEventHandler(FinalFrame_NewFrame);
        //    FinalFrame.Start();
        //}

        public void LoadDataById(string id)
        {
            InmatesService inmateserv = new InmatesService();
            var data = inmateserv.GetDetailById(id);
            LoadDataInmates(data);
            txtNoIdentitas.Focus();
        }

        //private void StopFrame()
        //{
        //    if (FinalFrame == null)
        //        return;

        //    if (FinalFrame.IsRunning == true)
        //    {
        //        FinalFrame.Stop();
        //    }
        //}

        //private void FinalFrame_NewFrame(object sender, NewFrameEventArgs eventArgs)
        //{
        //    picCanvas.Image = (Bitmap)eventArgs.Frame.Clone();
        //}

        private void Form_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (this.Tag == null)
                {
                    var question = UserFunction.Confirm("REGISTRASI DENGAN IDENTIFIKASI SIDIK JARI?");
                    if (question == DialogResult.Yes)
                    {
                        fingerprint = true;

                        MainForm.formMain.CameraSource.Location =
                        new Point(this.Width / 2 - MainForm.formMain.CameraSource.Size.Width / 2,
                          this.Height / 2 - MainForm.formMain.CameraSource.Size.Height / 2);

                        GlobalVariables.isVisitor = false;

                        UserFunction.ClearControls(tableLayoutPanel1);
                        MainForm.formMain.CaptureFingerPrint.Show();
                        MainForm.formMain.CaptureFingerPrint.ResetDevice();
                    }
                    else
                    {
                        fingerprint = false;

                        //StopFrame();
                        if (MainForm.formMain != null)
                            MainForm.formMain.CaptureFingerPrint.CloseDevice();

                        UserFunction.ClearControls(tableLayoutPanel1);
                    }
                }
                else
                {
                    //edit
                    MainForm.formMain.CameraSource.Location =
                        new Point(this.Width / 2 - MainForm.formMain.CameraSource.Size.Width / 2,
                          this.Height / 2 - MainForm.formMain.CameraSource.Size.Height / 2);


                    MainForm.formMain.CaptureFingerPrint.Show();
                    MainForm.formMain.CaptureFingerPrint.ResetDevice();
                    MainForm.formMain.CaptureFingerPrint.Hide();
                }

            }
            else
            {
                //StopFrame();
                if (MainForm.formMain != null)
                    MainForm.formMain.CaptureFingerPrint.CloseDevice();

                UserFunction.ClearControls(tableLayoutPanel1);
            }
        }
    }
}
