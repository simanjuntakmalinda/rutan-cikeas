﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class FormUser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUser));
            this.lblDetailSurat = new Telerik.WinControls.UI.RadLabel();
            this.headerPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.chkTampilKonfirmasi = new Telerik.WinControls.UI.RadCheckBox();
            this.radPanel15 = new Telerik.WinControls.UI.RadPanel();
            this.chk11 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel14 = new Telerik.WinControls.UI.RadPanel();
            this.chk10 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel13 = new Telerik.WinControls.UI.RadPanel();
            this.chk8 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel11 = new Telerik.WinControls.UI.RadPanel();
            this.chk9 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.ddlLocked = new Telerik.WinControls.UI.RadDropDownList();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.UsertxtPassword2 = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.UsertxtPassword1 = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.UsertxtUserName = new Telerik.WinControls.UI.RadTextBox();
            this.lblDetailUser = new Telerik.WinControls.UI.RadLabel();
            this.radLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailModul = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel12 = new Telerik.WinControls.UI.RadPanel();
            this.chk1 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel5 = new Telerik.WinControls.UI.RadPanel();
            this.chk6 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel6 = new Telerik.WinControls.UI.RadPanel();
            this.chk2 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel7 = new Telerik.WinControls.UI.RadPanel();
            this.chk3 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel10 = new Telerik.WinControls.UI.RadPanel();
            this.chk7 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel8 = new Telerik.WinControls.UI.RadPanel();
            this.chk5 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel9 = new Telerik.WinControls.UI.RadPanel();
            this.chk4 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.chkTampilKataSandi = new Telerik.WinControls.UI.RadCheckBox();
            this.radPanelError = new Telerik.WinControls.UI.RadPanel();
            this.lblError = new Telerik.WinControls.UI.RadLabel();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnBack = new Telerik.WinControls.UI.RadButton();
            this.btnSave = new Telerik.WinControls.UI.RadButton();
            this.object_06893159_dd6e_446b_a51f_c30e4803e016 = new Telerik.WinControls.UI.RadLabelRootElement();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).BeginInit();
            this.headerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTampilKonfirmasi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel15)).BeginInit();
            this.radPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel14)).BeginInit();
            this.radPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel13)).BeginInit();
            this.radPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel11)).BeginInit();
            this.radPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            this.radPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlLocked)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UsertxtPassword2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UsertxtPassword1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UsertxtUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailModul)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel12)).BeginInit();
            this.radPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).BeginInit();
            this.radPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).BeginInit();
            this.radPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).BeginInit();
            this.radPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel10)).BeginInit();
            this.radPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).BeginInit();
            this.radPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel9)).BeginInit();
            this.radPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTampilKataSandi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).BeginInit();
            this.radPanelError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDetailSurat
            // 
            this.lblDetailSurat.AutoSize = false;
            this.lblDetailSurat.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDetailSurat.Location = new System.Drawing.Point(0, 56);
            this.lblDetailSurat.Name = "lblDetailSurat";
            this.lblDetailSurat.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetailSurat.Size = new System.Drawing.Size(998, 52);
            this.lblDetailSurat.TabIndex = 67;
            this.lblDetailSurat.Text = "Mohon Isi seluruh Kolom yang Diperlukan";
            this.lblDetailSurat.Click += new System.EventHandler(this.lblDetailSurat_Click);
            // 
            // headerPanel
            // 
            this.headerPanel.Controls.Add(this.lblTitle);
            this.headerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Margin = new System.Windows.Forms.Padding(4);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(998, 56);
            this.headerPanel.TabIndex = 66;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 50, 0);
            this.lblTitle.Size = new System.Drawing.Size(998, 56);
            this.lblTitle.TabIndex = 8;
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 108);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(979, 492);
            this.radScrollablePanel1.Size = new System.Drawing.Size(998, 494);
            this.radScrollablePanel1.TabIndex = 68;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.59524F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.07143F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.Controls.Add(this.chkTampilKonfirmasi, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.radPanel15, 0, 18);
            this.tableLayoutPanel1.Controls.Add(this.radPanel14, 0, 17);
            this.tableLayoutPanel1.Controls.Add(this.radPanel13, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.radPanel11, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.radPanel4, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.radPanel3, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailUser, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.radLabel24, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radLabel2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailModul, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.radLabel4, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.radPanel12, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.radPanel5, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.radPanel6, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.radPanel7, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.radPanel10, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.radPanel8, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.radPanel9, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.chkTampilKataSandi, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 20;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(979, 1000);
            this.tableLayoutPanel1.TabIndex = 16;
            // 
            // chkTampilKonfirmasi
            // 
            this.chkTampilKonfirmasi.AutoSize = false;
            this.chkTampilKonfirmasi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkTampilKonfirmasi.Location = new System.Drawing.Point(292, 263);
            this.chkTampilKonfirmasi.Name = "chkTampilKonfirmasi";
            this.chkTampilKonfirmasi.Size = new System.Drawing.Size(250, 44);
            this.chkTampilKonfirmasi.TabIndex = 5;
            this.chkTampilKonfirmasi.Text = "Tampilkan KonfirmasiKata Sandi";
            // 
            // radPanel15
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel15, 3);
            this.radPanel15.Controls.Add(this.chk11);
            this.radPanel15.Controls.Add(this.radLabel14);
            this.radPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel15.Location = new System.Drawing.Point(13, 913);
            this.radPanel15.Name = "radPanel15";
            this.radPanel15.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.radPanel15.Size = new System.Drawing.Size(831, 44);
            this.radPanel15.TabIndex = 16;
            // 
            // chk11
            // 
            this.chk11.AutoSize = false;
            this.chk11.Location = new System.Drawing.Point(15, 7);
            this.chk11.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk11.Name = "chk11";
            this.chk11.Size = new System.Drawing.Size(41, 30);
            this.chk11.TabIndex = 126;
            // 
            // radLabel14
            // 
            this.radLabel14.AutoSize = false;
            this.radLabel14.Location = new System.Drawing.Point(63, 1);
            this.radLabel14.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(769, 42);
            this.radLabel14.TabIndex = 127;
            this.radLabel14.Text = "Modul Jadwal Kunjungan";
            // 
            // radPanel14
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel14, 3);
            this.radPanel14.Controls.Add(this.chk10);
            this.radPanel14.Controls.Add(this.radLabel13);
            this.radPanel14.Location = new System.Drawing.Point(13, 863);
            this.radPanel14.Name = "radPanel14";
            this.radPanel14.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.radPanel14.Size = new System.Drawing.Size(831, 44);
            this.radPanel14.TabIndex = 15;
            // 
            // chk10
            // 
            this.chk10.AutoSize = false;
            this.chk10.Location = new System.Drawing.Point(15, 7);
            this.chk10.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk10.Name = "chk10";
            this.chk10.Size = new System.Drawing.Size(41, 30);
            this.chk10.TabIndex = 124;
            // 
            // radLabel13
            // 
            this.radLabel13.AutoSize = false;
            this.radLabel13.Location = new System.Drawing.Point(63, 1);
            this.radLabel13.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(769, 42);
            this.radLabel13.TabIndex = 125;
            this.radLabel13.Text = "Modul Kendaraan Tahanan";
            // 
            // radPanel13
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel13, 3);
            this.radPanel13.Controls.Add(this.chk8);
            this.radPanel13.Controls.Add(this.radLabel12);
            this.radPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel13.Location = new System.Drawing.Point(13, 613);
            this.radPanel13.Name = "radPanel13";
            this.radPanel13.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.radPanel13.Size = new System.Drawing.Size(831, 44);
            this.radPanel13.TabIndex = 10;
            // 
            // chk8
            // 
            this.chk8.AutoSize = false;
            this.chk8.Location = new System.Drawing.Point(15, 4);
            this.chk8.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk8.Name = "chk8";
            this.chk8.Size = new System.Drawing.Size(41, 37);
            this.chk8.TabIndex = 113;
            // 
            // radLabel12
            // 
            this.radLabel12.AutoSize = false;
            this.radLabel12.Location = new System.Drawing.Point(63, 1);
            this.radLabel12.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(769, 42);
            this.radLabel12.TabIndex = 114;
            this.radLabel12.Text = "Modul Barang Bukti";
            // 
            // radPanel11
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel11, 3);
            this.radPanel11.Controls.Add(this.chk9);
            this.radPanel11.Controls.Add(this.radLabel11);
            this.radPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel11.Location = new System.Drawing.Point(13, 663);
            this.radPanel11.Name = "radPanel11";
            this.radPanel11.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.radPanel11.Size = new System.Drawing.Size(831, 44);
            this.radPanel11.TabIndex = 11;
            // 
            // chk9
            // 
            this.chk9.AutoSize = false;
            this.chk9.Location = new System.Drawing.Point(15, 4);
            this.chk9.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk9.Name = "chk9";
            this.chk9.Size = new System.Drawing.Size(41, 37);
            this.chk9.TabIndex = 115;
            // 
            // radLabel11
            // 
            this.radLabel11.AutoSize = false;
            this.radLabel11.Location = new System.Drawing.Point(63, 1);
            this.radLabel11.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(769, 42);
            this.radLabel11.TabIndex = 116;
            this.radLabel11.Text = "Modul Bon Tahanan";
            // 
            // radPanel4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel4, 2);
            this.radPanel4.Controls.Add(this.ddlLocked);
            this.radPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel4.Location = new System.Drawing.Point(292, 313);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(552, 44);
            this.radPanel4.TabIndex = 5;
            // 
            // ddlLocked
            // 
            this.ddlLocked.AutoSize = false;
            this.ddlLocked.BackColor = System.Drawing.Color.White;
            this.ddlLocked.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlLocked.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InOutBounce;
            this.ddlLocked.DropDownHeight = 400;
            this.ddlLocked.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlLocked.Location = new System.Drawing.Point(0, 0);
            this.ddlLocked.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.ddlLocked.Name = "ddlLocked";
            // 
            // 
            // 
            this.ddlLocked.RootElement.CustomFont = "Roboto";
            this.ddlLocked.RootElement.CustomFontSize = 13F;
            this.ddlLocked.Size = new System.Drawing.Size(552, 44);
            this.ddlLocked.TabIndex = 104;
            this.ddlLocked.ThemeName = "Material";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlLocked.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlLocked.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlLocked.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlLocked.GetChildAt(0))).Alignment = System.Drawing.ContentAlignment.TopCenter;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlLocked.GetChildAt(0).GetChildAt(2))).CustomFont = "Roboto";
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlLocked.GetChildAt(0).GetChildAt(2))).CustomFontSize = 15F;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlLocked.GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.ddlLocked.GetChildAt(0).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlLocked.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.ddlLocked.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).StretchHorizontally = true;
            // 
            // radPanel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel3, 2);
            this.radPanel3.Controls.Add(this.UsertxtPassword2);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel3.Location = new System.Drawing.Point(292, 213);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(552, 44);
            this.radPanel3.TabIndex = 4;
            // 
            // UsertxtPassword2
            // 
            this.UsertxtPassword2.AutoSize = false;
            this.UsertxtPassword2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UsertxtPassword2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F);
            this.UsertxtPassword2.ForeColor = System.Drawing.Color.Navy;
            this.UsertxtPassword2.Location = new System.Drawing.Point(0, 0);
            this.UsertxtPassword2.Name = "UsertxtPassword2";
            this.UsertxtPassword2.PasswordChar = '*';
            this.UsertxtPassword2.Size = new System.Drawing.Size(552, 44);
            this.UsertxtPassword2.TabIndex = 103;
            // 
            // radPanel2
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel2, 2);
            this.radPanel2.Controls.Add(this.UsertxtPassword1);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(292, 113);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(552, 44);
            this.radPanel2.TabIndex = 2;
            // 
            // UsertxtPassword1
            // 
            this.UsertxtPassword1.AutoSize = false;
            this.UsertxtPassword1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UsertxtPassword1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F);
            this.UsertxtPassword1.ForeColor = System.Drawing.Color.Navy;
            this.UsertxtPassword1.Location = new System.Drawing.Point(0, 0);
            this.UsertxtPassword1.Name = "UsertxtPassword1";
            this.UsertxtPassword1.PasswordChar = '*';
            this.UsertxtPassword1.Size = new System.Drawing.Size(552, 44);
            this.UsertxtPassword1.TabIndex = 101;
            // 
            // radPanel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel1, 2);
            this.radPanel1.Controls.Add(this.UsertxtUserName);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(292, 63);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(552, 44);
            this.radPanel1.TabIndex = 1;
            // 
            // UsertxtUserName
            // 
            this.UsertxtUserName.AutoSize = false;
            this.UsertxtUserName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UsertxtUserName.ForeColor = System.Drawing.Color.Navy;
            this.UsertxtUserName.Location = new System.Drawing.Point(0, 0);
            this.UsertxtUserName.Name = "UsertxtUserName";
            this.UsertxtUserName.Size = new System.Drawing.Size(552, 44);
            this.UsertxtUserName.TabIndex = 100;
            // 
            // lblDetailUser
            // 
            this.lblDetailUser.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailUser, 4);
            this.lblDetailUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailUser.Location = new System.Drawing.Point(13, 13);
            this.lblDetailUser.Name = "lblDetailUser";
            this.lblDetailUser.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetailUser.Size = new System.Drawing.Size(953, 44);
            this.lblDetailUser.TabIndex = 129;
            this.lblDetailUser.Text = "DETAIL PENGGUNA";
            // 
            // radLabel24
            // 
            this.radLabel24.AutoSize = false;
            this.radLabel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel24.Location = new System.Drawing.Point(14, 64);
            this.radLabel24.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel24.Name = "radLabel24";
            this.radLabel24.Size = new System.Drawing.Size(271, 42);
            this.radLabel24.TabIndex = 19;
            this.radLabel24.Text = "<html>Nama Pengguna<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel2.Location = new System.Drawing.Point(14, 214);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(271, 42);
            this.radLabel2.TabIndex = 66;
            this.radLabel2.Text = "<html>Konfirmasi Kata Sandi<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.Location = new System.Drawing.Point(14, 114);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(271, 42);
            this.radLabel1.TabIndex = 65;
            this.radLabel1.Text = "<html>Kata Sandi<span style=\"color: #ff0000\"> *</span></html>";
            // 
            // lblDetailModul
            // 
            this.lblDetailModul.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailModul, 4);
            this.lblDetailModul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailModul.Location = new System.Drawing.Point(13, 363);
            this.lblDetailModul.Name = "lblDetailModul";
            this.lblDetailModul.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.lblDetailModul.Size = new System.Drawing.Size(953, 44);
            this.lblDetailModul.TabIndex = 122;
            this.lblDetailModul.Text = "<html>MODUL PENGGUNA</html>";
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Location = new System.Drawing.Point(14, 314);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(271, 42);
            this.radLabel4.TabIndex = 120;
            this.radLabel4.Text = "<html>Status Login Terkunci<span style=\"color: #ff0000\">*</span></html>";
            // 
            // radPanel12
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel12, 3);
            this.radPanel12.Controls.Add(this.chk1);
            this.radPanel12.Controls.Add(this.radLabel6);
            this.radPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel12.Location = new System.Drawing.Point(13, 413);
            this.radPanel12.Name = "radPanel12";
            this.radPanel12.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.radPanel12.Size = new System.Drawing.Size(831, 44);
            this.radPanel12.TabIndex = 6;
            // 
            // chk1
            // 
            this.chk1.AutoSize = false;
            this.chk1.Location = new System.Drawing.Point(15, 6);
            this.chk1.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk1.Name = "chk1";
            // 
            // 
            // 
            this.chk1.RootElement.AutoSize = false;
            this.chk1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            this.chk1.Size = new System.Drawing.Size(41, 37);
            this.chk1.TabIndex = 105;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).CheckPrimitiveStyle = Telerik.WinControls.Enumerations.CheckPrimitiveStyleEnum.Mac;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk1.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Location = new System.Drawing.Point(63, 1);
            this.radLabel6.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(769, 42);
            this.radLabel6.TabIndex = 106;
            this.radLabel6.Text = "Modul Tahanan";
            // 
            // radPanel5
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel5, 3);
            this.radPanel5.Controls.Add(this.chk6);
            this.radPanel5.Controls.Add(this.radLabel5);
            this.radPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel5.Location = new System.Drawing.Point(13, 463);
            this.radPanel5.Name = "radPanel5";
            this.radPanel5.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.radPanel5.Size = new System.Drawing.Size(831, 44);
            this.radPanel5.TabIndex = 7;
            // 
            // chk6
            // 
            this.chk6.AutoSize = false;
            this.chk6.Location = new System.Drawing.Point(15, 4);
            this.chk6.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk6.Name = "chk6";
            // 
            // 
            // 
            this.chk6.RootElement.AutoSize = false;
            this.chk6.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            this.chk6.Size = new System.Drawing.Size(41, 37);
            this.chk6.TabIndex = 107;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk6.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).CheckPrimitiveStyle = Telerik.WinControls.Enumerations.CheckPrimitiveStyleEnum.Mac;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk6.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk6.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk6.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Location = new System.Drawing.Point(63, 4);
            this.radLabel5.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(769, 39);
            this.radLabel5.TabIndex = 108;
            this.radLabel5.Text = "Modul Pengunjung";
            // 
            // radPanel6
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel6, 3);
            this.radPanel6.Controls.Add(this.chk2);
            this.radPanel6.Controls.Add(this.radLabel7);
            this.radPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel6.Location = new System.Drawing.Point(13, 513);
            this.radPanel6.Name = "radPanel6";
            this.radPanel6.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.radPanel6.Size = new System.Drawing.Size(831, 44);
            this.radPanel6.TabIndex = 8;
            // 
            // chk2
            // 
            this.chk2.AutoSize = false;
            this.chk2.Location = new System.Drawing.Point(15, 4);
            this.chk2.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk2.Name = "chk2";
            this.chk2.Size = new System.Drawing.Size(41, 37);
            this.chk2.TabIndex = 109;
            ((Telerik.WinControls.Primitives.CheckPrimitive)(this.chk2.GetChildAt(0).GetChildAt(1).GetChildAt(1).GetChildAt(2))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Location = new System.Drawing.Point(63, 4);
            this.radLabel7.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(769, 39);
            this.radLabel7.TabIndex = 110;
            this.radLabel7.Text = "Modul Alokasi Sel";
            // 
            // radPanel7
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel7, 3);
            this.radPanel7.Controls.Add(this.chk3);
            this.radPanel7.Controls.Add(this.radLabel8);
            this.radPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel7.Location = new System.Drawing.Point(13, 563);
            this.radPanel7.Name = "radPanel7";
            this.radPanel7.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.radPanel7.Size = new System.Drawing.Size(831, 44);
            this.radPanel7.TabIndex = 9;
            // 
            // chk3
            // 
            this.chk3.AutoSize = false;
            this.chk3.Location = new System.Drawing.Point(15, 4);
            this.chk3.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk3.Name = "chk3";
            this.chk3.Size = new System.Drawing.Size(41, 37);
            this.chk3.TabIndex = 111;
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Location = new System.Drawing.Point(63, 1);
            this.radLabel8.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(769, 42);
            this.radLabel8.TabIndex = 112;
            this.radLabel8.Text = "Modul Pengelolaan User";
            // 
            // radPanel10
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel10, 3);
            this.radPanel10.Controls.Add(this.chk7);
            this.radPanel10.Controls.Add(this.radLabel3);
            this.radPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel10.Location = new System.Drawing.Point(13, 813);
            this.radPanel10.Name = "radPanel10";
            this.radPanel10.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.radPanel10.Size = new System.Drawing.Size(831, 44);
            this.radPanel10.TabIndex = 14;
            // 
            // chk7
            // 
            this.chk7.AutoSize = false;
            this.chk7.Location = new System.Drawing.Point(15, 7);
            this.chk7.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk7.Name = "chk7";
            this.chk7.Size = new System.Drawing.Size(41, 30);
            this.chk7.TabIndex = 121;
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Location = new System.Drawing.Point(63, 1);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(769, 42);
            this.radLabel3.TabIndex = 122;
            this.radLabel3.Text = "Modul Pencetakan Form";
            // 
            // radPanel8
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel8, 3);
            this.radPanel8.Controls.Add(this.chk5);
            this.radPanel8.Controls.Add(this.radLabel10);
            this.radPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel8.Location = new System.Drawing.Point(13, 763);
            this.radPanel8.Name = "radPanel8";
            this.radPanel8.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.radPanel8.Size = new System.Drawing.Size(831, 44);
            this.radPanel8.TabIndex = 13;
            // 
            // chk5
            // 
            this.chk5.AutoSize = false;
            this.chk5.Location = new System.Drawing.Point(15, 4);
            this.chk5.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk5.Name = "chk5";
            this.chk5.Size = new System.Drawing.Size(41, 37);
            this.chk5.TabIndex = 119;
            // 
            // radLabel10
            // 
            this.radLabel10.AutoSize = false;
            this.radLabel10.Location = new System.Drawing.Point(63, 1);
            this.radLabel10.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(769, 42);
            this.radLabel10.TabIndex = 120;
            this.radLabel10.Text = "Modul Audit Trail";
            // 
            // radPanel9
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.radPanel9, 3);
            this.radPanel9.Controls.Add(this.chk4);
            this.radPanel9.Controls.Add(this.radLabel9);
            this.radPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel9.Location = new System.Drawing.Point(13, 713);
            this.radPanel9.Name = "radPanel9";
            this.radPanel9.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.radPanel9.Size = new System.Drawing.Size(831, 44);
            this.radPanel9.TabIndex = 12;
            // 
            // chk4
            // 
            this.chk4.AutoSize = false;
            this.chk4.Location = new System.Drawing.Point(15, 4);
            this.chk4.Margin = new System.Windows.Forms.Padding(30, 10, 3, 3);
            this.chk4.Name = "chk4";
            this.chk4.Size = new System.Drawing.Size(41, 37);
            this.chk4.TabIndex = 117;
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Location = new System.Drawing.Point(63, 4);
            this.radLabel9.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(769, 42);
            this.radLabel9.TabIndex = 118;
            this.radLabel9.Text = "Modul Pelaporan/Reporting";
            // 
            // chkTampilKataSandi
            // 
            this.chkTampilKataSandi.AutoSize = false;
            this.chkTampilKataSandi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkTampilKataSandi.Location = new System.Drawing.Point(292, 163);
            this.chkTampilKataSandi.Name = "chkTampilKataSandi";
            this.chkTampilKataSandi.Size = new System.Drawing.Size(250, 44);
            this.chkTampilKataSandi.TabIndex = 3;
            this.chkTampilKataSandi.Text = "Tampilkan Kata Sandi";
            // 
            // radPanelError
            // 
            this.radPanelError.Controls.Add(this.lblError);
            this.radPanelError.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanelError.Location = new System.Drawing.Point(0, 602);
            this.radPanelError.Margin = new System.Windows.Forms.Padding(0);
            this.radPanelError.Name = "radPanelError";
            this.radPanelError.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.radPanelError.Size = new System.Drawing.Size(998, 43);
            this.radPanelError.TabIndex = 69;
            // 
            // lblError
            // 
            this.lblError.AutoSize = false;
            this.lblError.BackgroundImage = global::RDM.LP.Desktop.Presentation.Properties.Resources.warning;
            this.lblError.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblError.Location = new System.Drawing.Point(15, 0);
            this.lblError.Name = "lblError";
            this.lblError.Padding = new System.Windows.Forms.Padding(40, 0, 0, 0);
            this.lblError.Size = new System.Drawing.Size(983, 39);
            this.lblError.TabIndex = 24;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnBack);
            this.lblButton.Controls.Add(this.btnSave);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 649);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(998, 118);
            this.lblButton.TabIndex = 70;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.Location = new System.Drawing.Point(30, 30);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(244, 58);
            this.btnBack.TabIndex = 18;
            this.btnBack.Tag = "";
            this.btnBack.Text = "&BATAL";
            this.btnBack.ThemeName = "MaterialBlueGrey";
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.if_checked_2_309094;
            this.btnSave.Location = new System.Drawing.Point(663, 30);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(305, 58);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "&SIMPAN";
            this.btnSave.ThemeName = "MaterialBlueGrey";
            // 
            // object_06893159_dd6e_446b_a51f_c30e4803e016
            // 
            this.object_06893159_dd6e_446b_a51f_c30e4803e016.Name = "object_06893159_dd6e_446b_a51f_c30e4803e016";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // FormUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radPanelError);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblDetailSurat);
            this.Controls.Add(this.headerPanel);
            this.Name = "FormUser";
            this.Size = new System.Drawing.Size(998, 767);
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailSurat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).EndInit();
            this.headerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkTampilKonfirmasi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel15)).EndInit();
            this.radPanel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel14)).EndInit();
            this.radPanel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel13)).EndInit();
            this.radPanel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel11)).EndInit();
            this.radPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            this.radPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlLocked)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UsertxtPassword2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UsertxtPassword1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UsertxtUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailModul)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel12)).EndInit();
            this.radPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).EndInit();
            this.radPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).EndInit();
            this.radPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).EndInit();
            this.radPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel10)).EndInit();
            this.radPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).EndInit();
            this.radPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel9)).EndInit();
            this.radPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTampilKataSandi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelError)).EndInit();
            this.radPanelError.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel lblDetailSurat;
        private Telerik.WinControls.UI.RadPanel headerPanel;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel24;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel lblDetailModul;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadCheckBox chk1;
        private Telerik.WinControls.UI.RadCheckBox chk2;
        private Telerik.WinControls.UI.RadCheckBox chk3;
        private Telerik.WinControls.UI.RadCheckBox chk4;
        private Telerik.WinControls.UI.RadPanel radPanelError;
        private Telerik.WinControls.UI.RadLabel lblError;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnBack;
        private Telerik.WinControls.UI.RadButton btnSave;
        private Telerik.WinControls.UI.RadLabel lblDetailUser;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadCheckBox chk5;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadCheckBox chk6;
        private Telerik.WinControls.UI.RadLabelRootElement object_06893159_dd6e_446b_a51f_c30e4803e016;
        private Telerik.WinControls.UI.RadCheckBox chk7;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadDropDownList ddlLocked;
        private Telerik.WinControls.UI.RadPanel radPanel12;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPanel radPanel10;
        private Telerik.WinControls.UI.RadPanel radPanel9;
        private Telerik.WinControls.UI.RadPanel radPanel8;
        private Telerik.WinControls.UI.RadPanel radPanel7;
        private Telerik.WinControls.UI.RadPanel radPanel6;
        private Telerik.WinControls.UI.RadPanel radPanel5;
        private Telerik.WinControls.UI.RadPanel radPanel13;
        private Telerik.WinControls.UI.RadCheckBox chk8;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadPanel radPanel11;
        private Telerik.WinControls.UI.RadCheckBox chk9;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadPanel radPanel14;
        private Telerik.WinControls.UI.RadCheckBox chk10;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadPanel radPanel15;
        private Telerik.WinControls.UI.RadCheckBox chk11;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private Telerik.WinControls.UI.RadTextBox UsertxtPassword2;
        private Telerik.WinControls.UI.RadTextBox UsertxtPassword1;
        private Telerik.WinControls.UI.RadTextBox UsertxtUserName;
        private Telerik.WinControls.UI.RadCheckBox chkTampilKonfirmasi;
        private Telerik.WinControls.UI.RadCheckBox chkTampilKataSandi;
    }
}
