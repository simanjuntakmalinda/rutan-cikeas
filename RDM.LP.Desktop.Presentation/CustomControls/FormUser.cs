﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDM.LP.DataAccess.ViewModel;
using Telerik.WinControls.UI;
using RDM.LP.DataAccess.Service;
using Telerik.WinControls.UI.Data;
using RDM.LP.DataAccess.Helper;

namespace RDM.LP.Desktop.Presentation.CustomControls
{
    public partial class FormUser : Base
    {
        private Point MouseDownLocation;
        private AspnetUsers OldData;


        public FormUser()
        {
            InitializeComponent();
            InitForm();
            LoadData();
            InitEvents();    
        }

        public void LoadData()
        {
            this.lblTitle.Text = "     TAMBAH DATA BARU";
            ClearForm();

            if (this.Tag != null)
            {
                MainForm.formMain.AddNew.Text = "UBAH DATA PENGGUNA";
                this.lblTitle.Text = "     UBAH DATA PENGGUNA";
                AppUserService userserv = new AppUserService();
                var data = userserv.GetById(Convert.ToInt32(this.Tag));
                if (data == null)
                {
                    ddlLocked.SelectedIndex = 1;
                    ddlLocked.Enabled = false;
                    return;
                }
                else
                {
                    //this.lblTitle.Text = "     USER DATA";
                    ddlLocked.SelectedIndex = 0;
                    ddlLocked.Enabled = true;
                    UsertxtUserName.Enabled = false;
                    UsertxtPassword1.Text = "asdfghjkl";
                    UsertxtPassword1.Enabled = false;
                    UsertxtPassword2.Text = "asdfghjkl";
                    UsertxtPassword2.Enabled = false;
                    chkTampilKataSandi.Enabled = false;
                    chkTampilKonfirmasi.Enabled = false;

                    OldData = data;
                    UsertxtUserName.Text = data.UserName;
                    ddlLocked.SelectedValue = data.IsLocked;

                    chk1.Checked = data.InmateModul;
                    chk2.Checked = data.CellAllocationModul;
                    chk3.Checked = data.ReportModul;
                    chk4.Checked = data.UserManagementModul;
                    chk5.Checked = data.AuditTrailModul;
                    chk6.Checked = data.VisitorModul;
                    chk7.Checked = data.PrintFormModul;
                    chk8.Checked = data.SeizedAssetsModul;
                    chk9.Checked = data.BonTahananModul;
                    chk10.Checked = data.KendaraanModul;
                    chk11.Checked = data.JadwalKunjunganModul;
                }
            }
        }

        private void ClearForm()
        {
            this.lblError.Visible = false;
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;
            //ddl Status
            ddlLocked.Items.Clear();
            ddlLocked.Items.Add(new RadListDataItem("YA", 1));
            ddlLocked.Items.Add(new RadListDataItem("TIDAK", 0));
            ddlLocked.SelectedIndex = 1;
            ddlLocked.Enabled = false;

            UsertxtUserName.Text = null;
            UsertxtUserName.Enabled = true;
            UsertxtPassword1.Text = null;
            UsertxtPassword1.Enabled = true;
            UsertxtPassword2.Text = null;
            UsertxtPassword2.Enabled = true;

            chk1.Checked = false;
            chk2.Checked = false;
            chk3.Checked = false;
            chk4.Checked = false;
            chk5.Checked = false;
            chk6.Checked = false;
            chk7.Checked = false;
            chk8.Checked = false;
            chk9.Checked = false;
            chk10.Checked = false;
            chk11.Checked = false;
            chkTampilKataSandi.Checked = false;
            chkTampilKonfirmasi.Checked = false;
            chkTampilKonfirmasi.Enabled = true;
            chkTampilKataSandi.Enabled = true;
        }

        private void InitEvents()
        {
            //Events
            btnSave.Click += btnSave_Click;
            btnBack.Click += btnBack_Click;
            tableLayoutPanel1.CellPaint += tableLayoutPanel_CellPaint;
            lblTitle.MouseDown += lblTitle_MouseDown;
            lblTitle.MouseMove += lblTitle_MouseMove;

            radPanel1.KeyDown += Control_KeyDown;
            radPanel2.KeyDown += Control_KeyDown;
            radPanel3.KeyDown += Control_KeyDown;
            radPanel4.KeyDown += Control_KeyDown;
            radPanel5.KeyDown += Control_KeyDown;
            radPanel6.KeyDown += Control_KeyDown;
            radPanel7.KeyDown += Control_KeyDown;
            radPanel8.KeyDown += Control_KeyDown;
            radPanel9.KeyDown += Control_KeyDown;
            radPanel10.KeyDown += Control_KeyDown;
            radPanel11.KeyDown += Control_KeyDown;
            radPanel12.KeyDown += Control_KeyDown;
            radPanel13.KeyDown += Control_KeyDown;
            radPanel14.KeyDown += Control_KeyDown;
            radPanel15.KeyDown += Control_KeyDown;
            chkTampilKataSandi.KeyDown += Control_KeyDown;
            chkTampilKonfirmasi.KeyDown += Control_KeyDown;
            UsertxtUserName.KeyDown += Control_KeyDown;
            UsertxtPassword1.KeyDown += Control_KeyDown;
            UsertxtPassword2.KeyDown += Control_KeyDown;
            ddlLocked.KeyDown += Control_KeyDown;
            chk1.KeyDown += Control_KeyDown;
            chk2.KeyDown += Control_KeyDown;
            chk3.KeyDown += Control_KeyDown;
            chk4.KeyDown += Control_KeyDown;
            chk5.KeyDown += Control_KeyDown;
            chk6.KeyDown += Control_KeyDown;
            chk7.KeyDown += Control_KeyDown;
            chk8.KeyDown += Control_KeyDown;
            chk9.KeyDown += Control_KeyDown;
            chk10.KeyDown += Control_KeyDown;
            btnSave.KeyDown += Control_KeyDown;
            btnBack.KeyDown += Control_KeyDown;

            chkTampilKataSandi.CheckStateChanged += ChkTampilKataSandi_CheckStateChanged; ;
            chkTampilKonfirmasi.CheckStateChanged += ChkTampilKonfirmasi_CheckStateChanged;

        }

        private void ChkTampilKonfirmasi_CheckStateChanged(object sender, EventArgs e)
        {
            this.UsertxtPassword2.PasswordChar = this.chkTampilKonfirmasi.Checked ? char.MinValue : '*';
        }

        private void ChkTampilKataSandi_CheckStateChanged(object sender, EventArgs e)
        {
            this.UsertxtPassword1.PasswordChar = this.chkTampilKataSandi.Checked ? char.MinValue : '*';
        }

        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
                SendKeys.Send("{TAB}");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataValid())
            {
                if (this.Tag == null)
                    SaveUser();
                else
                    UpdatetData();

                UserFunction.ClearControls(tableLayoutPanel1);
                UserFunction.MsgBox(TipeMsg.Info, "Data Berhasil Disimpan !");
                UserFunction.LoadDataUserToGrid(MainForm.formMain.ListUser.GvUser);
                MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
                MainForm.formMain.AddNew.Text = "TAMBAH DATA BARU";
                this.lblTitle.Text = "     TAMBAH DATA BARU";
                this.Tag = null;
            }
        }

        private void SaveUser()
        {
            if(this.Tag == null)
                InsertData();
            else
                UpdatetData();
        }

        private void UpdatetData()
        {
            var username = UsertxtUserName.Text;
            AppUserService userserv = new AppUserService();
            var user = new AspnetUsers
            {
                Id = Convert.ToInt32(this.Tag),
                UserName = UsertxtUserName.Text,
                IsLocked = Convert.ToInt32(ddlLocked.SelectedItem.Tag),
                InmateModul = chk1.Checked,
                VisitorModul = chk6.Checked,
                CellAllocationModul = chk2.Checked,
                UserManagementModul = chk3.Checked,
                ReportModul = chk4.Checked,
                AuditTrailModul = chk5.Checked,
                PrintFormModul = chk7.Checked,
                SeizedAssetsModul = chk8.Checked,
                BonTahananModul = chk7.Checked,
                KendaraanModul = chk10.Checked,
                JadwalKunjunganModul = chk11.Checked,
                UpdatedDate = DateTime.Now.ToLocalTime(),
                UpdatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
            };
            userserv.Update(user);
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERUSER.ToString(), Activities = "Update User, Old Data=" + UserFunction.JsonString(OldData) + ",  New Data=" + UserFunction.JsonString(user) });
        }

        private void InsertData()
        {
            var username = UsertxtUserName.Text;
            var psw = SimpleRSA.Encrypt(UsertxtPassword1.Text);
            AppUserService userserv = new AppUserService();
            var user = new AspnetUsers
            {
                UserName = username,
                Password = psw,
                IsLocked = 0,
                IsLoggedIn = 0,
                InmateModul = chk1.Checked,
                VisitorModul = chk6.Checked,
                CellAllocationModul = chk2.Checked,
                UserManagementModul = chk3.Checked,
                ReportModul = chk4.Checked,
                AuditTrailModul = chk5.Checked,
                PrintFormModul = chk7.Checked,
                SeizedAssetsModul = chk8.Checked,
                BonTahananModul = chk9.Checked,
                KendaraanModul = chk10.Checked,
                JadwalKunjunganModul = chk11.Checked,
                CreatedDate = DateTime.Now.ToLocalTime(),
                CreatedBy = UserFunction.GetSettings(SaveSettingsType.UserID)
            };
            userserv.Post(user);
            user.Password = null;
            UserFunction.SaveActivitiesLog(new AuditTrail { Modul = ModulName.MASTERUSER.ToString(), Activities = "Add New User, Data=" + UserFunction.JsonString(user) });
        }

        private void InitForm()
        {
            this.lblError.Visible = false;
            this.headerPanel.RootElement.EnableElementShadow = false;
            this.headerPanel.BackColor = Global.MainColor;

            this.lblTitle.Text = "     TAMBAH DATA BARU";
            this.lblTitle.ForeColor = Color.White;
            this.lblTitle.LabelElement.CustomFont = Global.MainFont;
            this.lblTitle.LabelElement.CustomFontSize = 18.5f;
            this.lblTitle.LabelElement.CustomFontStyle = FontStyle.Bold;

            this.lblButton.BackColor = Global.MainColor;
            this.lblButton.LabelElement.CustomFont = Global.MainFont;
            this.lblButton.LabelElement.CustomFontSize = 18.5f;

            this.lblDetailSurat.LabelElement.CustomFont = Global.MainFont;
            this.lblDetailSurat.LabelElement.CustomFontSize = 14f;
            this.lblDetailSurat.ForeColor = Color.White;
            this.lblDetailSurat.BackColor = Color.FromArgb(77,77,77);

            this.btnSave.ButtonElement.CustomFont = Global.MainFont;
            this.btnSave.ButtonElement.CustomFontSize = 15.5f;

            this.btnBack.ButtonElement.CustomFont = Global.MainFont;
            this.btnBack.ButtonElement.CustomFontSize = 15.5f;

            this.lblError.LabelElement.CustomFont = Global.MainFont;
            this.lblError.LabelElement.CustomFontSize = 15.5f;

            UserFunction.SetDefaultTableLayoutPanelSetting(tableLayoutPanel1);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ClearForm();
            this.Tag = null;
            MainForm.formMain.AddNew.Text = "TAMBAH DATA BARU";
            this.lblTitle.Text = "     TAMBAH DATA BARU";
            UserFunction.ClearControls(tableLayoutPanel1);
            MainForm.formMain.MainContainer.SelectedPage = MainForm.formMain.Overview;
        }

        private bool DataValid()
        {
            this.lblError.Text = string.Empty;
            this.lblError.ForeColor = Color.Red;
            if (this.UsertxtUserName.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon isi Nama Pengguna!";
                return false;
            }
            if (this.UsertxtPassword1.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Kata Sandi!";
                return false;
            }

            if (this.UsertxtPassword1.Text.Length < 5)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Kata Sandi minimal 5 karakter!";
                return false;
            }

            if (this.UsertxtPassword2.Text == string.Empty)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon Isi Konfirmasi Kata Sandi!";
                return false;
            }

            if (this.UsertxtPassword2.Text.Length < 5)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Konfirmasi Kata Sandi minimal 5 karakter!";
                return false;
            }

            if (this.UsertxtPassword1.Text != this.UsertxtPassword2.Text)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Kata Sandi dan Konfirmasi Kata Sandi tidak sama!";
                return false;
            }

            if (!chk1.Checked && !chk2.Checked && !chk3.Checked && !chk4.Checked && !chk5.Checked && !chk6.Checked && !chk7.Checked && !chk8.Checked && !chk10.Checked)
            {
                this.lblError.Visible = true;
                this.lblError.Text = "Mohon pilih minimal 1 modul pengguna!";
                return false;
            }

            if (this.Tag == null) { 
                AppUserService userserv = new AppUserService();
                if (userserv.GetByUserName(UsertxtUserName.Text) != null)
                {
                    this.lblError.Visible = true;
                    this.lblError.Text = "Nama Pengguna sudah ada!";
                    return false;
                }
            }
            return true;
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Left = e.X + this.Left - MouseDownLocation.X;
                this.Top = e.Y + this.Top - MouseDownLocation.Y;
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                MouseDownLocation = e.Location;
            }
        }

        private void lblDetailSurat_Click(object sender, EventArgs e)
        {

        }
    }
}
