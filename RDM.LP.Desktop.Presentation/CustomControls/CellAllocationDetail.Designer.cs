﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class CellAllocationDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CellAllocationDetail));
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.universitas = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtRoommate = new Telerik.WinControls.UI.RadLabel();
            this.lblTanggalPenangkapan = new Telerik.WinControls.UI.RadLabel();
            this.lblTanggalKejadian = new Telerik.WinControls.UI.RadLabel();
            this.lblLokasiPenangkapan = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.txtPeran = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.lblphoto = new Telerik.WinControls.UI.RadLabel();
            this.lblrightthumb = new Telerik.WinControls.UI.RadLabel();
            this.lblleft = new Telerik.WinControls.UI.RadLabel();
            this.lblright = new Telerik.WinControls.UI.RadLabel();
            this.lblfront = new Telerik.WinControls.UI.RadLabel();
            this.lblRegID = new Telerik.WinControls.UI.RadLabel();
            this.picCanvas4 = new System.Windows.Forms.PictureBox();
            this.picCanvas3 = new System.Windows.Forms.PictureBox();
            this.picCanvas2 = new System.Windows.Forms.PictureBox();
            this.picCanvas1 = new System.Windows.Forms.PictureBox();
            this.txtNetwork = new Telerik.WinControls.UI.RadLabel();
            this.txtCategory = new Telerik.WinControls.UI.RadLabel();
            this.txtType = new Telerik.WinControls.UI.RadLabel();
            this.txtReligion = new Telerik.WinControls.UI.RadLabel();
            this.txtGender = new Telerik.WinControls.UI.RadLabel();
            this.txtIdentity = new Telerik.WinControls.UI.RadLabel();
            this.txtFullname = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.txtTotalInused = new Telerik.WinControls.UI.RadLabel();
            this.txtTotalFloor = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.txtAllocationStatus = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.txtDateEnter = new Telerik.WinControls.UI.RadLabel();
            this.txtBuildName = new Telerik.WinControls.UI.RadLabel();
            this.txtCellCode = new Telerik.WinControls.UI.RadLabel();
            this.lblJenisKelamin = new Telerik.WinControls.UI.RadLabel();
            this.lblNama = new Telerik.WinControls.UI.RadLabel();
            this.lblTglLahir = new Telerik.WinControls.UI.RadLabel();
            this.lblDataUser = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailData = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.txtTypeCell = new Telerik.WinControls.UI.RadCheckBox();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnClose = new Telerik.WinControls.UI.RadButton();
            this.fullpicPanel = new Telerik.WinControls.UI.RadPanel();
            this.closefull = new System.Windows.Forms.PictureBox();
            this.fullPic = new System.Windows.Forms.PictureBox();
            this.lblDetailVisit = new Telerik.WinControls.UI.RadLabel();
            this.pdf = new System.Windows.Forms.PictureBox();
            this.excel = new System.Windows.Forms.PictureBox();
            this.gvHistory = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRoommate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanggalPenangkapan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanggalKejadian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLokasiPenangkapan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeran)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblphoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblrightthumb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblleft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblright)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblfront)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNetwork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReligion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalInused)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalFloor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllocationStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateEnter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBuildName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCellCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJenisKelamin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglLahir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTypeCell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullpicPanel)).BeginInit();
            this.fullpicPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closefull)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisit)).BeginInit();
            this.lblDetailVisit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvHistory.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // universitas
            // 
            this.universitas.AutoSize = false;
            this.universitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.universitas.Location = new System.Drawing.Point(504, 414);
            this.universitas.Margin = new System.Windows.Forms.Padding(4);
            this.universitas.Name = "universitas";
            this.universitas.Size = new System.Drawing.Size(728, 42);
            this.universitas.TabIndex = 53;
            this.universitas.Text = "Tahun Lulus";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 56);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(1243, 700);
            this.radScrollablePanel1.Size = new System.Drawing.Size(1262, 702);
            this.radScrollablePanel1.TabIndex = 15;
            this.radScrollablePanel1.VerticalScrollBarState = Telerik.WinControls.UI.ScrollState.AlwaysShow;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.89809F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.10191F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.35032F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.64968F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 442F));
            this.tableLayoutPanel1.Controls.Add(this.gvHistory, 0, 18);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailVisit, 0, 17);
            this.tableLayoutPanel1.Controls.Add(this.txtRoommate, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblTanggalPenangkapan, 4, 15);
            this.tableLayoutPanel1.Controls.Add(this.lblTanggalKejadian, 4, 14);
            this.tableLayoutPanel1.Controls.Add(this.lblLokasiPenangkapan, 1, 15);
            this.tableLayoutPanel1.Controls.Add(this.radLabel8, 3, 15);
            this.tableLayoutPanel1.Controls.Add(this.radLabel6, 3, 14);
            this.tableLayoutPanel1.Controls.Add(this.radLabel5, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.txtPeran, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.radLabel3, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtNetwork, 4, 13);
            this.tableLayoutPanel1.Controls.Add(this.txtCategory, 4, 12);
            this.tableLayoutPanel1.Controls.Add(this.txtType, 4, 11);
            this.tableLayoutPanel1.Controls.Add(this.txtReligion, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.txtGender, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.txtIdentity, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.txtFullname, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.radLabel12, 3, 13);
            this.tableLayoutPanel1.Controls.Add(this.radLabel7, 3, 12);
            this.tableLayoutPanel1.Controls.Add(this.radLabel16, 3, 11);
            this.tableLayoutPanel1.Controls.Add(this.radLabel15, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.radLabel11, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.radLabel9, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.txtTotalInused, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtTotalFloor, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.radLabel4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.radLabel2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtAllocationStatus, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtDateEnter, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtBuildName, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtCellCode, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblJenisKelamin, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblNama, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblTglLahir, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblDataUser, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailData, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.radLabel10, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.txtTypeCell, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 20;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 450F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1243, 1374);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // txtRoommate
            // 
            this.txtRoommate.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtRoommate, 2);
            this.txtRoommate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtRoommate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRoommate.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtRoommate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtRoommate.Location = new System.Drawing.Point(324, 213);
            this.txtRoommate.Name = "txtRoommate";
            this.txtRoommate.Size = new System.Drawing.Size(347, 54);
            this.txtRoommate.TabIndex = 533;
            this.txtRoommate.Click += new System.EventHandler(this.txtRoommate_Click);
            // 
            // lblTanggalPenangkapan
            // 
            this.lblTanggalPenangkapan.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblTanggalPenangkapan, 2);
            this.lblTanggalPenangkapan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTanggalPenangkapan.Location = new System.Drawing.Point(678, 752);
            this.lblTanggalPenangkapan.Margin = new System.Windows.Forms.Padding(4);
            this.lblTanggalPenangkapan.Name = "lblTanggalPenangkapan";
            this.lblTanggalPenangkapan.Size = new System.Drawing.Size(551, 42);
            this.lblTanggalPenangkapan.TabIndex = 532;
            // 
            // lblTanggalKejadian
            // 
            this.lblTanggalKejadian.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblTanggalKejadian, 2);
            this.lblTanggalKejadian.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTanggalKejadian.Location = new System.Drawing.Point(678, 702);
            this.lblTanggalKejadian.Margin = new System.Windows.Forms.Padding(4);
            this.lblTanggalKejadian.Name = "lblTanggalKejadian";
            this.lblTanggalKejadian.Size = new System.Drawing.Size(551, 42);
            this.lblTanggalKejadian.TabIndex = 531;
            // 
            // lblLokasiPenangkapan
            // 
            this.lblLokasiPenangkapan.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblLokasiPenangkapan, 2);
            this.lblLokasiPenangkapan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLokasiPenangkapan.Location = new System.Drawing.Point(231, 752);
            this.lblLokasiPenangkapan.Margin = new System.Windows.Forms.Padding(4);
            this.lblLokasiPenangkapan.Name = "lblLokasiPenangkapan";
            this.lblLokasiPenangkapan.Size = new System.Drawing.Size(242, 42);
            this.lblLokasiPenangkapan.TabIndex = 530;
            // 
            // radLabel8
            // 
            this.radLabel8.AutoSize = false;
            this.radLabel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel8.Location = new System.Drawing.Point(481, 752);
            this.radLabel8.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(189, 42);
            this.radLabel8.TabIndex = 529;
            this.radLabel8.Text = "TANGGAL PENANGKAPAN";
            // 
            // radLabel6
            // 
            this.radLabel6.AutoSize = false;
            this.radLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel6.Location = new System.Drawing.Point(481, 702);
            this.radLabel6.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(189, 42);
            this.radLabel6.TabIndex = 528;
            this.radLabel6.Text = "TANGGAL KEJADIAN";
            // 
            // radLabel5
            // 
            this.radLabel5.AutoSize = false;
            this.radLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel5.Location = new System.Drawing.Point(14, 752);
            this.radLabel5.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(209, 42);
            this.radLabel5.TabIndex = 527;
            this.radLabel5.Text = "LOKASI PENANGKAPAN";
            // 
            // txtPeran
            // 
            this.txtPeran.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtPeran, 2);
            this.txtPeran.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPeran.Location = new System.Drawing.Point(231, 702);
            this.txtPeran.Margin = new System.Windows.Forms.Padding(4);
            this.txtPeran.Name = "txtPeran";
            this.txtPeran.Size = new System.Drawing.Size(242, 42);
            this.txtPeran.TabIndex = 526;
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel3.Location = new System.Drawing.Point(14, 702);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(209, 42);
            this.radLabel3.TabIndex = 525;
            this.radLabel3.Text = "PERAN";
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.Black;
            this.radPanel1.Controls.Add(this.lblphoto);
            this.radPanel1.Controls.Add(this.lblrightthumb);
            this.radPanel1.Controls.Add(this.lblleft);
            this.radPanel1.Controls.Add(this.lblright);
            this.radPanel1.Controls.Add(this.lblfront);
            this.radPanel1.Controls.Add(this.lblRegID);
            this.radPanel1.Controls.Add(this.picCanvas4);
            this.radPanel1.Controls.Add(this.picCanvas3);
            this.radPanel1.Controls.Add(this.picCanvas2);
            this.radPanel1.Controls.Add(this.picCanvas1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.radPanel1.Location = new System.Drawing.Point(819, 10);
            this.radPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.radPanel1.Name = "radPanel1";
            this.tableLayoutPanel1.SetRowSpan(this.radPanel1, 10);
            this.radPanel1.Size = new System.Drawing.Size(414, 538);
            this.radPanel1.TabIndex = 524;
            // 
            // lblphoto
            // 
            this.lblphoto.AutoSize = false;
            this.lblphoto.BackColor = System.Drawing.Color.Transparent;
            this.lblphoto.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblphoto.ForeColor = System.Drawing.Color.White;
            this.lblphoto.Image = ((System.Drawing.Image)(resources.GetObject("lblphoto.Image")));
            this.lblphoto.Location = new System.Drawing.Point(10, 7);
            this.lblphoto.Name = "lblphoto";
            this.lblphoto.Size = new System.Drawing.Size(398, 36);
            this.lblphoto.TabIndex = 144;
            this.lblphoto.Text = "      FOTO DAN SIDIK JARI";
            // 
            // lblrightthumb
            // 
            this.lblrightthumb.AutoSize = false;
            this.lblrightthumb.BackColor = System.Drawing.Color.Transparent;
            this.lblrightthumb.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrightthumb.ForeColor = System.Drawing.Color.White;
            this.lblrightthumb.Location = new System.Drawing.Point(215, 465);
            this.lblrightthumb.Name = "lblrightthumb";
            this.lblrightthumb.Size = new System.Drawing.Size(191, 22);
            this.lblrightthumb.TabIndex = 143;
            this.lblrightthumb.Text = "IBU JARI KANAN";
            this.lblrightthumb.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblleft
            // 
            this.lblleft.AutoSize = false;
            this.lblleft.BackColor = System.Drawing.Color.Transparent;
            this.lblleft.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblleft.ForeColor = System.Drawing.Color.White;
            this.lblleft.Location = new System.Drawing.Point(16, 465);
            this.lblleft.Name = "lblleft";
            this.lblleft.Size = new System.Drawing.Size(191, 22);
            this.lblleft.TabIndex = 142;
            this.lblleft.Text = "KIRI";
            this.lblleft.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblright
            // 
            this.lblright.AutoSize = false;
            this.lblright.BackColor = System.Drawing.Color.Transparent;
            this.lblright.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblright.ForeColor = System.Drawing.Color.White;
            this.lblright.Location = new System.Drawing.Point(218, 248);
            this.lblright.Name = "lblright";
            this.lblright.Size = new System.Drawing.Size(191, 22);
            this.lblright.TabIndex = 141;
            this.lblright.Text = "KANAN";
            this.lblright.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblfront
            // 
            this.lblfront.AutoSize = false;
            this.lblfront.BackColor = System.Drawing.Color.Transparent;
            this.lblfront.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfront.ForeColor = System.Drawing.Color.White;
            this.lblfront.Location = new System.Drawing.Point(16, 248);
            this.lblfront.Name = "lblfront";
            this.lblfront.Size = new System.Drawing.Size(191, 22);
            this.lblfront.TabIndex = 140;
            this.lblfront.Text = "DEPAN";
            this.lblfront.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRegID
            // 
            this.lblRegID.AutoSize = false;
            this.lblRegID.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblRegID.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegID.ForeColor = System.Drawing.Color.White;
            this.lblRegID.Location = new System.Drawing.Point(0, 492);
            this.lblRegID.Name = "lblRegID";
            this.lblRegID.Size = new System.Drawing.Size(414, 46);
            this.lblRegID.TabIndex = 139;
            this.lblRegID.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picCanvas4
            // 
            this.picCanvas4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.picCanvas4.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picCanvas4.Location = new System.Drawing.Point(210, 274);
            this.picCanvas4.Margin = new System.Windows.Forms.Padding(0);
            this.picCanvas4.Name = "picCanvas4";
            this.picCanvas4.Size = new System.Drawing.Size(194, 189);
            this.picCanvas4.TabIndex = 137;
            this.picCanvas4.TabStop = false;
            // 
            // picCanvas3
            // 
            this.picCanvas3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.picCanvas3.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picCanvas3.Location = new System.Drawing.Point(7, 274);
            this.picCanvas3.Margin = new System.Windows.Forms.Padding(0);
            this.picCanvas3.Name = "picCanvas3";
            this.picCanvas3.Size = new System.Drawing.Size(197, 188);
            this.picCanvas3.TabIndex = 136;
            this.picCanvas3.TabStop = false;
            // 
            // picCanvas2
            // 
            this.picCanvas2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.picCanvas2.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picCanvas2.Location = new System.Drawing.Point(210, 50);
            this.picCanvas2.Margin = new System.Windows.Forms.Padding(0);
            this.picCanvas2.Name = "picCanvas2";
            this.picCanvas2.Size = new System.Drawing.Size(196, 186);
            this.picCanvas2.TabIndex = 135;
            this.picCanvas2.TabStop = false;
            // 
            // picCanvas1
            // 
            this.picCanvas1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.picCanvas1.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.picCanvas1.Location = new System.Drawing.Point(8, 50);
            this.picCanvas1.Margin = new System.Windows.Forms.Padding(0);
            this.picCanvas1.Name = "picCanvas1";
            this.picCanvas1.Size = new System.Drawing.Size(197, 186);
            this.picCanvas1.TabIndex = 134;
            this.picCanvas1.TabStop = false;
            // 
            // txtNetwork
            // 
            this.txtNetwork.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtNetwork, 2);
            this.txtNetwork.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNetwork.Location = new System.Drawing.Point(678, 652);
            this.txtNetwork.Margin = new System.Windows.Forms.Padding(4);
            this.txtNetwork.Name = "txtNetwork";
            this.txtNetwork.Size = new System.Drawing.Size(551, 42);
            this.txtNetwork.TabIndex = 522;
            // 
            // txtCategory
            // 
            this.txtCategory.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtCategory, 2);
            this.txtCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCategory.Location = new System.Drawing.Point(678, 602);
            this.txtCategory.Margin = new System.Windows.Forms.Padding(4);
            this.txtCategory.Name = "txtCategory";
            this.txtCategory.Size = new System.Drawing.Size(551, 42);
            this.txtCategory.TabIndex = 521;
            // 
            // txtType
            // 
            this.txtType.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtType, 2);
            this.txtType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtType.Location = new System.Drawing.Point(678, 552);
            this.txtType.Margin = new System.Windows.Forms.Padding(4);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(551, 42);
            this.txtType.TabIndex = 520;
            // 
            // txtReligion
            // 
            this.txtReligion.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtReligion, 2);
            this.txtReligion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtReligion.Location = new System.Drawing.Point(231, 652);
            this.txtReligion.Margin = new System.Windows.Forms.Padding(4);
            this.txtReligion.Name = "txtReligion";
            this.txtReligion.Size = new System.Drawing.Size(242, 42);
            this.txtReligion.TabIndex = 519;
            // 
            // txtGender
            // 
            this.txtGender.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtGender, 2);
            this.txtGender.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGender.Location = new System.Drawing.Point(231, 602);
            this.txtGender.Margin = new System.Windows.Forms.Padding(4);
            this.txtGender.Name = "txtGender";
            this.txtGender.Size = new System.Drawing.Size(242, 42);
            this.txtGender.TabIndex = 518;
            // 
            // txtIdentity
            // 
            this.txtIdentity.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtIdentity, 2);
            this.txtIdentity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIdentity.Location = new System.Drawing.Point(231, 552);
            this.txtIdentity.Margin = new System.Windows.Forms.Padding(4);
            this.txtIdentity.Name = "txtIdentity";
            this.txtIdentity.Size = new System.Drawing.Size(242, 42);
            this.txtIdentity.TabIndex = 517;
            // 
            // txtFullname
            // 
            this.txtFullname.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtFullname, 2);
            this.txtFullname.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFullname.Location = new System.Drawing.Point(231, 502);
            this.txtFullname.Margin = new System.Windows.Forms.Padding(4);
            this.txtFullname.Name = "txtFullname";
            this.txtFullname.Size = new System.Drawing.Size(242, 42);
            this.txtFullname.TabIndex = 516;
            // 
            // radLabel12
            // 
            this.radLabel12.AutoSize = false;
            this.radLabel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel12.Location = new System.Drawing.Point(481, 652);
            this.radLabel12.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(189, 42);
            this.radLabel12.TabIndex = 515;
            this.radLabel12.Text = "JARINGAN";
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel7.Location = new System.Drawing.Point(481, 602);
            this.radLabel7.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(189, 42);
            this.radLabel7.TabIndex = 514;
            this.radLabel7.Text = "KATEGORI";
            // 
            // radLabel16
            // 
            this.radLabel16.AutoSize = false;
            this.radLabel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel16.Location = new System.Drawing.Point(481, 552);
            this.radLabel16.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(189, 42);
            this.radLabel16.TabIndex = 513;
            this.radLabel16.Text = "TIPE";
            // 
            // radLabel15
            // 
            this.radLabel15.AutoSize = false;
            this.radLabel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel15.Location = new System.Drawing.Point(14, 652);
            this.radLabel15.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(209, 42);
            this.radLabel15.TabIndex = 512;
            this.radLabel15.Text = "AGAMA";
            // 
            // radLabel11
            // 
            this.radLabel11.AutoSize = false;
            this.radLabel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel11.Location = new System.Drawing.Point(14, 602);
            this.radLabel11.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(209, 42);
            this.radLabel11.TabIndex = 511;
            this.radLabel11.Text = "JENIS KELAMIN";
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel9.Location = new System.Drawing.Point(14, 502);
            this.radLabel9.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(209, 42);
            this.radLabel9.TabIndex = 509;
            this.radLabel9.Text = "NAMA LENGKAP";
            // 
            // txtTotalInused
            // 
            this.txtTotalInused.AutoSize = false;
            this.txtTotalInused.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTotalInused.Location = new System.Drawing.Point(231, 214);
            this.txtTotalInused.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalInused.Name = "txtTotalInused";
            this.txtTotalInused.Size = new System.Drawing.Size(86, 52);
            this.txtTotalInused.TabIndex = 508;
            // 
            // txtTotalFloor
            // 
            this.txtTotalFloor.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtTotalFloor, 2);
            this.txtTotalFloor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTotalFloor.Location = new System.Drawing.Point(231, 164);
            this.txtTotalFloor.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalFloor.Name = "txtTotalFloor";
            this.txtTotalFloor.Size = new System.Drawing.Size(242, 42);
            this.txtTotalFloor.TabIndex = 506;
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel4.Location = new System.Drawing.Point(14, 214);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(209, 52);
            this.radLabel4.TabIndex = 505;
            this.radLabel4.Text = "JUMLAH ORANG / NAMA SEKAMAR";
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel2.Location = new System.Drawing.Point(14, 164);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(209, 42);
            this.radLabel2.TabIndex = 503;
            this.radLabel2.Text = "LANTAI";
            // 
            // txtAllocationStatus
            // 
            this.txtAllocationStatus.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtAllocationStatus, 2);
            this.txtAllocationStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAllocationStatus.Location = new System.Drawing.Point(231, 324);
            this.txtAllocationStatus.Margin = new System.Windows.Forms.Padding(4);
            this.txtAllocationStatus.Name = "txtAllocationStatus";
            this.txtAllocationStatus.Size = new System.Drawing.Size(242, 42);
            this.txtAllocationStatus.TabIndex = 146;
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel1.Location = new System.Drawing.Point(14, 324);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(209, 42);
            this.radLabel1.TabIndex = 145;
            this.radLabel1.Text = "STATUS";
            // 
            // txtDateEnter
            // 
            this.txtDateEnter.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtDateEnter, 3);
            this.txtDateEnter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateEnter.Location = new System.Drawing.Point(231, 274);
            this.txtDateEnter.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateEnter.Name = "txtDateEnter";
            this.txtDateEnter.Size = new System.Drawing.Size(439, 42);
            this.txtDateEnter.TabIndex = 59;
            // 
            // txtBuildName
            // 
            this.txtBuildName.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.txtBuildName, 2);
            this.txtBuildName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBuildName.Location = new System.Drawing.Point(231, 114);
            this.txtBuildName.Margin = new System.Windows.Forms.Padding(4);
            this.txtBuildName.Name = "txtBuildName";
            this.txtBuildName.Size = new System.Drawing.Size(242, 42);
            this.txtBuildName.TabIndex = 58;
            // 
            // txtCellCode
            // 
            this.txtCellCode.AutoSize = false;
            this.txtCellCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCellCode.Location = new System.Drawing.Point(231, 64);
            this.txtCellCode.Margin = new System.Windows.Forms.Padding(4);
            this.txtCellCode.Name = "txtCellCode";
            this.txtCellCode.Size = new System.Drawing.Size(86, 42);
            this.txtCellCode.TabIndex = 57;
            // 
            // lblJenisKelamin
            // 
            this.lblJenisKelamin.AutoSize = false;
            this.lblJenisKelamin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblJenisKelamin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblJenisKelamin.Location = new System.Drawing.Point(14, 274);
            this.lblJenisKelamin.Margin = new System.Windows.Forms.Padding(4);
            this.lblJenisKelamin.Name = "lblJenisKelamin";
            this.lblJenisKelamin.Size = new System.Drawing.Size(209, 42);
            this.lblJenisKelamin.TabIndex = 18;
            this.lblJenisKelamin.Text = "TANGGAL MASUK";
            // 
            // lblNama
            // 
            this.lblNama.AutoSize = false;
            this.lblNama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNama.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNama.Location = new System.Drawing.Point(14, 64);
            this.lblNama.Margin = new System.Windows.Forms.Padding(4);
            this.lblNama.Name = "lblNama";
            this.lblNama.Size = new System.Drawing.Size(209, 42);
            this.lblNama.TabIndex = 10;
            this.lblNama.Text = "KODE SEL";
            // 
            // lblTglLahir
            // 
            this.lblTglLahir.AutoSize = false;
            this.lblTglLahir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTglLahir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTglLahir.Location = new System.Drawing.Point(14, 114);
            this.lblTglLahir.Margin = new System.Windows.Forms.Padding(4);
            this.lblTglLahir.Name = "lblTglLahir";
            this.lblTglLahir.Size = new System.Drawing.Size(209, 42);
            this.lblTglLahir.TabIndex = 19;
            this.lblTglLahir.Text = "NAMA GEDUNG";
            // 
            // lblDataUser
            // 
            this.lblDataUser.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDataUser, 3);
            this.lblDataUser.Cursor = System.Windows.Forms.Cursors.No;
            this.lblDataUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDataUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDataUser.Image = ((System.Drawing.Image)(resources.GetObject("lblDataUser.Image")));
            this.lblDataUser.Location = new System.Drawing.Point(14, 14);
            this.lblDataUser.Margin = new System.Windows.Forms.Padding(4);
            this.lblDataUser.Name = "lblDataUser";
            this.lblDataUser.Size = new System.Drawing.Size(459, 42);
            this.lblDataUser.TabIndex = 62;
            this.lblDataUser.Text = "         INFORMASI SEL";
            // 
            // lblDetailData
            // 
            this.lblDetailData.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailData, 2);
            this.lblDetailData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDetailData.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.tahanan_color;
            this.lblDetailData.Location = new System.Drawing.Point(14, 452);
            this.lblDetailData.Margin = new System.Windows.Forms.Padding(4);
            this.lblDetailData.Name = "lblDetailData";
            this.lblDetailData.Size = new System.Drawing.Size(303, 42);
            this.lblDetailData.TabIndex = 71;
            this.lblDetailData.Text = "        INFORMASI TAHANAN";
            // 
            // radLabel10
            // 
            this.radLabel10.AutoSize = false;
            this.radLabel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel10.Location = new System.Drawing.Point(14, 552);
            this.radLabel10.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(209, 42);
            this.radLabel10.TabIndex = 510;
            this.radLabel10.Text = "JENIS IDENTITAS/NO";
            // 
            // txtTypeCell
            // 
            this.txtTypeCell.AutoSize = false;
            this.txtTypeCell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTypeCell.Enabled = false;
            this.txtTypeCell.Location = new System.Drawing.Point(324, 63);
            this.txtTypeCell.Name = "txtTypeCell";
            this.txtTypeCell.Size = new System.Drawing.Size(150, 44);
            this.txtTypeCell.TabIndex = 523;
            this.txtTypeCell.Text = "radCheckBox1";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1262, 56);
            this.lblTitle.TabIndex = 8;
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(1197, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 56);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 1;
            this.exit.TabStop = false;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.lblButton.Controls.Add(this.btnClose);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 772);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(1262, 122);
            this.lblButton.TabIndex = 67;
            // 
            // btnClose
            // 
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(30, 30);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(244, 62);
            this.btnClose.TabIndex = 10;
            this.btnClose.Tag = "";
            this.btnClose.Text = "&TUTUP";
            this.btnClose.ThemeName = "MaterialBlueGrey";
            // 
            // fullpicPanel
            // 
            this.fullpicPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(65)))));
            this.fullpicPanel.Controls.Add(this.closefull);
            this.fullpicPanel.Controls.Add(this.fullPic);
            this.fullpicPanel.Location = new System.Drawing.Point(32500, 127);
            this.fullpicPanel.Name = "fullpicPanel";
            this.fullpicPanel.Padding = new System.Windows.Forms.Padding(5);
            this.fullpicPanel.Size = new System.Drawing.Size(603, 641);
            this.fullpicPanel.TabIndex = 85;
            this.fullpicPanel.Visible = false;
            // 
            // closefull
            // 
            this.closefull.BackColor = System.Drawing.Color.Transparent;
            this.closefull.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.closefull.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closefull.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.delete1;
            this.closefull.Location = new System.Drawing.Point(237, 5);
            this.closefull.Name = "closefull";
            this.closefull.Size = new System.Drawing.Size(44, 45);
            this.closefull.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.closefull.TabIndex = 4;
            this.closefull.TabStop = false;
            // 
            // fullPic
            // 
            this.fullPic.BackColor = System.Drawing.Color.White;
            this.fullPic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fullPic.Location = new System.Drawing.Point(5, 5);
            this.fullPic.Margin = new System.Windows.Forms.Padding(10);
            this.fullPic.Name = "fullPic";
            this.fullPic.Size = new System.Drawing.Size(593, 631);
            this.fullPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fullPic.TabIndex = 0;
            this.fullPic.TabStop = false;
            // 
            // lblDetailVisit
            // 
            this.lblDetailVisit.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailVisit, 6);
            this.lblDetailVisit.Controls.Add(this.pdf);
            this.lblDetailVisit.Controls.Add(this.excel);
            this.lblDetailVisit.Cursor = System.Windows.Forms.Cursors.No;
            this.lblDetailVisit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailVisit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDetailVisit.Image = ((System.Drawing.Image)(resources.GetObject("lblDetailVisit.Image")));
            this.lblDetailVisit.Location = new System.Drawing.Point(14, 852);
            this.lblDetailVisit.Margin = new System.Windows.Forms.Padding(4);
            this.lblDetailVisit.Name = "lblDetailVisit";
            this.lblDetailVisit.Size = new System.Drawing.Size(1215, 42);
            this.lblDetailVisit.TabIndex = 534;
            this.lblDetailVisit.Text = "         REKAP ALOKASI SEL";
            // 
            // pdf
            // 
            this.pdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pdf.Dock = System.Windows.Forms.DockStyle.Right;
            this.pdf.Image = ((System.Drawing.Image)(resources.GetObject("pdf.Image")));
            this.pdf.Location = new System.Drawing.Point(1129, 0);
            this.pdf.Name = "pdf";
            this.pdf.Size = new System.Drawing.Size(43, 42);
            this.pdf.TabIndex = 7;
            this.pdf.TabStop = false;
            // 
            // excel
            // 
            this.excel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.excel.Dock = System.Windows.Forms.DockStyle.Right;
            this.excel.Image = ((System.Drawing.Image)(resources.GetObject("excel.Image")));
            this.excel.Location = new System.Drawing.Point(1172, 0);
            this.excel.Name = "excel";
            this.excel.Size = new System.Drawing.Size(43, 42);
            this.excel.TabIndex = 6;
            this.excel.TabStop = false;
            // 
            // gvHistory
            // 
            this.gvHistory.AutoScroll = true;
            this.tableLayoutPanel1.SetColumnSpan(this.gvHistory, 6);
            this.gvHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvHistory.Location = new System.Drawing.Point(13, 901);
            // 
            // 
            // 
            this.gvHistory.MasterTemplate.AllowAddNewRow = false;
            this.gvHistory.MasterTemplate.AllowCellContextMenu = false;
            this.gvHistory.MasterTemplate.AllowColumnChooser = false;
            this.gvHistory.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.gvHistory.MasterTemplate.AllowColumnReorder = false;
            this.gvHistory.MasterTemplate.AllowDragToGroup = false;
            this.gvHistory.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gvHistory.Name = "gvHistory";
            this.gvHistory.Size = new System.Drawing.Size(1217, 444);
            this.gvHistory.TabIndex = 535;
            // 
            // CellAllocationDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.fullpicPanel);
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblTitle);
            this.Name = "CellAllocationDetail";
            this.Size = new System.Drawing.Size(1262, 894);
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtRoommate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanggalPenangkapan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTanggalKejadian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLokasiPenangkapan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeran)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblphoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblrightthumb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblleft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblright)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblfront)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNetwork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReligion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdentity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalInused)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalFloor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAllocationStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateEnter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBuildName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCellCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJenisKelamin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTglLahir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTypeCell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullpicPanel)).EndInit();
            this.fullpicPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.closefull)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisit)).EndInit();
            this.lblDetailVisit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvHistory.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvHistory)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel universitas;
        private System.Windows.Forms.PictureBox exit;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel txtDateEnter;
        private Telerik.WinControls.UI.RadLabel txtBuildName;
        private Telerik.WinControls.UI.RadLabel txtCellCode;
        private Telerik.WinControls.UI.RadLabel lblJenisKelamin;
        private Telerik.WinControls.UI.RadLabel lblNama;
        private Telerik.WinControls.UI.RadLabel lblTglLahir;
        private Telerik.WinControls.UI.RadLabel lblDataUser;
        private Telerik.WinControls.UI.RadLabel lblDetailData;
        private Telerik.WinControls.UI.RadLabel txtAllocationStatus;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel txtTotalInused;
        private Telerik.WinControls.UI.RadLabel txtTotalFloor;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel txtNetwork;
        private Telerik.WinControls.UI.RadLabel txtCategory;
        private Telerik.WinControls.UI.RadLabel txtType;
        private Telerik.WinControls.UI.RadLabel txtReligion;
        private Telerik.WinControls.UI.RadLabel txtGender;
        private Telerik.WinControls.UI.RadLabel txtIdentity;
        private Telerik.WinControls.UI.RadLabel txtFullname;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnClose;
        private Telerik.WinControls.UI.RadPanel fullpicPanel;
        private System.Windows.Forms.PictureBox fullPic;
        private Telerik.WinControls.UI.RadCheckBox txtTypeCell;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadLabel lblphoto;
        private Telerik.WinControls.UI.RadLabel lblrightthumb;
        private Telerik.WinControls.UI.RadLabel lblleft;
        private Telerik.WinControls.UI.RadLabel lblright;
        private Telerik.WinControls.UI.RadLabel lblfront;
        private Telerik.WinControls.UI.RadLabel lblRegID;
        private System.Windows.Forms.PictureBox picCanvas4;
        private System.Windows.Forms.PictureBox picCanvas3;
        private System.Windows.Forms.PictureBox picCanvas2;
        private System.Windows.Forms.PictureBox picCanvas1;
        private System.Windows.Forms.PictureBox closefull;
        private Telerik.WinControls.UI.RadLabel lblTanggalPenangkapan;
        private Telerik.WinControls.UI.RadLabel lblTanggalKejadian;
        private Telerik.WinControls.UI.RadLabel lblLokasiPenangkapan;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel txtPeran;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel txtRoommate;
        private Telerik.WinControls.UI.RadLabel lblDetailVisit;
        private System.Windows.Forms.PictureBox pdf;
        private System.Windows.Forms.PictureBox excel;
        private Telerik.WinControls.UI.RadGridView gvHistory;
    }
}
