﻿namespace RDM.LP.Desktop.Presentation.CustomControls
{
    partial class VisitorDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisitorDetail));
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.universitas = new Telerik.WinControls.UI.RadLabel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.PDOfBirth = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.MaritalStatus = new Telerik.WinControls.UI.RadLabel();
            this.Vocation = new Telerik.WinControls.UI.RadLabel();
            this.Nationality = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.lblInmatePhoto = new Telerik.WinControls.UI.RadLabel();
            this.regID = new Telerik.WinControls.UI.RadLabel();
            this.lblRegID = new Telerik.WinControls.UI.RadLabel();
            this.namalengkap = new Telerik.WinControls.UI.RadLabel();
            this.lblNama = new Telerik.WinControls.UI.RadLabel();
            this.lblDataVisitor = new Telerik.WinControls.UI.RadLabel();
            this.panelPic = new Telerik.WinControls.UI.RadPanel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.picCanvas = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tot = new Telerik.WinControls.UI.RadLabel();
            this.next = new System.Windows.Forms.PictureBox();
            this.prev = new System.Windows.Forms.PictureBox();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.picFinger = new System.Windows.Forms.PictureBox();
            this.lblVisitorFinger = new Telerik.WinControls.UI.RadLabel();
            this.gvHistory = new Telerik.WinControls.UI.RadGridView();
            this.lblDetailVisit = new Telerik.WinControls.UI.RadLabel();
            this.pdf = new System.Windows.Forms.PictureBox();
            this.excel = new System.Windows.Forms.PictureBox();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.updatedate = new Telerik.WinControls.UI.RadLabel();
            this.updater = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.inputdate = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.inputter = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.lblDetailData = new Telerik.WinControls.UI.RadLabel();
            this.lblReligon = new Telerik.WinControls.UI.RadLabel();
            this.religion = new Telerik.WinControls.UI.RadLabel();
            this.telp = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.city = new Telerik.WinControls.UI.RadLabel();
            this.lblUniversitas = new Telerik.WinControls.UI.RadLabel();
            this.address = new Telerik.WinControls.UI.RadLabel();
            this.lblPendidikan = new Telerik.WinControls.UI.RadLabel();
            this.noidentitas = new Telerik.WinControls.UI.RadLabel();
            this.lblEmail = new Telerik.WinControls.UI.RadLabel();
            this.jenisidentitas = new Telerik.WinControls.UI.RadLabel();
            this.lblTelp = new Telerik.WinControls.UI.RadLabel();
            this.jeniskelamin = new Telerik.WinControls.UI.RadLabel();
            this.lblAgama = new Telerik.WinControls.UI.RadLabel();
            this.fullpicPanel = new Telerik.WinControls.UI.RadPanel();
            this.closefull = new System.Windows.Forms.PictureBox();
            this.fullPic = new System.Windows.Forms.PictureBox();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.exit = new System.Windows.Forms.PictureBox();
            this.lblButton = new Telerik.WinControls.UI.RadLabel();
            this.btnClose = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PDOfBirth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaritalStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nationality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInmatePhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.regID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.namalengkap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataVisitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelPic)).BeginInit();
            this.panelPic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.next)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVisitorFinger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvHistory.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisit)).BeginInit();
            this.lblDetailVisit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReligon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.religion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.city)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUniversitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.address)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPendidikan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.noidentitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jenisidentitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jeniskelamin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAgama)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullpicPanel)).BeginInit();
            this.fullpicPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closefull)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            this.lblTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).BeginInit();
            this.lblButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            this.SuspendLayout();
            // 
            // universitas
            // 
            this.universitas.AutoSize = false;
            this.universitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.universitas.Location = new System.Drawing.Point(504, 414);
            this.universitas.Margin = new System.Windows.Forms.Padding(4);
            this.universitas.Name = "universitas";
            this.universitas.Size = new System.Drawing.Size(728, 42);
            this.universitas.TabIndex = 53;
            this.universitas.Text = "Tahun Lulus";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 56);
            this.radScrollablePanel1.Margin = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.tableLayoutPanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(1086, 600);
            this.radScrollablePanel1.Size = new System.Drawing.Size(1105, 602);
            this.radScrollablePanel1.TabIndex = 15;
            this.radScrollablePanel1.VerticalScrollBarState = Telerik.WinControls.UI.ScrollState.AlwaysShow;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel1.Controls.Add(this.PDOfBirth, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.radLabel9, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.MaritalStatus, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.Vocation, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.Nationality, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.radLabel4, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.radLabel3, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.radLabel1, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.lblInmatePhoto, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.regID, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblRegID, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.namalengkap, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblNama, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblDataVisitor, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panelPic, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.radPanel2, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.lblVisitorFinger, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.gvHistory, 0, 22);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailVisit, 0, 21);
            this.tableLayoutPanel1.Controls.Add(this.radLabel14, 0, 19);
            this.tableLayoutPanel1.Controls.Add(this.updatedate, 1, 19);
            this.tableLayoutPanel1.Controls.Add(this.updater, 1, 18);
            this.tableLayoutPanel1.Controls.Add(this.radLabel13, 0, 18);
            this.tableLayoutPanel1.Controls.Add(this.inputdate, 1, 17);
            this.tableLayoutPanel1.Controls.Add(this.radLabel12, 0, 17);
            this.tableLayoutPanel1.Controls.Add(this.inputter, 1, 16);
            this.tableLayoutPanel1.Controls.Add(this.radLabel7, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.lblDetailData, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.lblReligon, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.religion, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.telp, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.radLabel2, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.city, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblUniversitas, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.address, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblPendidikan, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.noidentitas, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblEmail, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.jenisidentitas, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblTelp, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.jeniskelamin, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblAgama, 0, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10);
            this.tableLayoutPanel1.RowCount = 24;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 450F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1086, 1600);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // PDOfBirth
            // 
            this.PDOfBirth.AutoSize = false;
            this.PDOfBirth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PDOfBirth.Location = new System.Drawing.Point(314, 164);
            this.PDOfBirth.Margin = new System.Windows.Forms.Padding(4);
            this.PDOfBirth.Name = "PDOfBirth";
            this.PDOfBirth.Size = new System.Drawing.Size(408, 42);
            this.PDOfBirth.TabIndex = 62;
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel9.Location = new System.Drawing.Point(14, 164);
            this.radLabel9.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(292, 42);
            this.radLabel9.TabIndex = 37;
            this.radLabel9.Text = "Tempat, Tanggal Lahir";
            // 
            // MaritalStatus
            // 
            this.MaritalStatus.AutoSize = false;
            this.MaritalStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MaritalStatus.Location = new System.Drawing.Point(314, 564);
            this.MaritalStatus.Margin = new System.Windows.Forms.Padding(4);
            this.MaritalStatus.Name = "MaritalStatus";
            this.MaritalStatus.Size = new System.Drawing.Size(408, 42);
            this.MaritalStatus.TabIndex = 49;
            // 
            // Vocation
            // 
            this.Vocation.AutoSize = false;
            this.Vocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Vocation.Location = new System.Drawing.Point(314, 614);
            this.Vocation.Margin = new System.Windows.Forms.Padding(4);
            this.Vocation.Name = "Vocation";
            this.Vocation.Size = new System.Drawing.Size(408, 42);
            this.Vocation.TabIndex = 49;
            // 
            // Nationality
            // 
            this.Nationality.AutoSize = false;
            this.Nationality.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Nationality.Location = new System.Drawing.Point(314, 664);
            this.Nationality.Margin = new System.Windows.Forms.Padding(4);
            this.Nationality.Name = "Nationality";
            this.Nationality.Size = new System.Drawing.Size(408, 42);
            this.Nationality.TabIndex = 49;
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel4.Location = new System.Drawing.Point(14, 564);
            this.radLabel4.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(292, 42);
            this.radLabel4.TabIndex = 40;
            this.radLabel4.Text = "Status Pernikahan";
            // 
            // radLabel3
            // 
            this.radLabel3.AutoSize = false;
            this.radLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel3.Location = new System.Drawing.Point(14, 614);
            this.radLabel3.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(292, 42);
            this.radLabel3.TabIndex = 40;
            this.radLabel3.Text = "Pekerjaan";
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel1.Location = new System.Drawing.Point(14, 664);
            this.radLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(292, 42);
            this.radLabel1.TabIndex = 40;
            this.radLabel1.Text = "Kewarganegaraan";
            // 
            // lblInmatePhoto
            // 
            this.lblInmatePhoto.AutoSize = false;
            this.lblInmatePhoto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.lblInmatePhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInmatePhoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.lblInmatePhoto.ForeColor = System.Drawing.Color.White;
            this.lblInmatePhoto.Image = ((System.Drawing.Image)(resources.GetObject("lblInmatePhoto.Image")));
            this.lblInmatePhoto.Location = new System.Drawing.Point(731, 15);
            this.lblInmatePhoto.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblInmatePhoto.Name = "lblInmatePhoto";
            this.lblInmatePhoto.Padding = new System.Windows.Forms.Padding(8);
            this.lblInmatePhoto.Size = new System.Drawing.Size(340, 45);
            this.lblInmatePhoto.TabIndex = 180;
            this.lblInmatePhoto.Text = "         FOTO PENGUNJUNG";
            // 
            // regID
            // 
            this.regID.AutoSize = false;
            this.regID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.regID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.regID.Location = new System.Drawing.Point(314, 64);
            this.regID.Margin = new System.Windows.Forms.Padding(4);
            this.regID.Name = "regID";
            this.regID.Size = new System.Drawing.Size(408, 42);
            this.regID.TabIndex = 82;
            // 
            // lblRegID
            // 
            this.lblRegID.AutoSize = false;
            this.lblRegID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRegID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblRegID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblRegID.Location = new System.Drawing.Point(14, 64);
            this.lblRegID.Margin = new System.Windows.Forms.Padding(4);
            this.lblRegID.Name = "lblRegID";
            this.lblRegID.Size = new System.Drawing.Size(292, 42);
            this.lblRegID.TabIndex = 81;
            this.lblRegID.Text = "ID Register ";
            // 
            // namalengkap
            // 
            this.namalengkap.AutoSize = false;
            this.namalengkap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.namalengkap.Location = new System.Drawing.Point(314, 114);
            this.namalengkap.Margin = new System.Windows.Forms.Padding(4);
            this.namalengkap.Name = "namalengkap";
            this.namalengkap.Size = new System.Drawing.Size(408, 42);
            this.namalengkap.TabIndex = 57;
            // 
            // lblNama
            // 
            this.lblNama.AutoSize = false;
            this.lblNama.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblNama.Location = new System.Drawing.Point(14, 114);
            this.lblNama.Margin = new System.Windows.Forms.Padding(4);
            this.lblNama.Name = "lblNama";
            this.lblNama.Size = new System.Drawing.Size(292, 42);
            this.lblNama.TabIndex = 10;
            this.lblNama.Text = "Nama Lengkap";
            // 
            // lblDataVisitor
            // 
            this.lblDataVisitor.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDataVisitor, 2);
            this.lblDataVisitor.Cursor = System.Windows.Forms.Cursors.No;
            this.lblDataVisitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDataVisitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDataVisitor.Image = ((System.Drawing.Image)(resources.GetObject("lblDataVisitor.Image")));
            this.lblDataVisitor.Location = new System.Drawing.Point(14, 14);
            this.lblDataVisitor.Margin = new System.Windows.Forms.Padding(4);
            this.lblDataVisitor.Name = "lblDataVisitor";
            this.lblDataVisitor.Size = new System.Drawing.Size(708, 42);
            this.lblDataVisitor.TabIndex = 62;
            this.lblDataVisitor.Text = "       DETAIL PENGUNJUNG";
            // 
            // panelPic
            // 
            this.panelPic.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelPic.Controls.Add(this.radPanel1);
            this.panelPic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPic.Location = new System.Drawing.Point(729, 63);
            this.panelPic.Name = "panelPic";
            this.tableLayoutPanel1.SetRowSpan(this.panelPic, 7);
            this.panelPic.Size = new System.Drawing.Size(344, 344);
            this.panelPic.TabIndex = 84;
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.radPanel1.Controls.Add(this.picCanvas);
            this.radPanel1.Controls.Add(this.tableLayoutPanel2);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.radPanel1.Size = new System.Drawing.Size(344, 344);
            this.radPanel1.TabIndex = 85;
            // 
            // picCanvas
            // 
            this.picCanvas.BackColor = System.Drawing.Color.DimGray;
            this.picCanvas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picCanvas.Cursor = System.Windows.Forms.Cursors.NoMove2D;
            this.picCanvas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picCanvas.Location = new System.Drawing.Point(5, 5);
            this.picCanvas.Margin = new System.Windows.Forms.Padding(0);
            this.picCanvas.Name = "picCanvas";
            this.picCanvas.Size = new System.Drawing.Size(334, 286);
            this.picCanvas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCanvas.TabIndex = 87;
            this.picCanvas.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel2.Controls.Add(this.tot, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.next, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.prev, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(5, 291);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(334, 48);
            this.tableLayoutPanel2.TabIndex = 86;
            // 
            // tot
            // 
            this.tot.AutoSize = false;
            this.tot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tot.ForeColor = System.Drawing.Color.White;
            this.tot.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.tot.Location = new System.Drawing.Point(68, 3);
            this.tot.Name = "tot";
            this.tot.Size = new System.Drawing.Size(198, 42);
            this.tot.TabIndex = 4;
            this.tot.Text = "0/0";
            this.tot.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // next
            // 
            this.next.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("next.BackgroundImage")));
            this.next.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.next.Cursor = System.Windows.Forms.Cursors.Hand;
            this.next.Dock = System.Windows.Forms.DockStyle.Fill;
            this.next.Location = new System.Drawing.Point(272, 3);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(59, 42);
            this.next.TabIndex = 1;
            this.next.TabStop = false;
            // 
            // prev
            // 
            this.prev.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("prev.BackgroundImage")));
            this.prev.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.prev.Cursor = System.Windows.Forms.Cursors.Hand;
            this.prev.Dock = System.Windows.Forms.DockStyle.Fill;
            this.prev.Location = new System.Drawing.Point(3, 3);
            this.prev.Name = "prev";
            this.prev.Size = new System.Drawing.Size(59, 42);
            this.prev.TabIndex = 0;
            this.prev.TabStop = false;
            // 
            // radPanel2
            // 
            this.radPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.radPanel2.Controls.Add(this.picFinger);
            this.radPanel2.Location = new System.Drawing.Point(731, 460);
            this.radPanel2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Padding = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel1.SetRowSpan(this.radPanel2, 7);
            this.radPanel2.Size = new System.Drawing.Size(340, 345);
            this.radPanel2.TabIndex = 86;
            // 
            // picFinger
            // 
            this.picFinger.BackColor = System.Drawing.Color.DimGray;
            this.picFinger.Cursor = System.Windows.Forms.Cursors.NoMove2D;
            this.picFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picFinger.Location = new System.Drawing.Point(6, 6);
            this.picFinger.Margin = new System.Windows.Forms.Padding(0);
            this.picFinger.Name = "picFinger";
            this.picFinger.Size = new System.Drawing.Size(328, 333);
            this.picFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFinger.TabIndex = 87;
            this.picFinger.TabStop = false;
            // 
            // lblVisitorFinger
            // 
            this.lblVisitorFinger.AutoSize = false;
            this.lblVisitorFinger.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.lblVisitorFinger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblVisitorFinger.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Bold);
            this.lblVisitorFinger.ForeColor = System.Drawing.Color.White;
            this.lblVisitorFinger.Image = ((System.Drawing.Image)(resources.GetObject("lblVisitorFinger.Image")));
            this.lblVisitorFinger.Location = new System.Drawing.Point(731, 415);
            this.lblVisitorFinger.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblVisitorFinger.Name = "lblVisitorFinger";
            this.lblVisitorFinger.Padding = new System.Windows.Forms.Padding(8);
            this.lblVisitorFinger.Size = new System.Drawing.Size(340, 45);
            this.lblVisitorFinger.TabIndex = 184;
            this.lblVisitorFinger.Text = "         SIDIK JARI PENGUNJUNG";
            // 
            // gvHistory
            // 
            this.gvHistory.AutoScroll = true;
            this.tableLayoutPanel1.SetColumnSpan(this.gvHistory, 3);
            this.gvHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvHistory.Location = new System.Drawing.Point(13, 1113);
            // 
            // 
            // 
            this.gvHistory.MasterTemplate.AllowAddNewRow = false;
            this.gvHistory.MasterTemplate.AllowCellContextMenu = false;
            this.gvHistory.MasterTemplate.AllowColumnChooser = false;
            this.gvHistory.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.gvHistory.MasterTemplate.AllowColumnReorder = false;
            this.gvHistory.MasterTemplate.AllowDragToGroup = false;
            this.gvHistory.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gvHistory.Name = "gvHistory";
            this.gvHistory.Size = new System.Drawing.Size(1060, 444);
            this.gvHistory.TabIndex = 182;
            // 
            // lblDetailVisit
            // 
            this.lblDetailVisit.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailVisit, 3);
            this.lblDetailVisit.Controls.Add(this.pdf);
            this.lblDetailVisit.Controls.Add(this.excel);
            this.lblDetailVisit.Cursor = System.Windows.Forms.Cursors.No;
            this.lblDetailVisit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailVisit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDetailVisit.Image = ((System.Drawing.Image)(resources.GetObject("lblDetailVisit.Image")));
            this.lblDetailVisit.Location = new System.Drawing.Point(14, 1064);
            this.lblDetailVisit.Margin = new System.Windows.Forms.Padding(4);
            this.lblDetailVisit.Name = "lblDetailVisit";
            this.lblDetailVisit.Size = new System.Drawing.Size(1058, 42);
            this.lblDetailVisit.TabIndex = 181;
            this.lblDetailVisit.Text = "         REKAP KUNJUNGAN";
            // 
            // pdf
            // 
            this.pdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pdf.Dock = System.Windows.Forms.DockStyle.Right;
            this.pdf.Image = ((System.Drawing.Image)(resources.GetObject("pdf.Image")));
            this.pdf.Location = new System.Drawing.Point(972, 0);
            this.pdf.Name = "pdf";
            this.pdf.Size = new System.Drawing.Size(43, 42);
            this.pdf.TabIndex = 7;
            this.pdf.TabStop = false;
            // 
            // excel
            // 
            this.excel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.excel.Dock = System.Windows.Forms.DockStyle.Right;
            this.excel.Image = ((System.Drawing.Image)(resources.GetObject("excel.Image")));
            this.excel.Location = new System.Drawing.Point(1015, 0);
            this.excel.Name = "excel";
            this.excel.Size = new System.Drawing.Size(43, 42);
            this.excel.TabIndex = 6;
            this.excel.TabStop = false;
            // 
            // radLabel14
            // 
            this.radLabel14.AutoSize = false;
            this.radLabel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel14.Location = new System.Drawing.Point(14, 964);
            this.radLabel14.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(292, 42);
            this.radLabel14.TabIndex = 74;
            this.radLabel14.Text = "DIUBAH TANGGAL";
            // 
            // updatedate
            // 
            this.updatedate.AutoSize = false;
            this.updatedate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updatedate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updatedate.Location = new System.Drawing.Point(314, 964);
            this.updatedate.Margin = new System.Windows.Forms.Padding(4);
            this.updatedate.Name = "updatedate";
            this.updatedate.Size = new System.Drawing.Size(408, 42);
            this.updatedate.TabIndex = 79;
            // 
            // updater
            // 
            this.updater.AutoSize = false;
            this.updater.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updater.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.updater.Location = new System.Drawing.Point(314, 914);
            this.updater.Margin = new System.Windows.Forms.Padding(4);
            this.updater.Name = "updater";
            this.updater.Size = new System.Drawing.Size(408, 42);
            this.updater.TabIndex = 80;
            // 
            // radLabel13
            // 
            this.radLabel13.AutoSize = false;
            this.radLabel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel13.Location = new System.Drawing.Point(14, 914);
            this.radLabel13.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(292, 42);
            this.radLabel13.TabIndex = 73;
            this.radLabel13.Text = "DIUBAH OLEH";
            // 
            // inputdate
            // 
            this.inputdate.AutoSize = false;
            this.inputdate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputdate.Location = new System.Drawing.Point(314, 864);
            this.inputdate.Margin = new System.Windows.Forms.Padding(4);
            this.inputdate.Name = "inputdate";
            this.inputdate.Size = new System.Drawing.Size(408, 42);
            this.inputdate.TabIndex = 78;
            // 
            // radLabel12
            // 
            this.radLabel12.AutoSize = false;
            this.radLabel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel12.Location = new System.Drawing.Point(14, 864);
            this.radLabel12.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(292, 42);
            this.radLabel12.TabIndex = 72;
            this.radLabel12.Text = "DIBUAT TANGGAL";
            // 
            // inputter
            // 
            this.inputter.AutoSize = false;
            this.inputter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inputter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.inputter.Location = new System.Drawing.Point(314, 814);
            this.inputter.Margin = new System.Windows.Forms.Padding(4);
            this.inputter.Name = "inputter";
            this.inputter.Size = new System.Drawing.Size(408, 42);
            this.inputter.TabIndex = 77;
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel7.Location = new System.Drawing.Point(14, 814);
            this.radLabel7.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(292, 42);
            this.radLabel7.TabIndex = 68;
            this.radLabel7.Text = "DIBUAT OLEH";
            // 
            // lblDetailData
            // 
            this.lblDetailData.AutoSize = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lblDetailData, 2);
            this.lblDetailData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDetailData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblDetailData.Image = ((System.Drawing.Image)(resources.GetObject("lblDetailData.Image")));
            this.lblDetailData.Location = new System.Drawing.Point(14, 764);
            this.lblDetailData.Margin = new System.Windows.Forms.Padding(4);
            this.lblDetailData.Name = "lblDetailData";
            this.lblDetailData.Size = new System.Drawing.Size(708, 42);
            this.lblDetailData.TabIndex = 71;
            this.lblDetailData.Text = "        LOG DATA";
            // 
            // lblReligon
            // 
            this.lblReligon.AutoSize = false;
            this.lblReligon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblReligon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblReligon.Location = new System.Drawing.Point(14, 514);
            this.lblReligon.Margin = new System.Windows.Forms.Padding(4);
            this.lblReligon.Name = "lblReligon";
            this.lblReligon.Size = new System.Drawing.Size(292, 42);
            this.lblReligon.TabIndex = 38;
            this.lblReligon.Text = "Agama";
            // 
            // religion
            // 
            this.religion.AutoSize = false;
            this.religion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.religion.Location = new System.Drawing.Point(314, 514);
            this.religion.Margin = new System.Windows.Forms.Padding(4);
            this.religion.Name = "religion";
            this.religion.Size = new System.Drawing.Size(408, 42);
            this.religion.TabIndex = 47;
            // 
            // telp
            // 
            this.telp.AutoSize = false;
            this.telp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.telp.Location = new System.Drawing.Point(314, 464);
            this.telp.Margin = new System.Windows.Forms.Padding(4);
            this.telp.Name = "telp";
            this.telp.Size = new System.Drawing.Size(408, 42);
            this.telp.TabIndex = 48;
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.radLabel2.Location = new System.Drawing.Point(14, 464);
            this.radLabel2.Margin = new System.Windows.Forms.Padding(4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(292, 42);
            this.radLabel2.TabIndex = 64;
            this.radLabel2.Text = "No Telepon";
            // 
            // city
            // 
            this.city.AutoSize = false;
            this.city.Dock = System.Windows.Forms.DockStyle.Fill;
            this.city.Location = new System.Drawing.Point(314, 414);
            this.city.Margin = new System.Windows.Forms.Padding(4);
            this.city.Name = "city";
            this.city.Size = new System.Drawing.Size(408, 42);
            this.city.TabIndex = 65;
            // 
            // lblUniversitas
            // 
            this.lblUniversitas.AutoSize = false;
            this.lblUniversitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUniversitas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblUniversitas.Location = new System.Drawing.Point(14, 414);
            this.lblUniversitas.Margin = new System.Windows.Forms.Padding(4);
            this.lblUniversitas.Name = "lblUniversitas";
            this.lblUniversitas.Size = new System.Drawing.Size(292, 42);
            this.lblUniversitas.TabIndex = 37;
            this.lblUniversitas.Text = "Kota";
            // 
            // address
            // 
            this.address.AutoSize = false;
            this.address.Dock = System.Windows.Forms.DockStyle.Fill;
            this.address.Location = new System.Drawing.Point(314, 364);
            this.address.Margin = new System.Windows.Forms.Padding(4);
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(408, 42);
            this.address.TabIndex = 54;
            // 
            // lblPendidikan
            // 
            this.lblPendidikan.AutoSize = false;
            this.lblPendidikan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPendidikan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblPendidikan.Location = new System.Drawing.Point(14, 364);
            this.lblPendidikan.Margin = new System.Windows.Forms.Padding(4);
            this.lblPendidikan.Name = "lblPendidikan";
            this.lblPendidikan.Size = new System.Drawing.Size(292, 42);
            this.lblPendidikan.TabIndex = 33;
            this.lblPendidikan.Text = "Alamat";
            // 
            // noidentitas
            // 
            this.noidentitas.AutoSize = false;
            this.noidentitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.noidentitas.Location = new System.Drawing.Point(314, 314);
            this.noidentitas.Margin = new System.Windows.Forms.Padding(4);
            this.noidentitas.Name = "noidentitas";
            this.noidentitas.Size = new System.Drawing.Size(408, 42);
            this.noidentitas.TabIndex = 55;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = false;
            this.lblEmail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblEmail.Location = new System.Drawing.Point(14, 314);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(4);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(292, 42);
            this.lblEmail.TabIndex = 36;
            this.lblEmail.Text = "No Identitas";
            // 
            // jenisidentitas
            // 
            this.jenisidentitas.AutoSize = false;
            this.jenisidentitas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.jenisidentitas.Location = new System.Drawing.Point(314, 264);
            this.jenisidentitas.Margin = new System.Windows.Forms.Padding(4);
            this.jenisidentitas.Name = "jenisidentitas";
            this.jenisidentitas.Size = new System.Drawing.Size(408, 42);
            this.jenisidentitas.TabIndex = 56;
            // 
            // lblTelp
            // 
            this.lblTelp.AutoSize = false;
            this.lblTelp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblTelp.Location = new System.Drawing.Point(14, 264);
            this.lblTelp.Margin = new System.Windows.Forms.Padding(4);
            this.lblTelp.Name = "lblTelp";
            this.lblTelp.Size = new System.Drawing.Size(292, 42);
            this.lblTelp.TabIndex = 34;
            this.lblTelp.Text = "Jenis Identitas";
            // 
            // jeniskelamin
            // 
            this.jeniskelamin.AutoSize = false;
            this.jeniskelamin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.jeniskelamin.Location = new System.Drawing.Point(314, 214);
            this.jeniskelamin.Margin = new System.Windows.Forms.Padding(4);
            this.jeniskelamin.Name = "jeniskelamin";
            this.jeniskelamin.Size = new System.Drawing.Size(408, 42);
            this.jeniskelamin.TabIndex = 60;
            // 
            // lblAgama
            // 
            this.lblAgama.AutoSize = false;
            this.lblAgama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAgama.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F);
            this.lblAgama.Location = new System.Drawing.Point(14, 214);
            this.lblAgama.Margin = new System.Windows.Forms.Padding(4);
            this.lblAgama.Name = "lblAgama";
            this.lblAgama.Size = new System.Drawing.Size(292, 42);
            this.lblAgama.TabIndex = 35;
            this.lblAgama.Text = "Jenis Kelamin";
            // 
            // fullpicPanel
            // 
            this.fullpicPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.fullpicPanel.Controls.Add(this.closefull);
            this.fullpicPanel.Controls.Add(this.fullPic);
            this.fullpicPanel.Location = new System.Drawing.Point(7600, 22);
            this.fullpicPanel.Name = "fullpicPanel";
            this.fullpicPanel.Padding = new System.Windows.Forms.Padding(5);
            this.fullpicPanel.Size = new System.Drawing.Size(603, 641);
            this.fullpicPanel.TabIndex = 84;
            this.fullpicPanel.Visible = false;
            // 
            // closefull
            // 
            this.closefull.BackColor = System.Drawing.Color.Transparent;
            this.closefull.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.closefull.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closefull.Image = global::RDM.LP.Desktop.Presentation.Properties.Resources.delete1;
            this.closefull.Location = new System.Drawing.Point(553, 5);
            this.closefull.Name = "closefull";
            this.closefull.Size = new System.Drawing.Size(45, 45);
            this.closefull.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.closefull.TabIndex = 3;
            this.closefull.TabStop = false;
            // 
            // fullPic
            // 
            this.fullPic.BackColor = System.Drawing.Color.White;
            this.fullPic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fullPic.Location = new System.Drawing.Point(5, 5);
            this.fullPic.Name = "fullPic";
            this.fullPic.Size = new System.Drawing.Size(593, 631);
            this.fullPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fullPic.TabIndex = 0;
            this.fullPic.TabStop = false;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.Controls.Add(this.exit);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Image = ((System.Drawing.Image)(resources.GetObject("lblTitle.Image")));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(10, 0, 5, 0);
            this.lblTitle.Size = new System.Drawing.Size(1105, 56);
            this.lblTitle.TabIndex = 8;
            // 
            // exit
            // 
            this.exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exit.Dock = System.Windows.Forms.DockStyle.Right;
            this.exit.Image = ((System.Drawing.Image)(resources.GetObject("exit.Image")));
            this.exit.Location = new System.Drawing.Point(1040, 0);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(60, 56);
            this.exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.exit.TabIndex = 1;
            this.exit.TabStop = false;
            // 
            // lblButton
            // 
            this.lblButton.AutoSize = false;
            this.lblButton.Controls.Add(this.btnClose);
            this.lblButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblButton.Location = new System.Drawing.Point(0, 664);
            this.lblButton.Name = "lblButton";
            this.lblButton.Padding = new System.Windows.Forms.Padding(30);
            this.lblButton.Size = new System.Drawing.Size(1105, 122);
            this.lblButton.TabIndex = 89;
            // 
            // btnClose
            // 
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(30, 30);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(244, 62);
            this.btnClose.TabIndex = 28;
            this.btnClose.Tag = "";
            this.btnClose.Text = "&TUTUP";
            this.btnClose.ThemeName = "MaterialBlueGrey";
            // 
            // VisitorDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.fullpicPanel);
            this.Controls.Add(this.lblButton);
            this.Controls.Add(this.radScrollablePanel1);
            this.Controls.Add(this.lblTitle);
            this.Name = "VisitorDetail";
            this.Size = new System.Drawing.Size(1105, 786);
            ((System.ComponentModel.ISupportInitialize)(this.universitas)).EndInit();
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PDOfBirth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaritalStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nationality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInmatePhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.regID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.namalengkap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDataVisitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelPic)).EndInit();
            this.panelPic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picCanvas)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.next)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVisitorFinger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvHistory.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailVisit)).EndInit();
            this.lblDetailVisit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pdf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatedate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updater)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDetailData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReligon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.religion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.city)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUniversitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.address)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPendidikan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.noidentitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jenisidentitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jeniskelamin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAgama)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullpicPanel)).EndInit();
            this.fullpicPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.closefull)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fullPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            this.lblTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblButton)).EndInit();
            this.lblButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private Telerik.WinControls.UI.RadLabel universitas;
        private System.Windows.Forms.PictureBox exit;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel updater;
        private Telerik.WinControls.UI.RadLabel inputdate;
        private Telerik.WinControls.UI.RadLabel telp;
        private Telerik.WinControls.UI.RadLabel jeniskelamin;
        private Telerik.WinControls.UI.RadLabel namalengkap;
        private Telerik.WinControls.UI.RadLabel jenisidentitas;
        private Telerik.WinControls.UI.RadLabel lblReligon;
        private Telerik.WinControls.UI.RadLabel lblUniversitas;
        private Telerik.WinControls.UI.RadLabel lblAgama;
        private Telerik.WinControls.UI.RadLabel lblTelp;
        private Telerik.WinControls.UI.RadLabel lblPendidikan;
        private Telerik.WinControls.UI.RadLabel lblNama;
        private Telerik.WinControls.UI.RadLabel religion;
        private Telerik.WinControls.UI.RadLabel address;
        private Telerik.WinControls.UI.RadLabel noidentitas;
        private Telerik.WinControls.UI.RadLabel lblEmail;
        private Telerik.WinControls.UI.RadLabel lblDataVisitor;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel city;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel inputter;
        private Telerik.WinControls.UI.RadLabel lblDetailData;
        private Telerik.WinControls.UI.RadLabel updatedate;
        private Telerik.WinControls.UI.RadLabel lblRegID;
        private Telerik.WinControls.UI.RadLabel regID;
        private Telerik.WinControls.UI.RadPanel panelPic;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox next;
        private System.Windows.Forms.PictureBox prev;
        private System.Windows.Forms.PictureBox picCanvas;
        private Telerik.WinControls.UI.RadLabel tot;
        private Telerik.WinControls.UI.RadPanel fullpicPanel;
        private System.Windows.Forms.PictureBox fullPic;
        private System.Windows.Forms.PictureBox closefull;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private System.Windows.Forms.PictureBox picFinger;
        private Telerik.WinControls.UI.RadLabel lblInmatePhoto;
        private Telerik.WinControls.UI.RadLabel lblButton;
        private Telerik.WinControls.UI.RadButton btnClose;
        private Telerik.WinControls.UI.RadLabel lblDetailVisit;
        private Telerik.WinControls.UI.RadGridView gvHistory;
        private System.Windows.Forms.PictureBox pdf;
        private System.Windows.Forms.PictureBox excel;
        private Telerik.WinControls.UI.RadLabel lblVisitorFinger;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel Nationality;
        private Telerik.WinControls.UI.RadLabel Vocation;
        private Telerik.WinControls.UI.RadLabel MaritalStatus;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel PDOfBirth;
    }
}
