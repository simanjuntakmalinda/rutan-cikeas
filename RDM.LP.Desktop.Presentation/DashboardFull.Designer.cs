﻿namespace RDM.LP.Desktop.Presentation
{
    partial class DashboardFull
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DashboardFull));
            this.PanelHome = new Telerik.WinControls.UI.RadPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblVersion = new Telerik.WinControls.UI.RadLabel();
            this.lblTitle = new Telerik.WinControls.UI.RadLabel();
            this.dashboard1 = new RDM.LP.Desktop.Presentation.CustomControls.Dashboard();
            ((System.ComponentModel.ISupportInitialize)(this.PanelHome)).BeginInit();
            this.PanelHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelHome
            // 
            this.PanelHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.PanelHome.Controls.Add(this.pictureBox1);
            this.PanelHome.Controls.Add(this.lblVersion);
            this.PanelHome.Controls.Add(this.lblTitle);
            this.PanelHome.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelHome.Location = new System.Drawing.Point(50, 50);
            this.PanelHome.Margin = new System.Windows.Forms.Padding(0);
            this.PanelHome.Name = "PanelHome";
            this.PanelHome.Padding = new System.Windows.Forms.Padding(10);
            // 
            // 
            // 
            this.PanelHome.RootElement.Opacity = 0.4D;
            this.PanelHome.Size = new System.Drawing.Size(1141, 60);
            this.PanelHome.TabIndex = 19;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).ShadowColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).RippleAnimationColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).FocusBorderColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).HighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).BorderHighlightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).Opacity = 0.4D;
            ((Telerik.WinControls.UI.RadPanelElement)(this.PanelHome.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(10);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(0))).Opacity = 0.4D;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).LeftColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).TopColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).RightColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).BottomColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).ForeColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).ForeColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).ForeColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).InnerColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).InnerColor2 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).InnerColor3 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).InnerColor4 = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).ForeColor = System.Drawing.Color.Transparent;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.PanelHome.GetChildAt(0).GetChildAt(1))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 2);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(57, 56);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // lblVersion
            // 
            this.lblVersion.BackColor = System.Drawing.Color.Transparent;
            this.lblVersion.Location = new System.Drawing.Point(8, 10);
            this.lblVersion.Margin = new System.Windows.Forms.Padding(0);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Padding = new System.Windows.Forms.Padding(55, 0, 0, 0);
            this.lblVersion.Size = new System.Drawing.Size(57, 2);
            this.lblVersion.TabIndex = 34;
            this.lblVersion.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = false;
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblTitle.Font = new System.Drawing.Font("Verdana", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(10, 29);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.lblTitle.ShowItemToolTips = false;
            this.lblTitle.Size = new System.Drawing.Size(1322, 26);
            this.lblTitle.TabIndex = 13;
            this.lblTitle.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // dashboard1
            // 
            this.dashboard1.BackColor = System.Drawing.Color.Transparent;
            this.dashboard1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dashboard1.BackgroundImage")));
            this.dashboard1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.dashboard1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dashboard1.Location = new System.Drawing.Point(50, 110);
            this.dashboard1.Name = "dashboard1";
            this.dashboard1.Size = new System.Drawing.Size(1141, 610);
            this.dashboard1.TabIndex = 20;
            // 
            // DashboardFull
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1241, 760);
            this.Controls.Add(this.dashboard1);
            this.Controls.Add(this.PanelHome);
            this.Name = "DashboardFull";
            this.Padding = new System.Windows.Forms.Padding(50);
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "DashboardFull";
            ((System.ComponentModel.ISupportInitialize)(this.PanelHome)).EndInit();
            this.PanelHome.ResumeLayout(false);
            this.PanelHome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadPanel PanelHome;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadLabel lblVersion;
        private Telerik.WinControls.UI.RadLabel lblTitle;
        private CustomControls.Dashboard dashboard1;
    }
}
